//
//  ProfileTop.swift
//  IntoBuy
//
//  Created by Manojkumar on 27/10/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import Foundation
import UIKit

public protocol buttonProtocol : NSObjectProtocol
{
    func setToList(sender: UIButton)
    func setToGrid(sender: UIButton)
    func ChangeToProductOrPost(sender: UIButton)
    func ChangeToPost(sender: UIButton)
    func uploadProfilePic(sender: UIButton)
    func openDairyScreen(sender: UIButton)
    func openFollowersScreen(tapGesture : UITapGestureRecognizer)
    func openFollowingScreen(tapGesture : UITapGestureRecognizer)
    func followBtnTarget()
    func editProfileBtnAction()
}


class IBProfilePersonalDetailsView: UIView{
    
    let profileImgView:UIImageView = UIImageView()
    let coverImgView:UIImageView = UIImageView()
    let m_ratingView:UIImageView = UIImageView()
    let cameraBtn:UIButton = UIButton()
    var btnDelegate:buttonProtocol?
    
    var selectionView:UIView!
    var typeView:UIView!
    var userNameStr = ""
    
    var m_bottomImageArr  = ["intobuy-my-diary-locked-list-view-icon.png","intobuy-my-diary-locked-grid-view-icon.png","intobuy-my-diary-locked-shop-view-icon.png","intobuy-my-diary-locked-user-icon.png"]//,"intobuy-my-diary-locked-book-view-icon.png"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //self.addControls()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func addControls(profileEntity: IBProfileEntity)
    {
        coverImgView.frame = frame
        coverImgView.userInteractionEnabled = true
        coverImgView.backgroundColor = UIColor.grayColor()
        coverImgView.image = GRAPHICS.MY_PAGE_BG_IMAGE()
        self.addSubview(coverImgView)
        
        var posX:CGFloat = 10
        var posY:CGFloat = 5
        var width:CGFloat = frame.size.width/4
        var height:CGFloat = width
        profileImgView.frame =  CGRectMake(posX,posY,width,height)
      //  profileImgView.layer.cornerRadius = profileImgView.frame.size.height/2;
        profileImgView.load(profileEntity.avatar, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil);
        profileImgView.clipsToBounds = true
        profileImgView.contentMode = .ScaleAspectFill
        profileImgView.layer.cornerRadius = height/2
        profileImgView.backgroundColor = UIColor.clearColor()

        coverImgView .addSubview(profileImgView);
        
        posX = profileImgView.frame.origin.x+10
        posY = profileImgView.frame.maxY + 5
        width = profileImgView.frame.size.width + 50
        height = 15
        let namelbl = UILabel(frame: CGRectMake(posX,posY,width,height));
        namelbl.text = profileEntity.username as String//dataDict.objectForKey("username")as? String;
        namelbl.font = UIFont .boldSystemFontOfSize(14)
        namelbl.textColor = UIColor.whiteColor()
        namelbl.textAlignment = NSTextAlignment.Left
        namelbl.backgroundColor = UIColor.clearColor();
        coverImgView.addSubview(namelbl);
        namelbl.sizeToFit()
        userNameStr = profileEntity.username as String
        
        posX = namelbl.frame.origin.x
        posY = namelbl.frame.maxY + 3
        width = namelbl.frame.size.width
        let countrylbl = UILabel(frame: CGRectMake(posX,posY,width,height));
        countrylbl.text = profileEntity.CountryName//dataDict.objectForKey("CountryName")as? String;
        countrylbl.font = UIFont .systemFontOfSize(14)
        countrylbl.textColor = UIColor.whiteColor()
        countrylbl.textAlignment = NSTextAlignment.Left
        countrylbl.backgroundColor = UIColor.clearColor();
        coverImgView.addSubview(countrylbl);
        countrylbl.sizeToFit()
        
        posX = namelbl.frame.origin.x
        posY = countrylbl.frame.maxY + 3
        width = GRAPHICS.Screen_Width()/2
        let descLbl = UILabel(frame: CGRectMake(posX,posY,width,height));
        descLbl.text = profileEntity.aboutme//dataDict.objectForKey("aboutme")as? String
        descLbl.font = UIFont .systemFontOfSize(14)
        descLbl.textColor = UIColor.whiteColor()
        descLbl.textAlignment = NSTextAlignment.Left
        descLbl.backgroundColor = UIColor.clearColor();
        coverImgView.addSubview(descLbl);

        
        let cameraImg = GRAPHICS.PROFILE_CAMERA_IMAGE()
        width = cameraImg.size.width/2
        height = cameraImg.size.height/2
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = cameraImg.size.width/1.5
            height = cameraImg.size.height/1.5
        }
        posX = self.frame.width - width - 5
        posY = self.bounds.origin.y + 10
        cameraBtn.frame =  CGRectMake(posX,posY,width,height)
        cameraBtn.setBackgroundImage(cameraImg, forState: UIControlState.Normal)
        cameraBtn.addTarget(self, action: #selector(IBProfilePersonalDetailsView.cameraBtnAction(_:)), forControlEvents: .TouchUpInside)
        coverImgView.addSubview(cameraBtn)
        
        posY = cameraBtn.frame.maxY + 3
        height = 15
        let editLbl = UILabel(frame: CGRectMake(posX,posY,width,height))
        editLbl.text = "edit"
        editLbl.font = GRAPHICS.FONT_REGULAR(12)
        editLbl.textColor = UIColor.whiteColor()
        editLbl.textAlignment = NSTextAlignment.Center
        coverImgView.addSubview(editLbl)

        if profileEntity.username == getUserNameFromUserDefaults()
        {
            cameraBtn.hidden = false
            editLbl.hidden = false
        }
        else
        {
            cameraBtn.hidden = true
            editLbl.hidden = true
        }
        
        let ratingBgImg = GRAPHICS.PROFILE_RATING_BACKGROUND_IMAGE()
        width = GRAPHICS.Screen_Width()/2
        height = self.frame.size.height/5
        posX = cameraBtn.frame.origin.x - width - 5
        posY = self.bounds.origin.y + 5
        m_ratingView.frame = CGRectMake(posX, posY, width, height)
        m_ratingView.image = ratingBgImg;
        //coverImgView.addSubview(m_ratingView) //Commented based on feedback
        //self.subViewsForRatingViews(m_ratingView, profileEntity: profileEntity) //Commented based on feedback
        
        posX = m_ratingView.frame.origin.x
        posY = m_ratingView.frame.maxY + 10
        width = 50
        height = 15
        
        let detailArr = [Followers,Following]//,Personal]
        let followingArr = profileEntity.following//dataDict.objectForKey("following") as? NSArray
        let followingStr = String(format: "%d",(followingArr.count) )
        
        let followersArr = profileEntity.followers//dataDict.objectForKey("followers") as? NSArray
        let followersStr = String(format: "%d",(followersArr.count) )
        
        let postArr = profileEntity.myPost//dataDict.objectForKey("myPost")as? NSArray
        let postCountStr = String(format: "%d",(postArr.count) )
        let statusArr = [followersStr,followingStr]//,postCountStr]
        
//        let statusCountArr = [dataDict.objectForKey("aboutme")as? String,dataDict.objectForKey("aboutme")as? String,dataDict.objectForKey("aboutme")as? String]
        
        for i in 0  ..< detailArr.count 
        {
            let statusCntLbl = UILabel(frame: CGRectMake(posX,posY,width,height))
            statusCntLbl.text = statusArr[i]
            statusCntLbl.font = GRAPHICS.FONT_BOLD(14)
            statusCntLbl.textColor = UIColor.whiteColor()
            statusCntLbl.textAlignment = NSTextAlignment.Center
            coverImgView.addSubview(statusCntLbl)
            
            posY = statusCntLbl.frame.maxY + 5
            let statusLbl = UILabel(frame: CGRectMake(posX,posY,width + 5,height))
            statusLbl.text = detailArr[i]
            statusLbl.tag = i
            statusLbl.font = GRAPHICS.FONT_BOLD(12)
            statusLbl.textColor = UIColor.whiteColor()
            statusLbl.textAlignment = NSTextAlignment.Center
            coverImgView.addSubview(statusLbl)
            statusLbl.userInteractionEnabled = true
            
            let statusBigLbl = UILabel(frame: CGRectMake(posX,posY - 5,width + 5,height + 20))
            statusBigLbl.tag = i
            coverImgView.addSubview(statusBigLbl)
            statusBigLbl.userInteractionEnabled = true
            
            coverImgView.bringSubviewToFront(statusLbl)

            
            let bigLbltapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProfilePersonalDetailsView.statusLabelTapGesture(_:)))
            statusBigLbl.addGestureRecognizer(bigLbltapGesture)

            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProfilePersonalDetailsView.statusLabelTapGesture(_:)))
            statusLbl.addGestureRecognizer(tapGesture)
            
            let dividerImg = GRAPHICS.PROFILE_DIVIDER_LINE_IMAGE()
            let dividerWidth = dividerImg.size.width/2
            let dividerHeight = dividerImg.size.height/1.8
            posY = statusCntLbl.frame.origin.y
            posX = statusLbl.frame.maxX+5
            let dividerImgView = UIImageView(frame: CGRectMake(posX,posY,dividerWidth,dividerHeight))
            dividerImgView.image = GRAPHICS.PROFILE_DIVIDER_LINE_IMAGE()//GRAPHICS.MY_PRODUCT_GRID_DIVIDER_IMAGE() //Jose Edited //dividerImg
            dividerImgView.contentMode = .ScaleAspectFit
            coverImgView.addSubview(dividerImgView)
            if(i == detailArr.count - 1){
                dividerImgView.removeFromSuperview()
            }
            posX = dividerImgView.frame.maxX+5
        }
        
        let productImg = GRAPHICS.APP_PINK_BUTTON_IAMGE()
        posY = posY + (2 * height) + 15
        width = productImg.size.width/1.8
        posX = GRAPHICS.Screen_Width()/2
        height = productImg.size.height/1.8
        let followBtn = UIButton(frame: CGRectMake(posX,posY,width,height))
        followBtn.setBackgroundImage(productImg, forState: UIControlState.Normal)
        followBtn.setTitle("Follow", forState: .Normal)
        followBtn.titleLabel!.font = GRAPHICS.FONT_REGULAR(14)
        followBtn.addTarget(self, action: #selector(IBProfilePersonalDetailsView.followBtnAction), forControlEvents: .TouchUpInside)
        //coverImgView.addSubview(followBtn)
        //followBtnTarget
        if profileEntity.username == getUserNameFromUserDefaults()
        {
            followBtn.setTitle("Edit Profile", forState: .Normal)
        }
        else
        {
            if profileEntity.am_following == "1"
            {
                followBtn.setTitle("Unfollow", forState: .Normal)
            } 
            else
            {
                followBtn.setTitle("Follow", forState: .Normal)
            }
        }
        
        
        
        
        var paddingW:CGFloat = 20;
        if (GRAPHICS.Screen_Type() == IPHONE_6) {
           // paddingW = 30;
            paddingW = 25
        } else if (GRAPHICS.Screen_Type() == IPHONE_6_Plus) {
            paddingW = 40.0;
        }
        posX = coverImgView.frame.origin.x + paddingW
        
        
        height = coverImgView.frame.size.height/4.3//width - 25
        var xPos = CGFloat(10)//coverImgView.bounds.origin.x + 10
        
        width = ((coverImgView.frame.size.width - 20)/4) - (10) //Spacing between seprator and view
        //Uncomment this code if divider image is required
        //width = ((coverImgView.frame.size.width - 20)/4)/*View and button width*/ - (CGFloat(20/3)/4)/*SeperatorWidth*/ - (10) //Spacing between seprator and view
        
        posY = coverImgView.frame.size.height - height

        let seperatorWidth = CGFloat(20/3)

        for i in 0  ..< m_bottomImageArr.count
        {
            
            if i == 1{
            typeView = UIView(frame: CGRectMake(xPos,posY, width, height))
            typeView.backgroundColor = headerColor
            coverImgView.addSubview(typeView)
            }
            
            
            if i == 3{
            selectionView = UIView(frame: CGRectMake(xPos,posY, width, height))
            selectionView.backgroundColor = headerColor
            coverImgView.addSubview(selectionView)
            }
        
            let bottomBigBtns = UIButton(frame: CGRectMake(xPos,posY, width, height))
            bottomBigBtns.tag = i + 100
            bottomBigBtns.addTarget(self, action: #selector(IBProfilePersonalDetailsView.bottomBtnActions(_:)), forControlEvents: UIControlEvents.TouchUpInside);
            coverImgView.addSubview(bottomBigBtns)
            //coverImgView.bringSubviewToFront(bottomBigBtns)
            
            xPos = xPos + width
            
            let btnImage = UIImage(named: m_bottomImageArr[i])
            let btnW = width > height ? height - 10 : width - 10 //(btnImage?.size.width)!/2
            let btnH = btnW//(btnImage?.size.height)!/2
            let btnY = posY + 5//(self.frame.size.height - btnH) - 7
            let btnPosX = bottomBigBtns.frame.origin.x + ((bottomBigBtns.frame.width/2) - (btnW/2)) //posX + 5//CGFloat(5)
//            if GRAPHICS.Screen_Type() == IPHONE_5 || GRAPHICS.Screen_Type() == IPHONE_4{
//                posX = (bottomBigBtns.frame.maxX - btnW - btnW/2.2)
//            }
//            else if GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus{
//                posX = (bottomBigBtns.frame.maxX - btnW - btnW/1.5)
//            }
            let bottomBtns = UIImageView(frame: CGRectMake(btnPosX/*posX*/,btnY, btnW, btnH))//UIButton(frame: CGRectMake(btnPosX/*posX*/,btnY, btnW, btnH))
            bottomBtns.image = btnImage
            bottomBtns.tag = i + 10
            bottomBtns.contentMode = .ScaleAspectFit
//            bottomBtns .addTarget(self, action: #selector(IBProfilePersonalDetailsView.bottomBtnActions(_:)), forControlEvents: UIControlEvents.TouchUpInside);
            coverImgView.addSubview(bottomBtns)
            coverImgView.bringSubviewToFront(bottomBtns)

            xPos = bottomBigBtns.frame.maxX + 10 //This will apply if there is no seperator
            
//            //Jose Edited
//            if i != m_bottomImageArr.count - 1
//            {
//                let dividerLineForBlocks = UIImageView(frame: CGRectMake(bottomBigBtns.frame.maxX+5,posY,seperatorWidth,bottomBigBtns.frame.height))
//                dividerLineForBlocks.image = GRAPHICS.MY_PRODUCT_GRID_DIVIDER_IMAGE() //Jose Edited //dividerImg
//                dividerLineForBlocks.contentMode = .ScaleAspectFit
//                coverImgView.addSubview(dividerLineForBlocks)
//                coverImgView.bringSubviewToFront(dividerLineForBlocks)
//                
//                xPos = dividerLineForBlocks.frame.maxX + 5
//            }
            
            
//            if (GRAPHICS.Screen_Type() == IPHONE_6) {
//               //posX = posX + btnW + 38
//                posX = posX + btnW + 40
//            } else if (GRAPHICS.Screen_Type() == IPHONE_6_Plus) {
//                posX = posX + btnW + 50
//            }
//            else{
//                posX = posX + btnW + 30
//                
//            }

        }
    }
    func followBtnAction()  {
        if userNameStr == getUserNameFromUserDefaults()
        {
            btnDelegate!.editProfileBtnAction()
        }
        else
        {
            btnDelegate!.followBtnTarget()
        }
        
    }
    func bottomBtnActions(sender: UIButton)
    {
        //        let width = coverImgView.frame.size.width/5
//        let height = width - 25
        
        if(sender.tag == 10 || sender.tag == 100 )
        {
            if typeView != nil
            {
                typeView.removeFromSuperview()
                typeView = nil;
            }
            let width = sender.frame.width//coverImgView.frame.size.width/5
            let height = sender.frame.height//coverImgView.frame.size.height/4.3//width - 25
            let xPos:CGFloat!
            
            xPos = sender.frame.origin.x
//            if sender.frame.size.width == width
//            {
//                xPos = sender.frame.origin.x
//            }
//            else{
//                xPos = 0//sender.frame.origin.x - width/3.2
//            }
            let posY = sender.frame.origin.y//coverImgView.frame.size.height - height
            typeView = UIView(frame: CGRectMake(xPos,posY, width, height))
            typeView.backgroundColor = headerColor
            coverImgView.addSubview(typeView)
            
            if sender.tag == 100
            {
                //print(sender.tag%100)
                let button = coverImgView.viewWithTag(sender.tag%100 + 10) as! UIImageView
                coverImgView.bringSubviewToFront(button)
            }
            else
            {
                coverImgView.bringSubviewToFront(sender)
            }
            btnDelegate!.setToList(sender)
        }
        else if(sender.tag == 11 || sender.tag == 101){
            
            if typeView != nil
            {
                typeView.removeFromSuperview()
                typeView = nil;
            }
            let width = sender.frame.width//coverImgView.frame.size.width/5
            let height = sender.frame.height//coverImgView.frame.size.height/4.3//width - 25
            let xPos:CGFloat!
            xPos = sender.frame.origin.x

//            if sender.frame.size.width == width
//            {
//                xPos = sender.frame.origin.x
//            }
//            else{
//                let button = coverImgView.viewWithTag(sender.tag%10 + 100) as! UIButton
//
//                xPos = button.frame.origin.x//(sender.frame.maxX - width + sender.frame.size.width/2)
//            }//sender.frame.maxX - width
            let posY = sender.frame.origin.y//coverImgView.frame.size.height - height
            typeView = UIView(frame: CGRectMake(xPos,posY, width, height))
            typeView.backgroundColor = headerColor
            coverImgView.addSubview(typeView)
            
            if sender.tag == 101
            {
                let button = coverImgView.viewWithTag(sender.tag%100 + 10) as! UIImageView
                coverImgView.bringSubviewToFront(button)
            }
            else
            {
                coverImgView.bringSubviewToFront(sender)
            }

            
            btnDelegate!.setToGrid(sender)
        }
        else if sender.tag == 12 || sender.tag == 102
        {
            if selectionView != nil
            {
                selectionView.removeFromSuperview()
                selectionView = nil;
            }

            let width = sender.frame.width//coverImgView.frame.size.width/5
            let height = sender.frame.height//coverImgView.frame.size.height/4.3// width - 25
            let xPos:CGFloat!
            xPos = sender.frame.origin.x
//            if sender.frame.size.width == width
//            {
//                xPos = sender.frame.origin.x
//            }
//            else{
//                let button = coverImgView.viewWithTag(sender.tag%10 + 100) as! UIButton
//                
//                xPos = button.frame.origin.x
//               // xPos = sender.frame.origin.x - width/3.2
//            }//sender.frame.maxX - width
            let posY = sender.frame.origin.y//coverImgView.frame.size.height - height
            selectionView = UIView(frame: CGRectMake(xPos,posY, width, height))
            selectionView.backgroundColor = headerColor
            coverImgView.addSubview(selectionView)
            if sender.tag == 102
            {
                let button = coverImgView.viewWithTag(sender.tag%100 + 10) as! UIImageView
                coverImgView.bringSubviewToFront(button)
            }
            else
            {
                coverImgView.bringSubviewToFront(sender)
            }

            
            btnDelegate!.ChangeToProductOrPost(sender)
        }
        else if sender.tag == 13 || sender.tag == 103
        {
            if selectionView != nil
            {
                selectionView.removeFromSuperview()
                selectionView = nil;
            }

            let width = sender.frame.width//coverImgView.frame.size.width/5
            let height = sender.frame.height//coverImgView.frame.size.height/4.3// width - 25
            let xPos:CGFloat!
            xPos = sender.frame.origin.x
//            if sender.frame.size.width == width
//            {
//                xPos = sender.frame.origin.x
//            }
//            else{
//                let button = coverImgView.viewWithTag(sender.tag%10 + 100) as! UIButton
//                
//                xPos = button.frame.origin.x
//                //xPos = sender.frame.origin.x - width/3.2
//            }//sender.frame.maxX - width
            let posY = sender.frame.origin.y//coverImgView.frame.size.height - height
            selectionView = UIView(frame: CGRectMake(xPos,posY, width, height))
            selectionView.backgroundColor = headerColor
            coverImgView.addSubview(selectionView)
            coverImgView.bringSubviewToFront(sender)
            if sender.tag == 103
            {
                let button = coverImgView.viewWithTag(sender.tag%100 + 10) as! UIImageView
                coverImgView.bringSubviewToFront(button)
            }
            else
            {
                coverImgView.bringSubviewToFront(sender)
            }

            btnDelegate!.ChangeToPost(sender)
        }

        else if(sender.tag == 14 || sender.tag == 104){
            btnDelegate!.openDairyScreen(sender)
        }
    }
    // methods for controls in rating view
    func subViewsForRatingViews(view :UIView , profileEntity: IBProfileEntity)
    {
        let buyerImg = GRAPHICS.PROFILE_RATING_BUYER_ICON_IMAGE()
        var xPos = view.bounds.origin.x + 5
        var yPos = view.bounds.origin.y + 9
        if(GRAPHICS .Screen_Type() == IPHONE_6){
            xPos = view.bounds.origin.x + 10
            yPos = view.bounds.origin.y + 10
        }else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus){
            xPos = view.bounds.origin.x + 15
            yPos = view.bounds.origin.y + 15
        }
        
        var width = buyerImg.size.width/2
        var height = buyerImg.size.height/2
        let buyerImgView = UIImageView(frame: CGRectMake(xPos, yPos, width, height))
        buyerImgView.image = buyerImg
        view.addSubview(buyerImgView)
        
        xPos = buyerImgView.frame.maxX + 5
        if(GRAPHICS .Screen_Type() == IPHONE_6){
            xPos = buyerImgView.frame.maxX + 5
        }else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus){
            xPos = buyerImgView.frame.maxX + 7
        }
        
        var buyerCountStr = profileEntity.buytrophy//dataDict.objectForKey("buytrophy")as? String
        if buyerCountStr == ""
        {
            buyerCountStr = "0"
        }
        width = 1.5 * width
        let buyerCntLbl = UILabel(frame: CGRectMake(xPos, yPos, width, height))
        buyerCntLbl.text = buyerCountStr
        buyerCntLbl.textAlignment = NSTextAlignment.Center
        view.addSubview(buyerCntLbl)
        
        let sellerImg = GRAPHICS.PROFILE_RATING_SELLER_ICON_IMAGE()
        xPos = buyerCntLbl.frame.maxX + 7
        if(GRAPHICS .Screen_Type() == IPHONE_6){
            xPos = buyerCntLbl.frame.maxX + 10
        }else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus){
            xPos = buyerCntLbl.frame.maxX + 30
        }
        width = sellerImg.size.width/2
        height = sellerImg.size.height/2
        //xPos = view.frame.size.width/2 - width
        let sellerImgView = UIImageView(frame: CGRectMake(xPos, yPos, width, height))
        sellerImgView.image = sellerImg
        view.addSubview(sellerImgView)
        
        var sellerCountStr = profileEntity.selltrophy//dataDict.objectForKey("buytrophy")as? String
        if sellerCountStr == ""
        {
            sellerCountStr = "0"
        }

        xPos = sellerImgView.frame.maxX + 5
        width = 1.5 * width
        let sellerCntLbl = UILabel(frame: CGRectMake(xPos, yPos, width, height))
        sellerCntLbl.text = sellerCountStr
        sellerCntLbl.textAlignment = NSTextAlignment.Center
        view.addSubview(sellerCntLbl)
        
        xPos = view.frame.size.width/1.5//sellerCntLbl.frame.maxX + 5
        if(GRAPHICS .Screen_Type() == IPHONE_6){
            xPos = sellerCntLbl.frame.maxX + 3
        }else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus){
            xPos = sellerCntLbl.frame.maxX + 3
        }
        let starImg = GRAPHICS.PROFILE_RATING_ICON_IMAGE()
        width = starImg.size.width/2
        height = starImg.size.height/2
        for i in 0  ..< 3
        {
            let starImgView = UIImageView(frame: CGRectMake(xPos, yPos, width, height))
            starImgView.image = starImg
            view.addSubview(starImgView)
            xPos = xPos + width + 1.5;
            
        }
        
        view.frame.size.width = xPos + 10
        view.frame.origin.x = coverImgView.frame.size.width - view.frame.size.width - 10 - cameraBtn.frame.size.width
        sellerImgView.frame.origin.x = view.frame.size.width/2 - sellerImgView.frame.size.width - 10
        sellerCntLbl.frame.origin.x = sellerImgView.frame.maxX + 5
        
        var titleArr = [buyerPoints,sellerPoints,sellerRating]
        xPos = buyerImgView.frame.origin.x - 2
        if(GRAPHICS .Screen_Type() == IPHONE_6){
            xPos = buyerImgView.frame.origin.x - 5
        }else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus){
            xPos = buyerImgView.frame.origin.x - 10
        }

        yPos = buyerImgView.frame.maxY
        width = view.frame.size.width/3.3
        height = 15
        var fontSize = CGFloat()
        fontSize = 8
        if(GRAPHICS .Screen_Type() == IPHONE_6){
            height = 20
            fontSize = 8
        }else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus){
            height = 20
            fontSize = 9
        }

        for i in 0  ..< titleArr.count 
        {
            let titleLbl = UILabel(frame: CGRectMake(xPos, yPos, width, height))
            titleLbl.text = titleArr[i]
            titleLbl.textAlignment = NSTextAlignment.Center
            titleLbl.font = GRAPHICS.FONT_BOLD(fontSize)
            view.addSubview(titleLbl)
            xPos = xPos + width + 2;
        }

    }
    func cameraBtnAction(sender:UIButton)
    {
        btnDelegate!.uploadProfilePic(sender)
    }
    func statusLabelTapGesture(tapGesture : UITapGestureRecognizer) -> () {
        
        if tapGesture.view?.tag == 1
        {
            btnDelegate!.openFollowingScreen(tapGesture)
        }
        else if tapGesture.view?.tag == 0
        {
            btnDelegate!.openFollowersScreen(tapGesture)
        }
        else
        {
            
        }
        
    }
}
