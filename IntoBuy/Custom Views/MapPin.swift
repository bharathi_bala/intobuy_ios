//
//  MyAnnotation.swift
//  MeetIntro
//
//  Created by Abdullah on 08/01/16.
//  Copyright © 2016 Smaat Apps and Technologies. All rights reserved.
//
import MapKit
class MapPin : NSObject, MKAnnotation {
    dynamic var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}