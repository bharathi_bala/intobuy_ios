//
//  NSObject+iOS.swift
//  IntoBuy
//
//  Created by SmaatApps on 12/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import Foundation
import UIKit

public var wallUrl = "http://50.87.171.216/intobuy/Wall/"
public var userIDKey = "userId"
public var userNameKey = "username"
public var cartCountKey = "cartCount"
public var productDetailsKey = "productDetailsKey"
public var shippingDetailsKey = "shippingDetailsKey"

let requestedComponents: NSCalendarUnit = [
    NSCalendarUnit.Year,
    NSCalendarUnit.Month,
    NSCalendarUnit.Day,
    NSCalendarUnit.Hour,
    NSCalendarUnit.Minute,
    NSCalendarUnit.Second
]


public func showAlertViewWithMessage(message:NSString)->()
{
    let alertview = UIAlertView(title: IntoBuy, message:message as NSString as String, delegate: nil, cancelButtonTitle: Ok)
    alertview.show()
    
}
public func getWidthandHeightofImage(imageName:UIImage)->CGSize
{
    var fWidth:CGFloat = 0.0
    var fHeight:CGFloat = 0.0
    
    let searchiew:UIImage = imageName
    fWidth = searchiew.size.width/2.2
    fHeight = searchiew.size.height/2.2
    
    if GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus
    {
        fWidth = searchiew.size.width/1.7
        fHeight = searchiew.size.height/1.7
        
    }
    
    return CGSizeMake(fWidth, fHeight)
    
}

/*
 extension String
 {
 var floatValue: Float
 {
 return (self as String).floatValue
 }
 
 var doubleValue: Double
 {
 return (self as String).doubleValue
 }
 
 }
 */
//extension NSDate {



func getCurrencyCode() -> String
{
    let currentLocale = NSLocale.currentLocale()
    
    let  currentCode : String = currentLocale.objectForKey(NSLocaleCurrencyCode) as! String
    
    return currentCode
}

func getCurrencySymbol() -> String
{
    let currentLocale = NSLocale.currentLocale()
    
    let currencySymbol = currentLocale.objectForKey(NSLocaleCurrencySymbol) as! String
    
    return currencySymbol
    
}
//-(void)saveUserIdInUserDefaults:(NSString *)userProfile{
//    NSUserDefaults *userTypeID = [NSUserDefaults standardUserDefaults];
//    DLog(@"%@ stored",userProfile);
//    [userTypeID setValue:userProfile forKey:kUserIDKeyInDefaults];
//}

//Saving user id
func saveUserIdInUserDefaults(userProfile: String)
{
    let userIdUd = NSUserDefaults.standardUserDefaults()
    userIdUd .setValue(userProfile, forKey: userIDKey)
}
//Saving user name
func saveUserNameInUserDefaults(userName: String)
{
    let userNameUd = NSUserDefaults.standardUserDefaults()
    userNameUd .setValue(userName, forKey: userNameKey)
}
//saving usertype
func saveUserTypeinUserdefaults(strusertype: String)
{
    let usertype = NSUserDefaults.standardUserDefaults()
    usertype .setValue(strusertype, forKey: "usertype")
}
func getUserTypeString() -> String
{
    let userdefaults = NSUserDefaults.standardUserDefaults()
    var str : String!
    
    if userdefaults.objectForKey("usertype") != nil
    {
        str =  userdefaults.objectForKey("usertype") as! String
    }
    else
    {
        str = ""
    }
    return str
}

//Savbing CountryCode
func saveCountryCode(strcountryCode: String)
{
    let usertype = NSUserDefaults.standardUserDefaults()
    usertype .setValue(strcountryCode, forKey: "country")
}
func getCountryCode() -> String
{
    let userdefaults = NSUserDefaults.standardUserDefaults()
    var str : String!
    
    if userdefaults.objectForKey("country") != nil
    {
        str =  userdefaults.objectForKey("country") as! String
    }
    else
    {
        str = ""
    }
    return str
}
//Saving Country Name
func saveCountryName(strcountryName: String)
{
    let usertype = NSUserDefaults.standardUserDefaults()
    usertype .setValue(strcountryName, forKey: "CountryName")
}
func getCountryNameString() -> String
{
    let userdefaults = NSUserDefaults.standardUserDefaults()
    var str : String!
    
    if userdefaults.objectForKey("CountryName") != nil
    {
        str =  userdefaults.objectForKey("CountryName") as! String
    }
    else
    {
        str = ""
    }
    return str
}

func getUserIdFromUserDefaults()-> String
{
    let userdefaults = NSUserDefaults.standardUserDefaults()
    var str : String!
    
    if userdefaults.objectForKey(userIDKey) != nil
    {
        str =  userdefaults.objectForKey(userIDKey) as! String
    }
    else
    {
        str = ""
    }
    return str
}
func getUserNameFromUserDefaults()-> String
{
    
    let userdefaults = NSUserDefaults.standardUserDefaults()
    var str : String!
    
    if userdefaults.objectForKey(userNameKey) != nil
    {
        str =  userdefaults.objectForKey(userNameKey) as! String
    }
    else
    {
        str = ""
    }
    return str
   
}
//saving product details dict
func saveProductDetailsToUserDefaults(productDetailsArray : NSArray)
{
    //productDetailsKey
    let productDetailsUd = NSUserDefaults.standardUserDefaults()
    productDetailsUd .setValue(productDetailsArray, forKey: productDetailsKey)
}
// get product details
func getProductDetailsFromUD()-> NSArray
{
    var arry = NSArray()
    let userdefaults = NSUserDefaults.standardUserDefaults()
    if userdefaults.objectForKey(productDetailsKey) != nil
    {
        arry =  userdefaults.objectForKey(productDetailsKey) as! NSArray
    }
    return arry
}
//save shipping details
func saveShippingDetailsToUserDefaults(shippingDetailsDict : NSDictionary)
{
    //productDetailsKey
    let shippingDetailsUd = NSUserDefaults.standardUserDefaults()
    shippingDetailsUd .setValue(shippingDetailsDict, forKey: shippingDetailsKey)
}
// get shipping details
func getShippingDetailsFromUD()-> NSDictionary
{
    let shippingDetailsUd = NSUserDefaults.standardUserDefaults()
    let shippingDetailsDict = checkKeyNotAvailForDict(shippingDetailsUd, key: shippingDetailsKey)//shippingDetailsUd.objectForKey(shippingDetailsKey) as! NSDictionary
    return shippingDetailsDict as! NSDictionary
}

func checkKeyNotAvailForDict( defaults: NSUserDefaults, key:String) -> AnyObject
{
    if let val = defaults.objectForKey(key)
    {
        if val is NSDictionary
        {
            return val
        }
        return NSDictionary()
    }
    else {
        
        return NSDictionary()
    }
}


//saving cart count
func saveCartCountInUserDefaults(cartCount: String)
{
    let cartCountUD = NSUserDefaults.standardUserDefaults()
    cartCountUD .setValue(cartCount, forKey: cartCountKey)
}
func getCartCountFromUserDefaults()-> String
{
    let cartCountUD = NSUserDefaults.standardUserDefaults()
    let cartCountStr = GRAPHICS.checkKeyNotAvailInUD(cartCountUD, key: cartCountKey) as! String//cartCountUD.objectForKey(cartCountKey) as! String
    if cartCountStr is String
    {
        return cartCountStr
    }
    else{
        return ""
    }
}

func convertStringToEncodedString(text : String) -> String
{
    let strData = text.dataUsingEncoding(NSNonLossyASCIIStringEncoding)
    let encodeStr = String.init(data: strData!, encoding:NSUTF8StringEncoding)
    return encodeStr!
}
func decodeEncodedStringToNormalString(text : String) -> String {
    let strData = text.dataUsingEncoding(NSUTF8StringEncoding)
    let decodeStr = String.init(data: strData!, encoding:NSNonLossyASCIIStringEncoding)
    return decodeStr!
    
    
}


//-(NSString *)convertStringToEncodedString:(NSString *)string
//{
//    NSData *data = [string dataUsingEncoding:NSNonLossyASCIIStringEncoding];
//    NSString *base64String = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//    return base64String;
//}
//
//-(NSString *)decodeEncodedStringToNormalString:(NSString *)string
//{
//    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *decodedString = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
//    return decodedString;
//}


func convertUtcToLocalFormatForChat(utcTime: String) -> String
{
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = NSTimeZone(name: "UTC")
    let date = dateFormatter.dateFromString(utcTime)
    
    // change to a readable time format and change to local time zone
    dateFormatter.dateFormat = "dd-MMM hh:mm a"
    dateFormatter.timeZone = NSTimeZone.localTimeZone()
    let timeStamp = dateFormatter.stringFromDate(date!)
    
    return timeStamp
}

func convertUtcToLocalFormat(utcTime: String) -> String
{
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = NSTimeZone(name: "UTC")
    let date = dateFormatter.dateFromString(utcTime)
    
    // change to a readable time format and change to local time zone
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = NSTimeZone.localTimeZone()
    let timeStamp = dateFormatter.stringFromDate(date!)
    
    return timeStamp
}

func imageArrayForHomePage() -> NSMutableArray
{
    let bottomImageArr = [GRAPHICS.BOTTOM_BAR_HOME_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_PROFILE_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CAMERA_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CHAT_ICON_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CART_BTN_IMAGE()] as NSMutableArray
    
    return bottomImageArr;
}
func imageArrayForNotHomePage() -> NSMutableArray
{
    let bottomImageArr = [GRAPHICS.BOTTOM_BAR_HOME_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_PROFILE_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CAMERA_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_ACTIVITY_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CART_BTN_IMAGE()] as NSMutableArray
    return bottomImageArr;
}

