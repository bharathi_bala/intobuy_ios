//
//  CalloutAnnotation.swift
//  IntoBuy
//
//  Created by SmaatApps on 21/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import CoreLocation

class CalloutAnnotation: NSObject, MKAnnotation
{
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var strImagePropertyURl : String?
    var strAgentName  : String?
    var strPhoneNumber : String?
    var strPinIndex : String?
    var tag : Int?


    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}

