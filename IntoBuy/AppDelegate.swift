//
//  AppDelegate.swift
//  IntoBuy
//
//  Created by Manojkumar on 25/11/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import FBSDKLoginKit




@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navController : UINavigationController?
    var m_deviceToken:NSString!
    
    var m_firstImage : UIImage = UIImage()
    var m_secodImage : UIImage = UIImage()
    
    var m_thirdImage : UIImage = UIImage()
    
    var m_imageIndex1 : NSInteger = -1
    var diaryDictionary = NSDictionary()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        /*
        Paypal account details: smaatappsiteam@gmail.com
        
        password:Smaat123$
        
        Sandbox ID:APP-80W284485P519543T
        Live App ID:APP-68K0259635570873J
*/

        
        GRAPHICS.initializeTheScreen()
        
        // Start -> Registering for Notifications
        
        application.registerUserNotificationSettings(
            UIUserNotificationSettings(
                forTypes: [.Alert, .Badge, .Sound],
                categories: nil))
        application.statusBarHidden = false
        
       
        
        
        let splashVC = IBSplashViewController()
        navController = UINavigationController(rootViewController: splashVC)
        navController?.navigationBarHidden = true
        self.window?.rootViewController = navController
        
        let sharedApplication = UIApplication.sharedApplication();
        let isFacebookAuthorized = sharedApplication.canOpenURL(NSURL(fileURLWithPath: "fbauth://authorize"));
        
        [FBSDKApplicationDelegate .sharedInstance() .application(application, didFinishLaunchingWithOptions: launchOptions)];
        
        //#warning "Enter your credentials"
        
        PayPalMobile.initializeWithClientIdsForEnvironments([PayPalEnvironmentNoNetwork:"", PayPalEnvironmentSandbox:"ASFBliBfvvleddHQtWYIfruompOIgmIWq1-Tg23b8BTNraD_0KayqC2Im2JPmQmxZH-Xr-pI8-Ose6mf"])
        
        self.window?.backgroundColor = UIColor.whiteColor()
        
        self.window?.makeKeyAndVisible()

        return true
    }
    
    func application(application: UIApplication, willFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        let webView = UIWebView(frame: CGRectMake(0, 0, 320, 320))
        webView.delegate = nil
        return true
    }

    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
//        NSString *token = [[deviceToken description]
//            stringByTrimmingCharactersInSet:
//            [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
//        token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
//        token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
//        token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
//        NSLog(@"content---%@", token);
//        AppDelegate *appDelegate =
//            (AppDelegate *)[[UIApplication sharedApplication] delegate];
//        appDelegate.m_deviceToken = token;

        
       // let token = deviceToken.description .stringByTrimmingCharactersInSet(NSCharacterSet)
        let characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
        
        var token: String = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
        token = token .stringByReplacingOccurrencesOfString(">", withString: "")
        token = token.stringByReplacingOccurrencesOfString("<", withString: "")
        //print( token )
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.m_deviceToken = token

    }
    
   
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool
    {
        return FBSDKApplicationDelegate .sharedInstance() .application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation);
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        FBSDKAppEvents .activateApp()
        
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
        
        
        
    }


}

