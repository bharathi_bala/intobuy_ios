//
//  IBSearchTableViewCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 27/11/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

public protocol searchProtocol : NSObjectProtocol
{
    func tapOnProfileImage(tapGesture : UITapGestureRecognizer)
}


class IBSearchTableViewCell: UITableViewCell {

    var m_profileImgView = UIImageView()
    var m_nameLabel = UILabel()
    var m_trophyLbl = UILabel()
    var m_followBtn = UIButton()
    var isFromHome = Bool()
    var imgDelegate:searchProtocol!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsForSearch()
    }
    func createControlsForSearch()
    {
        self.frame.size.height = 70.0
        let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
        
        
        var viewWid = profileMaskImg.size.width/1.7
        var viewHei = profileMaskImg.size.height/1.7
        var posX = self.bounds.origin.x + 15
        var posY = (self.frame.size.height - viewHei)/2
        
        m_profileImgView.frame = CGRectMake(posX,posY,viewWid,viewHei)
        m_profileImgView.layer.cornerRadius = viewHei/2
        m_profileImgView.clipsToBounds = true
        m_profileImgView.userInteractionEnabled = true
        self.addSubview(m_profileImgView)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IBSearchTableViewCell.profileImgTapGesture(_:)))
        m_profileImgView.addGestureRecognizer(tapGesture)
        
        viewHei = 20
        viewWid = GRAPHICS.Screen_Width()/2.5
        posX = m_profileImgView.frame.maxX + 10
        let fontSize:CGFloat = 14;
        m_nameLabel.frame = CGRectMake(posX,posY,viewWid,viewHei)
        m_nameLabel.font = GRAPHICS.FONT_REGULAR(fontSize)
        self.addSubview(m_nameLabel)
        
        posY = m_nameLabel.frame.maxY
        m_trophyLbl.frame = CGRectMake(posX,posY,viewWid,viewHei)
        m_trophyLbl.font = GRAPHICS.FONT_REGULAR(fontSize)
        self.addSubview(m_trophyLbl)
        
        
        let followImg = GRAPHICS.SEARCH_FOLLOW_BTN_IMAGE()
       // let followingImg = GRAPHICS.SEARCH_FOLLOWING_BTN_IMAGE()
        viewWid = followImg.size.width/2;
        viewHei = followImg.size.height/2
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            viewWid = followImg.size.width/1.8;
            viewHei = followImg.size.height/1.8
        }
        posX = GRAPHICS.Screen_Width() - viewWid - 10
        posY = (self.frame.size.height - viewHei)/2
        
        m_followBtn.frame = CGRectMake(posX,posY,viewWid,viewHei)
        m_followBtn.setBackgroundImage(followImg, forState: UIControlState.Normal)
        //m_followBtn.setBackgroundImage(followingImg, forState: UIControlState.Selected)
        self.addSubview(m_followBtn)
        
//        let dividerImg = GRAPHICS.SEARCH_DIVIDER_IMAGE()
//        viewWid = dividerImg.size.width/2;
//        viewHei = dividerImg.size.height/2
//        posX = (self.frame.size.width - viewWid)/2
//        posY = (self.frame.size.height - viewHei) 
//        let dividerImgView = UIImageView(frame: CGRectMake(posX,posY,viewWid,viewHei))
//        dividerImgView.image = dividerImg
//        self.addSubview(dividerImgView)
//
        
    }
    func valuesForControls(profileImageUrl : String, name: String ,followState: String, var trophyStr: String, isFromHome : Bool ,profileImgTag : Int)
    {
        m_nameLabel.text = name
        //m_profileImgView.load(profileImageUrl, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil) //Commented by arun to avoid slow loading of images in table view
        //m_profileImgView.sd_setImageWithURL(NSURL(string: profileImageUrl), placeholderImage: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE())
        m_profileImgView.sd_setImageWithURL(NSURL(string: profileImageUrl), placeholderImage: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), options: .ScaleDownLargeImages)
        m_profileImgView.tag = profileImgTag
        m_profileImgView.clipsToBounds = true
        m_profileImgView.layer.cornerRadius = m_profileImgView.frame.size.height/2
        m_profileImgView.backgroundColor = UIColor.clearColor()
        if trophyStr == ""
        {
            trophyStr = "0"
        }
        m_trophyLbl.text = String(format: "%@ trophies", trophyStr)
        if(followState == "1")
        {
            //m_followBtn.selected = true
            m_followBtn.setTitle("Unfollow", forState: .Normal)
        }
        else{
            //m_followBtn.selected = false
            m_followBtn.setTitle("Follow", forState: .Normal)
        }
        if isFromHome == true
        {
            m_nameLabel.frame.size.height = 15;
            m_trophyLbl.frame.size.height = 15;
            m_nameLabel.frame.origin.y = (m_profileImgView.frame.maxY - m_nameLabel.frame.size.height)/2
            m_trophyLbl.frame.origin.y = m_nameLabel.frame.maxY
            m_nameLabel.font = GRAPHICS.FONT_REGULAR(12)
            m_trophyLbl.font = GRAPHICS.FONT_REGULAR(12)
        }
    }
    
    func ShowOrHideTrophyLbl(toHideTrophy: Bool)
    {
        if toHideTrophy == true{
            m_trophyLbl.hidden = true
        }
        else{
            m_trophyLbl.hidden = false
        }
    }
    
    func profileImgTapGesture(tapGesture : UITapGestureRecognizer) -> () {
        imgDelegate.tapOnProfileImage(tapGesture)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
