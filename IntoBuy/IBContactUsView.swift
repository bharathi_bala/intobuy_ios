//
//  IBContactUsView.swift
//  IntoBuy
//
//  Created by Manojkumar on 05/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

public protocol ContactProtocol : NSObjectProtocol
{
    func sendFeedback(sender: UIButton)
}


class IBContactUsView: UIView {
    
    let feedBackView = UIView()
    var btnTextArray:NSArray = [ContactUs_Gmail,ContactUs_YahooMail,ContactUs_email,ContactUs_zohomail,ContactUs_Outlook]
    var btnArray:NSArray = [GRAPHICS.CONTACTS_US_GMAIL_BUTTON_IMAGE(),GRAPHICS.CONTACTS_US_YAHOO_BUTTON_IMAGE(),GRAPHICS.CONTACTS_US_MAIL_BUTTON_IMAGE(),GRAPHICS.CONTACTS_US_ZICON_BUTTON_IMAGE(),GRAPHICS.CONTACTS_US_OUTLOOK_BUTTON_IMAGE()]
    var btnDelegate:ContactProtocol?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addControls()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addControls()
    {
        feedBackView.frame = self.bounds
        feedBackView.backgroundColor = UIColor.whiteColor()
        self.addSubview(feedBackView)
        
        let titleLbl = UILabel(frame: CGRectMake(self.bounds.origin.x,self.bounds.origin.y + 5,self.frame.size.width,15))
        titleLbl.text = ContactUs_SendEmail
        titleLbl.font = GRAPHICS.FONT_REGULAR(12)
        titleLbl.textAlignment = .Center
        self.addSubview(titleLbl)
        
        var xPos = self.bounds.origin.x + 25
        var yPos = titleLbl.frame.maxY + 50
        
        
        for i in 1  ..< btnArray.count + 1
        {
            let mailImages = btnArray.objectAtIndex(i - 1) as! UIImage
            let contentSize = GRAPHICS.getImageWidthHeight(mailImages)
            let width:CGFloat = contentSize.width
            var height:CGFloat = contentSize.height
            let mailBtns = UIButton(frame: CGRectMake(xPos,yPos,width,height))
            mailBtns.setBackgroundImage(mailImages, forState: .Normal)
            mailBtns.addTarget(self, action: #selector(IBContactUsView.mailBtnAction(_:)), forControlEvents: .TouchUpInside)
            self.addSubview(mailBtns)
            
            //yPos = yPos + height + 5

            height = 15
            let mailLbl = UILabel(frame: CGRectMake(xPos - 5,mailBtns.frame.maxY + 5 ,width + 10,height))
            mailLbl.text = btnTextArray.objectAtIndex(i - 1) as? String
            mailLbl.font = GRAPHICS.FONT_REGULAR(12)
            mailLbl.textAlignment = .Center
            self.addSubview(mailLbl)
            if(i % 3 == 0)
            {
                yPos = mailLbl.frame.maxY + 40;
                xPos = self.bounds.origin.x + 25;
                
            }
            else
            {
                if GRAPHICS.Screen_Type() == IPHONE_4 || GRAPHICS.Screen_Type() == IPHONE_5
                {
                    xPos = xPos + width + 40;
                }
                else
                {
                    xPos = xPos + width + 60;
                }
            }
        }

    }
    func mailBtnAction(sender : UIButton)
    {
        btnDelegate!.sendFeedback(sender)
    }

}
