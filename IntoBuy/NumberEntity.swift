//
//  NumberEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 22/06/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class NumberEntity: NSObject {
    
    var numValue = Int()
    
    init(dict: NSDictionary) {
        numValue = dict.objectForKey("Numbers") as! Int
    }


}
