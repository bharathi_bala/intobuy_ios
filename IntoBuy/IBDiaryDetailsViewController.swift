//
//  IBDiaryDetailsViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 19/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBDiaryDetailsViewController: IBBaseViewController,ServerAPIDelegate,UIScrollViewDelegate {
    
    var m_scrollview : UIScrollView!
    var m_dateView:UIView = UIView()
    var m_datelabel:UILabel = UILabel()
    var detailArray = NSMutableArray()
    var m_deleteImgView:UIImageView!
    var leftArrowBtn = UIButton()
    var rightArrowBtn = UIButton()
    

    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        
        // self.hideHeaderViewForView(false)
         self.hideBottomView(true)
        m_titleLabel.text = MyDiary
        
        
        self.initcontrols()
        
    }
    func initcontrols()
    {
        var xpos:CGFloat = 0
        let ypos:CGFloat = 0
        var width:CGFloat = GRAPHICS.Screen_Width()
        let height:CGFloat = 40
        
        m_dateView.frame = CGRectMake(xpos, ypos, width, height)
        m_dateView.backgroundColor = lightGrayColor
        self.m_bgImageView .addSubview(m_dateView)
        
        xpos = 10;
        width = 120;
        
        //creationOflabel
        let diaryEntity = detailArray.objectAtIndex(0) as! IBDiaryEntity
        let dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.dateFromString(diaryEntity.added_date)
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd MMM"
        let dateStr = formatter.stringFromDate(date!)

        
        m_datelabel.frame = CGRectMake(xpos, ypos, width, height)
        m_datelabel.font = GRAPHICS.FONT_REGULAR(14)
        m_datelabel.text = dateStr
        m_datelabel.textColor = UIColor.whiteColor()
        m_dateView .addSubview(m_datelabel)
        
                createViewToShowDetails()
        createDeleteButton()
    }
    
    func createViewToShowDetails() {
      
        var xPos = CGFloat(0.0)
        if m_scrollview != nil
        {
           m_scrollview.removeFromSuperview()
            m_scrollview = nil
        }
        
        m_scrollview = UIScrollView(frame :CGRectMake(GRAPHICS.Screen_X(), m_dateView.frame.maxY, GRAPHICS.Screen_Width(), self.m_bgImageView.frame.size.height - m_dateView.frame.size.height - 100));
        m_scrollview.pagingEnabled = true
        m_scrollview.delegate = self
        m_bgImageView .addSubview(m_scrollview)
        
        let leftArrowImg = GRAPHICS.MYDIARY_LEFTARROW_BUTTON_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(leftArrowImg)
        var width = contentSize.width
        var height = contentSize.height
        xPos = GRAPHICS.Screen_X()
        var yPos = m_scrollview.bounds.origin.y + 90
        
        leftArrowBtn = UIButton(frame : CGRectMake(xPos,yPos,width,height))
        leftArrowBtn.setBackgroundImage(leftArrowImg, forState: .Normal)
        leftArrowBtn.addTarget(self, action: #selector(IBDiaryDetailsViewController.leftBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_bgImageView.addSubview(leftArrowBtn)
        self.m_bgImageView.bringSubviewToFront(leftArrowBtn)
        leftArrowBtn.alpha = 0.5
        
        let rightArrowImg = GRAPHICS.MYDIARY_RIGHTARROW_BUTTON_IMAGE()

        xPos = m_bgImageView.frame.size.width - width
        rightArrowBtn = UIButton(frame : CGRectMake(xPos,yPos,width,height))
        rightArrowBtn.setBackgroundImage(rightArrowImg, forState: .Normal)
        rightArrowBtn.addTarget(self, action: #selector(IBDiaryDetailsViewController.rightBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_bgImageView.addSubview(rightArrowBtn)
        self.m_bgImageView.bringSubviewToFront(rightArrowBtn)
        rightArrowBtn.alpha = 0.5
        xPos = m_scrollview.bounds.origin.x
        yPos = m_scrollview.bounds.origin.y
        width = GRAPHICS.Screen_Width()
        height = m_scrollview.frame.size.height
        
        if detailArray.count > 1
        {
            leftArrowBtn.hidden = false
            rightArrowBtn.hidden = false
        }
        else
        {
            leftArrowBtn.hidden = true
            rightArrowBtn.hidden = true
        }
        
        for i in 0  ..< detailArray.count
        {
            let detailsView = UIView(frame: CGRectMake(xPos,yPos,width,height))
            detailsView.tag = i
            m_scrollview.addSubview(detailsView)
            
            let diaryEntity = detailArray.objectAtIndex(i) as! IBDiaryEntity
            let imgView = UIImageView(frame: CGRectMake(0, detailsView.bounds.origin.y, detailsView.frame.size.width, m_scrollview.frame.size.height/2.5))
            let imageStr = diaryEntity.post_image
            imgView.load(NSURL(string: imageStr)!, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            imgView.contentMode = .ScaleToFill
            detailsView.addSubview(imgView)
            imgView.contentMode = .ScaleAspectFit
            
            let diaryLbl = UILabel(frame: CGRectMake( 10,imgView.frame.maxY,detailsView.frame.size.width - 20,m_scrollview.frame.size.height - imgView.frame.size.height))
            diaryLbl.text = diaryEntity.post_text
            diaryLbl.numberOfLines = 0
            diaryLbl.sizeToFit()
            detailsView.addSubview(diaryLbl)
            
            xPos = detailsView.frame.maxX
        }
        m_scrollview.contentSize = CGSizeMake(m_scrollview.frame.size.width * CGFloat(detailArray.count), m_scrollview.frame.size.height)
    }
    func leftBtnAction(sender : UIButton) -> () {
        let contentOffsetX = m_scrollview.contentOffset.x
        if contentOffsetX > 0
        {
            m_scrollview.setContentOffset(CGPointMake(m_scrollview.contentOffset.x - GRAPHICS.Screen_Width(), 0), animated: true)
        }
        
        
    }
    func rightBtnAction(sender : UIButton) -> () {
        let contentOffsetX = m_scrollview.contentOffset.x
        
        if contentOffsetX < m_scrollview.contentSize.width - GRAPHICS.Screen_Width() {
            m_scrollview.setContentOffset(CGPointMake(m_scrollview.contentOffset.x + GRAPHICS.Screen_Width(), 0), animated: true)
        }
        
    }
    func createDeleteButton()
    {
        let deleteImg = GRAPHICS.MYDIARY_DELETEBUTTON_IMAGE()
        var width = deleteImg.size.width/2
        var height = deleteImg.size.height/2
        let deleteBtn = UIButton(frame: CGRectMake(m_bgImageView.frame.size.width - width - 10 ,m_bgImageView.frame.size.height - height - 10, width, height))
        deleteBtn.tag = 1;
        deleteBtn.setBackgroundImage(deleteImg, forState: .Normal)
        deleteBtn.addTarget(self, action: #selector(IBMyPageViewController.deleteTheImage(_:)), forControlEvents: .TouchUpInside)
        m_bgImageView.addSubview(deleteBtn)
        
        width = width + 20
        height = height + 20
        let deleteBigBtn = UIButton(frame: CGRectMake(m_bgImageView.frame.size.width - width ,m_bgImageView.frame.size.height - height - 10, width, height))
        deleteBigBtn.tag = 1;
        deleteBigBtn.addTarget(self, action: #selector(IBMyPageViewController.deleteTheImage(_:)), forControlEvents: .TouchUpInside)
        m_bgImageView.addSubview(deleteBigBtn)


    }
    
    func deleteTheImage(sender : UIButton)
    {

        if detailArray.count > 0 {
            createViewToDelete()
                }
        else
        {
            
        }
    }
    
    func createViewToDelete()
    {
        self.m_mainView.backgroundColor = UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
            
        let bgImgView = GRAPHICS.MY_POST_TRANSPARENT_BG()
        var posX = GRAPHICS.Screen_X()
        var posY = GRAPHICS.Screen_Y()
        var width = GRAPHICS.Screen_Width()
        var height = GRAPHICS.Screen_Height()
        
        m_deleteImgView = UIImageView(frame: CGRectMake(posX, posY, width, height))
        m_deleteImgView.image = bgImgView
        m_deleteImgView.contentMode = .ScaleAspectFit
        m_deleteImgView.userInteractionEnabled = true
        self.m_mainView.addSubview(m_deleteImgView)
        
        let alertImg = GRAPHICS.POST_ALERT_BG_IMAGE()
        width = alertImg.size.width/2
        height = alertImg.size.height/2
        posX = (GRAPHICS.Screen_Width() - width)/2
        posY = (GRAPHICS.Screen_Height() - height)/2
        let alertView = UIImageView(frame: CGRectMake(posX, posY, width, height))
        alertView.image = alertImg
        alertView.userInteractionEnabled = true
        m_deleteImgView.addSubview(alertView)
        
        posX = alertView.bounds.origin.x
        posY = alertView.bounds.origin.y + 5
        height = 20
        width = alertView.frame.size.width
        let titleLbl = UILabel(frame: CGRectMake(posX, posY, width, height))
        titleLbl.text = ConfirmDeletion
        titleLbl.font = GRAPHICS.FONT_REGULAR(14)
        titleLbl.textAlignment = .Center
        titleLbl.textColor = UIColor(red: 53.0/255.0, green: 36.0/255.0, blue: 198.0/255.0, alpha: 1.0)
        alertView.addSubview(titleLbl)
        
        posY = titleLbl.frame.maxY + 20
        let confirmLbl = UILabel(frame: CGRectMake(posX, posY, width, height))
        confirmLbl.text = confirmDelete
        confirmLbl.font = GRAPHICS.FONT_REGULAR(12)
        confirmLbl.textAlignment = .Center
        alertView.addSubview(confirmLbl)
        
        height = 40
        posY = alertView.frame.size.height - height
        let dontDelBtn = UIButton(frame: CGRectMake(posX, posY, width, height))
        dontDelBtn.setTitle(dontDelete, forState: .Normal)
        dontDelBtn.setTitleColor(UIColor(red: 24.0/255.0, green: 24.0/255.0, blue: 24.0/255.0, alpha: 1.0), forState: .Normal)
        dontDelBtn.titleLabel!.font = GRAPHICS.FONT_BOLD(12)
        dontDelBtn.addTarget(self, action: #selector(IBMyPageViewController.dontDeleteBtnAction(_:)), forControlEvents: .TouchUpInside)
        addBottomBorder(dontDelBtn)
        alertView.addSubview(dontDelBtn)
        
        posY = dontDelBtn.frame.origin.y - height
        let deleteBtn = UIButton(frame: CGRectMake(posX, posY, width, height))
        deleteBtn.setTitle("Delete", forState: .Normal)
        deleteBtn.titleLabel!.font = GRAPHICS.FONT_BOLD(12)
        deleteBtn.setTitleColor(UIColor(red: 24.0/255.0, green: 24.0/255.0, blue: 24.0/255.0, alpha: 1.0), forState: .Normal)
        deleteBtn.addTarget(self, action: #selector(IBMyPageViewController.deleteBtnAction(_:)), forControlEvents: .TouchUpInside)
        addBottomBorder(deleteBtn)
        
        
        
        alertView.addSubview(deleteBtn)
    }
    func dontDeleteBtnAction(sender: UIButton)
    {
        self.m_mainView.alpha = 1.0
        m_deleteImgView .removeFromSuperview()
        m_deleteImgView = nil
    }
    func deleteBtnAction(sender: UIButton)
    {
        self.m_mainView.alpha = 1.0
        var contentOffset = m_scrollview.contentOffset.x
        //print(contentOffset)
        contentOffset = contentOffset/SCREEN_WIDTH
        let diaryEntity = self.detailArray.objectAtIndex(Int(contentOffset)) as! IBDiaryEntity
        let userIdStr = getUserIdFromUserDefaults()
        let diaryId = diaryEntity.id
        
        if detailArray.count > 0{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_diarydelete(userIdStr ,id : diaryId)
            }
            detailArray.removeObjectAtIndex(Int(contentOffset))
            m_scrollview.removeFromSuperview()
            m_scrollview = nil
            
            createViewToShowDetails()
            
            m_deleteImgView .removeFromSuperview()
            m_deleteImgView = nil
        }
        else{
            showAlertViewWithMessage("No diary to delete")
        }
        //
        
    }
    func addBottomBorder(view: UIView)
    {
        //let lineImg = GRAPHICS.POST_ALERT_BORDER_LINE()
        let bottomBorder = UIImageView(frame: CGRectMake(view.frame.origin.x + 2, view.bounds.origin.y, view.frame.size.width - 4, 1.0))
        bottomBorder.backgroundColor = UIColor.lightGrayColor()
        view.addSubview(bottomBorder)
    }
    //MARK:- scrollview delegates
    func scrollViewDidScroll(scrollView: UIScrollView) {
        leftArrowBtn.userInteractionEnabled = false
        rightArrowBtn.userInteractionEnabled = false
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        leftArrowBtn.userInteractionEnabled = true
        rightArrowBtn.userInteractionEnabled = true
    }
    func scrollViewDidEndScrollingAnimation(scrollView: UIScrollView) {
        leftArrowBtn.userInteractionEnabled = true
        rightArrowBtn.userInteractionEnabled = true
    }
    //MARK:- API delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    func API_CALLBACK_diarydelete(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"
        {
            if detailArray.count == 0
            {
                self.navigationController?.popViewControllerAnimated(true)
            }
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }


}
