//
//  IBMyPageCollectionViewCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 23/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

public protocol ProfileProtocol : NSObjectProtocol
{
    func scrollViewGesture(tapGesture: UITapGestureRecognizer)
}

class IBMyPageCollectionViewCell: UICollectionViewCell,UIScrollViewDelegate {
    
    var m_scrlView:UIScrollView!
    var pageControl:UIPageControl!
    var profileProtocol:ProfileProtocol!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        
        
    }

    func setImagesToScrlView(imagesArray :NSArray ,scrollViewTag: Int)
    {
        m_scrlView = UIScrollView(frame: CGRectMake(self.bounds.minX,self.bounds.minY,frame.size.width,frame.size.height))
        //print(self.bounds.minX,self.bounds.minY,self.frame.size.width,self.frame.size.height)
        m_scrlView.backgroundColor = UIColor.clearColor()
        m_scrlView.pagingEnabled = true
        m_scrlView.delegate = self
        self.addSubview(m_scrlView)
        m_scrlView.tag = scrollViewTag
        var xPos = m_scrlView.bounds.origin.x
        let width = m_scrlView.frame.size.width
        for i in 0  ..< imagesArray.count
        {
            let imageDetails = imagesArray.objectAtIndex(i)as! NSDictionary
            let imageUrl = imageDetails.objectForKey("image")as! String
            
            let postImgView = UIImageView(frame :
                CGRectMake(xPos,m_scrlView.bounds.origin.y,width,m_scrlView.frame.size.height))
            
            /*let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) -> Void in
                
            }*/
            
            //postImgView.sd_setImageWithURL(NSURL(string:imageUrl ), placeholderImage:GRAPHICS.DEFAULT_CART_IMAGE(), completed: block)
            postImgView.sd_setImageWithURL(NSURL(string:imageUrl), placeholderImage: GRAPHICS.DEFAULT_CART_IMAGE(), options: .ScaleDownLargeImages)
            
          // postImgView.load(imageUrl, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            postImgView.tag = scrollViewTag + i
            postImgView.userInteractionEnabled = true
            m_scrlView.addSubview(postImgView)
            xPos = xPos + width
            
            if imagesArray.count > 0
            {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IBMyPageCollectionViewCell.productImageAction(_:)))
            tapGesture.numberOfTapsRequired = 1
            postImgView.addGestureRecognizer(tapGesture);
            }
        }
        if(imagesArray.count > 1){
        self.pageControl = UIPageControl(frame: CGRectMake((self.frame.size.width - CGFloat(imagesArray.count))/2, self.frame.size.height - 20, 10, 10))
        self.pageControl.numberOfPages = imagesArray.count
        self.pageControl.currentPage = 0
      //  self.pageControl.tintColor = UIColor(red: 89.0/255.0, green: 191.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        self.pageControl.pageIndicatorTintColor = UIColor.whiteColor()
        self.pageControl.currentPageIndicatorTintColor = UIColor(red: 89.0/255.0, green: 191.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        pageControl.addTarget(self, action: #selector(IBMyPageCollectionViewCell.changePage(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.addSubview(pageControl)
        }

        m_scrlView.contentSize = CGSizeMake(m_scrlView.frame.size.width * (CGFloat(imagesArray.count)), m_scrlView.frame.size.height)
    }
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * m_scrlView.frame.size.width
        m_scrlView.setContentOffset(CGPointMake(x, 0), animated: true)
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }

    func productImageAction(tapGesture: UITapGestureRecognizer)
    {
        profileProtocol!.scrollViewGesture(tapGesture)
    }
    
    required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
    }
}
