//
//  IBMessageViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 19/07/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBMessageViewController: IBBaseViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,ServerAPIDelegate {
    
    var msgScrollView = UIScrollView()
    var msgArray = NSMutableArray()
    var discussionDict = NSDictionary()
    var m_msgTblView = UITableView()
    var msgTextView = UITextView()
    var sendBtn = UIButton()
    var keyBoardTool:UIToolbar!
    var msgstring = ""
    var nameStr = ""
    var toUserId = ""
    var chatId = ""
    var productId = ""
    var isForProductDiscussion = Bool()
    var recieverDetails = NSDictionary()
    var senderDetails = NSDictionary()
    var lightGrayViw = UIView()
    var msgSendFlag = false
    var chatTimer = NSTimer()
    var isScrollChange = false // deepika to stop scrolling
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
            chatTimer.invalidate()
        
    }
    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        self.m_titleLabel.text = nameStr
        self.hideSettingsBtn(true)
        self.hideBackBtn(false)
        self.hideBottomView(false)
        createTableViewAndButton()
        if isForProductDiscussion
        {
            createGetProductDiscussionAPI()
        }
        else
        {
            createMessageConversationAPI()
        }
    }
    
  
    //MARK:- API to get chat conversation
    func createMessageConversationAPI()
    {
        let userId = getUserIdFromUserDefaults()
        
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        
        self.chatTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: #selector(IBMessageViewController.callMessageAPI), userInfo: nil, repeats: true)
        
        serverApi.API_GetMessageConversion(userId,chatId:chatId,toId:toUserId)
    }
    func callMessageAPI() {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            let userId = getUserIdFromUserDefaults()

            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_GetMessageConversion(userId,chatId:self.chatId,toId:self.toUserId)
            

        }
    }
    func callProductDiscussionAPI() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
            let userId = getUserIdFromUserDefaults()

            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_getProductDiscussion(userId, product_id: self.productId, receiverId: self.toUserId)

        }
    }
    //MARK:-  API to get product discussion
    func createGetProductDiscussionAPI()
    {
        let userId = getUserIdFromUserDefaults()
        
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
     self.chatTimer =   NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: #selector(IBMessageViewController.callProductDiscussionAPI), userInfo: nil, repeats: true)

        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getProductDiscussion(userId, product_id: productId, receiverId: toUserId)
    }
    //MARK:- API to add msg to chat
    func createAddMessageAPI()
    {
        let userId = getUserIdFromUserDefaults()
        
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        let messageStr = convertStringToEncodedString(msgTextView.text!)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_AddChatMessage(userId,toId:toUserId,message:messageStr)
    }
    
    //MARK:- API to add discussion of product
    func createAddDiscussionMessageApi()
    {
        let userId = getUserIdFromUserDefaults()
        
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        let messageStr = convertStringToEncodedString(msgTextView.text!)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_addMsgTodiscussion(userId, productId: productId, receiverId: toUserId, message: messageStr)
    }
    
    func createTableViewAndButton()
    {
        var xPos = self.m_bgImageView.bounds.origin.x
        var yPos = self.m_bgImageView.bounds.origin.y
        var width = self.m_bgImageView.frame.size.width 
        var height = self.m_bgImageView.frame.size.height - 30
        
        msgScrollView.frame = CGRectMake(xPos,yPos,width,height)
        msgScrollView.alwaysBounceVertical = false // deepika to stop scrolling
        msgScrollView.alwaysBounceHorizontal = false // deepika to stop scrolling
        self.m_bgImageView.addSubview(msgScrollView)

        m_msgTblView.frame = msgScrollView.bounds
        m_msgTblView.delegate = self
        m_msgTblView.dataSource = self
        m_msgTblView.separatorStyle = UITableViewCellSeparatorStyle.None
        m_msgTblView.scrollEnabled = false
        m_msgTblView.alwaysBounceVertical = false // deepika to stop scrolling
        m_msgTblView.alwaysBounceHorizontal = false // deepika to stop scrolling
        msgScrollView.addSubview(m_msgTblView)
        
        yPos = m_msgTblView.frame.maxY
        width = self.m_bgImageView.frame.size.width/1.2
        height = 30
        
       
        lightGrayViw = UIView(frame:CGRectMake(0,yPos-1,GRAPHICS.Screen_Width(),1))
        lightGrayViw.backgroundColor = applightGraycolor
         self.m_bgImageView.addSubview(lightGrayViw)
         self.m_bgImageView.addSubview(lightGrayViw)
        
        msgTextView = UITextView(frame: CGRectMake(xPos,yPos,width,height))
        msgTextView.delegate = self
        msgTextView.text = Contacts_EnterMsgText
        msgTextView.font = GRAPHICS.FONT_REGULAR(14)
        msgTextView.backgroundColor = UIColor.clearColor()
        self.m_bgImageView.addSubview(msgTextView)
        
        xPos = msgTextView.frame.maxX
        width = self.m_bgImageView.frame.size.width/6
        let sendButton = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        sendButton.setTitle("Send", forState: .Normal)
        sendButton.setTitleColor(headerColor, forState: .Normal)
        sendButton.addTarget(self, action: #selector(IBMessageViewController.sendButtonAction(_:)), forControlEvents: .TouchUpInside)
        self.m_bgImageView.addSubview(sendButton)
    }
    
    func sendButtonAction(sender : UIButton)
    {
       msgTextView.resignFirstResponder()
    
        if msgTextView.text != "" && msgTextView.text != Contacts_EnterMsgText
        {
            if isForProductDiscussion
            {
                createAddDiscussionMessageApi()
            }
            else
            {
                createAddMessageAPI()
            }
           
           
        }
        else
        {
            showAlertViewWithMessage("Please Enter Message")
        }
        msgTextView.frame.origin.y  = self.m_bgImageView.frame.size.height - 30
        lightGrayViw.frame.origin.y = msgTextView.frame.origin.y - 1
        msgTextView.text = Contacts_EnterMsgText
        
  }
    ////MARK: - tableview delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgArray.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var str =  String()
        var Height =  CGFloat()
        var STATIC_WIDTH = CGFloat()
        let font = GRAPHICS.FONT_REGULAR(12)!
        let textAttributes = [NSFontAttributeName: font]
        var textRect =  CGRect()
        if isForProductDiscussion
        {
            let msgDict = msgArray.objectAtIndex(indexPath.row) as! NSDictionary
            str =  decodeEncodedStringToNormalString(msgDict.objectForKey("message") as! String)
        }
        else
        {
            let msgEntity = msgArray.objectAtIndex(indexPath.row) as! IBMessageEntity
            str = decodeEncodedStringToNormalString(msgEntity.message)//msgArray.objectAtIndex(indexPath.row) as! String
        }
        STATIC_WIDTH = 10
        let wid = GRAPHICS.Screen_Width() - GRAPHICS.SEARCH_PROFILE_MASK_IMAGE().size.width * 2 - 20
        textRect = str.boundingRectWithSize(CGSizeMake(wid, 0), options: .UsesLineFragmentOrigin, attributes: textAttributes, context: nil)
        Height = heightForView(str, font: GRAPHICS.FONT_REGULAR(12)!, width:textRect.width + 10 )
        if  Height < 15.0
        {
            Height = 75
        }
        else
        {
            Height = Height + 60
        }
        return Height;
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cellIdentifier = "cellIdentifier"
        var cell:IBChatCustomCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBChatCustomCell
        if (cell == nil) {
            cell = IBChatCustomCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        if isForProductDiscussion
        {
            if discussionDict["discussion"] != nil
            {
                var str =  String()
                var  Height =  CGFloat()
                var  STATIC_WIDTH =  CGFloat()
                let font = GRAPHICS.FONT_REGULAR(12)!
                let textAttributes = [NSFontAttributeName: font]
                var textRect =  CGRect()
                let msgDict = msgArray.objectAtIndex(indexPath.row) as! NSDictionary
                str = decodeEncodedStringToNormalString(msgDict.objectForKey("message") as! String)//msgArray.objectAtIndex(indexPath.row) as! String
                STATIC_WIDTH = 10
                let wid = GRAPHICS.Screen_Width() - GRAPHICS.SEARCH_PROFILE_MASK_IMAGE().size.width * 2 - 20
                textRect = str.boundingRectWithSize(CGSizeMake(wid, 0), options: .UsesLineFragmentOrigin, attributes: textAttributes, context: nil)
                Height = heightForView(str, font: GRAPHICS.FONT_REGULAR(12)!, width:textRect.width + 10 )
                if  Height < 15.0
                {
                    Height =  75
                }
                else
                {
                    Height =  Height + 60
                }
                
                cell?.selectionStyle = .None
                cell?.m_Message_Label.text = str//msgArray.objectAtIndex(indexPath.row) as? String
                let senderId = msgDict.objectForKey("senderId") as! String
                if senderId == getUserIdFromUserDefaults()
                {
                    cell?.setControls("Mine",height: Height,width: textRect.width+STATIC_WIDTH,msgText:str , profilePic : senderDetails.objectForKey("avatar") as! String,date:msgDict.objectForKey("addedDate") as! String )
                }
                else
                {
                    cell?.setControls("SomeOne",height: Height,width: textRect.width+STATIC_WIDTH,msgText:str , profilePic : recieverDetails.objectForKey("avatar") as! String,date:msgDict.objectForKey("addedDate") as! String )
                }
            }
        }
        else{
            let msgEntity = msgArray.objectAtIndex(indexPath.row) as! IBMessageEntity
            
            var str =  String()
            var  Height =  CGFloat()
            var  STATIC_WIDTH =  CGFloat()
            let font = GRAPHICS.FONT_REGULAR(12)!
            let textAttributes = [NSFontAttributeName: font]
            var textRect =  CGRect()
            str =  decodeEncodedStringToNormalString(msgEntity.message)//msgArray.objectAtIndex(indexPath.row) as! String
            STATIC_WIDTH = 10
            let wid = GRAPHICS.Screen_Width() - GRAPHICS.SEARCH_PROFILE_MASK_IMAGE().size.width * 2 - 20
            textRect = str.boundingRectWithSize(CGSizeMake(wid, 0), options: .UsesLineFragmentOrigin, attributes: textAttributes, context: nil)
            Height = heightForView(str, font: GRAPHICS.FONT_REGULAR(12)!, width:textRect.width + 10 )
            if  Height < 15.0
            {
                Height =  75
            }
            else
            {
                Height =  Height + 60
            }
            
            cell?.selectionStyle = .None
            
            //cell?.m_Message_Label.text = decodeEncodedStringToNormalString(msgEntity.message)//msgArray.objectAtIndex(indexPath.row) as? String
            if msgEntity.msgFrom == getUserIdFromUserDefaults()
            {
                cell?.setControls("Mine",height: Height,width: textRect.width+STATIC_WIDTH,msgText:decodeEncodedStringToNormalString(msgEntity.message) , profilePic : msgEntity.FromAvatar,date:msgEntity.addedDate)
            }
            else
            {
                cell?.setControls("SomeOne",height: Height,width: textRect.width+STATIC_WIDTH,msgText:decodeEncodedStringToNormalString(msgEntity.message) , profilePic : msgEntity.FromAvatar,date:msgEntity.addedDate)
            }
        }
        m_msgTblView.frame.size.height = m_msgTblView.contentSize.height;//+m_cellHeight+20;
        
        m_msgTblView.frame.size.height = max(m_msgTblView.contentSize.height + (cell?.frame.size.height)!, m_msgTblView.contentSize.height);
        
        //Chnage here
        if msgArray.count == indexPath.row + 1 && !isScrollChange
        {
            changeScrollContent()
            isScrollChange = true // deepika to stop scrolling
        }
//        msgScrollView.contentSize = CGSizeMake(msgScrollView.frame.size.width,m_msgTblView.contentSize.height);
//        msgScrollView.contentOffset = CGPointMake(msgScrollView.frame.origin.x, m_msgTblView.frame.size.height - msgScrollView.frame.size.height);
    
     
        return cell!
        
    }
    func changeScrollContent()
    {
        msgScrollView.contentSize = CGSizeMake(0,m_msgTblView.contentSize.height);
        
        let pos = msgScrollView.contentSize.height > self.view.frame.height ? msgScrollView.contentSize.height - msgScrollView.frame.height : 0
        msgScrollView.contentOffset = CGPointMake(0,pos);
    }
    //MARK: - textview delegate
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        
        if msgTextView.text == Contacts_EnterMsgText {
            msgTextView.text = ""
        }
        
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations:
            {
            self.m_mainView.frame.origin.y =  -256 - 40
            }, completion: nil)
        textView.inputAccessoryView = self.createKeyboardToolBar()
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.m_mainView.frame.origin.y =  0
            self.msgTextView.frame.origin.y = self.m_msgTblView.frame.maxY
            self.lightGrayViw.frame.origin.y = self.m_msgTblView.frame.maxY-1
            self.msgTextView.frame.size.height = 30
            }, completion: nil)
        
        msgstring = msgTextView.text!;
    }
    func textViewDidChange(textView: UITextView) {
        let height = msgTextView.frame.size.height
        let rawLineNumber:CGFloat = (textView.contentSize.height - textView.textContainerInset.top - textView.textContainerInset.bottom) / textView.font!.lineHeight;
        let finalLineNumber:Int = Int(rawLineNumber)
        
        if finalLineNumber <=  3
        {
            if finalLineNumber == 2{
                msgTextView.frame.size.height = 40
            }
            else if finalLineNumber == 3
            {
                msgTextView.frame.size.height = 60
            }
            else
            {
                msgTextView.frame.size.height = 30
            }
            msgTextView.frame.origin.y = (self.m_bgImageView.frame.size.height) - msgTextView.frame.size.height
            lightGrayViw.frame.origin.y =  msgTextView.frame.origin.y - 1
            self.m_bgImageView.bringSubviewToFront(msgTextView)
            
//            let bgview = m_commentTextView.superview
//            let bgview = m_commentTextView.superview
//            m_commentTextView.frame.origin.y = (bgview?.frame.size.height)! - m_commentTextView.frame.size.height
//            
//            bgview!.bringSubviewToFront(m_commentTextView)
            
            //            m_commentTableView.frame.origin.y = m_commentTableView.frame.origin.y - m_commentTextView.frame.size.height
        }
        
        //m_commentTextView.frame.size.height =
    }

    func createKeyboardToolBar() -> UIToolbar
    {
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        keyBoardTool = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        keyBoardTool!.barStyle = UIBarStyle.BlackOpaque
        keyBoardTool!.translucent = true
        keyBoardTool!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        keyBoardTool!.sizeToFit()
        
        let cancel_Btn = UIButton ()
        cancel_Btn.frame = CGRectMake(10, 0, 70, 50)
        cancel_Btn.backgroundColor =  UIColor.clearColor();
        cancel_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        cancel_Btn.setTitle("Cancel", forState: UIControlState.Normal)
        cancel_Btn.addTarget(self, action: #selector(IBMessageViewController.cancelAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(cancel_Btn)
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBContactsViewController.doneAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(done_Btn)
        keyBoardTool!.userInteractionEnabled = true
        return keyBoardTool
        
    }
    func cancelAction()
    {
        msgTextView.text = Contacts_EnterMsgText
        msgTextView.resignFirstResponder()
        msgTextView.frame.origin.y  = self.m_bgImageView.frame.size.height - 30
        lightGrayViw.frame.origin.y = msgTextView.frame.origin.y - 1
    }
    func doneAction()
    {
        msgTextView.resignFirstResponder()
        
        if msgTextView.text == "" {
            msgTextView.text = Contacts_EnterMsgText
        }
        msgTextView.frame.origin.y  = self.m_bgImageView.frame.size.height - 30
        lightGrayViw.frame.origin.y = msgTextView.frame.origin.y - 1
    }
    //MARK:- dynamic height method
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    //MARK:- API delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    func API_CALLBACK_GetMessageConversion(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        msgArray.removeAllObjects()
        if errorCode == "1"
        {
           let chatArray = resultDict.objectForKey("result") as! NSArray
           for  i in 0 ..< chatArray.count
           {
                let chatDict = chatArray.objectAtIndex(i) as! NSDictionary
                let messageEntity = IBMessageEntity(dict: NSDictionary(dictionary: chatDict))
                msgArray.addObject(messageEntity)
            }
            
            let sortedArray = msgArray.sortedArrayUsingComparator {
                (obj1, obj2) -> NSComparisonResult in
                
                let p1 = obj1 as! IBMessageEntity
                let p2 = obj2 as! IBMessageEntity
                let result = p1.addedDate.compare(p2.addedDate as String)
                return result
            }
            let count = msgArray.count
            
            
            msgArray.removeAllObjects()
            msgArray.addObjectsFromArray(sortedArray)
            
            if count < msgArray.count {
                
                isScrollChange = false
            }
            
            m_msgTblView.reloadData()
        }
        else
        {
            
        }
    }
    func API_CALLBACK_AddChatMessage(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.createMessageConversationAPI()
            })
        }
        else{
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    func API_CALLBACK_getProductDiscussion(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"{
            
            discussionDict = resultDict.objectForKey("result") as! NSDictionary
            let  messageArray = (discussionDict.objectForKey("discussion")!.mutableCopy() as! NSMutableArray)
            if(msgArray.count != 0)
            {
                msgArray.removeAllObjects()
            }
            
            //msgArray = discussionDict.objectForKey("discussion") as! NSMutableArray
            let sortedArray = messageArray.sortedArrayUsingComparator {
                (obj1, obj2) -> NSComparisonResult in
                
                let p1 = obj1 as! NSDictionary
                let p2 = obj2 as! NSDictionary
                let result = p1.objectForKey("addedDate")!.compare(p2.objectForKey("addedDate") as! String)
                return result
            }
            msgArray.addObjectsFromArray(sortedArray)

            recieverDetails = discussionDict.objectForKey("receiverDetails") as! NSDictionary
            senderDetails = discussionDict.objectForKey("senderDetails") as! NSDictionary
            m_msgTblView.reloadData()
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    func API_CALLBACK_addMsgTodiscussion(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.createGetProductDiscussionAPI()
            })
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    
}
