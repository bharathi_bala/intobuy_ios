//
//  IBConfirmOrderViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 04/07/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBConfirmOrderViewController: IBBaseViewController,ServerAPIDelegate {
    
    var orderDict:NSDictionary!
    var confirmScrlView = UIScrollView()
    var productIdStr = String()
    var sellerIdStr = String()
    var orderIdArray = NSMutableArray()
    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        self.m_titleLabel.text = "Check out"
        self.hideBackBtn(false)
        
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_PlaceOrder(productIdStr, userId: getUserIdFromUserDefaults(), sellerId: sellerIdStr)

    }
    func createControls()
    {
        confirmScrlView.frame = self.m_bgImageView.bounds
        self.m_bgImageView.addSubview(confirmScrlView)
        
        let titleLbl = UILabel()
        titleLbl.frame = CGRectMake(0,0, GRAPHICS.Screen_Width(),20)
        titleLbl.text = "Confirm order"
        titleLbl.textAlignment = .Center
        titleLbl.font = GRAPHICS.FONT_BOLD(14)
        confirmScrlView.addSubview(titleLbl)
        
        
        let orderSummaryLbl = UILabel(frame : CGRectMake(self.m_bgImageView.bounds.origin.x + 10 , titleLbl.frame.maxY + 10,self.m_bgImageView.frame.size.width - 20,20))
        orderSummaryLbl.text = "Order summary"
        orderSummaryLbl.font = GRAPHICS.FONT_BOLD(16)
        confirmScrlView.addSubview(orderSummaryLbl)
        
        let orderArray = orderDict.objectForKey("orders") as! NSArray
        
        var yPos = orderSummaryLbl.frame.maxY + 10
        for  i in 0 ..< orderArray.count
        {
            let productDict = orderArray.objectAtIndex(i) as! NSDictionary
            let productNameLbl = UILabel(frame : CGRectMake(self.m_bgImageView.bounds.origin.x + 10 , yPos,self.m_bgImageView.frame.size.width/2.5,20))
            productNameLbl.text = (productDict.objectForKey("productname") as? String)!
            productNameLbl.font = GRAPHICS.FONT_REGULAR(16)
            confirmScrlView.addSubview(productNameLbl)
            
            let productPriceLbl = UILabel(frame : CGRectMake(productNameLbl.frame.maxX + 10 , yPos,self.m_bgImageView.frame.size.width/2.5,20))
            productPriceLbl.text = (productDict.objectForKey("price") as? String)!
           // productPriceLbl.textColor = grayColor
            productPriceLbl.font = GRAPHICS.FONT_REGULAR(16)
            confirmScrlView.addSubview(productPriceLbl)
            
            orderIdArray.addObject(productDict.objectForKey("orderId") as! String)
            
            yPos = yPos + 20
        }
        
        yPos = yPos + 30
        let totalLbl = UILabel(frame : CGRectMake(self.m_bgImageView.bounds.origin.x + 10 , yPos,self.m_bgImageView.frame.size.width/2.5,20))
        totalLbl.text = "Total"
        totalLbl.font = GRAPHICS.FONT_REGULAR(16)
        confirmScrlView.addSubview(totalLbl)
        
        let totalPriceLbl = UILabel(frame : CGRectMake(totalLbl.frame.maxX + 10 , yPos,self.m_bgImageView.frame.size.width - 20,20))
        totalPriceLbl.text = String(format : "$ %@",(orderDict.objectForKey("totalprice") as? String)!)
        totalPriceLbl.font = GRAPHICS.FONT_REGULAR(16)
        confirmScrlView.addSubview(totalPriceLbl)

        let shippingDict = orderArray.objectAtIndex(0) as! NSDictionary
        let shippingLbl = UILabel(frame : CGRectMake(self.m_bgImageView.bounds.origin.x + 10 , totalPriceLbl.frame.maxY + 20,self.m_bgImageView.frame.size.width - 20,20))
        shippingLbl.text = "Shipping Address"
        shippingLbl.font = GRAPHICS.FONT_BOLD(16)
        confirmScrlView.addSubview(shippingLbl)
        
        let userNameNameLbl = UILabel(frame : CGRectMake(self.m_bgImageView.bounds.origin.x + 10 , shippingLbl.frame.maxY + 10,self.m_bgImageView.frame.size.width/2.5,20))
        userNameNameLbl.text = (shippingDict.objectForKey("name") as? String)!
        userNameNameLbl.font = GRAPHICS.FONT_REGULAR(16)
        confirmScrlView.addSubview(userNameNameLbl)

        let address = String(format : "%@, %@, %@, %@, %@\n%@",(shippingDict.objectForKey("address1") as! String),(shippingDict.objectForKey("address2") as! String),(shippingDict.objectForKey("city") as! String),(shippingDict.objectForKey("state") as! String),(shippingDict.objectForKey("countryname") as! String),(shippingDict.objectForKey("contact") as! String))
        let addressLbl = UILabel(frame : CGRectMake(self.m_bgImageView.bounds.origin.x + 10 , userNameNameLbl.frame.maxY + 10,self.m_bgImageView.frame.size.width - 20,20))
        addressLbl.text = address
        addressLbl.font = GRAPHICS.FONT_REGULAR(16)
        confirmScrlView.addSubview(addressLbl)
        addressLbl.numberOfLines = 0
        addressLbl.sizeToFit()

        
        let saveImg = GRAPHICS.SETTINGS_BILLING_BTN_IMAGE()
        var width = (saveImg.size.width)/2
        var height = (saveImg.size.height)/2
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            //height = 30
            width = (saveImg.size.width)/1.8
            height = (saveImg.size.height)/1.8
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (saveImg.size.width)/1.5
            height = (saveImg.size.height)/1.5
        }
        
        var xPos = self.m_bgImageView.frame.size.width - width - 20
        yPos = addressLbl.frame.maxY + 10
        let changeBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        changeBtn.setBackgroundImage(saveImg, forState: UIControlState.Normal)
        changeBtn.setTitle("Change", forState: UIControlState.Normal)
        changeBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        changeBtn .addTarget(self, action: #selector(IBConfirmOrderViewController.changeBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        confirmScrlView.addSubview(changeBtn)

        xPos = (self.m_bgImageView.frame.size.width - width)/2
        yPos = changeBtn.frame.maxY + 50
        let proceedToPaymentBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        proceedToPaymentBtn.setBackgroundImage(saveImg, forState: UIControlState.Normal)
        proceedToPaymentBtn.setTitle("Pay", forState: UIControlState.Normal)
        proceedToPaymentBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        proceedToPaymentBtn .addTarget(self, action: #selector(IBConfirmOrderViewController.payBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        confirmScrlView.addSubview(proceedToPaymentBtn)
        
        confirmScrlView.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), proceedToPaymentBtn.frame.maxY + 10)
        
        
    }
    func changeBtnAction()
    {
        for  vc:UIViewController in (self.navigationController?.viewControllers)!
        {
            if vc .isKindOfClass(IBShippingDetailsViewController)
            {
                let homePage : IBShippingDetailsViewController = vc as! IBShippingDetailsViewController
                homePage.isFromProducts = true
            }
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    func payBtnAction()
    {
        let orderIdStr = orderIdArray.componentsJoinedByString(",")
        let billingDetails = IBBillingDetailsViewController()
        billingDetails.orderIdStr = orderIdStr
        billingDetails.isFromOrder = true
        self.navigationController?.pushViewController(billingDetails, animated: true)
    }
    //MARK:- Api delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    func API_CALLBACK_PlaceOrder(resultDict: NSDictionary)
    {
        //print(resultDict)
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
//            let resultArray = resultDict.objectForKey("result") as! NSArray
//            //print(resultArray.count)
//            let placeOrderDict = resultArray.objectAtIndex(0) as! NSDictionary
//            orderDict = placeOrderDict
            orderDict = resultDict.objectForKey("result") as! NSDictionary
            createControls()
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }

}
