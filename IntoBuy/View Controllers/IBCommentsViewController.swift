//
//  IBCommentsViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 16/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBCommentsViewController: IBBaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,ServerAPIDelegate,UITextViewDelegate {

    var m_commentTableView:UITableView!
    var m_commentTextView:UITextView!
    var m_wallIdStr = ""
    var m_commentStr = ""
    var m_commentArray = NSMutableArray()
    var toolBar:UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.hideSettingsBtn(false)
        self.m_bgImageView.image = GRAPHICS.LOGIN_BACKGROUND_IMAGE()
        self.m_titleLabel.text = WriteComment
        self.createControls()
        self.createServiceToGetComment()
        // Do any additional setup after loading the view.
    }
    func createControls()
    {
        var xPos = self.m_bgImageView.bounds.origin.x + 5
        var yPos = self.m_bgImageView.bounds.origin.y + 5
        var width = self.m_bgImageView.frame.size.width - 10
        var height = self.m_bgImageView.frame.size.height - 10
        
        let bgView = UIView(frame: CGRectMake(xPos,yPos,width,height))
        bgView.backgroundColor = UIColor.whiteColor()
        self.m_bgImageView.addSubview(bgView)
        
        xPos = bgView.bounds.origin.x
        yPos = bgView.bounds.origin.y
        width = bgView.frame.size.width
        height = bgView.frame.size.height - 30
        m_commentTableView = UITableView(frame: CGRectMake(xPos,yPos,width,height))
        m_commentTableView.delegate = self
        m_commentTableView.dataSource = self
        m_commentTableView.separatorStyle = UITableViewCellSeparatorStyle.None
        bgView.addSubview(m_commentTableView)
        
        let btnImg = GRAPHICS.COMMENT_BTN_IMG()
        yPos = m_commentTableView.frame.maxY
        width = bgView.frame.size.width - btnImg.size.width/2
        height = 30
        m_commentTextView = UITextView(frame: CGRectMake(xPos,yPos,width,height))
        m_commentTextView.delegate = self
        m_commentTextView.text = WriteComment
        m_commentTextView.font = GRAPHICS.FONT_REGULAR(14)
       // setLeftViewToTheTextField(m_commentTextField)
       // m_commentTextView.addTarget(self, action: "textFieldDidChange:", forControlEvents: .ValueChanged)
        bgView.addSubview(m_commentTextView)
        
        
        xPos = m_commentTextView.frame.maxX
        width = btnImg.size.width/2
        let sendButton = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        sendButton.setBackgroundImage(btnImg, forState: .Normal)
        sendButton.addTarget(self, action: #selector(IBCommentsViewController.sendButtonAction(_:)), forControlEvents: .TouchUpInside)
        bgView.addSubview(sendButton)
    }
    func createServiceToGetComment()
    {
       // let userDetailUD = NSUserDefaults()
        let userIdStr = getUserIdFromUserDefaults()
       
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        

        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getWallComments(m_wallIdStr, userId: userIdStr)
    }
    func sendButtonAction(sender: UIButton)
    {
        self.m_mainView.frame.origin.y = 0
        if (m_commentTextView.text == WriteComment || m_commentTextView.text == "")
        {
            self.view .endEditing(true)
            m_commentTextView.text = WriteComment
            showAlertViewWithMessage("Please Enter Comment")
        }
        else{
            let userDetailUD = NSUserDefaults()
            let userIdStr = userDetailUD.objectForKey("user_id") as! String;
            
            SwiftLoader.show(animated: true)
            SwiftLoader.show(title:"Loading...", animated:true)
            
            let encodeStr = convertStringToEncodedString(m_commentStr)
            m_commentTextView.resignFirstResponder()
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_addWallComment(m_wallIdStr, userId: userIdStr, comment: encodeStr)
            m_commentTextView.text = WriteComment;
            
        }
    }
    //MARK: - textview delegate
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        m_commentTextView.text = ""
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.m_mainView.frame.origin.y =  -256 - 40
            }, completion: nil)
        textView.inputAccessoryView = self.CreateToolBar()
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        m_commentStr = m_commentTextView.text!;
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.m_mainView.frame.origin.y =  0
            self.m_commentTextView.frame.size.height = 30
//            self.m_commentTableView.frame.origin.y = self.m_bgImageView.bounds.origin.y
            self.m_commentTextView.frame.origin.y = self.m_commentTableView.frame.maxY
            }, completion: nil)

    }
//    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
//        
//        return true
//    }
    func textViewDidChange(textView: UITextView) {
        m_commentStr = m_commentTextView.text!;
        let height = m_commentTextView.frame.size.height
        let rawLineNumber:CGFloat = (textView.contentSize.height - textView.textContainerInset.top - textView.textContainerInset.bottom) / textView.font!.lineHeight;
        let finalLineNumber:Int = Int(rawLineNumber)

        if finalLineNumber <=  3
        {
            if finalLineNumber == 2{
                m_commentTextView.frame.size.height = 40
            }
            else if finalLineNumber == 3
            {
                m_commentTextView.frame.size.height = 50
            }
            else
            {
                m_commentTextView.frame.size.height = 30
            }
            //            let bgview = m_commentTextView.superview
            let bgview = m_commentTextView.superview
            m_commentTextView.frame.origin.y = (bgview?.frame.size.height)! - m_commentTextView.frame.size.height
            
            bgview!.bringSubviewToFront(m_commentTextView)

//            m_commentTableView.frame.origin.y = m_commentTableView.frame.origin.y - m_commentTextView.frame.size.height
        }

                //m_commentTextView.frame.size.height =
    }
    //MARK: - tableview delegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return m_commentArray.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let dataDict = m_commentArray.objectAtIndex(indexPath.row)as! NSDictionary
        let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
        let width = profileMaskImg.size.width/2
        let imgHeight = profileMaskImg.size.height/2
        let decodestr = decodeEncodedStringToNormalString((dataDict.objectForKey("comment")as? String!)!)
        
        let stringVal : NSString = " "
        let fontVal = GRAPHICS.FONT_REGULAR(12)!
        let fontAttributes = [NSFontAttributeName: fontVal]
        let myString: NSString = stringVal as NSString
        
        let size = myString.sizeWithAttributes(fontAttributes)
        
        let counttoaddSpace = (width - 10) / CGFloat(size.width)
        
        var spacetoadd = String()
        for _ in 0..<Int(counttoaddSpace)
        {
            spacetoadd = spacetoadd + " "
        }
        
        let height = heightForView(spacetoadd + decodestr, font: GRAPHICS.FONT_REGULAR(12)!, width: GRAPHICS.Screen_Width() - width - 20)  + 24 + 20;
        return height ;
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell:IBCommentTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBCommentTableViewCell
        
        if (cell == nil)
        {
            cell = IBCommentTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = .None
        let dataDict = m_commentArray.objectAtIndex(indexPath.row)as! NSDictionary
     
        let decodestr = decodeEncodedStringToNormalString((dataDict.objectForKey("comment")as? String!)!)
        cell?.m_userNameLbl.text = dataDict.objectForKey("username")as? String!
        
        //let dateStr = dataDict.objectForKey("added_date") as? String!
        
        
        
        let addedDate = dataDict.objectForKey("added_date")as? String
        let addedDateStr = convertUtcToLocalFormat(addedDate!)
        let dateFormat:NSDateFormatter = NSDateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormat.timeZone = NSTimeZone.localTimeZone()
        let startDate = dateFormat.dateFromString(addedDateStr)
        let intervalString = startDate!.relativeTimeToString()
       // //print("\(addedDateStr), \(intervalString)")
        
        cell?.m_commentLbl.frame.origin.y = (cell?.m_userNameLbl.frame.maxY)!
//        cell?.m_commentLbl.frame.size.height = self.heightForView(decodestr, font: GRAPHICS.FONT_REGULAR(12)!, width: GRAPHICS.Screen_Width() - cell!.m_profileImgView.frame.size.width - 20);
        
        let stringVal : NSString = " "
        let fontVal = GRAPHICS.FONT_REGULAR(12)!
        let fontAttributes = [NSFontAttributeName: fontVal]
        let myString: NSString = stringVal as NSString
        
        let size = myString.sizeWithAttributes(fontAttributes)
        
        
        
        let counttoaddSpace = ((cell?.m_userNameLbl.frame.minX)! - (cell?.m_profileImgView.frame.minX)!) / CGFloat(size.width)
        
        var spacetoadd = String()
        for _ in 0..<Int(counttoaddSpace)
        {
            spacetoadd = spacetoadd + " "
        }
        
        cell?.m_commentLbl.frame.size.height = self.heightForView(spacetoadd + decodestr, font: GRAPHICS.FONT_REGULAR(12)!, width: GRAPHICS.Screen_Width() - cell!.m_profileImgView.frame.size.width - 20);
        cell?.m_commentLbl.text = spacetoadd + decodestr
        cell?.m_commentLbl.numberOfLines = 0
        cell?.m_commentLbl.lineBreakMode = .ByWordWrapping
        
        
       // cell?.frame.size.height = (cell?.m_commentLbl.frame.maxY)! + 20
        cell!.dividerImgView.frame.origin.y =  (cell?.m_commentLbl.frame.maxY)! + 7
        
        //let uTCDateStr =  getDatetimeFormat(dateStr!)
        
        cell?.m_dateLbl.text = intervalString
        //cell?.m_dateLbl.sizeToFit()
        cell?.m_profileImgView.load((dataDict.objectForKey("userProfile")as? String!)!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
        
        return cell!
    }
   
    
    //MARK: - API delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        GRAPHICS.showAlert(errorMessage)
    }

    func API_CALLBACK_getWallComments(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"{
            
            m_commentArray.removeAllObjects()
            m_commentArray.addObjectsFromArray(resultDict.objectForKey("result")!.mutableCopy() as! [AnyObject])
            
            let sortedArray = m_commentArray.sortedArrayUsingComparator {
                (obj1, obj2) -> NSComparisonResult in
                
                let p1 = obj1 as! NSDictionary
                let p2 = obj2 as! NSDictionary
                let result = p2.objectForKey("added_date")!.compare(p1.objectForKey("added_date") as! String)
                return result
            }
            m_commentArray.removeAllObjects()
            m_commentArray.addObjectsFromArray(sortedArray)
            m_commentTableView.reloadData()
            
        }
        else{
            
        }
    }
    func API_CALLBACK_addWallComment(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"{
            self.createServiceToGetComment()
        }
        else{
            
        }
    }
    func setLeftViewToTheTextField(textField: UITextField)
    {
        let leftPlaceHolderView = UIView(frame: CGRectMake(0, 0, 5,textField.frame.size.height))
        leftPlaceHolderView.backgroundColor = UIColor.clearColor()
        textField.leftView = leftPlaceHolderView
        textField.leftViewMode = UITextFieldViewMode.Always
    }
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    func addBottomBorder(view: UIView)
    {
        let lineImg = GRAPHICS.SEARCH_DIVIDER_IMAGE()
        let bottomBorder = UIImageView(frame: CGRectMake(view.frame.origin.x + 10, view.frame.maxY-1.0, GRAPHICS.Screen_Width() - 20, 1.0))
        bottomBorder.image = lineImg
        view.addSubview(bottomBorder)
    }

    // create tool bar
    func CreateToolBar() -> UIToolbar{
        
        if(toolBar != nil){
            toolBar.removeFromSuperview()
            toolBar = nil
        }
        
        toolBar = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, view.frame.size.width, 50.0))
        toolBar!.barStyle = UIBarStyle.Default
        toolBar!.translucent = true
        toolBar!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar!.sizeToFit()
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(IBCommentsViewController.doneAction))
        doneButton.tintColor = UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(IBCommentsViewController.cancelAction))
        cancelButton.tintColor = UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0)
        
        toolBar!.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar!.userInteractionEnabled = true
        //self.m_bgImageView .addSubview(toolBar)
        return toolBar
    }
    func doneAction(){
        self.view.endEditing(true)
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.m_mainView.frame.origin.y = 0
            }, completion: nil)
        if m_commentTextView.text == ""
        {
            m_commentTextView.text = WriteComment
        }
    }
    func cancelAction(){
        m_commentTextView.text = WriteComment
        self.view.endEditing(true)
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.m_mainView.frame.origin.y = 0
            }, completion: nil)

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
