//
//  IBBillingDetailsViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 27/01/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBBillingDetailsViewController: IBBaseViewController,UITextFieldDelegate,ServerAPIDelegate,UIPickerViewDataSource,UIPickerViewDelegate,CountryProtocol,PayPalPaymentDelegate,UIAlertViewDelegate {
    
    var m_billingScrollView = UIScrollView()
    var m_payPalBtn:UIButton!
    var m_cardNoTf:UITextField!
    var m_cardNameTF:UITextField!
    var m_expiryMonthTf:UITextField!
    var m_expiryYearTf:UITextField!
    var m_securityTf:UITextField!
    var m_address1TF:UITextField!
    var m_address2TF:UITextField!
    var m_cityTF:UITextField!
    var m_stateTF:UITextField!
    var m_zipTF:UITextField!
    var m_countryTF:UITextField!
    var keyBoardTool:UIToolbar!
    var toolBar:UIToolbar!
    var m_countryArray = NSMutableArray()
    var m_pickerView:UIPickerView!
    var m_pickerArray = NSMutableArray()
    var textFieldIndex = NSInteger()
    var btnIndex = NSInteger()
    var m_expString = String()
    var countryId = String()
    
    var isFromOrder = Bool()
    
    var orderIdStr = String()
    
    //PaypalEnvironMent no network
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnectWithEnvironment(newEnvironment)
            }
        }
    }
    
    #if HAS_CARDIO
    var acceptCreditCards: Bool = true {
    didSet {
    payPalConfig.acceptCreditCards = acceptCreditCards
    }
    }
    #else
    var acceptCreditCards: Bool = false {
        didSet {
            payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }
    #endif
    
    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        PayPalMobile.preconnectWithEnvironment(PayPalEnvironmentSandbox)
    }
    
    override func viewDidLoad()
    {
        bottomVal = 5
        toShowActivityBtn = true
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.m_settingsButton.hidden = true
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_titleLabel.text = Settings_Title
        
        m_billingScrollView.frame = self.m_bgImageView.bounds
        self.m_bgImageView .addSubview(m_billingScrollView)
        createControlsToEnterCardNumber()
        
//        payPalConfig.acceptCreditCards = acceptCreditCards;
//        payPalConfig.merchantName = "Siva Ganesh Inc."
//        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.sivaganesh.com/privacy.html")
//        payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.sivaganesh.com/useragreement.html")
//        payPalConfig.languageOrLocale = NSLocale.preferredLanguages()[0]
//        payPalConfig.payPalShippingAddressOption = .Provided
        
        payPalConfig.acceptCreditCards = true
        //PayPalMobile.preconnectWithEnvironment(environment)
        
        // call api to get billing details
        SwiftLoader.show(title: Loading, animated: true)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getBillingdetails(getUserIdFromUserDefaults())
        
        
    }
    
    func createControlsToEnterCardNumber()
    {
        var xPos = GRAPHICS.Screen_X() + 15
        var yPos = m_billingScrollView.bounds.origin.y + 20
        var width = GRAPHICS.Screen_Width() - 30
        var height:CGFloat = 15
//        let lblPadding:CGFloat = 5
//        let textPadding:CGFloat = 10
       // let textBoxImg = GRAPHICS.SETTINGS_BIG_TEXTBOX_IMAGE()
        
        let editTitleLbl = UILabel(frame:CGRectMake(xPos,yPos,width,height))
        editTitleLbl.text = "Edit Billing details"
        editTitleLbl.font = GRAPHICS.FONT_BOLD(14)
        m_billingScrollView .addSubview(editTitleLbl)
        
        
        
        let saveImg = GRAPHICS.SETTINGS_BILLING_BTN_IMAGE()
        width = (saveImg.size.width)/2
        height = (saveImg.size.height)/2
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            //height = 30
            width = (saveImg.size.width)/1.8
            height = (saveImg.size.height)/1.8
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (saveImg.size.width)/1.5
            height = (saveImg.size.height)/1.5
        }
        
        
        yPos = editTitleLbl.frame.maxY + 5
        m_payPalBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        m_payPalBtn.setBackgroundImage(saveImg, forState: UIControlState.Normal)
        m_payPalBtn.setTitle(usePaypal, forState: UIControlState.Normal)
        m_payPalBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        m_payPalBtn .addTarget(self, action: #selector(IBBillingDetailsViewController.paypalBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_billingScrollView.addSubview(m_payPalBtn)
        
        width = GRAPHICS.Screen_Width()/2 - 18
        height = 15
        yPos = m_payPalBtn.frame.maxY + 30
        let cardNoLbl = UILabel(frame:CGRectMake(xPos,yPos,width,height))
        cardNoLbl.text = String(format: "%@:",cardNumber)
        cardNoLbl.font = GRAPHICS.FONT_REGULAR(12)
       // m_billingScrollView .addSubview(cardNoLbl)
        
        let cardImg = GRAPHICS.SETTINGS_CARD_TEXTBOX_IMAGE()
        height = 25
        yPos = cardNoLbl.frame.maxY
        
        m_cardNoTf = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_cardNoTf.delegate = self
        m_cardNoTf.background = cardImg
        m_cardNoTf.keyboardType = .NumberPad
        m_cardNoTf.inputAccessoryView = self.createKeyboardToolBar()
        m_cardNoTf.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_cardNoTf)
       // m_billingScrollView.addSubview(m_cardNoTf)
        
        xPos = cardNoLbl.frame.maxX + 5
        height = 15
        yPos = cardNoLbl.frame.origin.y
        let cardNameLbl = UILabel(frame:CGRectMake(xPos,yPos,width,height))
        cardNameLbl.text = String(format: "%@:",cardName)
        cardNameLbl.font = GRAPHICS.FONT_REGULAR(12)
      //  m_billingScrollView .addSubview(cardNameLbl)
        
        yPos = cardNameLbl.frame.maxY
        height = 25
        m_cardNameTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_cardNoTf.delegate = self
        m_cardNameTF.background = cardImg
        m_cardNameTF.inputAccessoryView = self.createKeyboardToolBar()
        m_cardNameTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_cardNameTF)
      //  m_billingScrollView.addSubview(m_cardNameTF)

        
        xPos = cardNoLbl.frame.origin.x
        yPos = m_cardNoTf.frame.maxY + 15
        height = 15
        let expiryLbl = UILabel(frame:CGRectMake(xPos,yPos,width,height))
        expiryLbl.text = String(format: "%@: (MM/YY)",expiryText)
        expiryLbl.font = GRAPHICS.FONT_REGULAR(12)
     //   m_billingScrollView .addSubview(expiryLbl)
        
        let expTextImg = GRAPHICS.SETTINGS_SMALL_TEXTBOX_IMAGE()
        height = 25
        width = width/3
        yPos = expiryLbl.frame.maxY + 5
        m_expiryMonthTf = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_expiryMonthTf.delegate = self
        m_expiryMonthTf.background = expTextImg
        m_expiryMonthTf.font = GRAPHICS.FONT_REGULAR(14)
        createDropdownButtonForTextField(m_expiryMonthTf, tag: 1)
     //   m_billingScrollView.addSubview(m_expiryMonthTf)
        
        xPos = m_expiryMonthTf.frame.maxX + 10
        m_expiryYearTf = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_expiryYearTf.delegate = self
        m_expiryYearTf.background = expTextImg
        m_expiryYearTf.font = GRAPHICS.FONT_REGULAR(14)
        createDropdownButtonForTextField(m_expiryYearTf, tag: 2)
      //  m_billingScrollView.addSubview(m_expiryYearTf)
        
        
        yPos = expiryLbl.frame.origin.y
        width = expiryLbl.frame.size.width
        xPos = expiryLbl.frame.maxX + 10
        height = 15
        let securityLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        securityLbl.text = String(format: "%@ (CVV/CSC)",securityNumber)
        securityLbl.font = GRAPHICS.FONT_REGULAR(11)
     //   m_billingScrollView .addSubview(securityLbl)
        
        width = width/3
        yPos = securityLbl.frame.maxY + 5
        xPos = securityLbl.frame.maxX - width - 5
        height = 25
        
        m_securityTf = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_securityTf.delegate = self
        m_securityTf.background = expTextImg
        m_securityTf.keyboardType = .NumberPad
        m_securityTf.inputAccessoryView = self.createKeyboardToolBar()
        m_securityTf.font = GRAPHICS.FONT_REGULAR(13)
        setLeftViewToTheTextField(m_securityTf)
      //  m_billingScrollView.addSubview(m_securityTf)

        let checkBoxNormal = GRAPHICS.SETTINGS_CHECKBOX_IMAGE_NORMAL()
        let checkBoxSelect = GRAPHICS.SETTINGS_CHECKBOX_IMAGE_SELECTED()
        width = checkBoxNormal.size.width/2
        height = checkBoxNormal.size.height/2
        yPos = m_payPalBtn.frame.maxY + 30
        //yPos = m_securityTf.frame.maxY + 10
        xPos = m_securityTf.frame.maxX - width
        let checkBoxBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        checkBoxBtn.setBackgroundImage(checkBoxNormal, forState: .Normal)
        checkBoxBtn.setBackgroundImage(checkBoxSelect, forState: .Selected)
        checkBoxBtn.addTarget(self, action: #selector(IBBillingDetailsViewController.selectCheckBoxBtn(_:)), forControlEvents: .TouchUpInside)
        m_billingScrollView.addSubview(checkBoxBtn)
        
        width = securityLbl.frame.size.width
        xPos = checkBoxBtn.frame.origin.x - width - 5
        let sameLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        sameLbl.text = sameAsShippingAddr
        sameLbl.font = GRAPHICS.FONT_REGULAR(12);
        m_billingScrollView.addSubview(sameLbl);
        sameLbl.sizeToFit()

        self .createViewToEnterAddressDetails(sameLbl.frame.maxY + 10)
        
    }
    func createViewToEnterAddressDetails(posY : CGFloat)
    {
        var xPos = m_cardNoTf.frame.origin.x
        var yPos = posY//m_expiryMonthTf.frame.maxY + 25
        var width = m_securityTf.frame.size.width * 2
        var height:CGFloat = 15
        let lblPadding:CGFloat = 5
        let textPadding:CGFloat = 10
        let textBoxImg = GRAPHICS.SETTINGS_BIG_TEXTBOX_IMAGE()

        
        
        let titleLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        titleLbl.text = billingAdress
        titleLbl.font = GRAPHICS.FONT_REGULAR(12);
        m_billingScrollView.addSubview(titleLbl);
        titleLbl.sizeToFit()
        
        
        yPos = titleLbl.frame.maxY + textPadding
        height = 15
        width = GRAPHICS.Screen_Width() - 30
        let address1Lbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        address1Lbl.text = String(format: "%@:",addressLine1)
        address1Lbl.font = GRAPHICS.FONT_REGULAR(12)
        m_billingScrollView .addSubview(address1Lbl)
        
        yPos = address1Lbl.frame.maxY + lblPadding
        height = 20
        m_address1TF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_address1TF.delegate = self
        m_address1TF.returnKeyType = UIReturnKeyType.Next
        m_address1TF.background = textBoxImg
        m_address1TF.inputAccessoryView = self.createKeyboardToolBar()
        m_address1TF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_address1TF)
        m_billingScrollView .addSubview(m_address1TF)
        
        yPos = m_address1TF.frame.maxY + textPadding
        height = 15
        let address2Lbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        address2Lbl.text = String(format: "%@:",addressLine2)
        address2Lbl.font = GRAPHICS.FONT_REGULAR(12)
        m_billingScrollView .addSubview(address2Lbl)
        
        yPos = address2Lbl.frame.maxY + lblPadding
        height = 20
        m_address2TF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_address2TF.delegate = self
        m_address2TF.returnKeyType = UIReturnKeyType.Next
        m_address2TF.keyboardType = UIKeyboardType.EmailAddress
        m_address2TF.background = textBoxImg
        m_address2TF.inputAccessoryView = self.createKeyboardToolBar()
        m_address2TF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_address2TF)
        m_billingScrollView .addSubview(m_address2TF)
        
        // city field
        yPos = m_address2TF.frame.maxY + textPadding
        height = 15
        let cityLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        cityLbl.text = String(format: "%@:",city)
        cityLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_billingScrollView .addSubview(cityLbl)
        
        yPos = cityLbl.frame.maxY + lblPadding
        height = 20
        m_cityTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_cityTF.delegate = self
        m_cityTF.returnKeyType = UIReturnKeyType.Next
        m_cityTF.background = textBoxImg
        m_cityTF.inputAccessoryView = self.createKeyboardToolBar()
        m_cityTF.keyboardType = UIKeyboardType.EmailAddress
        m_cityTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_cityTF)
        m_billingScrollView .addSubview(m_cityTF)
        
        //state field
        yPos = m_cityTF.frame.maxY + textPadding
        height = 15
        let stateLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        stateLbl.text = String(format: "%@/%@/%@:",StateText,province,Region)
        stateLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_billingScrollView .addSubview(stateLbl)
        
        yPos = stateLbl.frame.maxY + lblPadding
        height = 20
        m_stateTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_stateTF.delegate = self
        m_stateTF.returnKeyType = UIReturnKeyType.Next
        m_stateTF.background = textBoxImg
        m_stateTF.inputAccessoryView = self.createKeyboardToolBar()
        m_stateTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_stateTF)
        m_billingScrollView .addSubview(m_stateTF)
        
        // zip field
        yPos = m_stateTF.frame.maxY + textPadding
        height = 15
        let zipLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        zipLbl.text = String(format: "%@/%@:",Zip,postalCode)
        zipLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_billingScrollView .addSubview(zipLbl)
        
        yPos = zipLbl.frame.maxY + lblPadding
        height = 20
        m_zipTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_zipTF.delegate = self
        m_zipTF.returnKeyType = UIReturnKeyType.Next
        m_zipTF.secureTextEntry = true
        m_zipTF.background = textBoxImg
        m_zipTF.inputAccessoryView = self.createKeyboardToolBar()
        setLeftViewToTheTextField(m_zipTF)
        m_zipTF.font = GRAPHICS.FONT_REGULAR(14)
        m_billingScrollView .addSubview(m_zipTF)
        
        // Country field
        yPos = m_zipTF.frame.maxY + textPadding
        height = 15
        let countryLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        countryLbl.text = String(format: "%@:",Country)
        countryLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_billingScrollView .addSubview(countryLbl)
        
        yPos = countryLbl.frame.maxY + lblPadding
        height = 20
        m_countryTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_countryTF.delegate = self
        m_countryTF.returnKeyType = UIReturnKeyType.Next
        m_countryTF.background = textBoxImg
        m_countryTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_countryTF)
        m_billingScrollView .addSubview(m_countryTF)

        
        let saveImg = GRAPHICS.SETTINGS_BILLING_BTN_IMAGE()
        width = (saveImg.size.width)/2
        height = (saveImg.size.height)/2
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            //height = 30
            width = (saveImg.size.width)/1.8
            height = (saveImg.size.height)/1.8
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (saveImg.size.width)/1.5
            height = (saveImg.size.height)/1.5
        }
        
        xPos = (m_billingScrollView.frame.size.width - width)/2
        yPos = m_countryTF.frame.maxY + 20
        let saveBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        saveBtn.setBackgroundImage(saveImg, forState: UIControlState.Normal)
        saveBtn.setTitle("Save", forState: UIControlState.Normal)
        saveBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        saveBtn .addTarget(self, action: #selector(IBBillingDetailsViewController.saveBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_billingScrollView.addSubview(saveBtn)
        
        m_billingScrollView.contentSize = CGSizeMake(m_billingScrollView.frame.size.width , saveBtn.frame.maxY + 40);

        
    }
    func saveBtnAction()
    {
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components([.Day , .Month , .Year], fromDate: date)
        let month = components.month
        let year =  components.year
        
//        if(month < Int(m_expiryMonthTf.text!)! &&  year == Int(m_expiryYearTf.text!)!)
//        {
//            GRAPHICS.showAlert("Dont select expired month and year")
//        }
        
        if self.validation()
        {
            if isFromOrder{
            }
            else{
                SwiftLoader.show(title: Loading, animated: true)
                let userIdStr = getUserIdFromUserDefaults()
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_AddBillingDetails(userIdStr, address1: m_address1TF.text!, address2: m_address2TF.text!, state: m_stateTF.text!, country: countryId, zip: m_zipTF.text!, city: m_cityTF.text!,contact:"0000000")
            }
        }
        
    }
    func selectCheckBoxBtn(sender: UIButton)
    {
        sender.selected = !sender.selected
        if sender.selected
        {
            let shippingDict = getShippingDetailsFromUD()
            
            if shippingDict.objectForKey("address1") as? String != nil
            {
            m_address1TF.text = shippingDict.objectForKey("address1") as? String
            m_address2TF.text = shippingDict.objectForKey("address2") as? String
            m_cityTF.text = shippingDict.objectForKey("city") as? String
            m_stateTF.text = shippingDict.objectForKey("state") as? String
            m_countryTF.text = shippingDict.objectForKey("country") as? String
            m_zipTF.text = shippingDict.objectForKey("zip") as? String
            countryId = (shippingDict.objectForKey("countryId") as? String)!
            }
            else
            {
                sender.selected = false
                showAlertViewWithMessage("Shipping details not available")
            }
        }
        else
        {
            m_address1TF.text = ""
            m_address2TF.text = ""
            m_cityTF.text = ""
            m_stateTF.text = ""
            m_countryTF.text = ""
            m_zipTF.text = ""
            countryId = ""
        }
        
    }
    
    // method for tool bar
    func createKeyboardToolBar() -> UIToolbar
    {
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        keyBoardTool = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        keyBoardTool!.barStyle = UIBarStyle.BlackOpaque
        keyBoardTool!.translucent = true
        keyBoardTool!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        keyBoardTool!.sizeToFit()
        
        
        let previousImage = GRAPHICS.PICKER_LEFT_ARROW_BTN()
        let nextImage = GRAPHICS.PICKER_RIGHT_ARROW_BTN()
        
        let prev_Btn = UIButton ()
        prev_Btn.frame = CGRectMake(10, 8, 40, 30)
        prev_Btn.backgroundColor =  UIColor.clearColor();
        prev_Btn.setBackgroundImage(previousImage, forState: .Normal)
        prev_Btn.addTarget(self, action: #selector(IBBillingDetailsViewController.previousAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(prev_Btn)
        
        let next_Btn = UIButton ()
        next_Btn.frame = CGRectMake(50, 8, 40, 30)
        next_Btn.backgroundColor =  UIColor.clearColor();
        next_Btn.setBackgroundImage(nextImage, forState: .Normal)
        next_Btn.addTarget(self, action: #selector(IBBillingDetailsViewController.nextAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(next_Btn)
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBBillingDetailsViewController.doneBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(done_Btn)
        
        
        keyBoardTool!.userInteractionEnabled = true
        return keyBoardTool
        
    }
    func doneBtnAction()
    {
        self.view .endEditing(true)
    }
    func nextAction()
    {
        if(m_cardNoTf.isFirstResponder() == true)
        {
            m_cardNoTf.resignFirstResponder()
            m_cardNameTF.resignFirstResponder()
        }
        else if(m_cardNameTF.isFirstResponder() == true)
        {
            m_cardNameTF.resignFirstResponder()
        }
        else if(m_securityTf.isFirstResponder() == true)
        {
            m_securityTf.resignFirstResponder()
        }
        else if(m_address1TF.isFirstResponder() == true)
        {
            m_address1TF.resignFirstResponder()
            m_address2TF.becomeFirstResponder()
        }
        else if(m_address2TF.isFirstResponder() == true)
        {
            m_address2TF.resignFirstResponder()
            m_cityTF.resignFirstResponder()
        }
        else if(m_cityTF.isFirstResponder() == true)
        {
            m_cityTF.resignFirstResponder()
            m_stateTF.becomeFirstResponder()
        }
        else if(m_stateTF.isFirstResponder() == true)
        {
            m_stateTF.resignFirstResponder()
            m_zipTF.becomeFirstResponder()
        }
        else if(m_zipTF.isFirstResponder() == true)
        {
            m_zipTF.resignFirstResponder()
        }

    }
    func previousAction()
    {
        if m_zipTF.isFirstResponder() == true
        {
            m_zipTF.resignFirstResponder()
            m_stateTF.becomeFirstResponder()
        }
        else if m_stateTF.isFirstResponder() == true
        {
            m_stateTF.resignFirstResponder()
            m_cityTF.becomeFirstResponder()
        }
        else if m_cityTF.isFirstResponder() == true
        {
            m_cityTF.resignFirstResponder()
            m_address2TF.becomeFirstResponder()
        }
        else if m_address2TF.isFirstResponder() == true
        {
            m_address2TF.resignFirstResponder()
            m_address1TF.becomeFirstResponder()
        }
        else if m_address1TF.isFirstResponder() == true
        {
            m_address1TF.resignFirstResponder()
        }
        else if m_securityTf.isFirstResponder() == true
        {
            m_securityTf.resignFirstResponder()
        }
        else if m_cardNameTF.isFirstResponder() == true
        {
            m_cardNameTF.resignFirstResponder()
            m_cardNoTf.becomeFirstResponder()
        }
        else if m_cardNoTf.isFirstResponder() == true
        {
            m_cardNoTf.resignFirstResponder()
        }
    }
    // MARK: - country service api
    func callServiceForCountry()
    {
        self.view .endEditing(true)
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getActiveCountry("36")
    }
    
    func navigateToCountryScreen()
    {
        let countryVC = IBCountryViewController()
        countryVC.mDelegate = self
        countryVC.isFromRegistration = true
        countryVC.m_countryArray = m_countryArray
        self.navigationController!.pushViewController(countryVC, animated: true)
        
    }

    
    // set padding for text field
    func setLeftViewToTheTextField(textField: UITextField)
    {
        let leftPlaceHolderView = UIView(frame: CGRectMake(0, 0, 10,textField.frame.size.height))
        leftPlaceHolderView.backgroundColor = UIColor.clearColor()
        textField.leftView = leftPlaceHolderView
        textField.leftViewMode = UITextFieldViewMode.Always
    }
    
    func createDropdownButtonForTextField(textField: UITextField, tag:Int)
    {
        let btnImg = GRAPHICS.SETTINGS_DROP_DOWN_IMAGE()
        let width = btnImg.size.width/2
        let height = btnImg.size.height/2
        let xPos = textField.frame.size.width - width - 2
        let yPos = (textField.frame.size.height - height)/2
        
        let dropDownBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        dropDownBtn.setBackgroundImage(btnImg, forState: .Normal)
        dropDownBtn.tag = tag
        dropDownBtn.addTarget(self, action: #selector(IBBillingDetailsViewController.openPicker(_:)), forControlEvents: .TouchUpInside)
        textField.addSubview(dropDownBtn)
        
    }
    func openPicker(sender: UIButton)
    {
        if(sender.tag == 1)
        {
            btnIndex = sender.tag
            createPicker(m_expiryMonthTf)
            CreateToolBar()
        }
        else{
            btnIndex = sender.tag
            createPicker(m_expiryYearTf)
            CreateToolBar()
        }
    }
    func createPicker(textField : UITextField)
    {
        m_pickerArray.removeAllObjects()
        if textField == m_expiryMonthTf
        {
           
            m_pickerArray = ["1","2","3","4","5","6","7","8","9","10","11","12"]
            m_expString = m_pickerArray.objectAtIndex(0) as! String
        }
        else
        {
            let date = NSDate()
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Day , .Month , .Year], fromDate: date)
            
            var year =  components.year
            //m_pickerArray .addObject(year)
            for i in 0  ..< 20
            {
                if(i > 0){
                year = year + 1
                }
                let yearStr = String(year)
                m_pickerArray .addObject(yearStr)
            }
            m_expString = m_pickerArray.objectAtIndex(0) as! String
        }
        let Height:CGFloat = 200.0
        let YPos:CGFloat = GRAPHICS.Screen_Height() - Height
        let Width = GRAPHICS.Screen_Width()
        let XPos = GRAPHICS.Screen_X()
        if(m_pickerView != nil)
        {
            m_pickerView .removeFromSuperview()
            m_pickerView = nil
        }
        m_pickerView = UIPickerView(frame:CGRectMake(XPos, YPos, Width, Height))
        m_pickerView.delegate = self
        m_pickerView.dataSource = self
        m_pickerView.showsSelectionIndicator = true
        m_pickerView.backgroundColor = UIColor.whiteColor()
        self.m_bgImageView.addSubview(m_pickerView)

        
    }
    func CreateToolBar(){
        
        if(toolBar != nil){
            toolBar.removeFromSuperview()
            toolBar = nil
        }
        
        toolBar = UIToolbar(frame:CGRectMake(0, (GRAPHICS.Screen_Height() - 180) - 50, view.frame.size.width, 50.0))
        toolBar!.barStyle = UIBarStyle.BlackTranslucent
        toolBar!.translucent = true
        toolBar!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar!.sizeToFit()
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(IBBillingDetailsViewController.doneAction))
        doneButton.tintColor = UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(IBBillingDetailsViewController.cancelAction))
        cancelButton.tintColor = UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0)
        
        toolBar!.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar!.userInteractionEnabled = true
        self.m_bgImageView .addSubview(toolBar)
    }
    func doneAction()
    {
        if(m_pickerView != nil)
        {
            m_pickerView .removeFromSuperview()
            m_pickerView = nil
        }
        if(toolBar != nil){
            toolBar.removeFromSuperview()
            toolBar = nil
        }
        if textFieldIndex == 1 || btnIndex == 1
        {
            m_expiryMonthTf.text = m_expString
        }
        else{
            m_expiryYearTf.text = m_expString
        }
    }
    func cancelAction()
    {
        if(m_pickerView != nil)
        {
            m_pickerView .removeFromSuperview()
            m_pickerView = nil
        }
        if(toolBar != nil){
            toolBar.removeFromSuperview()
            toolBar = nil
        }
        if textFieldIndex == 1 || btnIndex == 1
        {
            m_expiryMonthTf.text = ""
        }
        else{
            m_expiryYearTf.text = ""
        }
    }

    // paypal button action
    func paypalBtnAction()
    {
        if self.validation()
        {
        self.m_backButton.userInteractionEnabled = false
        self.m_bigBackButton.userInteractionEnabled = false
        self .paymentProcessForBuyNowButtonClickEvent()
        }
        else
        {
            showAlertViewWithMessage("Please enter billing details")
        }
    }
    //MARK:- textfield delgates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == m_cardNoTf)
        {
            m_cardNoTf.resignFirstResponder()
            m_cardNameTF.resignFirstResponder()
        }
        else if(textField == m_cardNameTF)
        {
            m_cardNameTF.resignFirstResponder()
        }
        else if(textField == m_securityTf)
        {
            m_securityTf.resignFirstResponder()
        }
        else if(textField == m_address1TF)
        {
            m_address1TF.resignFirstResponder()
            m_address2TF.becomeFirstResponder()
        }
        else if(textField == m_address2TF)
        {
            m_address2TF.resignFirstResponder()
            m_cityTF.resignFirstResponder()
        }
        else if(textField == m_cityTF)
        {
            m_cityTF.resignFirstResponder()
            m_stateTF.becomeFirstResponder()
        }
        else if(textField == m_stateTF)
        {
            m_stateTF.resignFirstResponder()
            m_zipTF.becomeFirstResponder()
        }
        else if(textField == m_zipTF)
        {
            m_zipTF.resignFirstResponder()
        }

        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if(textField == m_countryTF)
        {
            if(m_countryArray.count > 0){
                self.navigateToCountryScreen()
            }
            else{
                self.callServiceForCountry()
            }
            
            return false
        }
        if(textField.frame.origin.y >  self.m_bgImageView.frame.size.height - 200){
            var contentOffset:CGFloat!
            if(GRAPHICS.Screen_Type() == IPHONE_4){
                contentOffset = 120.0
            }
            else if(GRAPHICS.Screen_Type() == IPHONE_5){
                contentOffset = 150.0;
            }
            m_billingScrollView.setContentOffset(CGPointMake(0, (m_billingScrollView.frame.origin.y + contentOffset)), animated: true)
        }
        if(textField == m_expiryMonthTf)
        {
            textFieldIndex = 1;
            self.createPicker(m_expiryMonthTf)
            self.CreateToolBar()
            return false
        }
        else if(textField == m_expiryYearTf)
        {
            textFieldIndex = 2;
            self.createPicker(m_expiryYearTf)
            self.CreateToolBar()
            return false
        }
        
        return true
    }

    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if (textField == m_cardNoTf) {
            if (string.characters.count == 0) {
                var cardNumber = String(format: "%@",m_cardNoTf.text!) as NSString
                cardNumber = cardNumber.substringToIndex(cardNumber.length - 1)
                
            }else{
                let cardNumber = String(format: "%@",m_cardNoTf.text!) as NSString
                if (cardNumber.length < 19) {
                    if (cardNumber.length == 4 || cardNumber.length == 9 || cardNumber.length == 14) {
                        let updatedCardNumber = String(format: "%@ ",m_cardNoTf.text!)
                        m_cardNoTf.text = updatedCardNumber
                        return true;
                    }
                    else{
                        return true;
                    }
                }
                else{
                    return false;
                }
            }
            
        }
        else if(textField == m_securityTf)
        {
            if(textField.text?.characters.count > 2)
            {
                return false
            }
        }
        return true
    }
    //MARK:- API delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        GRAPHICS.showAlert(errorMessage);
    }
    func API_CALLBACK_getActiveCountry(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = (resultDict .objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
            if let countryArry = resultDict.objectForKey("result") as? NSArray
            {
               if countryArry.count > 0
               {
                let arry  = resultDict.objectForKey("result") as! NSArray
                m_countryArray.addObjectsFromArray(arry as [AnyObject])
                
               }
            
            }
         
            self.navigateToCountryScreen()
        }
        else{
            GRAPHICS.showAlert((resultDict .objectForKey("error_code")as? String)!)
            
        }
        //self.m_countryTblView .reloadData()
        
    }
    func API_CALLBACK_getBillingdetails(resultDict: NSDictionary) {
        SwiftLoader.hide()
        let errorCode = (resultDict .objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
            let resultArray = resultDict.valueForKey("result") as! NSArray
            if resultArray.count > 0{
            let resultValueDict = resultArray.objectAtIndex(0) as! NSDictionary
            let billingEntity = IBBillingDetailsEntity(dict: resultValueDict)
            m_cardNameTF.text = billingEntity.card_owner_name
            m_cardNoTf.text = billingEntity.card_number
            m_expiryYearTf.text = billingEntity.expiry_year
            m_expiryMonthTf.text = billingEntity.expiry_month
            m_securityTf.text = billingEntity.cvc
            m_address1TF.text = billingEntity.address
            m_address2TF.text = billingEntity.address2
            m_cityTF.text = billingEntity.city
            m_stateTF.text = billingEntity.state
            m_zipTF.text = billingEntity.zip
            m_countryTF.text = billingEntity.country_name
            countryId = billingEntity.country
            }
            else{
                showAlertViewWithMessage("Details not available")
            }
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    func API_CALLBACK_AddBillingDetails(resultDict: NSDictionary) {
        SwiftLoader.hide()
        if resultDict.objectForKey("error_code")as! String == "1"
        {
            showAlertViewWithMessage(resultDict.objectForKey("result")as! String)
            
            if isFromOrder{
                self.m_backButton.userInteractionEnabled = true
                self.m_bigBackButton.userInteractionEnabled = true
                for  vc:UIViewController in (self.navigationController?.viewControllers)!
                {
                    if vc .isKindOfClass(IBHomePageVcViewController)
                    {
                        let homeVC : IBHomePageVcViewController = vc as!  IBHomePageVcViewController
                        self.navigationController?.popToViewController(homeVC, animated: true)
                        return
                    }
                }
            }
            else{
                for  vc in (self.navigationController?.viewControllers)!
                {
                    if vc .isKindOfClass(IBSettingsViewController)
                    {
                        let settingsVc : IBSettingsViewController = vc as!  IBSettingsViewController
                        self.navigationController?.popToViewController(settingsVc, animated: true)
                    }
                }
            }
        }
        else{
            showAlertViewWithMessage(resultDict.objectForKey("msg")as! String)
        }
    }
    
    func API_CALLBACK_successOrder(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        if resultDict.objectForKey("error_code")as! String == "1"
        {
            showAlertViewWithMessageForTransaction("transaction success")
            
        }
        else{
            showAlertViewWithMessage(resultDict.objectForKey("msg")as! String)
        }

    }
    
   //MARK:- picker view delegates
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //print(m_pickerArray.count)
        return m_pickerArray.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //print(m_pickerArray.objectAtIndex(row) as? String)
        return m_pickerArray.objectAtIndex(row) as? String
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        if textFieldIndex == 1
//        {
//            m_expiryMonthTf.text = m_pickerArray.objectAtIndex(row) as? String
//        }
//        else{
//            m_expiryYearTf.text = m_pickerArray.objectAtIndex(row) as? String
//        }
        m_expString = (m_pickerArray.objectAtIndex(row) as? String)!
    }
    // validation for textfield
    func validation()->Bool{
        
        
        if m_address1TF.text?.isEmpty == true{
            GRAPHICS.showAlert("Please enter address1")
            return false
        }
        else if m_address2TF?.text?.isEmpty == true{
            GRAPHICS.showAlert("Please enter address2")
            return false
        }
        else if m_cityTF?.text?.isEmpty == true{
            GRAPHICS.showAlert("Please enter city")
            return false
        }
        else if m_stateTF?.text?.isEmpty == true{
            GRAPHICS.showAlert("Please re-enter state")
            return false
        }
        else if m_zipTF?.text?.isEmpty == true{
            GRAPHICS.showAlert("Please enter zip code")
            return false
        }
        else if m_countryTF?.text?.isEmpty == true{
            GRAPHICS.showAlert("Please enter country")
            return false
        }

        
        
        return true
    }
    
    func paymentProcessForBuyNowButtonClickEvent()
    {
        let productDetailsArray = getProductDetailsFromUD()
        // Remove our last completed payment, just for demo purposes.
        resultText = ""
        
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        
        var itemArray = NSMutableArray()
        for i in 0 ..< productDetailsArray.count
        {
            let productDetailsDict = productDetailsArray.objectAtIndex(i) as! NSDictionary
            let productNameStr = productDetailsDict.objectForKey("productname") as! String
            let productQuantity = Int(productDetailsDict.objectForKey("quantity") as! String)
            let item = PayPalItem(name: productNameStr, withQuantity: UInt(productQuantity!), withPrice: NSDecimalNumber(string: (productDetailsDict.objectForKey("price") as! String)), withCurrency: "USD", withSku: "Hip-0037")
            itemArray.addObject(item)
            
        }
        
//        let productNameStr = productDetailsDict.objectForKey("productname") as! String
//        let productQuantity = Int(productDetailsDict.objectForKey("quantity") as! String)
//        let productPrice = Int(Float(productDetailsDict.objectForKey("price") as! String)!) * productQuantity!

        
//        let item2 = PayPalItem(name: "MacBook Pro", withQuantity: 1, withPrice: NSDecimalNumber(string: "240.00"), withCurrency: "USD", withSku: "Hip-00066")
//        let item3 = PayPalItem(name: "SamsungLCD", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
        
        let items = itemArray as [AnyObject]
        let subtotal = PayPalItem.totalPriceForItems(items/*[item1]*/)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "10.00")
        let tax = NSDecimalNumber(string: "2.50")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.decimalNumberByAdding(shipping).decimalNumberByAdding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "IntoBuy Products", intent: .Sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            presentViewController(paymentViewController, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            //print("Payment not processalbe: \(payment)")
        }
    }
    // PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(paymentViewController: PayPalPaymentViewController!) {
        //print("PayPal Payment Cancelled")
        self.m_backButton.userInteractionEnabled = true
        self.m_bigBackButton.userInteractionEnabled = true
        resultText = ""
        
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func payPalPaymentViewController(paymentViewController: PayPalPaymentViewController!, didCompletePayment completedPayment: PayPalPayment!) {
        //print("PayPal Payment Success !")
        paymentViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            // send completed confirmaion to your server
            //print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            self.resultText = completedPayment!.description
            let confirmDict = completedPayment.confirmation as NSDictionary
            let responseDict = confirmDict.objectForKey("response") as! NSDictionary
            let transactionId = responseDict.objectForKey("id") as! String
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_successOrder(getUserIdFromUserDefaults(), orderId: self.orderIdStr, transactionId: transactionId)
            })
            //showAlertViewWithMessage("Your transaction has sucess")
        })
    }
    public func showAlertViewWithMessageForTransaction(message:NSString)->()
    {
        let alertview = UIAlertView(title: IntoBuy, message:message as NSString as String, delegate: self, cancelButtonTitle: Ok)
        alertview.show()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        alertView.removeFromSuperview()
        SwiftLoader.show(title: Loading, animated: true)
        let userIdStr = getUserIdFromUserDefaults()
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_AddBillingDetails(userIdStr, address1: self.m_address1TF.text!, address2: self.m_address2TF.text!, state: self.m_stateTF.text!, country: self.countryId, zip: self.m_zipTF.text!, city: self.m_cityTF.text!,contact:"0000000")
    }
    
    func sendArrayToPreviousVC(myArray: NSArray) {
        m_countryTF.text = myArray.objectAtIndex(0) as? String
        countryId = myArray.objectAtIndex(1) as! String
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
