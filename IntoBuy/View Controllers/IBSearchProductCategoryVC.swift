//
//  IBSearchProductCategoryVC.swift
//  IntoBuy
//
//  Created by Manojkumar on 01/02/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBSearchProductCategoryVC: IBBaseViewController,UITableViewDataSource,UITableViewDelegate,ServerAPIDelegate {
    
    var m_categoryArray = NSMutableArray()
    var subcategoryArray = NSMutableArray()
    var m_categoryTblView = UITableView()
    var m_selectionIndex:NSInteger!
    var categoryStr = "All categories"
    var isFromSell = Bool()
    var toGetSubcategory = Bool()
    var textFieldTag = NSInteger()
    var categoryIdStr = String()
    var subcategoryIdStr = String()
    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        self.hideSettingsBtn(true)

        self.hideHeaderViewForView(true)
        self.hideBottomView(true)
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_bgImageView.frame.size.height = GRAPHICS.Screen_Height()
        if isFromSell
        {
            if toGetSubcategory
            {
                callSubCategorysAPI()
            }
            else
            {
                callServiceForCategory()
            }
        }
        else{
        callServiceForCategory()
        }
        self.createControls()
        m_selectionIndex = 0;
    }
    //MARK: - service for category
    func callServiceForCategory()
    {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)

        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getActiveCategories()
    }
    //MARK: - service for category
    func callSubCategorysAPI()
    {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        
       // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            serverApi.API_getActiveSubCategories(categoryIdStr)
       // })
    }

    func createControls()
    {
        var xPos:CGFloat = 50
        var yPos = GRAPHICS.Screen_Y() + 20
        var width = GRAPHICS.Screen_Width() - 100
        var height:CGFloat = 25
        
        let titleLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        titleLbl.text = "Search within"
        titleLbl.font = GRAPHICS.FONT_REGULAR(16)
        titleLbl.textAlignment = .Center
        titleLbl.textColor = UIColor(red: 228/255, green: 0/255, blue: 35/255, alpha: 1)
        self.m_bgImageView.addSubview(titleLbl)
        
        
        let backImg = GRAPHICS.PRODUCT_CATEGORY_UP_ARROW_IMAGE()
        width = backImg.size.width/2
        height = backImg.size.height/2
        yPos = titleLbl.frame.maxY - height
        xPos = GRAPHICS.Screen_Width() - width - 10
        let backBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        backBtn.setBackgroundImage(backImg, forState: .Normal)
        backBtn.addTarget(self, action: #selector(IBSearchProductCategoryVC.backBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_bgImageView.addSubview(backBtn)
        
        width = width + 40
        height = height + 40
        xPos = xPos - 20
        yPos = yPos - 20
        let backBigBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        backBigBtn.addTarget(self, action: #selector(IBSearchProductCategoryVC.backBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_bgImageView.addSubview(backBigBtn)
        
        xPos = GRAPHICS.Screen_X()
        yPos = GRAPHICS.Screen_Y() + 10
        width = 50
        height = 35
        let doneBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        doneBtn.addTarget(self, action: #selector(IBSearchProductCategoryVC.doneBtnAction(_:)), forControlEvents: .TouchUpInside)
        doneBtn.setTitle(Done, forState: .Normal)
        doneBtn.titleLabel!.font = GRAPHICS.FONT_REGULAR(14)
        doneBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
        self.m_bgImageView.addSubview(doneBtn)

        

        
        xPos =  GRAPHICS.Screen_X();
        yPos = titleLbl.frame.maxY
        width = GRAPHICS.Screen_Width()
        height = GRAPHICS.Screen_Height() - titleLbl.frame.size.height
        
        m_categoryTblView.frame = CGRectMake(xPos,yPos,width,height)
        m_categoryTblView.delegate = self
        m_categoryTblView.dataSource = self
        m_categoryTblView.separatorStyle = .None
        self.m_bgImageView.addSubview(m_categoryTblView)
        
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFromSell
        {
            switch textFieldTag {
            case 0:
                return m_categoryArray.count
            default:
                return subcategoryArray.count
            }
        }
        else{
        return m_categoryArray.count
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40.0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell:IBSearchProductCategoryCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBSearchProductCategoryCell
        if (cell == nil) {
            cell = IBSearchProductCategoryCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        if isFromSell
        {
            switch textFieldTag {
            case 0:
                let dict = m_categoryArray.objectAtIndex(indexPath.row)as? NSDictionary
                let catName = dict?.objectForKey("catname")as? String!
                cell?.m_titleLbl.text = catName//m_categoryArray.objectAtIndex(indexPath.row) as? String
            default:
                let dict = subcategoryArray.objectAtIndex(indexPath.row)as? NSDictionary
                let catName = dict?.objectForKey("catname") as? String!
                cell?.m_titleLbl.text = catName
            }
        }
        else{
            let dict = m_categoryArray.objectAtIndex(indexPath.row)as? NSDictionary
            let catName = dict?.objectForKey("catname")as? String!
            cell?.m_titleLbl.text = catName//m_categoryArray.objectAtIndex(indexPath.row) as? String
        }
        
        if categoryStr == cell?.m_titleLbl.text
        {
            cell?.ShowOrHideTickButton(false)
        }
        else
        {
            cell?.ShowOrHideTickButton(true)
        }
        
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if isFromSell
        {
            switch textFieldTag {
            case 0:
                let dict = m_categoryArray.objectAtIndex(indexPath.row)as? NSDictionary
                let catName = dict?.objectForKey("catname")as? String!
                categoryStr = catName!
                categoryIdStr = (dict?.objectForKey("catId")as? String!)!
            default:
                let dict = subcategoryArray.objectAtIndex(indexPath.row)as? NSDictionary
                let catName = dict?.objectForKey("catname") as? String!
                subcategoryIdStr = (dict?.objectForKey("subCatId")as? String!)!
                categoryStr = catName!//(subcategoryArray.objectAtIndex(indexPath.row) as! String)
            }
        }
        else{
            let dict = m_categoryArray.objectAtIndex(indexPath.row)as? NSDictionary
            let catName = dict?.objectForKey("catname")as? String!
            categoryStr = catName!
            categoryIdStr = (dict?.objectForKey("catId")as? String!)!
        }
        //m_selectionIndex = indexPath.row;
        tableView.reloadData();
    }
    func backBtnAction(sender : UIButton)
    {
        if isFromSell
        {
            for  vc in (self.navigationController?.viewControllers)!
            {
                if vc .isKindOfClass(IBProductsSellViewController)
                {
                    let sellVc : IBProductsSellViewController = vc as!  IBProductsSellViewController
                    switch textFieldTag {
                    case 0:
                        sellVc.categoryStr = categoryStr
                        sellVc.productEntity.categoryId = categoryIdStr
                    default:
                        sellVc.subcategoryStr = categoryStr
                        sellVc.productEntity.subcategoryId = subcategoryIdStr
                    }

                    
                }
            }
        }
        else
        {
            for  vc in (self.navigationController?.viewControllers)!
            {
                if vc .isKindOfClass(IBHomeViewController)
                {
                    let homePage : IBHomeViewController = vc as! IBHomeViewController
                    homePage.m_categoryStr = categoryStr//(m_categoryArray.objectAtIndex(m_selectionIndex) as? String)!
                    homePage.isFromCategory = true
                    homePage.isFromFilter = false
                    homePage.isFromLocation = false
                }
            }
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    func doneBtnAction(sender : UIButton)
    {
        if isFromSell
        {
            for  vc in (self.navigationController?.viewControllers)!
            {
                if vc .isKindOfClass(IBProductsSellViewController)
                {
                    let sellVc : IBProductsSellViewController = vc as!  IBProductsSellViewController
                    switch textFieldTag {
                    case 0:
                        sellVc.categoryStr = categoryStr
                        sellVc.productEntity.categoryId = categoryIdStr
                    default:
                        sellVc.subcategoryStr = categoryStr
                        sellVc.productEntity.subcategoryId = subcategoryIdStr
                    }
                }
            }
        }
        else
        {
            for  vc in (self.navigationController?.viewControllers)!
            {
                if vc .isKindOfClass(IBHomeViewController)
                {
                    let homePage : IBHomeViewController = vc as! IBHomeViewController
                    homePage.m_categoryStr = categoryStr//(m_categoryArray.objectAtIndex(m_selectionIndex) as? String)!
                    homePage.isFromCategory = true
                    homePage.isFromFilter = false
                    homePage.isFromLocation = false
                }
            }
        }
        self.navigationController?.popViewControllerAnimated(true)

    }
    
    // MARK: Api delegate
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    func API_CALLBACK_getActiveCategories(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
           if let categoryAry = resultDict.objectForKey("result") as? NSArray
           {
              if categoryAry.count > 0
              {
               if let categoryAry1 = resultDict.objectForKey("result") as? NSArray
               {
                  if categoryAry1.count > 0
                  {
                    m_categoryArray = NSMutableArray()
                    
                    for i in 0 ..< categoryAry1.count
                    {
                        let dict = categoryAry1[i] as! NSDictionary
                        m_categoryArray.addObject(dict)
                    }
                    
                  }
                }
              }
            
           }
           
        }
        m_categoryTblView.reloadData()
    }
    func API_CALLBACK_getActiveSubCategories(resultDict: NSDictionary)
    {
        NSLog("active subcategories");
        SwiftLoader.hide()
        
        if resultDict.objectForKey("error_code") as? String == "1"
        {
            let array1 : NSArray = (resultDict.objectForKey("result") as? NSArray)!
            
            for i in 0 ..< array1.count
            {
                
                subcategoryArray.addObject(array1.objectAtIndex(i))
            }
            //print("subcategories list Array/%@",subcategoryArray)
            if subcategoryArray.count == 0
            {
                showAlertViewWithMessage(Noresults_found_text)
            }
            m_categoryTblView.reloadData()
        }
        else{
            
        }
        
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
