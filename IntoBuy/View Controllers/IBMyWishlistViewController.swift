//
//  IBMyWishlistViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 02/08/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBMyWishlistViewController: IBBaseViewController,ServerAPIDelegate,UITableViewDelegate,UITableViewDataSource,MGSwipeTableCellDelegate {
    
    var wishListTblView = UITableView()
    var wishListArray = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.m_titleLabel.text = MyFavoritesTitle
        self.hideSettingsBtn(true)
        self.hideBackBtn(false)
        self.hideBottomView(false)
        createTableView()
        callAPIToGetWishlist()
    }
    
    func createTableView()
    {
        wishListTblView.frame = self.m_bgImageView.bounds
        wishListTblView.delegate = self
        wishListTblView.dataSource = self
        wishListTblView.separatorStyle = .None
        self.m_bgImageView.addSubview(wishListTblView)
    }
    
    func callAPIToGetWishlist() ->  ()
    {
        SwiftLoader.show(title: Loading, animated: true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getMyWhishlist(getUserIdFromUserDefaults())
    }
    
    //MARK:- tableview delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishListArray.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        //print((GRAPHICS.Screen_Width()/4) + 20)
        return (GRAPHICS.Screen_Width()/4) + 20
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell \(indexPath.row) \(indexPath.section)"
        var cell:IBMyWishlistCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBMyWishlistCell
        
        if (cell == nil) {
            cell = IBMyWishlistCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        let wishListEntity = wishListArray.objectAtIndex(indexPath.row) as! IBWishListEntity
        cell?.setValuesForControls(wishListEntity.image, productNamestr: wishListEntity.productname, reviewStr: wishListEntity.reviewcount, costStr: wishListEntity.price , rating: wishListEntity.average)
        cell?.delegate = self
        cell?.selectionStyle = .None
        cell?.frame.size.height = (cell?.bgView.frame.maxY)!
        //print(cell?.frame.size.height)
        return cell!
    }
    
    //MARK:- mgswipe tablew cell
    func swipeTableCell(cell: MGSwipeTableCell!, canSwipe direction: MGSwipeDirection) -> Bool {
        return true
    }
    func swipeTableCell(cell: MGSwipeTableCell!, swipeButtonsForDirection direction: MGSwipeDirection, swipeSettings: MGSwipeSettings!, expansionSettings: MGSwipeExpansionSettings!) -> [AnyObject]! {
        
        swipeSettings.transition = .Border;
        expansionSettings.buttonIndex = 0;
        //  __weak MSSendNotificationViewController * me = self;
        
        //let   me =  IBListOfCartsViewController()
        //    MSNotificationEntity *entity = [m_notificationsArray objectAtIndex:[m_notificationTableView indexPathForCell:cell].row];
        
        if (direction == .LeftToRight) {
            
        }
        else {
            
            expansionSettings.fillOnTrigger = true;
            expansionSettings.threshold = 1.1;
            // Add a remove button to the cell
            let removeButton = MGSwipeButton(title: "Remove", backgroundColor: UIColor.redColor(), callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                //print(self.wishListArray)
                let indexPath = self.wishListTblView.indexPathForCell(sender);
                self.deleteAction(indexPath!)
                return true
            })
            cell?.rightButtons = [removeButton]
            return [removeButton];
        }
        
        return nil
    }
    func swipeTableCell(cell: MGSwipeTableCell!, didChangeSwipeState state: MGSwipeState, gestureIsActive: Bool) {
        
    }
    func deleteAction(indexPath : NSIndexPath)
    {
        //print(indexPath.row)
        //print(self.wishListArray)
        let wishListEntity = self.wishListArray.objectAtIndex(indexPath.row) as! IBWishListEntity
        let userIdStr = getUserIdFromUserDefaults()
        let wishId = wishListEntity.wishId
        
        self.wishListArray.removeObject(wishListEntity)
        SwiftLoader.show(title: "Loading...", animated: true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_removeWish(wishId, userId: userIdStr)
    }
    
    
    
    //MARK:- server API delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    func API_CALLBACK_getMyWhishlist(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"
        {
            wishListArray.removeAllObjects()
            let resultArray = resultDict.objectForKey("result") as! NSArray
            if resultArray.count > 0
            {
                for i in 0 ..< resultArray.count
                {
                    let wishListDict = resultArray.objectAtIndex(i) as! NSDictionary
                    let wishListEntity = IBWishListEntity(dict: NSDictionary(dictionary: wishListDict))
                    wishListArray.addObject(wishListEntity)
                }
            }
            else{
                showAlertViewWithMessage("No products available in ou favorites")
            }
            wishListTblView.reloadData()
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    func API_CALLBACK_removeWish(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"
        {
            showAlertViewWithMessage(resultDict.objectForKey("result") as! String)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.callAPIToGetWishlist()
            })
            
            //wishListTblView.reloadData()
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    func API_CALLBACK_addProductToCart(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            showAlertViewWithMessage((resultDict.objectForKey("result")as? String!)!)
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.callAPIToGetWishlist()
            })
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_cartcount(getUserIdFromUserDefaults())
            }
        }
        else
        {
            //showAlertViewWithMessage((resultDict.objectForKey("msg")as? String!)!)
            showAlertViewWithMessage((resultDict.objectForKey("result")as? String!)!)
        }
    }
    func API_CALLBACK_cartcount(resultDict: NSDictionary)
    {
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            let cartCountStr = resultDict.objectForKey("result")as! String
            saveCartCountInUserDefaults(cartCountStr)
            cartcountLbl.text = cartCountStr
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    
    
}
