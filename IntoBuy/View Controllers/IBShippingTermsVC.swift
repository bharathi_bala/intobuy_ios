//
//  IBShippingTermsVC.swift
//  IntoBuy
//
//  Created by Manojkumar on 06/06/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBShippingTermsVC: IBBaseViewController {
    
    var shippingTextView = UITextView()
    var toolBar:UIToolbar!
    var shippingStr = ""
    

    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        self.m_titleLabel.text = "Shipping Terms & Conditions"
        //self.m_titleLabel.font = GRAPHICS.FONT_JENNASUE(20)
        self.m_titleLabel.adjustsFontSizeToFitWidth = true
        createControls()
    }
    func createControls()
    {
        
        let headingLbl = UILabel(frame: CGRectMake(GRAPHICS.Screen_X() + 10, self.m_bgImageView.bounds.origin.y + 20, GRAPHICS.Screen_Width() - 20, 20))
        headingLbl.text = "Enter shipping Terms & Conditions"
        headingLbl.font = GRAPHICS.FONT_REGULAR(14)
        self.m_bgImageView.addSubview(headingLbl)
        
        
        shippingTextView.frame = CGRectMake(GRAPHICS.Screen_X() + 10, headingLbl.frame.maxY + 20, GRAPHICS.Screen_Width() - 20, 100)
        shippingTextView.inputAccessoryView = openToolbar()
        shippingTextView.layer.cornerRadius = 1.0
        shippingTextView.layer.borderWidth = 1.0
        shippingTextView.layer.borderColor = UIColor.blackColor().CGColor
        self.m_bgImageView.addSubview(shippingTextView)
        let shippingTermsStr = decodeEncodedStringToNormalString(shippingStr)
        shippingTextView.text = shippingTermsStr
        
        
        
        let saveImg = GRAPHICS.SETTINGS_BILLING_BTN_IMAGE()
        var width = (saveImg.size.width)/2
        var height = (saveImg.size.height)/2
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            //height = 30
            width = (saveImg.size.width)/1.8
            height = (saveImg.size.height)/1.8
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (saveImg.size.width)/1.5
            height = (saveImg.size.height)/1.5
        }
        
        let xPos = (m_bgImageView.frame.size.width - width)/2
        let yPos = shippingTextView.frame.maxY + 50
        let saveBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        saveBtn.setBackgroundImage(saveImg, forState: UIControlState.Normal)
        saveBtn.setTitle("Save", forState: UIControlState.Normal)
        saveBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        saveBtn .addTarget(self, action: #selector(IBShippingTermsVC.saveBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        self.m_bgImageView.addSubview(saveBtn)

        
    }
    
    func openToolbar() -> UIToolbar
    {
        toolBar = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        toolBar.barStyle = UIBarStyle.BlackOpaque
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: #selector(IBProductsSellViewController.cancelBtnAction))
        let doneBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(IBProductsSellViewController.doneBtnAction))
        let btnArray = [cancelBtn,flexibleSpace,doneBtn];
        toolBar.setItems(btnArray, animated: false)
        return toolBar
    }
    func cancelBtnAction()
    {
        shippingTextView.text = ""
        toolBar .removeFromSuperview()
        self.view.endEditing(true)
    }
    func doneBtnAction()
    {
        
        toolBar .removeFromSuperview()
        self.view.endEditing(true)
        // m_categoryPicker .removeFromSuperview()
    }
    func saveBtnAction()
    {
        for  vc in (self.navigationController?.viewControllers)!
        {
            if vc .isKindOfClass(IBProductsSellViewController)
            {
                let sellVc : IBProductsSellViewController = vc as!  IBProductsSellViewController
                let shippingTermsText = convertStringToEncodedString(shippingTextView.text)
                sellVc.productEntity.terms = shippingTermsText
            }
        }
        self.navigationController?.popViewControllerAnimated(true)
    }

}
