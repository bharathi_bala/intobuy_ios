//
//  IBProductsSellViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 30/11/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBProductsSellViewController: IBBaseViewController ,UITextFieldDelegate,ServerAPIDelegate , UITextViewDelegate {

    let m_bgScrollView = UIScrollView()
    let m_imgScrollView = UIScrollView()
    var m_categoryTF = UITextField()
    var m_subCategoryTF = UITextField()
    var m_itemTF = UITextField()
    var m_priceTF = UITextField()
    var quantityTF = UITextField()
    var descriptionTF = UITextView()
    let m_categoryPicker = UIPickerView()
    var m_pickerToolBar:UIToolbar!
    var categoryStr = String()
    var subcategoryStr = String()
    var categoryIdStr = String()
    var subcategoryIdStr = String()
    var locationStr = String()
    var m_uploadImage: UIImage = UIImage()
    var addLocationBtn:UIButton!
    
    var isFirstImage : Bool = false
    var isSecondImage: Bool = false
    var isThirdImage : Bool = false
    var isFourthImage :Bool = false
    
    
    var firstImageView = UIImageView()
    var secondImageView = UIImageView()
    var thirdImageView = UIImageView()
    var fourthImageView = UIImageView()
    
    var isFromProfile = Bool()
    
    var productImage1Data = NSData()
    var productImage2Data = NSData()
    var productImage3Data = NSData()
    var productImage4Data = NSData()
    var productEntity = IBAddProductEntity()
    
    var productDetailsDict = NSDictionary()

    
    var m_categoryArray = ["Toys","Stationaries","Electricals","Electronics","vehicles","Sports items"]
    

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
       
        m_categoryTF.text = categoryStr
        m_subCategoryTF.text = subcategoryStr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
            self.hideHeaderViewForView(false)
            self.hideBottomView(false)
        self.hideSettingsBtn(true)
        m_titleLabel.text = Sell
        m_bgScrollView.frame = m_bgImageView.frame
        m_bgImageView .addSubview(m_bgScrollView)
        self.createControlsForSell()
        productEntity.twitter = "0"
        productEntity.facebook = "0"
        productEntity.latitude = "0.00"
        productEntity.longitude = "0.00"
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(IBProductsSellViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(IBProductsSellViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            
            if descriptionTF.isFirstResponder() {
                
                
                self.view.frame.origin.y -= keyboardSize.height/4 //dhatshin
            }
        }
    }
    func keyboardWillHide(notification: NSNotification) {
        
        self.view.frame.origin.y = 0
    
    }
        
        
    func createControlsForSell()
    {
        
        var xPos = GRAPHICS .Screen_X()
        var ypos = m_bgScrollView.bounds.origin.y
        var width = GRAPHICS .Screen_Width()
        var height = m_bgScrollView.frame.size.height / 3.5
        
        m_imgScrollView.frame = CGRectMake(xPos, ypos, width, height)
        m_bgScrollView .addSubview(m_imgScrollView)
        
        var imageArray:NSArray!
        if isFromProfile
        {
            imageArray = productDetailsDict.objectForKey("images") as! NSArray
        }
    
        let productBtnImage = GRAPHICS .SELL_ADD_PRODUCT_BTN_IMAGE()
        let imgSize = GRAPHICS.getImageWidthHeight(productBtnImage)
        xPos = m_imgScrollView.bounds.origin.x + 5
        ypos = m_imgScrollView.bounds.origin.y + 15
        width = imgSize.width//m_imgScrollView.frame.size.width/4 - 6
        height = imgSize.height//width - 50//m_imgScrollView.frame.size.height - 40
        
        firstImageView.frame = CGRectMake(xPos, ypos, width, height)
        firstImageView.userInteractionEnabled = true
        firstImageView.tag = 100
        switch isFromProfile {
        case true:
            let imageDict = imageArray.objectAtIndex(0) as! NSDictionary
            let imageStr = imageDict.objectForKey("image") as! String
            let imgUrl = NSURL(string: imageStr)
            firstImageView.load(imgUrl!, placeholder: productBtnImage, completionHandler: nil)
            productEntity.productimage = UIImagePNGRepresentation(firstImageView.image!)!
            createCloseButtonForImage(firstImageView)
            break
        case false:
            if (IBSingletonClass .sharedInstance.m_isFromProductFirstImage)
            {
                firstImageView.image = IBSingletonClass .sharedInstance.m_productFirstImage
                productEntity.productimage = UIImagePNGRepresentation(IBSingletonClass .sharedInstance.m_productFirstImage)!
                createCloseButtonForImage(firstImageView)
            }
            else
            {
                firstImageView.image = productBtnImage;
            }
            
            
        default:
            break
            
        }
                m_imgScrollView.addSubview(firstImageView)
        let firstimgTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductsSellViewController.productImgBtnAction(_:)))
        firstImageView.addGestureRecognizer(firstimgTapGesture)
        
        xPos = xPos + width + 5
        secondImageView.frame = CGRectMake(xPos, ypos, width, height)
        secondImageView.image = productBtnImage
        secondImageView.userInteractionEnabled = true
        secondImageView.tag = 101
        m_imgScrollView.addSubview(secondImageView)
        
        switch isFromProfile {
        case true:
            if imageArray.count >= 2 {
                let imageDict = imageArray.objectAtIndex(1) as! NSDictionary
                let imageStr = imageDict.objectForKey("image") as! String
                let imgUrl = NSURL(string: imageStr)
                secondImageView.load(imgUrl!, placeholder: productBtnImage, completionHandler: nil)
                productEntity.productimage2 = UIImagePNGRepresentation(secondImageView.image!)!
                createCloseButtonForImage(secondImageView)
            }
            break
        case false:
            break
        default:
            break
            
        }
        
        let secondImgTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductsSellViewController.productImgBtnAction(_:)))
        secondImageView.addGestureRecognizer(secondImgTapGesture)
        
        xPos = xPos + width + 5
        thirdImageView.frame = CGRectMake(xPos, ypos, width, height)
        thirdImageView.image = productBtnImage
        thirdImageView.tag = 102
        thirdImageView.userInteractionEnabled = true
        switch isFromProfile {
        case true:
            if imageArray.count >= 3 {
                let imageDict = imageArray.objectAtIndex(2) as! NSDictionary
                let imageStr = imageDict.objectForKey("image") as! String
                let imgUrl = NSURL(string: imageStr)

                thirdImageView.load(imgUrl!, placeholder: productBtnImage, completionHandler: nil)
                productEntity.productimage3 = UIImagePNGRepresentation(thirdImageView.image!)!
                createCloseButtonForImage(thirdImageView)
            }
            break
        case false:
            break
        default:
            break
            
        }

        m_imgScrollView.addSubview(thirdImageView)
        let thirdImgTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductsSellViewController.productImgBtnAction(_:)))
        thirdImageView.addGestureRecognizer(thirdImgTapGesture)


        xPos = xPos + width + 5
        fourthImageView.frame = CGRectMake(xPos, ypos, width, height)
        fourthImageView.image = productBtnImage
        fourthImageView.tag = 103
        fourthImageView.userInteractionEnabled = true
        switch isFromProfile {
        case true:
            if imageArray.count >= 4 {
                let imageDict = imageArray.objectAtIndex(3) as! NSDictionary
                let imageStr = imageDict.objectForKey("image") as! String
                let imgUrl = NSURL(string: imageStr)
//                let imgdata = NSData(contentsOfURL: imgUrl!)
//                fourthImageView.image = UIImage(data: imgdata!)
                fourthImageView.load(imgUrl!, placeholder: productBtnImage, completionHandler: nil)
                productEntity.productimage4 = UIImagePNGRepresentation(fourthImageView.image!)!
                createCloseButtonForImage(fourthImageView)
            }
            break
        case false:
            break
        default:
            break
            
        }

        m_imgScrollView.addSubview(fourthImageView)

        let fourthImgTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductsSellViewController.productImgBtnAction(_:)))
        fourthImageView.addGestureRecognizer(fourthImgTapGesture)
        
        
      
        
        firstImageView.contentMode = .ScaleAspectFill
        firstImageView.clipsToBounds = true
        secondImageView.contentMode = .ScaleAspectFill
        secondImageView.clipsToBounds = true
        thirdImageView.contentMode = .ScaleAspectFill
        thirdImageView.clipsToBounds = true
        fourthImageView.contentMode = .ScaleAspectFill
        fourthImageView.clipsToBounds = true
        
        m_imgScrollView.frame.size.height = ypos + height + 10
        m_imgScrollView.contentSize = CGSizeMake(xPos+10, ypos + height + 10)
        self.controlsForSell()
    }
    func createCloseButtonForImage(view : UIImageView)
    {
        let closeImg = GRAPHICS.SELL_DELETE_BTN_IMAGE()
        let imgSize = GRAPHICS.getImageWidthHeight(closeImg)
        let width = imgSize.width
        let height = imgSize.height
        let xPos = view.frame.size.width - width
        let yPos = view.bounds.origin.y
        let closeBtn = UIButton(frame : CGRectMake(xPos,yPos,width,height))
        closeBtn.setBackgroundImage(closeImg, forState: .Normal)
        closeBtn.addTarget(self, action: #selector(IBProductsSellViewController.productCloseBtnImage(_:)), forControlEvents: .TouchUpInside)
        view.addSubview(closeBtn)
        view.bringSubviewToFront(closeBtn)
    }
    func productCloseBtnImage(sender : UIButton) -> () {
        let imageView = sender.superview as! UIImageView
        
        if imageView.tag == 100
        {
            IBSingletonClass .sharedInstance.m_isFromProductFirstImage = false
            productEntity.productimage = NSData()
        }
        else if imageView.tag == 101
        {
            IBSingletonClass .sharedInstance.m_isFromProductSecondImage = false
            productEntity.productimage2 = NSData()
        }
        else if imageView.tag == 102
        {
            IBSingletonClass .sharedInstance.m_isFromProductThirdImage = false
            productEntity.productimage3 = NSData()
        }
        else
        {
            IBSingletonClass .sharedInstance.m_isFromProductFourthImage = false
            productEntity.productimage4 = NSData()
        }

        imageView.image = nil
        imageView.image = GRAPHICS .SELL_ADD_PRODUCT_BTN_IMAGE()
        sender.removeFromSuperview()
    
    }
    func controlsForSell()
    {
        var xPos = GRAPHICS .Screen_X() + 5
        var ypos = m_imgScrollView.frame.maxY
        var width = GRAPHICS .Screen_Width()
        var height:CGFloat = 20
        
        let detailsLbl = UILabel(frame: CGRectMake(xPos, ypos, width, height))
        detailsLbl.text = String(format: "%@:",Details)
        detailsLbl.font = GRAPHICS.FONT_BOLD(12)
        m_bgScrollView.addSubview(detailsLbl)
        
        ypos = detailsLbl.frame.maxY + 15
        width = GRAPHICS.Screen_Width()/2
        height = 20
        m_categoryTF.frame = CGRectMake(xPos, ypos, width, height)
        let placeHolder=NSAttributedString(string: category, attributes:[NSForegroundColorAttributeName : UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)])
        m_categoryTF.attributedPlaceholder = placeHolder
        m_categoryTF.font = GRAPHICS.FONT_REGULAR(12)
        m_categoryTF.backgroundColor = UIColor.clearColor()
        m_categoryTF.delegate = self
        m_bgScrollView.addSubview(m_categoryTF)
        
        let dropDownImg = GRAPHICS.SELL_DROP_BTN_IMAGE()
        width = dropDownImg.size.width;
        height = dropDownImg.size.height;
        xPos = GRAPHICS.Screen_Width() - width - 25
        ypos = (m_categoryTF.frame.maxY - height)
        
        let catdropDownBtn = UIButton(frame: CGRectMake(xPos, ypos, width, height))
        catdropDownBtn.setBackgroundImage(dropDownImg, forState: UIControlState.Normal)
        catdropDownBtn.addTarget(self, action: #selector(IBProductsSellViewController.categorydropDownBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_bgScrollView .addSubview(catdropDownBtn)
        
        width = 40
        height = 10
        xPos = catdropDownBtn.frame.origin.x - width
        ypos = (m_categoryTF.frame.maxY - height)-5
        let catSelectLbl = UILabel(frame: CGRectMake(xPos, ypos, width, height))
        catSelectLbl.text = "-select-"
        catSelectLbl.font = GRAPHICS.FONT_BOLD(10)
        catSelectLbl.userInteractionEnabled = true
        m_bgScrollView.addSubview(catSelectLbl)
        self.addBottomBorder(m_categoryTF)
        
        let catTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductsSellViewController.categorydropDownBtnAction))
        catTapGesture.numberOfTapsRequired = 1
        catSelectLbl.addGestureRecognizer(catTapGesture)
        
        
        xPos = GRAPHICS .Screen_X() + 5
        ypos = m_categoryTF.frame.maxY + 15
        width = GRAPHICS.Screen_Width()/1.5
        height = 20
        m_subCategoryTF.frame = CGRectMake(xPos, ypos, width, height)
        let subplaceHolder=NSAttributedString(string: subCategory, attributes:[NSForegroundColorAttributeName : UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)])
        m_subCategoryTF.attributedPlaceholder = subplaceHolder
        m_subCategoryTF.font = GRAPHICS.FONT_REGULAR(12)
        m_subCategoryTF.backgroundColor = UIColor.clearColor()
        m_subCategoryTF.delegate = self
        m_bgScrollView.addSubview(m_subCategoryTF)
        
        width = dropDownImg.size.width;
        height = dropDownImg.size.height;
        xPos = GRAPHICS.Screen_Width() - width - 25
        ypos = (m_subCategoryTF.frame.maxY - height)
        
        let subdropDownBtn = UIButton(frame: CGRectMake(xPos, ypos, width, height))
        subdropDownBtn.setBackgroundImage(dropDownImg, forState: UIControlState.Normal)
        subdropDownBtn.addTarget(self, action: #selector(IBProductsSellViewController.subCatdropDownBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_bgScrollView .addSubview(subdropDownBtn)

        
        width = 40
        height = 10
        xPos = subdropDownBtn.frame.origin.x - width
        ypos = (m_subCategoryTF.frame.maxY - height)-5
        let selectLbl = UILabel(frame: CGRectMake(xPos, ypos, width, height))
        selectLbl.text = "-select-"
        selectLbl.font = GRAPHICS.FONT_BOLD(10)
        m_bgScrollView.addSubview(selectLbl)
        selectLbl.userInteractionEnabled = true
        self.addBottomBorder(m_subCategoryTF)
        
        let subcatTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductsSellViewController.subCatdropDownBtnAction))
        subcatTapGesture.numberOfTapsRequired = 1
        selectLbl.addGestureRecognizer(subcatTapGesture)
        

        
        xPos = GRAPHICS .Screen_X() + 5
        ypos = m_subCategoryTF.frame.maxY + 15
        width = 100
        height = 20
        let itemLbl = UILabel(frame: CGRectMake(xPos, ypos, width, height))
        itemLbl.text = itemName
        itemLbl.textColor = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        itemLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_bgScrollView.addSubview(itemLbl)
        
        
        width = GRAPHICS.Screen_Width()/2
        xPos = GRAPHICS.Screen_Width() - width - 25
        //ypos = (m_itemTF.frame.maxY - height)-5
        
        height = 20
        m_itemTF.frame = CGRectMake(xPos, ypos, width, height)
        let itemPlaceHolder=NSAttributedString(string: WhatSelling, attributes:[NSForegroundColorAttributeName : UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)])
        m_itemTF.attributedPlaceholder = itemPlaceHolder
        m_itemTF.textAlignment = .Right
        m_itemTF.font = GRAPHICS.FONT_REGULAR(12)
        m_itemTF.delegate = self
        m_itemTF.returnKeyType = UIReturnKeyType.Next
        m_itemTF.backgroundColor = UIColor.clearColor()
        m_itemTF.inputAccessoryView = openToolbar()
        m_bgScrollView.addSubview(m_itemTF)
        self.addBottomBorder(m_itemTF)
        
        width = GRAPHICS.Screen_Width()/3
        height = 20
        xPos = GRAPHICS .Screen_X() + 5
        ypos = m_itemTF.frame.maxY + 15
        let priceLbl = UILabel(frame: CGRectMake(xPos, ypos, width, height))
        priceLbl.text = price
        priceLbl.font = GRAPHICS.FONT_REGULAR(12)
        priceLbl.textColor = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        m_bgScrollView.addSubview(priceLbl)
        

        
        width = GRAPHICS.Screen_Width()/4
        xPos = GRAPHICS.Screen_Width() - width - 10
        height = 20
        m_priceTF.frame = CGRectMake(xPos, ypos, width, height)
        let pricePlaceHolder=NSAttributedString(string: "0.00", attributes:[NSForegroundColorAttributeName : UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)])
        m_priceTF.attributedPlaceholder = pricePlaceHolder
        m_priceTF.font = GRAPHICS.FONT_REGULAR(12)
        m_priceTF.textAlignment = .Center
        m_priceTF.delegate = self
        m_priceTF.keyboardType = .NumberPad
        m_priceTF.backgroundColor = UIColor.clearColor()
        let priceLeftView =  UILabel(frame: CGRect(x: 0, y: 0, width: 10, height: height))
        priceLeftView.font = GRAPHICS.FONT_REGULAR(12)
        priceLeftView.text = "$"
        m_priceTF.leftView = priceLeftView
        m_priceTF.leftViewMode = .Always
        m_priceTF.inputAccessoryView = openToolbar()
        m_bgScrollView.addSubview(m_priceTF)
        self.addBottomBorder(m_priceTF)
        
        xPos = GRAPHICS .Screen_X() + 5
        ypos = m_priceTF.frame.maxY + 15
        width = GRAPHICS.Screen_Width()/3
        height = 20
        let quantiyLbl = UILabel(frame: CGRectMake(xPos, ypos, width, height))
        quantiyLbl.text = "Quantity"
        quantiyLbl.font = GRAPHICS.FONT_REGULAR(12)
        quantiyLbl.textColor = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        m_bgScrollView.addSubview(quantiyLbl)
        self.addBottomBorder(quantiyLbl)
        
        let quantityTextImg = GRAPHICS.SELL_QUANTITY_BOX_IMAGE()
        width =   GRAPHICS.Screen_Width()/4
        height = 25
        xPos = GRAPHICS.Screen_Width() - width - 25
        
        quantityTF.frame = CGRectMake(xPos, ypos - 5, width, height)
        quantityTF.delegate = self
        quantityTF.textAlignment = .Center
        quantityTF.background = quantityTextImg
        quantityTF.keyboardType = .NumberPad
        quantityTF.font = GRAPHICS.FONT_REGULAR(12)
        quantityTF.inputAccessoryView = openToolbar()
        m_bgScrollView.addSubview(quantityTF)
        

        ypos = quantiyLbl.frame.maxY + 15
        xPos = GRAPHICS .Screen_X() + 5
        width = GRAPHICS.Screen_Width()/2
        let locationLbl = UILabel(frame: CGRectMake(xPos, ypos, width, height))
        locationLbl.text = "Add Location"
        locationLbl.font = GRAPHICS.FONT_REGULAR(12)
        locationLbl.textColor = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        m_bgScrollView.addSubview(locationLbl)
        self.addBottomBorder(locationLbl)
        
        let btnImg = GRAPHICS.SELL_SWITCH_BTN_NORMAL_IMAGE()
        let btnSelImg = GRAPHICS.SELL_SWITCH_BTN_SELECTED_IMAGE()
        let btnW = btnImg.size.width/1.5;
        let btnH = btnImg.size.height/1.5;
        xPos = GRAPHICS.Screen_Width() - btnW - 20
        ypos = (locationLbl.frame.maxY - btnH)-5
        addLocationBtn = UIButton(frame: CGRectMake(xPos, ypos, btnW, btnH))
        addLocationBtn.setBackgroundImage(btnImg, forState: UIControlState.Normal)
        addLocationBtn.setBackgroundImage(btnSelImg, forState: UIControlState.Selected)
        addLocationBtn.addTarget(self, action: #selector(IBProductsSellViewController.addLocationBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        m_bgScrollView .addSubview(addLocationBtn)
        self.addBottomBorder(locationLbl)

        xPos = GRAPHICS .Screen_X() + 5
        ypos = locationLbl.frame.maxY + 15
        width = GRAPHICS.Screen_Width() - 100
        let shippingTermsLbl = UILabel(frame: CGRectMake(xPos, ypos, width, height))
        shippingTermsLbl.text = "Add Shipping Terms & Conditions"
        shippingTermsLbl.font = GRAPHICS.FONT_REGULAR(12)
        shippingTermsLbl.textColor = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        m_bgScrollView.addSubview(shippingTermsLbl)
        self.addBottomBorder(shippingTermsLbl)
        
        shippingTermsLbl.userInteractionEnabled = true
        
        let shippingTermsGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductsSellViewController.shippingTapGesture))
        shippingTermsLbl.addGestureRecognizer(shippingTermsGesture)

        width = dropDownImg.size.width;
        height = dropDownImg.size.height;
        xPos = GRAPHICS.Screen_Width() - width - 25
        ypos = (shippingTermsLbl.frame.maxY - height)-5
        
        let shippingDropDownBtn = UIButton(frame: CGRectMake(xPos, ypos, width, height))
        shippingDropDownBtn.setBackgroundImage(dropDownImg, forState: UIControlState.Normal)
        shippingDropDownBtn.addTarget(self, action: #selector(IBProductsSellViewController.shippingDropDownBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_bgScrollView .addSubview(shippingDropDownBtn)

        xPos = GRAPHICS .Screen_X() + 5
        ypos = shippingTermsLbl.frame.maxY + 15
        width = GRAPHICS.Screen_Width() - 10
        height = 50
        descriptionTF.frame = CGRectMake(xPos, ypos - 5, width, height)
        descriptionTF.delegate = self
        descriptionTF.text = Produts_descrption
        descriptionTF.font = GRAPHICS.FONT_REGULAR(12)
        descriptionTF.inputAccessoryView = openToolbar()
        m_bgScrollView.addSubview(descriptionTF)

        self.addBottomBorder(descriptionTF)
        //descriptionTF
        
        if isFromProfile
        {
            //print((productDetailsDict.objectForKey("catogery_name") as? String)!, (productDetailsDict.objectForKey("sub_catogery_name") as? String)!)
            categoryStr = (productDetailsDict.objectForKey("catogery_name") as? String)!
            subcategoryStr = (productDetailsDict.objectForKey("sub_catogery_name") as? String)!
            m_categoryTF.text = categoryStr
            m_subCategoryTF.text = subcategoryStr
            m_priceTF.text = productDetailsDict.objectForKey("price") as? String
            quantityTF.text = productDetailsDict.objectForKey("stockavailability") as? String
            descriptionTF.text = productDetailsDict.objectForKey("description") as? String
            m_itemTF.text = productDetailsDict.objectForKey("productname") as? String
            if productDetailsDict.objectForKey("address") as? String != ""
            {
                addLocationBtn.selected = true
            }
            productEntity.categoryId = (productDetailsDict.objectForKey("catId") as? String)!
            productEntity.subcategoryId = (productDetailsDict.objectForKey("subCatId") as? String)!
            productEntity.productname = m_itemTF.text!
            productEntity.price = m_priceTF.text!
            productEntity.quantity = quantityTF.text!
            productEntity.descriptionStr = descriptionTF.text!
            productEntity.address = (productDetailsDict.objectForKey("address") as? String)!
            productEntity.facebook = (productDetailsDict.objectForKey("facebook") as? String)!
            productEntity.twitter = (productDetailsDict.objectForKey("twitter") as? String)!
            productEntity.latitude = (productDetailsDict.objectForKey("latitude") as? String)!
            productEntity.longitude = (productDetailsDict.objectForKey("logitude") as? String)!
        }

        self.createControlsForShareOptions()
        
    }
    func createControlsForShareOptions()
    {
        var xPos = GRAPHICS .Screen_X() + 5
        var ypos = descriptionTF.frame.maxY + 20//(m_priceTF.frame.maxY * 1.5) + 20
        var width = GRAPHICS .Screen_Width()
        var height:CGFloat = 20
        
        let shareLbl = UILabel(frame: CGRectMake(xPos, ypos, width, height))
        shareLbl.text = String(format: "%@:",shareOn)
        shareLbl.font = GRAPHICS.FONT_BOLD(12)
        m_bgScrollView.addSubview(shareLbl)

        xPos = GRAPHICS .Screen_X() + 5
        ypos = shareLbl.frame.maxY + 15
        width = GRAPHICS.Screen_Width()/2
        height = 25
        
        let twitterLbl = UILabel(frame: CGRectMake(xPos, ypos, width, height))
        twitterLbl.text = twitter
        twitterLbl.textColor = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        twitterLbl.font = GRAPHICS.FONT_REGULAR(13)
        m_bgScrollView.addSubview(twitterLbl)
        
        let btnImg = GRAPHICS.SELL_SWITCH_BTN_NORMAL_IMAGE()
        let btnSelImg = GRAPHICS.SELL_SWITCH_BTN_SELECTED_IMAGE()
        let btnW = btnImg.size.width/1.5;
        let btnH = btnImg.size.height/1.5;
        xPos = GRAPHICS.Screen_Width() - btnW - 20
        ypos = (twitterLbl.frame.maxY - btnH)-5
        let twitterBtn = UIButton(frame: CGRectMake(xPos, ypos, btnW, btnH))
        twitterBtn.setBackgroundImage(btnImg, forState: UIControlState.Normal)
        twitterBtn.setBackgroundImage(btnSelImg, forState: UIControlState.Selected)
        twitterBtn.addTarget(self, action: #selector(IBProductsSellViewController.twitterBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        m_bgScrollView .addSubview(twitterBtn)
        self.addBottomBorder(twitterLbl)
        
        xPos = GRAPHICS.Screen_X() + 5
        ypos = twitterLbl.frame.maxY + 10
        width = GRAPHICS.Screen_Width()/2
        height = 25
        let facebookLbl = UILabel(frame: CGRectMake(xPos, ypos, width, height))
        facebookLbl.text = Facebook
        facebookLbl.font = GRAPHICS.FONT_REGULAR(13)
        facebookLbl.textColor = UIColor(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1.0)
        m_bgScrollView.addSubview(facebookLbl)
        
        xPos = GRAPHICS.Screen_Width() - btnW - 20
        ypos = (facebookLbl.frame.maxY - btnH)-5
        let facebookBtn = UIButton(frame: CGRectMake(xPos, ypos, btnW, btnH))
        facebookBtn.setBackgroundImage(btnImg, forState: UIControlState.Normal)
        facebookBtn.setBackgroundImage(btnSelImg, forState: UIControlState.Selected)
        facebookBtn.addTarget(self, action: #selector(IBProductsSellViewController.facebookBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        m_bgScrollView .addSubview(facebookBtn)
        self.addBottomBorder(facebookLbl)
        
        if isFromProfile
        {
            if (productDetailsDict.objectForKey("facebook") as? String)! == "1"
            {
                facebookBtn.selected = true
                productEntity.facebook = "1"
            }
            else
            {
                facebookBtn.selected = false
                productEntity.facebook = "0"
            }
            if (productDetailsDict.objectForKey("twitter") as? String)! == "1"
            {
                twitterBtn.selected = true
                productEntity.twitter = "1"
            }
            else
            {
                twitterBtn.selected = false
                productEntity.twitter = "0"
            }
        }

        var productBtnImg:UIImage!
        if isFromProfile
        {
            productBtnImg = GRAPHICS.SETTINGS_BILLING_BTN_IMAGE()

        }
        else
        {
            productBtnImg = GRAPHICS.SELL_PRODUCT_BTN_IMAGE()
        }
        
        width = productBtnImg.size.width/2
        height = productBtnImg.size.height/2
        ypos = facebookLbl.frame.maxY + 30
        xPos = (GRAPHICS.Screen_Width() - width)/2
        let postProductBtn = UIButton(frame: CGRectMake(xPos, ypos, width, height))
        postProductBtn.setBackgroundImage(productBtnImg, forState: UIControlState.Normal)
        postProductBtn.addTarget(self, action: #selector(IBProductsSellViewController.postProductBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        if isFromProfile
        {
            postProductBtn.setTitle(save, forState: .Normal)
        }
        m_bgScrollView .addSubview(postProductBtn)
        m_bgScrollView.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), postProductBtn.frame.maxY+10)
        
    }
    func shippingTapGesture() -> () {
        let shippingTerms = IBShippingTermsVC()
        if isFromProfile
        {
            shippingTerms.shippingStr = productDetailsDict.objectForKey("terms") as! String
            productEntity.terms = productDetailsDict.objectForKey("terms") as! String
        }
        else
        {
            if productEntity.terms != ""
            {
                shippingTerms.shippingStr = productEntity.terms
            }
            else{
                shippingTerms.shippingStr = ""
            }
        }
        self.navigationController?.pushViewController(shippingTerms, animated: true)
    }
    func productImgBtnAction(tapGesture : UITapGestureRecognizer)
    {
        var index = IBSingletonClass.sharedInstance.m_UploadImageIndex
        //print(tapGesture.view?.tag)
        index = (tapGesture.view?.tag)!
        IBSingletonClass.sharedInstance.screenIndex = 0
        IBSingletonClass.sharedInstance.m_UploadImageIndex = index
        IBSingletonClass.sharedInstance.isFromSell = true
        IBSingletonClass.sharedInstance.isFromUpload = false
        
        if (tapGesture.view?.tag == 100)
        {
            isFirstImage = true
            isSecondImage = false
            isThirdImage = false
            isFourthImage = false
            IBSingletonClass.sharedInstance.m_isFromProductFirstImage = true
            IBSingletonClass.sharedInstance.isFromImageTap = true
            
        }
        else if (tapGesture.view?.tag == 101)
        {
            isFirstImage = false
            isSecondImage = true
            isThirdImage = false
            isFourthImage = false
            IBSingletonClass.sharedInstance.m_isFromProductSecondImage = true
            
        }
        else if (tapGesture.view?.tag == 102)
        {
            isFirstImage = false
            isSecondImage = false
            isThirdImage = true
            isFourthImage = false
            IBSingletonClass.sharedInstance.m_isFromProductThirdImage = true
        }
        else if (tapGesture.view?.tag == 103)
        {
            isFirstImage = false
            isSecondImage = false
            isThirdImage = false
            isFourthImage = true
            IBSingletonClass.sharedInstance.m_isFromProductFourthImage = true
        }
        if isFromProfile
        {
            IBSingletonClass.sharedInstance.toEditProduct = true
        }
        else{
            IBSingletonClass.sharedInstance.toEditProduct = false
        }

        //let ycameraViewController1 = YCameraViewController()
        //self.presentViewController(ycameraViewController1, animated: true, completion: nil)
//        if (tapGesture.view?.tag == 100)
//        {
//
//        let ycameraViewVc = YCameraViewController()
//        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//
//        let camNav = UINavigationController(rootViewController: ycameraViewVc)
//        camNav .setNavigationBarHidden(true, animated: false)
//        appDelegate.navController!.presentViewController(camNav, animated: false, completion: nil);
//        }
//        else{
        if isFromProfile
        {
            let ycameraViewVc = YCameraViewController()
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            
            let camNav = UINavigationController(rootViewController: ycameraViewVc)
            camNav .setNavigationBarHidden(true, animated: false)
            appDelegate.navController!.presentViewController(camNav, animated: false, completion: nil);
        }
        else
        {
//            if IBSingletonClass.sharedInstance.isFromImageTap == true
//            {
//                let ycameraViewVc = YCameraViewController()
//                self.navigationController!.presentViewController(ycameraViewVc, animated: false, completion: nil)
//            }
//            else
//            {
                let ycameraViewVc = YCameraViewController()
                self.navigationController!.pushViewController(ycameraViewVc, animated: false);
           // }
        }
        
      //  }
        
        
    }
    func postProductBtnAction(sender : UIButton)
    {
        if self.validation()
        {
            SwiftLoader.show(animated: true)
            SwiftLoader.show(title:"Loading...", animated:true)
         productEntity.userId = getUserIdFromUserDefaults()
//            productEntity.latitude = "72.8561644"
//            productEntity.longitude = "19.0176147"
            if isFromProfile
            {
                let productIdStr = productDetailsDict.objectForKey("productId") as! String
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_editProduct(productEntity, productId: productIdStr)
            }
            else
            {
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_addProduct(productEntity)
            }
        }
    }
    func moveToHomeScreen()
    {
        SwiftLoader.hide()
        self.view.endEditing(true)
        self .dismissViewControllerAnimated(true, completion: { () -> Void in
        })

    }
    func twitterBtnAction(sender : UIButton)
    {
        sender.selected = !sender.selected
        if sender.selected
        {
            productEntity.twitter = "1"
        }
        else{
            productEntity.twitter = "0"
        }

    }
    func facebookBtnAction(sender : UIButton)
    {
        sender.selected = !sender.selected
        if sender.selected
        {
            productEntity.facebook = "1"
        }
        else{
            productEntity.facebook = "0"
        }
    }
    
    func validation()->Bool{
        
        if m_categoryTF.text?.isEmpty == true{
            showAlert("Please enter your category")
            return false
        }
        else if m_subCategoryTF.text?.isEmpty == true{
            showAlert("Please enter your sub category")
            return false
        }
        else if m_itemTF.text?.isEmpty == true{
            showAlert("Please enter your sub item")
            return false
        }
        else if m_priceTF.text?.isEmpty == true{
            showAlert("Please enter your price")
            return false
        }
        else if quantityTF.text?.isEmpty == true{
            showAlert("Please enter your quantity")
            return false
        }
        else if descriptionTF.text?.isEmpty == true{
            showAlert("Please enter description")
            return false
        }

        return true
    }
    func showAlert(msg:String!){
        
        let alert = UIAlertView(title: "IntoBuy", message:msg, delegate: nil, cancelButtonTitle:"Ok")
        alert .show()
        
    }

    func addLocationBtnAction(sender : UIButton) -> () {
        sender.selected = !sender.selected
        if sender.selected
        {
            let mapScreenVC = IBSearchProductLocationVC()
            mapScreenVC.isFromSell = true
            self.navigationController?.pushViewController(mapScreenVC, animated: true)
        }
        else{
            
        }
    }
    func categorydropDownBtnAction()
    {
        let selectCategoryVc = IBSearchProductCategoryVC()
        selectCategoryVc.isFromSell = true
        selectCategoryVc.textFieldTag = 0
        if categoryStr == ""
        {
            categoryStr = "All categories"
        }
        selectCategoryVc.categoryStr = categoryStr
        selectCategoryVc.toGetSubcategory = false
        self.navigationController?.pushViewController(selectCategoryVc, animated: true)

        //self.openPickerView();
    }
    func subCatdropDownBtnAction()
    {
        let selectCategoryVc = IBSearchProductCategoryVC()
        selectCategoryVc.isFromSell = true
        selectCategoryVc.textFieldTag = 1
        if subcategoryStr == ""
        {
            subcategoryStr = "For him clothing"
        }
        selectCategoryVc.toGetSubcategory = true
        selectCategoryVc.categoryStr = subcategoryStr
        selectCategoryVc.categoryIdStr = productEntity.categoryId
        self.navigationController?.pushViewController(selectCategoryVc, animated: true)
        //self.openPickerView();
    }

    func shippingDropDownBtnAction() -> () {
        let shippingTerms = IBShippingTermsVC()
        self.navigationController?.pushViewController(shippingTerms, animated: true)
    }
    override func backBtnAction() {
        
        if isFromProfile{
            self.view.endEditing(true)
            self.navigationController?.popViewControllerAnimated(true)
        }
        else{
            IBSingletonClass.sharedInstance.m_isFromProductFirstImage = false
            IBSingletonClass.sharedInstance.m_isFromProductSecondImage = false
            IBSingletonClass.sharedInstance.m_isFromProductThirdImage = false
            IBSingletonClass.sharedInstance.m_isFromProductFourthImage = false
            isFirstImage = false
            isSecondImage = false
            isThirdImage = false
            isFourthImage = false
            
            self.view.endEditing(true)
            self .dismissViewControllerAnimated(true, completion: { () -> Void in
                
            })
        }
    }
    //MARK:- textfield delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if(textField == m_itemTF)
        {
            m_itemTF .resignFirstResponder()
            m_priceTF.becomeFirstResponder()
        }
        else
        {
            m_priceTF .resignFirstResponder()
        }
        return true
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if(textField == m_categoryTF)
        {
//            self.openPickerView();
//            self.openToolbar();
            let selectCategoryVc = IBSearchProductCategoryVC()
            selectCategoryVc.isFromSell = true
            selectCategoryVc.textFieldTag = 0
            if categoryStr == ""
            {
                categoryStr = "All categories"
            }
            selectCategoryVc.categoryStr = categoryStr
            selectCategoryVc.toGetSubcategory = false
            self.navigationController?.pushViewController(selectCategoryVc, animated: true)
            return false;
        }
        else if textField == m_subCategoryTF
        {
            let selectCategoryVc = IBSearchProductCategoryVC()
            selectCategoryVc.isFromSell = true
            selectCategoryVc.textFieldTag = 1
            if subcategoryStr == ""
            {
                subcategoryStr = "For him clothing"
            }
            selectCategoryVc.toGetSubcategory = true
            selectCategoryVc.categoryStr = subcategoryStr
            selectCategoryVc.categoryIdStr = productEntity.categoryId
            self.navigationController?.pushViewController(selectCategoryVc, animated: true)
            return false;
        }
        else if textField == m_priceTF
        {
            if m_priceTF.text == ""
            {
//                m_priceTF.text = "$ "
            }
        }
        
        if(textField.frame.origin.y >  m_bgScrollView.frame.size.height - 250){
            var contentOffset:CGFloat!
            if(GRAPHICS.Screen_Type() == IPHONE_4){
                contentOffset = 120.0
            }
            else if(GRAPHICS.Screen_Type() == IPHONE_5){
                contentOffset = 150.0;
            }
            else
            {
                contentOffset = 100
            }
            m_bgScrollView.setContentOffset(CGPointMake(0, (m_bgScrollView.frame.origin.y + contentOffset)), animated: true)
        }

        
        return true
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == m_itemTF
        {
                productEntity.productname = m_itemTF.text!
        }
        else if textField == m_priceTF
        {
            var priceStr = m_priceTF.text!
//            priceStr = priceStr.stringByReplacingOccurrencesOfString("$ ", withString: "")
            productEntity.price = priceStr
        }
        else if textField == quantityTF
        {
                productEntity.quantity = quantityTF.text!
        }
        else if textField == descriptionTF
        {
            let descriptionStr = convertStringToEncodedString(descriptionTF.text!)
                productEntity.descriptionStr = descriptionStr
        }
        if(textField.frame.origin.y >  self.m_bgImageView.frame.size.height - 200){
            var contentOffset:CGFloat!
            if(GRAPHICS.Screen_Type() == IPHONE_4){
                contentOffset = 120.0
            }
            else if(GRAPHICS.Screen_Type() == IPHONE_5){
                contentOffset = 150.0;
            }
            m_bgScrollView.setContentOffset(CGPointMake(0, 0), animated: true)
        }


    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField == m_priceTF
        {
            if textField.text?.characters.count <= 8
            {
                if textField.text?.characters.count == 8
                {
                    if string == ""
                    {
                        return true
                    }
                    else
                    {
                        return false
                    }
                }
                else
                {
                    return true
                }
            }
            else
            {
                return false
            }
        }
        return true
    }
    
    
    func textViewDidBeginEditing(textView: UITextView) {
     
        if textView == descriptionTF
        {
            if descriptionTF.text == Produts_descrption {
                descriptionTF.text = ""
            }
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
         m_pickerToolBar .removeFromSuperview() //deepika
         if textView == descriptionTF
        {
            let descriptionStr = convertStringToEncodedString(descriptionTF.text!)
            productEntity.descriptionStr = descriptionStr
        }
    }
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        
        return true
    }
    func addBottomBorder(view: UIView)
    {
        let lineImg = GRAPHICS.SELL_LINE_IMAGE()
        let bottomBorder = UIImageView(frame: CGRectMake(GRAPHICS.Screen_X()+5, view.frame.maxY+5, GRAPHICS.Screen_Width() - 10, 1.0))
        bottomBorder.image = lineImg
        m_bgScrollView.addSubview(bottomBorder)
    }
    func openPickerView()
    {
//        m_categoryPicker.frame = CGRectMake(0, GRAPHICS.Screen_Height() - 200, GRAPHICS.Screen_Width(), 150)
//        m_categoryPicker.delegate = self;
//        m_categoryPicker.dataSource = self;
//        m_categoryPicker.backgroundColor = UIColor.whiteColor();
//        m_categoryPicker.showsSelectionIndicator = true;
//        self.m_bgImageView .addSubview(m_categoryPicker)
    }
    func openToolbar() -> UIToolbar
    {
        m_pickerToolBar = UIToolbar(frame : CGRectMake(GRAPHICS.Screen_X(), m_categoryPicker.frame.origin.y - 44, GRAPHICS.Screen_Width(), 44))
        m_pickerToolBar.barStyle = UIBarStyle.BlackOpaque
        
        let previousImage = GRAPHICS.PICKER_LEFT_ARROW_BTN()
        let nextImage = GRAPHICS.PICKER_RIGHT_ARROW_BTN()
        
        let prev_Btn = UIButton ()
        prev_Btn.frame = CGRectMake(10, 8, 40, 30)
        prev_Btn.backgroundColor =  UIColor.clearColor();
        prev_Btn.setBackgroundImage(previousImage, forState: .Normal)
        prev_Btn.addTarget(self, action: #selector(IBProductsSellViewController.previousAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_pickerToolBar.addSubview(prev_Btn)
        
        let next_Btn = UIButton ()
        next_Btn.frame = CGRectMake(50, 8, 40, 30)
        next_Btn.backgroundColor =  UIColor.clearColor();
        next_Btn.setBackgroundImage(nextImage, forState: .Normal)
        next_Btn.addTarget(self, action: #selector(IBProductsSellViewController.nextAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_pickerToolBar.addSubview(next_Btn)
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBProductsSellViewController.doneBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_pickerToolBar.addSubview(done_Btn)

        
//        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
//        let cancelBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Cancel, target: self, action: #selector(IBProductsSellViewController.cancelBtnAction))
//        let doneBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(IBProductsSellViewController.doneBtnAction))
//        let btnArray = [cancelBtn,flexibleSpace,doneBtn];
//        m_pickerToolBar.setItems(btnArray, animated: false)
        return m_pickerToolBar
    }
    func cancelBtnAction()
    {
        self.view.endEditing(true)
        m_pickerToolBar .removeFromSuperview()
    }
    func previousAction()
    {
        if m_itemTF.isFirstResponder()
        {
            m_itemTF.resignFirstResponder()
        }
        else if m_priceTF.isFirstResponder()
        {
            m_priceTF.resignFirstResponder()
            m_itemTF.becomeFirstResponder()
        }
        else if quantityTF.isFirstResponder()
        {
            quantityTF.resignFirstResponder()
            m_priceTF.becomeFirstResponder()
        }
        else if descriptionTF.isFirstResponder()
        {
            descriptionTF.resignFirstResponder()
        }

    }
    func nextAction()
    {
        if m_itemTF.isFirstResponder()
        {
            m_itemTF.resignFirstResponder()
            m_priceTF.becomeFirstResponder()
        }
        else if m_priceTF.isFirstResponder()
        {
            m_priceTF.resignFirstResponder()
            quantityTF.becomeFirstResponder()
        }
        else if quantityTF.isFirstResponder()
        {
            quantityTF.resignFirstResponder()
        }
        else if descriptionTF.isFirstResponder()
        {
            descriptionTF.resignFirstResponder()
        }

    }
    func doneBtnAction()
    {
        if m_itemTF.isFirstResponder()
        {
            productEntity.productname = m_itemTF.text!
        }
                self.view.endEditing(true)
        m_pickerToolBar .removeFromSuperview()
       // m_categoryPicker .removeFromSuperview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- api delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlert(errorMessage)
    }
    func API_CALLBACK_AddProduct(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        if resultDict.objectForKey("error_code") as! String == "1"
        {
            self .dismissViewControllerAnimated(true, completion: { () -> Void in
                
            })
        }
        else{
            showAlert(resultDict.objectForKey("msg") as! String)
        }
    }
    func API_CALLBACK_editProduct(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        if resultDict.objectForKey("error_code") as! String == "1"
        {
            showAlert(resultDict.objectForKey("result") as! String)
            self.navigationController?.popViewControllerAnimated(true)
//            self .dismissViewControllerAnimated(true, completion: { () -> Void in
//                
//            })
        }
        else{
            showAlert(resultDict.objectForKey("msg") as! String)
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
