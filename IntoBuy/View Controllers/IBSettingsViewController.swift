//
//  IBSettingsViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 07/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class IBSettingsViewController: IBBaseViewController,UITableViewDelegate,UITableViewDataSource,ServerAPIDelegate,UIAlertViewDelegate {
    
    var m_settingsTblView : UITableView!
    var m_settingsArray : NSMutableArray = NSMutableArray()
    var languageBGView:UIView!
    //Account type, My coins, language setting, Manage product
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        self.m_titleLabel.text = Settings_Title
        self.hideBottomView(false)
        self.hideSettingsBtn(true)
        self.toShowActivityBtn = true
        let subscriptionArray = [Settings_EditProfile] //,Settings_AccountType,Settings_MyCoins
        let settingsArray = [Settings_ProfileVisible,Settings_NotificationSetting]
        let aboutArray = [Settings_InviteFriends,Settings_PrivacyPolicy,Settings_Terms,Settings_ContactUs,Settings_AboutVersion]
        let manageProductArray = [Settings_LogOut] //Settings_Wishlist,Settings_BillingDetails,Settings_ShippingDetails,
        
        m_settingsArray = [subscriptionArray,settingsArray,aboutArray,manageProductArray]
        
        self.createTableView()
        // Do any additional setup after loading the view.
    }
    func createTableView()
    {
        m_settingsTblView = UITableView(frame: self.m_bgImageView.bounds)
        m_settingsTblView.delegate = self
        m_settingsTblView.dataSource = self
        m_settingsTblView.separatorStyle = .None
        self.m_bgImageView .addSubview(m_settingsTblView)
        
    }
    // table view delegates
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == 0)
        {
            if indexPath.row == 0 || indexPath.row == 1{
                return 30;
            }
            else{
                return 40
            }
        }
        else{
            return 40
        }
        //        return 40
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return m_settingsArray.count;
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if(section == 3){
            return 0
        }
        
        return 30;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0;
        if(section == 0)
        {
            let array = m_settingsArray .objectAtIndex(0)
            count = array.count;
        }
        else if(section == 1){
            let array = m_settingsArray .objectAtIndex(1)
            count = array.count;
        }
        else if(section == 2){
            let array = m_settingsArray .objectAtIndex(2)
            count = array.count;
        }
        else if(section == 3){
            let array = m_settingsArray .objectAtIndex(3)
            count = array.count;
        }
        
        return count;
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell:IBSettingsTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBSettingsTableViewCell
        
        if (cell == nil) {
            cell = IBSettingsTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        cell?.m_switchBtn.tag = indexPath.row
        cell?.m_switchBtn.addTarget(self, action: #selector(IBSettingsViewController.switchBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        if(indexPath.section == 0)
        {
            let array = m_settingsArray .objectAtIndex(0)
            cell?.m_titleLbl.text = array.objectAtIndex(indexPath.row) as? String
            cell?.frame.size.height = 40
            cell?.showOrHideSwitchBtn(true)
            cell?.m_arrowBtn.hidden = false;
            if indexPath.row == array.count - 1
            {
                cell?.hideBottomLine(false)
            }
            else
            {
                cell?.hideBottomLine(true)
            }
            
        }
        else if (indexPath.section == 1)
        {
            let array = m_settingsArray .objectAtIndex(1)
            cell?.m_titleLbl.text = array.objectAtIndex(indexPath.row) as? String
            if(indexPath.row == 0)
            {
                cell?.frame.size.height = 40
                cell?.showOrHideSwitchBtn(true)
                cell?.m_arrowBtn.hidden = false;
            }
            else if(indexPath.row == 1)
            {
                cell?.frame.size.height = 40
                cell?.showOrHideSwitchBtn(false)
                cell?.m_arrowBtn.hidden = true;
            }
            if indexPath.row == array.count - 1
            {
                cell?.hideBottomLine(false)
            }
            else
            {
                cell?.hideBottomLine(true)
            }
            
        }
        else if (indexPath.section == 2)
        {
            let array = m_settingsArray .objectAtIndex(2)
            cell?.m_titleLbl.text = array.objectAtIndex(indexPath.row) as? String
            cell?.frame.size.height = 40
            cell?.showOrHideSwitchBtn(true)
            cell?.m_arrowBtn.hidden = false;
            if indexPath.row == array.count - 1
            {
                cell?.hideBottomLine(false)
            }
            else
            {
                cell?.hideBottomLine(true)
            }
        }
        else if (indexPath.section == 3)
        {
            let array = m_settingsArray .objectAtIndex(3)
            cell?.m_titleLbl.text = array.objectAtIndex(indexPath.row) as? String
            cell?.frame.size.height = 40
            cell?.showOrHideSwitchBtn(true)
            cell?.m_arrowBtn.hidden = false;
            if indexPath.row == 3
            {
                cell?.hideBottomLine(false)
            }
            else
            {
                cell?.hideBottomLine(true)
            }
            
        }
            
        else{
            
        }
        return cell!
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRectMake(0,0,GRAPHICS.Screen_Width(),30))
        headerView.backgroundColor = UIColor.clearColor()
        
        let titleLabel = UILabel(frame: CGRectMake(10,0,GRAPHICS.Screen_Width(),30))
        titleLabel.font = GRAPHICS.FONT_BOLD(12)
        titleLabel.backgroundColor = UIColor.whiteColor()
        if(section == 0){
            titleLabel.text = Settings_Subscription
        }
        else if(section == 1){
            titleLabel.text = Settings_TitleCaps
        }
        else if(section == 2){
            titleLabel.text = Settings_About
        }
        else if(section == 3){
            titleLabel.text = Settings_ManageProduct
        }
        else{
            titleLabel.text = Settings_LogOut
        }
        
        titleLabel.textColor = UIColor(red: 14/255, green: 14/255, blue: 14/255, alpha: 1)
        headerView .addSubview(titleLabel)
        return headerView
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if(indexPath.section == 0)
        {
            if(indexPath.row == 0){
                //Edit Profile
            }
            if(indexPath.row == 1){
                let memberVC = IBMemberShipPlanViewController()
                memberVC.isFromSettings = true
                self.navigationController!.pushViewController(memberVC, animated: true)
                
                //                let shipping = IBActivityViewController()
                //                self.navigationController?.pushViewController(shipping, animated: true)
            }
            if(indexPath.row == 2)
            {
                let purchasePointsVC = IBPurchasePointsViewController()
                self.navigationController?.pushViewController(purchasePointsVC, animated: true)
            }
        }
        else if (indexPath.section == 1)
        {
            if(indexPath.row == 1)
            {
                //                let purchasePointsVC = IBHomeViewController()
                //                self.navigationController?.pushViewController(purchasePointsVC, animated: true)
            }
            else
            {
                let purchasePointsVC = IBChangePasswordViewController()
                self.navigationController?.pushViewController(purchasePointsVC, animated: true)
            }
            
        }
        else if (indexPath.section == 2)
        {
            if(indexPath.row == 0){
                createAlertForLanguage()
            }
            else if(indexPath.row == 1){
                let shipping = IBPrivacyPolicyViewController()
                self.navigationController?.pushViewController(shipping, animated: true)
            }
            else if(indexPath.row == 2){
                let termsAndConditionVC = IBTermsAndConditionViewController()
                self.navigationController?.pushViewController(termsAndConditionVC, animated: true)
            }
            else if(indexPath.row == 3){
                let shipping = IBContactUsViewController()
                self.navigationController?.pushViewController(shipping, animated: true)
            }
        }
        else if (indexPath.section == 3)
        {
            if(indexPath.row == 0){
                let wishListVC = IBMyWishlistViewController()
                self.navigationController?.pushViewController(wishListVC, animated: true)
            }
            else if(indexPath.row == 1){
                let shipping = IBBillingAndShippingViewController()
                self.navigationController?.pushViewController(shipping, animated: true)
            }
            else if(indexPath.row == 2){
                let termsAndConditionVC = IBBillingAndShippingViewController()
                self.navigationController?.pushViewController(termsAndConditionVC, animated: true)
            }
            
            let array = m_settingsArray .objectAtIndex(3)
            if indexPath.row == array.count - 1
            {
                let alertView = UIAlertView(title: IntoBuy, message: "Are you sure you want to logout?", delegate: self, cancelButtonTitle: "No", otherButtonTitles: "Yes")
                alertView.show()
            }
        }
    }
    
    func addBottomBorder(view: UIView)
    {
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRectMake(0, view.frame.size.height-1.0, view.frame.size.width, 1.0)
        bottomBorder.backgroundColor = UIColor.lightGrayColor().CGColor
        view.layer.addSublayer(bottomBorder)
    }
    
    
    func switchBtnAction(sender:UIButton)
    {
        sender.selected = !sender.selected
    }
    func API_CALLBACK_logout(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"
        {
            let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.logOut()
            PayPalMobile.clearAllUserData()
            
            let userUD = NSUserDefaults()
            userUD.setObject("", forKey: "user_id");
            userUD.removeObjectForKey("userDict")
            userUD.setBool(false, forKey: "isLogged")
            let loginVc = IBLoginViewController()
            self.navigationController!.pushViewController(loginVc, animated: true)
            
        }else{
            showAlert(errorCode);
        }
        
    }
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlert(errorMessage);
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        if(buttonIndex == 1)
        {
            let userIdUD = NSUserDefaults()
            let userIdStr = userIdUD.objectForKey("user_id") as! String;
            
            SwiftLoader.show(title:Loading, animated:true)
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_logout(userIdStr)
            
        }else
        {
            
        }
    }
    func showAlert(msg:String!){
        
        let alert = UIAlertView(title: "IntoBuy", message:msg, delegate: nil, cancelButtonTitle:"Ok")
        alert .show()
        
    }
    
    func createAlertForLanguage()
    {
        languageBGView = UIView(frame : self.view.bounds)
        languageBGView.backgroundColor = UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
        self.m_mainView.addSubview(languageBGView)
        
        let bgImage = GRAPHICS.SETTINGS_LANGUAGE_BG()
        var contentSize = GRAPHICS.getImageWidthHeight(bgImage)
        var width = contentSize.width
        var height = contentSize.height
        var xPos = (languageBGView.frame.size.width - width)/2
        var yPos = (languageBGView.frame.size.height - height)/2
        
        let languageImgView = UIImageView(frame: CGRectMake(xPos, yPos, width, height))
        languageImgView.image = bgImage
        languageImgView.userInteractionEnabled = true
        languageBGView.addSubview(languageImgView)
        
        xPos = languageImgView.bounds.origin.x
        yPos = languageImgView.bounds.origin.y
        height = languageImgView.frame.size.height/4
        let titleLbl = UILabel(frame: CGRectMake(xPos, yPos, width, height))
        titleLbl.text = String(format: "%@:",Settings_selectLanguage)
        titleLbl.textAlignment = .Center
        titleLbl.font = GRAPHICS.FONT_REGULAR(14)
        titleLbl.textColor = UIColor.whiteColor()
        languageImgView.addSubview(titleLbl)
        
        let btnImg = GRAPHICS.SETTINGS_LANGUAGE_BTN_NORMAL()
        let btnImgSel = GRAPHICS.SETTINGS_LANGUAGE_BTN_SELECTED()
        contentSize = GRAPHICS.getImageWidthHeight(btnImg)
        yPos = titleLbl.frame.maxY + 20
        for i in 0 ..< 2 {
            width = contentSize.width
            height = contentSize.height
            xPos = languageImgView.bounds.origin.x + 15
            let radioBtn = UIButton(frame: CGRectMake(xPos, yPos, width, height))
            radioBtn.setBackgroundImage(btnImg, forState: .Normal)
            radioBtn.setBackgroundImage(btnImgSel, forState: .Selected)
            radioBtn.addTarget(self, action: #selector(IBSettingsViewController.radioBtnAction(_:)), forControlEvents: .TouchUpInside)
            radioBtn.tag = i
            languageImgView.addSubview(radioBtn)
            
            xPos = xPos + width + 15
            width = languageImgView.frame.size.width/2
            
            let languageLbl = UILabel(frame: CGRectMake(xPos, yPos, width, height))
            if i == 0 {
                languageLbl.text = "English"
            }
            else
            {
                languageLbl.text = "Chinese(Simplified)"
            }
            languageLbl.font = GRAPHICS.FONT_REGULAR(12)
            languageImgView.addSubview(languageLbl)
            yPos = radioBtn.frame.maxY + 25
        }
        
    }
    func radioBtnAction(sender : UIButton) -> () {
        for button in languageBGView.subviews {
            if button .isKindOfClass(UIButton.self) {
                let buttn = button as! UIButton
                buttn.selected = false
            }
        }
        sender.selected = true
        UIView.animateWithDuration(0.50, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.languageBGView.removeFromSuperview()
            }, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
