//
//  IBLoginViewController.swift
//  IntoBuy
//
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit


class IBLoginViewController: IBBaseViewController , UITextFieldDelegate,ServerAPIDelegate {

    var loginView : FBSDKLoginButton = FBSDKLoginButton()
    
    let m_loginBGView:UIImageView = UIImageView()
    let m_loginScrlView:UIScrollView = UIScrollView()
    let m_logoImgView:UIImageView = UIImageView()
    let m_userNameTF:UITextField = UITextField()
    let m_pwdTF:UITextField = UITextField()
    var m_forgotBgView:UIView!
    var bgView:UIImageView!
    var m_emailTf:UITextField!
    var keyBoardTool:UIToolbar!
    var isFromFb:Bool!
    var m_countryArray = NSMutableArray()
    var m_faceBookDict = NSDictionary()
    var userNameStr = ""
    var regBgView:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()


        
        // Do any additional setup after loading the view.
        self.m_bottomView.hidden = true

        self.hideHeaderViewForView(true)
        self .controlsForLogin()
        
    }
    func controlsForLogin()
    {
        m_loginBGView.frame = self.m_bgImageView.frame;
        m_loginBGView.image = GRAPHICS.LOGIN_BG_IMAGE()
        m_loginBGView.userInteractionEnabled = true
        self.m_bgImageView .addSubview(m_loginBGView)
        //print(m_loginBGView.frame)
        
        m_loginScrlView.frame = m_loginBGView.frame
        m_loginBGView .addSubview(m_loginScrlView)
        
        let logoImage = GRAPHICS.LOGIN_LOGO_IMAGE()
        var width = (logoImage?.size.width)!/2;
        var height = (logoImage?.size.height)!/2
        var yPadding:CGFloat = 70
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (logoImage?.size.width)!/1.7
            height = (logoImage?.size.height)!/1.7
            yPadding = 100
        }
        var xPos = (m_loginScrlView.frame.size.width - width)/2
        var yPos = m_loginScrlView.bounds.origin.y + yPadding
        
        m_logoImgView.frame = CGRectMake(xPos, yPos, width, height)
        m_logoImgView.image = logoImage
        m_loginScrlView .addSubview(m_logoImgView)
        
        xPos = GRAPHICS.Screen_X() + 47
        yPos = m_logoImgView.frame.maxY + 70
        width = GRAPHICS.Screen_Width() - 80
        height = 23
        let userNameLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        userNameLbl.text = UserName
        userNameLbl.textColor = UIColor.whiteColor()
        userNameLbl.font = GRAPHICS.FONT_REGULAR(12)
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            userNameLbl.font = GRAPHICS.FONT_REGULAR(14)
        }
        m_loginScrlView .addSubview(userNameLbl)
        
        let textBoxImg = GRAPHICS.LOGIN_TEXTBOX_IMAGE()
        yPos = userNameLbl.frame.maxY
        width = (textBoxImg?.size.width)!/2
        height = (textBoxImg?.size.height)!/2
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            //height = 30
            width = (textBoxImg?.size.width)!/1.8
            height = (textBoxImg?.size.height)!/1.8
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (textBoxImg?.size.width)!/1.5
            height = (textBoxImg?.size.height)!/1.5
        }
        xPos = (m_loginScrlView.frame.size.width - width)/2
        m_userNameTF.frame = CGRectMake(xPos, yPos, width, height)
        m_userNameTF.background = textBoxImg
        m_userNameTF.returnKeyType = UIReturnKeyType.Next
        m_userNameTF.font = GRAPHICS.FONT_REGULAR(14)
        m_userNameTF.inputAccessoryView = createKeyboardToolBar()
        m_userNameTF.delegate = self
        m_userNameTF.autocorrectionType = .No
        
        self.setLeftViewToTheTextField(m_userNameTF)
        m_loginScrlView .addSubview(m_userNameTF)
        
        yPos = m_userNameTF.frame.maxY + 15
        height = 23
        xPos = GRAPHICS.Screen_X() + 47
        let pwdLbl = UILabel(frame: CGRectMake(xPos, yPos, width, height))
        pwdLbl.text = PassWord
        pwdLbl.textColor = UIColor.whiteColor()
        pwdLbl.font = GRAPHICS.FONT_REGULAR(12)
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            pwdLbl.font = GRAPHICS.FONT_REGULAR(14)
        }

        m_loginScrlView.addSubview(pwdLbl)
        
        yPos = pwdLbl.frame.maxY
        height = 25
        width = (textBoxImg?.size.width)!/2
        height = (textBoxImg?.size.height)!/2
        xPos = (m_loginScrlView.frame.size.width - width)/2
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            //height = 30
            width = (textBoxImg?.size.width)!/1.8
            height = (textBoxImg?.size.height)!/1.8
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (textBoxImg?.size.width)!/1.5
            height = (textBoxImg?.size.height)!/1.5
        }

        xPos = (m_loginScrlView.frame.size.width - width)/2
        m_pwdTF.frame = CGRectMake(xPos, yPos, width, height)
        m_pwdTF.background = textBoxImg
        m_pwdTF.returnKeyType = UIReturnKeyType.Done
        m_pwdTF.secureTextEntry = true
        m_pwdTF.font = GRAPHICS.FONT_REGULAR(14)
        m_pwdTF.delegate = self
        m_pwdTF.inputAccessoryView = createKeyboardToolBar()
        self.setLeftViewToTheTextField(m_pwdTF)
        m_loginScrlView .addSubview(m_pwdTF)
        
        
        userNameLbl.frame.origin.x = m_userNameTF.frame.origin.x + 8
        pwdLbl.frame.origin.x = userNameLbl.frame.origin.x
        
        yPos = m_pwdTF.frame.maxY+5
        height = 15
        width = m_pwdTF.frame.size.width/2.4
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            width = m_pwdTF.frame.size.width/2.7
            height = 20
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = m_pwdTF.frame.size.width/3.1
            height = 20
        }
        xPos = userNameLbl.frame.origin.x
        let forgotPwdBtn = UIButton(frame: CGRectMake(xPos, yPos, width, height))
        forgotPwdBtn .setTitle(ForgotPassword, forState: UIControlState.Normal)
        forgotPwdBtn .setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        forgotPwdBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(12)
        forgotPwdBtn.addTarget(self, action: #selector(IBLoginViewController.forgotPasswordBtnTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        m_loginScrlView .addSubview(forgotPwdBtn)
        self.addBottomBorder(forgotPwdBtn)
        
        self .createSigninAndRegisterButtons(forgotPwdBtn.frame.maxY + 25)
        
        //--------------- Requesting for camer access authorization ------------------------//
        
        if AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) ==  AVAuthorizationStatus.Authorized
        {
            // Already Authorized
        }
        else
        {
            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    // User granted
                }
                else
                {
                    // User Rejected
                }
            });
        }
        
        //--------------- Requesting for camer access authorization ------------------------//
    
    }
    func createSigninAndRegisterButtons(posY : CGFloat)
    {
        let signInImg = GRAPHICS.LOGIN_SIGNIN_BTN_IMAGE();
//        var width = GRAPHICS.Screen_Width() - 80
//        var height:CGFloat = 25
        var width = (signInImg?.size.width)!/2
        var height:CGFloat = (signInImg?.size.height)!/2

        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            width = (signInImg?.size.width)!/1.8
            height = (signInImg?.size.height)!/1.8

        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (signInImg?.size.width)!/1.5
            height = (signInImg?.size.height)!/1.5
        }

        var xPos = m_userNameTF.frame.origin.x;//(m_loginScrlView.frame.size.width - width)/2
        let signInBtn = UIButton(frame: CGRectMake(xPos, posY, width, height))
        signInBtn.setBackgroundImage(signInImg, forState: UIControlState.Normal)
        signInBtn.addTarget(self, action: #selector(IBLoginViewController.signInBtnTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        m_loginScrlView .addSubview(signInBtn)
        
        
        
        let fbBtnImg = GRAPHICS.LOGIN_FB_SIGNIN_BTN_IMAGE()
        var yPos = posY + height + 15
        let faceBookBtn = UIButton(frame: CGRectMake(xPos, yPos, width, height))
        faceBookBtn.setBackgroundImage(fbBtnImg, forState: .Normal)
        faceBookBtn.addTarget(self, action: #selector(IBLoginViewController.faceBookBtnTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        m_loginScrlView .addSubview(faceBookBtn)
        
        let regBgImg = GRAPHICS.LOGIN_SIGNUP_BG_IMAGE()
        var padding:CGFloat!
        if(GRAPHICS.Screen_Type() == IPHONE_5 || GRAPHICS.Screen_Type() == IPHONE_4)
        {
             padding = 65;
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
             padding = 103
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            padding = 150
        }


        yPos = yPos + height + padding
        width = GRAPHICS.Screen_Width()
        height = (regBgImg?.size.height)!/2.0;
        xPos = GRAPHICS.Screen_X()
        regBgView = UIImageView(frame: CGRectMake(xPos, yPos, width, height))
        regBgView.image = regBgImg
        regBgView.userInteractionEnabled = true
        m_loginScrlView.addSubview(regBgView)
        
        xPos = GRAPHICS.Screen_Width()/4
        height = 20
        yPos = (regBgView.frame.size.height - height)/2
        width = 130
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            xPos = GRAPHICS.Screen_Width()/4.5
            width = 150
        }

        let haveAccLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        haveAccLbl.text = DontHaveAnAccount
        haveAccLbl.textColor = UIColor.blackColor()
        haveAccLbl.font = GRAPHICS.FONT_REGULAR(12)
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            haveAccLbl.font = GRAPHICS.FONT_REGULAR(14)
        }

        regBgView .addSubview(haveAccLbl)
        
        xPos = haveAccLbl.frame.maxX
        width = 50
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = 55
        }
        let registerBtn = UIButton(frame: CGRectMake(xPos, yPos, width, height))
        registerBtn.setTitle(SignUp, forState: .Normal)
        registerBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            registerBtn.titleLabel!.font = GRAPHICS.FONT_REGULAR(15)
        }

        registerBtn.setTitleColor(UIColor(red: 42/255, green: 148/255, blue: 220/255, alpha: 1), forState: .Normal)
        registerBtn.addTarget(self, action: #selector(IBLoginViewController.registerBtnTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        regBgView .addSubview(registerBtn)
        
        
        m_loginScrlView.contentSize = CGSizeMake(m_loginScrlView.frame.size.width, regBgView.frame.maxY)
    }
    func addBottomBorder(view: UIView)
    {
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRectMake(0, view.frame.size.height-1.0, view.frame.size.width, 1.0)
        bottomBorder.backgroundColor = UIColor.whiteColor().CGColor
        view.layer.addSublayer(bottomBorder)
    }
    
    func registerBtnTapped(sender: UIButton)
    {
        
        let mydiarycalendar = IBRegistrationViewController()
        self.navigationController?.pushViewController(mydiarycalendar, animated: true)
    }

        func fetchUserInfo(){
                if((FBSDKAccessToken.currentAccessToken()) != nil){
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, gender"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                        if (error == nil){
                            //print(result)
                            
                            self.userNameStr = result.valueForKey("name") as! String
                            self.m_faceBookDict = (result as! NSDictionary?)!
                            SwiftLoader.show(animated: true)
                            SwiftLoader.show(title:"Loading...", animated:true)
                            let serverApi = ServerAPI()
                            serverApi.delegate = self
                            serverApi.API_callRegisterWebService(result.valueForKey("first_name") as! String, LastName: result.valueForKey("last_name") as! String, Email: result.valueForKey("email") as! String, PhoneNo: "", password: "", DeviceId: "", Device: "", Gender: result.valueForKey("gender") as! String, DOB: "", UserName: result.valueForKey("name") as! String, Country: "0", AboutMe: "", UserType: "5", FaceBookId: result.valueForKey("id") as! String)
                            
                            
                            
                        }
                        
                        
                    })
                }
                
              

                
            }

    func signInBtnTapped(sender: UIButton)
    {
        let whitespaceSet = NSCharacterSet.whitespaceCharacterSet()
        isFromFb = false
        if m_userNameTF.text == "" || m_userNameTF.text!.stringByTrimmingCharactersInSet(whitespaceSet) == ""
        {
            let alertView = UIAlertView();
            alertView.addButtonWithTitle("Ok");
            alertView.title = IntoBuy;
            alertView.message = "Please enter Username";
            alertView.show();
            return
        }
        else if m_pwdTF.text == "" || m_pwdTF.text!.stringByTrimmingCharactersInSet(whitespaceSet) == ""
        {
            let alertView = UIAlertView();
            alertView.addButtonWithTitle("Ok");
            alertView.title = "Into Buy";
            alertView.message = "Please enter password";
            alertView.show();
            return
        }
        else
        {
            SwiftLoader.show(animated: true)
            SwiftLoader.show(title:"Loading...", animated:true)

            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_login(m_userNameTF.text!, password: m_pwdTF.text!, device_id: "123132123", device: "IOS")

        }

    }
    func faceBookBtnTapped(sender: UIButton)
    {
        
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.loginBehavior = FBSDKLoginBehavior.Web
        
        fbLoginManager.logInWithReadPermissions(["email"], fromViewController: self.parentViewController,
        
        handler: { (response:FBSDKLoginManagerLoginResult!, error: NSError!) in
            ////print(error);
        if(error != nil){
        // Handle error
        }
        else if(response.isCancelled){
        // Authorization has been canceled by user
        }
        else {
        // Authorization successful
        // println(FBSDKAccessToken.currentAccessToken())
        // no longer necessary as the token is already in the response
        
        [self .fetchUserInfo()];
            
        //print(response.token.tokenString)
        }
        })
        

    }
    func forgotPasswordBtnTapped(sender: UIButton)
    {
       m_userNameTF.resignFirstResponder()
       m_pwdTF.resignFirstResponder()
       createControlsForForgotPassword()
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        self.view.endEditing(true)
    }
    // textfield delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == m_userNameTF)
        {
            m_userNameTF .resignFirstResponder()
            m_pwdTF.becomeFirstResponder()
        }
        else if(textField == m_pwdTF){
            m_pwdTF .resignFirstResponder()
            m_loginScrlView.setContentOffset(CGPointMake(0, 0), animated: true)
        }
        if(m_emailTf != nil)
        {
            m_emailTf.resignFirstResponder()
        }

        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        m_loginScrlView.contentSize = CGSizeMake(m_loginScrlView.frame.size.width, regBgView.frame.maxY + 100)
            if(textField.frame.origin.y > self.m_bgImageView.frame.size.height - 280){
            var contentOffset:CGFloat!
            if(GRAPHICS.Screen_Type() == IPHONE_4){
                contentOffset = 120.0
            }
            else if(GRAPHICS.Screen_Type() == IPHONE_5){
                contentOffset = 80.0;
            }
            m_loginScrlView.setContentOffset(CGPointMake(0, (m_loginScrlView.frame.origin.y + contentOffset)), animated: true)
        }
        if(m_emailTf != nil)
        {
            bgView.frame.origin.y = bgView.frame.origin.y - 50;
            let Btn = m_forgotBgView.viewWithTag(1) as! UIButton
            Btn.frame.origin.y = bgView.frame.origin.y - Btn.frame.size.height + 5
            
        }

        return true
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        m_loginScrlView.contentSize = CGSizeMake(m_loginScrlView.frame.size.width, regBgView.frame.maxY)
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if(m_emailTf != nil)
        {
            bgView.frame.origin.y = (m_forgotBgView.frame.size.height - bgView.frame.height)/2
            let Btn = m_forgotBgView.viewWithTag(1) as! UIButton
            Btn.frame.origin.y = bgView.frame.origin.y - Btn.frame.size.height + 5
        }
    }
    func setLeftViewToTheTextField(textField: UITextField)
    {
        let leftPlaceHolderView = UIView(frame: CGRectMake(0, 0, 10,textField.frame.size.height))
        leftPlaceHolderView.backgroundColor = UIColor.clearColor()
        textField.leftView = leftPlaceHolderView
        textField.leftViewMode = UITextFieldViewMode.Always
    }
    
    func createControlsForForgotPassword()
    {
        var xPos = GRAPHICS.Screen_X()
        var yPos = GRAPHICS.Screen_Y()
        var width = GRAPHICS.Screen_Width()
        var height = GRAPHICS.Screen_Height()
        
        m_forgotBgView = UIView(frame: CGRectMake(xPos,yPos,width,height))
        m_forgotBgView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.5)
        self.m_mainView.addSubview(m_forgotBgView)
        
        height = GRAPHICS.Screen_Width()/2
        yPos = (GRAPHICS.Screen_Height() - height)/2
        xPos = GRAPHICS.Screen_X() + 30
        width = GRAPHICS.Screen_Width() - 60
        bgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        bgView.backgroundColor = UIColor.grayColor()
        bgView.userInteractionEnabled = true
        bgView.layer.borderWidth = 1.0
        bgView.layer.borderColor = UIColor.blackColor().CGColor
        bgView.image = GRAPHICS.LOGIN_BACKGROUND_IMAGE()
        m_forgotBgView.addSubview(bgView)
        
        height = 30
        yPos = 0
        xPos = bgView.bounds.origin.x + 20
        width = bgView.frame.size.width - 40
        let titleLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        titleLbl.text = "Forgot Password"
        titleLbl.textAlignment = NSTextAlignment.Center
        titleLbl.font = GRAPHICS.FONT_REGULAR(14)
        bgView .addSubview(titleLbl)
        
        xPos = bgView.bounds.origin.x + 30
        width = bgView.frame.size.width - 60
        height = 15
        yPos = titleLbl.frame.maxY+20
        let emailLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        emailLbl.text = "Enter your Email"
        emailLbl.font = GRAPHICS.FONT_REGULAR(14)
        bgView .addSubview(emailLbl)
        
        let txtImg = GRAPHICS.LOGIN_TEXTBOX_IMAGE()
        width = (txtImg?.size.width)!/2
        height = (txtImg?.size.height)!/2
        xPos = (m_loginScrlView.frame.size.width - width)/2
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            //height = 30
            width = (txtImg?.size.width)!/1.8
            height = (txtImg?.size.height)!/1.8
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (txtImg?.size.width)!/1.5
            height = (txtImg?.size.height)!/1.5
        }
        xPos = (bgView.frame.size.width - width)/2
        yPos = (bgView.frame.size.height - height)/2 //titleLbl.frame.maxY+10
        
        m_emailTf = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_emailTf.delegate = self
        m_emailTf.background = txtImg
        m_emailTf.font = GRAPHICS.FONT_REGULAR(14)
        m_emailTf.keyboardType = UIKeyboardType.EmailAddress;
        m_emailTf.returnKeyType = .Done
        setLeftViewToTheTextField(m_emailTf)
        bgView.addSubview(m_emailTf)
        
        let submitImg = GRAPHICS.SETTINGS_BILLING_BTN_IMAGE()
        width = (submitImg.size.width)/2
        height = (submitImg.size.height)/2
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            //height = 30
            width = (submitImg.size.width)/1.8
            height = (submitImg.size.height)/1.8
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (submitImg.size.width)/1.5
            height = (submitImg.size.height)/1.5
        }

        xPos = (bgView.frame.size.width - width)/2
        yPos = bgView.frame.size.height - height - 10
        let submitBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        submitBtn.setBackgroundImage(submitImg, forState: UIControlState.Normal)
        submitBtn.setTitle("Submit", forState: UIControlState.Normal)
        submitBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        submitBtn .addTarget(self, action: #selector(IBLoginViewController.submitBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        bgView.addSubview(submitBtn)
        
        
        let closeBtnImg = GRAPHICS.LOGIN_POPUP_CLOSE_BTN_IMAGE()
        width = closeBtnImg!.size.width/2
        height = closeBtnImg!.size.height/2
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            //height = 30
            width = (closeBtnImg?.size.width)!/1.8
            height = (closeBtnImg?.size.height)!/1.8
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (closeBtnImg?.size.width)!/1.5
            height = (closeBtnImg?.size.height)!/1.5
        }

        xPos = bgView.frame.size.width + 15
        yPos = bgView.frame.origin.y - height/2
        let closeBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        closeBtn.setBackgroundImage(closeBtnImg, forState: .Normal)
        closeBtn.layer.cornerRadius = height/2
        closeBtn .addTarget(self, action: #selector(IBLoginViewController.closeBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        closeBtn.tag = 1;
        m_forgotBgView.addSubview(closeBtn)
    }
    func closeBtnAction()
    {
        m_forgotBgView.removeFromSuperview()
    }
    func submitBtnAction()
    {
        m_emailTf .resignFirstResponder()
    
        if m_emailTf.text != ""
        {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
      //  let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_forgotpassword(m_emailTf.text!)
        }
        else
        {
        showAlertViewWithMessage(login_email_alert)
        
        }

    }
    // toolbar 
    // method for keyboard toolbar
    func createKeyboardToolBar() -> UIToolbar
    {
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        keyBoardTool = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        keyBoardTool!.barStyle = UIBarStyle.BlackOpaque
        keyBoardTool!.translucent = true
        keyBoardTool!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        keyBoardTool!.sizeToFit()
        
        
        // let doneButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("nextAction"))
        let previousImage = GRAPHICS.PICKER_LEFT_ARROW_BTN()
        let nextImage = GRAPHICS.PICKER_RIGHT_ARROW_BTN()
        
        let prev_Btn = UIButton ()
        prev_Btn.frame = CGRectMake(10, 8, 40, 30)
        prev_Btn.backgroundColor =  UIColor.clearColor();
        prev_Btn.setBackgroundImage(previousImage, forState: .Normal)
        prev_Btn.addTarget(self, action: #selector(IBLoginViewController.previousAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(prev_Btn)
        
        let next_Btn = UIButton ()
        next_Btn.frame = CGRectMake(50, 8, 40, 30)
        next_Btn.backgroundColor =  UIColor.clearColor();
        next_Btn.setBackgroundImage(nextImage, forState: .Normal)
        next_Btn.addTarget(self, action: #selector(IBLoginViewController.nextAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(next_Btn)
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBLoginViewController.doneAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(done_Btn)
        
        keyBoardTool!.userInteractionEnabled = true
        return keyBoardTool
        
    }
    func doneAction()
    {
//    if(keyBoardTool != nil){
//        keyBoardTool .removeFromSuperview()
//        keyBoardTool = nil
//    }
        if m_userNameTF.isFirstResponder() == true
        {
            m_userNameTF.resignFirstResponder()
            m_loginScrlView.setContentOffset(CGPointMake(0, 0), animated: true)
        }
        else if m_pwdTF.isFirstResponder() == true
        {
            m_pwdTF.resignFirstResponder()
            m_loginScrlView.setContentOffset(CGPointMake(0, 0), animated: true)
        }

    
    }
    func nextAction()
    {
        if m_userNameTF.isFirstResponder() == true
        {
            m_userNameTF.resignFirstResponder()
            m_pwdTF.becomeFirstResponder()
        }
        else
        {
            m_pwdTF.resignFirstResponder()
            m_loginScrlView.setContentOffset(CGPointMake(0, 0), animated: true)
        }
    }
    
    func previousAction()
    {
        if m_userNameTF.isFirstResponder() == true
        {
            m_userNameTF.resignFirstResponder();
            m_loginScrlView.setContentOffset(CGPointMake(0, 0), animated: true)
        }
        else if m_pwdTF.isFirstResponder() == true
        {
            m_pwdTF.resignFirstResponder()
            m_userNameTF.becomeFirstResponder()
            
        }
        
    }
    
    // MARK: Api delegate
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        if(m_forgotBgView != nil ){
            m_forgotBgView.removeFromSuperview()
            m_forgotBgView = nil
        }
        showAlert(errorMessage);
        
    }
    func API_CALLBACK_forgotpassword(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = (resultDict.objectForKey("error_code") as? String)!
        m_forgotBgView.removeFromSuperview()
        if errorCode == "1"
        {
         showAlert(resultDict .objectForKey("result")as? String!)
        }
        else{
            showAlert(resultDict .objectForKey("msg")as? String!)
        }
    }
    func API_CALLBACK_login(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = (resultDict.objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
            let resultArray = (resultDict .objectForKey("result")as? NSArray)
            let resultDictVal = resultArray?.objectAtIndex(0) as! NSDictionary
            let userDefaults = NSUserDefaults()
            userDefaults .setValue((resultDictVal .objectForKey("userId")as? String)!, forKey: "user_id")
           
            userDefaults .setValue(resultDictVal, forKey: "userDict")
            let userIdStr = (resultDictVal .objectForKey("userId")as? String)!
            saveUserIdInUserDefaults(userIdStr)
            saveUserNameInUserDefaults((resultDictVal .objectForKey("username")as? String)!)
            saveUserTypeinUserdefaults((resultDictVal .objectForKey("userType")as? String)!)
            saveCountryCode((resultDictVal .objectForKey("country")as? String)!)
            saveCountryName((resultDictVal .objectForKey("CountryName")as? String)!)
            if((resultDictVal .objectForKey("userType")as? String)  == "5")
            {
                let selectProfileVC = IBSelectProfileViewController()
                self.navigationController?.pushViewController(selectProfileVC, animated: true)
            }
            else{
                userDefaults.setBool(true, forKey: "isLogged")
                let HomeScreenVC = IBHomePageVcViewController()
                self.navigationController?.pushViewController(HomeScreenVC, animated: true)
            }
        }
        else{
            if (resultDict.objectForKey("msg")as? String)! == "Invalid credentials"
            {
                showAlert("Wrong Email Address or Password. Please try again.")
            }
            else
            {
                showAlert((resultDict.objectForKey("msg")as? String)!)
            }
        }
    }
    func API_CALLBACK_SignUp(resultDict: NSDictionary)
    {
        
        SwiftLoader.hide()
        let errorCode = (resultDict .objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
            let userDefaults = NSUserDefaults()
            let loginArray = (resultDict.objectForKey("result")as! NSArray)
            let loginDict = loginArray.objectAtIndex(0) as! NSDictionary
            userDefaults.setObject(loginDict.objectForKey("userId")as! String, forKey: "user_id")
            
            userDefaults .setValue(loginDict, forKey: "userDict")
            
            let userIdStr = (loginDict .objectForKey("userId")as? String)!
            saveUserIdInUserDefaults(userIdStr)
            saveUserNameInUserDefaults((loginDict .objectForKey("username")as? String)!)
            saveUserTypeinUserdefaults((loginDict .objectForKey("userType")as? String)!)
            saveCountryCode((loginDict .objectForKey("country")as? String)!)
            saveCountryName((loginDict .objectForKey("CountryName")as? String)!)
            if((loginDict .objectForKey("userType")as? String)  == "5"){
                let mydiarycalendar = IBSelectProfileViewController()
                self.navigationController?.pushViewController(mydiarycalendar, animated: true)
            }
            else{
                userDefaults.setBool(true, forKey: "isLogged")
                let mydiarycalendar = IBHomePageVcViewController()
                self.navigationController?.pushViewController(mydiarycalendar, animated: true)
                
            }

        }
        else{
            let msgStr = (resultDict .objectForKey("msg")as? String)!
            if msgStr == "This username already exists."
            {
                showAlert("This username already exists.")
            }
            else{
            
            showAlert((resultDict .objectForKey("msg")as? String)!)
            }
        }
    }
    
    func showAlert(msg:String!){
        
        let alert = UIAlertView(title: "IntoBuy", message:msg, delegate: nil, cancelButtonTitle:"Ok")
        alert .show()
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
