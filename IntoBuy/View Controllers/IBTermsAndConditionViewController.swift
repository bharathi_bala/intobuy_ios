//
//  IBTermsAndConditionViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 20/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBTermsAndConditionViewController: IBBaseViewController {
    
    let termsTextView = UITextView()
    
    override func viewDidLoad()
    {
        bottomVal = 5
        toShowActivityBtn = true
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.m_settingsButton.hidden = true
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_titleLabel.text = Settings_Title
        createControlsForTermsAndConditions()
    }
    func createControlsForTermsAndConditions()
    {
        let xPos = GRAPHICS.Screen_X() + 10
        var yPos = self.m_bgImageView.bounds.origin.y
        let width = GRAPHICS.Screen_Width() - 20
        var height = CGFloat(30)
        let titleLbl = UILabel(frame :CGRectMake(xPos,yPos,width,height))
        titleLbl.text = "Terms and Conditions"
        titleLbl.font = GRAPHICS.FONT_BOLD(12)
        self.m_bgImageView.addSubview(titleLbl)
        
        yPos = titleLbl.frame.maxY
        height = self.m_bgImageView.frame.size.height - height
        termsTextView.frame = CGRectMake(xPos,yPos,width,height)
        termsTextView.editable = false
        termsTextView.text = ""
        
        self.m_bgImageView.addSubview(termsTextView)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
