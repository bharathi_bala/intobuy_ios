
//
//  IBSplashViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 10/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBSplashViewController: IBBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.hideBottomView(true)
        self.hideHeaderViewForView(true)
        
        let splashImg = GRAPHICS.SPLASH_IMAGE()
        let splashImgView = UIImageView(frame: CGRectMake(GRAPHICS.Screen_X(), GRAPHICS.Screen_Y(), GRAPHICS.Screen_Width(), GRAPHICS.Screen_Height()))
        splashImgView.image = splashImg
        self.m_bgImageView.addSubview(splashImgView)
        
        //self.performSelector("moveTo")
        self.performSelector(#selector(IBSplashViewController.moveTo), withObject: nil, afterDelay: 0.9)
    }
    func moveTo()
    {
        let userDefaults = NSUserDefaults()
       // let userIdStr = getUserIdFromUserDefaults();
        if userDefaults .boolForKey("isLogged") == true
        {
            let homeScr = IBHomePageVcViewController()
            self.navigationController!.pushViewController(homeScr, animated: true)
        }
        else{
            let loginVC = IBLoginViewController()
            self.navigationController?.pushViewController(loginVC, animated: true)
        }
    
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
