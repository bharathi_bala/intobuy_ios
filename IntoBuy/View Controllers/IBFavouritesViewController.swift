//
//  IBFavouritesViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 21/04/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBFavouritesViewController: IBBaseViewController,UITableViewDataSource,UITableViewDelegate,MGSwipeTableCellDelegate {

    var m_tableView:UITableView = UITableView()
    var m_priceArray:NSMutableArray!
    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.m_titleLabel.text = Favourites_Title
        m_priceArray = ["10.00","20.00","30.00","40.00"]
        self.initControls()
        
    }
    func initControls()
    {
        m_tableView.frame = CGRectMake(0, 0, GRAPHICS.Screen_Width(), self.m_bgImageView.frame.size.height - self.m_bottomView.frame.size.height)
        m_tableView.dataSource = self
        m_tableView.delegate = self
        self.m_bgImageView.addSubview(m_tableView)
    }
    //Tableview datasource and delagte methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return m_priceArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cellIdentifier = "Cell"
        
        var cell:IBListCartTableCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBListCartTableCell
        
        if (cell == nil) {
            cell = IBListCartTableCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        
        tableView.frame.size.height = max(10,tableView.contentSize.height)
        cell?.m_priceAmountLbl.text = m_priceArray.objectAtIndex(indexPath.row)as? String
        //m_downview.frame.origin.y = tableView.frame.maxY
        cell?.delegate = self
        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        
        cell?.showORhideCartButton(false)
        
        return cell!
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 110
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        m_priceArray.removeObjectAtIndex(indexPath.row)
        m_tableView.reloadData()
    }
    
    //MARK:- mgswipe tablew cell
    func swipeTableCell(cell: MGSwipeTableCell!, canSwipe direction: MGSwipeDirection) -> Bool {
        return true
    }
    func swipeTableCell(cell: MGSwipeTableCell!, swipeButtonsForDirection direction: MGSwipeDirection, swipeSettings: MGSwipeSettings!, expansionSettings: MGSwipeExpansionSettings!) -> [AnyObject]! {
        
        swipeSettings.transition = .Border;
        expansionSettings.buttonIndex = 0;
        //  __weak MSSendNotificationViewController * me = self;
        
        //let   me =  IBFavouritesViewController()
        //    MSNotificationEntity *entity = [m_notificationsArray objectAtIndex:[m_notificationTableView indexPathForCell:cell].row];
        
        if (direction == .LeftToRight) {
            
        }
        else {
            
            expansionSettings.fillOnTrigger = true;
            expansionSettings.threshold = 1.1;
            // Add a remove button to the cell
            let removeButton = MGSwipeButton(title: "Remove", backgroundColor: UIColor.redColor(), callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                //print("price array is",(self.m_priceArray))
                let indexPath = self.m_tableView.indexPathForCell(sender);
                self.deleteAction(indexPath!.row)
                return true
            })
            cell?.rightButtons = [removeButton]
            return [removeButton];
        }
        
        return nil
    }
    func swipeTableCell(cell: MGSwipeTableCell!, didChangeSwipeState state: MGSwipeState, gestureIsActive: Bool) {
        
    }
    func deleteAction(tag : Int)
    {
        //print(tag)
        //print(m_priceArray)
        //m_tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
        m_priceArray.removeObjectAtIndex(tag)
        m_tableView.reloadData()
        
    }

    
    
}
