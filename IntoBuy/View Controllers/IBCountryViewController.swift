//
//  IBCountryViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 08/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

public protocol CountryProtocol : NSObjectProtocol
{
    func sendArrayToPreviousVC(myArray: NSArray)
}

class IBCountryViewController: IBBaseViewController,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,ServerAPIDelegate {

    var m_searchBar:UISearchBar!
    var m_countryTblView:UITableView!
    var m_countryArray:NSMutableArray!
    var m_filterArray = NSMutableArray()
    var mDelegate:CountryProtocol?
    var type:String!
    var m_fbDict = NSDictionary()
    var isFromRegistration:Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(true)
        self.m_settingsButton.hidden = true
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_titleLabel.text = "Select"
        type = ""

        let serverApi = ServerAPI()
        serverApi.delegate = self
        createControlsForSearch()
    }
    func createControlsForSearch()
    {
        var xPos = GRAPHICS.Screen_X()
        var yPos = self.m_bgImageView.bounds.origin.y
        var width = GRAPHICS.Screen_Width()
        var height:CGFloat = 60
        let searchView = UIView(frame: CGRectMake(xPos,yPos,width,height))
        searchView.backgroundColor = UIColor.grayColor()
        self.m_bgImageView.addSubview(searchView)
        
        xPos = searchView.bounds.origin.x + 10
        yPos = searchView.bounds.origin.y + 10
        width = searchView.frame.size.width - 20
        height = searchView.frame.size.height - 20
        
        m_searchBar = UISearchBar(frame: CGRectMake(xPos,yPos,width,height))
        m_searchBar.delegate = self;
        m_searchBar.backgroundColor = UIColor.grayColor()
        m_searchBar.setBackgroundImage(GRAPHICS.SEARCH_TEXTBOX_IMAGE(), forBarPosition: .Top, barMetrics: .Default)
        m_searchBar.searchBarStyle = .Minimal
        m_searchBar.barStyle = .Default
        m_searchBar.placeholder = "Search Country"
        m_searchBar.translucent = false;
        m_searchBar.opaque = false
        searchView.addSubview(m_searchBar)
        
        xPos = GRAPHICS.Screen_X()
        yPos = searchView.frame.maxY
        width = GRAPHICS.Screen_Width()
        height = self.m_bgImageView.frame.size.height - searchView.frame.size.height
        m_countryTblView = UITableView(frame: CGRectMake(xPos,yPos,width,height))
        m_countryTblView.delegate = self
        m_countryTblView.dataSource = self
        self.m_bgImageView.addSubview(m_countryTblView)
        
    }
    // MARK: - search bar delegate
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        return true
    }
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        
        searchBar .setShowsCancelButton(true, animated: true)
    }
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar .setShowsCancelButton(false, animated: true)
    }
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(false, animated: true)
        return true
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        type = "alllist";
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar .resignFirstResponder()
        m_countryTblView.reloadData()
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        type = "searching"
        if searchBar.text == ""
        {
            
        }
        else{
            let searchPredicate = NSPredicate(format: "cntrName BEGINSWITH[cd] %@", searchText as String!)
            let array = m_countryArray.filteredArrayUsingPredicate(searchPredicate)
            //print(array)
            m_filterArray .removeAllObjects()
            m_filterArray .addObjectsFromArray(array)
            m_countryTblView.reloadData()
        }

        
    }
    // MARK: - table view delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if type == "searching"
        {
            return m_filterArray.count
        }
        else{
        return m_countryArray.count
        }
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40;
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell:UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        var textDict = NSDictionary()
        if type == "searching"
        {
            textDict = m_filterArray.objectAtIndex(indexPath.row) as! NSDictionary
        }
        else{
            textDict = m_countryArray.objectAtIndex(indexPath.row) as! NSDictionary
        }
        cell?.textLabel?.text = ((textDict.objectForKey("cntrName"))as! String)
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var textDict = NSDictionary()
       // let textDict = m_countryArray.objectAtIndex(indexPath.row)
        if(isFromRegistration == true){
        if type == "searching"
        {
            textDict = m_filterArray.objectAtIndex(indexPath.row) as! NSDictionary
        }
        else{
            textDict = m_countryArray.objectAtIndex(indexPath.row) as! NSDictionary
        }
        mDelegate?.sendArrayToPreviousVC([((textDict.objectForKey("cntrName"))as! String),((textDict.objectForKey("cntrID"))as! String)])
        self.navigationController?.popViewControllerAnimated(true)
        }
        else{
            
        }
    
    }
    // MARK: - api delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlert(errorMessage);
    }
        
    func callServiceForFbLogin(countryStr : String)
    {
//        gender
//        result.valueForKey("email") as! String
//        result.valueForKey("id") as! String
//        result.valueForKey("name") as! String
//        result.valueForKey("first_name") as! String
//        result.valueForKey("last_name") as! String
        
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)

      
    }
    
    func showAlert(msg:String!){
        
        let alert = UIAlertView(title: "IntoBuy", message:msg, delegate: nil, cancelButtonTitle:"Ok")
        alert .show()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
