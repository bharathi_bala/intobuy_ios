//
//  IBMyOrderViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 28/01/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBMyOrderViewController: IBBaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    var m_orderTblView:UITableView!
    var m_orderArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        m_orderArray = ["Transformer deception cufflinks","Lamphorgini children toy","BMW with air bag","Bharath Benz"]
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.hideSettingsBtn(false)
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_titleLabel.text = OrderDetails
        self.createTableView()
    }
    func createTableView()
    {
        m_orderTblView = UITableView(frame: self.m_bgImageView.frame)
        m_orderTblView.delegate = self
        m_orderTblView.dataSource = self
        m_orderTblView.separatorStyle = .None
        self.m_bgImageView.addSubview(m_orderTblView)
    }
    //MARK:- tableview delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return m_orderArray.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 103.667;
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell:IBMyOrdersCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBMyOrdersCell
        if (cell == nil) {
            cell = IBMyOrdersCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        var buttonStatus = String()
        if indexPath.row%2 == 0
        {
            buttonStatus = "1"
        }
        else{
            buttonStatus = "0"
        }
        cell?.selectionStyle = .None
        //cell?.setTitlesToAllLabels(m_orderArray.objectAtIndex(indexPath.row) as! String, dateString: "30/11/2015", orderNoStr: "1234567890", transIdStr: "Suman192", quantityStr: "4", priceStr: "19", buttonStatus: buttonStatus)
        return cell!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
