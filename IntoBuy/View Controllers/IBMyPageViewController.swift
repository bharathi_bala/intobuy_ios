//
//  IBMyProductsViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 26/11/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import Foundation
import UIKit

class IBMyPageViewController: IBBaseViewController ,buttonProtocol,ServerAPIDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIWebViewDelegate,ProfileProtocol,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,ProductProtocol {

    var m_myPageImgView:UIImageView!// = UIImageView()
    var m_pageScrlView:UIScrollView!// = UIScrollView()
    var gridView: GridView?
    var m_myPostCollView:UICollectionView!
    //var dataDict = NSDictionary()
    var profileEntity:IBProfileEntity!
    var m_postArray : NSMutableArray!
    var productArray : NSMutableArray!
    var m_listIndex = NSInteger()
    var m_gridIndex = NSInteger()
    var m_bgImgView: UIView!
    var m_headerBgView: UIView!
    var productScrlView: UIScrollView!
    var pageControl: UIPageControl!
    var m_deleteImgView:UIImageView!
    var m_isDisplay:Bool!
    var wallId:String!
    var imageUrl:String!
    var m_tapGestureTag:Int!
    var m_imagePicker:UIImagePickerController!
    var profileTop : IBProfilePersonalDetailsView!
    var postBtnIndex = 0
    
    var moreBgView:UIImageView!
    var otherUserId = String()
    var isFromProduct = Bool()
    var indexPath:NSIndexPath!
    var dateStr = String()
    
    //for temporary
    var shareStatus = ""
    
    override func viewDidLoad() {
        bottomVal = 1
        super.viewDidLoad()
        self.toShowActivityBtn = false
        self.hideHeaderViewForView(false)
        let userIdStr = getUserIdFromUserDefaults()
        if otherUserId == userIdStr
        {
            self.hideSettingsBtn(false)
        }
        else
        {
            self.hideSettingsBtn(true)
        }
        
        self.hideBottomView(false)
        
      //  self.createServiceForMyProfile()
        m_isDisplay = false
        m_imagePicker = UIImagePickerController()
        m_imagePicker.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:Loading, animated:true)

        postBtnIndex  = 0
        m_gridIndex = 1
        if self.m_isFromCameraBtn == true
        {
            self.createServiceForMyProfile()
        }
        else
        {
            createServiceForOtherUserProfile()
        }
    }
    //MARK:- API for my profile
    func createServiceForMyProfile()
    {
        
        let userIdStr = getUserIdFromUserDefaults()
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_myaccount(userIdStr)

    }
    //MARK:- API for other user profile
    func createServiceForOtherUserProfile()
    {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:Loading, animated:true)
        
        let userIdStr = getUserIdFromUserDefaults()
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getUserprofile(otherUserId, myId: userIdStr)
    }

    func createControlsForMyProducts()
    {
        if(m_myPageImgView != nil)
        {
            m_myPageImgView.removeFromSuperview()
            m_myPageImgView = nil
        }
        m_gridIndex = 1
        m_myPageImgView = UIImageView(frame: self.m_bgImageView.frame)
        m_myPageImgView.image = UIImage(named: "intobuy-products-grid-view-bg.png")
        m_myPageImgView.userInteractionEnabled = true
        self.m_bgImageView .addSubview(m_myPageImgView)
        
        m_pageScrlView = UIScrollView(frame: m_myPageImgView.bounds)
        m_myPageImgView.addSubview(m_pageScrlView)
        
        var viewH:CGFloat = m_myPageImgView.frame.size.height/2;
        if (GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_5) {
            viewH = m_pageScrlView.frame.size.height/2.5;
        } else if (GRAPHICS.Screen_Type() == IPHONE_6_Plus) {
            viewH = m_pageScrlView.frame.size.height/2.5;
        }
        profileTop = IBProfilePersonalDetailsView(frame: CGRectMake(m_pageScrlView.bounds.origin.x,m_pageScrlView.bounds.origin.y,GRAPHICS.Screen_Width(),viewH))
        profileTop.btnDelegate = self
        profileTop.addControls(profileEntity)
        m_pageScrlView.addSubview(profileTop); 
    
        createCollectionView()
        
        
    }
    func createCollectionView()
    {
        if m_postArray != nil{
            m_postArray = nil
        }
        let postLocalArray = NSMutableArray(array:  profileEntity.myPost)//dataDict.objectForKey("myPost")!.mutableCopy() as! NSMutableArray
       // let postLocalArray = m_postArray
        m_postArray = NSMutableArray()
        for i in 0 ..< postLocalArray.count
        {
            
            let dict = postLocalArray.objectAtIndex(i) as! NSDictionary
            if dict.objectForKey("post_image") as! String != ""
            {
                if  profileEntity.userId == getUserIdFromUserDefaults() {
                    self.m_postArray.addObject(dict)
                    }
               else if dict.objectForKey("status")  as! String == "1" && profileEntity.userId != getUserIdFromUserDefaults() {
                    self.m_postArray.addObject(dict)
                }
                
            }
            
        }
        
        let sortedArray = m_postArray.sortedArrayUsingComparator {
            (obj1, obj2) -> NSComparisonResult in
            
            let p1 = obj1 as! NSDictionary
            let p2 = obj2 as! NSDictionary
            let result = p2.objectForKey("added_date")!.compare(p1.objectForKey("added_date") as! String)
            return result
        }
        m_postArray.removeAllObjects()
        m_postArray.addObjectsFromArray(sortedArray)
        
        if productArray != nil{
            productArray = nil
        }

        productArray = NSMutableArray(array:  profileEntity.myProduct)
        let productSortedArray = productArray.sortedArrayUsingComparator {
            (obj1, obj2) -> NSComparisonResult in
            
            let p1 = obj1 as! NSDictionary
            let p2 = obj2 as! NSDictionary
            let result = p2.objectForKey("addedDate")!.compare(p1.objectForKey("addedDate") as! String)
            return result
        }
        productArray.removeAllObjects()
        productArray.addObjectsFromArray(productSortedArray)

        
        
        let categorylayOut = UICollectionViewFlowLayout()
        //categorylayOut.itemSize =  CGSizeMake(m_pageScrlView.frame.size.width/3.1 , m_pageScrlView.frame.size.width/3.1)
        categorylayOut.scrollDirection = .Vertical
        
        if postBtnIndex == 0
        {
            if (m_gridIndex == 1){
                categorylayOut.itemSize = CGSize(width: GRAPHICS.Screen_Width()/3, height: 120)
                categorylayOut.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
            }
            else{
                categorylayOut.itemSize = CGSize(width: GRAPHICS.Screen_Width(), height: 120)
                categorylayOut.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
            }
        }
        else
        {
            if (m_gridIndex == 1){
                categorylayOut.itemSize = CGSizeMake(GRAPHICS.Screen_Width()/2, 195.5)//CGSize(width: GRAPHICS.Screen_Width()/2 , height: 120)
                categorylayOut.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
                categorylayOut.minimumInteritemSpacing = 0
                categorylayOut.minimumLineSpacing = 0
            }
            else{
                categorylayOut.itemSize = CGSize(width: GRAPHICS.Screen_Width() , height: GRAPHICS.Screen_Width())
                categorylayOut.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
                categorylayOut.minimumInteritemSpacing = 0
                categorylayOut.minimumLineSpacing = 0
            }
        }

        if(m_myPostCollView != nil)
        {
            m_myPostCollView.removeFromSuperview()
            m_myPostCollView = nil
        }
        m_myPostCollView = UICollectionView(frame: CGRectMake(m_pageScrlView.bounds.origin.x,profileTop.frame.maxY,m_pageScrlView.frame.size.width,m_pageScrlView.frame.size.height - profileTop.frame.size.height), collectionViewLayout: categorylayOut)
        m_myPostCollView.backgroundColor = UIColor.clearColor()
        m_myPostCollView.scrollEnabled = false
        m_myPostCollView.bounces = true
        m_myPostCollView.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        if postBtnIndex == 0
        {
            m_myPostCollView.registerClass(IBMyPageCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "cellIdentifier")
        }
        else{
            m_myPostCollView.registerClass(IBMyProductsCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "cellIdentifier")
        }
        m_myPostCollView.delegate = self
        m_myPostCollView.dataSource = self
        m_pageScrlView.addSubview(m_myPostCollView)
        

    }
    
    //MARK:- collectionview delegates
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if postBtnIndex == 0{
            return m_postArray.count
        }
        else
        {
            return productArray.count
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        if postBtnIndex == 0{
            let cell:IBMyPageCollectionViewCell? = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath) as? IBMyPageCollectionViewCell
            if (cell == nil) {
                // cell = IBMyPageCollectionViewCell(frame: CGR)
            }
            let myPostDict = m_postArray.objectAtIndex(indexPath.item)as? NSDictionary
            let imageArray = myPostDict?.objectForKey("images")as! NSArray
            if m_gridIndex == 1{
                cell!.frame.size.width = GRAPHICS.Screen_Width() / 3
                cell!.frame.size.height = cell!.frame.size.width
                collectionView.frame.size.height = (cell?.frame.maxY)! + 10
                m_pageScrlView.contentSize = CGSizeMake(m_pageScrlView.frame.size.width, collectionView.frame.maxY)
                
            }
            else{
                cell!.frame.size.width = GRAPHICS.Screen_Width()
                cell!.frame.size.height = GRAPHICS.Screen_Width()
                collectionView.frame.size.height =  cell!.frame.size.height * (CGFloat(m_postArray.count)) + GRAPHICS.Screen_Width()
                m_pageScrlView.contentSize = CGSizeMake(m_pageScrlView.frame.size.width, collectionView.frame.maxY)
            }
            cell?.setImagesToScrlView(imageArray, scrollViewTag: indexPath.item * 10)
            cell?.profileProtocol = self
            
            return cell!
        }
        else{
            let cell:IBMyProductsCollectionViewCell? = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath) as? IBMyProductsCollectionViewCell
            let myProductDict = productArray.objectAtIndex(indexPath.item)as? NSDictionary
            let mainImg = myProductDict?.objectForKey("mainImage")as! String
 
            cell?.productDelegate = self
            if m_gridIndex == 1
            {
                let gridImg = GRAPHICS .PRODUCT_GRID_IMAGE()
                cell?.createControlsForProductList((cell?.frame.size.width)!, height: (cell?.frame.size.height)!, bgImage: gridImg! ,gridIndex : m_gridIndex , productImgUrl : mainImg)
                cell!.frame.size.height = (cell?.bgView.frame.maxY)! + 10
                
                    var newHeight = cell!.frame.size.height * CGFloat((Int(productArray.count)/2))//(cell?.frame.maxY)!
                
                if productArray.count % 2 == 1 {
                     newHeight += cell!.frame.size.height
                }
                collectionView.frame.size.height = newHeight
                
                //print(cell!.frame.size.height)
                //print(collectionView.frame.size.height)
            }
            else
            {
                let listImg = GRAPHICS .PRODUCT_LIST_IMAGE()
                cell?.createControlsForProductList((cell?.frame.size.width)!, height: (cell?.frame.size.height)!, bgImage: listImg! ,gridIndex : m_gridIndex , productImgUrl : mainImg)
                collectionView.frame.size.height = cell!.frame.size.height * (CGFloat(productArray.count))
            }
            cell?.bgImgView.tag = indexPath.item
            cell!.setvaluesForProducts(myProductDict?.objectForKey("productname") as! String, ProductRate: myProductDict?.objectForKey("price") as! String , commentArray :myProductDict?.objectForKey("comments") as! NSArray, likeBtnTag : indexPath.item * 10 , commentBtnTag : indexPath.item * 100 , cartBtnTag : indexPath.item * 1000 , isLiked :myProductDict?.objectForKey("isLiked") as! String , moreBtnTag: indexPath.item * 10000 , cartStatus:myProductDict?.objectForKey("is_in_cart") as! String ,likeCount :  myProductDict?.objectForKey("likecount") as! String ,quantityStr: myProductDict?.objectForKey("quantity") as! String)
            m_pageScrlView.contentSize = CGSizeMake(m_pageScrlView.frame.size.width, collectionView.frame.maxY + 10)
            
            return cell!
        }
        
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if postBtnIndex == 1
        {
            let productDetailVC = IBProductDetailsViewController()
            let myProductDict = productArray.objectAtIndex(indexPath.item)as? NSDictionary
            productDetailVC.productId = myProductDict?.objectForKey("productId") as! String
            self.navigationController?.pushViewController(productDetailVC, animated: true)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        var newSize:CGSize!
            if postBtnIndex == 0
            {
                if (m_gridIndex == 1){
                    newSize = CGSize(width: m_pageScrlView.frame.size.width/3.2, height: m_pageScrlView.frame.size.width/3.2)
                }
                else{
                    newSize = CGSize(width: m_pageScrlView.frame.size.width, height: m_pageScrlView.frame.size.width)
                }
            }
            else{
                if (m_gridIndex == 1){
                    let gridImg = GRAPHICS .PRODUCT_GRID_IMAGE()
                    _ = GRAPHICS.getImageWidthHeight(gridImg!)
                    var height:CGFloat!
                    if GRAPHICS.Screen_Type() == IPHONE_4 || GRAPHICS.Screen_Type() == IPHONE_5
                    {
                        height = gridImg!.size.height/1.4
                    }
                    else
                    {
                        height = gridImg!.size.height/1.2
                    }

                    newSize = CGSize(width: GRAPHICS.Screen_Width()/2 , height: height)
                }
                else{
                    let listImg = GRAPHICS .PRODUCT_LIST_IMAGE()
                    let contentSize = GRAPHICS.getImageWidthHeight(listImg!)
                    let height = contentSize.height
                    newSize = CGSize(width: GRAPHICS.Screen_Width(), height: height)
                }
            }
        return newSize
    }
    func setToList(sender: UIButton)
    {
        if moreBgView != nil{
            moreBgView.removeFromSuperview()
            moreBgView = nil
        }
        m_listIndex = 1
        m_gridIndex = 0
        //m_myPostCollView.reloadData()
        if(m_myPostCollView != nil){
            m_myPostCollView.removeFromSuperview()
            m_myPostCollView = nil
        }
        createCollectionView()

    }
    func setToGrid(sender: UIButton)
    {
        if moreBgView != nil{
            moreBgView.removeFromSuperview()
            moreBgView = nil
        }

        m_listIndex = 0
        m_gridIndex = 1
      //  m_myPostCollView.reloadData()
        if(m_myPostCollView != nil){
            m_myPostCollView.removeFromSuperview()
            m_myPostCollView = nil
        }
        createCollectionView()
    }
    func ChangeToProductOrPost(sender: UIButton)
    {
        if moreBgView != nil{
            moreBgView.removeFromSuperview()
            moreBgView = nil
        }

        postBtnIndex = 1
        if(m_myPostCollView != nil){
            m_myPostCollView.removeFromSuperview()
            m_myPostCollView = nil
        }
        createCollectionView()
    }
    
    func ChangeToPost(sender: UIButton)
    {
        if moreBgView != nil{
            moreBgView.removeFromSuperview()
            moreBgView = nil
        }
        
        postBtnIndex = 0
        if(m_myPostCollView != nil){
            m_myPostCollView.removeFromSuperview()
            m_myPostCollView = nil
        }
        createCollectionView()
    }
    func openDairyScreen(sender: UIButton)
    {
        if moreBgView != nil{
            moreBgView.removeFromSuperview()
            moreBgView = nil
        }
        let calenderScreen = IBMydiaryCalendarViewController()
        calenderScreen.userIdStr = profileEntity.userId
        self.navigationController?.pushViewController(calenderScreen, animated: true)

        /*let toUserId = profileEntity.userId
        let fromUserId = getUserIdFromUserDefaults()
        let date = NSDate()
        let dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateStr = dateFormatter.stringFromDate(date)

        SwiftLoader.show(title: Loading, animated: true)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_checkDiaryAccess(fromUserId, toUserId: toUserId, today: dateStr)*/
        
        
    }
    //MARK: - tap gesture to open image in full screen
    func scrollViewGesture(tapGesture: UITapGestureRecognizer)
    {
        m_isDisplay = false
        //print((tapGesture.view?.tag)!)
        m_tapGestureTag = tapGesture.view!.tag / 10
        let contentOffset = (tapGesture.view?.tag)! % 10
        //print("offset is",contentOffset)
        //print(m_tapGestureTag)
        //print("tag is",m_tapGestureTag)
        m_bgImgView = UIView(frame: CGRectMake (GRAPHICS.Screen_X(),GRAPHICS.Screen_Y(),GRAPHICS.Screen_Width(),GRAPHICS.Screen_Height()))
        m_bgImgView.backgroundColor = UIColor.blackColor()
        self.view.addSubview(m_bgImgView)
        
        let tapGesturebg = UITapGestureRecognizer(target: self, action: #selector(IBMyPageViewController.bgTapGesture(_:)))
        m_bgImgView.addGestureRecognizer(tapGesturebg)
        
        
        m_headerBgView = UIView(frame: CGRectMake(0, 0, GRAPHICS.Screen_Width(), 50))
        m_headerBgView.backgroundColor = UIColor.clearColor()
//        m_headerBgView.alpha = 0.0
        m_bgImgView.addSubview(m_headerBgView)
        
        
        let titleLbl = UILabel(frame: CGRect(x: 0, y: 0, width: GRAPHICS.Screen_Width(), height: 50))
        m_headerBgView.addSubview(titleLbl)
        titleLbl.text = "Tap anywhere to dismiss"
        titleLbl.textColor = UIColor.whiteColor()
        titleLbl.font = GRAPHICS.FONT_BOLD(12)
        titleLbl.textAlignment = .Center
        

        
        
        let scrlHeight = GRAPHICS.Screen_Height() - m_headerBgView.frame.maxY - 100
        productScrlView = UIScrollView(frame: CGRectMake(GRAPHICS.Screen_X(),m_headerBgView.frame.maxY,GRAPHICS.Screen_Width(),scrlHeight))
        productScrlView.pagingEnabled = true
        productScrlView.delegate = self
        m_bgImgView.addSubview(productScrlView)
        
        let resultDict = m_postArray.objectAtIndex(m_tapGestureTag)as! NSDictionary
        let imageArray = (resultDict.objectForKey("images")as? NSArray!)!
        
        var xPos = productScrlView.bounds.origin.x
        for i in 0  ..< imageArray.count 
        {
            let imageDict = imageArray.objectAtIndex(i)as! NSDictionary
//            let webView = UIWebView(frame: CGRectMake(xPos,productScrlView.bounds.origin.y,GRAPHICS.Screen_Width(),productScrlView.frame.size.height))
//            webView.backgroundColor = UIColor.clearColor();
//            webView.delegate = self;
//            webView.opaque = false;
//            webView.stringByEvaluatingJavaScriptFromString("document. body.style.zoom = 5.0;");
//            webView.scalesPageToFit = true;
//            let urlStr = imageDict.objectForKey("image")as! String
//            let imgUrl = NSURL(string: urlStr)
//            let request = NSURLRequest(URL: imgUrl!)
//            webView.loadRequest(request)
//            productScrlView.addSubview(webView)
//            UIView.animateWithDuration(0.5, delay: 0.5, options: UIViewAnimationOptions.CurveLinear, animations: {
//                webView.alpha = 1.0
//                }, completion: nil)
            let urlStr = imageDict.objectForKey("image")as! String
            let imgUrl = NSURL(string: urlStr)
            let postImgView = UIImageView(frame: CGRectMake(xPos,productScrlView.bounds.origin.y,GRAPHICS.Screen_Width(),productScrlView.frame.size.height))
            postImgView.load(imgUrl!, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            postImgView.contentMode = .ScaleAspectFit
            productScrlView.addSubview(postImgView)

            xPos = xPos + GRAPHICS.Screen_Width();
        
        }
//        pageLikeBtnAction
//        pageCommentBtnAction
        
        
        if (imageArray.count > 1){
        self.pageControl = UIPageControl(frame: CGRectMake((m_bgImgView.frame.size.width - CGFloat(imageArray.count))/2, productScrlView.frame.maxY + 10, 10, 10))
        self.pageControl.numberOfPages = imageArray.count
        self.pageControl.currentPage = contentOffset
        self.pageControl.pageIndicatorTintColor = UIColor.whiteColor()
        self.pageControl.currentPageIndicatorTintColor = UIColor(red: 89.0/255.0, green: 191.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        pageControl.addTarget(self, action: #selector(IBMyPageViewController.changePage(_:)), forControlEvents: UIControlEvents.ValueChanged)
    
        m_bgImgView.addSubview(pageControl)
        }
//        MY_PAGE_LIKE_BTN_NORMAL_IMAGE
//        MY_PAGE_LIKE_BTN_SELECTED_IMAGE
//        MY_PAGE_COMMENT_BTN_NORMAL_IMAGE
//        MY_PAGE_COMMENT_BTN_SELECTED_IMAGE
        let isLikedStr = (resultDict.objectForKey("isLiked")as? String!)!
        let likeImg = GRAPHICS.MY_PAGE_LIKE_BTN_NORMAL_IMAGE()
        let likeSel = GRAPHICS.MY_PAGE_LIKE_BTN_SELECTED_IMAGE()
        var width = likeImg.size.width/2
        var height = likeImg.size.height/2
        xPos = GRAPHICS.Screen_X() + 10
        let likeBtn = UIButton(frame : CGRectMake(xPos,productScrlView.frame.maxY + 10,width,height))
        likeBtn.setBackgroundImage(likeImg, forState: .Normal)
        likeBtn.setBackgroundImage(likeSel, forState: .Selected)
        likeBtn.addTarget(self, action: #selector(IBMyPageViewController.pageLikeBtnAction(_:)), forControlEvents: .TouchUpInside)
        likeBtn.tag = 3
        m_bgImgView.addSubview(likeBtn)
        
        let likeBigBtn = UIButton(frame : CGRectMake(xPos - 10,productScrlView.frame.maxY,width + 20,height + 20))
        likeBigBtn.addTarget(self, action: #selector(IBMyPageViewController.pageLikeBtnAction(_:)), forControlEvents: .TouchUpInside)
        likeBigBtn.tag = 4
        m_bgImgView.addSubview(likeBigBtn)

    
        if isLikedStr == "1"
        {
            likeBtn.selected = true
        }
        else
        {
            likeBtn.selected = false
        }
        
        let commentImg = GRAPHICS.MY_PAGE_COMMENT_BTN_NORMAL_IMAGE()
        xPos = likeBtn.frame.maxX + 15
        width = commentImg.size.width/1.7
        height = commentImg.size.height/1.7
        let commentBtn = UIButton(frame : CGRectMake(xPos,productScrlView.frame.maxY + 10,width,height))
        commentBtn.tag = 2
        let commentArray = (resultDict.objectForKey("Comments")as? NSArray!)!
        if commentArray.count > 0
        {
            commentBtn.setBackgroundImage(GRAPHICS.MY_PAGE_COMMENT_BTN_SELECTED_IMAGE(), forState: .Normal)
        }
        else{
            commentBtn.setBackgroundImage(GRAPHICS.MY_PAGE_COMMENT_BTN_NORMAL_IMAGE(), forState: .Normal)
        }
       // commentBtn.setBackgroundImage(commentImg, forState: .Normal)
        commentBtn.addTarget(self, action: #selector(IBMyPageViewController.pageCommentBtnAction(_:)), forControlEvents: .TouchUpInside)
        m_bgImgView.addSubview(commentBtn)

        if m_isFromCameraBtn
        {
            let deleteImg = GRAPHICS.POST_DELETE_BUTTON()
            width = deleteImg.size.width/2
            height = deleteImg.size.height/2
            let deleteBtn = UIButton(frame: CGRectMake(m_bgImgView.frame.size.width - width ,productScrlView.frame.maxY + 10, width, height))
            deleteBtn.tag = 1;
            deleteBtn.setBackgroundImage(deleteImg, forState: .Normal)
            deleteBtn.addTarget(self, action: #selector(IBMyPageViewController.deleteTheImage(_:)), forControlEvents: .TouchUpInside)
            m_bgImgView.addSubview(deleteBtn)
        }
        
        productScrlView.contentOffset = CGPointMake((CGFloat(contentOffset)) * GRAPHICS.Screen_Width(), productScrlView.bounds.origin.y)
        productScrlView.contentSize = CGSizeMake(GRAPHICS.Screen_Width() * (CGFloat(imageArray.count)), productScrlView.frame.size.height)
        
//        let doneImg = GRAPHICS.IMAGE_FULL_SCREEN_DONE_BUTTON()
//        width = doneImg.size.width + 20
//        height = doneImg.size.height + 5
//        let doneBtn = UIButton(frame: CGRectMake(GRAPHICS.Screen_Width() - width,GRAPHICS.Screen_Y() + 10,width,height))
////        doneBtn.layer.cornerRadius = 2.0
//        doneBtn.setTitle("done", forState: .Normal)
//        doneBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(16)
////        doneBtn.layer.borderColor = UIColor.whiteColor().CGColor
//        //doneBtn.setBackgroundImage(doneImg, forState: .Normal)
//        doneBtn.addTarget(self, action: #selector(IBMyPageViewController.removeTheView(_:)), forControlEvents: .TouchUpInside)
//        m_headerBgView.addSubview(doneBtn)
//        m_headerBgView.frame.size.height = doneBtn.frame.maxY;
        
        let postDict = m_postArray.objectAtIndex(m_tapGestureTag)as! NSDictionary
        let imagesArray = postDict.objectForKey("images")as! NSArray
        let imageDict = imagesArray.objectAtIndex(0)as! NSDictionary
        wallId = imageDict.objectForKey("wall_id")as! String
        imageUrl = imageDict.objectForKey("image")as! String

        
    }
    func deleteTheImage(sender :UIButton)
    {
        createViewToDelete()
    }
    func removeTheView(sender :UIButton)
    {
        m_bgImgView .removeFromSuperview()
        m_bgImgView = nil
    }
    func bgTapGesture(tapGesture: UITapGestureRecognizer)
    {
        
        m_bgImgView .removeFromSuperview()
        m_bgImgView = nil
        /*
        if m_isDisplay == false
        {
            m_isDisplay = true
            
            UIView.animateWithDuration(0.5, delay: 0.5, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.m_headerBgView.alpha = 1.0
                }, completion: nil)
        }
            
        else
        {
            m_isDisplay = false
            
            UIView.animateWithDuration(0.5, delay: 0.5, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.m_headerBgView.alpha = 0.0
                }, completion: nil)
        }
        */
        
    }

    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * m_pageScrlView.frame.size.width
        m_pageScrlView.setContentOffset(CGPointMake(x, 0), animated: true)
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if(productScrlView != nil){
            if(productScrlView.contentSize.width > GRAPHICS.Screen_Width()){
                let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                pageControl.currentPage = Int(pageNumber)
                let postDict = m_postArray.objectAtIndex(m_tapGestureTag)as! NSDictionary
                let imageArray = postDict.objectForKey("images")as! NSArray
                let imageDict = imageArray.objectAtIndex(Int(pageNumber))as! NSDictionary
                wallId = imageDict.objectForKey("wall_id")as! String
                imageUrl = imageDict.objectForKey("image")as! String
            }
        }
    }
    
    
    func createViewToDelete()
    {
        let bgImgView = GRAPHICS.MY_POST_TRANSPARENT_BG()
        var posX = GRAPHICS.Screen_X()
        var posY = GRAPHICS.Screen_Y()
        var width = GRAPHICS.Screen_Width()
        var height = GRAPHICS.Screen_Height()
        
        m_deleteImgView = UIImageView(frame: CGRectMake(posX, posY, width, height))
        m_deleteImgView.image = bgImgView
        m_deleteImgView.userInteractionEnabled = true
        self.m_bgImgView.addSubview(m_deleteImgView)
        
        let alertImg = GRAPHICS.POST_ALERT_BG_IMAGE()
        width = alertImg.size.width/2
        height = alertImg.size.height/2
        posX = (GRAPHICS.Screen_Width() - width)/2
        posY = (GRAPHICS.Screen_Height() - height)/2
        let alertView = UIImageView(frame: CGRectMake(posX, posY, width, height))
        alertView.image = alertImg
        alertView.userInteractionEnabled = true
        m_deleteImgView.addSubview(alertView)
        
        posX = alertView.bounds.origin.x
        posY = alertView.bounds.origin.y + 5
        height = 20
        width = alertView.frame.size.width
        let titleLbl = UILabel(frame: CGRectMake(posX, posY, width, height))
        titleLbl.text = ConfirmDeletion
        titleLbl.font = GRAPHICS.FONT_REGULAR(14)
        titleLbl.textAlignment = .Center
        titleLbl.textColor = UIColor(red: 53.0/255.0, green: 36.0/255.0, blue: 198.0/255.0, alpha: 1.0)
        alertView.addSubview(titleLbl)
        
        posY = titleLbl.frame.maxY + 20
        let confirmLbl = UILabel(frame: CGRectMake(posX, posY, width, height))
        confirmLbl.text = confirmDelete
        confirmLbl.font = GRAPHICS.FONT_REGULAR(12)
        confirmLbl.textAlignment = .Center
        alertView.addSubview(confirmLbl)
        
        height = 40
        posY = alertView.frame.size.height - height
        let dontDelBtn = UIButton(frame: CGRectMake(posX, posY, width, height))
        dontDelBtn.setTitle(dontDelete, forState: .Normal)
        dontDelBtn.setTitleColor(UIColor(red: 24.0/255.0, green: 24.0/255.0, blue: 24.0/255.0, alpha: 1.0), forState: .Normal)
        dontDelBtn.titleLabel!.font = GRAPHICS.FONT_BOLD(12)
        dontDelBtn.addTarget(self, action: #selector(IBMyPageViewController.dontDeleteBtnAction(_:)), forControlEvents: .TouchUpInside)
        addBottomBorder(dontDelBtn)
        alertView.addSubview(dontDelBtn)
        
        posY = dontDelBtn.frame.origin.y - height
        let deleteBtn = UIButton(frame: CGRectMake(posX, posY, width, height))
        deleteBtn.setTitle("Delete", forState: .Normal)
        deleteBtn.titleLabel!.font = GRAPHICS.FONT_BOLD(12)
        deleteBtn.setTitleColor(UIColor(red: 24.0/255.0, green: 24.0/255.0, blue: 24.0/255.0, alpha: 1.0), forState: .Normal)
        deleteBtn.addTarget(self, action: #selector(IBMyPageViewController.deleteBtnAction(_:)), forControlEvents: .TouchUpInside)
        addBottomBorder(deleteBtn)
        alertView.addSubview(deleteBtn)
    }
    func dontDeleteBtnAction(sender: UIButton)
    {
        m_deleteImgView .removeFromSuperview()
        m_deleteImgView = nil
    }
    func deleteBtnAction(sender: UIButton)
    {
        let userIdStr = getUserIdFromUserDefaults()
        SwiftLoader.show(animated: true)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_deleteImageFromPost(userIdStr, wall_id: wallId, imagenameurl: imageUrl)
    }
    
        
    func addBottomBorder(view: UIView)
    {
        //let lineImg = GRAPHICS.POST_ALERT_BORDER_LINE()
        let bottomBorder = UIImageView(frame: CGRectMake(view.frame.origin.x + 2, view.bounds.origin.y, view.frame.size.width - 4, 1.0))
        bottomBorder.backgroundColor = UIColor.lightGrayColor()
        view.addSubview(bottomBorder)
    }
    func uploadProfilePic(sender: UIButton)
    {
        self.m_isFromCameraBtn = false
        let actionSheet = UIActionSheet(title: Choosefrom, delegate: self, cancelButtonTitle: cancel, destructiveButtonTitle: nil, otherButtonTitles: Takephoto,ChoosePhoto)
        
        actionSheet.showInView(self.view)
   
    }
    //MARK:-ActionSheet Delegate methods
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex
        {
        case 0:
            
            actionSheet .dismissWithClickedButtonIndex(buttonIndex, animated: true)
            break
            
        case 1:
            self.tappedOnCameraButton()
            break
            
        case 2:
            self.galleryButtonTapped()
            break
            
        default:
            break
            
        }
        
    }
    
    func tappedOnCameraButton()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
        {
            m_imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            self.presentViewController(m_imagePicker, animated: true, completion: nil)
        }
        else
        {
            
            showAlertViewWithMessage(cameraNotAvailable)
            
        }
        
        
    }
    
    func galleryButtonTapped()
    {
        m_imagePicker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
        m_imagePicker.allowsEditing = true //deepika to set crop 
        self.presentViewController(m_imagePicker, animated: true, completion: nil)
        
    }
    //MARK:- follow btn action
    func followBtnTarget()
    {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let userDetailUD = NSUserDefaults()
        let userIdStr = userDetailUD.objectForKey("user_id") as! String;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_follow(userIdStr, followerId: self.profileEntity.userId)
        }
    }

    
    //MARK: ImagePickerView Delagate Methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        
        let selectedImage : UIImage = info[UIImagePickerControllerOriginalImage]as! UIImage
        
        profileTop.profileImgView.image = selectedImage
        let profilePicImg = UIImageJPEGRepresentation(selectedImage,1.0);
        
        let userIdStr = getUserIdFromUserDefaults()
        
        SwiftLoader.show(animated: true)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_Uploadprofilepic(userIdStr, ProfilePic: profilePicImg!)

        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        
        self .dismissViewControllerAnimated(true, completion: nil)
    }

    
    //MARK:- more button action
    func productMoreBtnAction(sender : UIButton)
    {
        
        let moreBgImg:UIImage!
        let shareBtnImg:UIImage!
        let editBtnImg:UIImage!
        let deleteBtnImg:UIImage!
        let chatBtnImg:UIImage!
        var imageArray = NSArray()
        let dividerImg:UIImage!
       // var moreBgView:UIImageView!
        
        
        if m_gridIndex == 1{
            moreBgImg = GRAPHICS.MY_PRODUCT_GRID_MORE_BG_IMAGE()
            shareBtnImg = GRAPHICS.MY_PRODUCT_GRID_SHARE_IMAGE()
            editBtnImg = GRAPHICS.MY_PRODUCT_GRID_EDIT_IMAGE()
            deleteBtnImg = GRAPHICS.MY_PRODUCT_GRID_DELETE_IMAGE()
            chatBtnImg = GRAPHICS.MY_PRODUCT_GRID_COMMENT_IMAGE()
            dividerImg = GRAPHICS.MY_PRODUCT_GRID_DIVIDER_IMAGE()
            imageArray = [chatBtnImg,editBtnImg,deleteBtnImg,shareBtnImg]
        }
        else{
            moreBgImg = GRAPHICS.MY_PRODUCT_LIST_MORE_BG_IMAGE()
            shareBtnImg = GRAPHICS.MY_PRODUCT_LIST_SHARE_IMAGE()
            editBtnImg = GRAPHICS.MY_PRODUCT_LIST_EDIT_IMAGE()
            deleteBtnImg = GRAPHICS.MY_PRODUCT_LIST_DELETE_IMAGE()
            chatBtnImg = GRAPHICS.MY_PRODUCT_LIST_COMMENT_IMAGE()
            dividerImg = GRAPHICS.MY_PRODUCT_LIST_DIVIDER_IMAGE()
            imageArray = [chatBtnImg,editBtnImg,deleteBtnImg,shareBtnImg]
        }
        var contentSize = GRAPHICS.getImageWidthHeight(moreBgImg)
        var width = contentSize.width
        var height = contentSize.height
        var xPos = sender.frame.maxX - width - 10
        var yPos = sender.frame.origin.y - height
        
        if moreBgView != nil
        {
            moreBgView .removeFromSuperview()
            moreBgView = nil
        }
        //print(sender.tag)
        moreBgView = UIImageView(frame : CGRectMake(xPos,yPos,width,height))
        moreBgView.userInteractionEnabled = true
        moreBgView.image = moreBgImg
        moreBgView.tag = sender.tag/10000
        sender.superview!.addSubview(moreBgView)
        
        xPos = moreBgView.bounds.origin.x + 5
        
        for i in 0 ..< imageArray.count
        {
            let image = imageArray.objectAtIndex(i) as! UIImage
            contentSize = GRAPHICS.getImageWidthHeight(image)
            width = contentSize.width
            height = contentSize.height
            var padding:Int!
            if m_gridIndex == 1{
                padding = 5
            }
            else{
                padding = 10
            }
            yPos = moreBgView.bounds.origin.y + CGFloat(padding)
            let buttons = UIButton(frame : CGRectMake(xPos,yPos,width,height))
            buttons.setBackgroundImage(image, forState: .Normal)
            buttons.addTarget(self, action: #selector(IBMyPageViewController.moreBgAlertActions(_:)), forControlEvents: .TouchUpInside)
            buttons.tag = moreBgView.tag + i
            moreBgView.addSubview(buttons)
            
            contentSize = GRAPHICS.getImageWidthHeight(dividerImg)
            width = contentSize.width
            height = contentSize.height
            xPos = buttons.frame.maxX + CGFloat(padding)/2
           // yPos = (moreBgView.frame.size.height - height)/2
            if m_gridIndex == 1{
               yPos = yPos - 2.5
            }
            else{
               yPos = yPos - 5
            }

            
            let dividerImgView = UIImageView(frame : CGRectMake(xPos,yPos,width,height))
            dividerImgView.image = dividerImg
            moreBgView.addSubview(dividerImgView)
            if i == 3
            {
                dividerImgView.hidden = true
            }
            else
            {
                dividerImgView.hidden = false
            }
            
            xPos = dividerImgView.frame.maxX + CGFloat(padding)
        }
    }
    func moreBgAlertActions(sender : UIButton)
    {
        if sender.tag == moreBgView.tag + 0
        {
           /* let index = sender.tag
            //print(index)
            let productDict = productArray.objectAtIndex(index) as! NSMutableDictionary
            let sellerId = productDict.objectForKey("sellerId") as! String
            let productId = productDict.objectForKey("productId") as! String
            let shareStatus = productDict.objectForKey("shareStatus") as! String
            var toShare:String!
            if shareStatus == "1"
            {
                toShare = "0"
            }
            else{
                toShare = "1"
            }
            let userIdStr = getUserIdFromUserDefaults()
            if sellerId == userIdStr
            {
                SwiftLoader.show(title: Loading, animated: true)
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_Shareproduct(userIdStr, productId: productId, shareStatus: toShare)
            }
            else
            {
                showAlertViewWithMessage("You can't edit others product")
            }*/
        }
        else if sender.tag == moreBgView.tag + 1
        {
            let index = sender.tag - 1
            //print(index)
            let productDict = productArray.objectAtIndex(index) as! NSDictionary
            let sellerId = productDict.objectForKey("sellerId") as! String
            let userIdStr = getUserIdFromUserDefaults()
            if sellerId == userIdStr
            {
                let productSellVC = IBProductsSellViewController()
                productSellVC.isFromProfile = true
                productSellVC.productDetailsDict = productDict
                self.navigationController?.pushViewController(productSellVC, animated: true)
            }
            else
            {
                showAlertViewWithMessage("You can't edit others product")
            }
        }
        else if sender.tag ==  moreBgView.tag + 2
        {
            let index = sender.tag - 2
            //print(index)
            let productDict = productArray.objectAtIndex(index) as! NSDictionary
            let productId = productDict.objectForKey("productId") as! String
            let sellerId = productDict.objectForKey("sellerId") as! String
            let userIdStr = getUserIdFromUserDefaults()
            
            if sellerId == userIdStr
            {
                SwiftLoader.show(animated: true)
                SwiftLoader.show(title: Loading, animated: true)
                
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_deleteproduct(userIdStr, productId: productId)
            }
            else
            {
                showAlertViewWithMessage("You can't delete others product")
            }
            
        }
        else
        {
            
        }
        if moreBgView != nil{
            moreBgView.removeFromSuperview()
            moreBgView = nil
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func pageLikeBtnAction(sender : UIButton)
    {
            //print(sender.tag)
            isFromProduct = false
            sender.userInteractionEnabled = false
            let userIdStr = getUserIdFromUserDefaults()//userDetailUD.objectForKey("user_id") as! String;
            let mypostArr = profileEntity.myPost
            let myPostDict = mypostArr.objectAtIndex(m_tapGestureTag)as! NSDictionary
            let wallIdStr = myPostDict.objectForKey("wall_id") as! String
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_addWallLike(wallIdStr as String, userId: userIdStr)
    }

    func pageCommentBtnAction(sender : UIButton)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let mypostArr = profileEntity.myPost
            let myPostDict = mypostArr.objectAtIndex(m_tapGestureTag)as! NSDictionary
            let wallIdStr = myPostDict.objectForKey("wall_id") as! String
           // let resultDict = m_feedArray.objectAtIndex(sender.tag - 100)as! IBMyFeedEntity
            let commentScreen = IBCommentsViewController()
            commentScreen.m_wallIdStr = wallIdStr//resultDict.objectForKey("wall_id") as! String
            self.navigationController!.pushViewController(commentScreen, animated: true)
        }
        else
        {
            showAlertViewWithMessage(NoInternetConnection)
        }

    }

    //MARK:- followers tap gesture
    func openFollowersScreen(tapGesture : UITapGestureRecognizer)
    {
        //deepika
        let followersScreen = IBFollowersListVC()
        followersScreen.userIdStr = profileEntity.userId//dataDict.objectForKey("userId") as! String
        self.navigationController?.pushViewController(followersScreen, animated: true)
    }
    //MARK:- following tap gesture
    func openFollowingScreen(tapGesture : UITapGestureRecognizer)
    {
        let followingScreen = IBFollowingViewController()
        followingScreen.userId = profileEntity.userId//dataDict.objectForKey("userId") as! String
        self.navigationController?.pushViewController(followingScreen, animated: true)
    }
    func editProfileBtnAction()
    {
        
    }
    
    //MARK:- product type button actions
    func productLikeBtnAction(sender : UIButton)
    {
        indexPath = NSIndexPath(forItem: sender.tag/10 , inSection: 0)
        //print(sender.tag)
        if Reachability.isConnectedToNetwork() == true
        {
            isFromProduct = true
            sender.selected = !sender.selected
            let myProductsDict = productArray.objectAtIndex(sender.tag/10) as! NSDictionary
            let wallIdStr = myProductsDict.objectForKey("wall_id") as! String
            let userIdStr = getUserIdFromUserDefaults()
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_addWallLike(wallIdStr as String, userId: userIdStr)
            }
        }
        else
        {
            showAlertViewWithMessage(NoInternetConnection)
        }
        
    }
    func productCommentBtnAction(sender : UIButton)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let myProductsDict = productArray.objectAtIndex(sender.tag/100) as! NSDictionary
            let commentScreen = IBCommentsViewController()
            commentScreen.m_wallIdStr = myProductsDict.objectForKey("wall_id") as! String
            self.navigationController!.pushViewController(commentScreen, animated: true)
        }
        else{
            showAlertViewWithMessage(NoInternetConnection)
        }

    }
    func productCartBtnAction(sender : UIButton)
    {
        if profileEntity.userId == getUserIdFromUserDefaults(){
            showAlertViewWithMessage(AddCart_Alert)
        }
        else{
            indexPath = NSIndexPath(forItem: sender.tag/1000 , inSection: 0)
        let myProductsDict = productArray.objectAtIndex(sender.tag/1000) as! NSDictionary
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title: Loading, animated: true)
        
        let userIdStr = getUserIdFromUserDefaults()
        let productIdStr = myProductsDict.objectForKey("productId") as! String
        let sellerIdStr = myProductsDict.objectForKey("sellerId") as! String
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_addProductToCart(userIdStr, productId: productIdStr, sellerId: sellerIdStr)
        }
    }
    
    //MARK: - api delegate
    func API_CALLBACK_myaccount(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = (resultDict.objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
            let resultArray = resultDict.objectForKey("result")as! NSArray
            let dataDict = (resultArray.objectAtIndex(0)as? NSDictionary)!
            //print(dataDict)
            profileEntity = IBProfileEntity(dict: dataDict)
            self.m_titleLabel.text = "My page"
            self.createControlsForMyProducts()
        }
        else{
           showAlertViewWithMessage(ServerNotRespondingMessage) 
        }
        
    }
    func API_CALLBACK_GetUserprofile(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = (resultDict.objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
            let resultArray = resultDict.objectForKey("result")as! NSArray
            let dataDict = (resultArray.objectAtIndex(0)as? NSDictionary)!
            //print(dataDict)
            profileEntity = IBProfileEntity(dict: dataDict)
            if profileEntity.userId != getUserIdFromUserDefaults() {
                self.m_titleLabel.text = String(format: "%@ 's page",profileEntity.username)
                self.m_titleLabel.adjustsFontSizeToFitWidth = true
            }
            else{
                self.m_titleLabel.text = "My page"
            }
            self.createControlsForMyProducts()
        }
        else{
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
        
    }
    func API_CALLBACK_addWallLike(resultDict: NSDictionary)
    {
        //SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            if isFromProduct
            {
                let dict = productArray[indexPath.row] as! NSDictionary
                let mutDict = dict.mutableCopy()
                
                var likedCount = mutDict.objectForKey("likecount") as! String
                
                let colleViewCell = self.m_myPostCollView.cellForItemAtIndexPath(indexPath) as! IBMyProductsCollectionViewCell
//                let bgImgView = colleViewCell?.viewWithTag(indexPath.item) as! UIImageView
//                let likeBtn = bgImgView.viewWithTag(bgImgView.tag * 10) as! UIButton
                let msgStr = resultDict.objectForKey("result")as? String!
//                let likeInt = Int(colleViewCell.likeLbl.text!)
                if msgStr == "You Unliked successfully"
                {
                    mutDict.setObject("0", forKey: "isLiked")
                    let intLiked = likedCount != "" ? Int(likedCount)! - 1 : 0
                    likedCount = "\(intLiked)"
                    mutDict.setObject(likedCount, forKey: "likecount")
                    colleViewCell.likeBtn.selected = false
                    colleViewCell.likeLbl.text = mutDict.objectForKey("likecount") as? String
                }
                else{
                    mutDict.setObject("1", forKey: "isLiked")
                    let intLiked = likedCount != "" ? Int(likedCount)! + 1 : 1
                    likedCount = "\(intLiked)"
                    mutDict.setObject(likedCount, forKey: "likecount")
                    colleViewCell.likeBtn.selected = true
                    colleViewCell.likeLbl.text = mutDict.objectForKey("likecount") as? String
                }
                productArray.removeObjectAtIndex(indexPath.row)
                productArray.insertObject(mutDict, atIndex: indexPath.row)
                
//                m_myPostCollView.reloadItemsAtIndexPaths([indexPath])
            }
            else
            {
                
                let likeBtn = m_bgImgView.viewWithTag(3) as! UIButton
                let likeBigBtn = m_bgImgView.viewWithTag(4) as! UIButton
                likeBtn.userInteractionEnabled = true
                likeBigBtn.userInteractionEnabled = true
                if likeBtn.selected
                {
                    likeBtn.selected = false
                    likeBigBtn.selected = false
                }
                else{
                    likeBtn.selected = true
                    likeBigBtn.selected = true
                }
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                    if self.m_isFromCameraBtn == true
                    {
                        self.createServiceForMyProfile()
                    }
                    else
                    {
                        self.createServiceForOtherUserProfile()
                    }
                }
            }
            
            //self.callServiceForGetMyFeeds()
        }
        else{
            showAlertViewWithMessage((resultDict.objectForKey("msg")as? String!)!)
        }
    }

    
    func API_CALLBACK_addProductToCart(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            showAlertViewWithMessage((resultDict.objectForKey("result")as? String!)!)
            let colleViewCell = self.m_myPostCollView.cellForItemAtIndexPath(indexPath) as! IBMyProductsCollectionViewCell
            //                let bgImgView = colleViewCell?.viewWithTag(indexPath.item) as! UIImageView
            //                let likeBtn = bgImgView.viewWithTag(bgImgView.tag * 10) as! UIButton
            let msgStr = resultDict.objectForKey("result")as? String!
            if msgStr == "Product added to cart successfully"
            {
                colleViewCell.cartBtn.selected = true
                colleViewCell.cartbigBtn.selected = true
            }

            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_cartcount(getUserIdFromUserDefaults())
            }
            
        }
        else
        {
            showAlertViewWithMessage((resultDict.objectForKey("result")as? String!)!)
        }
    }
    
    func API_CALLBACK_cartcount(resultDict: NSDictionary)
    {
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            let cartCountStr = resultDict.objectForKey("result")as! String
            saveCartCountInUserDefaults(cartCountStr)
            cartcountLbl.text = cartCountStr
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
        
    }
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
        
    }
    func API_CALLBACK_deleteImageFromPost(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"
        {
            m_deleteImgView .removeFromSuperview()
            m_deleteImgView = nil
            m_bgImgView.removeFromSuperview()
            m_bgImgView = nil
            // m_myPostCollView.reloadData()
            self.createServiceForMyProfile()
        }
        else{
            showAlertViewWithMessage(resultDict.objectForKey("msg") as! String)
        }
    }
    func API_CALLBACK_Uploadprofilepic(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        
        let errorCode = resultDict.objectForKey("error_code")as! String
        if errorCode == "1"
        {
            showAlertViewWithMessage("Profile pic updated successfully")
        }
        else{
            showAlertViewWithMessage("Something went wrong")
        }
    }
    func API_CALLBACK_deleteproduct(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        
        let errorCode = resultDict.objectForKey("error_code")as! String
        if errorCode == "1"
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                self.createServiceForMyProfile()
            }
            showAlertViewWithMessage(resultDict.objectForKey("result") as! String)
        }
        else{
            showAlertViewWithMessage("Something went wrong")
        }

    }
    func API_CALLBACK_checkDiaryAccess(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        
        let errorCode = resultDict.objectForKey("error_code")as! String
        if errorCode == "1"
        {
            let accessStr = resultDict.objectForKey("access")as! String
            if accessStr == "false"
            {
                showAlertViewWithMessage("You cant access the diary")
            }
            else
            {
                let calenderScreen = IBMydiaryCalendarViewController()
                calenderScreen.userIdStr = profileEntity.userId
                self.navigationController?.pushViewController(calenderScreen, animated: true)
            }
        }
        else{
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    // api call back for follow
    func API_CALLBACK_follow(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.createServiceForOtherUserProfile()
            })
            //m_searchTblView.reloadData()
        }
        else{
            showAlertViewWithMessage((resultDict.objectForKey("msg")as? String!)!)
        }
    }
    
    func API_CALLBACK_Shareproduct(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            showAlertViewWithMessage((resultDict.objectForKey("result")as? String!)!)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.createServiceForMyProfile()
            })
        }
        else
        {
            showAlertViewWithMessage((resultDict.objectForKey("result")as? String!)!)
        }

    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
