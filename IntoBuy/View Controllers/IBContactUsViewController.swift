//
//  IBContactUsViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 05/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBContactUsViewController: IBBaseViewController,UITableViewDataSource,UITableViewDelegate,ContactProtocol {
    
    var contactUsTblView = UITableView()
    var contactUsArray:NSArray = ["Report suspected Fraud","General Enquiries & Feedback","Deactivate Account"]
    var contactUsBgView:UIView!
    
    override func viewDidLoad() {
        bottomVal = 5
        toShowActivityBtn = true
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.m_settingsButton.hidden = true
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_titleLabel.text = Settings_Title
        createContactForContactUs()
    }
    func createContactForContactUs()
    {
        let titleLbl = UILabel(frame : CGRectMake(GRAPHICS.Screen_X() + 10, self.m_bgImageView.bounds.origin.y,GRAPHICS.Screen_Width() - 20 , 70))
        if GRAPHICS.Screen_Type() == IPHONE_4 || GRAPHICS.Screen_Type() == IPHONE_5{
            titleLbl.frame.size.height = 50
        }
        titleLbl.text = Settings_ContactUs
        titleLbl.font = GRAPHICS.FONT_BOLD(12)
        self.m_bgImageView.addSubview(titleLbl)
        addBottomBorder(titleLbl)
        
        contactUsTblView.frame = CGRectMake(GRAPHICS.Screen_X(), titleLbl.frame.maxY, GRAPHICS.Screen_Width(), self.m_bgImageView.frame.size.height - titleLbl.frame.size.height)
        contactUsTblView.delegate = self
        contactUsTblView.dataSource = self
        contactUsTblView.separatorStyle = .None
        contactUsTblView.scrollEnabled = false
        self.m_bgImageView .addSubview(contactUsTblView)
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactUsArray.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell:IBSettingsTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBSettingsTableViewCell
        
        if (cell == nil) {
            cell = IBSettingsTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        cell?.m_titleLbl.text = contactUsArray.objectAtIndex(indexPath.row) as? String
        cell?.frame.size.height = 40
        cell?.showOrHideSwitchBtn(true)
        if indexPath.row == contactUsArray.count - 1
        {
            cell?.hideBottomLine(true)
        }
        else
        {
            cell?.hideBottomLine(false)
        }
        cell?.m_arrowBtn.hidden = false;
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row == 1
        {
            self.m_bgImageView.alpha = 0.5
            contactUsBgView = UIView(frame: self.view.bounds)
            self.view.addSubview(contactUsBgView)
            
            let contactUsView = IBContactUsView(frame: CGRectMake(GRAPHICS.Screen_X() + 15 , self.contactUsBgView.bounds.origin.y + 70,GRAPHICS.Screen_Width() - 30, self.m_bgImageView.frame.size.height - 100))
            contactUsView.alpha = 1
            contactUsView.btnDelegate = self
            contactUsView.layer.borderColor = UIColor.blackColor().CGColor
            contactUsView.layer.borderWidth = 1.0
            contactUsBgView.addSubview(contactUsView)
        }
        
    }
    func addBottomBorder(view: UIView)
    {
        let lineImg = GRAPHICS.SEARCH_DIVIDER_IMAGE()
        let bottomBorder = UIImageView(frame: CGRectMake(view.bounds.origin.x , view.frame.size.height-1.0, view.frame.size.width, 1.0))
        bottomBorder.image = lineImg
        view.addSubview(bottomBorder)
    }
    
    func sendFeedback(sender: UIButton)
    {
        self.m_bgImageView.alpha = 1
        contactUsBgView.removeFromSuperview()
        contactUsBgView = nil
    }

}
