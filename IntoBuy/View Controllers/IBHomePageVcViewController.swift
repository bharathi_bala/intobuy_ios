//
//  IBHomePageVcViewController.swift
//  IntoBuy
//
//  Created by SmaatApps on 09/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//
import GoogleMobileAds
import UIKit

class IBHomePageVcViewController: IBBaseViewController,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,ServerAPIDelegate,HomeProtocol,UICollectionViewDataSource,UICollectionViewDelegate,UIWebViewDelegate,searchProtocol,UIActionSheetDelegate{
    
    var m_pageArray : NSMutableArray = NSMutableArray()
    var m_view : UIView = UIView()
    var m_shoppingBtn:UIButton = UIButton()
    var m_productsBtn:UIButton = UIButton()
    var m_friendsBtn:UIButton = UIButton()
    var m_scrollview:UIScrollView = UIScrollView()
    var m_shoppingView:UIImageView = UIImageView()
    var m_productsView:UIView = UIView()
    var m_friendsView:UIView = UIView()
    var m_shoppingLabl:UILabel = UILabel()
    var m_productslabl:UILabel = UILabel()
    var m_friendslabl:UILabel = UILabel()
    var m_globeBtn:UIButton = UIButton()
    var m_searchTblView:UITableView!
    var m_productTblView:UITableView!
    var m_searchArray = NSMutableArray()
    var m_feedArray = NSMutableArray()
    var m_bgImgView:UIView!
    var m_headerBgView:UIView!
    var m_isDisplay = Bool()
    var m_bannerCollView:UICollectionView!
    var m_bannerArray = NSMutableArray()
    var m_categoryCollView:UICollectionView!
    var m_categoryArray = NSMutableArray()
    var imageCurrentIndex = NSInteger()
    var gridView:GridView?
    var m_catScrlView:UIScrollView!
    var m_tapGestureTag:NSInteger!
    var pageControl:UIPageControl!
    var productScrlView:UIScrollView!
    var m_bottomArray = NSMutableArray()
    var bottomView = UIView()
    var myActivityIndicator: UIActivityIndicatorView!
    var m_likeBtnTag = NSInteger()
    var copyAlertView:UIView!
    
    var adView = UIView()
    var bannerView = GADBannerView()
    var textToShare = ""//shareText1
    var imageToShare : UIImage!
    var strurl : NSURL!
    var contactsBtn = UIButton()
    
    var facebookStatus = ""
    var twitterStatus = ""
    
    var lastPageOffset = CGPoint()
    var shouldScrollLastPage = false
    
    override func viewDidLoad()
    {
        bottomVal = 0
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        m_isDisplay = false
        
        self.hideBottomView(false)
        self.hideHeaderViewForView(false)
        self.hideBackBtn(true)
        self.m_titleLabel.text = Intobuy
        self .hideSettingsBtn(true)
        self.createHeaders()
        self .createControls()
        //  m_bottomArray = imageArrayForHomePage()
        //self.calculateTheTimeDifference()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self.callServiceForSearch()
        })
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
        toShowActivityBtn = true
        self.createViewForCategories()
        self.callServiceForGetMyFeeds(false)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self.callServiceForBanner()
        })
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self.callServiceForCategory()
        })
        m_scrollview.contentOffset = CGPointMake(GRAPHICS.Screen_Width(), m_scrollview.bounds.origin.y)
        
        //        if(m_bannerCollView != nil){
        //        if(self.navigationController?.visibleViewController == self)
        //        {
        //            self.performSelector("animateViewAutomatically", withObject: nil, afterDelay: 3)
        //        }
        //        }
        
        if shouldScrollLastPage
        {
            shouldScrollLastPage = false
            m_scrollview.setContentOffset(lastPageOffset, animated: false)
        }
    }
    //MARK: - service for banner
    func callServiceForBanner()
    {
        let userDetailUD = NSUserDefaults()
        let userIdStr = userDetailUD.objectForKey("user_id") as! String;
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getActiveBanners(userIdStr)
        
    }
    
    //MARK: - service for category
    func callServiceForCategory()
    {
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getActiveCategories()
    }
    
    //MARK: - service for search
    func callServiceForSearch()
    {
        let userDetailUD = NSUserDefaults()
        let userIdStr = userDetailUD.objectForKey("user_id") as! String;
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getAllUsers(userIdStr)
    }
    //MARK: - service for my feeds
    func callServiceForGetMyFeeds(isToRefresh : Bool)
    {
        if !isToRefresh{
            SwiftLoader.show(title:Loading, animated:true)
        }
        let userIdStr = getUserIdFromUserDefaults()//userDetailUD.objectForKey("user_id") as! String;
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getMyFeeds(userIdStr)
    }
    //Mark: - header view controls
    func createHeaders()
    {
        var xPos:CGFloat = m_headerView.frame.origin.x + 5;
        var ypos:CGFloat!
        
        
        let webImageIcon = GRAPHICS.HOMEPAGE_WEB_ICON()
        var width = (webImageIcon.size.width)/2;
        var height = (webImageIcon.size.height)/2;
        if (GRAPHICS.Screen_Type() == IPHONE_6) {
            width = webImageIcon.size.width/1.6;
            height = webImageIcon.size.height/1.6;
        }
        else if (GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = webImageIcon.size.width/1.4;
            height = webImageIcon.size.height/1.4;
        }
        ypos = m_headerView.frame.height - height - 5
        //creating the buttons
        m_globeBtn.frame = CGRectMake(xPos, ypos, width, height)
        m_globeBtn.backgroundColor = UIColor.clearColor()
        m_globeBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_WEB_ICON(), forState: .Normal)
        
        // m_globeBtn .setBackgroundImage(GRAPHICS.PROFILE_RATING_ICON_IMAGE(), forState: .Selected)
        
        m_globeBtn .addTarget(self, action:#selector(IBHomePageVcViewController.globeBtnPressed(_:)), forControlEvents:.TouchUpInside)
//        m_headerView .addSubview(m_globeBtn)
        
        
        let shoppingIcon = GRAPHICS.HOMEPAGE_POINT_NORMAL()
        width = (shoppingIcon.size.width)/2;
        height  = (shoppingIcon.size.height)/2;
        xPos = m_headerView.frame.width/2.3
        ypos = m_headerView.frame.size.height - height - 2
        
        //creating the buttons
        m_shoppingBtn.frame = CGRectMake(xPos, ypos, width, height)
        m_shoppingBtn.backgroundColor = UIColor.clearColor()
        m_shoppingBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_NORMAL(), forState: .Normal)
        
        m_shoppingBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_SELECTED(), forState: .Selected)
        
        m_shoppingBtn .addTarget(self, action:#selector(IBHomePageVcViewController.buttonsPressed(_:)), forControlEvents:.TouchUpInside)
        m_shoppingBtn.tag = 1111
        m_headerView .addSubview(m_shoppingBtn)
        
        xPos = m_shoppingBtn.frame.maxX+10;
        
        //creating the buttons
        m_productsBtn.frame = CGRectMake(xPos, ypos, width, height)
        m_productsBtn.backgroundColor = UIColor.clearColor()
        m_productsBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_NORMAL(), forState: .Normal)
        
        m_productsBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_SELECTED(), forState: .Selected)
        m_productsBtn .addTarget(self, action:#selector(IBHomePageVcViewController.buttonsPressed(_:)), forControlEvents:.TouchUpInside)
        m_productsBtn.tag = 2222
        m_headerView .addSubview(m_productsBtn)
        
        xPos = m_productsBtn.frame.maxX+10;
        
        m_friendsBtn.frame = CGRectMake(xPos, ypos, width, height)
        m_friendsBtn.backgroundColor = UIColor.clearColor()
        m_friendsBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_NORMAL(), forState: .Normal)
        m_friendsBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_SELECTED(), forState: .Selected)
        m_friendsBtn .addTarget(self, action:#selector(IBHomePageVcViewController.buttonsPressed(_:)), forControlEvents:.TouchUpInside)
        m_friendsBtn.tag = 3333
        m_headerView .addSubview(m_friendsBtn)
        
        let contactsBtnImg = GRAPHICS.BOTTOM_BAR_CHAT_ICON_BTN_IMAGE()
        //let contentSize = GRAPHICS.getImageWidthHeight(contactsBtnImg)
        width = contactsBtnImg.size.width/2;
        height = contactsBtnImg.size.height/2;
        ypos = m_headerView.frame.size.height - height - 5
        xPos = m_headerView.frame.size.width - width - 5
        contactsBtn.frame = CGRectMake(xPos, ypos, width, height)
        contactsBtn.backgroundColor = UIColor.clearColor()
        contactsBtn .setBackgroundImage(contactsBtnImg, forState: .Normal)
        contactsBtn .addTarget(self, action:#selector(IBHomePageVcViewController.contactsBtnPressed(_:)), forControlEvents:.TouchUpInside)
        m_headerView .addSubview(contactsBtn)
        
        
        //creationOFFreindsBtn on header
        /*      let friendsBtn:UIButton = UIButton()
         friendsBtn.frame = CGRectMake(posx, posY, Width, height)
         friendsBtn.addTarget(self, action: "friendsBtnAction:", forControlEvents: UIControlEvents.TouchUpInside)
         friendsBtn.setBackgroundImage(GRAPHICS.HOMEPAGE_FRIENDS_PROFILE(), forState: UIControlState.Normal)
         //   m_headerView .addSubview(friendsBtn)
         
         
         posx = friendsBtn.frame.maxX+20
         
         //creation Of Menu button On Header
         let menuBtn:UIButton = UIButton(frame: CGRectMake(posx, posY, Width, height))
         //menuBtn.frame = CGRectMake(posx, posY, Width, height)
         menuBtn.addTarget(self, action: "menuBtnAuction:", forControlEvents: UIControlEvents.TouchUpInside)
         menuBtn.setBackgroundImage(GRAPHICS.HOMEPAGE_ADDMENU_ICON(), forState: UIControlState.Normal)
         //     m_headerView .addSubview(menuBtn)
         
         
         
         posx = self.m_headerView.frame.width-15-Width
         
         
         //creation Of Menu button On Header
         let cartBtn:UIButton = UIButton()
         cartBtn.frame = CGRectMake(posx, posY, Width, height)
         cartBtn.addTarget(self, action: "cartBtnAction:", forControlEvents: UIControlEvents.TouchUpInside)
         cartBtn.setBackgroundImage(GRAPHICS.HOMEPAGE_FRIENDS_SHOP_ICON(), forState: UIControlState.Normal)
         //  m_headerView .addSubview(cartBtn)
         
         posx = cartBtn.frame.origin.x-50
         
         //creation Of Menu button On Header
         let messagesbtn:UIButton = UIButton()
         messagesbtn.frame = CGRectMake(posx, posY, Width, height)
         messagesbtn.addTarget(self, action: "messagesbtnAction:", forControlEvents: UIControlEvents.TouchUpInside)
         messagesbtn.setBackgroundImage(GRAPHICS.HOMEPAGE_FRIENDS_CHAT_ICON(), forState: UIControlState.Normal)
         // m_headerView .addSubview(messagesbtn)    */
        
        
    }
    
    func createControls()
    {
        //Creating the view
        m_view.frame = CGRectMake(GRAPHICS .Screen_X(),m_headerView.frame.maxY, GRAPHICS.Screen_Width(), 36)
        m_view.backgroundColor = UIColor.grayColor()
        //m_bgImageView .addSubview(m_view)
        
        var xPos = m_view.frame.width/3
        var ypos : CGFloat = 2
        //        var width : CGFloat = 20
        //        var height:CGFloat = 20
        
        let webImageIcon = GRAPHICS.HOMEPAGE_WEB_ICON()
        var width:CGFloat = (webImageIcon.size.width)/2;
        var height:CGFloat = (webImageIcon.size.height)/2;
        
        //creating the buttons
        /*        m_globeBtn.frame = CGRectMake(GRAPHICS.Screen_X()+15, ypos, width, height)
         m_globeBtn.backgroundColor = UIColor.clearColor()
         m_globeBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_WEB_ICON(), forState: .Normal)
         
         // m_globeBtn .setBackgroundImage(GRAPHICS.PROFILE_RATING_ICON_IMAGE(), forState: .Selected)
         
         m_globeBtn .addTarget(self, action:"globeBtnPressed:", forControlEvents:.TouchUpInside)
         m_view .addSubview(m_globeBtn)
         
         
         let shoppingIcon = GRAPHICS.HOMEPAGE_POINT_NORMAL()
         width = (shoppingIcon.size.width)/2;
         height  = (shoppingIcon.size.height)/2;
         
         //creating the buttons
         m_shoppingBtn.frame = CGRectMake(xPos - 20, ypos, width, height)
         m_shoppingBtn.backgroundColor = UIColor.clearColor()
         m_shoppingBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_NORMAL(), forState: .Normal)
         
         m_shoppingBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_SELECTED(), forState: .Selected)
         
         m_shoppingBtn .addTarget(self, action:"buttonsPressed:", forControlEvents:.TouchUpInside)
         m_shoppingBtn.tag = 1111
         m_view .addSubview(m_shoppingBtn)
         
         xPos = m_shoppingBtn.frame.maxX+20;
         
         //creating the buttons
         m_productsBtn.frame = CGRectMake(xPos, ypos, width, height)
         m_productsBtn.backgroundColor = UIColor.clearColor()
         m_productsBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_NORMAL(), forState: .Normal)
         
         m_productsBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_SELECTED(), forState: .Selected)
         m_productsBtn .addTarget(self, action:"buttonsPressed:", forControlEvents:.TouchUpInside)
         m_productsBtn.tag = 2222
         m_view .addSubview(m_productsBtn)
         
         xPos = m_productsBtn.frame.maxX+20;
         
         m_friendsBtn.frame = CGRectMake(xPos, ypos, width, height)
         m_friendsBtn.backgroundColor = UIColor.clearColor()
         m_friendsBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_NORMAL(), forState: .Normal)
         
         m_friendsBtn .setBackgroundImage(GRAPHICS.HOMEPAGE_POINT_SELECTED(), forState: .Selected)
         m_friendsBtn .addTarget(self, action:"buttonsPressed:", forControlEvents:.TouchUpInside)
         m_friendsBtn.tag = 3333
         m_view .addSubview(m_friendsBtn)*/
        
        xPos = GRAPHICS .Screen_X()
        ypos = m_headerView.frame.maxY
        width = GRAPHICS.Screen_Width()
        height = GRAPHICS.Screen_Height()-110
        
        m_scrollview.frame = CGRectMake(xPos, ypos, width, height)
        m_scrollview.backgroundColor = UIColor.clearColor()
        m_scrollview.tag = 1
        m_scrollview.pagingEnabled = true
        m_scrollview.delegate = self
        m_bgImageView .addSubview(m_scrollview)
        
        /*
         xPos = GRAPHICS .Screen_X()
         ypos = m_view.frame.maxY
         width = GRAPHICS.Screen_Width()
         height = GRAPHICS.Screen_Height()-m_view.frame.maxY
         */
        
        ypos = 0
        
        
        m_shoppingView.frame = CGRectMake(xPos, ypos, width, height)
        m_shoppingView.image = GRAPHICS.PRODUCT_BG_IMAGE()
        m_shoppingView.userInteractionEnabled = true
        m_scrollview .addSubview(m_shoppingView)
        
        //Creation of shoppingLabel
        //        m_shoppingLabl.frame = CGRectMake(0, 150, GRAPHICS.Screen_Width(), 30)
        //        m_shoppingLabl.text = "shopping"
        //        m_shoppingLabl.textAlignment = NSTextAlignment.Center
        //        m_shoppingView .addSubview(m_shoppingLabl)
        
        xPos = m_shoppingView.frame.maxX
        
        m_productsView.frame = CGRectMake(xPos, ypos, width, height)
        m_productsView.backgroundColor = UIColor.clearColor()
        m_scrollview .addSubview(m_productsView)
        
        // create product table view
        m_productTblView = UITableView(frame: m_productsView.bounds)
        m_productTblView.delegate = self
        m_productTblView.dataSource = self
        m_productTblView.tag = 1
        m_productTblView.separatorStyle = UITableViewCellSeparatorStyle.None
        m_productsView.addSubview(m_productTblView)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(IBHomePageVcViewController.refreshTheDatas(_:)), forControlEvents: .ValueChanged)
        m_productTblView.addSubview(refreshControl)
        
        
        xPos = m_productsView.frame.maxX
        
        m_friendsView.frame = CGRectMake(xPos, ypos, width, height)
        m_friendsView.backgroundColor = UIColor.clearColor()
        m_scrollview .addSubview(m_friendsView)
        
        ypos = m_friendsView.bounds.origin.y
        xPos = m_friendsView.bounds.origin.x
        height = 50
        width = m_friendsView.frame.size.width
        adView.frame = CGRectMake(xPos, ypos, width, height)
        
        m_friendsView.addSubview(adView)
        
        //print("Google Mobile Ads SDK version: %@", GADRequest.sdkVersion())
        self.bannerView.frame = adView.bounds
        self.bannerView.adUnitID = "ca-app-pub-6854884534976526/2950531096"
        self.bannerView.rootViewController = self
        //  self.bannerView.backgroundColor = UIColor.greenColor()
        self.bannerView.loadRequest(DFPRequest())
        self.adView.addSubview(self.bannerView)
        
        //  let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) as dispatch_queue_t
        
        //        dispatch_async(dispatch_get_main_queue()) {
        //            self.bannerView.adUnitID = "ca-app-pub-6854884534976526/2950531096"
        //            self.bannerView.rootViewController = self
        //            dispatch_async(dispatch_get_main_queue()) {
        //                let gadRequest = GADRequest.request()
        //                self.bannerView.loadRequest(gadRequest)
        //            }
        //        }
        
        ypos = adView.frame.maxY
        height = m_friendsView.frame.size.height - adView.frame.size.height
        
        
        //Creation of search table
        m_searchTblView = UITableView(frame: CGRectMake(xPos, ypos, width, height))
        m_searchTblView.delegate = self
        m_searchTblView.dataSource = self
        m_searchTblView.tag = 2
        m_searchTblView.separatorStyle = .None
        m_friendsView.addSubview(m_searchTblView)
        
        m_scrollview.contentOffset = CGPointMake(GRAPHICS.Screen_Width(), m_scrollview.frame.origin.y)
        m_productsBtn.selected = true
        m_scrollview.contentSize = CGSizeMake(m_friendsView.frame.maxX, height)
        
    }
    
    func refreshTheDatas(refreshControl : UIRefreshControl)
    {
        refreshControl.endRefreshing()
        callServiceForGetMyFeeds(true)
    }
    
    //MARK: - create controls for categories
    func createViewForCategories()
    {
        
        m_catScrlView = UIScrollView(frame: m_shoppingView.frame)
        m_catScrlView.tag = 2
        m_catScrlView.bounces = false
        m_shoppingView.addSubview(m_catScrlView)
        
        let posX = m_shoppingView.bounds.origin.x
        let posY = m_shoppingView.bounds.origin.y
        let width = m_shoppingView.frame.size.width
        let height = m_shoppingView.frame.size.height/2.5
        
        let advertiseMentView = UIView(frame: CGRectMake(posX,posY,width,height))
        advertiseMentView.backgroundColor = UIColor.clearColor()
        m_catScrlView.addSubview(advertiseMentView)
        
        let bannerlayOut = UICollectionViewFlowLayout()
        bannerlayOut.itemSize =  CGSizeMake(width, height)
        bannerlayOut.scrollDirection = .Horizontal
        bannerlayOut.minimumInteritemSpacing = 0;
        bannerlayOut.minimumLineSpacing = 0;
        bannerlayOut.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        if(m_bannerCollView != nil){
            m_bannerCollView.removeFromSuperview()
            m_bannerCollView = nil
        }
        
        m_bannerCollView = UICollectionView(frame: CGRectMake(posX,posY,width,height), collectionViewLayout: bannerlayOut)
        m_bannerCollView.pagingEnabled = true
        m_bannerCollView.bounces = false
        m_bannerCollView.registerClass(IBBannerCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "cellIdentifier")
        m_bannerCollView.delegate = self
        m_bannerCollView.dataSource = self
        advertiseMentView.addSubview(m_bannerCollView)
        
        let bannerLbl = UILabel(frame :advertiseMentView.frame)
        bannerLbl.textAlignment = .Center
        bannerLbl.text = "Sell. Share. Feel young again!"
        bannerLbl.font = GRAPHICS.FONT_JENNASUE(37)
        bannerLbl.userInteractionEnabled = false
        bannerLbl.textColor = UIColor.whiteColor()
        advertiseMentView.addSubview(bannerLbl)
        advertiseMentView.bringSubviewToFront(bannerLbl);
        //        if(m_bannerArray.count > 0){
        //        self.performSelector("animateViewAutomatically", withObject: nil, afterDelay: 3)
        //        }
        let categorylayOut = UICollectionViewFlowLayout()
        categorylayOut.itemSize =  CGSizeMake((m_catScrlView.frame.size.width/2) - 10.0, (m_catScrlView.frame.size.width/2.5) - 15.0)
        categorylayOut.scrollDirection = .Vertical
        categorylayOut.sectionInset = UIEdgeInsetsMake(5, 5, 3, 3);
        
        if(m_categoryCollView != nil){
            m_categoryCollView.removeFromSuperview()
            m_categoryCollView = nil
        }
        
        m_categoryCollView = UICollectionView(frame: CGRectMake(m_catScrlView.bounds.origin.x,advertiseMentView.frame.maxY + 8,m_catScrlView.frame.size.width,m_catScrlView.frame.size.height - advertiseMentView.frame.size.height), collectionViewLayout: categorylayOut)
        m_categoryCollView.backgroundColor = UIColor.clearColor()
        m_categoryCollView.scrollEnabled = false
        m_categoryCollView.bounces = true
        m_categoryCollView.registerClass(IBCategoryCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "cellIdentifier")
        m_categoryCollView.delegate = self
        m_categoryCollView.dataSource = self
        m_catScrlView.addSubview(m_categoryCollView)
        m_catScrlView.contentSize = CGSizeMake(m_catScrlView.frame.size.width,m_categoryCollView.frame.maxY + 10)
        
    }
    
    
    func animateViewAutomatically()
    {
        
        // let contentOffsetWhenFullyScrolledRight:CGFloat = (m_categoryCollView.frame.size.width) *  CGFloat(m_bannerArray.count - 1);
        
        if (m_bannerCollView.contentOffset.x == GRAPHICS.Screen_Width()) {
            // user is scrolling to the right from the last item to the 'fake' item 1.
            // reposition offset to show the 'real' item 1 at the left-hand end of the collection view
            if m_bannerArray.count > 2
            {
            let newIndexPath = NSIndexPath.init(forItem: m_bannerArray.count - 2, inSection: 0);
            m_bannerCollView.scrollToItemAtIndexPath(newIndexPath, atScrollPosition: .Right, animated: true)
        }
        }
        else if (m_bannerCollView.contentOffset.x == 0)  {
            // user is scrolling to the left from the first item to the fake 'item N'.
            // reposition offset to show the 'real' item N at the right end end of the collection view
            
            if m_bannerArray.count > 1
            {
            let newIndexPath = NSIndexPath.init(forItem: m_bannerArray.count - 1, inSection: 0);
            m_bannerCollView.scrollToItemAtIndexPath(newIndexPath, atScrollPosition: .Left, animated: true)
            }
        }
        else
        {
            imageCurrentIndex = Int(round(m_bannerCollView.contentOffset.x / m_bannerCollView.frame.size.width))
            m_bannerCollView.setContentOffset(CGPointMake(CGFloat((Int(imageCurrentIndex) + Int(1))) * CGFloat(m_bannerCollView.frame.size.width), 0), animated: true)
            imageCurrentIndex = Int(round(m_bannerCollView.contentOffset.x / m_bannerCollView.frame.size.width))
            imageCurrentIndex = imageCurrentIndex + 1
        }
        if(self.navigationController?.visibleViewController == self)
        {
            self.performSelector(#selector(IBHomePageVcViewController.animateViewAutomatically), withObject: nil, afterDelay: 3)
        }
        
        let   tempimageCurrentIndex = Int(round(m_bannerCollView.contentOffset.x / m_bannerCollView.frame.size.width));
        
        ////print("animating %d",tempimageCurrentIndex);
    }
    
    //MARK: - tableview delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if tableView.tag == 1
        {
            return m_feedArray.count
        }
        else{
            return 1
        }
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView.tag == 1
        {
            var xPos = GRAPHICS.Screen_X()
            var yPos:CGFloat = 0
            var width = GRAPHICS.Screen_Width()
            var height:CGFloat = 50
            
            
            let bgView = UIView(frame: CGRectMake(xPos,yPos,width,height))
            bgView.backgroundColor = UIColor.whiteColor()
            
            let feedEntity = m_feedArray.objectAtIndex(section)as! IBMyFeedEntity
            let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
            xPos = bgView.bounds.origin.x + 10
            yPos = bgView.bounds.origin.y +  5
            width = profileMaskImg.size.width/2
            height = profileMaskImg.size.height/2;
            let profileImgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
            
            //profileImgView.image = UIImage(contentsOfFile :(resultDict.objectForKey("userProfile")as? String)!)
            //profileImgView.load(NSURL(string: feedEntity.userProfile as String)!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler:nil)
            profileImgView.sd_setImageWithURL(NSURL(string: feedEntity.userProfile as String)!, placeholderImage: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), options: .ScaleDownLargeImages)
            profileImgView.clipsToBounds = true
            profileImgView.layer.cornerRadius = height/2
            profileImgView.backgroundColor = UIColor.whiteColor()
            profileImgView.tag = section
            bgView.addSubview(profileImgView)
            profileImgView.userInteractionEnabled = true
            profileImgView.contentMode = .ScaleToFill
            
            let profileTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBHomePageVcViewController.profileImgAction(_:)))
            profileTapGesture.numberOfTapsRequired = 1
            profileImgView.addGestureRecognizer(profileTapGesture);
            
            height = 15
            xPos = profileImgView.frame.maxX + 5
            yPos = profileImgView.frame.maxY/2 - height/2
            width = GRAPHICS.Screen_Width()/2
            let profileNameLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
            profileNameLbl.text = feedEntity.username as String
            profileNameLbl.font = GRAPHICS.FONT_REGULAR(12)
            if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
            {
                profileNameLbl.font = GRAPHICS.FONT_REGULAR(13)
            }
            
            bgView.addSubview(profileNameLbl)
            
            let addedDate = feedEntity.added_date as String
            let addedDateStr = convertUtcToLocalFormat(addedDate)
            let dateFormat:NSDateFormatter = NSDateFormatter()
            dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormat.timeZone = NSTimeZone.localTimeZone()
            let startDate = dateFormat.dateFromString(addedDateStr)
            let intervalString = startDate!.relativeTimeToString()
            //print("\(addedDateStr), \(intervalString)")
            
            
            width = 100
            //let currentDate = NSDate()
            xPos = GRAPHICS.Screen_Width() - width - 20
            yPos = profileNameLbl.frame.origin.y//bgView.bounds.origin.y + 10
            let dateLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
            dateLbl.font = GRAPHICS.FONT_REGULAR(12)
            if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
            {
                dateLbl.font = GRAPHICS.FONT_REGULAR(13)
            }
            
            dateLbl.text = intervalString
            dateLbl.textAlignment = .Right
            bgView.addSubview(dateLbl)
            //dateLbl.sizeToFit()
            
            bgView.frame.size.height = profileImgView.frame.maxY + 5
            
            
            
            return bgView
        }
        else{
            return nil
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(tableView.tag == 1)
        {
            if(GRAPHICS.Screen_Type() == IPHONE_5 || GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
            {
                let feedEntity = m_feedArray.objectAtIndex(indexPath.section)as! IBMyFeedEntity
                if feedEntity.wall_type == "1"
                {
                    return GRAPHICS.Screen_Height()/1.3;
                }
                else
                {
                    return GRAPHICS.Screen_Height()/1.4;
                }
            }
            else
            {
                return GRAPHICS.Screen_Height()/1.1;
            }
        }
        else
        {
            return 70.0
        }
    }
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(tableView.tag == 1){
            return 50;
        }
        else{
            return 0.0
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView.tag == 1){
            return 1
        }
        else{
            return m_searchArray.count
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //let cellIdentifier = "Cell \(indexPath.row) \(indexPath.section)"
        let cellIdentifier = "homeCell"
        if(tableView == m_searchTblView)
        { // search peple
            var cell:IBSearchTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBSearchTableViewCell
            
            if (cell == nil) {
                cell = IBSearchTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            }
            SwiftLoader.hide()
            var folloStatus = "0"
            if m_searchArray.count > 0{
                let resultDict = m_searchArray.objectAtIndex(indexPath.row)as! NSDictionary
                let followStatus = resultDict.objectForKey("amIfollowing")as? String!;
                if(followStatus! == "1")
                {
                    folloStatus = "1"
                }
                else{
                    folloStatus = "0"
                }
                cell?.imgDelegate = self
                cell?.selectionStyle = UITableViewCellSelectionStyle.None
                cell?.valuesForControls((resultDict.objectForKey("avatar")as? String!)!, name: (resultDict.objectForKey("username")as? String!)!, followState: folloStatus, trophyStr: (resultDict.objectForKey("buytrophy")as? String!)!,isFromHome: true ,profileImgTag: indexPath.row * 10)
                cell?.ShowOrHideTrophyLbl(false)
                cell?.isFromHome = true
                cell?.m_followBtn.tag = indexPath.row
                cell?.m_followBtn.setBackgroundImage(UIImage(named: "intobuy-homepage-friends-unfollow-button.png"), forState: .Normal)
                //cell?.m_followBtn.setTitle("Follow", forState: .Normal)
                //cell?.m_followBtn.setTitle("Unfollow", forState: .Selected)
                cell?.m_followBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(12)
                cell?.m_followBtn.addTarget(self, action: #selector(IBHomePageVcViewController.followBtnTarget(_:)), forControlEvents: .TouchUpInside)
            }
            return cell!;
        }
            
        else{// home page
            SwiftLoader.hide()
            var cell:IBProductsTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBProductsTableViewCell
            
            if (cell == nil) {
                cell = IBProductsTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            }
            cell?.btnDelegate = self
            
            cell?.m_shareBtn.tag = 1000 * indexPath.section
            
            let feedEntity = m_feedArray.objectAtIndex(indexPath.section)as! IBMyFeedEntity
            let likedPersonsArray = feedEntity.likedUserdetails
            var firstNameStr:String!
            var secondNamestr:String!
            for i in 0 ..< likedPersonsArray.count
            {
                let likedPersonDict = likedPersonsArray.objectAtIndex(i) as! NSDictionary
                let likesUserName = likedPersonDict.objectForKey("username") as! String
                if i == 0
                {
                    firstNameStr = likesUserName
                }
                if i == 1
                {
                    secondNamestr = likesUserName
                }
            }
            
//            let remainingCount = likedPersonsArray.count - (likedPersonsArray.count - 2)
            var likeString:String!
            likeString = likedPersonsArray.count > 0 ? (likedPersonsArray.count > 1 ? "\(likedPersonsArray.count) " + "likes" : "\(likedPersonsArray.count) " + "like") : ""
            
            
//            if likedPersonsArray.count == 1
//            {
//                likeString = String(format: "%@ liked this",firstNameStr)
//            }
//            else if likedPersonsArray.count == 2
//            {
//                likeString = String(format: "%@ and %@ liked this",firstNameStr,secondNamestr)
//            }
//            else if likedPersonsArray.count > 2
//            {
//                likeString = String(format: "%@,%@ and %d other liked this",firstNameStr,secondNamestr,remainingCount)
//            }
//            else
//            {
//                likeString = ""
//            }
            if feedEntity.wall_type == "1"
            {
                cell?.setTextToTheControls(feedEntity.catogery_name as String, productStr: feedEntity.product_name as String, likeStr: "" as String , commentArray: feedEntity.Comments, commentStr: feedEntity.isCommented as String, likedNamesStr: likeString , likeTag: indexPath.section, commentTag: 100 + indexPath.section,imageArray:feedEntity.images,isLiked:feedEntity.isLiked as String, toHideCartBtn:false ,scrollViewTag:indexPath.section * 10 , imageStr : feedEntity.product_image as String ,shareBtnTag : 1000 * indexPath.section , cartStatus : feedEntity.is_in_cart as String)
                cell?.m_addCartBtn.tag = 10000 * indexPath.section
            }
            else{
                cell?.setTextToTheControls("", productStr: feedEntity.post_text as String, likeStr: "" as String, commentArray: feedEntity.Comments, commentStr: feedEntity.isCommented, likedNamesStr: likeString , likeTag: indexPath.section, commentTag: 100 + indexPath.section,imageArray:feedEntity.images,isLiked:feedEntity.isLiked as String, toHideCartBtn:true ,scrollViewTag:indexPath.section * 10,imageStr:feedEntity.post_image as String,shareBtnTag: 1000 * indexPath.section, cartStatus : "")
            }
            cell?.m_likePersonsLbl.tag = indexPath.section * 100000
            cell?.selectionStyle = .None
            
            return cell!;
        }
    }
    //MARK: - collection view delegates
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == m_bannerCollView){
            return m_bannerArray.count;
        }
        else{
            return m_categoryArray.count
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if(collectionView == m_bannerCollView){
            let cell:IBBannerCollectionViewCell? = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath) as? IBBannerCollectionViewCell
            let dict = m_bannerArray.objectAtIndex(indexPath.item)as? NSDictionary
            let imageUrlStr = dict?.objectForKey("image")as? String!
            cell?.bannerImgView .load(imageUrlStr!, placeholder: nil, completionHandler: nil)
            
            //        if (cell == nil) {
            //            cell = IBSearchTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            //        }
            return cell!
        }
        else{
            let cell:IBCategoryCollectionViewCell? = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath) as? IBCategoryCollectionViewCell
            let dict = m_categoryArray.objectAtIndex(indexPath.item)as? NSDictionary
            let imageUrlStr = dict?.objectForKey("catimage")as? String!
            let catName = dict?.objectForKey("catname")as? String!
            cell?.m_categoryImgView.load(imageUrlStr!)
            cell?.m_categoryLbl.text = catName
            
            //            collectionView.frame.size.height = collectionView.frame.size.height * CGFloat(m_categoryArray.count)
            //            m_catScrlView.frame.size.height = collectionView.frame.size.height * CGFloat(m_categoryArray.count)//collectionView.frame.maxY
            //            //m_catScrlView.contentSize = CGSizeMake(m_catScrlView.frame.size.width,collectionView.frame.maxY)
            //            //print(m_catScrlView.contentSize)
            //m_catScrlView.frame.size.height = collectionView.frame.size.height * CGFloat(m_categoryArray.count)//collectionView.frame.maxY
            collectionView.frame.size.height =  (cell!.frame.size.height * ((CGFloat(m_categoryArray.count)/2))) + 50.0
            m_catScrlView.contentSize = CGSizeMake(m_catScrlView.frame.size.width,collectionView.frame.maxY + 10)
            return cell!
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if(collectionView == m_bannerCollView)
        {
            let dict = m_bannerArray.objectAtIndex(indexPath.item)as? NSDictionary
            let linkUrl = dict?.objectForKey("url")as? String!
            let openLink = NSURL(string : linkUrl!)
            UIApplication.sharedApplication().openURL(openLink!)
            
        }
        else
        {
            let dict = m_categoryArray.objectAtIndex(indexPath.item)as? NSDictionary
            let subcategoriesArry = IBSubCategoryViewController()
            bottomVal = 0
            lastPageOffset = m_scrollview.contentOffset
            shouldScrollLastPage = true
            subcategoriesArry.m_catID = (dict?.objectForKey("catId") as? String!)!
            self.navigationController?.pushViewController(subcategoriesArry, animated: true)
            
            
        }
    }
    func profileImgAction(tapGesture: UITapGestureRecognizer){
        let feedEntity = m_feedArray.objectAtIndex((tapGesture.view?.tag)!)as! IBMyFeedEntity
        let myPageVC = IBMyPageViewController()
        myPageVC.m_isFromCameraBtn = false
        myPageVC.otherUserId = feedEntity.userId as String
        self.navigationController?.pushViewController(myPageVC, animated: true)
    }
    
    func scrollViewGesture(tapGesture: UITapGestureRecognizer){
        // tap gesture for image view in custom cell
        //print((tapGesture.view?.tag)!)
        m_isDisplay = false
        m_tapGestureTag = (tapGesture.view!.tag) / 10
        let contentOffset = (tapGesture.view!.tag - 1) % 10
       // //print(m_tapGestureTag)
        //print("tag is %d",m_tapGestureTag)
        let indexVal  = (tapGesture.view?.superview?.tag)!/10
        let feedEntity = m_feedArray.objectAtIndex(indexVal)as! IBMyFeedEntity
        if feedEntity.wall_type == "1" {
            let productDetailsVC = IBProductDetailsViewController()
            productDetailsVC.productId = feedEntity.product_id as String
            self.navigationController?.pushViewController(productDetailsVC, animated: true)
            
        }
        else{
            
            m_bgImgView = UIView(frame: CGRectMake (GRAPHICS.Screen_X(),GRAPHICS.Screen_Y(),GRAPHICS.Screen_Width(),GRAPHICS.Screen_Height()))
            m_bgImgView.backgroundColor = UIColor.blackColor()
            self.view.addSubview(m_bgImgView)
            
            let tapGesturebg = UITapGestureRecognizer(target: self, action: #selector(IBHomePageVcViewController.bgTapGesture(_:)))
            m_bgImgView.addGestureRecognizer(tapGesturebg)
            
            
            m_headerBgView = UIView(frame: CGRectMake(0, 0, GRAPHICS.Screen_Width(), 50))
            m_headerBgView.backgroundColor = UIColor.clearColor()
            m_headerBgView.alpha = 1.0
            m_bgImgView.addSubview(m_headerBgView)
            
            
            let titleLbl = UILabel(frame: CGRect(x: 0, y: 0, width: GRAPHICS.Screen_Width(), height: 50))
            m_headerBgView.addSubview(titleLbl)
            titleLbl.text = "Tap anywhere to dismiss"
            titleLbl.textColor = UIColor.whiteColor()
titleLbl.font = GRAPHICS.FONT_BOLD(12)
            titleLbl.textAlignment = .Center
            
            let scrlHeight = GRAPHICS.Screen_Height() - m_headerBgView.frame.maxY
            productScrlView = UIScrollView(frame: CGRectMake(GRAPHICS.Screen_X(),(GRAPHICS.Screen_Height() - scrlHeight)/2,GRAPHICS.Screen_Width(),scrlHeight))
            productScrlView.pagingEnabled = true
            productScrlView.backgroundColor = UIColor.clearColor()
            productScrlView.delegate = self
            m_bgImgView.addSubview(productScrlView)
            
            let resultDict = m_feedArray.objectAtIndex(m_tapGestureTag)as!  IBMyFeedEntity
            let imageArray = resultDict.images;
            
            var xPos = productScrlView.bounds.origin.x
            //            let imageDict = imageArray.objectAtIndex(m_tapGestureTag)as! NSDictionary
            //            let urlStr = imageDict.objectForKey("image")as! String
            //            let imgUrl = NSURL(string: urlStr)
            //self.displayImage(imgUrl! , subView : productScrlView)
                      
            if imageArray.count > 0
            {
                let activityVc = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
                activityVc.center = m_bgImgView.center
                activityVc.startAnimating()
                m_bgImgView.addSubview(activityVc)
                
            for i in 0  ..< imageArray.count
            {
                let imageDict = imageArray.objectAtIndex(i)as! NSDictionary
                let urlStr = imageDict.objectForKey("image")as! String
                let imgUrl = NSURL(string: urlStr)
                let imageView = UIImageView(frame: CGRectMake(xPos,productScrlView.bounds.origin.y,GRAPHICS.Screen_Width(),productScrlView.frame.size.height))
                imageView.contentMode = .ScaleAspectFit
                /*let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) -> Void in
                    
                }
                
                imageView.sd_setImageWithURL(imgUrl, placeholderImage:GRAPHICS.DEFAULT_CART_IMAGE(), completed: block)*/
                imageView.sd_setImageWithURL(imgUrl, placeholderImage: GRAPHICS.DEFAULT_CART_IMAGE(), options: .ScaleDownLargeImages)
                //imageView.setImageWithURL(imgUrl, placeholderImage: GRAPHICS.DEFAULT_CART_IMAGE())
//                imageView.contentMode = .ScaleToFill
               // imageView.setupImageViewerWithImageURL(imgUrl)
                imageView.clipsToBounds = true
                productScrlView.addSubview(imageView)
                xPos = xPos + GRAPHICS.Screen_Width();
               activityVc.stopAnimating()
            }
            }
            
            
            /*for i in 0  ..< imageArray.count
             {
             let imageDict = imageArray.objectAtIndex(i)as! NSDictionary
             let webView = UIWebView(frame: CGRectMake(xPos,productScrlView.bounds.origin.y,GRAPHICS.Screen_Width(),productScrlView.frame.size.height))
             webView.backgroundColor = UIColor.clearColor();
             webView.delegate = self;
             webView.opaque = false;
             webView.stringByEvaluatingJavaScriptFromString("document. body.style.zoom = 5.0;");
             webView.scalesPageToFit = true;
             let urlStr = imageDict.objectForKey("image")as! String
             let imgUrl = NSURL(string: urlStr)
             let request = NSURLRequest(URL: imgUrl!)
             webView.loadRequest(request)
             webView.sizeToFit()
             productScrlView.addSubview(webView)
             xPos = xPos + GRAPHICS.Screen_Width();
             
             }
             }*/
            if(imageArray.count > 1){
                self.pageControl = UIPageControl(frame: CGRectMake((m_bgImgView.frame.size.width - CGFloat(imageArray.count))/2, productScrlView.frame.maxY + 10, 10, 10))
                self.pageControl.numberOfPages = imageArray.count
                self.pageControl.currentPage = contentOffset
                self.pageControl.pageIndicatorTintColor = UIColor.whiteColor()
                self.pageControl.currentPageIndicatorTintColor = UIColor(red: 89.0/255.0, green: 191.0/255.0, blue: 239.0/255.0, alpha: 1.0)
                //  pageControl.addTarget(self, action: Selector("changePage:"), forControlEvents: UIControlEvents.ValueChanged)
                m_bgImgView.addSubview(pageControl)
                
            }
            productScrlView.contentOffset = CGPointMake((CGFloat(contentOffset)) * GRAPHICS.Screen_Width(), productScrlView.bounds.origin.y)
            productScrlView.contentSize = CGSizeMake(GRAPHICS.Screen_Width() * (CGFloat(imageArray.count)), productScrlView.frame.size.height)
            
            let doneImg = GRAPHICS.IMAGE_FULL_SCREEN_DONE_BUTTON()
            let width = doneImg.size.width + 20
            let height = doneImg.size.height + 5
            let doneBtn = UIButton(frame: CGRectMake(GRAPHICS.Screen_Width() - width,GRAPHICS.Screen_Y() + 10,width,height))
            doneBtn.setTitle("done", forState: .Normal)
            doneBtn.titleLabel?.textAlignment = .Right
            doneBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(16)
            doneBtn.addTarget(self, action: #selector(IBHomePageVcViewController.removeTheView(_:)), forControlEvents: .TouchUpInside)
//            m_headerBgView.addSubview(doneBtn)
//            m_headerBgView.frame.size.height = doneBtn.frame.maxY;
            
            
            //                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
            //                    let resultDict = self.m_feedArray.objectAtIndex(self.m_tapGestureTag)as!  IBMyFeedEntity
            //                    let imageArray = resultDict.images;
            //
            //
            //                    let imageDict = imageArray.objectAtIndex(self.m_tapGestureTag)as! NSDictionary
            //                    let urlStr = imageDict.objectForKey("image")as! String
            //                    let imgUrl = NSURL(string: urlStr)
            //
            //                    let indexPath = NSIndexPath(forRow: 0, inSection: (tapGesture.view?.superview!.tag)!/10);
            //                    let tableViewCell = self.m_productTblView.cellForRowAtIndexPath(indexPath) as! IBProductsTableViewCell
            //                    //print(tableViewCell.m_productScrlView.tag)
            //
            //                    let imgView = tableViewCell.m_productScrlView.viewWithTag(tableViewCell.m_productScrlView.tag + contentOffset + 1) as! UIImageView
            //
            //                self.displayImage(imgView, imgUrl: imgUrl!)
            //                }
            
        }
    }
    
    func bgTapGesture(tapGesture: UITapGestureRecognizer)
    {
        m_bgImgView.removeFromSuperview()

        /*
        if m_isDisplay == false
        {
            m_isDisplay = true
            
            UIView.animateWithDuration(0.5, delay: 0.5, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.m_headerBgView.alpha = 1.0
                }, completion: nil)
        }
        else
        {
            m_isDisplay = false
            
            UIView.animateWithDuration(0.5, delay: 0.5, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.m_headerBgView.alpha = 0.0
                }, completion: nil)
        }
 */

    }
    
    func removeTheView(sender:UIButton)
    {
        m_bgImgView.removeFromSuperview()
    }
    //add product to cart button action
    func addProductToCart(sender: UIButton)
    {
        
        let feedEntity = m_feedArray.objectAtIndex(sender.tag/10000)as!  IBMyFeedEntity
        
        if feedEntity.userId == getUserIdFromUserDefaults()
        {
            showAlertViewWithMessage(AddCart_Alert)
        }
        else{
            SwiftLoader.show(animated: true)
            SwiftLoader.show(title: "Loading...", animated: true)
            
            let userIdStr = getUserIdFromUserDefaults()
            
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_addProductToCart(userIdStr, productId: feedEntity.product_id as String, sellerId: feedEntity.seller_id as String)
        }
    }
    
    
    func followBtnTarget(sender:UIButton)
    {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let userDetailUD = NSUserDefaults()
        let userIdStr = userDetailUD.objectForKey("user_id") as! String;
        let resultDict = m_searchArray.objectAtIndex(sender.tag)as! NSDictionary
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_follow(userIdStr, followerId: resultDict.objectForKey("userId") as! String)
        }
    }
    
    func globeBtnPressed(sender:UIButton)
    {
        let shareWorldVC = IBShareWorldViewController()
        self.navigationController?.pushViewController(shareWorldVC, animated: true)
    }
    func contactsBtnPressed(sender:UIButton) -> () {
        let contactsVC = IBContactsViewController()
        bottomVal = 0
        self.navigationController?.pushViewController(contactsVC, animated: true)
        
    }
    //like btn action
    func likeForPost(sender: UIButton)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            m_likeBtnTag = sender.tag;
            //print(sender.tag)
            let userDetailUD = NSUserDefaults()
            //user idb
            let userIdStr = userDetailUD.objectForKey("user_id") as! String;
            let userNameStr = getUserNameFromUserDefaults()
            let feedEntity = m_feedArray.objectAtIndex(sender.tag) as! IBMyFeedEntity
            let wallIdStr = feedEntity.wall_id;//resultDict.objectForKey("wall_id") as! String
            let likeArray = NSMutableArray(array:  feedEntity.likedUserdetails)
            
            if feedEntity.isLiked == "1"
            {
                feedEntity.isLiked = "0"
                for i in 0 ..< feedEntity.likedUserdetails.count
                {
                    let likedDict = feedEntity.likedUserdetails.objectAtIndex(i) as! NSDictionary
                    let likedUserId = likedDict.objectForKey("userId") as! String
                    if userIdStr == likedUserId
                    {
                        likeArray.removeObjectAtIndex(i)
                    }
                }
            }
            else{
                feedEntity.isLiked = "1"
                let likedDict = NSMutableDictionary()
                likedDict.setValue(userIdStr, forKey: "userId")
                likedDict.setValue(userNameStr, forKey: "username")
                likedDict.setValue("", forKey: "email")
                likedDict.setValue("0", forKey: "am_following")
                likedDict.setValue("", forKey: "avatar")
                likeArray.addObject(likedDict)
            }
            
            feedEntity.likedUserdetails = likeArray
            /* if feedEntity.isLiked == "1"
             {
             feedEntity.isLiked = "0"
             if(feedEntity.likedUserNames .containsString(","))
             {
             
             let nameArray = feedEntity.likedUserNames.componentsSeparatedByString(",") as NSArray
             let aMutableArray = (nameArray as NSArray).mutableCopy() as! NSMutableArray
             feedEntity.likedUserNames = "";
             for i in 0  ..< aMutableArray.count
             {
             if aMutableArray.objectAtIndex(i) .containsString(getUserNameFromUserDefaults())
             {
             //  aMutableArray.removeObjectAtIndex(i)
             if i < aMutableArray.count
             {
             feedEntity.likedUserNames = aMutableArray.componentsJoinedByString(",")
             }
             
             }
             }
             }
             else{
             feedEntity.likedUserNames = "";
             }
             }
             else
             {
             feedEntity.isLiked = "1"
             if(feedEntity.likedUserNames != ""){
             feedEntity.likedUserNames = getUserNameFromUserDefaults() + "," + (feedEntity.likedUserNames as String)
             }
             else{
             feedEntity.likedUserNames = getUserNameFromUserDefaults()
             }
             }*/
            //print(feedEntity.images)
            
            m_productTblView.indexPathsForVisibleRows
            m_productTblView.reloadData()
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_addWallLike(wallIdStr as String, userId: userIdStr)
            }
        }
        else{
            showAlertViewWithMessage(NoInternetConnection)
        }
    }
    // move to like label
    func likeLabelGesture(tapGesture: UITapGestureRecognizer)
    {
        
        let feedEntity = m_feedArray.objectAtIndex((tapGesture.view!.tag)/100000) as! IBMyFeedEntity
        let likeArray = feedEntity.likedUserdetails
        let wallIdStr = feedEntity.wall_id
        if likeArray.count > 0
        {
            let likeUserVC = IBLikedUserNamesViewController()
            likeUserVC.likeArray = NSMutableArray(array: likeArray)
            self.navigationController?.pushViewController(likeUserVC, animated: true)
        }
        else{
            
        }
            
    }
    
    // comment post action
    func commentForPost(sender: UIButton)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let resultDict = m_feedArray.objectAtIndex(sender.tag - 100)as! IBMyFeedEntity
            let commentScreen = IBCommentsViewController()
            commentScreen.m_wallIdStr = resultDict.wall_id as String//resultDict.objectForKey("wall_id") as! String
            self.navigationController!.pushViewController(commentScreen, animated: true)
        }
        else{
            showAlertViewWithMessage(NoInternetConnection)
        }
        
        //        let mydiarycalendar = IBAddProductViewController()
        //        self.navigationController?.pushViewController(mydiarycalendar, animated: true)
        
    }
    func shareForPost(sender: UIButton)
    {
        let resultDict = self.m_feedArray.objectAtIndex(sender.tag / 1000) as! IBMyFeedEntity
        //        if resultDict.facebook == "1" && resultDict.twitter == "1"
        //        {
        
        //print(sender.tag)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            let resultDict = self.m_feedArray.objectAtIndex(sender.tag / 1000) as! IBMyFeedEntity
            self.facebookStatus = resultDict.facebook as String
            self.twitterStatus = resultDict.twitter as String
            //print(sender.tag / 1000)
            //        let shareText1 = resultDict.post_text as String
            
            
            if resultDict.wall_type == "2"
            {
                self.textToShare = resultDict.post_text as String
            }
            else
            {
                self.textToShare = resultDict.product_name as String
            }
            
            let indexPath = NSIndexPath(forRow: 0, inSection: sender.tag/1000);
            let tableViewCell = self.m_productTblView.cellForRowAtIndexPath(indexPath) as! IBProductsTableViewCell
            //print(tableViewCell.m_productScrlView.tag)
            //print(sender.tag)
            //        let imageView:UIImageView = tableViewCell.m_productScrlView.viewWithTag(tableViewCell.m_productScrlView.tag + 1) as! UIImageView
            //
            //        let url = NSURL(string: image1)
            //        let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
            //           let ShareImag = UIImage(data: data!)
            
            
            
            let contentOffsetX = tableViewCell.m_productScrlView.contentOffset.x
            if contentOffsetX == 0
            {
                let imgTag = sender.tag/100
                if let firstImageView = tableViewCell.m_productScrlView.viewWithTag(imgTag + 1) as? UIImageView
                {
                    
                self.imageToShare = firstImageView.image!//tableViewCell.m_firstImage.image!
                if resultDict.images.count > 0
                {
                    let imgDict = resultDict.images.objectAtIndex(0) as! NSDictionary
                    self.strurl = NSURL(string: imgDict.objectForKey("image") as! String)!
                }
                else
                {
                    if resultDict.wall_type == "2"
                    {
                        if resultDict.images.count > 0
                        {
                        self.strurl = NSURL(string: resultDict.post_image as String)!
                        }
                    }
                    else
                    {
                        if resultDict.images.count > 0
                        {
                        self.strurl = NSURL(string: resultDict.product_image as String)!
                        }
                    }
                }
                }
            }
            else if contentOffsetX == GRAPHICS.Screen_Width()
            {
                let imgTag = sender.tag/100
                let secondImageView = tableViewCell.m_productScrlView.viewWithTag(imgTag + 2) as! UIImageView
                self.imageToShare = secondImageView.image!
                //self.imageToShare = tableViewCell.m_secondImage.image!
                // if resultDict.images.count > 0{
                let imgDict = resultDict.images.objectAtIndex(1) as! NSDictionary
                self.strurl = NSURL(string:imgDict.objectForKey("image") as! String )!
            }
            else if contentOffsetX == (GRAPHICS.Screen_Width() * 2)
            {
                let imgTag = sender.tag/100
                let thirdImgView = tableViewCell.m_productScrlView.viewWithTag(imgTag + 3) as! UIImageView
                self.imageToShare = thirdImgView.image!
                //   self.imageToShare = tableViewCell.m_thirdImage.image!
                let imgDict = resultDict.images.objectAtIndex(2) as! NSDictionary
                self.strurl = NSURL(string: imgDict.objectForKey("image") as! String)!
            }
            else if contentOffsetX == (GRAPHICS.Screen_Width() * 3)
            {
                let imgTag = sender.tag/100
                let fourthImgView = tableViewCell.m_productScrlView.viewWithTag(imgTag + 4) as! UIImageView
                self.imageToShare = fourthImgView.image!
                
                //            self.imageToShare = tableViewCell.m_fourthImage.image!
                let imgDict = resultDict.images.objectAtIndex(3) as! NSDictionary
                self.strurl = NSURL(string: imgDict.objectForKey("image") as! String)!
            }
        })
        
        //        shareText(shareText1, image1: tableViewCell.m_firstImage.image, url: strurl!)
        
        viewForReportAndShare()
    }
    func friendsBtnAction(sender:UIButton)
    {
        let addproductVc = IBMyPageViewController()
        self.navigationController?.pushViewController(addproductVc, animated: true)
        
    }
    func menuBtnAuction(sender:UIButton)
    {
        /*let mydiarycalendar = IBMydiaryCalendarViewController()
         self.navigationController?.pushViewController(mydiarycalendar, animated: true)*/
        
        
        let settingsVC = IBSettingsViewController()
        self.navigationController!.pushViewController(settingsVC, animated: true)
        
    }
    func cartBtnAction(sender:UIButton)
    {
        let cartVc = IBActivityViewController()
        //        let cartVc = IBListOfCartsViewController()
        self.navigationController?.pushViewController(cartVc, animated: true)
    }
    func messagesbtnAction(sender:UIButton)
    {
        //        let productDetailsVc = IBProductDetailsViewController()
        //        self.navigationController?.pushViewController(productDetailsVc, animated: true)
    }
    func tapOnProfileImage(tapGesture : UITapGestureRecognizer)
    {
        let resultDict = m_searchArray.objectAtIndex((tapGesture.view?.tag)!/10)as! NSDictionary
        let otherUserId = resultDict.objectForKey("userId") as! String
        let myPageVC = IBMyPageViewController()
        myPageVC.m_isFromCameraBtn = false
        myPageVC.otherUserId = otherUserId
        self.navigationController?.pushViewController(myPageVC, animated: true)
        
    }
    func buttonsPressed(sender:UIButton)
    {
        
        var xpos : CGFloat = 0;
        
        // var height:CGFloat = GRAPHICS.Screen_Height()-100
        
        // height = GRAPHICS.Screen_Height()-100
        
        
        //print("buttonclicked \(sender.tag)")
        
        if(sender.tag == 1111)
        {
            m_shoppingBtn.selected = true
            m_productsBtn.selected = false
            m_friendsBtn.selected = false
            self.view .bringSubviewToFront(m_shoppingView)
            m_scrollview.contentOffset = CGPointMake(xpos, 0)
        }
        else if(sender.tag == 2222)
        {
            xpos = GRAPHICS.Screen_Width()*1
            m_shoppingBtn.selected = false
            m_productsBtn.selected = true
            m_friendsBtn.selected = false
            self.view .bringSubviewToFront(m_productsView)
            m_scrollview.contentOffset = CGPointMake(xpos, 0)
            // m_productTblView.reloadData()
        }
        else if(sender.tag == 3333)
        {
            xpos = GRAPHICS.Screen_Width()*2
            m_shoppingBtn.selected = false
            m_productsBtn.selected = false
            m_friendsBtn.selected = true
            self.view .bringSubviewToFront(m_friendsView)
            m_scrollview.contentOffset = CGPointMake(xpos, 0)
        }
    }
    
    
    
    func scrollViewDidScroll(scrollView: UIScrollView)
    {
        if(scrollView.tag == 1){
            if(scrollView == m_scrollview)
            {
                if(scrollView.contentOffset.x == 0)
                {
                    m_shoppingBtn.selected = true
                    m_productsBtn.selected = false
                    m_friendsBtn.selected = false
                    
                }
                else if(scrollView.contentOffset.x == GRAPHICS.Screen_Width())
                {
                    m_shoppingBtn.selected = false
                    m_productsBtn.selected = true
                    m_friendsBtn.selected = false
                    
                }
                else if(scrollView.contentOffset.x == GRAPHICS.Screen_Width()*2)
                {
                    m_shoppingBtn.selected = false
                    m_productsBtn.selected = false
                    m_friendsBtn.selected = true
                    
                }
            }
        }
        else
        {
            
        }
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        if(productScrlView != nil){
            if(productScrlView.contentSize.width > GRAPHICS.Screen_Width()){
                let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                pageControl.currentPage = Int(pageNumber)
            }
        }
    }
    
    // MARK: Api delegate
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    // api call back for get all users
    func API_CALLBACK_getAllUsers(resultDict: NSDictionary)
    {
        dispatch_async(dispatch_get_main_queue()) {
        
        //SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            let searchLocalArr = (resultDict.objectForKey("result")!.mutableCopy() as! NSArray)
            
            self.m_searchArray.removeAllObjects()
            
            if searchLocalArr.count > 0
            {
            // //print(m_searchArray)
            for i in 0  ..< searchLocalArr.count
            {
                let userDict = searchLocalArr.objectAtIndex(i) as! NSDictionary
                let userId = userDict.objectForKey("userId") as! String
                if (userId != getUserIdFromUserDefaults())
                {
                   self.m_searchArray.addObject(userDict)
                }
                
            }
            }
            //m_searchArray = (resultDict.objectForKey("result")!.mutableCopy() as! NSMutableArray)
            //            //print(m_searchArray)
            //            //print(m_searchArray.count)
            self.m_searchTblView.reloadData()
        }
        else{
            GRAPHICS.showAlert(resultDict.objectForKey("msg")as? String!)
        }
        }
    }
    // api call back for follow
    func API_CALLBACK_follow(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            self.callServiceForSearch()
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.callServiceForGetMyFeeds(false)
            })
            //m_searchTblView.reloadData()
        }
        else{
            showAlertViewWithMessage((resultDict.objectForKey("msg")as? String!)!)
        }
    }
    // api call back for get my feeds
    func API_CALLBACK_getMyFeeds(resultDict: NSDictionary)
    {
        dispatch_async(dispatch_get_main_queue()) {

        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            let  feedArray = (resultDict.objectForKey("result")!.mutableCopy() as! NSArray)
            if(self.m_feedArray.count != 0)
            {
                self.m_feedArray.removeAllObjects()
            }
            for i in 0  ..< feedArray.count
            {
                let dictResult = feedArray.objectAtIndex(i)
                let feedEntity = IBMyFeedEntity(dict: dictResult as! NSDictionary)
                self.m_feedArray .addObject(feedEntity)
            }
            
            let sortedArray = self.m_feedArray.sortedArrayUsingComparator {
                (obj1, obj2) -> NSComparisonResult in
                
                let p1 = obj1 as! IBMyFeedEntity
                let p2 = obj2 as! IBMyFeedEntity
                let result = p2.added_date.compare(p1.added_date as String)
                return result
            }
            self.m_feedArray.removeAllObjects()
            self.m_feedArray.addObjectsFromArray(sortedArray)
            
            self.m_productTblView.reloadData()
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_cartcount(getUserIdFromUserDefaults())
            }
            
            
        }
        else{
            SwiftLoader.hide()
            showAlertViewWithMessage((resultDict.objectForKey("msg")as? String!)!)
        }
        }
    }
    // api call back for like
    func API_CALLBACK_addWallLike(resultDict: NSDictionary)
    {
        //SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            
            //self.callServiceForGetMyFeeds()
        }
        else{
            showAlertViewWithMessage((resultDict.objectForKey("msg")as? String!)!)
        }
    }
    // api callback for banner
    func API_CALLBACK_getActiveBanners(resultDict: NSDictionary)
    {
        dispatch_async(dispatch_get_main_queue()) {

        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            if let bannerAry = resultDict.objectForKey("result") as? NSArray
            {
                if bannerAry.count > 0
                {
                    for i in 0 ..< bannerAry.count
                    {
                        let dict = bannerAry[i] as! NSDictionary
                        self.m_bannerArray.addObject(dict)
                    }
                }
            
            }
        //self .createViewForCategories()
            self.m_bannerCollView.reloadData()
            if(self.navigationController?.visibleViewController == self)
            {
                self.performSelector(#selector(IBHomePageVcViewController.animateViewAutomatically), withObject: nil, afterDelay: 3)
            }
            
        }
        }
    }
    //
    func API_CALLBACK_getActiveCategories(resultDict: NSDictionary)
    {
        dispatch_async(dispatch_get_main_queue()) {

        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            if let categoryArry = resultDict.objectForKey("result") as? NSArray
            {
            if categoryArry.count > 0
            {
               if let cateAry = resultDict.objectForKey("result") as? NSArray
               {
                  if cateAry.count > 0
                  {
                    self.m_categoryArray = NSMutableArray ()
                    
                    for i in 0 ..< cateAry.count
                    {
                       let dicct = cateAry[i] as! NSDictionary
                      self.m_categoryArray.addObject(dicct)
                    }
                    
                  }
               }
            }
          
            }
            self.m_categoryCollView.reloadData()
        }
        }
        
    }
    func API_CALLBACK_addProductToCart(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            showAlertViewWithMessage((resultDict.objectForKey("result")as? String!)!)
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                self.callServiceForGetMyFeeds(false)
            })
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_cartcount(getUserIdFromUserDefaults())
            }
        }
        else
        {
            //showAlertViewWithMessage((resultDict.objectForKey("msg")as? String!)!)
            showAlertViewWithMessage((resultDict.objectForKey("result")as? String!)!)
        }
    }
    func API_CALLBACK_cartcount(resultDict: NSDictionary)
    {
        dispatch_async(dispatch_get_main_queue()) {

        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            let cartCountStr = resultDict.objectForKey("result")as! String
            saveCartCountInUserDefaults(cartCountStr)
            self.cartcountLbl.text = cartCountStr
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
        }
    }
    //MARK: - share alert view
    func shareText(text: String, image1: UIImage?, url: NSURL?)
    {
        let sharingItems = NSMutableArray()
        if (text != "")
        {
            sharingItems.addObject(text)
        }
        if image1  != nil
        {
            sharingItems.addObject(image1!)
        }
        if url != nil
        {
            sharingItems.addObject(url!)
        }
        
        let activityViewController = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
        var excludedItems = [String]()
        if facebookStatus == "0" && twitterStatus == "0" {
            if #available(iOS 9.0, *) {
                excludedItems = [UIActivityTypeMail,UIActivityTypeAirDrop,UIActivityTypeMessage,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypePostToWeibo,UIActivityTypeOpenInIBooks,UIActivityTypePrint]
            } else {
                // Fallback on earlier versions
            }
        }
        else if facebookStatus == "1" && twitterStatus == "0" {
            if #available(iOS 9.0, *) {
                excludedItems = [UIActivityTypeMail,UIActivityTypeAirDrop,UIActivityTypeMessage,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypePostToWeibo,UIActivityTypeOpenInIBooks,UIActivityTypePrint,UIActivityTypePostToFacebook]
            } else {
                // Fallback on earlier versions
            }
        }
        else if facebookStatus == "0" && twitterStatus == "1" {
            if #available(iOS 9.0, *) {
                excludedItems = [UIActivityTypeMail,UIActivityTypeAirDrop,UIActivityTypeMessage,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypePostToWeibo,UIActivityTypeOpenInIBooks,UIActivityTypePrint,UIActivityTypePostToTwitter]
            } else {
                // Fallback on earlier versions
            }
        }
        else{
            if #available(iOS 9.0, *) {
                excludedItems = [UIActivityTypeMail,UIActivityTypeAirDrop,UIActivityTypeMessage,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypePostToWeibo,UIActivityTypeOpenInIBooks,UIActivityTypePrint,UIActivityTypePostToFacebook,UIActivityTypePostToTwitter]
            } else {
                // Fallback on earlier versions
            }
            
        }
        
        activityViewController.excludedActivityTypes = excludedItems
        self.navigationController!.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    //MARK: - webview delegate
    func webViewDidStartLoad(webView: UIWebView)
    {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
    }
    func webViewDidFinishLoad(webView: UIWebView)
    {
        
        var frame = webView.frame
        
        let fittingSize = webView.sizeThatFits(CGSizeZero)
        frame.size = fittingSize
       // //print(frame.size)
        webView.frame = frame
        
        
        productScrlView.frame = webView.bounds
        productScrlView.frame.origin.y = (GRAPHICS.Screen_Height() - productScrlView.frame.size.height)/2
        webView.frame.origin.y = (productScrlView.frame.size.height - webView.frame.size.height)/2
        
        SwiftLoader.hide()
    }
    
    
    
    
    func viewForReportAndShare() -> () {
        
        let actionSheet = UIActionSheet(title: Choosefrom, delegate: self, cancelButtonTitle: cancel, destructiveButtonTitle: nil, otherButtonTitles: Report_Inappropriate,Copy_share_URL)
        
        actionSheet.showInView(self.view)
        
        
        
        /*copyAlertView = UIView(frame: self.m_mainView.bounds)
         self.m_mainView.addSubview(copyAlertView)
         self.m_mainView.backgroundColor = UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
         
         let mainViewGesture = UITapGestureRecognizer(target: self, action: #selector(IBHomePageVcViewController.mainViewTapGesture))
         copyAlertView.addGestureRecognizer(mainViewGesture)
         
         let reportShareView = UIView(frame: CGRectMake(GRAPHICS.Screen_X() + 40,GRAPHICS.Screen_Height()/2 - 30,GRAPHICS.Screen_Width() - 80,60))
         reportShareView.backgroundColor = UIColor.whiteColor()
         reportShareView.layer.borderWidth = 1.0
         reportShareView.layer.borderColor = UIColor.lightGrayColor().CGColor
         copyAlertView.addSubview(reportShareView)
         
         let reportBtn = UIButton(frame : CGRectMake(reportShareView.bounds.origin.x,reportShareView.bounds.origin.y,reportShareView.frame.size.width,reportShareView.frame.size.height/2))
         reportBtn.setTitle("Report Inappropriate", forState: .Normal)
         reportBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
         reportBtn.titleLabel?.font = GRAPHICS.FONT_BOLD(12)
         reportBtn.addTarget(self, action: #selector(IBHomePageVcViewController.reportBtnAction), forControlEvents: .TouchUpInside)
         reportShareView.addSubview(reportBtn)
         
         let dividerView = UIView(frame: CGRectMake(reportBtn.bounds.origin.x,reportBtn.frame.size.height - 1,reportBtn.frame.size.width,1))
         dividerView.backgroundColor = UIColor.lightGrayColor()
         reportBtn.addSubview(dividerView)
         
         
         let copyUrlBtn = UIButton(frame : CGRectMake(reportShareView.bounds.origin.x,reportBtn.frame.maxY,reportShareView.frame.size.width,reportShareView.frame.size.height/2))
         copyUrlBtn.setTitle("Copy share URL", forState: .Normal)
         copyUrlBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
         copyUrlBtn.titleLabel?.font = GRAPHICS.FONT_BOLD(12)
         copyUrlBtn.addTarget(self, action: #selector(IBHomePageVcViewController.copyBtnAction), forControlEvents: .TouchUpInside)
         reportShareView.addSubview(copyUrlBtn)*/
    }
    //MARK:-ActionSheet Delegate methods
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex
        {
        case 0:
            
            actionSheet .dismissWithClickedButtonIndex(buttonIndex, animated: true)
            break
            
        case 1:
            self.reportBtnAction()
            break
            
        case 2:
            self.copyBtnAction()
            break
            
        default:
            break
            
        }
        
    }
    func reportBtnAction()
    {
        //        copyAlertView.removeFromSuperview()
        //        copyAlertView = nil
        //        self.m_mainView.backgroundColor = UIColor.clearColor()
        //        self.m_mainView.alpha = 1.0
        
        
    }
    func copyBtnAction()
    {
      
     
        if imageToShare != nil && strurl != nil
        {
        //print(textToShare,imageToShare,strurl)
        shareText(textToShare, image1: imageToShare, url: strurl)
        }
        //        copyAlertView.removeFromSuperview()
        //        copyAlertView = nil
        //        self.m_mainView.backgroundColor = UIColor.clearColor()
        //        self.m_mainView.alpha = 1.0
    }
    
    func mainViewTapGesture() -> () {
        copyAlertView.removeFromSuperview()
        copyAlertView = nil
        self.m_mainView.backgroundColor = UIColor.clearColor()
        self.m_mainView.alpha = 1.0
    }
    
    func displayImage(imageView : UIImageView,  imgUrl:NSURL)
    {
        imageView.setImageWithURL(imgUrl, placeholderImage: GRAPHICS.DEFAULT_CART_IMAGE())
        imageView.contentMode = .ScaleToFill
        imageView.setupImageViewerWithImageURL(imgUrl)
        imageView.clipsToBounds = true
        self.view.bringSubviewToFront(imageView)
    }
    
    //    - (void) displayImage:(UIImageView*)imageView withImage:(UIImage*)image withImgUrl:(NSURL*)imgURL
    //    {
    //    [imageView setImage:image];
    //    [imageView setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"default_img.png"]];
    //    imageView.contentMode = UIViewContentModeScaleAspectFit;
    //    [imageView setupImageViewerWithImageURL:imgURL];
    //    imageView.clipsToBounds = YES;
    //    
    //    [self.view bringSubviewToFront:imageView];
    //    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
