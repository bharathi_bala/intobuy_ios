//
//  IBSearchProductFilterVC.swift
//  IntoBuy
//
//  Created by Manojkumar on 01/02/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBSearchProductFilterVC: IBBaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {

    var m_filterTblView = UITableView()
    var m_filterArray = NSMutableArray()
    var m_selectionIndex:NSInteger!
    var m_minPriceTF:UITextField!
    var m_maxPriceTF:UITextField!
    var keyBoardTool:UIToolbar!
    var m_selectedString = "Popular"
    var minPriceStr = ""
    var maxPriceStr = ""
    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        
        self.hideHeaderViewForView(true)
        self.hideBottomView(true)
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_bgImageView.frame.size.height = GRAPHICS.Screen_Height()
        self.createControls()
        m_selectionIndex = 0;
        

    }
    func createControls()
    {
        m_filterArray = ["Popular","Recent","Nearest"]
        //m_selectedString = "Popular"
        var xPos:CGFloat = 50
        var yPos = GRAPHICS.Screen_Y()
        var width = GRAPHICS.Screen_Width() - 100
        var height:CGFloat = 50
        
        let titleLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        titleLbl.text = "Filter"
        titleLbl.font = GRAPHICS.FONT_REGULAR(16)
        titleLbl.textAlignment = .Center
        titleLbl.textColor = UIColor(red: 228/255, green: 0/255, blue: 35/255, alpha: 1)
        self.m_bgImageView.addSubview(titleLbl)
        
        let backImg = GRAPHICS.PRODUCT_CATEGORY_UP_ARROW_IMAGE()
        width = backImg.size.width/2
        height = backImg.size.height/2
        yPos = (titleLbl.frame.size.height - height)/2
        xPos = GRAPHICS.Screen_Width() - width - 10
        let backBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        backBtn.setBackgroundImage(backImg, forState: .Normal)
        backBtn.addTarget(self, action: #selector(IBSearchProductFilterVC.backBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_bgImageView.addSubview(backBtn)
        
        width = width + 40
        height = height + 40
        xPos = xPos - 20
        yPos = yPos - 20
        let backBigBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        backBigBtn.addTarget(self, action: #selector(IBSearchProductCategoryVC.backBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_bgImageView.addSubview(backBigBtn)


        
        xPos = GRAPHICS.Screen_X() + 5;
        yPos = titleLbl.frame.maxY
        width = GRAPHICS.Screen_Width() - 10
        height = 20
        let headingLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        headingLbl.text = "Sort by"
        headingLbl.font = GRAPHICS.FONT_BOLD(12)
        self.m_bgImageView.addSubview(headingLbl)

        
        xPos =  GRAPHICS.Screen_X();
        yPos = headingLbl.frame.maxY + 10
        width = GRAPHICS.Screen_Width()
        height = 120
        // height gave as static value
        m_filterTblView.frame = CGRectMake(xPos,yPos,width,height)
        m_filterTblView.delegate = self
        m_filterTblView.dataSource = self
        m_filterTblView.separatorStyle = .None
        m_filterTblView.scrollEnabled = false
        self.addBottomBorder(m_filterTblView)
        self.m_bgImageView.addSubview(m_filterTblView)
        self.createViewForPrice()
    }
    
    func createViewForPrice()
    {
        var xPos = GRAPHICS.Screen_X() + 10;
        var yPos = m_filterTblView.frame.maxY + 10
        var width = GRAPHICS.Screen_Width() - 10
        var height:CGFloat = 15
        let priceRangeLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        priceRangeLbl.text = "Price Range"
        priceRangeLbl.font = GRAPHICS.FONT_REGULAR(12)
        self.m_bgImageView.addSubview(priceRangeLbl)
        
        xPos = GRAPHICS.Screen_X() + 10
        yPos = priceRangeLbl.frame.maxY + 10
        width = GRAPHICS.Screen_Width()/4
        let minimumLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        minimumLbl.text = "Minimum"
        minimumLbl.font = GRAPHICS.FONT_REGULAR(12)
        self.m_bgImageView.addSubview(minimumLbl)
        
        let textBoxImg = GRAPHICS.PRODUCT_FILTER_TEXTBOX_IMAGE()
        width = textBoxImg.size.width/2
        height = textBoxImg.size.height/2
        xPos = minimumLbl.frame.maxX
        m_minPriceTF = UITextField(frame : CGRectMake(xPos,yPos,width,height))
        m_minPriceTF.delegate = self
        m_minPriceTF.background = textBoxImg
        m_minPriceTF.text = minPriceStr
        m_minPriceTF.keyboardType = .NumberPad
        m_minPriceTF.font = GRAPHICS.FONT_REGULAR(12)
        m_minPriceTF.inputAccessoryView = createKeyboardToolBar()
        self.m_bgImageView.addSubview(m_minPriceTF)
        setLeftViewToTheTextField(m_minPriceTF)
        
        xPos = minimumLbl.frame.origin.x
        yPos = minimumLbl.frame.maxY + 10
        width = GRAPHICS.Screen_Width()/4
        let maximumLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        maximumLbl.text = "Maximum"
        maximumLbl.font = GRAPHICS.FONT_REGULAR(12)
        self.m_bgImageView.addSubview(maximumLbl)
        
        width = textBoxImg.size.width/2
        height = textBoxImg.size.height/2
        xPos = maximumLbl.frame.maxX
        m_maxPriceTF = UITextField(frame : CGRectMake(xPos,yPos,width,height))
        m_maxPriceTF.delegate = self
        m_maxPriceTF.background = textBoxImg
        m_maxPriceTF.text = maxPriceStr
        m_maxPriceTF.keyboardType = .NumberPad
        m_maxPriceTF.font = GRAPHICS.FONT_REGULAR(12)
        m_maxPriceTF.inputAccessoryView = createKeyboardToolBar()
        self.m_bgImageView.addSubview(m_maxPriceTF)
        setLeftViewToTheTextField(m_maxPriceTF)
        
        xPos = GRAPHICS.Screen_X() + 5
        yPos = m_maxPriceTF.frame.maxY + 5
        width = GRAPHICS.Screen_Width() - 10
        height = 0.5
        let lineImg = GRAPHICS.PRODUCT_CATEGORY_BORDER_LINE_IMAGE()
        let bottomBorder = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        bottomBorder.image = lineImg
        self.m_bgImageView.addSubview(bottomBorder)
        
        let applyImg = GRAPHICS.PRODUCT_FILTER_APPLY_BTN_IMAGE()
        width = applyImg.size.width/2
        height = applyImg.size.height/2
        xPos = (GRAPHICS.Screen_Width() - width)/2
        yPos = bottomBorder.frame.maxY + 20
        let applyBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        applyBtn.setBackgroundImage(applyImg, forState: .Normal)
        applyBtn.addTarget(self, action: #selector(IBSearchProductFilterVC.applyBtnTapped(_:)), forControlEvents: .TouchUpInside);
        self.m_bgImageView.addSubview(applyBtn);
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return m_filterArray.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40.0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell:IBSearchProductCategoryCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBSearchProductCategoryCell
        if (cell == nil) {
            cell = IBSearchProductCategoryCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        cell?.m_titleLbl.text = m_filterArray.objectAtIndex(indexPath.row) as? String
        if m_selectedString == cell?.m_titleLbl.text
        {
            cell?.ShowOrHideTickButton(false)
        }
        else{
            cell?.ShowOrHideTickButton(true)
        }
        
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        m_selectionIndex = indexPath.row;
        m_selectedString = (m_filterArray.objectAtIndex(indexPath.row) as? String)!
        tableView.reloadData();
    }
    func applyBtnTapped(sender : UIButton)
    {
        let maxValue:Int? = Int(m_maxPriceTF.text!)
        let minValue:Int? = Int(m_minPriceTF.text!)
        
        if(minValue >= maxValue)
        {
            GRAPHICS.showAlert("Minimum price should be lower than Maximun")
            return
        }
        //print(m_selectedString,m_maxPriceTF.text!,m_minPriceTF.text!)
        
        if(m_maxPriceTF.text?.isEmpty == true || m_maxPriceTF.text?.isEmpty == true)
        {
            GRAPHICS.showAlert("You forget something")
        }
        else{
            for  vc in (self.navigationController?.viewControllers)!
            {
                if vc .isKindOfClass(IBHomeViewController)
                {
                    let homePage : IBHomeViewController = vc as! IBHomeViewController
                    homePage.m_filterStr = (m_filterArray.objectAtIndex(m_selectionIndex) as? String)!
                    homePage.m_minPriceStr = m_minPriceTF.text!
                    homePage.m_maxPriceStr = m_maxPriceTF.text!
                    homePage.isFromFilter = true
                    homePage.isFromCategory = false
                    homePage.isFromLocation = false
                }
            }
            
            self.navigationController?.popViewControllerAnimated(true)

        }
        
    }
    // method for keyboard toolbar
    func createKeyboardToolBar() -> UIToolbar
    {
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        keyBoardTool = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        keyBoardTool!.barStyle = UIBarStyle.BlackOpaque
        keyBoardTool!.translucent = true
        keyBoardTool!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        keyBoardTool!.sizeToFit()
        
        
        // let doneButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("nextAction"))
        let previousImage = GRAPHICS.PICKER_LEFT_ARROW_BTN()
        let nextImage = GRAPHICS.PICKER_RIGHT_ARROW_BTN()
        
        let prev_Btn = UIButton ()
        prev_Btn.frame = CGRectMake(10, 8, 40, 30)
        prev_Btn.backgroundColor =  UIColor.clearColor();
        prev_Btn.setBackgroundImage(previousImage, forState: .Normal)
        prev_Btn.addTarget(self, action: #selector(IBSearchProductFilterVC.previousAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(prev_Btn)
        
        let next_Btn = UIButton ()
        next_Btn.frame = CGRectMake(50, 8, 40, 30)
        next_Btn.backgroundColor =  UIColor.clearColor();
        next_Btn.setBackgroundImage(nextImage, forState: .Normal)
        next_Btn.addTarget(self, action: #selector(IBSearchProductFilterVC.nextAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(next_Btn)
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBSearchProductFilterVC.doneAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(done_Btn)
        
        
        keyBoardTool!.userInteractionEnabled = true
        return keyBoardTool
        
    }

    func addBottomBorder(view: UIView)
    {
//        let bottomBorder = CALayer()
//        bottomBorder.frame = CGRectMake(0, view.frame.size.height-1.0, view.frame.size.width, 1.0)
//        bottomBorder.backgroundColor = UIColor.whiteColor().CGColor
//        view.layer.addSublayer(bottomBorder)
        let lineImg = GRAPHICS.PRODUCT_CATEGORY_BORDER_LINE_IMAGE()
        let bottomBorder = UIImageView()
        bottomBorder.frame = CGRectMake(view.frame.origin.x + 5, view.bounds.origin.y, GRAPHICS.Screen_Width() - 10, 1.0)
        bottomBorder.image = lineImg
        view.addSubview(bottomBorder)

    }
    func setLeftViewToTheTextField(textField: UITextField)
    {
        let leftPlaceHolderView = UIView(frame: CGRectMake(0, 0, 10,textField.frame.size.height))
        leftPlaceHolderView.backgroundColor = UIColor.clearColor()
        textField.leftView = leftPlaceHolderView
        textField.leftViewMode = UITextFieldViewMode.Always
    }
    func previousAction()
    {
        if(m_minPriceTF.isFirstResponder())
        {
            m_minPriceTF.resignFirstResponder()
        }
        else{
            m_maxPriceTF.resignFirstResponder()
            
            m_minPriceTF.becomeFirstResponder()
        }

    }
    func nextAction()
    {
        if(m_minPriceTF.isFirstResponder())
        {
            m_minPriceTF.resignFirstResponder()
            m_maxPriceTF.becomeFirstResponder()
        }
        else{
            m_maxPriceTF.resignFirstResponder()
        }

    }
    func doneAction()
    {
        if(m_minPriceTF.isFirstResponder())
        {
            m_minPriceTF.resignFirstResponder()
        }
        else{
            m_maxPriceTF.resignFirstResponder()
        }
    }
    func backBtnAction(sender : UIButton)
    {
        for  vc in (self.navigationController?.viewControllers)!
        {
            if vc .isKindOfClass(IBHomeViewController)
            {
                let homePage : IBHomeViewController = vc as! IBHomeViewController
                homePage.m_filterStr = (m_filterArray.objectAtIndex(m_selectionIndex) as? String)!
                homePage.m_minPriceStr = m_minPriceTF.text!
                homePage.m_maxPriceStr = m_maxPriceTF.text!
                homePage.isFromFilter = true
            }
        }

        self.navigationController?.popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
