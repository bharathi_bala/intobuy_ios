//
//  IBSelectProfileViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 08/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBSelectProfileViewController: IBBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideHeaderViewForView(false)
        self.hideBottomView(true)
        self.m_settingsButton.hidden = true
        self.m_bgImageView.image = GRAPHICS.LOGIN_BACKGROUND_IMAGE()
        self.m_titleLabel.text = SelectProfile
        self .createControls()
        // Do any additional setup after loading the view.
    }
    func createControls()
    {
        let freeUserImg = UIImage(named: "free_user")
        var width = (freeUserImg?.size.width)!/1.5
        var height = (freeUserImg?.size.height)!/1.5
        var xPos = (GRAPHICS.Screen_Width() - width)/2
        var yPos = self.m_bgImageView.bounds.origin.y + 30
        let freeUserImgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        freeUserImgView.image = freeUserImg
        self.m_bgImageView .addSubview(freeUserImgView)
        
        let freeBtnImg = UIImage(named: "gray_btn")
        width = (freeBtnImg?.size.width)!/2
        height  = (freeBtnImg?.size.height)!/2
        xPos = (GRAPHICS.Screen_Width() - width)/2
        yPos = freeUserImgView.frame.maxY + 20
        let freeUserBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        freeUserBtn.setBackgroundImage(freeBtnImg, forState: UIControlState.Normal)
        freeUserBtn.setTitle("Free Member", forState: UIControlState.Normal)
        freeUserBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        freeUserBtn.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
        freeUserBtn.addTarget(self, action: #selector(IBSelectProfileViewController.freeUserBtn), forControlEvents: UIControlEvents.TouchUpInside)
        self.m_bgImageView .addSubview(freeUserBtn)
        
        let orLineImg = UIImage(named: "or_lines.png")
        width = (orLineImg?.size.width)!/2
        height  = (orLineImg?.size.height)!/2
        xPos = (GRAPHICS.Screen_Width() - width)/2
        yPos = freeUserBtn.frame.maxY + 45
        let orLineImgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        orLineImgView.image = orLineImg
        self.m_bgImageView .addSubview(orLineImgView)
        
        width = 30
        height = 20
        xPos = (GRAPHICS.Screen_Width() - width)/2
        yPos = orLineImgView.frame.origin.y - height/2
        let orLabel = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        orLabel.text = "Or"
        orLabel.textColor = UIColor.grayColor()
        orLabel.font = GRAPHICS.FONT_REGULAR(16)
        orLabel.textAlignment = NSTextAlignment.Center
        self.m_bgImageView.addSubview(orLabel)
        
        let vipUserImg = UIImage(named: "vip_user")
        width = (vipUserImg?.size.width)!/1.5
        height = (vipUserImg?.size.height)!/1.5
        xPos = (GRAPHICS.Screen_Width() - width)/2
        yPos = orLineImgView.frame.maxY + 30
        let vipUserImgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        vipUserImgView.image = vipUserImg
        self.m_bgImageView .addSubview(vipUserImgView)
        
        let vipBtnImg = UIImage(named: "pink_btn")
        width = (vipBtnImg?.size.width)!/2
        height  = (vipBtnImg?.size.height)!/2
        xPos = (GRAPHICS.Screen_Width() - width)/2
        yPos = vipUserImgView.frame.maxY + 20
        let vipUserBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        vipUserBtn.setBackgroundImage(vipBtnImg, forState: UIControlState.Normal)
        vipUserBtn.setTitle("VIP Member", forState: UIControlState.Normal)
        vipUserBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        vipUserBtn.addTarget(self, action: #selector(IBSelectProfileViewController.vipUserBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        self.m_bgImageView .addSubview(vipUserBtn)
        
    }
    func vipUserBtnAction()
    {
        let memberVC = IBMemberShipPlanViewController()
        memberVC.isFromSettings = false
        self.navigationController!.pushViewController(memberVC, animated: true)

    }
    func freeUserBtn()
    {
        let userDetailUD = NSUserDefaults()
       // let userIdStr = getUserIdFromUserDefaults()
        userDetailUD.setBool(true, forKey: "isLogged")
        
        let homeVc = IBHomePageVcViewController()
        self.navigationController!.pushViewController(homeVc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
