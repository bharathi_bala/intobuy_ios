//
//  IBSearchViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 27/11/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBSearchViewController: IBBaseViewController ,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var m_searchTextField = UITextField()
    var m_searchTblView = UITableView()
    var m_searchArray = NSMutableArray()
    var searchArr = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.createControls()
        
        m_searchArray = ["Manoj","Kumar","Bharathi","Aravind","Azik","Jyothi","Karthi","Jose","Dheena"]
        searchArr = NSMutableArray(array: m_searchArray)
    }
    func createControls(){
    let searchView = UIView(frame: CGRectMake(GRAPHICS.Screen_X(),self.m_headerView.frame.maxY ,GRAPHICS.Screen_Width(),60))
        searchView.backgroundColor = UIColor.lightGrayColor()
        self.m_bgImageView .addSubview(searchView)
        
        let textImg = GRAPHICS.SEARCH_TEXTBOX_IMAGE()
        var imgWidth = textImg.size.width/2//
        var imgHei = textImg.size.height/1.5//
                
        m_searchTextField.frame = CGRectMake(searchView.bounds.origin.x + 40, (searchView.frame.size.height - imgHei)/2, imgWidth, imgHei)
        m_searchTextField.background = textImg
        m_searchTextField.placeholder = SearchFriends
        m_searchTextField.font = GRAPHICS.FONT_REGULAR(14)
        m_searchTextField.textAlignment = NSTextAlignment.Center
        m_searchTextField.addTarget(self, action: #selector(IBSearchViewController.textFieldDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
        searchView.addSubview(m_searchTextField)
        
        let searchImage = GRAPHICS.PRODUCT_SEARCH_IMAGE()
        imgWidth = (searchImage.size.width)/1.5
        imgHei = searchImage.size.height/1.5

        let searchImgView = UIImageView(frame: CGRectMake(m_searchTextField.frame.maxX + 5, (searchView.frame.size.height - imgHei)/2, imgWidth, imgHei))
        searchImgView.image = searchImage
        searchView.addSubview(searchImgView)

        
        m_searchTblView.frame = CGRectMake(GRAPHICS .Screen_X(), searchView.frame.maxY, GRAPHICS.Screen_Width(), self.m_bgImageView.frame.size.height - 60)
        m_searchTblView.backgroundColor = UIColor.whiteColor()
        m_searchTblView.delegate = self
        m_searchTblView.dataSource = self
        m_searchTblView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.m_bgImageView.addSubview(m_searchTblView)
        

    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70.0
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return m_searchArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell:IBSearchTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBSearchTableViewCell
        
        if (cell == nil) {
            cell = IBSearchTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        var folloStatus = "0"
        if(indexPath.row % 2 == 0)
        {
            folloStatus = "1"
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        cell?.valuesForControls("", name: m_searchArray[indexPath.row] as! String, followState: folloStatus, trophyStr: "" ,isFromHome: false ,profileImgTag : indexPath.row)
        cell?.ShowOrHideTrophyLbl(true)
        cell?.isFromHome = false
        return cell!;
    }
    func textFieldDidChange(textField : UITextField)
    {
        if(textField.text == "")
        {
            m_searchArray .removeAllObjects()
            m_searchArray .addObjectsFromArray(searchArr as [AnyObject])
 
        }
        else{
            
            let searchPredicate = NSPredicate(format: "self BEGINSWITH[cd] %@", textField.text as String!)
            let array = searchArr.filteredArrayUsingPredicate(searchPredicate)
            //print(array)
            m_searchArray .removeAllObjects()
            m_searchArray .addObjectsFromArray(array)

        }
        m_searchTblView .reloadData()
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField .resignFirstResponder()
        return true
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
