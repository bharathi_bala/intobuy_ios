//
//  IBUploadImageViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 16/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

public protocol UploadImageProtoCol : NSObjectProtocol
{
    func UploadImaageView(recogniser: UIGestureRecognizer)
   
}

class IBUploadImageViewController: IBBaseViewController, UITextFieldDelegate,ServerAPIDelegate
{

    var m_addCaptionTF:UITextField!
    var m_imageTypeArr:NSMutableArray!
    var totalYpos:CGFloat = 0
    var m_personalViw : UIView = UIView()
    var m_productViw : UIView = UIView()
    var m_charityViw : UIView = UIView()
    var m_categoryLbl = UILabel()
    var m_subCatogoryLbl = UILabel()
    var m_priceLabel = UILabel()
    var m_qunatityLbl = UILabel()
    var m_itemDescrptionTextField = UITextField()
    var m_catogoryTextField = UITextField()
    var m_subCatogoryTextfield = UITextField()
    var m_priceAmuntTextField = UITextField()
    var m_quantityTextField = UITextField()
    var isCategoryPickerShown:Bool = true
    var m_keyboardToolBar:UIToolbar = UIToolbar()
    var m_uploadImage: UIImage = UIImage()
    var postImage1Data = NSData()
     var postImage2Data = NSData()
     var postImage3Data = NSData()
    var postImage4Data = NSData()
     var isFirstImage : Bool = false
     var isSecondImage: Bool = false
     var isThirdImage : Bool = false
     var isFourthImage : Bool = false
     var uploadDelegate:UploadImageProtoCol!
    
    var publicImageviw: UIImageView = UIImageView()
    var privateImageViw: UIImageView = UIImageView()
    
    var imageType = "1"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self .hideSettingsBtn(true)
        self.hideHeaderViewForView(false)
        self.hideBackBtn(true)
        
        self.hideBottomView(true)
        self.m_titleLabel.text = UploadImage
        self.m_bgImageView.image = GRAPHICS.LOGIN_BACKGROUND_IMAGE()
        self.createControlsToUploadImage()
        m_imageTypeArr = [PersonalImage,ProductImageStr,CharityImage]
        self.createButtons()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
        
    }
    
    func createButtons()
    {
        let crossBtnImg = GRAPHICS.CREATE_POST_CROSS_BTN_IMG()
        var width = crossBtnImg.size.width/2
        var height = crossBtnImg.size.height/2
        var xPos = self.m_headerView.frame.minX + 10
        var yPos = self.m_headerView.frame.height - height - 5
        
        let closeBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        closeBtn.setBackgroundImage(crossBtnImg, forState: .Normal)
        closeBtn.addTarget(self, action: #selector(IBUploadImageViewController.closeBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_headerView.addSubview(closeBtn)
        
        let tickBtnImg = GRAPHICS.CREATE_POST_HEADER_TICK_BTN_IMG()
        width = tickBtnImg.size.width/2
        height = tickBtnImg.size.height/2
        xPos = self.m_headerView.frame.width - width - 10
        yPos = self.m_headerView.frame.height - height - 5
        
        let doneBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        doneBtn.setBackgroundImage(tickBtnImg, forState: .Normal)
        doneBtn.addTarget(self, action: #selector(IBUploadImageViewController.doneBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_headerView.addSubview(doneBtn)
    }
    func createControlsToUploadImage()
    {
        let uploadImg = GRAPHICS .SELL_ADD_PRODUCT_BTN_IMAGE()
        let imgSize = GRAPHICS.getImageWidthHeight(uploadImg)
        let padding:CGFloat = 5.0
//        var xPos = m_imgScrollView.bounds.origin.x + 5
//        var ypos = m_imgScrollView.bounds.origin.y + 15
        var width = imgSize.width//m_imgScrollView.frame.size.width/4 - 6
        var height = imgSize.height/*GRAPHICS.DEFAULT_CAMERA_IMAGE()
        var contentSize = GRAPHICS.getImageWidthHeight(uploadImg)
        var width = uploadImg.size.width/1.6
        var height = uploadImg.size.height/1.6
        var padding:CGFloat = 15.0
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            width = uploadImg.size.width/1.4
            height = uploadImg.size.height/1.4
            padding = 20.0
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = uploadImg.size.width
            height = uploadImg.size.height
            padding = 20.0
        }*/
        var xPos = GRAPHICS.Screen_X() + padding
        var yPos = self.m_bgImageView.bounds.origin.y + 10
        
        for i in 0  ..< 4
        {
            let uploadImgView = UIImageView(frame: CGRectMake(xPos, yPos, width, height))
               uploadImgView.image = uploadImg;
            uploadImgView.contentMode = .ScaleAspectFill
            uploadImgView.clipsToBounds = true
            
          //let returnVal = IBSingletonClass .sharedInstance.m_UploadImageIndex
            
       // //print("m_UploadImageIndex/%@",IBSingletonClass .sharedInstance.m_UploadImageIndex)
           
       // //print("firstImage/%@",IBSingletonClass .sharedInstance.m_FirstImage)
            
       // //print("secodnImage/%@",IBSingletonClass .sharedInstance.m_secondImage)
        ////print("ThirdImage/%@",IBSingletonClass .sharedInstance.m_ThirdImage)
            
            if (i == 0)
            {
              
                if (IBSingletonClass .sharedInstance.m_isFromFirstImage)
                {
            uploadImgView.image = IBSingletonClass .sharedInstance.m_FirstImage

            postImage1Data = UIImageJPEGRepresentation(uploadImgView.image!, 0.6)!//UIImagePNGRepresentation(uploadImgView.image!)!
                }
                else
                {
                 uploadImgView.image = uploadImg;
                }
                
            }
            else if(i == 1)
            {
              
                if (IBSingletonClass .sharedInstance.m_isFromSecondImage)
                {
                    uploadImgView.image = IBSingletonClass .sharedInstance.m_secondImage
                    
                    postImage2Data = UIImageJPEGRepresentation(uploadImgView.image!, 0.6)!//UIImagePNGRepresentation(uploadImgView.image!)!
                }
                else
                {
                    uploadImgView.image = uploadImg;
                }
                
                
            }
            else if (i == 2)
            {
                if (IBSingletonClass .sharedInstance.m_isFromThirdImage)
                {
                
                uploadImgView.image = IBSingletonClass .sharedInstance.m_ThirdImage
                
                postImage3Data = UIImageJPEGRepresentation(uploadImgView.image!, 0.6)!//UIImagePNGRepresentation(uploadImgView.image!)!
                }
                else
                {
                    uploadImgView.image = uploadImg;
                }

                
            }
            else if (i == 3)
            {
                if (IBSingletonClass .sharedInstance.m_isFromFourthImage)
                {
                    
                    uploadImgView.image = IBSingletonClass .sharedInstance.m_FourthImage
                    
                    postImage4Data = UIImageJPEGRepresentation(uploadImgView.image!, 0.6)!//UIImagePNGRepresentation(uploadImgView.image!)!
                }
                else
                {
                    uploadImgView.image = uploadImg;
                }
                
                
            }

            
            uploadImgView.tag = i+300
          
            self.m_bgImageView.addSubview(uploadImgView);
            
            
            uploadImgView.userInteractionEnabled = true
            xPos = xPos + width + 5
          
            
            let tapGetsure = UITapGestureRecognizer()
            tapGetsure.addTarget(self, action:#selector(IBUploadImageViewController.tappedImageView(_:)))
            uploadImgView.addGestureRecognizer(tapGetsure)
            
        }
        xPos = GRAPHICS.Screen_X() + 15
        yPos = yPos + height + 10
        width = GRAPHICS.Screen_Width() - 30
        height = 40
        m_addCaptionTF = UITextField(frame: CGRectMake(xPos, yPos, width, height))
        m_addCaptionTF.delegate = self
        m_addCaptionTF.placeholder = AddCaption
        m_addCaptionTF.font = GRAPHICS.FONT_REGULAR(14)
        m_addCaptionTF.backgroundColor = UIColor.whiteColor()
        self.setLeftViewtotheTextfield(m_addCaptionTF)
        self.m_bgImageView.addSubview(m_addCaptionTF)
         
        yPos = m_addCaptionTF.frame.maxY + 10
        height = 30
        let selectLbl = UILabel(frame: CGRectMake(xPos, yPos, width, height))
        selectLbl.textColor = UIColor.blackColor()
        selectLbl.text = SelectImageType
        selectLbl.font = GRAPHICS.FONT_REGULAR(16)
        self.m_bgImageView.addSubview(selectLbl)
        
        yPos = selectLbl.frame.maxY + 5
        xPos = self.m_bgImageView.bounds.origin.x + 15
        
        selectLbl.hidden = true
        
        /*for i in 0  ..< 2
        {
            xPos = self.m_bgImageView.bounds.origin.x + 15
            width = GRAPHICS.Screen_Width() - 30
            height = 30
    
            let buttonView = UIView(frame: CGRectMake(xPos, yPos, width, height))
            buttonView.backgroundColor = UIColor.whiteColor()
            buttonView.tag = i
            self.m_bgImageView.addSubview(buttonView)
            
            width = buttonView.frame.size.width/1.2
            xPos = buttonView.bounds.origin.x
            yPos = buttonView.bounds.origin.y
            let  imageBtn = UIButton(frame: CGRectMake(xPos, yPos, width, height))
            imageBtn.backgroundColor = UIColor.clearColor()
            imageBtn.addTarget(self, action: #selector(IBUploadImageViewController.imgTypeBtnTapped(_:)), forControlEvents: .TouchUpInside)
            imageBtn.tag = i + 100
            imageBtn .setTitleColor(grayColor, forState: UIControlState.Normal)
            imageBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
            imageBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
            imageBtn.titleLabel?.textColor = UIColor.blackColor()
            imageBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
            imageBtn.titleLabel?.textAlignment = NSTextAlignment.Left
            imageBtn.hidden = true
            
            buttonView.addSubview(imageBtn)
            
            yPos = buttonView.bounds.origin.y + 5
            height = buttonView.frame.size.height - 10
            width = height
            xPos = buttonView.frame.size.width - width - 10
            let  imageView1 = UIImageView(frame: CGRectMake(xPos, yPos, width, height))
            imageView1.backgroundColor = UIColor.clearColor()
            imageView1.image = GRAPHICS.CREATE_POST_TICK_BTN_IMG()
            imageView1.tag = i + 200
            if(buttonView.tag == 0)
            {
                imageBtn.selected = true
                imageView1.hidden = false
                imageBtn .setTitle(uploadImage_Public_image, forState: .Normal)
               // self.creationOfPersonalImages((buttonView.frame.maxY ) + 50)
            }
           else if(buttonView.tag == 1)
            {
                imageBtn.selected = false
                imageView1.hidden = true
                imageBtn .setTitle(uploadImage_Private_Image, forState: .Normal)
                //imageBtn .setTitle(uploadImage_Product_image, forState: .Normal)
            }
            
            buttonView.addSubview(imageView1)
            
            
            yPos = buttonView.frame.maxY + 5
            
            totalYpos = yPos
        }
        
        selectLbl.hidden = true*/
    }
    func tappedImageView(recogniser:UIGestureRecognizer)
    {
          //setFirstImage((recogniser.view?.tag)!)
        IBSingletonClass.sharedInstance.screenIndex = 0
        var index = IBSingletonClass.sharedInstance.m_UploadImageIndex
        
        index = (recogniser.view?.tag)!
        
        IBSingletonClass.sharedInstance.m_UploadImageIndex = index
      
               
        if (recogniser.view?.tag == 300)
        {
            isFirstImage = true
            isSecondImage = false
            isThirdImage = false
            isFourthImage = false
             // IBSingletonClass.sharedInstance.m_isFromFirstImage = true
            
        }
       else if (recogniser.view?.tag == 301)
        {
            isFirstImage = false
            isSecondImage = true
            isThirdImage = false
            isFourthImage = false
            // IBSingletonClass.sharedInstance.m_isFromSecondImage = true
            
        }
       else if (recogniser.view?.tag == 302)
        {
            isFirstImage = false
            isSecondImage = false
            isThirdImage = true
            isFourthImage = false
            // IBSingletonClass.sharedInstance.m_isFromThirdImage = true
        }
        else if (recogniser.view?.tag == 303)
        {
            isFirstImage = false
            isSecondImage = false
            isThirdImage = false
            isFourthImage = true
            // IBSingletonClass.sharedInstance.m_isFromThirdImage = true
        }

    
        
        //let ycameraViewController1 = YCameraViewController()
        //self.presentViewController(ycameraViewController1, animated: true, completion: nil)
        self.navigationController!.popToRootViewControllerAnimated(false);
        
    }
    
    
    func imgTypeBtnTapped(sender:UIButton)
    {
        //sender.selected = !sender.selected
        let view = sender.superview
        
        let PublicImgvIew : UIImageView = self.m_bgImageView.viewWithTag(200) as! UIImageView
        
        let charityImgView : UIImageView = self.m_bgImageView.viewWithTag(201) as! UIImageView
        
        
        
//        let imageView = view?.viewWithTag(sender.tag + 100) as? UIImageView
//            if(sender.selected){
//                imageView!.hidden = false
//            }
//            else{
//                imageView!.hidden = true
//            }
        
        
        
        
//        for imgView in (view?.subviews)!{
//            if imgView is UIImageView{
//                if(sender.selected){
                //imgView.hidden = false
                    if(sender.tag == 100)
                    {
                       imageType = "1"
                    // personalBtn?.selected = true
                    // CharityBtn?.selected = false
                    m_personalViw.hidden = false
                  //  self.creationOfPersonalImages(totalYpos)
                    PublicImgvIew.hidden = false
                    charityImgView.hidden = true
                    self.view.bringSubviewToFront(m_personalViw)
                    
                    }
                   else if(sender.tag == 101)
                    {
                        imageType = "0"
                       // personalBtn?.selected = false
                        //CharityBtn?.selected = true
                       // self.creationOfProductImages(totalYpos)
                         //m_personalViw.hidden = true
                        m_personalViw.removeFromSuperview()
                        PublicImgvIew.hidden = true
                        charityImgView.hidden = false
                        m_productViw.hidden = false
                         m_personalViw.hidden = true
                        //self.view.bringSubviewToFront(m_personalViw)
                    
                    }
                    else if(sender.tag == 102)
                    {
                         m_personalViw.removeFromSuperview()
                       // m_personalViw.hidden = true
                        m_productViw.hidden = true
                        
                    }
                }
                    
//             else
//                {
//                    imgView.hidden = true
//                    
//                    m_personalViw.hidden = true
//                    m_productViw.hidden = true
//                    
//                }
       
       // }
        
    //}
    
    func creationOfProductImages(yposition:CGFloat)->()
    {
        var  ypos :CGFloat = yposition
        var xpos :CGFloat = 0
        var width :CGFloat = 0
        var height: CGFloat = 0
        
        m_productViw.frame = CGRectMake(0, ypos, GRAPHICS.Screen_Width(), 250)
        m_productViw.backgroundColor = UIColor.yellowColor()
        m_bgImageView.addSubview(m_productViw)
        
        xpos =  15
        width = 120
        height = 38
        ypos = 5
        
        //creationOfCatogory
        m_categoryLbl = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr: category)
        m_productViw.addSubview(m_categoryLbl)
        
        //Creation OF categoryTextfield
      
        xpos = GRAPHICS.Screen_Width()-30-100
        
        m_catogoryTextField.frame = CGRectMake(xpos, ypos,90, height)
        m_catogoryTextField.delegate = self
        m_catogoryTextField.font = GRAPHICS.FONT_REGULAR(14)
        m_catogoryTextField.layer.borderWidth = 1.0
       // m_catogoryTextField.layer.borderColor = textFieldBordercolor.CGColor
        m_catogoryTextField.placeholder = "Select-"
        m_catogoryTextField.layer.cornerRadius = 5
        
        // [m_selectCategoryTextField setValue:[GRAPHICS colorFromHexString:strColorCode]
        //forKeyPath:@"_placeholderLabel.textColor"];
        
        m_catogoryTextField.font = GRAPHICS.FONT_REGULAR(14)
        m_catogoryTextField.textColor =  grayColor
        m_catogoryTextField.returnKeyType = .Done;
        m_productViw .addSubview(m_catogoryTextField)
         self.setRightDropDownForSelectCategory(m_catogoryTextField)
        
        //Creation Of SubCategoryTextfield

        ypos = m_categoryLbl.frame.maxY
        xpos = 15
        
        m_subCatogoryLbl = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr: uploadImage_SubCatogory_text)
        m_productViw.addSubview(m_subCatogoryLbl)
        
        
        xpos = GRAPHICS.Screen_Width()-30-100
        
        m_subCatogoryTextfield.frame = CGRectMake(xpos, ypos,90, height)
        m_subCatogoryTextfield.delegate = self
        m_subCatogoryTextfield.font = GRAPHICS.FONT_REGULAR(14)
        m_subCatogoryTextfield.layer.borderWidth = 1.0
        // m_catogoryTextField.layer.borderColor = textFieldBordercolor.CGColor
        m_subCatogoryTextfield.placeholder = "Select-"
        m_subCatogoryTextfield.layer.cornerRadius = 5
        
        // [m_selectCategoryTextField setValue:[GRAPHICS colorFromHexString:strColorCode]
        //forKeyPath:@"_placeholderLabel.textColor"];
        
        m_subCatogoryTextfield.font = GRAPHICS.FONT_REGULAR(14)
        m_subCatogoryTextfield.textColor =  grayColor
        m_subCatogoryTextfield.returnKeyType = .Done;
        m_productViw .addSubview(m_subCatogoryTextfield)
        self.setRightDropDownForSelctSubCategory(m_subCatogoryTextfield)
        

        //Creation Of PriceCatogory TextField
        
        ypos = m_subCatogoryLbl.frame.maxY
        xpos = 15
        
        m_priceLabel = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr: price)
        m_productViw.addSubview(m_priceLabel)
        
        
        xpos = GRAPHICS.Screen_Width()-30-100
        
        m_priceAmuntTextField.frame = CGRectMake(xpos, ypos,90, height)
        m_priceAmuntTextField.delegate = self
        m_priceAmuntTextField.font = GRAPHICS.FONT_REGULAR(14)
        m_priceAmuntTextField.layer.borderWidth = 1.0
        // m_catogoryTextField.layer.borderColor = textFieldBordercolor.CGColor
        m_priceAmuntTextField.placeholder = "Select Price"
        //m_priceAmuntTextField.layer.cornerRadius = 5
        
        // [m_selectCategoryTextField setValue:[GRAPHICS colorFromHexString:strColorCode]
        //forKeyPath:@"_placeholderLabel.textColor"];
        
        m_priceAmuntTextField.font = GRAPHICS.FONT_REGULAR(14)
        m_priceAmuntTextField.textColor =  grayColor
        m_priceAmuntTextField.returnKeyType = .Done;
        m_productViw .addSubview(m_priceAmuntTextField)
     

        //Creation Of Quantity TextField
        
        ypos = m_priceLabel.frame.maxY
        xpos = 15
        
        m_qunatityLbl = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr: cart_quantity_text)
        m_productViw.addSubview(m_qunatityLbl)
        
        
        xpos = GRAPHICS.Screen_Width()-30-100
        
        m_quantityTextField.frame = CGRectMake(xpos, ypos,90, height)
        m_quantityTextField.delegate = self
        m_quantityTextField.font = GRAPHICS.FONT_REGULAR(14)
        m_quantityTextField.layer.borderWidth = 1.0
        // m_catogoryTextField.layer.borderColor = textFieldBordercolor.CGColor
        m_quantityTextField.placeholder = "Select Qunatity"
        //m_priceAmuntTextField.layer.cornerRadius = 5
        
        // [m_selectCategoryTextField setValue:[GRAPHICS colorFromHexString:strColorCode]
        //forKeyPath:@"_placeholderLabel.textColor"];
        
        m_quantityTextField.font = GRAPHICS.FONT_REGULAR(14)
        m_quantityTextField.textColor =  grayColor
        m_quantityTextField.returnKeyType = .Done;
        m_productViw .addSubview(m_quantityTextField)
        
        ypos = m_qunatityLbl.frame.maxY
        xpos = 15
        width = GRAPHICS.Screen_Width()-30
        
        m_itemDescrptionTextField.frame = CGRectMake(xpos, ypos,90, height)
        m_itemDescrptionTextField.delegate = self
        m_itemDescrptionTextField.font = GRAPHICS.FONT_REGULAR(14)
       // m_itemDescrptionLbl.layer.borderWidth = 1.0
        // m_catogoryTextField.layer.borderColor = textFieldBordercolor.CGColor
        m_itemDescrptionTextField.placeholder = "Item Description"
        //m_priceAmuntTextField.layer.cornerRadius = 5
        
        // [m_selectCategoryTextField setValue:[GRAPHICS colorFromHexString:strColorCode]
        //forKeyPath:@"_placeholderLabel.textColor"];
        
        m_itemDescrptionTextField.font = GRAPHICS.FONT_REGULAR(14)
        m_itemDescrptionTextField.textColor =  grayColor
        m_itemDescrptionTextField.returnKeyType = .Done;
        m_productViw .addSubview(m_itemDescrptionTextField)
        
    
    }
    //Creation oF Product labels
    func  createLabel(xpos:CGFloat,ypos: CGFloat,width: CGFloat,height:CGFloat,textStr : String)-> UILabel
    {
        let lbl : UILabel = UILabel()
        lbl.frame = CGRectMake(xpos, ypos, width, height)
        lbl.backgroundColor = UIColor.clearColor()
        lbl.text = textStr
        lbl.textColor = UIColor.blackColor()
        lbl.font = GRAPHICS.FONT_REGULAR(13)
        return lbl
        
    }
    
    //creation Of BottomLines
    func creationOfBottomLineView(xpos: CGFloat,ypos: CGFloat,width : CGFloat,height:CGFloat)->UIView
    {
        let viw : UIView = UIView()
        viw.frame = CGRectMake(xpos, ypos, width, height)
        viw.backgroundColor = UIColor.lightGrayColor()
        return viw
    }
    
    
    func creationOfPersonalImages(yposition : CGFloat)->()
    {
        m_addCaptionTF.resignFirstResponder()
        
        var  yPos :CGFloat = yposition
        var xPos :CGFloat = 0
        var width :CGFloat = 0
        var height: CGFloat = 0
        
        m_personalViw.frame = CGRectMake(0, yPos, GRAPHICS.Screen_Width(), 200)
        m_personalViw.backgroundColor = UIColor.clearColor()
        self.m_bgImageView.addSubview(m_personalViw)
        
         yPos = 10
        
//        for var i = 0 ; i < 2 ; i++
//        {
            xPos = self.m_bgImageView.bounds.origin.x + 15
            width = GRAPHICS.Screen_Width() - 30
            height = 35
           
            let buttonView = UIView(frame: CGRectMake(xPos, yPos, width, height))
            buttonView.backgroundColor = UIColor.whiteColor()
            buttonView.tag = 0
            m_personalViw.addSubview(buttonView)
            
            width = buttonView.frame.size.width/1.2
            xPos = buttonView.bounds.origin.x
            //yPos = buttonView.frame.origin.y
            let  imageBtn = UIButton(frame: CGRectMake(xPos, 0, width, height))
           // imageBtn.backgroundColor = UIColor.clearcolor()
        
            imageBtn.tag = 0 + 2000
            imageBtn .setTitleColor(grayColor, forState: UIControlState.Normal)
            imageBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left;
            imageBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
            imageBtn.titleLabel?.textColor = UIColor.blackColor()
            imageBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
            imageBtn.backgroundColor = UIColor.clearColor()
            imageBtn.titleLabel?.textAlignment = NSTextAlignment.Left
          imageBtn.addTarget(self, action: #selector(IBUploadImageViewController.PublicImageButtonTapped), forControlEvents: .TouchUpInside)
          imageBtn .setTitle(uploadImage_Public_image, forState:.Normal)
         
            buttonView.addSubview(imageBtn)
        
            //yPos = 5
            height = buttonView.frame.size.height - 10
            width = height
            xPos = buttonView.frame.size.width - width - 10
           publicImageviw = UIImageView(frame: CGRectMake(xPos, 0, width, height))
            //publicImageviw.backgroundColor = appPinkColor
            publicImageviw.image = GRAPHICS.CREATE_POST_TICK_BTN_IMG()
            publicImageviw.tag = 0 + 7000
            publicImageviw.hidden = false
            buttonView.addSubview(publicImageviw)
        /*
            if(buttonView.tag == 0)
            {
                imageBtn.selected = false
                imageView.hidden = true
                imageBtn .setTitle(uploadImage_Public_image, forState:.Normal)
            }
            else if(buttonView.tag == 1)
            {
                imageBtn.selected = true
                imageView.hidden = false
               // imageBtn.backgroundColor = appPinkColor
                imageBtn .setTitle(uploadImage_Private_Image, forState: .Normal)
            }
           
            else
            {
                //imageView.hidden = true
            }
            imageBtn.addSubview(imageView)
            */
        xPos = self.m_bgImageView.bounds.origin.x + 15
        width = GRAPHICS.Screen_Width() - 30
        height = 35
              yPos = buttonView.frame.maxY + 5
        
        //Creation OF Personal Images
        
        let personalviw = UIView(frame: CGRectMake(xPos, yPos, width, height))
        personalviw.backgroundColor = UIColor.whiteColor()
        personalviw.tag = 0
        m_personalViw.addSubview(personalviw)
        
       // width = buttonView.frame.size.width/1.2
       // xPos = buttonView.bounds.origin.x
        //yPos = buttonView.frame.origin.y
        let  personalBtn = UIButton(frame: CGRectMake(0, 0, width, height))
        // imageBtn.backgroundColor = UIColor.clearcolor()
        personalBtn.addTarget(self, action: #selector(IBUploadImageViewController.PersonalButtonTapped), forControlEvents: .TouchUpInside)
        personalBtn.tag = 0 + 2000
        personalBtn .setTitleColor(grayColor, forState: UIControlState.Normal)
        personalBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left;
        personalBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0)
        personalBtn.titleLabel?.textColor = UIColor.blackColor()
        personalBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        personalBtn.backgroundColor = UIColor.clearColor()
        personalBtn.titleLabel?.textAlignment = NSTextAlignment.Left
        personalBtn .setTitle(uploadImage_Private_Image, forState:.Normal)

        
        personalviw.addSubview(personalBtn)
        
        //yPos = 5
        height = buttonView.frame.size.height - 10
        width = height
        xPos = buttonView.frame.size.width - width - 10
       privateImageViw = UIImageView(frame: CGRectMake(xPos, 0, width, height))
        //privateImageViw.backgroundColor = appPinkColor
        privateImageViw.image = GRAPHICS.CREATE_POST_TICK_BTN_IMG()
        privateImageViw.hidden = true
        //privateImageViw.tag = 0 + 7000
        /*
        if(buttonView.tag == 0)
        {
            imageBtn.selected = false
            imageView.hidden = true
            imageBtn .setTitle(uploadImage_Public_image, forState:.Normal)
        }
        else if(buttonView.tag == 1)
        {
            imageBtn.selected = true
            imageView.hidden = false
            // imageBtn.backgroundColor = appPinkColor
            imageBtn .setTitle(uploadImage_Private_Image, forState: .Normal)
        }
            
        else
        {
            //imageView.hidden = true
        }*/
        personalviw.addSubview(privateImageViw)
        
        
        
        }
    
   
    
 func PublicImageButtonTapped()
 {
//    publicImageviw.hidden = true
//    privateImageViw.hidden = false
    publicImageviw.hidden = false
    privateImageViw.hidden = true

}
func PersonalButtonTapped()
{
//    publicImageviw.hidden = false
//    privateImageViw.hidden = true
    publicImageviw.hidden = true
    privateImageViw.hidden = false
}


    func PersonalImgButtonTapped(sender:UIButton)
    {
     
        print ("imagetapped /%d",sender.tag)
        
       // sender.selected = !sender.selected
        let view = sender.superview
        
        let publicImage1  = view?.superview!.viewWithTag(7000) as! UIImageView
        
         let personalimage1  = view?.superview!.viewWithTag(7001) as! UIImageView
        
         publicImage1.image = GRAPHICS.CREATE_POST_TICK_BTN_IMG()
         personalimage1.image = GRAPHICS.CREATE_POST_TICK_BTN_IMG()
        
      //  personalimage1.hidden = false
       // publicImage1.hidden = false
        
       // publicImage1.backgroundColor = appPinkColor
       // personalimage1.backgroundColor = grayColor
        
       // personalimage1.hidden = false
       // publicImage1.hidden = false
        
        
        
        
        
        //        let imageView = view?.viewWithTag(sender.tag + 100) as? UIImageView
        //            if(sender.selected){
        //                imageView!.hidden = false
        //            }
        //            else{
        //                imageView!.hidden = true
        //            }
//        for imgView in (view?.subviews)!{
//            if imgView is UIImageView{
//                if(sender.selected){
//                    //imgView.hidden = false
                    if(sender.tag == 2000)
                    {
                        personalimage1.hidden = true
                        publicImage1.hidden = false
                        
                    }
        else
//                    else if(sender.tag == 2001)
                    {
                        personalimage1.hidden = false
                        publicImage1.hidden = true
                        
                    }
                    
               // }
//                else
//                {
//                    imgView.hidden = true
//                    
//                   
//                    
//                }
          //  }
            
        //}

    
    }
    
    
    func closeBtnAction(sender: UIButton)
    {
//        self.parentViewController?.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
//        
//        let homePresentVc = IBHomePageVcViewController()
//        
//        self.presentViewController(homePresentVc, animated: true, completion: nil)
    self.view.endEditing(true)
    self .dismissViewControllerAnimated(true, completion: { () -> Void in
    
    })
        //setFirstImage(-1)
        
    }
    func doneBtnAction(sender: UIButton)
    {
        //self.dismissViewControllerAnimated(true, completion: {})
        //setFirstImage(-1)
        
 //m_addCaptionTF.text as! String!;.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) == ""
        self.view.endEditing(true)
//        if validation()
//        {
        
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        
        let userDetailUD = NSUserDefaults()
        let userIdStr = userDetailUD.objectForKey("user_id") as! String;
        
        let addCaptionText : String = (m_addCaptionTF.text )!
            
        serverApi.API_addFeed("2", userId: userIdStr, post_heading:"TestIntoBuy", post_text: addCaptionText,comment:"",postrImage1:postImage1Data, postrImage2: postImage2Data, postrImage3: postImage3Data, postrImage4:postImage4Data,wall_status:imageType)
            
     //   }
    }
    
    func API_CALLBACK_AddFeed(resultDict: NSDictionary)
    {
        NSLog("result dictionary");
        
         SwiftLoader.hide()
        
        let errorCode = (resultDict.objectForKey("error_code") as? String)!
        if errorCode == "1"
        {
            NSLog("home page navigation");
            let vc = self.presentedViewController
            let vc1 = self.presentingViewController
            
//            postImage1Data = nil
//            postImage2Data = nil
//            postImage3Data = nil
            
            self .dismissViewControllerAnimated(true, completion: { () -> Void in
                
            })
            
         //   self.dismissViewControllerAnimated(true, completion: nil)
//              self.parentViewController?.parentViewController?.dismissViewControllerAnimated(true, completion: nil)
            
         //    NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("someSelector"), userInfo: nil, repeats: false)

//            var dispatchTime: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
//            dispatch_after(dispatchTime, dispatch_get_main_queue(), {
            
//            })
            
            
//            self.navigationController!.popViewControllerAnimated(false)
            
//            let homePresentVc = IBHomePageVcViewController()
//            
//            self.presentViewController(homePresentVc, animated: true, completion: nil)
            
        }
        else
        {
            showAlertViewWithMessage((resultDict .objectForKey("msg")as? String!)!)
        }
        
    }
    func someSelector() {
        let appDelegate = AppDelegate()
        if let viewControllers = appDelegate.navController?.viewControllers
        {
            for vc in viewControllers
            {
                // pop to final registration screen process
                if vc.isKindOfClass(IBHomePageVcViewController)
                {
                    appDelegate.navController?.popToViewController(vc, animated: false)
                    break
                }
            }
        }
//        let appDelegate = AppDelegate()
////        if let vcArray = appDelegate.navController
////        {
//            for controller in appDelegate.navController!.viewControllers  {
//                if controller.isKindOfClass(IBHomePageVcViewController) {
//                    self.navigationController?.popToViewController(controller as UIViewController, animated: true)
//                    break
//                }
//            }
// 
//    //    }
        
        }
    // MARK: Api delegate
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
//        m_forgotBgView.removeFromSuperview()
        showAlertViewWithMessage(errorMessage);
    }
    
    
    
    //MARK: - textfield delegae
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField .resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        textField.inputAccessoryView = self.createKeyboardToolbar()
        return true
    }
    
    func setRightDropDownForSelectCategory(textField2:UITextField)->()
    {
        let rightButton = UIButton(frame:CGRectMake(200, 0, 16, 16))
        
        // rightButton.frame = CGRectMake(textField2.frame.width-100, 0, 16, 16)
        rightButton .setBackgroundImage(GRAPHICS.DROP_DOWN_IMAGE(), forState: .Normal)
        //rightButton.backgroundColor = UIColor.clearcolor()
        rightButton.addTarget(self, action: #selector(IBUploadImageViewController.categoryDropDownClicked(_:)), forControlEvents: .TouchUpInside)
        textField2.rightView = rightButton
        textField2.rightViewMode = UITextFieldViewMode.Always
    }
    func categoryDropDownClicked(sender:UIButton)
    {
        NSLog("select category button clicked")
        isCategoryPickerShown = true
      //  self.categoryMethod()
        
    }
    
    func setRightDropDownForSelctSubCategory(textField2:UITextField)->()
    {
        let rightButton:UIButton = UIButton()
        rightButton.frame = CGRectMake(textField2.frame.width-40, 0, 16, 16)
        rightButton .setBackgroundImage(GRAPHICS.DROP_DOWN_IMAGE(), forState: .Normal)
        //rightButton.backgroundColor = UIColor.clearcolor()
        rightButton.addTarget(self, action: #selector(IBUploadImageViewController.subCategoryDropDownClicked(_:)), forControlEvents: .TouchUpInside)
        textField2.rightView = rightButton
        textField2.rightViewMode = UITextFieldViewMode.Always
    }
    func subCategoryDropDownClicked(sender:UIButton)
    {
        
        NSLog("sub category button clicked")
        isCategoryPickerShown = false
       // self.categoryMethod()
        
    }
    func createKeyboardToolbar()->UIToolbar
    {
        m_keyboardToolBar.frame = CGRectMake(0, 0, 320, 40)
        m_keyboardToolBar.barStyle = .Default
        
        var image1:UIImage = GRAPHICS.PICKER_LEFT_ARROW_BTN()
        image1 = image1.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        
       // let previousButton  = UIBarButtonItem(image: image1, style:UIBarButtonItemStyle.Plain, target: self, action: "textFieldPreviousButtonClicked")
        
        var nextButtonImage:UIImage = GRAPHICS.PICKER_RIGHT_ARROW_BTN()
        nextButtonImage = nextButtonImage.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        
        //let NextButton = UIBarButtonItem(image:nextButtonImage, style: UIBarButtonItemStyle.Plain, target: self, action: "textFieldNextButtonClicked")
        
        let spaceBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let doneButtonItem = UIBarButtonItem(title: Done, style:.Done, target: self, action: #selector(IBUploadImageViewController.textFieldDoneButtonClicked))
        
        m_keyboardToolBar.setItems([spaceBarButtonItem, doneButtonItem], animated: false)
        
        return m_keyboardToolBar
        
    }
    
    func textFieldDoneButtonClicked()
    {
         m_addCaptionTF.resignFirstResponder()
          self.view.endEditing(true)
    
    }
    func setLeftViewtotheTextfield(myTextField:UITextField)
    {
        let paddingView = UIView(frame: CGRectMake(0, 0, 15, myTextField.frame.height))
        myTextField.leftView = paddingView
        myTextField.leftViewMode = UITextFieldViewMode.Always
    }
    
    func validation()->Bool
    {
        if m_addCaptionTF.text == ""
        {
            showAlertViewWithMessage("Please enter a valid Caption")
            return false
        
        }
    return true
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
