//
//  IBPurchasePointsViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 27/01/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBPurchasePointsViewController: IBBaseViewController,UITableViewDelegate,UITableViewDataSource,ServerAPIDelegate {
    
    var m_purchaseScrollView = UIScrollView()
    var m_purchaseTblView = UITableView()
    var m_selectionIndex = Bool()
    var packageDict = NSDictionary()
    var m_purchaseArray = NSMutableArray()
    
    override func viewDidLoad() {
        bottomVal = 5
        toShowActivityBtn = true
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.m_settingsButton.hidden = true
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_titleLabel.text = Settings_Title
        m_purchaseArray = ["Total Coins today","Coins balance","Top up","Upgrade"];
       // amountArray = ["$ 199","$ 399","$ 599"];
        SwiftLoader.show(title: Loading, animated: true)
            let serverAPi = ServerAPI()
            serverAPi.delegate = self
            serverAPi.API_getrewardpointsplans(getUserIdFromUserDefaults())
        
        m_purchaseScrollView.frame = self.m_bgImageView.bounds
        self.m_bgImageView .addSubview(m_purchaseScrollView)
        
        
        
    }
    func createViewForPurchase()
    {
        var xPos:CGFloat = 10
        var yPos = m_purchaseScrollView.bounds.origin.y
        var width:CGFloat = GRAPHICS.Screen_Width() - 20
        var height:CGFloat = 40
        
        let headingLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        headingLbl.text = "My coins"
        headingLbl.font = GRAPHICS.FONT_BOLD(12)
        m_purchaseScrollView.addSubview(headingLbl)
        
        
        xPos = GRAPHICS.Screen_X()
        yPos = headingLbl.frame.maxY
        width = GRAPHICS.Screen_Width()
        height = m_purchaseScrollView.frame.size.height - headingLbl.frame.size.height
        m_purchaseTblView = UITableView(frame: CGRectMake(xPos,yPos,width,height))
        m_purchaseTblView.delegate = self
        m_purchaseTblView.dataSource = self
        m_purchaseTblView.separatorStyle = .None
        m_purchaseScrollView.addSubview(m_purchaseTblView)
    }
    // table view delegates
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return m_purchaseArray.count
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if m_selectionIndex == false{
            return 1
        }
        else{
            if(section == 2)
            {
                let packageArray = packageDict.objectForKey("reward_points_plan") as! NSArray
                return packageArray.count + 1;
            }
            else{
                return 1
            }
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell:IBPurchasePointsCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBPurchasePointsCell
        
        if (cell == nil) {
            cell = IBPurchasePointsCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        cell?.m_titleLbl.text = m_purchaseArray.objectAtIndex(indexPath.section) as? String
        if(indexPath.section <= 1)
        {
            cell?.m_amountBtn.hidden = true
            cell?.m_arrowBtn.hidden = true
            cell!.showOrHideDivider(false)
            if indexPath.section == 0
            {
                cell?.m_valueLbl.text = String(format : "%@ coins",(packageDict.objectForKey("today_coin_balance") as? String)!)
            }
            else{
                cell?.m_valueLbl.text = String(format : "%@ coins",(packageDict.objectForKey("coin_balance") as? String)!)
            }
        }
        else{
            if(indexPath.section == 2){
                // condition to show and hide the roee
                
                if(m_selectionIndex ){
                    cell?.m_valueLbl.hidden = true
                    //condition for first row
                    if(indexPath.row == 0){
                        cell!.showOrHideDivider(false)
                        cell?.m_arrowBtn.selected = true
                        cell?.m_arrowBtn.hidden = false
                        cell?.m_amountBtn.hidden = true
                        cell?.m_titleLbl.text = m_purchaseArray.objectAtIndex(2) as? String
                    }
                        // else for rest of rows
                    else{
                        cell!.showOrHideDivider(true)
                        cell?.m_arrowBtn.hidden = true
                        cell?.m_amountBtn.hidden = false
                        cell?.m_amountBtn.tag = indexPath.row - 1
                        let packageArray = packageDict.objectForKey("reward_points_plan") as! NSArray
                        let packageArrayDict = packageArray.objectAtIndex(indexPath.row - 1) as? NSDictionary

                        cell?.m_titleLbl.text = packageArrayDict?.objectForKey("name") as? String
                        let amountFloat = Float((packageArrayDict?.objectForKey("amount") as? String)!)
                        let amountInt = Int(amountFloat!)
                        let amountStr = String(format: "$ %d",amountInt)
                        cell?.m_amountBtn.setTitle(amountStr, forState: .Normal)
                        cell?.m_amountBtn.addTarget(self, action: #selector(IBPurchasePointsViewController.amountBtnAction(_:)), forControlEvents: .TouchUpInside)
                    }
                }
                else{
                    cell?.m_arrowBtn.selected = false
                    cell?.m_valueLbl.hidden = true
                    cell?.m_amountBtn.hidden = true
                    cell?.m_arrowBtn.hidden = false
                }
            }
            else{
                cell!.showOrHideDivider(false)
                cell?.m_valueLbl.hidden = true
                cell?.m_amountBtn.hidden = true
                cell?.m_arrowBtn.hidden = false
            }
        }
        return cell!;
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section == 2){
            if(m_selectionIndex == false){
                m_selectionIndex = true
            }
            else{
                m_selectionIndex = false
            }
            m_purchaseTblView.reloadData()
        }
        
    }
    //MARK:- serverapi delegate
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    
    func API_CALLBACK_getrewardpointsplans(resultDict: NSDictionary) {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"
        {
            packageDict = resultDict.objectForKey("result") as! NSDictionary
            self.createViewForPurchase()
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    func API_CALLBACK_purchaseRewardPointPlan(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"
        {
            showAlertViewWithMessage(resultDict.objectForKey("result") as! String)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                let serverAPi = ServerAPI()
                serverAPi.delegate = self
                serverAPi.API_getrewardpointsplans(getUserIdFromUserDefaults())
            })
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
        
    }

    
    func amountBtnAction(sender : UIButton)
    {
        let packageArray = packageDict.objectForKey("reward_points_plan") as! NSArray
        let packageArrayDict = packageArray.objectAtIndex(sender.tag) as? NSDictionary
        let planId = (packageArrayDict?.objectForKey("id") as? String)!
        SwiftLoader.show(title: Loading, animated: true)

        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_purchaseRewardPointPlan(getUserIdFromUserDefaults(), reward_plan_Id: planId)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
