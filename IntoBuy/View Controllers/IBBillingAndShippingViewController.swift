//
//  IBBillingAndShippingViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 21/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBBillingAndShippingViewController: IBBaseViewController,ServerAPIDelegate {

    let shippingView = UIView()
    let billingView = UIView()
    var shippingDict = NSDictionary()
    var billingDict = NSDictionary()
    var totalApi = 2
    var completedApi = 0
    
//    override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(true)
//        createAPIForShippingDetails()
//    }
    
    override func viewDidLoad() {
        bottomVal = 5
        toShowActivityBtn = true
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.m_settingsButton.hidden = true
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_titleLabel.text = Settings_Title
        createAPIForShippingDetails()
//        createControlsForShippingDetails()
//        createControlsForBillingDetails()
        
    }
    
    func createAPIForShippingDetails()
    {
        let userIdStr = getUserIdFromUserDefaults()
        SwiftLoader.show(title: "Loading...", animated: true)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getShippingAddress(userIdStr)
    }
    
    func createAPIForBillingDetails()
    {
        let userIdStr = getUserIdFromUserDefaults()
        SwiftLoader.show(title: Loading, animated: true)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getBillingdetails(userIdStr)
    }

    
    func createControlsForShippingDetails() -> () {
        
        var xPos = GRAPHICS.Screen_X() + 10
        var yPos = self.m_bgImageView.bounds.origin.y
        var width = GRAPHICS.Screen_Width() - 20
        var height = CGFloat(20)
        
        shippingView.frame = CGRectMake(xPos, yPos, width, height)
        self.m_bgImageView.addSubview(shippingView)
        
        xPos = shippingView.bounds.origin.x
        yPos = shippingView.bounds.origin.y + 20
        height = 15
        
        let titleLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        titleLbl.text = "Shipping Details"
        titleLbl.font = GRAPHICS.FONT_BOLD(12)
        shippingView.addSubview(titleLbl)
        
        yPos = titleLbl.frame.maxY + 22
        let subTitleLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        subTitleLbl.text = String(format: "%@:","Current quick settings")
        subTitleLbl.font = GRAPHICS.FONT_BOLD(12)
        shippingView.addSubview(subTitleLbl)
        
        yPos = subTitleLbl.frame.maxY + 15
        let nameLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        nameLbl.text = shippingDict.objectForKey("name") as? String//"Roy Wong"
        nameLbl.textColor = grayColor
        nameLbl.font = GRAPHICS.FONT_REGULAR(12)
        shippingView.addSubview(nameLbl)
        
        let address = String(format : "%@ %@ %@ %@ %@\n%@",(shippingDict.objectForKey("address1") as! String),(shippingDict.objectForKey("address2") as! String),(shippingDict.objectForKey("city") as! String),(shippingDict.objectForKey("state") as! String),(shippingDict.objectForKey("country_name") as! String),(shippingDict.objectForKey("zip") as! String))

        yPos = nameLbl.frame.maxY
        width = shippingView.frame.size.width/1.8
        let addressLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        addressLbl.text = address//"140, Paya  lebar road #09-22,AZ Building Singapore, Singapore 409015"
        addressLbl.font = GRAPHICS.FONT_REGULAR(12)
        addressLbl.numberOfLines = 0
        addressLbl.textColor = grayColor
        addressLbl.lineBreakMode = NSLineBreakMode.ByWordWrapping
        addressLbl.frame.size.height = heightForView(addressLbl.text!, font: GRAPHICS.FONT_REGULAR(12)!, width: width)
        shippingView.addSubview(addressLbl)
        
        yPos = addressLbl.frame.maxY + 20
        let phNoLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        phNoLbl.text = (shippingDict.objectForKey("contact") as! String)//"+91-651651651"
        phNoLbl.font = GRAPHICS.FONT_REGULAR(12)
        phNoLbl.textColor = grayColor
        shippingView.addSubview(phNoLbl)

        
        
        shippingView.frame.size.height = phNoLbl.frame.maxY + 10
        addBottomBorder(shippingView)
        createAPIForBillingDetails()
    }
    
    func createControlsForBillingDetails() -> () {
        
        var xPos = GRAPHICS.Screen_X() + 10
        var yPos = shippingView.frame.maxY
        var width = GRAPHICS.Screen_Width() - 20
        var height = CGFloat(20)
        
        billingView.frame = CGRectMake(xPos, yPos, width, height)
        self.m_bgImageView.addSubview(billingView)
        
        xPos = billingView.bounds.origin.x
        yPos = billingView.bounds.origin.y + 15
        height = 15
        
        let titleLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        titleLbl.text = "Billing Details"
        titleLbl.font = GRAPHICS.FONT_BOLD(12)
        billingView.addSubview(titleLbl)

        yPos = titleLbl.frame.maxY + 15
        width = billingView.frame.size.width/1.8
        let address = String(format : "%@ %@ %@ %@ %@\n%@",(billingDict.objectForKey("address") as! String),(billingDict.objectForKey("address2") as! String),(billingDict.objectForKey("city") as! String),(billingDict.objectForKey("state") as! String),(billingDict.objectForKey("country_name") as! String),(billingDict.objectForKey("zip") as! String))

        
        let addressLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        addressLbl.text = address//"140, Paya  lebar road #09-22,AZ Building Singapore, Singapore 409015"
        addressLbl.font = GRAPHICS.FONT_REGULAR(12)
        addressLbl.numberOfLines = 0
        addressLbl.textColor = grayColor
        addressLbl.lineBreakMode = NSLineBreakMode.ByWordWrapping
        addressLbl.frame.size.height = heightForView(addressLbl.text!, font: GRAPHICS.FONT_REGULAR(12)!, width: width)
        billingView.addSubview(addressLbl)
        
        

        billingView.frame.size.height = addressLbl.frame.maxY + 15
        addBottomBorder(billingView)
        createControlsForEditing(billingView.frame.maxY)
    }
    func createControlsForEditing(yPos : CGFloat)
    {
        let xPos = GRAPHICS.Screen_X() + 10
        var yPos =  yPos
//        let shippingAddr = shippingDict.objectForKey("address1") as! String
//        let billingAddr = billingDict.objectForKey("address1") as! String
//        if let val = dict[key] {
//            // now val is not nil and the Optional has been unwrapped, so use it
//        }
        let width = GRAPHICS.Screen_Width() - 20
        let height = CGFloat(50)
        
        
        let editShippingdetailsLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        editShippingdetailsLbl.text = "Edit Shipping Details"
        editShippingdetailsLbl.font = GRAPHICS.FONT_REGULAR(12)
        self.m_bgImageView.addSubview(editShippingdetailsLbl)
        editShippingdetailsLbl.userInteractionEnabled = true
        self.addBottomBorder(editShippingdetailsLbl)
        editShippingdetailsLbl.textColor = grayColor
        setArrowButton(editShippingdetailsLbl)
        
        let shippingGesture = UITapGestureRecognizer(target: self, action: #selector(IBBillingAndShippingViewController.shippingTapGesture))
        editShippingdetailsLbl.addGestureRecognizer(shippingGesture)

        yPos = editShippingdetailsLbl.frame.maxY
        let editBillingdetailsLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        editBillingdetailsLbl.text = "Edit Billing Details"
        editBillingdetailsLbl.font = GRAPHICS.FONT_REGULAR(12)
        self.m_bgImageView.addSubview(editBillingdetailsLbl)
        editBillingdetailsLbl.userInteractionEnabled = true
        self.addBottomBorder(editBillingdetailsLbl)
        setArrowButton(editBillingdetailsLbl)
        editBillingdetailsLbl.textColor = grayColor

        let billingGesture = UITapGestureRecognizer(target: self, action: #selector(IBBillingAndShippingViewController.billingTapGesture))
        editBillingdetailsLbl.addGestureRecognizer(billingGesture)
        
    }
    func shippingTapGesture() -> () {
        let shippingView = IBShippingDetailsViewController()
        self.navigationController?.pushViewController(shippingView, animated: true)
    }
    
    func billingTapGesture() -> () {
        let billingView = IBBillingDetailsViewController()
        self.navigationController?.pushViewController(billingView, animated: true)

    }


    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    func addBottomBorder(view: UIView)
    {
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRectMake(0, view.frame.size.height-1.0, view.frame.size.width, 1.0)
        bottomBorder.backgroundColor = UIColor.lightGrayColor().CGColor
        view.layer.addSublayer(bottomBorder)
    }
    
    func setArrowButton(view : UIView) -> () {
        let arrowBtnImg = GRAPHICS.SETTINGS_ARROW_BTN_IMAGE()
        var width = arrowBtnImg.size.width/2
        var height = arrowBtnImg.size.height/2
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = arrowBtnImg.size.width/1.7
            height = arrowBtnImg.size.height/1.7
        }
        let posX = view.frame.size.width - width - 15
        let posY = (view.frame.size.height - height)/2
        
        let arrowBtn = UIButton(frame : CGRectMake(posX,posY,width,height))
        arrowBtn.setBackgroundImage(arrowBtnImg, forState: .Normal)
        view.addSubview(arrowBtn)
    }
    
    //MARK:- api delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        GRAPHICS.showAlert(errorMessage);
    }
    //Api for shipping details
    func API_CALLBACK_getShippingAddress(resultDict: NSDictionary)
    {
        if resultDict.objectForKey("error_code")as! String == "1"
        {
            let resultArray = resultDict.objectForKey("result") as! NSArray
            if resultArray.count > 0
            {
                shippingDict = resultArray.objectAtIndex(0) as! NSDictionary
                createControlsForShippingDetails()
            }
            else
            {
               // createControlsForEditing(self.m_bgImageView.bounds.origin.y)
            }
            completedApi = completedApi + 1
        }
        else
        {
            createAPIForBillingDetails()
            showAlertViewWithMessage(resultDict.objectForKey("msg")as! String)
            SwiftLoader.hide()
        }
        if totalApi == completedApi
        {
            SwiftLoader.hide()
        }
    }
    
    // API for billing details
    func API_CALLBACK_getBillingdetails(resultDict: NSDictionary) {
        let errorCode = (resultDict .objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
            let resultArray = resultDict.valueForKey("result") as! NSArray
            if resultArray.count > 0{
                billingDict = resultArray.objectAtIndex(0) as! NSDictionary
                createControlsForBillingDetails()
            }
            else
            {
                showAlertViewWithMessage("Billing Details not available")
                var yPos:CGFloat
                if let val = shippingDict["address1"]
                {
                    if let val2 = billingDict["address1"]
                    {
                        createControlsForEditing(billingView.frame.maxY)
                    }
                    else
                    {
                        createControlsForEditing(shippingView.frame.maxY)
                    }
                }
                else
                {
                    createControlsForEditing(self.m_bgImageView.bounds.origin.y)//billingView.frame.maxY
                }

            }
            completedApi = completedApi + 1
        }
        else
        {
            showAlertViewWithMessage((resultDict .objectForKey("msg")as? String)!)
            createControlsForEditing(self.m_bgImageView.bounds.origin.y)
            SwiftLoader.hide()
        }
        if totalApi == completedApi
        {
            SwiftLoader.hide()
        }

    }



}
