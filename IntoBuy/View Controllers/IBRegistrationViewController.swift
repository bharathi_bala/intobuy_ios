//
//  IBRegistrationViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 08/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit
import Foundation

class IBRegistrationViewController: IBBaseViewController,UITextFieldDelegate,ServerAPIDelegate,CountryProtocol,UIPickerViewDataSource,UIPickerViewDelegate {

    var m_regScrollView = UIScrollView()
    var m_firstNameTF:UITextField!
    var m_lastNameTF:UITextField!
    var m_emailTF:UITextField!
    var m_reTypeEmailTF:UITextField!
    var m_userNameTF:UITextField!
    var m_passWordTF:UITextField!
    var m_countryTF:UITextField!
    var m_dayTF:UITextField!
    var m_monthTF:UITextField!
    var m_yearTF:UITextField!
    var m_genderTF:UITextField!
    var toolBar:UIToolbar!
    var keyBoardTool:UIToolbar!
    var datePicker:UIDatePicker!
    var genderPicker:UIPickerView!
    var dateStr:String!
    var countryId = ""
    var countryName = ""
    var m_countryArray = NSMutableArray()
    var genderArray = NSMutableArray()
    var pickerIndex:Int = 0
    var genderStr = "Male"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(true)
        self.m_settingsButton.hidden = true
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_titleLabel.text = Register
        
        m_regScrollView.frame = self.m_bgImageView.bounds
        self.m_bgImageView .addSubview(m_regScrollView)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IBRegistrationViewController.resignKeyboard(_:)))
        m_regScrollView.addGestureRecognizer(tapGesture)
        genderArray = ["Male","Female"]
        self.createKeyboardToolBar()
        self.createControls()
        // Do any additional setup after loading the view.
    }
//    override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(true)
//        
//        m_countryTF.text = countryName
//    }
    func createControls()
    {
        var xPos = GRAPHICS.Screen_X() + 15
        var yPos = m_regScrollView.bounds.origin.y + 40
        var width = GRAPHICS.Screen_Width() - 30
        var height:CGFloat = 15
        let lblPadding:CGFloat = 2
        let textPadding:CGFloat = 5
        let textBoxImg = GRAPHICS.SETTINGS_BIG_TEXTBOX_IMAGE()
        
        let firstNameLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        firstNameLbl.text = String(format: "%@:",FirstName)
        firstNameLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_regScrollView .addSubview(firstNameLbl)
        
        yPos = firstNameLbl.frame.maxY + lblPadding
        height = 25
        m_firstNameTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_firstNameTF.delegate = self
        m_firstNameTF.background = textBoxImg
        m_firstNameTF.returnKeyType = UIReturnKeyType.Next
        m_firstNameTF.inputAccessoryView = self.createKeyboardToolBar()
        m_firstNameTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_firstNameTF)
        m_regScrollView .addSubview(m_firstNameTF)
        
        yPos = m_firstNameTF.frame.maxY + textPadding
        height = 15
        let lastNameLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        lastNameLbl.text = String(format: "%@:",LastName)
        lastNameLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_regScrollView .addSubview(lastNameLbl)
        
        yPos = lastNameLbl.frame.maxY + lblPadding
        height = 25
        m_lastNameTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_lastNameTF.delegate = self
        m_lastNameTF.returnKeyType = UIReturnKeyType.Next
        m_lastNameTF.background = textBoxImg
        m_lastNameTF.inputAccessoryView = self.createKeyboardToolBar()
        m_lastNameTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_lastNameTF)
        m_regScrollView .addSubview(m_lastNameTF)
        
        yPos = m_lastNameTF.frame.maxY + textPadding
        height = 15
        let emailLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        emailLbl.text = String(format: "%@:",EmailAddress)
        emailLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_regScrollView .addSubview(emailLbl)
        
        yPos = emailLbl.frame.maxY + lblPadding
        height = 25
        m_emailTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_emailTF.delegate = self
        m_emailTF.returnKeyType = UIReturnKeyType.Next
        m_emailTF.keyboardType = UIKeyboardType.EmailAddress
        m_emailTF.background = textBoxImg
        m_emailTF.inputAccessoryView = self.createKeyboardToolBar()
        m_emailTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_emailTF)
        m_regScrollView .addSubview(m_emailTF)
        
        yPos = m_emailTF.frame.maxY + textPadding
        height = 15
        let reTypeEmailLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        reTypeEmailLbl.text = String(format: "%@:",RetypeEmail)
        reTypeEmailLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_regScrollView .addSubview(reTypeEmailLbl)
        
        yPos = reTypeEmailLbl.frame.maxY + lblPadding
        height = 25
        m_reTypeEmailTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_reTypeEmailTF.delegate = self
        m_reTypeEmailTF.returnKeyType = UIReturnKeyType.Next
        m_reTypeEmailTF.background = textBoxImg
        m_reTypeEmailTF.inputAccessoryView = self.createKeyboardToolBar()
        m_reTypeEmailTF.keyboardType = UIKeyboardType.EmailAddress
        m_reTypeEmailTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_reTypeEmailTF)
        m_regScrollView .addSubview(m_reTypeEmailTF)
        
        yPos = m_reTypeEmailTF.frame.maxY + textPadding
        height = 15
        let userNameLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        userNameLbl.text = String(format: "%@:",UserName)
        userNameLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_regScrollView .addSubview(userNameLbl)
        
        yPos = userNameLbl.frame.maxY + lblPadding
        height = 25
        m_userNameTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_userNameTF.delegate = self
        m_userNameTF.returnKeyType = UIReturnKeyType.Next
        m_userNameTF.background = textBoxImg
        m_userNameTF.inputAccessoryView = self.createKeyboardToolBar()
        m_userNameTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_userNameTF)
        m_regScrollView .addSubview(m_userNameTF)
        
        yPos = m_userNameTF.frame.maxY + textPadding
        height = 15
        let passwordLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        passwordLbl.text = String(format: "%@:",PassWord)
        passwordLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_regScrollView .addSubview(passwordLbl)
        
        yPos = passwordLbl.frame.maxY + lblPadding
        height = 25
        m_passWordTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_passWordTF.delegate = self
        m_passWordTF.returnKeyType = UIReturnKeyType.Next
        m_passWordTF.secureTextEntry = true
        m_passWordTF.background = textBoxImg
        m_passWordTF.inputAccessoryView = self.createKeyboardToolBar()
        setLeftViewToTheTextField(m_passWordTF)
        m_passWordTF.font = GRAPHICS.FONT_REGULAR(14)
        m_regScrollView .addSubview(m_passWordTF)
        
        yPos = m_passWordTF.frame.maxY + textPadding
        height = 15
        let countryLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        countryLbl.text = String(format: "%@:",Country)
        countryLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_regScrollView .addSubview(countryLbl)
        
        yPos = countryLbl.frame.maxY + lblPadding
        height = 25
        m_countryTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_countryTF.delegate = self
        m_countryTF.returnKeyType = UIReturnKeyType.Next
        m_countryTF.background = textBoxImg
        m_countryTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_countryTF)
        m_regScrollView .addSubview(m_countryTF)

        yPos = m_countryTF.frame.maxY + textPadding*2
        height = 15
        width = GRAPHICS.Screen_Width()/2 - 30
        let dobLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        dobLbl.text = String(format: "%@:%@",DOB,"(DDMMYYYY)")
        dobLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_regScrollView .addSubview(dobLbl)
        
        let dateTextBox = GRAPHICS.SETTINGS_SMALL_TEXTBOX_IMAGE()
        yPos = dobLbl.frame.maxY + lblPadding*2
        height = 25
        width = 30
        m_dayTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_dayTF.delegate = self
        m_dayTF.returnKeyType = UIReturnKeyType.Next
        m_dayTF.background = dateTextBox
        m_dayTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_dayTF)
        m_regScrollView .addSubview(m_dayTF)
        
        xPos = m_dayTF.frame.maxX + 10
        m_monthTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_monthTF.delegate = self
        m_monthTF.returnKeyType = UIReturnKeyType.Next
        m_monthTF.background = dateTextBox
        m_monthTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_monthTF)
        m_regScrollView .addSubview(m_monthTF)

        xPos = m_monthTF.frame.maxX + 10
        width = 60
        m_yearTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_yearTF.delegate = self
        m_yearTF.returnKeyType = UIReturnKeyType.Next
        m_yearTF.background = dateTextBox
        m_yearTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_yearTF)
        m_regScrollView .addSubview(m_yearTF)
        
        xPos = m_yearTF.frame.maxX + 30
        yPos = dobLbl.frame.origin.y
        width = GRAPHICS.Screen_Width()/5
        let genderLbl = UILabel(frame: CGRectMake(xPos,yPos,width,dobLbl.frame.size.height))
        genderLbl.text = String(format: "%@:",Gender)
        genderLbl.font = GRAPHICS.FONT_REGULAR(14)
        m_regScrollView .addSubview(genderLbl)
        
        yPos = m_yearTF.frame.origin.y
        m_genderTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_genderTF.delegate = self
        m_genderTF.returnKeyType = UIReturnKeyType.Next
       // m_genderTF.textAlignment = .Center
        m_genderTF.background = dateTextBox
        m_genderTF.font = GRAPHICS.FONT_REGULAR(14)
        m_genderTF.inputAccessoryView = self.createKeyboardToolBar()
        setLeftViewToTheTextField(m_genderTF)
        m_regScrollView .addSubview(m_genderTF)
        
        let signUpBtnImg = GRAPHICS.SETTINGS_BILLING_BTN_IMAGE()
        width = signUpBtnImg.size.width/2
        height = signUpBtnImg.size.height/2
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = signUpBtnImg.size.width/1.7
            height = signUpBtnImg.size.height/1.7
        }
        yPos = m_genderTF.frame.maxY + 30
        xPos = (GRAPHICS.Screen_Width() - width)/2
        let signUpBtn = UIButton(frame: CGRectMake(xPos, yPos, width, height))
        signUpBtn.setBackgroundImage(signUpBtnImg, forState: UIControlState.Normal)
        signUpBtn.addTarget(self, action: #selector(IBRegistrationViewController.signUpBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        signUpBtn.setTitle(SignUp, forState: UIControlState.Normal)
        signUpBtn.titleLabel!.font = GRAPHICS.FONT_REGULAR(14)
        m_regScrollView .addSubview(signUpBtn)
        m_regScrollView.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), signUpBtn.frame.maxY+10)


    }
    func signUpBtnAction(sender: AnyObject)
    {
        

        if(validation())
        {
            SwiftLoader.show(animated: true)
            SwiftLoader.show(title:"Loading...", animated:true)

            //let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_callRegisterWebService(m_firstNameTF.text!, LastName: m_lastNameTF.text!, Email: m_emailTF.text!, PhoneNo: "", password: m_passWordTF.text!, DeviceId: "", Device: "IOS", Gender: m_genderTF.text!, DOB: dateStr, UserName: m_userNameTF.text!, Country: countryId, AboutMe: "", UserType: "-1", FaceBookId: "")
        }
    }
    // method for email validation
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    // textfield validation method
    func validation()->Bool{
        
        if m_firstNameTF.text?.isEmpty == true{
            showAlertViewWithMessage("Please enter First name")
            return false
        }
        else if m_lastNameTF.text?.isEmpty == true{
            showAlertViewWithMessage("Please enter Last name")
            return false
        }
        else if (isValidEmail(m_emailTF.text!) ==  false){
            showAlertViewWithMessage("Please enter valid email")
            return false
        }
        else if (isValidEmail(m_reTypeEmailTF.text!) ==  false){
            showAlertViewWithMessage("Please enter valid email in retype email")
            return false
        }
        else if m_reTypeEmailTF?.text?.isEmpty == true{
            showAlertViewWithMessage("Please re-enter email")
            return false
        }

        else if m_emailTF.text  != m_reTypeEmailTF.text{
            showAlertViewWithMessage("Email id different")
            return false
        }
        else if m_userNameTF.text?.isEmpty == true
        {
            showAlertViewWithMessage("Please enter username")
            return false
        }
        else if m_passWordTF?.text?.isEmpty == true{
            showAlertViewWithMessage("Please enter password")
            return false
        }
        else if m_countryTF?.text?.isEmpty == true{
            showAlertViewWithMessage("Please enter country")
            return false
        }
        else if m_dayTF?.text?.isEmpty == true{
            showAlertViewWithMessage("Please enter date of birth")
            return false
        }
        else if m_genderTF?.text?.isEmpty == true{
            showAlertViewWithMessage("Please enter gender")
            return false
        }
        return true
    }
    
    // method for picker
    func createDatePickerView()
    {
        
        let Height:CGFloat = 200.0
        let YPos:CGFloat = GRAPHICS.Screen_Height() - Height
        let Width = GRAPHICS.Screen_Width()
        if(datePicker != nil)
        {
            datePicker .removeFromSuperview()
            datePicker = nil
        }
        datePicker = UIDatePicker(frame:CGRectMake(0, YPos, Width, Height))
        datePicker?.datePickerMode = UIDatePickerMode.Date
        datePicker.backgroundColor = UIColor.whiteColor()
        datePicker.maximumDate = NSDate()
        self.m_bgImageView.addSubview(datePicker!)

    }
    func createGenderPickerView()
    {
        
        let Height:CGFloat = 200.0
        let YPos:CGFloat = GRAPHICS.Screen_Height() - Height
        let Width = GRAPHICS.Screen_Width()
        if(genderPicker != nil)
        {
            genderPicker .removeFromSuperview()
            genderPicker = nil
        }
        genderPicker = UIPickerView(frame:CGRectMake(0, YPos, Width, Height))
        genderPicker.delegate = self
        genderPicker.dataSource = self
        genderPicker.showsSelectionIndicator = true
        genderPicker.backgroundColor = UIColor.whiteColor()
        self.m_bgImageView.addSubview(genderPicker)
        
    }

    // method for keyboard toolbar
    func createKeyboardToolBar() -> UIToolbar
    {
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        keyBoardTool = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        keyBoardTool!.barStyle = UIBarStyle.BlackOpaque
        keyBoardTool!.translucent = true
        keyBoardTool!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        keyBoardTool!.sizeToFit()
        
        
       // let doneButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("nextAction"))
        let previousImage = GRAPHICS.PICKER_LEFT_ARROW_BTN()
        let nextImage = GRAPHICS.PICKER_RIGHT_ARROW_BTN()

        let prev_Btn = UIButton ()
        prev_Btn.frame = CGRectMake(10, 8, 40, 30)
        prev_Btn.backgroundColor =  UIColor.clearColor();
        prev_Btn.setBackgroundImage(previousImage, forState: .Normal)
        prev_Btn.addTarget(self, action: #selector(IBRegistrationViewController.previousAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(prev_Btn)
        
        let next_Btn = UIButton ()
        next_Btn.frame = CGRectMake(50, 8, 40, 30)
        next_Btn.backgroundColor =  UIColor.clearColor();
        next_Btn.setBackgroundImage(nextImage, forState: .Normal)
        next_Btn.addTarget(self, action: #selector(IBRegistrationViewController.nextAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(next_Btn)
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBRegistrationViewController.doneAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(done_Btn)

        
        keyBoardTool!.userInteractionEnabled = true
        return keyBoardTool

    }
    func doneBtnAction()
    {
        if(keyBoardTool != nil){
        keyBoardTool .removeFromSuperview()
        keyBoardTool = nil
        }
        self.view .endEditing(true)
        
    }
    func nextAction(){
        
        if m_firstNameTF.isFirstResponder() == true{
            
            m_firstNameTF.resignFirstResponder()
            m_lastNameTF.becomeFirstResponder()
        }
        else if m_lastNameTF.isFirstResponder() == true{
            
            m_lastNameTF.resignFirstResponder()
            m_emailTF.becomeFirstResponder()
        }
        else if m_emailTF.isFirstResponder() == true{
            
            m_emailTF.resignFirstResponder()
            m_reTypeEmailTF.becomeFirstResponder()
        }
        else if m_reTypeEmailTF.isFirstResponder() == true{
            
            m_reTypeEmailTF.resignFirstResponder()
            m_userNameTF.becomeFirstResponder()
        }
        else if m_userNameTF.isFirstResponder() == true{
            m_userNameTF.resignFirstResponder()
            m_passWordTF.becomeFirstResponder()
        }
        else if m_passWordTF.isFirstResponder() == true{
            m_passWordTF.resignFirstResponder()
            m_regScrollView .setContentOffset(CGPointMake(0, 0), animated: true)
        }
        else if m_genderTF.isFirstResponder() == true{
            m_genderTF.resignFirstResponder()
            m_regScrollView .setContentOffset(CGPointMake(0, 0), animated: true)
        }

        
    }
    
    func previousAction(){
        
        
        if m_genderTF.isFirstResponder() == true{
            
            m_genderTF.resignFirstResponder()
            m_yearTF.becomeFirstResponder()
            //self.CreateToolBar()
            //m_lastNameTF.becomeFirstResponder()
        }
        else if m_passWordTF.isFirstResponder() == true{
            
            m_passWordTF.resignFirstResponder()
            m_userNameTF.becomeFirstResponder()
        }
        else if m_userNameTF.isFirstResponder() == true{
            
            m_userNameTF.resignFirstResponder()
            m_reTypeEmailTF.becomeFirstResponder()
        }
        else if m_reTypeEmailTF.isFirstResponder() == true{
            
            m_reTypeEmailTF.resignFirstResponder()
            m_emailTF.becomeFirstResponder()
        }
        else if m_emailTF.isFirstResponder() == true{
            m_emailTF.resignFirstResponder()
            m_lastNameTF.becomeFirstResponder()
        }
        else if m_lastNameTF.isFirstResponder() == true{
            m_lastNameTF.resignFirstResponder()
            m_firstNameTF.becomeFirstResponder()
        }
        else if m_firstNameTF.isFirstResponder() == true{
            m_firstNameTF.resignFirstResponder()
        }
//        else if m_genderTF.isFirstResponder() == true{
//            m_genderTF.resignFirstResponder()
//        }
        
    }

    
    // method for tool bar
    func CreateToolBar(){
        
        if(toolBar != nil){
            toolBar.removeFromSuperview()
            toolBar = nil
        }

        toolBar = UIToolbar(frame:CGRectMake(0, (GRAPHICS.Screen_Height() - 180) - 50, view.frame.size.width, 50.0))
        toolBar!.barStyle = UIBarStyle.BlackTranslucent
        toolBar!.translucent = true
        toolBar!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar!.sizeToFit()
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(IBRegistrationViewController.doneAction))
        doneButton.tintColor = UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(IBRegistrationViewController.cancelAction))
        cancelButton.tintColor = UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0)
        
        toolBar!.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar!.userInteractionEnabled = true
        self.m_bgImageView .addSubview(toolBar)
    }
    func doneAction(){
        
        if(datePicker != nil){
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateStr = dateFormatter.stringFromDate(datePicker.date)
        let strArray = dateStr.componentsSeparatedByString("-")
        m_yearTF.text = strArray[0]
        m_monthTF.text = strArray[1]
        m_dayTF.text = strArray[2]
        datePicker .removeFromSuperview()
            datePicker = nil
        toolBar.removeFromSuperview()
            toolBar = nil
        }
        if(genderPicker != nil){
            m_genderTF.text = genderStr as String
            genderPicker.removeFromSuperview();
            genderPicker = nil
            if(toolBar != nil){
            toolBar.removeFromSuperview()
            toolBar = nil
            }
        }
        self.view.endEditing(true)
        m_regScrollView.setContentOffset(CGPointMake(0, 0), animated: true)
    }
    func cancelAction(){
        if(datePicker != nil){
        datePicker .removeFromSuperview()
            datePicker = nil
            m_dayTF.text = ""
            m_monthTF.text = ""
            m_yearTF.text = ""
        }
        if(genderPicker != nil){
            genderPicker.removeFromSuperview()
            genderPicker = nil
            m_genderTF.text = ""
        }
        if(toolBar != nil){
        toolBar.removeFromSuperview()
        toolBar = nil
        }
        m_regScrollView.setContentOffset(CGPointMake(0, 0), animated: true)
    }
    // MARK: - uipickerView delegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArray.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderArray.objectAtIndex(row) as? String
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerIndex = row
        genderStr = (genderArray.objectAtIndex(row) as? String)!
    }
    // MARK: - textfield delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == m_firstNameTF
        {
            m_firstNameTF.resignFirstResponder()
            m_lastNameTF.becomeFirstResponder()
        }
        else if textField == m_lastNameTF
        {
            m_lastNameTF.resignFirstResponder()
            m_emailTF.becomeFirstResponder()
        }
        else if textField == m_emailTF
        {
            m_emailTF.resignFirstResponder()
            m_reTypeEmailTF.becomeFirstResponder()
        }
        else if textField == m_reTypeEmailTF
        {
            m_reTypeEmailTF.resignFirstResponder()
            m_userNameTF.becomeFirstResponder()
        }
        else if textField == m_userNameTF
        {
            m_userNameTF.resignFirstResponder()
            m_passWordTF.becomeFirstResponder()
        }
        else if textField == m_passWordTF
        {
            m_passWordTF.resignFirstResponder()
        }
        else {
            m_genderTF .resignFirstResponder()
        }
        

        return true
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.autocorrectionType = .No
        if(textField == m_yearTF || textField == m_dayTF || textField == m_monthTF)
        {
            if(datePicker != nil)
            {
                datePicker .removeFromSuperview()
                datePicker = nil
                toolBar.removeFromSuperview()
                toolBar = nil
            }
            if(genderPicker != nil)
            {
                genderPicker .removeFromSuperview()
                genderPicker = nil
                toolBar.removeFromSuperview()
                toolBar = nil
            }
            var offSetPadding:CGFloat!
            if GRAPHICS.Screen_Type() == IPHONE_5
            {
                offSetPadding = 100
            }
            else
            {
                offSetPadding = 200
            }

            m_regScrollView.setContentOffset(CGPointMake(0, m_regScrollView.frame.origin.y + offSetPadding), animated: true)
            self.createDatePickerView()
            self.CreateToolBar()
            return false
        }
        else if(textField == m_countryTF)
        {
            if(datePicker != nil)
            {
                datePicker .removeFromSuperview()
                datePicker = nil
                toolBar.removeFromSuperview()
                toolBar = nil
//                if(datePicker != nil)
//                {
//                    datePicker .removeFromSuperview()
//                    datePicker = nil
//                    
//                }

            }
            if(m_countryArray.count > 0){
                self.navigateToCountryScreen()
            }
            else{
                self.callServiceForCountry()
            }

            return false
        }
        else if(textField == m_genderTF)
        {
            var offSetPadding:CGFloat!
            if(genderPicker != nil)
            {
                genderPicker .removeFromSuperview()
                genderPicker = nil
                toolBar.removeFromSuperview()
                toolBar = nil
            }
            if(datePicker != nil)
            {
                datePicker .removeFromSuperview()
                datePicker = nil
                toolBar.removeFromSuperview()
                toolBar = nil

            }
                if GRAPHICS.Screen_Type() == IPHONE_5
                {
                    offSetPadding = 130
                }
                else
                {
                    offSetPadding = 210
                }
                self.createGenderPickerView()
                self.CreateToolBar()
            m_regScrollView.setContentOffset(CGPointMake(0, m_regScrollView.frame.origin.y + offSetPadding), animated: true)

            return false

        }
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField.frame.origin.y > m_regScrollView.frame.size.height - 280
        {
            var offSetPadding:CGFloat!
            if GRAPHICS.Screen_Type() == IPHONE_5
            {
                offSetPadding = 130
            }
            else if GRAPHICS.Screen_Type() == IPHONE_4
            {
                offSetPadding = 100
//                if textField == m_genderTF
//                {
//                    offSetPadding = 210;
//                }
            }
            else
            {
                offSetPadding = 40
            }
            m_regScrollView.setContentOffset(CGPointMake(0, m_regScrollView.frame.origin.y + offSetPadding), animated: true)
        }
    }
//    func textFieldDidEndEditing(textField: UITextField)
//    {
//        if textField == m_genderTF
//        {
//            m_regScrollView.setContentOffset(CGPointMake(0, 0), animated: true)
//        }
//    }
    func resignKeyboard(sender: AnyObject)
    {
        self.view.endEditing(true)
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        m_regScrollView .setContentOffset(CGPointMake(0, 0), animated: true)
    }
    func setLeftViewToTheTextField(textField: UITextField)
    {
        let leftPlaceHolderView = UIView(frame: CGRectMake(0, 0, 10,textField.frame.size.height))
        leftPlaceHolderView.backgroundColor = UIColor.clearColor()
        textField.leftView = leftPlaceHolderView
        textField.leftViewMode = UITextFieldViewMode.Always
    }
    func sendArrayToPreviousVC(myArray: NSArray) {
        m_countryTF.text = myArray.objectAtIndex(0) as? String
        countryId = (myArray.objectAtIndex(1) as? String)!
    }
    
    // MARK: - country service api
    func callServiceForCountry()
    {
        self.view .endEditing(true)
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getActiveCountry("36")
    }
    
    // MARK: - api delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage);
    }
    func API_CALLBACK_SignUp(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = (resultDict .objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
//            
            let alert = UIAlertView(title: "IntoBuy", message:(resultDict .objectForKey("result")as? String)!, delegate: self, cancelButtonTitle:"Ok")
            alert .show()
//            let countryVC = IBSelectProfileViewController()
//            self.navigationController!.pushViewController(countryVC, animated: true)
//
        }
        else{
            showAlertViewWithMessage((resultDict .objectForKey("msg")as? String)!)
        }
    }
    
    func API_CALLBACK_getActiveCountry(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = (resultDict .objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
            if let countryAry = resultDict.objectForKey("result") as? NSArray
            {
            if countryAry.count > 0
            {
                let countryArry = resultDict.objectForKey("result") as! NSArray
                m_countryArray.addObjectsFromArray(countryArry as [AnyObject])
            }
            }
            self.navigateToCountryScreen()
        }
        else{
            showAlertViewWithMessage((resultDict .objectForKey("error_code")as? String)!)
        }
        //self.m_countryTblView .reloadData()
        
    }
    func navigateToCountryScreen()
    {
        let countryVC = IBCountryViewController()
        countryVC.mDelegate = self
        countryVC.isFromRegistration = true
        countryVC.m_countryArray = m_countryArray
        self.navigationController!.pushViewController(countryVC, animated: true)

    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
        self.navigationController! .popViewControllerAnimated(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
