//
//  IBHomeViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 25/11/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBHomeViewController: IBBaseViewController,ServerAPIDelegate {
    let m_homeBgView:UIImageView = UIImageView()
    let m_homeScrlView:UIScrollView = UIScrollView()
    let m_searchTextField:UITextField = UITextField()
    let m_categoryView = UIView()
    var gridView: GridView?
    var m_categoryStr = String()
    var m_locationDict = NSDictionary()
    var m_filterStr = String()
    var m_minPriceStr = String()
    var m_maxPriceStr = String()
    var isFromCategory = Bool()
    var isFromFilter = Bool()
    var isFromLocation = Bool()

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        if(isFromCategory)
        {
            let view = m_categoryView.viewWithTag(1)
            let label = view?.viewWithTag(21) as? UILabel
            label?.text = m_categoryStr
        }
        else if(isFromFilter)
        {
            let view = m_categoryView.viewWithTag(2)
            let label = view?.viewWithTag(22) as? UILabel
            label?.text = m_filterStr
        }
        else if(isFromLocation)
        {
            let view = m_categoryView.viewWithTag(0)
            let label = view?.viewWithTag(20) as? UILabel
            let locationStr = m_locationDict.objectForKey("address")
            if locationStr != nil{
            let addressArray = locationStr!.componentsSeparatedByString(",") as NSArray
            label?.text = addressArray.objectAtIndex(addressArray.count - 3) as? String
            }
        }
        else
        {
            
        }
    }
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
    }
    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
      //  self.hideBottomView(true)
//        let serverApi = ServerAPI()
//        serverApi.delegate = self
//        serverApi.API_PlaceSuggestion("raj")

        self.hideSettingsBtn(true)
        self.hideHeaderViewForView(false)
        self.hideBottomView(true)
        self.m_titleLabel.text = "for Her"
        self.createControlsForHomeScreen()
    
        // Do any additional setup after loading the view.
    }
    func createControlsForHomeScreen()
    {
        m_homeBgView.frame = self.m_bgImageView.frame;
        m_homeBgView.userInteractionEnabled = true
        self.m_bgImageView .addSubview(m_homeBgView)
        
        let searchView = UIView(frame: CGRectMake(m_homeBgView.bounds.origin.x,m_homeBgView.bounds.origin.y,m_homeBgView.frame.size.width,50))
        searchView.backgroundColor = grayColor
        m_homeBgView .addSubview(searchView)
        let searchImage = GRAPHICS.PRODUCT_SEARCH_IMAGE()
        var imgWidth = (searchImage.size.width)/1.5
        var imgHei = searchImage.size.height/1.5
        
        let searchImgView = UIImageView(frame: CGRectMake(searchView.bounds.origin.x + 10, (searchView.frame.size.height - imgHei)/2, imgWidth, imgHei))
        searchImgView.image = searchImage
        searchView.addSubview(searchImgView)
        
        
        
        let textImg = GRAPHICS.PRODUCT_SEARCH_TEXT_IMAGE()
        imgWidth = textImg.size.width/2
        imgHei = textImg.size.height/1.5
        m_searchTextField.frame = CGRectMake(searchImgView.frame.maxX + 5, (searchView.frame.size.height - imgHei)/2, imgWidth, imgHei)
        m_searchTextField.background = textImg
        m_searchTextField.placeholder = Search
        m_searchTextField.textAlignment = NSTextAlignment.Center
        searchView.addSubview(m_searchTextField)
        
        m_categoryView.frame = CGRectMake(GRAPHICS.Screen_X(), searchView.frame.maxY, GRAPHICS.Screen_Width(), 40);
        m_homeBgView.addSubview(m_categoryView)
        
        let categoryArray:NSMutableArray = [location,category,filter];
        let valueArray:NSMutableArray = ["India","All categories","Popular"];
        var xPos = m_categoryView.bounds.origin.x
        let yPos = m_categoryView.bounds.origin.y
        for i in 0  ..< 3
        {
            let view = UIView(frame: CGRectMake(xPos,yPos,GRAPHICS.Screen_Width()/3,m_categoryView.frame.size.height))
            view.tag = i
            m_categoryView.addSubview(view)
            
            let viewGesture = UITapGestureRecognizer(target: self, action: #selector(IBHomeViewController.selectfields(_:)))
            view.addGestureRecognizer(viewGesture);
            
            let categoryTitleLbl = UILabel(frame: CGRectMake(view.bounds.origin.x,view.bounds.origin.y+2,view.frame.size.width,15))
            categoryTitleLbl.text = String(format: "%@:",categoryArray.objectAtIndex(i) as! String)
            categoryTitleLbl.tag = i+10
            //categoryTitleLbl.backgroundColor = UIColor.greenColor()
            categoryTitleLbl.font = GRAPHICS.FONT_REGULAR(12)
            categoryTitleLbl.textAlignment = .Center
            view.addSubview(categoryTitleLbl)
            
            let lblWid = view.frame.size.width/1.5
            let categoryValueLbl = UILabel(frame: CGRectMake((view.frame.size.width - lblWid)/3,view.frame.size.height - 15,lblWid,15))
            categoryValueLbl.font = GRAPHICS.FONT_REGULAR(13)
            categoryValueLbl.text = valueArray.objectAtIndex(i) as? String;
            categoryValueLbl.tag = i+20
            categoryValueLbl.textAlignment = .Center
            view.addSubview(categoryValueLbl)
//            categoryValueLbl.sizeToFit()
//            categoryValueLbl.frame.origin.x = (view.frame.size.width - categoryValueLbl.frame.size.width)/2
            
            let backImg = GRAPHICS.PRODUCT_CATEGORY_DOWN_ARROW_IMAGE()
            let btnWidth = backImg.size.width/2
            let btnHeight = backImg.size.height/2
            
            let dropDownBtn = UIButton(frame: CGRectMake(categoryValueLbl.frame.maxX + 2,categoryValueLbl.frame.origin.y - 5,btnWidth,btnHeight))
            dropDownBtn.setBackgroundImage(backImg, forState: .Normal)
            dropDownBtn.addTarget(self, action: #selector(IBHomeViewController.dropDownBtnAction(_:)), forControlEvents: .TouchUpInside)
            view.addSubview(dropDownBtn)

            if(i == 0 || i == 1)
            {
                let dividerImg = GRAPHICS.PRODUCT_SEARCH_DIVIDER_IMAGE()
                let dividerWidth = dividerImg.size.width/2
                let dividerHeight = dividerImg.size.height/1.8
                let dividerImgView = UIImageView(frame: CGRectMake(view.frame.size.width - dividerWidth,(view.frame.size.height - dividerHeight)/2,dividerWidth,dividerHeight))
                dividerImgView.image = dividerImg
                view.addSubview(dividerImgView)
            }
            xPos = view.frame.maxX
        }
        
        
        m_homeScrlView.frame = CGRectMake(m_homeBgView.bounds.origin.x, m_categoryView.frame.maxY, GRAPHICS.Screen_Width(), m_homeBgView.frame.size.height - searchImgView.frame.size.height - 50);
        m_homeScrlView.userInteractionEnabled = true
        m_homeBgView .addSubview(m_homeScrlView)
        
        
        let coloumnCount:UInt = 2
        let rowCount:UInt = 4
        let gridImg = GRAPHICS .PRODUCT_GRID_IMAGE()
        //let gridWid = (gridImg?.size.width)!
        var gridHei = (gridImg?.size.height)!/1.8
        if (GRAPHICS .Screen_Type() == IPHONE_4 || GRAPHICS.Screen_Type() == IPHONE_5)
        {
            gridHei = (gridImg?.size.height)!/2
        }
        
        var gridFrame = CGRect.zero
        gridFrame.size.width = GRAPHICS.Screen_Width()
        gridFrame.size.height = gridHei
        let margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        self.gridView = GridView(frame: gridFrame, columns: coloumnCount, rows: rowCount, margins: margins, padding: 0.0) {
            column, row, contentView in
            let productImgView = UIImageView(frame: contentView.bounds)
            productImgView.image = gridImg
            contentView .addSubview(productImgView);
            
            let productView = UIImageView(frame: CGRectMake(productImgView.bounds.origin.x+14, productImgView.bounds.origin.y+13, productImgView.frame.size.width - 28, productImgView.frame.size.height - 58))
            productView.backgroundColor = UIColor.redColor()
            productImgView.addSubview(productView)
            
            let productNameLbl = UILabel(frame: CGRectMake(productView.frame.origin.x,productView.frame.maxY,productView.frame.size.width, 15))
            productNameLbl.text  = "Iphone 4"
            productNameLbl.font = GRAPHICS.FONT_REGULAR(12)
            productImgView.addSubview(productNameLbl)
            
            let priceLbl = UILabel(frame: CGRectMake(productView.frame.origin.x,productNameLbl.frame.maxY,productView.frame.size.width, 15))
            priceLbl.text  = "$ 300"
            priceLbl.font = GRAPHICS.FONT_REGULAR(12)
            productImgView.addSubview(priceLbl)
            
            let commentImg = GRAPHICS.HOME_COMMENT_BUTTON_IMAGE()
            var contentSize = GRAPHICS.getImageWidthHeight(commentImg)
            var width = contentSize.width//commentImg.size.width/1.7
            var height = contentSize.height//commentImg.size.height/1.7
            var xPos = productImgView.frame.size.width  - width - 10
            let yPos = productNameLbl.frame.maxY - 10
            
            let m_commentBtn = UIButton(frame : CGRectMake(xPos,yPos,width,height))
            m_commentBtn.setBackgroundImage(commentImg, forState: .Normal)
            m_commentBtn.addTarget(self, action: #selector(IBHomeViewController.commentBtnAction(_:)), forControlEvents: .TouchUpInside)
            productImgView.addSubview(m_commentBtn)
            
            xPos = m_commentBtn.frame.origin.x - width + 10
            let commentLbl = UILabel(frame: CGRectMake(xPos,yPos-5,width,height))
            commentLbl.font = GRAPHICS.FONT_REGULAR(12)
            commentLbl.text = "29"
            if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
            {
                commentLbl.font = GRAPHICS.FONT_REGULAR(13)
            }
            
            productImgView.addSubview(commentLbl)

            
            let likeImg = GRAPHICS.HOME_FAVOURITE_BUTTON_IMAGE()
            contentSize = GRAPHICS.getImageWidthHeight(likeImg)
            width = contentSize.width//likeImg.size.width/2
            height = contentSize.width//likeImg.size.height/2
            xPos = commentLbl.frame.origin.x - width
            let m_likeBtn = UIButton(frame : CGRectMake(xPos,yPos,width,height))
            m_likeBtn.setBackgroundImage(likeImg, forState: .Normal)
            m_likeBtn.addTarget(self, action: #selector(IBHomeViewController.likeBtnAction(_:)), forControlEvents: .TouchUpInside)
            productImgView.addSubview(m_likeBtn)
            
            xPos = m_likeBtn.frame.origin.x - width + 10
            let likeLbl = UILabel(frame: CGRectMake(xPos,yPos-5,width,height))
            likeLbl.font = GRAPHICS.FONT_REGULAR(12)
            likeLbl.text = "26"
            if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
            {
                likeLbl.font = GRAPHICS.FONT_REGULAR(13)
            }
                productImgView.addSubview(likeLbl)

        }
        
        //m_homeBgView .addSubview(self.gridView?)
        m_homeScrlView.addSubview(self.gridView!)
        m_homeScrlView.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), (gridView?.bounds.maxY)!+50)
    }
    func commentBtnAction(sender : UIButton)
    {
        
    }

    func likeBtnAction(sender : UIButton)
    {
    
    }
    
    func selectfields(tapGesture : UITapGestureRecognizer)
    {
        if(tapGesture.view?.tag == 0)
        {
            let searchLocation = IBSearchProductLocationVC()
            searchLocation.locationDict  = NSMutableDictionary(dictionary: m_locationDict)
            self.navigationController?.pushViewController(searchLocation, animated: true)
        }
        else if(tapGesture.view?.tag == 1)
        {
            if m_categoryStr == ""
            {
                m_categoryStr = "All categories"
            }
            let searchLocation = IBSearchProductCategoryVC()
            searchLocation.categoryStr = m_categoryStr
            self.navigationController?.pushViewController(searchLocation, animated: true)
        }
        else
        {
            if  m_filterStr == ""
            {
                m_filterStr = "Popular"
                m_minPriceStr = ""
                m_maxPriceStr = ""
            }
            let searchLocation = IBSearchProductFilterVC()
            searchLocation.m_selectedString = m_filterStr
            searchLocation.minPriceStr = m_minPriceStr
            searchLocation.maxPriceStr = m_maxPriceStr
            self.navigationController?.pushViewController(searchLocation, animated: true)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField .resignFirstResponder()
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func dropDownBtnAction(sender : UIButton)
    {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
