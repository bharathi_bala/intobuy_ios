//
//  IBMyProductsViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 27/11/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBMyProductsViewController: IBBaseViewController {

    var m_myProductImgView:UIImageView = UIImageView()
    var m_productScrlView:UIScrollView = UIScrollView()
    var gridView:GridView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.createControlsForView()
        self.m_titleLabel.text = "My products"
        // Do any additional setup after loading the view.
    }
    func createControlsForView()
    {
        m_myProductImgView.frame = self.m_bgImageView.frame
        m_myProductImgView.image = GRAPHICS.PRODUCT_BG_IMAGE()
        m_myProductImgView.userInteractionEnabled = true
        self.m_bgImageView .addSubview(m_myProductImgView)
        
        var viewH:CGFloat = m_myProductImgView.frame.size.height/2;
        if (GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_5) {
            viewH = m_myProductImgView.frame.size.height/2.5;
        } else if (GRAPHICS.Screen_Type() == IPHONE_6_Plus) {
            viewH = m_myProductImgView.frame.size.height/2.5;
        }
        let profileTop = IBProfilePersonalDetailsView(frame: CGRectMake(m_myProductImgView.bounds.origin.x,m_myProductImgView.bounds.origin.y,GRAPHICS.Screen_Width(),viewH))
        //profileTop.btnDelegate = self
        m_myProductImgView.addSubview(profileTop);
        
        m_productScrlView.frame = CGRectMake(GRAPHICS.Screen_X(), profileTop.frame.maxY, GRAPHICS.Screen_Width(), m_myProductImgView.frame.size.height - profileTop.frame.size.height)
        m_myProductImgView.addSubview(m_productScrlView)
        
        self.createGridView(2, row: 4)
        
    }
    func createGridView(coloumn : UInt , row : UInt){
        
        let coloumnCount:UInt = coloumn
        let rowCount:UInt = row
        var gridImg = GRAPHICS .PRODUCT_GRID_IMAGE()
        let gridWid = GRAPHICS.Screen_Width() ;
        var gridHei = (gridImg?.size.height)!/1.8;
        if(coloumn == 1){
            gridImg = GRAPHICS .PRODUCT_LIST_IMAGE()
            gridHei = gridWid
        }
        
//        if (GRAPHICS .Screen_Type() == IPHONE_4 || GRAPHICS.Screen_Type() == IPHONE_5)
//        {
//            gridHei = (gridImg?.size.height)!/2
//        }

        var gridFrame = CGRect.zero
        gridFrame.size.width = gridWid
        gridFrame.size.height = gridHei
        let margins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        
        let subViews = m_productScrlView.subviews as Array<UIView>
        
        for someView in subViews
        {
            someView.removeFromSuperview()
        }
        
        
        self.gridView = GridView(frame: gridFrame, columns: coloumnCount, rows: rowCount, margins: margins, padding: 0.0) {
            column, row, contentView in
            
                
            
                let productImgView = UIImageView(frame: contentView.bounds)
                productImgView.image = gridImg
                contentView .addSubview(productImgView);
                
                let productView = UIImageView(frame: CGRectMake(productImgView.bounds.origin.x+14, productImgView.bounds.origin.y+15, productImgView.frame.size.width - 31, productImgView.frame.size.height - 60))
                productView.backgroundColor = UIColor.redColor()
                productView.image = UIImage(named: "IMG_1300.jpg")
                productImgView.addSubview(productView)
            
            //print("product image view is",productImgView.frame)
                //print("product view is",productView.frame)
            
                let productNameLbl = UILabel(frame: CGRectMake(productView.frame.origin.x,productView.frame.maxY,productView.frame.size.width, 15))
                productNameLbl.text  = "Iphone 4"
                productNameLbl.font = GRAPHICS.FONT_REGULAR(12)
                productImgView.addSubview(productNameLbl)
                
                let priceLbl = UILabel(frame: CGRectMake(productView.frame.origin.x,productNameLbl.frame.maxY,productView.frame.size.width, 15))
                priceLbl.text  = "$ 300"
                priceLbl.font = GRAPHICS.FONT_REGULAR(12)
                productImgView.addSubview(priceLbl)
        }
        m_productScrlView .addSubview(self.gridView!)
        m_productScrlView.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), (self.gridView?.frame.maxY)!)
    }

    
        
    func setToList(sender: UIButton)
    {
        self.createGridView(1, row: 8)
        //self .createGridView(1, row: 9)
    }
    func setToGrid(sender: UIButton)
    {
        self.createGridView(2, row: 4)
      //  self.createGridView(3, row: 3)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
