//
//  IBMemberShipPlanViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 09/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBMemberShipPlanViewController: IBBaseViewController,ServerAPIDelegate,PayPalPaymentDelegate {

    var m_resultArray = NSMutableArray()
    var m_scrlView:UIScrollView!
    var planId = ""
    var isFromSettings = Bool()
    var planDict = NSDictionary()
    var userView:UIView!
    
    //PaypalEnvironMent no network
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnectWithEnvironment(newEnvironment)
            }
        }
    }
    
    #if HAS_CARDIO
    var acceptCreditCards: Bool = true {
    didSet {
    payPalConfig.acceptCreditCards = acceptCreditCards
    }
    }
    #else
    var acceptCreditCards: Bool = false {
        didSet {
            payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }
    #endif
    
    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        PayPalMobile.preconnectWithEnvironment(PayPalEnvironmentSandbox)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(true)
        self.m_settingsButton.hidden = true
        self.m_bgImageView.image = GRAPHICS.LOGIN_BACKGROUND_IMAGE()
        self.m_titleLabel.text = MembershipPlan
        
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:Loading, animated:true)
        let serverApi = ServerAPI()
        serverApi.delegate = self

        
        if isFromSettings
        {
            serverApi.API_getMyPlan(getUserIdFromUserDefaults())
        }
        else{
            serverApi.API_getAllPlans()
        }
        
        m_scrlView = UIScrollView(frame: self.m_bgImageView.bounds)
        self.m_bgImageView.addSubview(m_scrlView)
        
        payPalConfig.acceptCreditCards = true
        
      //  self.createControlsForPlan()

        // Do any additional setup after loading the view.
    }
    func createControlsToSetPlans()
    {

        let titleArray:NSMutableArray = [UserName,PlanName,Points,ServiceExpiration]
        let userNameStr = getUserNameFromUserDefaults()
        let planNameStr = planDict.objectForKey("planname") as! String
        let pointsStr = planDict.objectForKey("points") as! String
        let serviceExpirationStr = planDict.objectForKey("status") as! String
        let valuesArray:NSMutableArray = [userNameStr,planNameStr,pointsStr,serviceExpirationStr]
        
        var xPos = GRAPHICS.Screen_X() + 10
        var yPos = m_scrlView.bounds.origin.y + 10
        var width = GRAPHICS.Screen_Width() - 20
        var height = GRAPHICS.Screen_Height()/2.5
        
        userView = UIView(frame: CGRectMake(xPos, yPos, width, height))
        userView.backgroundColor = UIColor.lightGrayColor()
        m_scrlView.addSubview(userView)
        
        yPos = userView.bounds.origin.y + 10
        height = 18
        for i in 0 ..< titleArray.count
        {
            xPos = 10
            width = (userView.frame.size.width/2) - 20
            let titleLbl = UILabel(frame : CGRectMake(xPos, yPos, width, height))
            titleLbl.text = String(format : "%@ :",titleArray.objectAtIndex(i) as! String)
            titleLbl.font = GRAPHICS.FONT_REGULAR(14)
            userView.addSubview(titleLbl)
            
            xPos = xPos + width
            width = (userView.frame.size.width/2) + 10
            let valueLbl = UILabel(frame : CGRectMake(xPos, yPos, width, height))
            valueLbl.text = valuesArray.objectAtIndex(i) as? String
            valueLbl.font = GRAPHICS.FONT_REGULAR(14)
            userView.addSubview(valueLbl)
            yPos = yPos + height + 10
        }
        userView.frame.size.height = yPos
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_getAllPlans()
        })

        
    }
    func createControlsForPlan()
    {
        
        var yPos:CGFloat //= m_scrlView.bounds.origin.y + 30
        if isFromSettings
        {
            yPos = userView.frame.maxY + 10
        }
        else
        {
            yPos = m_scrlView.bounds.origin.y + 30
        }
        
        
        for i in 0  ..< m_resultArray .count 
        {
            let planDict = (m_resultArray.objectAtIndex(i)as? NSDictionary)!
            var xPos = GRAPHICS.Screen_X() + 10
            var width = GRAPHICS.Screen_Width() - 20
            var height:CGFloat = 50
            let bgImg = GRAPHICS.MEMBERSHIP_PLAN_BG_IMAGE()
            let bgView = UIImageView(frame: CGRectMake(xPos, yPos, width, height))
            bgView.userInteractionEnabled = true
            bgView.image = bgImg
            m_scrlView .addSubview(bgView)
            
            xPos = bgView.bounds.origin.x + 5
            
            width = bgView.frame.size.width/3
            
            if isFromSettings
            {
                yPos = bgView.bounds.origin.y
                height = bgView.frame.size.height/2
            }
            else{
                yPos = bgView.bounds.origin.y + 5
                height = bgView.frame.size.height - 10
            }
            let expireMonLbl = UILabel(frame: CGRectMake(xPos, yPos, width, height))
            expireMonLbl.text = (planDict.objectForKey("planname")as? String)!
            expireMonLbl.font = GRAPHICS.FONT_REGULAR(14)
            expireMonLbl.textColor = UIColor.grayColor()
            //expireMonLbl.textAlignment = NSTextAlignment.Center
            bgView.addSubview(expireMonLbl)
            if isFromSettings
            {
                yPos = expireMonLbl.frame.maxY
                xPos = expireMonLbl.frame.origin.x + 5
                
                let pointsLbl = UILabel(frame: CGRectMake(xPos, yPos, width, height))
                let pointStr = String(format : "%@ %@",(planDict.objectForKey("points")as? String)! , "Points")
                pointsLbl.text = pointStr//(planDict.objectForKey("planname")as? String)!
                pointsLbl.font = GRAPHICS.FONT_REGULAR(14)
                pointsLbl.textColor = UIColor.grayColor()
                //expireMonLbl.textAlignment = NSTextAlignment.Center
                bgView.addSubview(pointsLbl)
                
            }
            
            xPos = expireMonLbl.frame.maxX + 5
            width = bgView.frame.size.width/5
            yPos = bgView.bounds.origin.y + 5
            let amountLbl = UILabel(frame: CGRectMake(xPos, yPos, width, height))
            amountLbl.text = (planDict.objectForKey("amount")as? String)!
            amountLbl.font = GRAPHICS.FONT_REGULAR(16)
            amountLbl.textColor = UIColor.darkGrayColor()
            amountLbl.textAlignment = NSTextAlignment.Center
            bgView.addSubview(amountLbl)
            
            let buyNowImg = GRAPHICS.MEMBERSHIP_PLAN_BUYNOW_BTN_IMAGE()
            width = buyNowImg.size.width/2
            height = buyNowImg.size.height/2
            xPos = bgView.frame.size.width - width - 10
            yPos = (bgView.frame.size.height - height)/2
            let buyNowBtn = UIButton(frame: CGRectMake(xPos, yPos, width, height))
            buyNowBtn.setBackgroundImage(buyNowImg, forState: UIControlState.Normal)
            buyNowBtn.addTarget(self, action: #selector(IBMemberShipPlanViewController.buyNowBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside);
            buyNowBtn.tag = i
            buyNowBtn.setTitle(BuyNow, forState: UIControlState.Normal)
            buyNowBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
            bgView .addSubview(buyNowBtn)
            
            yPos = bgView.frame.maxY + 10
        }
        m_scrlView.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), yPos - 20)
        
    }
    func buyNowBtnAction(Sender : UIButton)
    {
        let planDict = (m_resultArray.objectAtIndex(Sender.tag)as? NSDictionary)!
        let amountStr = (planDict.objectForKey("amount")as? String)!
        planId = String(format: "%d",Sender.tag + 1)
        paymentProcessForBuyNowButtonClickEvent(amountStr)
        
    }
    
    func paymentProcessForBuyNowButtonClickEvent(amountStr : String)
    {
        // Remove our last completed payment, just for demo purposes.
        resultText = ""
        
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        
        let item = PayPalItem(name: "Purchase Membership", withQuantity: 1, withPrice: NSDecimalNumber(string: amountStr), withCurrency: "USD", withSku: "Hip-0037")
        
//        var itemArray = NSMutableArray()
//        for i in 0 ..< productDetailsArray.count
//        {
//            let productDetailsDict = productDetailsArray.objectAtIndex(i) as! NSDictionary
//            let productNameStr = productDetailsDict.objectForKey("productname") as! String
//            let productQuantity = Int(productDetailsDict.objectForKey("quantity") as! String)
//            let item = PayPalItem(name: productNameStr, withQuantity: UInt(productQuantity!), withPrice: NSDecimalNumber(string: (productDetailsDict.objectForKey("price") as! String)), withCurrency: "USD", withSku: "Hip-0037")
//            itemArray.addObject(item)
//            
//        }
        
        //        let productNameStr = productDetailsDict.objectForKey("productname") as! String
        //        let productQuantity = Int(productDetailsDict.objectForKey("quantity") as! String)
        //        let productPrice = Int(Float(productDetailsDict.objectForKey("price") as! String)!) * productQuantity!
        
        
        //        let item2 = PayPalItem(name: "MacBook Pro", withQuantity: 1, withPrice: NSDecimalNumber(string: "240.00"), withCurrency: "USD", withSku: "Hip-00066")
        //        let item3 = PayPalItem(name: "SamsungLCD", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
        
        let items = [item]//itemArray as [AnyObject]
        let subtotal = PayPalItem.totalPriceForItems(items/*[item1]*/)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.decimalNumberByAdding(shipping).decimalNumberByAdding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "IntoBuy Membership", intent: .Sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            presentViewController(paymentViewController, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            //print("Payment not processalbe: \(payment)")
        }
    }
    // PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(paymentViewController: PayPalPaymentViewController!) {
        //print("PayPal Payment Cancelled")
        self.m_backButton.userInteractionEnabled = true
        self.m_bigBackButton.userInteractionEnabled = true
        resultText = ""
        
        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func payPalPaymentViewController(paymentViewController: PayPalPaymentViewController!, didCompletePayment completedPayment: PayPalPayment!) {
        //print("PayPal Payment Success !")
        paymentViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            // send completed confirmaion to your server
            //print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            self.resultText = completedPayment!.description
            let confirmDict = completedPayment.confirmation as NSDictionary
            let responseDict = confirmDict.objectForKey("response") as! NSDictionary
            let transactionId = responseDict.objectForKey("id") as! String
            
            SwiftLoader.show(animated: true)
            SwiftLoader.show(title:"Loading...", animated:true)

            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                let serverApi = ServerAPI()
                serverApi.delegate = self
                let userId = getUserIdFromUserDefaults()
                serverApi.API_purchaseMembership(userId, planId: self.planId,transactionId:transactionId)

            })
            //showAlertViewWithMessage("Your transaction has sucess")
        })
    }

    //MARK:- API delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    func API_CALLBACK_getAllPlans(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = (resultDict.objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
            let resultAry = resultDict.objectForKey("result") as! NSArray
            if resultAry.count > 0
            {
                for i in 0 ..< resultAry.count
                {
                    let dict = resultAry.objectAtIndex(i) as! NSDictionary
                    m_resultArray.addObject(dict)
                }
          
            }
        }
        self.createControlsForPlan()
    }
    func API_CALLBACK_purchaseMembership(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = (resultDict.objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
            let userDetailUD = NSUserDefaults()
            let userIdStr = getUserIdFromUserDefaults();
            userDetailUD.setBool(true, forKey: "isLogged")
            showAlert((resultDict.objectForKey("result")as? String)!)
        }
        else{
            //showAlert(Noresults_found_text)
            showAlertViewWithMessage(Noresults_found_text)
        }
    }
    func API_CALLBACK_getMyPlan(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = (resultDict.objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
            if let resultArray = resultDict.objectForKey("result") as? NSArray
            {
                planDict = resultArray.objectAtIndex(0) as! NSDictionary
                createControlsToSetPlans()
            }
            else if let resultArray = resultDict.objectForKey("result") as? String
            {
                showAlertViewWithMessage(resultArray)
            }
        }
        else
        {
            showAlertViewWithMessage(Noresults_found_text)
        }
    }
    func showAlert(msg:String!){
        SwiftLoader.hide()
        let alert = UIAlertView(title: "IntoBuy", message:msg, delegate: self, cancelButtonTitle:"Ok")
        alert .show()
        
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int)
    {
            let homeVc = IBHomePageVcViewController()
            self.navigationController!.pushViewController(homeVc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
