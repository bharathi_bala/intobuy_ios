//
//  IBActivityViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 18/02/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBActivityViewController: IBBaseViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ServerAPIDelegate {
    
    var m_youTblView:UITableView!
    var m_youArray = NSMutableArray()
    var m_FollowingArray =  NSMutableArray()
    var searchView:UIView!
    var searchTextField:UITextField!
    var btnIndex = 0
    var keyBoardTool:UIToolbar!
    let youButton = UIButton()
    var followingButton = UIButton()
    let normalHeight = CGFloat(60)
    let dynamicCellheight = CGFloat(110)
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
        if searchTextField != nil
        {
          searchTextField.text = ""
        }
     }
    
    override func viewDidLoad() {
        bottomVal = 3
        super.viewDidLoad()
        self.hideSettingsBtn(true)
        self.m_bottomView.hidden = false
        createSearchControls()
        createControls()
        self.m_titleLabel.text = "Activity"
        
        createServiceForGetMyNews()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self.createServiceForGetNews()
        })
        
        
        //        let youDict = NSMutableDictionary()
        //        youDict.setObject("Colonlover added a Post", forKey: "userName")
        //        let youEntity = IBActivityYouEntity(dict: youDict)
        //        m_youArray.addObject(youEntity)
        //
        //        let followingDict = NSMutableDictionary()
        //        followingDict.setObject("Anaballe likes titiloShop photos", forKey: "userName")
        //        let followingEntity = IBActivityFollowingEntity(dict: followingDict)
        //        m_FollowingArray.addObject(followingEntity)
    }
    
    func createServiceForGetNews()
    {
        let userIdStr = getUserIdFromUserDefaults()
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getNews(userIdStr)
    }
    func createServiceForGetMyNews()
    {
        let userIdStr = getUserIdFromUserDefaults()
        
        SwiftLoader.show(title: Loading, animated: true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getMyNews(userIdStr)
    }
    
    func createSearchControls()
    {
        
        let textImg = getWidthandHeightofImage(GRAPHICS.PRODUCT_SEARCH_TEXT_IMAGE())
        
        searchView = UIView(frame: CGRectMake(self.m_bgImageView.bounds.origin.x,self.m_bgImageView.bounds.origin.y,self.m_bgImageView.frame.size.width,textImg.height + 20))//5 is the searchimgviw height
        searchView.backgroundColor = lightGrayColor
        self.m_bgImageView .addSubview(searchView)
        
        
        let searchImage = getWidthandHeightofImage(GRAPHICS.PRODUCT_SEARCH_IMAGE())
        
        searchTextField = UITextField(frame:CGRectMake((searchView.frame.size.width - textImg.width - searchImage.width - 5)/2, (searchView.frame.size.height - textImg.height)/2, textImg.width, textImg.height))
        searchTextField.background = GRAPHICS.PRODUCT_SEARCH_TEXT_IMAGE()
        searchTextField.delegate = self
        // searchTextField.inputAccessoryView = createKeyboardToolBar()
        searchTextField.placeholder = SearchFriends
        searchTextField.font = GRAPHICS.FONT_REGULAR(12)
        searchTextField.addTarget(self, action: #selector(UITextViewDelegate.textViewDidChange(_:)), forControlEvents:UIControlEvents.ValueChanged)
        searchTextField.inputAccessoryView = createKeyboardToolBar()
        searchTextField.textAlignment = NSTextAlignment.Center
        searchTextField.returnKeyType = .Done
        searchView.addSubview(searchTextField)
        
        let searchImgView = UIImageView(frame: CGRectMake(searchTextField.frame.maxX + 5, (searchView.frame.size.height - searchImage.height)/2, searchImage.width, searchImage.height))
        searchImgView.image = GRAPHICS.PRODUCT_SEARCH_IMAGE()
        searchView.addSubview(searchImgView)
        //createSegmentButtons()
    }
    
    func createControls()
    {
        let headerBtnNormal = GRAPHICS.ACTIVITY_BUTTON_NORMAL_IMAGE()
        let headerBtnSelected = GRAPHICS.ACTIVITY_BUTTON_SELECTED_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(GRAPHICS.ACTIVITY_BUTTON_NORMAL_IMAGE())
        var xPos = GRAPHICS.Screen_X()
        let yPos = searchView.frame.maxY
        let width = GRAPHICS.Screen_Width()/2//headerBtnNormal.size.width/2
        let height = contentSize.height//headerBtnNormal.size.height/2
        
        youButton.frame = CGRectMake(xPos,yPos,width,height)
        youButton.setBackgroundImage(headerBtnNormal, forState: .Normal)
        youButton.setBackgroundImage(headerBtnSelected, forState: .Selected)
        youButton.setTitle("You", forState: .Normal)
        youButton.titleLabel?.font = GRAPHICS.FONT_BOLD(13)
        youButton.addTarget(self, action: #selector(IBActivityViewController.youBtnAction(_:)), forControlEvents: .TouchUpInside)
        youButton.selected = true
        self.m_bgImageView.addSubview(youButton)
        
        xPos = youButton.frame.maxX
        followingButton.frame = CGRectMake(xPos,yPos,width,height)
        followingButton.setBackgroundImage(headerBtnNormal, forState: .Normal)
        followingButton.setBackgroundImage(headerBtnSelected, forState: .Selected)
        followingButton.setTitle("Following", forState: .Normal)
        followingButton.titleLabel?.font = GRAPHICS.FONT_BOLD(13)
        followingButton.addTarget(self, action: #selector(IBActivityViewController.followingBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_bgImageView.addSubview(followingButton)
        addDividerImage(followingButton)
        
        //self.createYouTableView()
        
        m_youTblView = UITableView(frame: CGRectMake(GRAPHICS.Screen_X(),youButton.frame.maxY,GRAPHICS.Screen_Width(), self.m_bgImageView.frame.size.height - searchView.frame.size.height - followingButton.frame.size.height))
        m_youTblView.delegate = self
        m_youTblView.dataSource = self
        m_youTblView.separatorStyle = .None
        self.m_bgImageView.addSubview(m_youTblView)
        
    }
    func youBtnAction(sender : UIButton)
    {
        btnIndex = 0
        youButton.selected = true
        followingButton.selected = false
        //        if sender.selected
        //        {
        //            sender.selected = false
        //        }
        //        else{
        //            sender.selected = true
        //        }
        m_youTblView.reloadData()
        
        //        m_followingTblView.hidden = true
        //        m_youTblView.hidden = false
    }
    func followingBtnAction(sender : UIButton)
    {
        btnIndex = 1
        youButton.selected = false
        followingButton.selected = true
        m_youTblView.reloadData()
    }
    //MARK:- tableView delegates
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        var entity : IBActivityFollowingEntity!
        
        if btnIndex == 0 || btnIndex == 1
        {
            if btnIndex == 0
            {
                entity = m_youArray.objectAtIndex(indexPath.row) as!IBActivityFollowingEntity
            }
            else
            {
                entity = m_FollowingArray.objectAtIndex(indexPath.row) as! IBActivityFollowingEntity
            }
            if entity.images.count > 1
            {
                return dynamicCellheight
            }
            else
            {
                return normalHeight;
            }
            
        }
        else
        {
            return 60;
            
        }
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if( btnIndex == 0)
        {
            return m_youArray.count;
        }
        else if( btnIndex == 1){
            return m_FollowingArray.count;
        }
        else
        {
            return 2
        }
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cellIdentifier = "cell Identifier \(indexPath.row)"
        
        if(btnIndex == 0 || btnIndex == 1)
        {
            var cell:IBActivityYouCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBActivityYouCell
            if (cell == nil)
            {
                cell = IBActivityYouCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            }
            
            var entity : IBActivityFollowingEntity!
            
            if btnIndex == 0
            {
                entity = m_youArray.objectAtIndex(indexPath.row) as! IBActivityFollowingEntity
            }
            else
            {
                entity = m_FollowingArray.objectAtIndex(indexPath.row) as? IBActivityFollowingEntity
            }
            cell?.setValuesForControls((entity.images as NSArray), dateStr: (entity.addedDate as String), delegate: self, indexpath: indexPath, entity: entity,btnindex1:btnIndex)
            
            cell?.selectionStyle = .None
                    
//             if indexPath.row % 2 == 0
//             {
//             cell?.backgroundColor = UIColor.greenColor()
//             }
//             else
//             {
//             cell?.backgroundColor = UIColor.purpleColor()
//             }
           
            return cell!
        }
            
        else
        {
            var cell:IBActivitySearchCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBActivitySearchCell
            if (cell == nil) {
                cell = IBActivitySearchCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            }
            cell?.friendBtn.addTarget(self, action: #selector(IBActivityViewController.friendButtonAction(_:)), forControlEvents: .TouchUpInside)
            return cell!
        }
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func ProfileImgGesture(tapGesture: UITapGestureRecognizer)
    {
        var entity : IBActivityFollowingEntity!
        
        if btnIndex == 0
        {
            entity = m_youArray.objectAtIndex((tapGesture.view?.tag)!) as! IBActivityFollowingEntity
        }
        else
        {
            entity = m_FollowingArray.objectAtIndex((tapGesture.view?.tag)!) as? IBActivityFollowingEntity
        }
        
        let myPageVC = IBMyPageViewController()
        myPageVC.m_isFromCameraBtn = false
        myPageVC.otherUserId = (entity.userId as String)
        self.navigationController?.pushViewController(myPageVC, animated: true)
        
    }
    
    func friendButtonAction(sender : UIButton) -> () {
        if sender.selected == false
        {
            sender.selected = true
        }
    }
    //MARK:- textfield delegates
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
      
        return true
    }
    func textViewDidChange(textView: UITextView) {
        //msgstring = msgTextView.text!;
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        doneAction()
        
        return true
    }
    
    func createKeyboardToolBar() -> UIToolbar
    {
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        keyBoardTool = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        keyBoardTool!.barStyle = UIBarStyle.BlackOpaque
        keyBoardTool!.translucent = true
        keyBoardTool!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        keyBoardTool!.sizeToFit()
        
        
        // let doneButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("nextAction"))
        
        let cancel_Btn = UIButton ()
        cancel_Btn.frame = CGRectMake(10, 0, 70, 50)
        cancel_Btn.backgroundColor =  UIColor.clearColor();
        cancel_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        cancel_Btn.setTitle("Cancel", forState: UIControlState.Normal)
        cancel_Btn.addTarget(self, action: #selector(IBActivityViewController.cancelAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(cancel_Btn)
        
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBActivityViewController.doneAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(done_Btn)
        
        keyBoardTool!.userInteractionEnabled = true
        return keyBoardTool
        
    }
    func cancelAction()
    {
        if searchTextField.isFirstResponder()
        {
            searchTextField.text = ""
            searchTextField.resignFirstResponder()
            btnIndex = 0
            m_youTblView.reloadData()
        }
    }
    func doneAction()
    {
        if searchTextField.isFirstResponder()
        {
            searchTextField.resignFirstResponder()
        }
        if searchTextField.text == ""
        {
          showAlertViewWithMessage(searchFriends_validation_text)
        }
        else
        {
            SwiftLoader.show(title: Loading, animated: true)
            let serverApi = ServerAPI()
            serverApi.API_getSearchFriends(getUserIdFromUserDefaults(), delegate: self, usertype: getUserTypeString(), country: "")
        }
        
    }
    
    func API_CALLBACK_SearchFriends(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        
        let searchArry = NSMutableArray()
        
        if let strErrorCode = resultDict.objectForKey("error_code") as? String
        {
            if strErrorCode == "1"
            {
                if let resultArry = resultDict.objectForKey("result") as? NSArray
                {
                    if resultArry.count > 0
                    {
                        for i in 0 ..< resultArry.count
                        {
                            let searchEnity = IBActivityFollowingEntity(dict:resultArry[i] as! NSDictionary)
                            searchArry.addObject(searchEnity)
                            
                        }
                    }
                               
                }
            }
            
        }
        
        if searchArry.count > 0
        {
            
            let searchPredicate = NSPredicate(format: "strUserName contains [cd] %@", searchTextField.text!,searchTextField.text!)
            
            // Filter the data array and get only those countries that match the search text.
            
            let array = searchArry.filteredArrayUsingPredicate(searchPredicate)
           // //print("filtered arry %@",array)
            searchArry .removeAllObjects()
            searchArry .addObjectsFromArray(array)
            
            
            let searchfrinevc = IBSearchFreindsViewController()
            searchfrinevc.m_serachArry = searchArry
            self.navigationController?.pushViewController(searchfrinevc, animated: true)
            searchTextField.text! = ""
        }
        else
        {
          showAlertViewWithMessage(no_search_results_found)
        }
        
        
    }
    
    func addDividerImage(view : UIView)
    {
        let dividerImg = GRAPHICS.ACTIVITY_DIVIDER_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(dividerImg)
        let width = contentSize.width
        let height = contentSize.height
        let dividerImgView = UIImageView(frame : CGRectMake(view.bounds.origin.x, (view.frame.size.height - height)/2, width, height))
        dividerImgView.image = dividerImg
        view.addSubview(dividerImgView)
    }
    
    func creationOfAttributeLabelWithTitleStr(titleStr:String) -> NSMutableAttributedString
    {
        let str : String = titleStr
        
        let strArray:NSArray = str.componentsSeparatedByString(" ")
        let firstStr = strArray.objectAtIndex(0)
        
        let attributedStr : NSMutableAttributedString = NSMutableAttributedString(string: str)
        attributedStr.addAttribute(NSForegroundColorAttributeName, value: headerColor, range: NSMakeRange(0,firstStr.length))
        
        
        return attributedStr
        
    }
    
    func creationOfAttributeTextForFollowing(titleStr:String) -> NSMutableAttributedString
    {
        let str : String = titleStr
        
        let strArray:NSArray = str.componentsSeparatedByString(" ")
        let firstStr = strArray.objectAtIndex(0)
        let lastNameStr = strArray.objectAtIndex(2)
        
        let attributedStr : NSMutableAttributedString = NSMutableAttributedString(string: str)
        attributedStr.addAttribute(NSForegroundColorAttributeName, value: headerColor, range: NSMakeRange(0,firstStr.length))
        attributedStr.addAttribute(NSForegroundColorAttributeName, value: headerColor, range: NSMakeRange(titleStr.characters.count - 7 - lastNameStr.length,lastNameStr.length))
        
        
        return attributedStr
        
    }
    
    //MARK:- server API delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    func API_CALLBACK_getNews(resultDict: NSDictionary)
    {
        //print("get following Arry api call %@",resultDict)
        SwiftLoader.hide()
        if let errorCode = resultDict.objectForKey("error_code") as? String
        {
            if errorCode == "1"
            {
                m_FollowingArray.removeAllObjects()
                let resultArray = resultDict.objectForKey("result") as! NSArray
                if resultArray.count > 0{
                    for i in 0 ..< resultArray.count
                    {
                        let followingDict = resultArray.objectAtIndex(i) as! NSDictionary
                        let followingEntity = IBActivityFollowingEntity(dict: NSDictionary(dictionary: followingDict))
                        m_FollowingArray.addObject(followingEntity)
                    }
                    ////print(m_FollowingArray.count)
                    
                    m_youTblView.reloadData()
                }
                else
                {
                    showAlertViewWithMessage(NoResultsFound)
                }
            }
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    func API_CALLBACK_getMyNews(resultDict: NSDictionary)
    {
        //print("get mynews Arry api call %@",resultDict)
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"
        {
            let resultArray = resultDict.objectForKey("result") as! NSArray
            m_youArray.removeAllObjects()
            if resultArray.count > 0{
                for i in 0 ..< resultArray.count
                {
                    let youDict = resultArray.objectAtIndex(i) as! NSDictionary
                    let youEntity = IBActivityFollowingEntity(dict: NSDictionary(dictionary: youDict))
                    m_youArray.addObject(youEntity)
                }
                ////print(m_youArray.count)
                
                m_youTblView.reloadData()
            }
            else
            {
                showAlertViewWithMessage(NoResultsFound)
            }
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    
    // func 
    
}
