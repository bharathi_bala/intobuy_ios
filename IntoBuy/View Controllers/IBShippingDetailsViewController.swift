//
//  IBShippingDetails.swift
//  IntoBuy
//
//  Created by Manojkumar on 27/01/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBShippingDetailsViewController: IBBaseViewController,UITextFieldDelegate,CountryProtocol,ServerAPIDelegate {
    
    var m_shippingScrollView = UIScrollView()
    var m_fullNameTF:UITextField!
    var m_address1TF:UITextField!
    var m_address2TF:UITextField!
    var m_cityTF:UITextField!
    var m_stateTF:UITextField!
    var m_zipTF:UITextField!
    var m_countryTF:UITextField!
    var m_phnoTF:UITextField!
    var keyBoardTool:UIToolbar!
    var m_countryArray = NSMutableArray()
    var countryIdStr = String()
    var isFromProducts = Bool()
    
    var productIdStr = String()
    var sellerIdStr = String()
    var shippingdict = NSMutableDictionary()
    
    
    override func viewDidLoad() {
        bottomVal = 5
        toShowActivityBtn = true
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.toShowActivityBtn = true
        self.m_settingsButton.hidden = true
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_titleLabel.text = Settings_Title
        
        let userIdStr = getUserIdFromUserDefaults()
        
        m_shippingScrollView.frame = self.m_bgImageView.bounds
        self.m_bgImageView .addSubview(m_shippingScrollView)
        
        self.createControlsForShipping()
        
        SwiftLoader.show(title: "Loading...", animated: true)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getShippingAddress(userIdStr)

    }
    
    // view for controls
    func createControlsForShipping()
    {
        
        var xPos = GRAPHICS.Screen_X() + 15
        var yPos = m_shippingScrollView.bounds.origin.y + 30
        var width = GRAPHICS.Screen_Width() - 30
        var height:CGFloat = 15
        let lblPadding:CGFloat = 5
        let textPadding:CGFloat = 10
        let textBoxImg = GRAPHICS.SETTINGS_BIG_TEXTBOX_IMAGE()
        
        let editTitleLbl = UILabel(frame:CGRectMake(xPos,yPos,width,height))
        editTitleLbl.text = "Edit shipping details"
        editTitleLbl.font = GRAPHICS.FONT_BOLD(14)
        m_shippingScrollView .addSubview(editTitleLbl)
        
        yPos = editTitleLbl.frame.maxY + lblPadding * 2
        height = 15
        let fullNameLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        fullNameLbl.text = String(format: "%@:",fullname)
        fullNameLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_shippingScrollView .addSubview(fullNameLbl)
        
        yPos = fullNameLbl.frame.maxY + lblPadding
        height = 20
        m_fullNameTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_fullNameTF.delegate = self
        m_fullNameTF.background = textBoxImg
        m_fullNameTF.returnKeyType = UIReturnKeyType.Next
        m_fullNameTF.inputAccessoryView = self.createKeyboardToolBar()
        m_fullNameTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_fullNameTF)
        m_shippingScrollView .addSubview(m_fullNameTF)
        
        yPos = m_fullNameTF.frame.maxY + textPadding
        height = 15
        let address1Lbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        address1Lbl.text = String(format: "%@:",addressLine1)
        address1Lbl.font = GRAPHICS.FONT_REGULAR(12)
        m_shippingScrollView .addSubview(address1Lbl)
        
        yPos = address1Lbl.frame.maxY + lblPadding
        height = 20
        m_address1TF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_address1TF.delegate = self
        m_address1TF.returnKeyType = UIReturnKeyType.Next
        m_address1TF.background = textBoxImg
        m_address1TF.inputAccessoryView = self.createKeyboardToolBar()
        m_address1TF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_address1TF)
        m_shippingScrollView .addSubview(m_address1TF)
        
        yPos = m_address1TF.frame.maxY + textPadding
        height = 15
        let address2Lbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        address2Lbl.text = String(format: "%@:",addressLine2)
        address2Lbl.font = GRAPHICS.FONT_REGULAR(12)
        m_shippingScrollView .addSubview(address2Lbl)
        
        yPos = address2Lbl.frame.maxY + lblPadding
        height = 20
        m_address2TF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_address2TF.delegate = self
        m_address2TF.returnKeyType = UIReturnKeyType.Next
        m_address2TF.keyboardType = UIKeyboardType.EmailAddress
        m_address2TF.background = textBoxImg
        m_address2TF.inputAccessoryView = self.createKeyboardToolBar()
        m_address2TF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_address2TF)
        m_shippingScrollView .addSubview(m_address2TF)
        
        // city field
        yPos = m_address2TF.frame.maxY + textPadding
        height = 15
        let cityLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        cityLbl.text = String(format: "%@:",city)
        cityLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_shippingScrollView .addSubview(cityLbl)
        
        yPos = cityLbl.frame.maxY + lblPadding
        height = 20
        m_cityTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_cityTF.delegate = self
        m_cityTF.returnKeyType = UIReturnKeyType.Next
        m_cityTF.background = textBoxImg
        m_cityTF.inputAccessoryView = self.createKeyboardToolBar()
        m_cityTF.keyboardType = UIKeyboardType.EmailAddress
        m_cityTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_cityTF)
        m_shippingScrollView .addSubview(m_cityTF)
        
        //state field
        yPos = m_cityTF.frame.maxY + textPadding
        height = 15
        let stateLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        stateLbl.text = String(format: "%@/%@/%@:",StateText,province,Region)
        stateLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_shippingScrollView .addSubview(stateLbl)
        
        yPos = stateLbl.frame.maxY + lblPadding
        height = 20
        m_stateTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_stateTF.delegate = self
        m_stateTF.returnKeyType = UIReturnKeyType.Next
        m_stateTF.background = textBoxImg
        m_stateTF.inputAccessoryView = self.createKeyboardToolBar()
        m_stateTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_stateTF)
        m_shippingScrollView .addSubview(m_stateTF)
        
        // zip field
        yPos = m_stateTF.frame.maxY + textPadding
        height = 15
        let zipLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        zipLbl.text = String(format: "%@/%@:",Zip,postalCode)
        zipLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_shippingScrollView .addSubview(zipLbl)
        
        yPos = zipLbl.frame.maxY + lblPadding
        height = 20
        m_zipTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_zipTF.delegate = self
        m_zipTF.returnKeyType = UIReturnKeyType.Next
        m_zipTF.background = textBoxImg
        m_zipTF.inputAccessoryView = self.createKeyboardToolBar()
        setLeftViewToTheTextField(m_zipTF)
        m_zipTF.font = GRAPHICS.FONT_REGULAR(14)
        m_shippingScrollView .addSubview(m_zipTF)
        
        // Country field
        yPos = m_zipTF.frame.maxY + textPadding
        height = 15
        let countryLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        countryLbl.text = String(format: "%@:",Country)
        countryLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_shippingScrollView .addSubview(countryLbl)
        
        yPos = countryLbl.frame.maxY + lblPadding
        height = 20
        m_countryTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_countryTF.delegate = self
        m_countryTF.returnKeyType = UIReturnKeyType.Next
        m_countryTF.background = textBoxImg
        m_countryTF.font = GRAPHICS.FONT_REGULAR(14)
        setLeftViewToTheTextField(m_countryTF)
        m_shippingScrollView .addSubview(m_countryTF)
        
        //phone number field
        yPos = m_countryTF.frame.maxY + textPadding
        height = 15
        let phnoLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        phnoLbl.text = String(format: "%@:",phoneNumber)
        phnoLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_shippingScrollView .addSubview(phnoLbl)
        
        yPos = phnoLbl.frame.maxY + lblPadding
        height = 20
        m_phnoTF = UITextField(frame: CGRectMake(xPos,yPos,width,height))
        m_phnoTF.returnKeyType = UIReturnKeyType.Done
        m_phnoTF.delegate = self
        m_phnoTF.background = textBoxImg
        m_phnoTF.font = GRAPHICS.FONT_REGULAR(14)
        m_phnoTF.keyboardType = .NumberPad
        m_phnoTF.inputAccessoryView = self.createKeyboardToolBar()
        setLeftViewToTheTextField(m_phnoTF)
        m_shippingScrollView .addSubview(m_phnoTF)
        
        
        let saveImg = GRAPHICS.SETTINGS_BILLING_BTN_IMAGE()
        width = (saveImg.size.width)/2
        height = (saveImg.size.height)/2
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            //height = 30
            width = (saveImg.size.width)/1.8
            height = (saveImg.size.height)/1.8
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (saveImg.size.width)/1.5
            height = (saveImg.size.height)/1.5
        }
        
        xPos = (m_shippingScrollView.frame.size.width - width)/2
        yPos = m_phnoTF.frame.maxY + 50
        let saveBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        saveBtn.setBackgroundImage(saveImg, forState: UIControlState.Normal)
        saveBtn.setTitle("Save", forState: UIControlState.Normal)
        saveBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        saveBtn .addTarget(self, action: #selector(IBShippingDetailsViewController.saveBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_shippingScrollView.addSubview(saveBtn)

        m_shippingScrollView.contentSize = CGSizeMake(m_shippingScrollView.frame.size.width , saveBtn.frame.maxY + 40);
    }
    
    func saveBtnAction()
    {
        if self.validation()
        {
            shippingdict.setValue(m_address1TF.text, forKey: "address1")
            shippingdict.setValue(m_address2TF.text, forKey: "address2")
            shippingdict.setValue(m_cityTF.text, forKey: "city")
            shippingdict.setValue(m_stateTF.text, forKey: "state")
            shippingdict.setValue(m_countryTF.text, forKey: "country")
            shippingdict.setValue(countryIdStr, forKey: "countryId")
            shippingdict.setValue(m_zipTF.text, forKey: "zip")
            saveShippingDetailsToUserDefaults(NSDictionary(dictionary: shippingdict))
            
            SwiftLoader.show(title: Loading, animated: true)
            let userIdStr = getUserIdFromUserDefaults()
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_editShippingAddress(userIdStr, country: countryIdStr, name: m_fullNameTF.text!, address1: m_address1TF.text!, address2: m_address2TF.text!, state: m_stateTF.text!, contact: m_phnoTF.text!, zip: m_zipTF.text!, remark: "b", city: m_cityTF.text!)
        }
        else{
            
        }
    }
    
    // method for tool bar
    func createKeyboardToolBar() -> UIToolbar
    {
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        keyBoardTool = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        keyBoardTool!.barStyle = UIBarStyle.BlackOpaque
        keyBoardTool!.translucent = true
        keyBoardTool!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        keyBoardTool!.sizeToFit()
        
        
        let previousImage = GRAPHICS.PICKER_LEFT_ARROW_BTN()
        let nextImage = GRAPHICS.PICKER_RIGHT_ARROW_BTN()
        
        let prev_Btn = UIButton ()
        prev_Btn.frame = CGRectMake(10, 8, 40, 30)
        prev_Btn.backgroundColor =  UIColor.clearColor();
        prev_Btn.setBackgroundImage(previousImage, forState: .Normal)
        prev_Btn.addTarget(self, action: #selector(IBShippingDetailsViewController.previousAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(prev_Btn)
        
        let next_Btn = UIButton ()
        next_Btn.frame = CGRectMake(50, 8, 40, 30)
        next_Btn.backgroundColor =  UIColor.clearColor();
        next_Btn.setBackgroundImage(nextImage, forState: .Normal)
        next_Btn.addTarget(self, action: #selector(IBShippingDetailsViewController.nextAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(next_Btn)
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBShippingDetailsViewController.doneBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(done_Btn)
        
        
        keyBoardTool!.userInteractionEnabled = true
        return keyBoardTool
        
    }
    // MARK: - textfield delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == m_fullNameTF
        {
            m_fullNameTF.resignFirstResponder()
            m_address1TF.becomeFirstResponder()
        }
        else if textField == m_address1TF
        {
            m_address1TF.resignFirstResponder()
            m_address2TF.becomeFirstResponder()
        }
        else if textField == m_address2TF
        {
            m_address2TF.resignFirstResponder()
            m_cityTF.becomeFirstResponder()
        }
        else if textField == m_cityTF
        {
            m_cityTF.resignFirstResponder()
            m_stateTF.becomeFirstResponder()
        }
        else if textField == m_stateTF
        {
            m_stateTF.resignFirstResponder()
            m_zipTF.becomeFirstResponder()
        }
        else if textField == m_zipTF
        {
            m_zipTF.resignFirstResponder()
            //m_countryTF.becomeFirstResponder()
        }
        else if textField == m_phnoTF{
            m_phnoTF.resignFirstResponder()        }
        
        return true
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if(textField == m_countryTF)
        {
            if(m_countryArray.count > 0){
                self.navigateToCountryScreen()
            }
            else{
                self.callServiceForCountry()
            }
            
            return false
        }
        if(textField.frame.origin.y >  self.m_bgImageView.frame.size.height - 200){
            var contentOffset:CGFloat!
            if(GRAPHICS.Screen_Type() == IPHONE_4){
                contentOffset = 120.0
            }
            else if(GRAPHICS.Screen_Type() == IPHONE_5){
                contentOffset = 150.0;
            }
            else
            {
                contentOffset = 160.0;
            }
            m_shippingScrollView.setContentOffset(CGPointMake(0, (m_shippingScrollView.frame.origin.y + contentOffset)), animated: true)
        }
        return true
    }
    
    // MARK: - country service api
    func callServiceForCountry()
    {
        self.view .endEditing(true)
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getActiveCountry("36")
    }


    func navigateToCountryScreen()
    {
        let countryVC = IBCountryViewController()
        countryVC.mDelegate = self
        countryVC.isFromRegistration = true
        countryVC.m_countryArray = m_countryArray
        self.navigationController!.pushViewController(countryVC, animated: true)
        
    }

    
    func nextAction(){
        
        if m_fullNameTF.isFirstResponder() == true{
            
            m_fullNameTF.resignFirstResponder()
            m_address1TF.becomeFirstResponder()
        }
        else if m_address1TF.isFirstResponder() == true{
            
            m_address1TF.resignFirstResponder()
            m_address2TF.becomeFirstResponder()
        }
        else if m_address2TF.isFirstResponder() == true{
            
            m_address2TF.resignFirstResponder()
            m_cityTF.becomeFirstResponder()
        }
        else if m_cityTF.isFirstResponder() == true{
            
            m_cityTF.resignFirstResponder()
            m_stateTF.becomeFirstResponder()
        }
        else if m_stateTF.isFirstResponder() == true{
            m_stateTF.resignFirstResponder()
            m_zipTF.becomeFirstResponder()
        }
        else if m_zipTF.isFirstResponder() == true{
            m_zipTF.resignFirstResponder()
            m_countryTF.becomeFirstResponder()
        }
        else if m_countryTF.isFirstResponder() == true{
            m_countryTF.resignFirstResponder()
            m_phnoTF.becomeFirstResponder()
        }
        else
        {
            m_phnoTF.resignFirstResponder()
        }
    }
    
    func previousAction(){
        
        
        if m_phnoTF.isFirstResponder() == true{
            
            m_phnoTF.resignFirstResponder()
            m_countryTF.becomeFirstResponder()
            //self.CreateToolBar()
            //m_lastNameTF.becomeFirstResponder()
        }
        else if m_countryTF.isFirstResponder() == true{
            
            m_countryTF.resignFirstResponder()
            m_zipTF.becomeFirstResponder()
        }
        else if m_zipTF.isFirstResponder() == true{
            
            m_zipTF.resignFirstResponder()
            m_stateTF.becomeFirstResponder()
        }
        else if m_stateTF.isFirstResponder() == true{
            
            m_stateTF.resignFirstResponder()
            m_cityTF.becomeFirstResponder()
        }
        else if m_cityTF.isFirstResponder() == true{
            m_cityTF.resignFirstResponder()
            m_address2TF.becomeFirstResponder()
        }
        else if m_address2TF.isFirstResponder() == true{
            m_address2TF.resignFirstResponder()
            m_address1TF.becomeFirstResponder()
        }
        else if m_address1TF.isFirstResponder() == true{
            m_address1TF.resignFirstResponder()
            m_fullNameTF.becomeFirstResponder()
        }
        else{
            m_fullNameTF.resignFirstResponder()
        }
        //        else if m_genderTF.isFirstResponder() == true{
        //            m_genderTF.resignFirstResponder()
        //        }
        
    }
    func doneBtnAction()
    {
        self.view .endEditing(true)
    }

    func sendArrayToPreviousVC(myArray: NSArray) {
        m_countryTF.text = myArray.objectAtIndex(0) as? String
        countryIdStr = (myArray.objectAtIndex(1) as? String)!
    }
    
    // set padding for text field
    func setLeftViewToTheTextField(textField: UITextField)
    {
        let leftPlaceHolderView = UIView(frame: CGRectMake(0, 0, 10,textField.frame.size.height))
        leftPlaceHolderView.backgroundColor = UIColor.clearColor()
        textField.leftView = leftPlaceHolderView
        textField.leftViewMode = UITextFieldViewMode.Always
    }
        func validation()->Bool{
        
        if m_fullNameTF.text?.isEmpty == true{
            GRAPHICS.showAlert("Please enter full name")
            return false
        }
        else if m_address1TF.text?.isEmpty == true{
            GRAPHICS.showAlert("Please enter address1")
            return false
        }
        else if m_address2TF?.text?.isEmpty == true{
            GRAPHICS.showAlert("Please enter address2")
            return false
        }
        else if m_cityTF?.text?.isEmpty == true{
            GRAPHICS.showAlert("Please enter city")
            return false
        }
        else if m_stateTF?.text?.isEmpty == true{
            GRAPHICS.showAlert("Please re-enter state")
            return false
        }
        else if m_zipTF?.text?.isEmpty == true{
            GRAPHICS.showAlert("Please enter zip code")
            return false
        }
        else if m_countryTF?.text?.isEmpty == true{
            GRAPHICS.showAlert("Please enter country")
            return false
        }
        
        
        
        return true
    }
    //MARK:- api delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        GRAPHICS.showAlert(errorMessage);
    }
    func API_CALLBACK_getActiveCountry(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = (resultDict .objectForKey("error_code")as? String)!
        if errorCode == "1"
        {
           if let countryAry = (resultDict.objectForKey("result") as? NSArray)
           {
              if countryAry.count > 0
              {
                let countryArray = resultDict.objectForKey("result") as! NSArray
                m_countryArray.addObjectsFromArray(countryArray as [AnyObject])
            
              }
            
           }
           
            self.navigateToCountryScreen()
        }
        else{
            GRAPHICS.showAlert((resultDict .objectForKey("error_code")as? String)!)
            
        }
        //self.m_countryTblView .reloadData()
        
    }

    func API_CALLBACK_editShippingAddress(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        if resultDict.objectForKey("error_code")as! String == "1"
        {
            showAlertViewWithMessage(resultDict.objectForKey("result")as! String)
            if isFromProducts
            {
               let confirmOrderVC = IBConfirmOrderViewController()
                confirmOrderVC.productIdStr = productIdStr
                confirmOrderVC.sellerIdStr = sellerIdStr
                self.navigationController?.pushViewController(confirmOrderVC, animated: true)
            }
            else
            {
                for  vc:UIViewController in (self.navigationController?.viewControllers)!
                {
                    if vc .isKindOfClass(IBSettingsViewController)
                    {
                        let settingsVc : IBSettingsViewController = vc as!  IBSettingsViewController
                        self.navigationController?.popToViewController(settingsVc, animated: true)
                    }
                }
                
            }
        }
        else{
            showAlertViewWithMessage(resultDict.objectForKey("msg")as! String)
        }
    }
    func API_CALLBACK_getShippingAddress(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        if resultDict.objectForKey("error_code")as! String == "1"
        {
            let resultArray = resultDict.objectForKey("result") as! NSArray
            let shippingDict = resultArray.objectAtIndex(0) as! NSDictionary
            
            m_fullNameTF.text = shippingDict.objectForKey("name") as? String
            m_address1TF.text = shippingDict.objectForKey("address1") as? String
            m_address2TF.text = shippingDict.objectForKey("address2") as? String
            m_cityTF.text = shippingDict.objectForKey("city") as? String
            m_stateTF.text = shippingDict.objectForKey("state") as? String
            m_zipTF.text = shippingDict.objectForKey("zip") as? String
            m_countryTF.text = shippingDict.objectForKey("country_name") as? String
            m_phnoTF.text = shippingDict.objectForKey("contact") as? String
            countryIdStr = (shippingDict.objectForKey("country") as? String)!
        }
        else
        {
            showAlertViewWithMessage(resultDict.objectForKey("msg")as! String)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
