//
//  IBFollowersListVC.swift
//  IntoBuy
//
//  Created by Manojkumar on 02/06/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBFollowersListVC: IBBaseViewController,UITableViewDelegate,UITableViewDataSource,ServerAPIDelegate,FollowProtocol {
    
    var followersListTblView = UITableView()
    var followersArray = NSArray()
    var userIdStr = String()
    var showFollowBtn: Bool = false
    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        self.hideSettingsBtn(true)
        self.m_titleLabel.text = "Followers"
        callServiceToGetFollowersList()
        createTableView()
    }
    func createTableView()
    {
        //Creation of search table
        followersListTblView.frame = self.m_bgImageView.bounds
        followersListTblView.delegate = self
        followersListTblView.dataSource = self
        followersListTblView.tag = 2
        followersListTblView.separatorStyle = .None
        self.m_bgImageView.addSubview(followersListTblView)
    }
    //MARK: - service for search
    func callServiceToGetFollowersList()
    {
//        let userDetailUD = NSUserDefaults()
//        let userIdStr = userDetailUD.objectForKey("user_id") as! String;
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)

        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getFollowersForUser(userIdStr, myUserId: getUserIdFromUserDefaults())
        
        if userIdStr == getUserIdFromUserDefaults() { //Looged in user only viewing followers list page.
            showFollowBtn = true
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            return 70.0
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return followersArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell \(indexPath.row) \(indexPath.section)"
            var cell:IBFollowTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBFollowTableViewCell
            
            if (cell == nil) {
                cell = IBFollowTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            }
            if followersArray.count > 0 {
                let resultDict = followersArray.objectAtIndex(indexPath.row)as! NSDictionary
                let followStatus = resultDict.objectForKey("isIamFollowing")as! String;
                cell?.selectionStyle = UITableViewCellSelectionStyle.None
                cell?.valuesForControls((resultDict.objectForKey("avatar")as? String!)!, name: (resultDict.objectForKey("username")as? String!)!, followState: followStatus , trophyStr: (resultDict.objectForKey("buytrophy")as? String!)!,isfromFollowers:true,profileImgTag: indexPath.row * 10, showFollowBtn:false) //deepika to hidden the m_followBtn by default i set as false
                cell?.followProtocol = self
                cell?.m_trophyLbl.hidden = true //deepika to hide the m_trophyLbl
                cell?.m_followBtn.tag = indexPath.row
                
                cell?.m_followBtn.addTarget(self, action: #selector(IBFollowersListVC.followBtnTarget(_:)), forControlEvents: .TouchUpInside)
            }
            return cell!;
            
    }
    
    func tapOnProfileImage(tapGesture : UITapGestureRecognizer)
    {
        let resultDict = followersArray.objectAtIndex((tapGesture.view?.tag)!/10)as! NSDictionary
        let profileUserId = resultDict.objectForKey("userId") as! String
        let myPageVC = IBMyPageViewController()
        myPageVC.m_isFromCameraBtn = false
        myPageVC.otherUserId = profileUserId
        self.navigationController?.pushViewController(myPageVC, animated: true)
    }
    
    func followBtnTarget(sender:UIButton)
    {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:Loading, animated:true)
        
        let userDetailUD = NSUserDefaults()
        let userIdStr = userDetailUD.objectForKey("user_id") as! String;
        let resultDict = followersArray.objectAtIndex(sender.tag)as! NSDictionary
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_follow(userIdStr, followerId: resultDict.objectForKey("userId") as! String)
        }
    }


    // MARK: Api delegate
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    // api call back for follow
    func API_CALLBACK_follow(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            self.callServiceToGetFollowersList()
            //m_searchTblView.reloadData()
        }
        else{
            showAlertViewWithMessage((resultDict.objectForKey("msg")as? String!)!)
        }
    }
    // api call back for get all users
    func API_CALLBACK_getFollowersForUser(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            followersArray = resultDict.objectForKey("result") as!  NSArray
            followersListTblView.reloadData()
        }
        else{
            GRAPHICS.showAlert(resultDict.objectForKey("msg")as? String!)
        }
    }

}
