//
//  IBSearchProductLocationVC.swift
//  IntoBuy
//
//  Created by Manojkumar on 01/02/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class IBSearchProductLocationVC: IBBaseViewController,CLLocationManagerDelegate,ServerAPIDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource {
    
    var m_mapView: MKMapView!
    var m_locationManager:CLLocationManager!
    var m_minLabel:UILabel!
    var m_maxLbl:UILabel!
    var locationDict = NSMutableDictionary()
    var searchView:UIView!
    var searchTextField:UITextField!
    var keyBoardTool:UIToolbar!
    var suggestedPlacesTblView:UITableView!
    var isFromSell = Bool()
    var placesArray = NSMutableArray()

    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        self.hideHeaderViewForView(true)
        self.hideBottomView(true)
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_bgImageView.frame.size.height = GRAPHICS.Screen_Height()
        self.createControls()
        createtableViewToDisplaySuggestedPlaces()

    }
    
    
    
    func createControls()
    {
        var xPos:CGFloat = 50
        var yPos = GRAPHICS.Screen_Y()
        var width = GRAPHICS.Screen_Width() - 100
        var height:CGFloat = 50
        
        let titleLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        titleLbl.text = "Location"
        titleLbl.font = GRAPHICS.FONT_REGULAR(16)
        titleLbl.textAlignment = .Center
        titleLbl.textColor = UIColor(red: 228/255, green: 0/255, blue: 35/255, alpha: 1)
        self.m_bgImageView.addSubview(titleLbl)
        
        let backImg = GRAPHICS.PRODUCT_CATEGORY_UP_ARROW_IMAGE()
        width = backImg.size.width/2
        height = backImg.size.height/2
        yPos = (titleLbl.frame.size.height - height)/2
        xPos = GRAPHICS.Screen_Width() - width - 10
        let backBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        backBtn.setBackgroundImage(backImg, forState: .Normal)
        backBtn.addTarget(self, action: #selector(IBSearchProductLocationVC.backBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_bgImageView.addSubview(backBtn)
        
        width = width + 40
        height = height + 40
        xPos = xPos - 20
        yPos = yPos - 20
        let backBigBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        backBigBtn.addTarget(self, action: #selector(IBSearchProductCategoryVC.backBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_bgImageView.addSubview(backBigBtn)

        xPos = GRAPHICS.Screen_X()
        yPos = GRAPHICS.Screen_Y() + 10
        width = 50
        height = 35
        let doneBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        doneBtn.addTarget(self, action: #selector(IBSearchProductLocationVC.doneBtnAction(_:)), forControlEvents: .TouchUpInside)
        doneBtn.setTitle(Done, forState: .Normal)
        doneBtn.titleLabel!.font = GRAPHICS.FONT_REGULAR(14)
        doneBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
        self.m_bgImageView.addSubview(doneBtn)

        
        
        searchView = UIView(frame: CGRectMake(self.m_bgImageView.bounds.origin.x,titleLbl.frame.maxY,self.m_bgImageView.frame.size.width,50))
        searchView.backgroundColor = lightGrayColor
        self.m_bgImageView .addSubview(searchView)
        
        let searchImage = GRAPHICS.PRODUCT_SEARCH_IMAGE()
        var imgWidth = (searchImage.size.width)/1.5
        var imgHei = searchImage.size.height/1.5
        
        let searchImgView = UIImageView(frame: CGRectMake(searchView.bounds.origin.x + 5, (searchView.frame.size.height - imgHei)/2, imgWidth, imgHei))
        searchImgView.image = searchImage
        searchView.addSubview(searchImgView)
        
        
        let textImg = GRAPHICS.PRODUCT_SEARCH_TEXT_IMAGE()
        imgWidth = textImg.size.width/2
        imgHei = textImg.size.height/1.5
        searchTextField = UITextField(frame:CGRectMake(searchImgView.frame.maxX + 10, (searchView.frame.size.height - imgHei)/2, imgWidth, imgHei))
        if GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus
        {
            searchTextField.frame.origin.x = searchView.bounds.origin.x + 50
        }
        searchTextField.background = textImg
        searchTextField.delegate = self
        // searchTextField.inputAccessoryView = createKeyboardToolBar()
        searchTextField.placeholder = Search
        searchTextField.font = GRAPHICS.FONT_REGULAR(14)
        searchTextField.addTarget(self, action: #selector(IBSearchProductLocationVC.textViewDidChange(_:)), forControlEvents:UIControlEvents.EditingChanged)
        searchTextField.inputAccessoryView = createKeyboardToolBar()
        searchTextField.textAlignment = NSTextAlignment.Center
        searchTextField.returnKeyType = .Done
        searchView.addSubview(searchTextField)

        
        
        xPos = GRAPHICS.Screen_X()
        width = GRAPHICS.Screen_Width()
        height = GRAPHICS.Screen_Height() - 155
        yPos = searchView.frame.maxY
        m_mapView = MKMapView(frame: CGRectMake(xPos,yPos,width,height))
        m_mapView.mapType = MKMapType.Standard
        m_mapView.showsUserLocation = true
        self.m_bgImageView.addSubview(m_mapView)
        let pointsannotation = MKPointAnnotation()
        
        if locationDict["latitude"] != nil || locationDict["longitude"] != nil
        {
        let latitude = Double(locationDict["latitude"] as! String)!
        let longitude = Double(locationDict["longitude"] as! String)!
            
            
            let coordinates:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
            let point = MKPointAnnotation()
            point.coordinate = coordinates
            
            self.m_mapView.removeAnnotations(self.m_mapView.annotations)
            
            self.m_mapView.addAnnotation(point)
            
            //                self.m_Latitude = coordinates.latitude
            //                self.itude = coordinates.longitude
            
            let center = CLLocationCoordinate2D(latitude:coordinates.latitude, longitude:coordinates.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpanMake(0.15,0.15))
          //  let region = MKCoordinateRegionMakeWithDistance(center, 1000, 1000)

            
//            var coordinate2 = CLLocationCoordinate2D()
//            
//            coordinate2 = CLLocationCoordinate2DMake(latitude, longitude)
//            
//            let point = MKPointAnnotation()
//            point.coordinate = coordinate2
//            m_mapView.addAnnotation(point)
//            
//            var region:MKCoordinateRegion  = m_mapView.region
//            var span:MKCoordinateSpan  = m_mapView.region.span
//            span.latitudeDelta = coordinate2.latitude
//            span.longitudeDelta = coordinate2.longitude;
//            region.span=span;
            m_mapView .setRegion(region, animated: true)

        }
        
       // m_mapView.removeAnnotations(mapView.annotations)
        
        
        
    
        
        m_locationManager = CLLocationManager()
        m_locationManager.desiredAccuracy = kCLLocationAccuracyBest
        m_locationManager.delegate = self
        m_locationManager.requestAlwaysAuthorization()
        m_locationManager.startUpdatingLocation()
        
        let mapTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBSearchProductLocationVC.selectLocationYouNeed(_:)))
        m_mapView.addGestureRecognizer(mapTapGesture)
        
        height = 55
        width = GRAPHICS.Screen_Width()
        yPos = GRAPHICS.Screen_Height() - height
        xPos = GRAPHICS.Screen_X()
        
        let sliderView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        sliderView.image = GRAPHICS.PRODUCT_LOCATION_BOTTOMVIEW_IMAGE()
        sliderView.userInteractionEnabled = true
        self.m_bgImageView.addSubview(sliderView)
        
        height = 15
        width = sliderView.frame.size.width/2
        xPos = (sliderView.frame.size.width - width)/2
        yPos = sliderView.bounds.origin.y
        let sliderTitleLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        sliderTitleLbl.text = String(format: "%@:",setRadius)
        sliderTitleLbl.font = GRAPHICS.FONT_BOLD(12);
        sliderTitleLbl.textColor = UIColor.whiteColor()
        sliderTitleLbl.textAlignment = .Center
        sliderView.addSubview(sliderTitleLbl);
        
       
//        var frame = CGRect();
//        frame.origin = CGPointZero
//        frame.origin.y = sliderTitleLbl.frame.maxY + 3
        
        
//        let slider = ASRangeSlider(spectrum: FloatRangeMake(0,500))
//        slider.setActiveAreaBackgroundImage(GRAPHICS.PRODUCT_LOCATION_SLIDER_BG_IMAGE())
//        slider.setThumbBackgroundImage(GRAPHICS.PRODUCT_LOCATION_SLIDER_THUMB_IMAGE())
//        frame.size.width = GRAPHICS.PRODUCT_LOCATION_SLIDER_BG_IMAGE().size.width/2
//        frame.size.height = GRAPHICS.PRODUCT_LOCATION_SLIDER_BG_IMAGE().size.height/2
//        frame.origin.x = (sliderView.frame.size.width - frame.size.width)/2
//        slider.frame = frame;
//        slider.rightmostThumb.hidden = true;
//        slider.addTarget(self, action: #selector(IBSearchProductLocationVC.sliderValueChanged(_:)), forControlEvents: .ValueChanged)
//        sliderView.addSubview(slider)
        
        let thumbImage  = GRAPHICS.PRODUCT_LOCATION_SLIDER_THUMB_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(thumbImage)
        height = contentSize.height
        width = sliderView.frame.size.width - 20
        xPos = (sliderView.frame.size.width - width)/2
        yPos = sliderTitleLbl.frame.maxY
        
        let slider = UISlider()
        slider.frame = CGRectMake(xPos,yPos,width,height)
        slider.minimumValue = 1
        slider.maximumValue = 500
        slider.value = 0
        slider.continuous = true
        slider.thumbTintColor = UIColor.clearColor()
        slider.addTarget(self, action: #selector(IBSearchProductLocationVC.sliderValueChanged(_:)), forControlEvents: .ValueChanged)
        slider.tintColor = UIColor.whiteColor()
        sliderView.addSubview(slider)
        slider.trackRectForBounds(CGRectMake(10, (slider.frame.size.height - 40)/2, sliderView.frame.size.width - 20, height))
        
        let ratio : CGFloat = CGFloat ( slider.value)
        
        let size = CGSizeMake( contentSize.width * ratio, contentSize.height * ratio )
        slider.setThumbImage(self.imageWithImage(thumbImage, scaledToSize: size), forState: .Normal)
        slider.setMaximumTrackImage(GRAPHICS.PRODUCT_LOCATION_SLIDER_BG_IMAGE(), forState: .Normal)
        slider.setThumbImage(self.imageWithImage(thumbImage, scaledToSize: size), forState: .Highlighted)

        
//        let sliderViewe = UISlider()
//        sliderViewe.se
        
        
        xPos = slider.frame.origin.x - 5
        
        yPos = slider.frame.maxY
        width = 70
        height = 15
        m_minLabel = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_minLabel.text = "1 km"
        m_minLabel.font = GRAPHICS.FONT_BOLD(12);
        m_minLabel.textColor = UIColor.whiteColor()
        sliderView.addSubview(m_minLabel);
        if locationDict["distance"] != nil
        {
            
            let intValue = Int(locationDict["distance"] as! String)
            let floatValue = Float(intValue!)
            slider.value = floatValue//FloatRangeMake(floatValue, Float(500))
            //slider.value = FloatRange.init(min: Float(locationDict["distance"] as! String)!, max: Float(500.0))
           // slider.value = FloatRangeMake(Float(locationDict["distance"] as! String)!,Float(500.0))//Float(locationDict["distance"] as! String)!
            m_minLabel.text = String(format: "%@",locationDict["distance"] as! String)
        }
        
        xPos = sliderView.frame.size.width - width - 10
        m_maxLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_maxLbl.text = "500 km"
        m_maxLbl.font = GRAPHICS.FONT_BOLD(12);
        m_maxLbl.textColor = UIColor.whiteColor()
        m_maxLbl.textAlignment = .Right
        sliderView.addSubview(m_maxLbl);

        
        
        
    }
    func imageWithImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!;
    }
    
    func createtableViewToDisplaySuggestedPlaces()
    {
        suggestedPlacesTblView = UITableView(frame: CGRectMake(GRAPHICS.Screen_X() + 10, searchView.frame.maxY - 5, GRAPHICS.Screen_Width() - 20, 250))
        suggestedPlacesTblView.delegate = self
        suggestedPlacesTblView.dataSource = self
        self.m_bgImageView.addSubview(suggestedPlacesTblView)
        suggestedPlacesTblView.hidden = true
        //self.m_bgImageView.bringSubviewToFront(suggestedPlacesTblView)
    }
    
    func sliderValueChanged(rangeSlider: ASRangeSlider)
    {
        let range:FloatRange = rangeSlider.value
        //print("Range slider value changed: (\(range.min) , \(range.max))")
        let minVal:Int = Int(range.min)
        let minValStr = String(format: "%d",minVal)
        //print(minValStr)
        m_minLabel.text = String(format: "%d km",minVal);
        locationDict.setValue(minValStr, forKey: "distance")
    }
    
    func selectLocationYouNeed(tapGesture: UITapGestureRecognizer)
    {
        if (tapGesture.state != .Ended) {
            return;
        }
        m_mapView.removeAnnotations(m_mapView.annotations)
        let touchPoint = tapGesture.locationInView(m_mapView);
        let touchMapCoordinate:CLLocationCoordinate2D!
        touchMapCoordinate = m_mapView.convertPoint(touchPoint, toCoordinateFromView: m_mapView)
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = touchMapCoordinate
        
        let coordinate2:CLLocationCoordinate2D = CLLocationCoordinate2DMake(touchMapCoordinate.latitude, touchMapCoordinate.longitude)
        
        let annotationPoint = MKPointAnnotation()
        annotationPoint.coordinate = coordinate2
        m_mapView.addAnnotation(annotationPoint)
        
        let lati:Double = touchMapCoordinate.latitude
        let long:Double = touchMapCoordinate.longitude
        
        let latStr = String(lati)
        let longStr = String(long)
        
        locationDict.setValue(latStr, forKey: "latitude")
        locationDict.setValue(longStr, forKey: "longitude")
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_getAddressFromGoogle(latStr, longitude: longStr)
        })
        
//        let geoCoder = CLGeocoder()
//        let location = CLLocation(latitude: touchMapCoordinate.latitude, longitude: touchMapCoordinate.longitude)
//        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
//            
//            // Place details
//            var placeMark: CLPlacemark!
//            placeMark = placemarks?[0]
//            
//            // Address dictionary
//            //print(placeMark.addressDictionary)
//            let locationDict = placeMark.addressDictionary as! NSDictionary
//            self.locationStr = locationDict.objectForKey("SubLocality") as! String
//        })
        
    }
    //MARK:- cllocation manager delegate
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let newLocation = locations.last
        var location = CLLocationCoordinate2D();
        
        if locationDict["latitude"] != nil &&  locationDict["longitude"] != nil
        {
            location.latitude = Double(locationDict["latitude"] as! String)!
            location.longitude = Double(locationDict["longitude"] as! String)!
        }
        else{
        location.latitude = (newLocation?.coordinate.latitude)!
        location.longitude = (newLocation?.coordinate.longitude)!
        }
        
        let region:MKCoordinateRegion = MKCoordinateRegionMakeWithDistance(location, 1000, 1000)
        m_mapView .setRegion(region, animated: true)
        m_locationManager .stopUpdatingLocation()
    }
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        
    }
    func backBtnAction(sender : UIButton)
    {
        if isFromSell
        {
            for  vc in (self.navigationController?.viewControllers)!
            {
                if vc .isKindOfClass(IBProductsSellViewController)
                {
                    let sellVC : IBProductsSellViewController = vc as! IBProductsSellViewController
                    if let val = locationDict["address"]
                    {
//                    if locationDict.objectForKey("address") as? String != nil
//                    {
                        let addrStr = locationDict.objectForKey("address") as! String
                        let addrArray = addrStr.componentsSeparatedByString(",") as NSArray
                        sellVC.productEntity.latitude = locationDict.objectForKey("latitude") as! String//locationDict["latitude"]
                        sellVC.productEntity.longitude = locationDict["longitude"] as! String
                        if addrArray.count > 2{
                            let locationStr = addrArray.objectAtIndex(addrArray.count - 3) as! String
                            sellVC.productEntity.address = locationStr
                        }
                        else
                        {
                            let locationStr = addrArray.objectAtIndex(addrArray.count - 1) as! String
                            sellVC.productEntity.address = locationStr
                        }
                        sellVC.addLocationBtn.selected = true
                    }
                    else
                    {
                        sellVC.productEntity.address = ""
                        sellVC.addLocationBtn.selected = false
                    }

                }
            }
        }
        else{
            for  vc in (self.navigationController?.viewControllers)!
            {
                if vc .isKindOfClass(IBHomeViewController)
                {
                    let homePage : IBHomeViewController = vc as! IBHomeViewController
                    homePage.m_locationDict = locationDict
                    homePage.isFromLocation = true
                    homePage.isFromFilter = false
                    homePage.isFromCategory = false
                }
            }
        }
        
        self.navigationController?.popViewControllerAnimated(true)

    }
    func doneBtnAction(sender : UIButton)
    {
        if isFromSell
        {
            for  vc in (self.navigationController?.viewControllers)!
            {
                if vc .isKindOfClass(IBProductsSellViewController)
                {
                    let sellVC : IBProductsSellViewController = vc as! IBProductsSellViewController
                    if let val = locationDict["address"]
                    {
//                    if locationDict.objectForKey("address") as? String != nil
//                    {
                        let addrStr = locationDict.objectForKey("address") as! String
                        let addrArray = addrStr.componentsSeparatedByString(",") as NSArray
                        sellVC.productEntity.latitude = locationDict.objectForKey("latitude") as! String//locationDict["latitude"]
                        sellVC.productEntity.longitude = locationDict["longitude"] as! String

                        if addrArray.count > 2{
                            let locationStr = addrArray.objectAtIndex(addrArray.count - 3) as! String
                            sellVC.productEntity.address = locationStr
                        }
                        else
                        {
                            let locationStr = addrArray.objectAtIndex(addrArray.count - 1) as! String
                            sellVC.productEntity.address = locationStr
                        }
                        sellVC.addLocationBtn.selected = true
                    }
                    else
                    {
                        sellVC.productEntity.address = ""
                        sellVC.addLocationBtn.selected = false
                    }
                }
            }
        }
        else{
            for  vc in (self.navigationController?.viewControllers)!
            {
                if vc .isKindOfClass(IBHomeViewController)
                {
                    let homePage : IBHomeViewController = vc as! IBHomeViewController
                    homePage.m_locationDict = locationDict
                    homePage.isFromLocation = true
                    homePage.isFromFilter = false
                    homePage.isFromCategory = false
                }
            }
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func API_CALLBACK_getAddress(resultDict: NSDictionary)
    {
        //print(resultDict)
        let resultarr = resultDict.objectForKey("results")as! NSArray
        if resultarr.count > 0{
        let addrDict = resultarr.objectAtIndex(0) as! NSDictionary
        let formattedAddress = addrDict.objectForKey("formatted_address") as! String
        locationDict.setValue(formattedAddress, forKey: "address")
        }
        
        
    }
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        showAlertViewWithMessage(errorMessage)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func createKeyboardToolBar() -> UIToolbar
    {
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        keyBoardTool = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        keyBoardTool!.barStyle = UIBarStyle.BlackOpaque
        keyBoardTool!.translucent = true
        keyBoardTool!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        keyBoardTool!.sizeToFit()
        
        
        // let doneButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("nextAction"))
        
        let cancel_Btn = UIButton ()
        cancel_Btn.frame = CGRectMake(10, 0, 70, 50)
        cancel_Btn.backgroundColor =  UIColor.clearColor();
        cancel_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        cancel_Btn.setTitle("Cancel", forState: UIControlState.Normal)
        cancel_Btn.addTarget(self, action: #selector(IBActivityViewController.cancelAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(cancel_Btn)
        
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBActivityViewController.doneAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(done_Btn)
        
        keyBoardTool!.userInteractionEnabled = true
        return keyBoardTool
        
    }
    
    func cancelAction()
    {
        if searchTextField.isFirstResponder()
        {
            searchTextField.text = ""
            searchTextField.resignFirstResponder()
          //  btnIndex = 0
           // m_youTblView.reloadData()
        }
    }
    func doneAction()
    {
        if searchTextField.isFirstResponder()
        {
            searchTextField.resignFirstResponder()
        }
        if searchTextField.text == ""
        {
         //   btnIndex = 0
           // m_youTblView.reloadData()
        }
    }
    func textViewDidChange(textField : UITextField) -> () {
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_PlaceSuggestion(NSMutableString(string: textField.text!))
            }

    func API_CALLBACK_PlaceSuggestion(resultArray: NSMutableArray)
    {
        //print(resultArray)
        placesArray.removeAllObjects()
        for i in 0  ..< resultArray.count
        {
            let dict = resultArray.objectAtIndex(i) as! NSDictionary
            let placeStr = dict.objectForKey("description") as! String
            placesArray.addObject(placeStr)
        }
        if (placesArray.count > 0) {
            suggestedPlacesTblView.reloadData()
            suggestedPlacesTblView.hidden = false;
        } else {
            suggestedPlacesTblView.hidden = true;
            
        }
        
    }
    //MARK:- tableview delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placesArray.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40;
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell:UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        cell?.textLabel?.text = placesArray.objectAtIndex(indexPath.row) as? String
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        let serverApi = ServerAPI()
//        serverApi.delegate = self
        suggestedPlacesTblView.hidden = true
        let address = placesArray.objectAtIndex(indexPath.row) as! String
        let geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                //print("Error", error)
            }
            if let placemark = placemarks?.first {
                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                let point = MKPointAnnotation()
                point.coordinate = coordinates
                
                self.m_mapView.removeAnnotations(self.m_mapView.annotations)
                
                self.m_mapView.addAnnotation(point)
                
//                self.m_Latitude = coordinates.latitude
//                self.itude = coordinates.longitude
                
                let center = CLLocationCoordinate2D(latitude:coordinates.latitude, longitude:coordinates.longitude)
                let region = MKCoordinateRegion(center: center, span: MKCoordinateSpanMake(0.15,0.15))//MKCoordinateRegionMakeWithDistance(center, 500, 500)
                
                self.m_mapView.setRegion(region, animated: true)
            }
        })
    }

}
