 //
//  IBMydiaryCalendarViewController.swift
//  IntoBuy
//
//  Created by SmaatApps on 10/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit



class IBMydiaryCalendarViewController: IBBaseViewController,FSCalendarDataSource, FSCalendarDelegate,UITableViewDataSource,UITableViewDelegate,ServerAPIDelegate,FSCalendarDelegateAppearance
{
    
    var m_calendarview:FSCalendar!
    var m_profileView:UIView = UIView()
    var m_nameLabel:UILabel = UILabel()
    var m_countryLabel:UILabel = UILabel()
    var m_descrptionlabel:UILabel = UILabel()
    var m_profileImageView:UIImageView = UIImageView()
    var diaryScrlView = UIScrollView()
    var diaryTblView:UITableView!
    // var diaryDataDict = NSMutableDictionary()
    var dateStr = ""
    var diaryArray = NSMutableArray()
    var userIdStr = ""
    var selectedDateArray = NSMutableArray()
    var dateArray = NSMutableArray()
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        //let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //diaryDataDict = NSMutableDictionary(dictionary:appdelegate.diaryDictionary)
        let date = NSDate()
        let dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateStr = dateFormatter.stringFromDate(date)
        let dateArray:NSArray = dateStr.componentsSeparatedByString("-")
        m_calendarview.selectDate(m_calendarview.dateWithYear(Int(dateArray.objectAtIndex(0) as! String)!, month: Int(dateArray.objectAtIndex(1) as! String)!, day: Int(dateArray.objectAtIndex(2) as! String)!))
        
        //let userIdStr = getUserIdFromUserDefaults()
        SwiftLoader.show(animated: true)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getDiary(userIdStr, today: "")
        
    }
    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        
        // self.hideHeaderViewForView(false)
        // self.hideBottomView(true)
        m_titleLabel.text = MyDiary
        
        diaryScrlView.frame = self.m_bgImageView.bounds
        self.m_bgImageView.addSubview(diaryScrlView)
        self.initControls()
        
        
    }
    func initControls()
    {
        //
        m_profileView.frame = CGRectMake(0, 0, GRAPHICS .Screen_Width(), 100)
        m_profileView.backgroundColor = UIColor.grayColor()
        diaryScrlView .addSubview(m_profileView)
        
        
        m_profileImageView.frame = CGRectMake(15, 10, 80, 80)
        m_profileImageView.layer.cornerRadius = 80/2;
        m_profileImageView.clipsToBounds = true
        m_profileView .addSubview(m_profileImageView)
        
        var xpos:CGFloat = m_profileImageView.frame.maxX+10
        var ypos:CGFloat = m_profileImageView.frame.origin.y+10
        var width:CGFloat = GRAPHICS.Screen_Width()-30-m_profileImageView.frame.size.width
        var height:CGFloat = 16
        
        m_nameLabel.frame = CGRectMake(xpos, ypos, width, height)
        m_nameLabel.font = GRAPHICS.FONT_REGULAR(12)
        m_nameLabel.textAlignment = NSTextAlignment.Left
        m_nameLabel.textColor = UIColor.whiteColor()
        m_profileView.addSubview(m_nameLabel)
        
        ypos = m_nameLabel.frame.maxY+3
        
        m_countryLabel.frame = CGRectMake(xpos, ypos, width, height)
        m_countryLabel.font = GRAPHICS.FONT_REGULAR(12)
        m_countryLabel.textAlignment = NSTextAlignment.Left
        m_countryLabel.textColor = UIColor.whiteColor()
        m_profileView.addSubview(m_countryLabel)
        
        ypos = m_countryLabel.frame.maxY+5
        
        m_descrptionlabel.frame = CGRectMake(xpos, ypos, width, height)
        m_descrptionlabel.font = GRAPHICS.FONT_REGULAR(12)
        m_descrptionlabel.textAlignment = NSTextAlignment.Left
        m_descrptionlabel.textColor = UIColor.whiteColor()
        m_profileView.addSubview(m_descrptionlabel)
        
        
        ypos = m_profileView.bounds.origin.y
        width = m_profileView.frame.size.width/4
        height = width
        xpos = m_profileView.frame.size.width - width
        let addBigBtn = UIButton(frame: CGRectMake(xpos, ypos, width, height))
        addBigBtn.addTarget(self, action: #selector(IBMydiaryCalendarViewController.addBtnAction(_:)), forControlEvents: .TouchUpInside)
        m_profileView.addSubview(addBigBtn)
        
        
        
        
        let addImg = UIImage(named: "intobuy-bottom-bar-add-icon.png")
        width = (addImg?.size.width)!/2
        height = (addImg?.size.height)!/2
        xpos = m_profileView.frame.size.width - width - 10
        ypos = m_profileView.bounds.origin.y + 10
        let addBtn = UIButton(frame: CGRectMake(xpos, ypos, width, height))
        addBtn.setBackgroundImage(addImg, forState: .Normal)
        addBtn.addTarget(self, action: #selector(IBMydiaryCalendarViewController.addBtnAction(_:)), forControlEvents: .TouchUpInside)
        m_profileView.addSubview(addBtn)
        m_profileView.bringSubviewToFront(addBtn)
        
        
        if userIdStr == getUserIdFromUserDefaults()
        {
            addBigBtn.hidden = false
            addBtn.hidden = false
        }
        else
        {
            addBigBtn.hidden = true
            addBtn.hidden = true
        }
        createCalendarView()
        
        
        //    let allKeys = diaryDataDict.allKeys as NSArray
        //    if (allKeys.containsObject(dateStr)) {
        //        //diaryTblView.reloadData()
        //        createTableForDairy()
        //    }
        
        
        
        
        //        calendar.allowsMultipleSelection = true
        
        // Uncomment this to test month->week and week->month transition
        /*
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(1.5 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
         self.calendar.setScope(.Week, animated: true)
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (Int64)(1.5 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
         self.calendar.setScope(.Month, animated: true)
         }
         }
         */
        
    }
    func createCalendarView()
    {
        let ypos = m_profileView.frame.maxY
        ///
        if m_calendarview != nil{
            m_calendarview.removeFromSuperview()
            m_calendarview = nil
        }
        m_calendarview = FSCalendar()
        m_calendarview.frame = CGRectMake(0, ypos, GRAPHICS.Screen_Width(), 280)
        m_calendarview.delegate = self;
        m_calendarview.dataSource = self;
        diaryScrlView.addSubview(m_calendarview)
        m_calendarview.scrollDirection = .Vertical
        m_calendarview.appearance.caseOptions = [.HeaderUsesUpperCase,.WeekdayUsesUpperCase]
        
            let date = NSDate()
            let dateFormatter:NSDateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let dateStr = dateFormatter.stringFromDate(date)
            let dateArray:NSArray = dateStr.componentsSeparatedByString("-")
            m_calendarview.selectDate(m_calendarview.dateWithYear(Int(dateArray.objectAtIndex(2) as! String)!, month: Int(dateArray.objectAtIndex(1) as! String)!, day: Int(dateArray.objectAtIndex(0) as! String)!))
    }
    func createTableForDairy() -> () {
        
        if diaryTblView != nil
        {
            diaryTblView.removeFromSuperview()
            diaryTblView = nil
        }
        diaryTblView = UITableView(frame: CGRectMake(0, m_calendarview.frame.maxY, GRAPHICS .Screen_Width(),/*self.m_bgImageView.frame.size.height - m_profileView.frame.size.height - m_calendarview.frame.size.height*/500))
        diaryTblView.delegate = self
        diaryTblView.dataSource = self
        diaryTblView.separatorStyle = .None
        diaryScrlView.addSubview(diaryTblView)
        diaryTblView.scrollEnabled = false
        diaryScrlView.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), diaryTblView.frame.maxY + 10)
    }
    //MARK:- calender delegates
    func minimumDateForCalendar(calendar: FSCalendar!) -> NSDate! {
        return calendar.dateWithYear(2015, month: 1, day: 1)
    }
    
    func maximumDateForCalendar(calendar: FSCalendar!) -> NSDate! {
        return calendar.dateWithYear(2016, month: 12, day: 31)
    }
    
    
    func calendar(calendar: FSCalendar!, hasEventForDate date: NSDate!) -> Bool {
        return calendar.dayOfDate(date) == 5
    }
    
    func calendarCurrentPageDidChange(calendar: FSCalendar!) {
        NSLog("change page to \(calendar.stringFromDate(calendar.currentPage))")
    }
    
    func calendar(calendar: FSCalendar!, didSelectDate date: NSDate!) {
        NSLog("calendar did select date \(calendar.stringFromDate(date))")
        let dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateStr = dateFormatter.stringFromDate(date)
        
        
//          let allKeys = diaryDataDict.allKeys
//        
//            let allKeys = diaryDataDict.allKeys as NSArray
//            if (allKeys.containsObject(dateStr)) {
//                //diaryTblView.reloadData()
//                createTableForDairy()
//                let userIdStr = getUserIdFromUserDefaults()
//                let serverApi = ServerAPI()
//                serverApi.delegate = self
//                serverApi.API_getDiary(userIdStr, today: dateStr)
        
        getDiaryDetailsOfSelectedDate()
//            }
//            else {
//                if diaryTblView != nil
//                {
//                    diaryTblView.removeFromSuperview()
//                    diaryTblView = nil
//                }
//                GRAPHICS.showAlert("No Records Found")
//            }
        
        
        
        //    let mydiaryNotesVc = IBMyDiaryPostingViewController()
        //    mydiaryNotesVc.dateStr = dateStr
        //    self.navigationController?.pushViewController(mydiaryNotesVc, animated: true)
    }
    func calendar(calendar: FSCalendar!, appearance: FSCalendarAppearance!, titleDefaultColorForDate date: NSDate!) -> UIColor!
    {
        let dateString: String = calendar.stringFromDate(date, format: "yyyy-MM-dd")
        
        
        if dateArray.containsObject(dateString)
        {
            return UIColor.redColor()
        }
        else
        {
            return nil
        }
        
    }
    
    
    func addBtnAction(sender : UIButton)
    {
        let date = NSDate()
        let dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateStr = dateFormatter.stringFromDate(date)
        
        //        let allKeys = diaryDataDict.allKeys as NSArray
        //
        //        if (allKeys.containsObject(dateStr)) {
        //        }
        //        else {
        //            let dataArray = NSMutableArray()
        //            diaryDataDict.setValue(dataArray, forKey: dateStr)
        //        }
        let mydiaryNotesVc = IBMyDiaryPostingViewController()
        mydiaryNotesVc.dateStr = dateStr
        //  mydiaryNotesVc.postDiaryDict = NSMutableDictionary(dictionary: diaryDataDict)
        self.navigationController?.pushViewController(mydiaryNotesVc, animated: true)
    }
    
    func calendarCurrentScopeWillChange(calendar: FSCalendar!, animated: Bool) {
        // calendarHeightConstraint.constant = calendar.sizeThatFits(CGSizeZero).height
        view.layoutIfNeeded()
    }
    
    func calendar(calendar: FSCalendar!, imageForDate date: NSDate!) -> UIImage! {
        return [13,24].containsObject(calendar.dayOfDate(date)) ? UIImage(named: "icon_cat") : nil
    }
    
    //MARK:- tableView delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedDateArray.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        // let diaryArr = diaryDataDict.objectForKey(dateStr) as! NSArray
        //   heightForView(dataDict.objectForKey("comment")as! String!, font: GRAPHICS.FONT_REGULAR(12)!, width: GRAPHICS.Screen_Width() - width - 20)
        if selectedDateArray.count > 0
        {
            let diaryEntity = diaryArray.objectAtIndex(indexPath.row) as! IBDiaryEntity
            //            let imageStr = diaryEntity.post_image
            //            let imageData = NSData(contentsOfURL: NSURL(string: imageStr)!)
            //            let image = UIImage(data: imageData!)
            let diarytext = diaryEntity.post_text
            let height = heightForView(diarytext, font: GRAPHICS.FONT_REGULAR(14)!, width: GRAPHICS.Screen_Width() - 20/*(image?.size.width)!/2*/) + 10
            return max(50,height)
        }
        else{
            return 50
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell\(indexPath.row)"
        var cell:IBDiaryDataCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBDiaryDataCell
        if (cell == nil) {
            cell = IBDiaryDataCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        //let diaryArr = diaryDataDict.objectForKey(dateStr) as! NSArray
        if selectedDateArray.count > 0
        {
            let diaryEntity = selectedDateArray.objectAtIndex(indexPath.row) as! IBDiaryEntity
            let imageStr = diaryEntity.post_image
            cell?.leftImgView.load(NSURL(string: imageStr)!, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            let decodedStr = decodeEncodedStringToNormalString(diaryEntity.post_text)
            cell?.diaryLbl.text = decodedStr
            cell?.diaryLbl.frame.size.height = heightForView(diaryEntity.post_text, font: GRAPHICS.FONT_REGULAR(14)!, width: (cell?.diaryLbl.frame.size.width)!)
            cell?.diaryLbl.lineBreakMode = NSLineBreakMode.ByWordWrapping
            
            if cell?.diaryLbl.frame.size.height > cell?.leftImgView.frame.size.height {
                cell?.frame.size.height = (cell?.diaryLbl.frame.maxY)! + 10
            }
            else{
                cell?.frame.size.height = (cell?.leftImgView.frame.maxY)! + 10
            }
            
            diaryTblView.frame.size.height = cell!.frame.size.height * CGFloat(selectedDateArray.count)
            //print(diaryTblView.frame.size.height)
            diaryScrlView.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), diaryTblView.frame.maxY + 10)
            
            cell?.selectionStyle = .None
            
        }
        
        return cell!;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //let diaryArr = diaryDataDict.objectForKey(dateStr) as! NSArray
        //let diaryEntity = diaryArray.addObject(indexPath.row) as! IBDiaryEntity
        let detailsView = IBDiaryDetailsViewController()
        detailsView.detailArray = selectedDateArray
        self.navigationController?.pushViewController(detailsView, animated: true)
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    //MARK:- api delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        if diaryTblView != nil
        {
            diaryTblView.removeFromSuperview()
            diaryTblView = nil
        }
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    func API_CALLBACK_getDiary(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        if resultDict.objectForKey("error_code") as! String == "1"
        {
            diaryArray.removeAllObjects()
            let diaryDictionary = resultDict.objectForKey("result") as! NSDictionary
            let userDetailsArr = diaryDictionary.objectForKey("user_details") as! NSArray
            let userDetailsDict = userDetailsArr.objectAtIndex(0) as! NSDictionary
            m_profileImageView.load(NSURL(string: userDetailsDict.objectForKey("avatar") as! String!)!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
            m_nameLabel.text = userDetailsDict.objectForKey("username") as! String!
            m_countryLabel.text = userDetailsDict.objectForKey("CountryName") as! String!
            m_descrptionlabel.text = String(format: "%@ is the best",m_countryLabel.text!)
            
            let diaryArr = diaryDictionary.objectForKey("dairy") as! NSArray
            if diaryArr.count > 0
            {
                for  i in 0 ..< diaryArr.count
                {
                    let diarydict = diaryArr.objectAtIndex(i) as! NSDictionary
                    let diaryEntity = IBDiaryEntity(dict: diarydict)
                    
                    let existingDateformat = NSDateFormatter()
                    existingDateformat.dateFormat = "yyyy-MM-dd"
                    let date = existingDateformat.dateFromString(diaryEntity.added_date)
                    let dateFormatter:NSDateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let dateString = dateFormatter.stringFromDate(date!)
                    _ = dateFormatter.dateFromString(dateString)
                    //let dateArray:NSArray = dateString.componentsSeparatedByString("-")
                    diaryArray.addObject(diaryEntity)
                    dateArray.addObject(dateString)
                    
                }
                getDiaryDetailsOfSelectedDate()
            }
                
            else
            {
                if diaryTblView != nil
                {
                    diaryTblView.removeFromSuperview()
                    diaryTblView = nil
                }
                showAlertViewWithMessage("No data's available")
            }
        }
        else
        {
            if diaryTblView != nil
            {
                diaryTblView.removeFromSuperview()
                diaryTblView = nil
            }
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    func getDiaryDetailsOfSelectedDate()
    {
        selectedDateArray.removeAllObjects()
        for  i in 0 ..< diaryArray.count
        {
            let diaryEntity = diaryArray.objectAtIndex(i) as! IBDiaryEntity
            if dateStr == diaryEntity.added_date
            {
                selectedDateArray.addObject(diaryEntity)
            }
        }
        m_calendarview.reloadData()
 //
        if selectedDateArray.count > 0
        {
            createTableForDairy()
        }
        else
        {
            showAlertViewWithMessage("No data's available")
            if diaryTblView != nil
            {
                diaryTblView.removeFromSuperview()
                diaryTblView = nil
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 }




