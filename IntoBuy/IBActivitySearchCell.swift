//
//  IBActivitySearchCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 05/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBActivitySearchCell: UITableViewCell {

    var profileImgView = UIImageView()
    var nameLabel = UILabel()
    var friendBtn = UIButton()
    var vwLine = UIView()
    let cellheight = CGFloat(60)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsForActivitySearch()
    }

    func createControlsForActivitySearch()
    {
        self.frame.size.height = 60.0
        self.frame.size.width = GRAPHICS.Screen_Width()
        let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
        
        
        var viewWid = CGFloat(40)
        var viewHei = CGFloat(40)
        var posX = self.bounds.origin.x + 15
        var posY = CGFloat(10)
        
        profileImgView.frame = CGRectMake(posX,10,viewWid,viewHei)
        profileImgView.layer.cornerRadius = viewWid/2
        profileImgView.clipsToBounds = true
        profileImgView.image = profileMaskImg
        profileImgView.userInteractionEnabled = true
        self.addSubview(profileImgView)
        
        
        let addFriendBtnImg = GRAPHICS.ACTIVITY_ADD_FRIEND_IMAGE()
        let tickBtnImg = GRAPHICS.ACTIVITY_BUTTON_TICK_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(addFriendBtnImg)
        
        viewHei = 15
        viewWid = GRAPHICS.Screen_Width() - viewWid - contentSize.width - 15
        posX = profileImgView.frame.maxX + 10
        posY = (self.frame.size.height - viewHei)/2
        let fontSize:CGFloat = 12;
        nameLabel.frame = CGRectMake(posX,posY,viewWid,profileImgView.frame.size.height)
        nameLabel.font = GRAPHICS.FONT_REGULAR(fontSize)
        nameLabel.text = "Manoj likes ur post"
        nameLabel.numberOfLines = 0
        nameLabel.textAlignment = .Left
        self.addSubview(nameLabel)
        
       
        viewHei = contentSize.width//addFriendBtnImg.size.width/2
        viewWid = contentSize.height//addFriendBtnImg.size.height/2
        posX = GRAPHICS.Screen_Width() - viewWid - 10 - 5
        posY = (self.frame.size.height - viewHei)/2
        friendBtn.frame = CGRectMake(posX,posY,viewWid,viewHei)
        friendBtn.setBackgroundImage(addFriendBtnImg, forState: .Normal)
        friendBtn.setBackgroundImage(tickBtnImg, forState: .Selected)
        self.addSubview(friendBtn)
        
        vwLine =  UIView(frame:CGRectMake(10,cellheight-1,GRAPHICS.Screen_Width()-20,1))
        vwLine.backgroundColor = applightGraycolor
        self.addSubview(vwLine)
        
    }
    func setControls(entity:IBActivityFollowingEntity,indexpath:NSIndexPath,delegate:AnyObject)
    {
        /*let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) -> Void in
            
        }
        
        profileImgView.sd_setImageWithURL(NSURL(string: (entity.stravathar as String))!, placeholderImage:GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completed: block)*/
        profileImgView.sd_setImageWithURL(NSURL(string: (entity.stravathar as String))!, placeholderImage: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), options: .ScaleDownLargeImages)
        profileImgView.frame.origin.y = (cellheight  - profileImgView.frame.size.height)/2
        nameLabel.frame.origin.y = profileImgView.frame.minY
        nameLabel.text = entity.strUserName as String
        friendBtn.frame.origin.y = (cellheight  - friendBtn.frame.size.height)/2
        if entity.stramIFollowing as String == "1"
        {
            friendBtn.selected = true
        }
        else
        {
           friendBtn.selected = false
        }
       vwLine.frame.origin.y = cellheight - 1
        
        profileImgView.tag = indexpath.row
        profileImgView.userInteractionEnabled = true
        let profileTapGesture = UITapGestureRecognizer(target: delegate, action: #selector(IBSearchFreindsViewController.profilePicTapped(_:)))
        profileImgView.addGestureRecognizer(profileTapGesture)
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
