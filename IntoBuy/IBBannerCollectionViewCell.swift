//
//  IBBannerCollectionViewCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 22/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBBannerCollectionViewCell: UICollectionViewCell {
    var bannerImgView:UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        bannerImgView = UIImageView(frame: CGRectMake(self.bounds.minX,self.bounds.minY,self.frame.size.width,self.frame.size.height))
        bannerImgView.backgroundColor = UIColor.clearColor()
        self.addSubview(bannerImgView)
        
        
        let transparentViw :UIView = UIView()
        transparentViw.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
        transparentViw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.4)
        bannerImgView.addSubview(transparentViw)
    }
    
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
}
