//
//  IBProductDetailsViewController.swift
//  IntoBuy
//
//  Created by SmaatApps on 16/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBProductDetailsViewController: IBBaseViewController,ServerAPIDelegate,UITableViewDelegate,UITableViewDataSource
{
       var m_scrollview:UIScrollView = UIScrollView()
       var m_imageScrollview:UIScrollView = UIScrollView()
       var m_imageview:UIImageView = UIImageView()
       var m_lblProductName:UILabel = UILabel()
       var m_lblPrice:UILabel = UILabel()
       var m_lblAvailability:UILabel = UILabel()
       var m_lblshareThisdeal:UILabel = UILabel()
       var m_lbladdtoFav:UILabel = UILabel()
    var m_lblComments:UILabel = UILabel()
    
       var m_lblDescrption:UILabel = UILabel()
       var m_lbldetailDescprion:UILabel = UILabel()
       var m_lblrating:UILabel = UILabel()
       var m_viwproductName:UIView = UIView()
       var m_viwprice:UIView = UIView()
       var m_viwavailability:UIView = UIView()
       var m_viwdescDetail:UIView = UIView()
       var m_ViwaddToFav:UIView = UIView()
       var m_lblseller:UILabel = UILabel()
       var m_buyNowBtn:UIButton = UIButton()
       var m_productiamgesArray = NSArray()
       var m_lblPriceAmount:UILabel = UILabel()
       var m_lblstockAvailble:UILabel = UILabel()
       var m_locationLbl = UILabel()
       var m_BtnShare:UIButton = UIButton()
       var m_ratingImageview:UIImageView = UIImageView()
       var m_entity : IBProductDetailsEntity!
       var commentsTblView = UITableView()
       var commentBtnIndex = 0
    var chatButton = UIButton()
        //var commentArray = NSArray()
       var currentIndex = NSInteger()
       var scrollLeft = true
       var lastContentOffset = Float()
       var pageControl:UIPageControl!
       var productId = String()
       var productCommentsArray = NSMutableArray()
      let imgHeight = CGFloat(50)
    
    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()

        hideSettingsBtn(true)
        // Do any additional setup after loading the view.
        
       m_titleLabel.text = Products_my_products
        
     //   self.setUPPaypalConfiguration()
        createServiceForProductDetails()
        
        
    }
   /* func setUPPaypalConfiguration()
    {
        // Set up payPalConfig
        payPalConfig.acceptCreditCards = acceptCreditCards;
        payPalConfig.merchantName = "SmaatApps Pvt ltd"
        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        // Setting the languageOrLocale property is optional.
        //
        // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
        // its user interface according to the device's current language setting.
        //
        // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
        // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
        // to use that language/locale.
        //
        // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
        
        payPalConfig.languageOrLocale = NSLocale.preferredLanguages()[0]
        
        // Setting the payPalShippingAddressOption property is optional.
        //
        // See PayPalConfiguration.h for details.
        
        payPalConfig.payPalShippingAddressOption = .PayPal;
        
        //print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
    
    }*/
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       // PayPalMobile.preconnectWithEnvironment(environment)
    }
    func createServiceForProductDetails()
    {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_ProductDetail(productId)
    }
    
    func initControols()
    {
        m_scrollview.frame = CGRectMake(GRAPHICS.Screen_X(), self.m_bgImageView.bounds.origin.y, GRAPHICS.Screen_Width(), GRAPHICS.Screen_Height()-self.m_headerView.frame.size.height - self.m_bottomView.frame.size.height);
        m_bgImageView .addSubview(m_scrollview)
        
        var xpos:CGFloat = 0
        var ypos:CGFloat = 0
        var width:CGFloat = GRAPHICS.Screen_Width()
        var height:CGFloat = m_bgImageView.frame.size.height/2.3
        let bottomlineHeight:CGFloat = 1
        let Diff:CGFloat = 0;
        
        m_imageScrollview.frame = CGRectMake(xpos, ypos, width, height)
        m_imageScrollview.delegate = self
      //  m_imageScrollview.pagingEnabled = true
        
        m_scrollview.addSubview(m_imageScrollview)
        
        width = GRAPHICS.Screen_Width()/1.5
        ypos = ypos + 10
        height = height - 10
        self.creationOfScrollviewIamges(xpos, ypos: ypos, width: width, height: height)
        
        if(m_productiamgesArray.count > 1){
            self.pageControl = UIPageControl(frame: CGRectMake((m_imageScrollview.frame.size.width - CGFloat(3))/2, m_imageScrollview.frame.size.height - 20, 10, 10))
            self.pageControl.numberOfPages = m_productiamgesArray.count
            self.pageControl.currentPage = 0
            //  self.pageControl.tintColor = UIColor(red: 89.0/255.0, green: 191.0/255.0, blue: 239.0/255.0, alpha: 1.0)
            self.pageControl.pageIndicatorTintColor = UIColor.whiteColor()
            self.pageControl.currentPageIndicatorTintColor = UIColor(red: 89.0/255.0, green: 191.0/255.0, blue: 239.0/255.0, alpha: 1.0)
//            pageControl.addTarget(self, action: Selector("changePage:"), forControlEvents: UIControlEvents.ValueChanged)
        
            
            m_scrollview.addSubview(pageControl)
            m_scrollview.bringSubviewToFront(pageControl)
        }
    
//        m_imageview.frame = CGRectMake(xpos, ypos, width, 140)
//        m_imageview.backgroundColor = UIColor.yellowColor()
//        m_scrollview .addSubview(m_imageview)
        
        xpos = 10
        ypos = m_imageScrollview.frame.maxY
        width = GRAPHICS.Screen_Width()-xpos*2
        height = 35
       
        //creation of Product Name Label
        
        m_lblProductName = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr:m_entity.productname as String)
       // m_lblProductName.textColor = UIColor.blackColor()
        m_lblProductName.font = GRAPHICS.FONT_REGULAR(16)
        m_scrollview .addSubview(m_lblProductName)
       
        ypos = m_lblProductName.frame.maxY-1
        
        m_viwproductName = self.creationOfBottomLineView(xpos, ypos: ypos, width: width, height: bottomlineHeight)
        m_scrollview.addSubview(m_viwproductName)
        
        //Creation of Price Label
        
        ypos = m_viwproductName.frame.maxY+Diff
        width = 120
        
        m_lblPrice = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr:"Price")
        m_scrollview .addSubview(m_lblPrice)
        
        m_lblPriceAmount.frame = CGRectMake(m_viwproductName.frame.maxX-150, ypos, 140, height)
        m_lblPriceAmount.textColor = appPinkColor
        
        let currencySymbolStr : String = getCurrencySymbol() as String!
        
        m_lblPriceAmount.text = String(format: "%@%@",currencySymbolStr,m_entity.price)
        
        m_lblPriceAmount.font = GRAPHICS.FONT_REGULAR(13)
        m_lblPriceAmount.textAlignment = .Right
        m_scrollview.addSubview(m_lblPriceAmount)
        
        ypos = m_lblPrice.frame.maxY-1
        width = GRAPHICS.Screen_Width()-xpos*2
        
        m_viwprice = self.creationOfBottomLineView(xpos, ypos: ypos, width: width, height: bottomlineHeight)
        m_scrollview.addSubview(m_viwprice)
        
        //Creation of availabilty Label
        
        ypos = m_viwprice.frame.maxY+Diff
        width = 140
        
        m_lblAvailability = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr:Products_availability)
        m_scrollview .addSubview(m_lblAvailability)
        
        var availabiltyStr:String
        if m_entity.stockavailability == "0"
        {
            availabiltyStr = "Sold"
        }
        else
        {
            availabiltyStr = "In STOCK"
        }
        
        m_lblstockAvailble = self.createLabel(m_viwprice.frame.maxX-150, ypos: ypos, width: 140, height: height, textStr:availabiltyStr)
       // m_lblstockAvailble.textColor = UIColor.blackColor()
        m_lblstockAvailble.textAlignment = .Right
        m_scrollview .addSubview(m_lblstockAvailble)
        
        ypos = m_lblAvailability.frame.maxY-1
        width = GRAPHICS.Screen_Width()-xpos*2

        
        m_viwavailability = self.creationOfBottomLineView(xpos, ypos: ypos, width: width, height: bottomlineHeight)
        m_scrollview.addSubview(m_viwavailability)
        
        //Creation of Share This detail
        
        ypos = m_viwavailability.frame.maxY+Diff
        width = 180
        
        m_lblshareThisdeal = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr:Products_shareThisdeal)
        m_scrollview .addSubview(m_lblshareThisdeal)
        
      let btnImageSize:CGSize = self.getImageWidthHeight(GRAPHICS.PRODUCT_SHARE_ICON_IAMGE())
        
        m_BtnShare.frame = CGRectMake(m_viwavailability.frame.maxX-40, ypos, btnImageSize.width, btnImageSize.height)
        //m_BtnShare.backgroundColor = appPinkColor
       m_BtnShare .setBackgroundImage(GRAPHICS.PRODUCT_SHARE_ICON_IAMGE(), forState: .Normal)
        m_BtnShare.addTarget(self, action: #selector(IBProductDetailsViewController.shareButtonClicked(_:)), forControlEvents:.TouchUpInside)
        m_scrollview.addSubview(m_BtnShare)
        
        ypos = m_lblshareThisdeal.frame.maxY
        
        //Change here
        
        m_lbladdtoFav = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr:Products_addToFav)
        m_lbladdtoFav.userInteractionEnabled = true
        m_scrollview .addSubview(m_lbladdtoFav)
        
        let commentsCount = "\(m_entity.comments.count)" + " Comments"
        
        m_lblComments = self.createLabel(m_lbladdtoFav.frame.maxX, ypos: ypos, width: GRAPHICS.Screen_Width() - m_lbladdtoFav.frame.maxX - 10, height: height, textStr:commentsCount)
        m_lblComments.textAlignment = .Right
        m_lblComments.userInteractionEnabled = true
        m_scrollview .addSubview(m_lblComments)
        
        let favoriteTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductDetailsViewController.addToFavoritesLabelGesture(_:)))
        m_lbladdtoFav.addGestureRecognizer(favoriteTapGesture)
        
        let favoriteTapGestureComments = UITapGestureRecognizer(target: self, action: #selector(IBProductDetailsViewController.commentsLabelGesture(_:)))
        m_lblComments.addGestureRecognizer(favoriteTapGestureComments)
        
        
        ypos = m_lbladdtoFav.frame.maxY-1
        width = GRAPHICS.Screen_Width()-xpos*2

        m_ViwaddToFav = self.creationOfBottomLineView(xpos, ypos: ypos, width: width, height: bottomlineHeight)
        m_scrollview.addSubview(m_ViwaddToFav)
        
        //Creation of Descrption label
        
        ypos = m_ViwaddToFav.frame.maxY+Diff
        width = 180
        
        m_lblDescrption = self.createLabel(xpos, ypos: ypos, width: width, height: 30, textStr:Produts_descrption)
        m_scrollview .addSubview(m_lblDescrption)
        
        ypos = m_lblDescrption.frame.maxY
        width = GRAPHICS.Screen_Width()-xpos*2
        height = 50
        
        //let shippingTermsStr = decodeEncodedStringToNormalString(shippingStr)
        let detaildescrptionStr:String =  decodeEncodedStringToNormalString(m_entity.descriptionStr as String!)
        
      let  height1: CGFloat = heightForView(detaildescrptionStr, font: GRAPHICS.FONT_REGULAR(12)!, width: width)
        
        m_lbldetailDescprion = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr:detaildescrptionStr)
       // m_lbldetailDescprion.textColor = UIColor.blackColor()
        m_lbldetailDescprion.numberOfLines = 0
        m_lbldetailDescprion.lineBreakMode = NSLineBreakMode.ByWordWrapping
        m_lbldetailDescprion.frame.size.height = height1
        m_scrollview .addSubview(m_lbldetailDescprion)
        
        ypos = m_lbldetailDescprion.frame.maxY+10
        width = GRAPHICS.Screen_Width()-xpos*2
        
        m_viwdescDetail = self.creationOfBottomLineView(xpos, ypos: ypos, width: width, height: bottomlineHeight)
        m_scrollview.addSubview(m_viwdescDetail)
        
        //creation Of Seller & rating label
        
        ypos = m_viwdescDetail.frame.maxY+Diff
        width = 270
        height = 35
        
//        var sellerStr:String = String()
//        let str:String = m_entity.sellername as String
//        let str2:String = Products_Seller
        let sellerStr = String(format:"%@ %@",Products_Seller,m_entity.sellername)
        let sellerNameStr = m_entity.sellername as String
        let attributedText = NSMutableAttributedString(string : sellerStr)
        attributedText.addAttribute(NSFontAttributeName, value: GRAPHICS.FONT_BOLD(12)!, range:NSMakeRange(sellerStr.characters.count - sellerNameStr.characters.count , m_entity.sellername.length))
            //NSMakeRange(Products_Seller.length + 1 ,m_entity.sellername.length))
        m_lblseller = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr:"")
        m_lblseller.attributedText = attributedText
        m_scrollview .addSubview(m_lblseller)
        
        ypos = m_lblseller.frame.maxY
        width = 50
        
        m_lblrating = self.createLabel(xpos, ypos: ypos, width: width, height: 20, textStr:Products_rating)
        m_lblrating.hidden = true
        m_scrollview .addSubview(m_lblrating)
        
        self.creationOfRatingImages(m_lblrating.frame.maxX, ypos: m_lblrating.frame.origin.y+5, width: 0, height: 0)
        
        ypos = m_lblrating.frame.maxY+10
        width = GRAPHICS.Screen_Width()-xpos*2
        
//        m_viwrating = self.creationOfBottomLineView(xpos, ypos: ypos, width: width, height: bottomlineHeight)
//        m_scrollview.addSubview(m_viwrating)postma
        
        ypos = m_lblseller.frame.maxY + 10
        width = GRAPHICS.Screen_Width()/2
        let locationStr = String(format: "%@: %@",location,m_entity.address)
        m_locationLbl = self.createLabel(xpos, ypos: ypos, width: width, height: 20, textStr:"")
        m_locationLbl.attributedText = creationOfAttributeLabelWithTitleStr(locationStr)
        m_scrollview .addSubview(m_locationLbl)
        m_locationLbl.sizeToFit()
        
        ypos = m_locationLbl.frame.maxY+10
        width = GRAPHICS.Screen_Width()-xpos*2
        
        let locBtmLine = self.creationOfBottomLineView(xpos, ypos: ypos, width: width, height: bottomlineHeight)
        m_scrollview.addSubview(locBtmLine)

        ypos = locBtmLine.frame.maxY + 10
        width = GRAPHICS.Screen_Width()/2
        let publicCommentsLbl = self.createLabel(xpos, ypos: ypos, width: width, height: 20, textStr:"Public comments")
        m_scrollview .addSubview(publicCommentsLbl)
        
        ypos = publicCommentsLbl.frame.maxY + 10
        width = GRAPHICS.Screen_Width()-100
        let commentBtn = UIButton(frame: CGRectMake(xpos,ypos,width,25))
        commentBtn.setTitle(ProductDetails_ViewComments, forState: .Normal)
        commentBtn.setTitleColor(headerColor, forState: .Normal)
        commentBtn.titleLabel?.font = GRAPHICS.FONT_BOLD(12)
        commentBtn.addTarget(self, action: #selector(IBProductDetailsViewController.commentbuttonClicked(_:)), forControlEvents: .TouchUpInside)
        commentBtn.contentHorizontalAlignment = .Left
        m_scrollview .addSubview(commentBtn)
        if m_entity.comments.count > 0
        {
            publicCommentsLbl.hidden = false
        }
        else
        {
            publicCommentsLbl.hidden = true
        }
        if m_entity.comments.count > 4
        {
            commentBtn.hidden = false
            publicCommentsLbl.hidden = false
            ypos = commentBtn.frame.maxY+10
        }
        else
        {
            commentBtn.hidden = true
            ypos = publicCommentsLbl.frame.maxY+10
           // publicCommentsLbl.hidden = true
        }
        

       // ypos = commentBtn.frame.maxY
        width = GRAPHICS.Screen_Width() - 2*xpos
        
        
        let sortedArray = m_entity.comments.sortedArrayUsingComparator {
            (obj1, obj2) -> NSComparisonResult in
            
            let p1 = obj1 as! NSDictionary
            let p2 = obj2 as! NSDictionary
            let result = p2.objectForKey("added_date")!.compare(p1.objectForKey("added_date") as! String)
            return result
        }
        productCommentsArray.addObjectsFromArray(sortedArray)
        
        
        
        let rowHeight = heightForView("divabs badis;b bi;auds ", font: GRAPHICS.FONT_REGULAR(14)!, width: GRAPHICS.Screen_Width() - imgHeight - 30) + imgHeight
        if productCommentsArray.count > 4
        {
            height = CGFloat(4) * rowHeight
        }
        else{
            height = CGFloat(m_entity.comments.count) * rowHeight
        }
        commentsTblView.frame = CGRectMake(0, ypos, GRAPHICS.Screen_Width(), height)
        commentsTblView.delegate = self
        commentsTblView.dataSource = self
        commentsTblView.separatorStyle = .None
        commentsTblView.scrollEnabled = false
        m_scrollview.addSubview(commentsTblView)
        
        let imgSize:CGSize = self .getImageWidthHeight(GRAPHICS.PRODUCT_BUYNOW_IMAGE())
        xpos = GRAPHICS.Screen_X() + 15//(GRAPHICS.Screen_Width()-imgSize.width)/2
        //Creation Of BuyNow Button
        //ypos = commentBtn.frame.maxY+20
        if productCommentsArray.count > 0
        {
            ypos = commentsTblView.frame.maxY + 30
        }
        else{
            ypos = m_locationLbl.frame.maxY + 30
        }

        xpos = (GRAPHICS.Screen_Width() - (2 * imgSize.width) - 10)/2
        m_buyNowBtn.frame = CGRectMake(xpos, ypos, imgSize.width, imgSize.height)
        m_buyNowBtn .setTitle("Buy Now", forState: .Normal)
        m_buyNowBtn.addTarget(self, action: #selector(IBProductDetailsViewController.buyNowbuttonClicked(_:)), forControlEvents: .TouchUpInside)
        m_buyNowBtn .setBackgroundImage(GRAPHICS.PRODUCT_BUYNOW_IMAGE(), forState: .Normal)
        m_buyNowBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        m_scrollview .addSubview(m_buyNowBtn)
        
        xpos = m_buyNowBtn.frame.maxX + 10
        chatButton.frame = CGRectMake(xpos, ypos, imgSize.width, imgSize.height)
        chatButton .setTitle("Chat", forState: .Normal)
        chatButton.addTarget(self, action: #selector(IBProductDetailsViewController.chatbuttonClicked(_:)), forControlEvents: .TouchUpInside)
        chatButton .setBackgroundImage(GRAPHICS.PRODUCT_BUYNOW_IMAGE(), forState: .Normal)
        chatButton.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        m_scrollview .addSubview(chatButton)
        
        
        m_scrollview.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), m_buyNowBtn.frame.maxY+20)
        
        
        
    }
    // comment post action
    func commentsLabelGesture(tapGesture:UITapGestureRecognizer)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let commentScreen = IBCommentsViewController()
            commentScreen.m_wallIdStr = m_entity.wall_id as String//resultDict.objectForKey("wall_id") as! String
            self.navigationController!.pushViewController(commentScreen, animated: true)
        }
        else{
            showAlertViewWithMessage(NoInternetConnection)
        }
        
        //        let mydiarycalendar = IBAddProductViewController()
        //        self.navigationController?.pushViewController(mydiarycalendar, animated: true)
        
    }
    func addToFavoritesLabelGesture(tapGesture:UITapGestureRecognizer) -> () {
        
        if m_entity.sellerId == getUserIdFromUserDefaults()
        {
            showAlertViewWithMessage("It's your own product.")
        }
        else {
        SwiftLoader.show(title: Loading, animated: true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_addWish(getUserIdFromUserDefaults(), productId: productId)
        }
        
    }
    func chatbuttonClicked(sender:UIButton) -> () {
        if m_entity.sellerId == getUserIdFromUserDefaults()
        {
            showAlertViewWithMessage("It's your own product.")
        }
        else {
        let messageVc = IBMessageViewController()
        messageVc.nameStr = m_entity.sellername as String
        messageVc.toUserId = m_entity.sellerId as String
        messageVc.isForProductDiscussion = true
        messageVc.productId = m_entity.productId as String
        self.navigationController?.pushViewController(messageVc, animated: true)
        }
    }
    
    func buyNowbuttonClicked(sender:UIButton)->()
    {
        if m_entity.stockavailability == "0"
        {
            showAlertViewWithMessage("Product sold already")
        }
        else{
            if m_entity.sellerId == getUserIdFromUserDefaults()
            {
                showAlertViewWithMessage("You can't buy your own product")
            }
            else{
                SwiftLoader.show(title: "Loading...", animated: true)
                
                let userIdStr = getUserIdFromUserDefaults()
                
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_addProductToCart(userIdStr, productId: m_entity.productId as String, sellerId: m_entity.sellerId as String)
                
            }
        }
    }
    func  commentbuttonClicked(sender:UIButton) -> () {
        if commentBtnIndex == 0
        {
            sender.setTitle(ProductDetails_HideComments, forState: .Normal)
            commentBtnIndex = 1
            var Height = CGFloat()
            for i in 0 ..< productCommentsArray.count
            {
                let commentDict = productCommentsArray.objectAtIndex(i) as! NSDictionary
                let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
                let width = profileMaskImg.size.width/1.5
                Height = heightForView(commentDict.objectForKey("comment") as! String, font: GRAPHICS.FONT_REGULAR(14)!, width: GRAPHICS.Screen_Width() - width - 20) + imgHeight;
            }
//         commentsTblView.frame.size.height = CGFloat(productCommentsArray.count) * Height
//            m_buyNowBtn.frame.origin.y = commentsTblView.frame.maxY
//            chatButton.frame.origin.y = commentsTblView.frame.maxY
//            m_scrollview.contentSize = CGSizeMake(m_scrollview.frame.size.width, m_buyNowBtn.frame.maxY + 20)
            
        }
        else
        {
            sender.setTitle(ProductDetails_ViewComments, forState: .Normal)
            commentBtnIndex = 0
            var Height = CGFloat()
            for i in 0 ..< 4
            {
                let commentDict = productCommentsArray.objectAtIndex(i) as! NSDictionary
                let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
                let width = profileMaskImg.size.width/1.5
                Height = heightForView(commentDict.objectForKey("comment") as! String, font: GRAPHICS.FONT_REGULAR(12)!, width: GRAPHICS.Screen_Width() - imgHeight - 30) + imgHeight;
            }
//            commentsTblView.frame.size.height = CGFloat(4) * Height
//            m_buyNowBtn.frame.origin.y = commentsTblView.frame.maxY
//            chatButton.frame.origin.y = commentsTblView.frame.maxY
//            m_scrollview.contentSize = CGSizeMake(m_scrollview.frame.size.width, m_buyNowBtn.frame.maxY + 20)
        }
        commentsTblView.reloadData()
    }
    
    
     func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    func shareButtonClicked(sender:UIButton)
    {
         NSLog("share button clikced")
   
        let textToShare = "InotoBuy Prodtc sharing iamges "
                
                if let myWebsite = NSURL(string: "http://www.IntoBuy.com/")
                {
                    let objectsToShare = [textToShare, myWebsite]
                    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                    
                    self.presentViewController(activityVC, animated: true, completion: nil)
                }
        
    }
    func creationOfRatingImages(xpos:CGFloat,ypos:CGFloat,width:CGFloat,height:CGFloat)->()
    {
        let ratingImage = GRAPHICS.PROFILE_RATING_ICON_IMAGE()
        
        var m_ratingImagesArray = [UIImage]()
        
        m_ratingImagesArray = [ratingImage, ratingImage,ratingImage,ratingImage,ratingImage]
        
        var xposition:CGFloat = xpos
        
        let ratingImgSize:CGSize = self.getImageWidthHeight(GRAPHICS.PROFILE_RATING_ICON_IMAGE())
        let ratingNum = NSNumberFormatter().numberFromString(m_entity.rating as String)
        let ratingInt = ratingNum!.integerValue
        
        for i in 0 ..< ratingInt
        {
            let m_imageView1:UIImageView = UIImageView()
            m_imageView1.frame = CGRectMake(xposition, ypos, ratingImgSize.width, ratingImgSize.height)
            m_imageView1.image = m_ratingImagesArray[i] as UIImage
            m_imageView1.backgroundColor = UIColor.clearColor()
            m_scrollview .addSubview(m_imageView1)
            
            xposition = m_imageView1.frame.maxX + 5
            
        }
        
    }

    
    func creationOfScrollviewIamges(xpos:CGFloat,ypos:CGFloat,width:CGFloat,height:CGFloat)->()
    {
       // let productImage = UIImage(named:"iphone5_ios7.png")!
        
       // let productImage = GRAPHICS.PROFILE_RATING_ICON_IMAGE()
        
        m_productiamgesArray = m_entity.images //[productImage, productImage,productImage,productImage,productImage]
//        float imageXPadding =[GRAPHICS SCREEN_WIDTH]/4;
//        float imageWidth =[GRAPHICS SCREEN_WIDTH]/2;
//        float imageXPadding =[GRAPHICS SCREEN_WIDTH]/4;
//        float imageWidth =[GRAPHICS SCREEN_WIDTH]/2;

        
        var xposition:CGFloat = xpos
        
        let imageXPadding = CGFloat(15);
        xposition = (GRAPHICS.Screen_Width() - width)/2//imageXPadding + (i*((imageXPadding/2)+imageWidth))
        
        if m_productiamgesArray.count == 0
        {
            let imageUrl = m_entity.mainImage as String
            let m_imageView1:UIImageView = UIImageView()
            m_imageView1.frame = CGRectMake(xposition, ypos, width, height)
            m_imageView1.tag = 10
            /*let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) -> Void in
                
            }*/
            
            m_imageView1.contentMode = .ScaleAspectFit
            //m_imageView1.sd_setImageWithURL(NSURL(string: imageUrl)!, placeholderImage:GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completed: block)
            m_imageView1.sd_setImageWithURL(NSURL(string: imageUrl)!, placeholderImage: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), options: .ScaleDownLargeImages)
           // m_imageView1.load(imageUrl, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            m_imageScrollview .addSubview(m_imageView1)
            m_imageView1.userInteractionEnabled = true
            
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductDetailsViewController.imageTapGesture(_:)))
            m_imageView1.addGestureRecognizer(tapGesture)

        }
        
        for i in 0 ..< m_productiamgesArray.count
        {
            //xposition = imageXPadding + (CGFloat(i)*((imageXPadding/2)+width))
            let imageDict = m_productiamgesArray[i] as! NSDictionary
            let imageUrl = imageDict.objectForKey("image") as! String
            let m_imageView1:UIImageView = UIImageView()
            m_imageView1.frame = CGRectMake(xposition, ypos, width, height)
            m_imageView1.tag = i + 10
            m_imageView1.load(imageUrl, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            m_imageView1.contentMode = .ScaleAspectFit
            m_imageScrollview .addSubview(m_imageView1)
            m_imageView1.userInteractionEnabled = true
            xposition = m_imageView1.frame.maxX + 10
            
            m_imageScrollview.contentSize = CGSizeMake(m_imageView1.frame.maxX + 10, height)
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductDetailsViewController.imageTapGesture(_:)))
            m_imageView1.addGestureRecognizer(tapGesture)
            
        }
    }
    
    func imageTapGesture(tapGesture : UITapGestureRecognizer)
    {
        //print((tapGesture.view?.tag)!)
        let imageView = m_imageScrollview.viewWithTag((tapGesture.view?.tag)!) as! UIImageView
        let imgDict = m_productiamgesArray.objectAtIndex((tapGesture.view?.tag)! - 10) as! NSDictionary
        let imgStr = imgDict.objectForKey("image") as! String
        let imgUrl = NSURL(string: imgStr)
        displayImage(imageView, imgUrl: imgUrl!)
    }
    
    func getImageWidthHeight(imageName:UIImage)->CGSize
    {
        var fWidth:CGFloat = 0.0
        var fHeight:CGFloat = 0.0
        
        let searchiew:UIImage = imageName
        fWidth = searchiew.size.width/2
        fHeight = searchiew.size.height/2
        
        if GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus
        {
            fWidth = searchiew.size.width/1.5
            fHeight = searchiew.size.height/1.5
            
        }
        
        return CGSizeMake(fWidth, fHeight)
        
    }
    
    //Creation oF Product labels
    func  createLabel(xpos:CGFloat,ypos: CGFloat,width: CGFloat,height:CGFloat,textStr : String)-> UILabel
    {
        let lbl : UILabel = UILabel()
        lbl.frame = CGRectMake(xpos, ypos, width, height)
        lbl.backgroundColor = UIColor.clearColor()
        lbl.text = textStr
        lbl.textColor = UIColor.darkGrayColor()
        lbl.font = GRAPHICS.FONT_REGULAR(12)
        return lbl
        
    }
    
    //creation Of BottomLines
    func creationOfBottomLineView(xpos: CGFloat,ypos: CGFloat,width : CGFloat,height:CGFloat)->UIView
    {
        let viw : UIView = UIView()
        viw.frame = CGRectMake(xpos, ypos, width, height)
        viw.backgroundColor = UIColor.lightGrayColor()
        return viw
     }
    // MARK: - tableview delegates
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if commentBtnIndex == 0
        {
            if productCommentsArray.count > 2
            {
                return 4
            }
            else
            {
                return productCommentsArray.count
            }
            
        }
        else{
                return productCommentsArray.count
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
       
        let commentDict = productCommentsArray.objectAtIndex(indexPath.row) as! NSDictionary
        let commentText = decodeEncodedStringToNormalString(commentDict.objectForKey("comment") as! String)
        let height = heightForView(commentText, font: GRAPHICS.FONT_REGULAR(12)!, width: GRAPHICS.Screen_Width() - imgHeight - 30) + imgHeight;
        return height
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cellIdentifier = "cell"
        var cell:IBProductDetailsCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBProductDetailsCell
        
        if (cell == nil) {
            cell = IBProductDetailsCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        //print(indexPath.row)
        let commentDict = productCommentsArray.objectAtIndex(indexPath.row) as! NSDictionary
        let commentText = decodeEncodedStringToNormalString(commentDict.objectForKey("comment") as! String)
        cell?.commentLabel.frame.size.height = self.heightForView(commentText, font: GRAPHICS.FONT_REGULAR(12)!, width: GRAPHICS.Screen_Width() - cell!.profileImgView.frame.size.width - 30);
        cell?.timeLabel.frame.origin.y = (cell?.commentLabel.frame.maxY)!
        cell?.frame.size.height = (cell?.timeLabel.frame.maxY)! + 10
        
        cell?.setValuesToTheControls(commentDict)
        
        if commentBtnIndex == 0
        {
            commentsTblView.frame.size.height = commentsTblView.contentSize.height// (cell?.frame.maxY)!
            //commentsTblView.frame.size.height = max(commentsTblView.contentSize.height + (cell?.frame.size.height)!, commentsTblView.contentSize.height);
            m_buyNowBtn.frame.origin.y = commentsTblView.frame.maxY + 20
            chatButton.frame.origin.y = commentsTblView.frame.maxY + 20
            m_scrollview.contentSize = CGSizeMake(m_scrollview.frame.size.width, m_buyNowBtn.frame.maxY + 20)
        }
        else{
            commentsTblView.frame.size.height = commentsTblView.contentSize.height//(cell?.frame.maxY)!
           // commentsTblView.frame.size.height = max(commentsTblView.contentSize.height + (cell?.frame.size.height)!, commentsTblView.contentSize.height);
            m_buyNowBtn.frame.origin.y = commentsTblView.frame.maxY + 20
            chatButton.frame.origin.y = commentsTblView.frame.maxY + 20
            m_scrollview.contentSize = CGSizeMake(m_scrollview.frame.size.width, m_buyNowBtn.frame.maxY + 20)
        }
        
        
        cell?.profileImgView.userInteractionEnabled = true
        cell?.profileImgView.tag = indexPath.row
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(IBProductDetailsViewController.profileImageClicked(_:)))
        cell?.profileImgView.addGestureRecognizer(tapgesture)
        
        cell?.selectionStyle = .None
        
        //cell?.m_dateLbl.sizeToFit()
        return cell!

    }
    
    func profileImageClicked(recognise:UIGestureRecognizer)
    {
        let entity = IBProductDetailsEntity(dict: productCommentsArray.objectAtIndex((recognise.view?.tag)!) as! NSDictionary)
        let myPageVC = IBMyPageViewController()
        myPageVC.m_isFromCameraBtn = false
        myPageVC.otherUserId = (entity.userId as String)
        self.navigationController?.pushViewController(myPageVC, animated: true)
        
    }
    
    // MARK: - API delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    func API_CALLBACK_ProductDetail(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if(errorCode == "1")
        {
            let resultArray = resultDict.objectForKey("result")as! NSArray
            if resultArray.count > 0
            {
                let resultDictionary = resultArray.objectAtIndex(0) as! NSDictionary
                saveProductDetailsToUserDefaults(resultArray)
                m_entity = IBProductDetailsEntity.init(dict: resultDictionary)
                if m_entity.sellerId != getUserIdFromUserDefaults()
                {
                    self.m_titleLabel.text = String(format: "%@ 's Products",m_entity.sellername)
                }
                self.initControols()
            }
        }
    }
    func API_CALLBACK_addProductToCart(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            showAlertViewWithMessage((resultDict.objectForKey("result")as? String!)!)
        }
        else
        {
            showAlertViewWithMessage((resultDict.objectForKey("result")as? String!)!)
        }
        let shippingDetailsVC = IBShippingDetailsViewController()
        shippingDetailsVC.isFromProducts = true
        shippingDetailsVC.productIdStr = m_entity.productId as String
        shippingDetailsVC.sellerIdStr = m_entity.sellerId as String
        self.navigationController?.pushViewController(shippingDetailsVC, animated: true)

    }
    
    func API_CALLBACK_addWish(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        if resultDict.objectForKey("error_code") as? String == "1"
        {
            showAlertViewWithMessage((resultDict.objectForKey("result") as? String)!)
        }
        else
        {
            showAlertViewWithMessage((resultDict.objectForKey("result") as? String)!)
        }
    }


    //MARK:- scrollview delegates
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if self.lastContentOffset > Float(scrollView.contentOffset.x)
        {
            scrollLeft = false;
        }
        else if self.lastContentOffset < Float(scrollView.contentOffset.x)
        {
            scrollLeft = true;
        }
        self.lastContentOffset = Float(scrollView.contentOffset.x);
        currentIndex = Int(Float(scrollView.contentOffset.x)/Float(scrollView.frame.size.width));
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let divider1 = GRAPHICS.Screen_Width()/4
        let divider2 = GRAPHICS.Screen_Width()/2
        
        if scrollLeft
        {
            let pageIndex = m_imageScrollview.contentOffset.x - divider1
            let tempFloat = pageIndex/(GRAPHICS.Screen_Width() * CGFloat(m_productiamgesArray.count) / CGFloat(m_productiamgesArray.count) - 1)
            currentIndex = Int(ceil(tempFloat))
        }
        else
        {
            let pageIndex = m_imageScrollview.contentOffset.x ;//- divider2
            let tempFloat = pageIndex/(GRAPHICS.Screen_Width() * CGFloat(m_productiamgesArray.count) / CGFloat(m_productiamgesArray.count) - 1)
            currentIndex = Int(ceil(tempFloat))
            
            if (m_imageScrollview.contentOffset.x + m_imageScrollview.frame.width + 150) >= m_imageScrollview.contentSize.width {
                
                currentIndex = pageControl.numberOfPages - 1
            }
        }
        //print(currentIndex)
        pageControl.currentPage = currentIndex
        
        let targetPoint = CGPointMake((GRAPHICS.Screen_Width()/1.5 * CGFloat(currentIndex) ), 0);
        
        m_imageScrollview.setContentOffset(targetPoint, animated: false)
    }
//    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        let divider1 = GRAPHICS.Screen_Width()/4
//        let divider2 = GRAPHICS.Screen_Width()/2
//        if scrollLeft
//        {
//            let pageIndex = m_imageScrollview.contentOffset.x - divider1
//            let tempFloat = pageIndex/(GRAPHICS.Screen_Width() * 3 / 4)
//            currentIndex = Int(ceil(tempFloat))
//        }
//        else
//        {
//            let pageIndex = m_imageScrollview.contentOffset.x - divider2
//            let tempFloat = pageIndex/(GRAPHICS.Screen_Width() * 3 / 4)
//            currentIndex = Int(ceil(tempFloat))
//        }
//        //print(currentIndex)
//        let targetPoint = CGPointMake((GRAPHICS.Screen_Width() * CGFloat(currentIndex) * 5 / 4), 0);
//        m_imageScrollview.setContentOffset(targetPoint, animated: true)
//    }
    func creationOfAttributeLabelWithTitleStr(titleStr:String) -> NSMutableAttributedString
    {
        let str : String = titleStr
        
        let strArray:NSArray = str.componentsSeparatedByString(":")
        let firstStr = strArray.objectAtIndex(1)
        
        let attributedStr : NSMutableAttributedString = NSMutableAttributedString(string: str)
        attributedStr.addAttribute(NSForegroundColorAttributeName, value: headerColor, range: NSMakeRange(titleStr.characters.count - firstStr.length,firstStr.length))
        
        
        return attributedStr
        
    }
    
    func displayImage(imageView : UIImageView,  imgUrl:NSURL)
    {
       // imageView.setImageWithURL(imgUrl, placeholderImage: GRAPHICS.DEFAULT_CART_IMAGE())
        imageView.contentMode = .ScaleAspectFit
        imageView.setupImageViewerWithImageURL(imgUrl)
//        imageView.clipsToBounds = true
//        self.view.bringSubviewToFront(imageView)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
