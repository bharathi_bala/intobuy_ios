//
//  IBContactsCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 19/04/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

public protocol ContactDelegate : NSObjectProtocol
{
    func tapOnProfileImage(tapGesture : UITapGestureRecognizer)
}


class IBContactsCell: UITableViewCell {
    
    var profileImgView = UIImageView()
    var nameLabel = UILabel()
    var msgBtn = UIButton()
    var diaryBtn = UIButton()
    var contactProtocol:ContactDelegate?
    //var followBtn = UIButton()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsForContacts()
    }
    func createControlsForContacts()
    {
        self.frame.size.height = 60.0
        self.frame.size.width = GRAPHICS.Screen_Width()
        let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
        
        
        var viewWid = profileMaskImg.size.width/1.7
        var viewHei = profileMaskImg.size.height/1.7
        var posX = self.bounds.origin.x + 15
        var posY = (self.frame.size.height - viewHei)/2
        
        profileImgView.frame = CGRectMake(posX,posY,viewWid,viewHei)
        profileImgView.layer.cornerRadius = viewHei/2
        profileImgView.clipsToBounds = true
        profileImgView.userInteractionEnabled = true
        self.addSubview(profileImgView)
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IBContactsCell.profileImgTapGesture(_:)))
        profileImgView.addGestureRecognizer(tapGesture)

        
        viewHei = 20
        viewWid = GRAPHICS.Screen_Width()/2.5
        posX = profileImgView.frame.maxX + 10
        posY = (self.frame.size.height - viewHei)/2
        let fontSize:CGFloat = 14;
        nameLabel.frame = CGRectMake(posX,posY,viewWid,viewHei)
        nameLabel.font = GRAPHICS.FONT_REGULAR(fontSize)
        nameLabel.text = "Manoj"
        self.addSubview(nameLabel)
        
        let msgBtnImg = GRAPHICS.CONTACTS_MESSAGE_BUTTON_IMAGE()
        viewHei = msgBtnImg.size.width/2
        viewWid = msgBtnImg.size.height/2
        posX = GRAPHICS.Screen_Width() - viewWid - 15
        posY = (self.frame.size.height - viewHei)/2
        msgBtn.frame = CGRectMake(posX,posY,viewWid,viewHei)
        msgBtn.setBackgroundImage(msgBtnImg, forState: .Normal)
        self.addSubview(msgBtn)
        
        
        let bookBtnImg = GRAPHICS.CONTACTS_CONTACTS_BOOK_BUTTON_IMAGE()
        viewHei = bookBtnImg.size.width/2
        viewWid = bookBtnImg.size.height/2
        posX = msgBtn.frame.origin.x - viewWid - 15
        posY = (self.frame.size.height - viewHei)/2
        diaryBtn.frame = CGRectMake(posX,posY,viewWid,viewHei)
        diaryBtn.setBackgroundImage(bookBtnImg, forState: .Normal)
        self.addSubview(diaryBtn)
        
        /*viewHei = 30
        viewWid = 30
        let followBtnImg = GRAPHICS.CONTACTS_ADD_CONTACTS_BUTTON_IMAGE()
        viewHei = followBtnImg.size.width/2
        viewWid = followBtnImg.size.height/2
        posX = diaryBtn.frame.origin.x - viewWid - 15
        posY = (self.frame.size.height - viewHei)/2
        followBtn.frame = CGRectMake(posX,posY,viewWid,viewHei)
        followBtn.setBackgroundImage(followBtnImg, forState: .Normal)
        self.addSubview(followBtn)*/

        
                let dividerImg = GRAPHICS.SEARCH_DIVIDER_IMAGE()
                viewWid = self.frame.size.width - 20//dividerImg.size.width/2;
                viewHei = dividerImg.size.height/2
                posX = (self.frame.size.width - viewWid)/2
                posY = (self.frame.size.height - viewHei)
                let dividerImgView = UIImageView(frame: CGRectMake(posX,posY,viewWid,viewHei))
                dividerImgView.image = dividerImg
                self.addSubview(dividerImgView)

        
        
    }
    /*func showOrHideFollowBtn(toHideBtn : Bool)
    {
        if toHideBtn
        {
            followBtn.hidden = true
        }
        else
        {
            followBtn.hidden = false
        }
    }*/
    
    func profileImgTapGesture(tapGesture : UITapGestureRecognizer) -> () {
        contactProtocol!.tapOnProfileImage(tapGesture)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    

}
