//
//  IBWishListEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 02/08/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBWishListEntity: NSObject {
    
    var userId:String!
    var productId:String!
    var sellerId:String!
    var productname:String!
    var price:String!
    var image:String!
    var average:String!
    var reviewcount:String!
    var username:String!
    var userpicture:String!
    var wishId:String!
    
    init(dict: NSDictionary) {
        self.userId = (GRAPHICS.checkKeyNotAvail(dict, key:"userId")as? String)!
        self.productId = (GRAPHICS.checkKeyNotAvail(dict, key:"productId")as? String)!
        self.sellerId = (GRAPHICS.checkKeyNotAvail(dict, key:"sellerId")as? String)!
        self.productname = (GRAPHICS.checkKeyNotAvail(dict, key:"productname")as? String)!
        self.price = (GRAPHICS.checkKeyNotAvail(dict, key:"price")as? String)!
        self.image = (GRAPHICS.checkKeyNotAvail(dict, key:"image")as? String)!
        self.average = (GRAPHICS.checkKeyNotAvail(dict, key:"average")as? String)!
        self.reviewcount = (GRAPHICS.checkKeyNotAvail(dict, key:"reviewcount")as? String)!
        self.username = (GRAPHICS.checkKeyNotAvail(dict, key:"username")as? String)!
        self.userpicture = (GRAPHICS.checkKeyNotAvail(dict, key:"userpicture")as? String)!
        self.wishId = (GRAPHICS.checkKeyNotAvail(dict, key:"wishId")as? String)!
    }
    
}
