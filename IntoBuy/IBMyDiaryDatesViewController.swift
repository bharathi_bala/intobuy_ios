//
//  IBMyDiaryDatesViewController.swift
//  IntoBuy
//
//  Created by SmaatApps on 12/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBMyDiaryDatesViewController: IBBaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    var m_profileView:UIView = UIView()
    var m_nameLabel:UILabel = UILabel()
    var m_countryLabel:UILabel = UILabel()
    var m_descrptionlabel:UILabel = UILabel()
    var m_profileImageView:UIImageView = UIImageView()
    var m_tableview:UITableView = UITableView()


    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        
        m_titleLabel.text = MyDiary
        
        self.initControls()
        
       m_tableview.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")

        // Do any additional setup after loading the view.
    }
    
    func initControls()
    {
        //
        m_profileView.frame = CGRectMake(0, 0, GRAPHICS .Screen_Width(), 100)
        m_profileView.backgroundColor = UIColor.grayColor()
        m_bgImageView .addSubview(m_profileView)
        
        m_profileImageView.frame = CGRectMake(15, 10, 80, 80)
        m_profileImageView.backgroundColor = UIColor.redColor()
        m_profileImageView.layer.cornerRadius = 80/2;
        m_profileImageView.clipsToBounds = true
        m_profileView .addSubview(m_profileImageView)
        
        let xpos:CGFloat = m_profileImageView.frame.maxX+10
        var ypos:CGFloat = m_profileImageView.frame.origin.y+10
        let width:CGFloat = GRAPHICS.Screen_Width()-30-m_profileImageView.frame.size.width
        let height:CGFloat = 16
        
        m_nameLabel.frame = CGRectMake(xpos, ypos, width, height)
        m_nameLabel.font = GRAPHICS.FONT_REGULAR(12)
        m_nameLabel.textAlignment = NSTextAlignment.Left
        m_nameLabel.text = "JyothiReddy"
        m_nameLabel.textColor = UIColor.whiteColor()
        m_profileView.addSubview(m_nameLabel)
        
        ypos = m_nameLabel.frame.maxY+3
        
        m_countryLabel.frame = CGRectMake(xpos, ypos, width, height)
        m_countryLabel.font = GRAPHICS.FONT_REGULAR(12)
        m_countryLabel.textAlignment = NSTextAlignment.Left
        m_countryLabel.text = "Malisia"
        m_countryLabel.textColor = UIColor.whiteColor()
        m_profileView.addSubview(m_countryLabel)
        
        ypos = m_countryLabel.frame.maxY+5
        
        m_descrptionlabel.frame = CGRectMake(xpos, ypos, width, height)
        m_descrptionlabel.font = GRAPHICS.FONT_REGULAR(12)
        m_descrptionlabel.textAlignment = NSTextAlignment.Left
        m_descrptionlabel.text = "Malisia is the best"
        m_descrptionlabel.textColor = UIColor.whiteColor()
        m_profileView.addSubview(m_descrptionlabel)
        
        self.createTableView(m_profileView.frame.maxY)
        
    }
    
    func createTableView(ypos:CGFloat)
    {
        m_tableview.frame = CGRectMake(0, ypos, GRAPHICS.Screen_Width(), GRAPHICS.Screen_Height()-ypos-10)
        m_tableview.backgroundColor = UIColor.clearColor()
        m_tableview.dataSource = self
        m_tableview.delegate = self
        m_bgImageView .addSubview(m_tableview)
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        
        cell.textLabel?.text = "14 nov"
        cell.textLabel?.font = GRAPHICS.FONT_REGULAR(12)
        return cell;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
