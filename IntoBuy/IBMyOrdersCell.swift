//
//  IBMyOrdersCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 28/01/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBMyOrdersCell: UITableViewCell {
    
    var m_productImgView:UIImageView!
    var m_productNameLbl:UILabel!
    var m_transDateLbl:UILabel!
    var m_orderNoLbl:UILabel!
    var m_transIdLbl:UILabel!
    var m_quantityLbl:UILabel!
    var m_priceLbl:UILabel!
    var m_statusBtn:UIButton!
    var dividerImgView:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControls()
    }
    
    func createControls()
    {
        self.frame.size.width = GRAPHICS.Screen_Width()
        
        let bgView = UIView(frame: CGRectMake(0,0,self.frame.size.width,50));
        self.addSubview(bgView)
        
        var xPos = GRAPHICS.Screen_X() + 10
        var yPos = self.bounds.origin.y + 10
        var width:CGFloat = 80
        var height:CGFloat = 80
        
        m_productImgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        bgView.addSubview(m_productImgView)
        
        xPos = m_productImgView.frame.maxX + 10
        width = self.frame.size.width/1.5
        height = height/6
        m_productNameLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_productNameLbl.font = GRAPHICS.FONT_BOLD(12)
        bgView.addSubview(m_productNameLbl)
        
        width = self.frame.size.width/2
        yPos = m_productNameLbl.frame.maxY
        m_transDateLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_transDateLbl.font = GRAPHICS.FONT_REGULAR(10)
        self.addSubview(m_transDateLbl)
        
        yPos = m_transDateLbl.frame.maxY
        m_orderNoLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_orderNoLbl.font = GRAPHICS.FONT_REGULAR(10)
        bgView.addSubview(m_orderNoLbl)
        
        yPos = m_orderNoLbl.frame.maxY
        width = self.frame.size.width/1.5
        m_transIdLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_transIdLbl.font = GRAPHICS.FONT_REGULAR(10)
        bgView.addSubview(m_transIdLbl)
        
        yPos = m_transIdLbl.frame.maxY
        m_quantityLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_quantityLbl.font = GRAPHICS.FONT_REGULAR(10)
        bgView.addSubview(m_quantityLbl)

        yPos = m_quantityLbl.frame.maxY
        m_priceLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_priceLbl.font = GRAPHICS.FONT_REGULAR(10)
        bgView.addSubview(m_priceLbl)
        
        let soldImage = GRAPHICS.ORDER_DETAILS_SOLD_IMAGE()
        let boughtImage = GRAPHICS.ORDER_DETAILS_BOUGHT_IMAGE()
        width = soldImage.size.width/2
        height = soldImage.size.height/2
        xPos = self.frame.size.width - width - 10
        
        m_statusBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        m_statusBtn.setBackgroundImage(soldImage, forState: .Selected)
        m_statusBtn.setBackgroundImage(boughtImage, forState: .Normal)
        m_statusBtn.addTarget(self, action: #selector(IBMyOrdersCell.statusBtnAction(_:)), forControlEvents: .TouchUpInside)
        bgView.addSubview(m_statusBtn)
        
        bgView.frame.size.height = m_statusBtn.frame.maxY + 10
        self.frame.size.height = bgView.frame.maxY
        //print(self.frame.size.height)
        
        let dividerImg = GRAPHICS.SEARCH_DIVIDER_IMAGE()
        width = GRAPHICS.Screen_Width() - 20;
        height = dividerImg.size.height/2
        xPos = GRAPHICS.Screen_X() + 10
        yPos = (bgView.frame.size.height - height)
        dividerImgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        dividerImgView.image = dividerImg
        bgView.addSubview(dividerImgView)
        
        

    }
    func setTitlesToAllLabels(productString : String, dateString : String, orderNoStr : String , transIdStr : String, quantityStr : String, priceStr: String ,buttonStatus:String , productImage:String)
    {
        m_productNameLbl.text = String(format: "%@",productString)
        //m_productNameLbl.sizeToFit()
        
        m_transDateLbl.text = String(format: "%@:%@",TransactionDate,dateString)
        m_transDateLbl.sizeToFit()
        
        m_orderNoLbl.text = String(format: "%@:%@",orderNo,orderNoStr)
        m_orderNoLbl.sizeToFit()
        
        m_transIdLbl.text = String(format: "%@:%@",transactionId,transIdStr)
        m_transIdLbl.adjustsFontSizeToFitWidth = true

        m_quantityLbl.text = String(format: "%@:%@",Quantity,quantityStr)
        m_quantityLbl.sizeToFit()
        
        m_priceLbl.text = String(format: "%@:$%@",price,priceStr)
        m_priceLbl.sizeToFit()
        
        if buttonStatus == "Bought"
        {
            m_statusBtn.selected = false
        }
        else{
            m_statusBtn.selected = true
        }

        m_productImgView.load(NSURL(string: productImage)!, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
        
    }
    func statusBtnAction(sender : UIButton)
    {
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
