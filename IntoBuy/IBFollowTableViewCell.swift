//
//  IBFollowTableViewCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 02/06/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

public protocol FollowProtocol : NSObjectProtocol
{
    func tapOnProfileImage(tapGesture : UITapGestureRecognizer)
}


class IBFollowTableViewCell: UITableViewCell {
    
    var m_profileImgView = UIImageView()
    var m_nameLabel = UILabel()
    var m_trophyLbl = UILabel()
    var m_followBtn = UIButton()
    var isFromHome = Bool()
    var followProtocol:FollowProtocol!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createControls()
    }
    
    func createControls() -> () {
        self.frame.size.height = 70.0
        let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
        
        var viewWid = profileMaskImg.size.width/1.7
        var viewHei = profileMaskImg.size.height/1.7
        var posX = self.bounds.origin.x + 15
        var posY = (self.frame.size.height - viewHei)/2
        
        m_profileImgView.frame = CGRectMake(posX,posY,viewWid,viewHei)
        m_profileImgView.image = profileMaskImg
        m_profileImgView.userInteractionEnabled = true
        self.addSubview(m_profileImgView)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IBSearchTableViewCell.profileImgTapGesture(_:)))
        m_profileImgView.addGestureRecognizer(tapGesture)

        
        viewHei = 20
        viewWid = GRAPHICS.Screen_Width()/2.5
        posX = m_profileImgView.frame.maxX + 10
        let fontSize:CGFloat = 14;
        m_nameLabel.frame = CGRectMake(posX,posY,viewWid,viewHei)
        m_nameLabel.font = GRAPHICS.FONT_REGULAR(fontSize)
        self.addSubview(m_nameLabel)
        
        posY = m_nameLabel.frame.maxY
        m_trophyLbl.frame = CGRectMake(posX,posY,viewWid,viewHei)
        m_trophyLbl.font = GRAPHICS.FONT_REGULAR(fontSize)
        self.addSubview(m_trophyLbl)
        
        let grayImg = GRAPHICS.FOLLOWING_FOLLOW_BTN_IMAGE()
        let redImg = GRAPHICS.APP_PINK_BUTTON_IAMGE()
        
        viewWid = grayImg.size.width/2;
        viewHei = grayImg.size.height/2
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            viewWid = grayImg.size.width/1.8;
            viewHei = grayImg.size.height/1.8
        }
        posX = GRAPHICS.Screen_Width() - viewWid - 10
        posY = (self.frame.size.height - viewHei)/2
        
        m_followBtn.frame = CGRectMake(posX,posY,viewWid,viewHei)
        m_followBtn.setBackgroundImage(grayImg, forState: UIControlState.Normal)
        m_followBtn.setBackgroundImage(redImg, forState: UIControlState.Selected)
        m_followBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(12)
        self.addSubview(m_followBtn)
    }
    func valuesForControls(profileImageUrl : String, name: String ,followState: String, var trophyStr: String ,isfromFollowers:Bool,profileImgTag : Int, showFollowBtn: Bool)
        
    {
        if showFollowBtn {
            m_followBtn.hidden = false
        }
        else {
              m_followBtn.hidden = true
        }
        
        m_nameLabel.text = name
        m_profileImgView.load(profileImageUrl, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
        m_profileImgView.clipsToBounds = true
        m_profileImgView.layer.cornerRadius = m_profileImgView.frame.size.height/2
        m_profileImgView.backgroundColor = UIColor.clearColor()
        m_profileImgView.tag = profileImgTag
        if trophyStr == ""
        {
            trophyStr = "0"
        }
        m_trophyLbl.text = String(format: "%@ trophies", trophyStr)
        
            if(followState == "1")
            {
                m_followBtn.selected = true
                m_followBtn.setTitle("Unfollow", forState: .Normal)
            }
            else
            {
                m_followBtn.selected = false
                m_followBtn.setTitle("Follow", forState: .Normal)
            }
        
    }
    
    func profileImgTapGesture(tapGesture : UITapGestureRecognizer) -> () {
        followProtocol.tapOnProfileImage(tapGesture)
    }


    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
