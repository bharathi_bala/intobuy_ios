//
//  ProductsEntity.swift
//  IntoBuy
//
//  Created by SmaatApps on 19/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit
import Foundation

class ProductsEntity: NSObject
{
    var strProductID : String = ""
    var strMainImage : String = ""
    var strCatID  : String = ""
    var strSubCatID : String = ""
    var strProductName : String = ""
    var strDescrption : String = ""
    var strTerms : String = ""
    var strQuantity : String = ""
    var strprice : String = ""
    var strUserName : String = ""
    var strAvatar : String = ""
    var strRating : String = ""
    
    init(productDictionary : NSDictionary)
    {
        strProductID = (productDictionary.objectForKey("productId") as? String!)!
        
         strMainImage = productDictionary.objectForKey("mainImage") as! String
        
         strCatID = productDictionary.objectForKey("catId") as! String
        
         strSubCatID = productDictionary.objectForKey("subCatId") as! String
        
         strProductName = productDictionary.objectForKey("productname") as! String
        
         strTerms = productDictionary.objectForKey("terms") as! String
        
         strQuantity = productDictionary.objectForKey("quantity") as! String
        
         strprice = productDictionary.objectForKey("price") as! String
        
          strUserName = productDictionary.objectForKey("sellername") as! String
        
          strAvatar = productDictionary.objectForKey("avatar") as! String
        
        strRating = productDictionary.objectForKey("rating") as! String
        
    }

}

