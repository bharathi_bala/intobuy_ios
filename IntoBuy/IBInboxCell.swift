//
//  IBInboxCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 19/04/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

public protocol InboxDelegate : NSObjectProtocol
{
    func profileImageGesture(tapGesture : UITapGestureRecognizer)
    func productImageGesture(tapGesture : UITapGestureRecognizer)
}



class IBInboxCell: MGSwipeTableCell {

    var bgView = UIView()
    var profileImgView = UIImageView()
    var nameLabel = UILabel()
    var msgLabel = UILabel()
    var productNameLbl = UILabel()
    var productImgView = UIImageView()
    var inboxProtocol:InboxDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsForContacts()
    }
    func createControlsForContacts()
    {
        self.frame.size.height = 80.0
        self.frame.size.width = GRAPHICS.Screen_Width()
        
        bgView.frame = CGRectMake(GRAPHICS.Screen_X(), 0, GRAPHICS.Screen_Width(), 890)
        self.contentView.addSubview(bgView)
        
        let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
        
        
        var viewWid = profileMaskImg.size.width/1.7
        var viewHei = profileMaskImg.size.height/1.7
        var posX = self.bounds.origin.x + 15
        var posY = (self.frame.size.height - viewHei)/2
        
        profileImgView.frame = CGRectMake(posX,posY,viewWid,viewHei)
        profileImgView.layer.cornerRadius = viewHei/2
        profileImgView.image = profileMaskImg
        bgView.addSubview(profileImgView)
        profileImgView.userInteractionEnabled = true
        profileImgView.clipsToBounds = true
        
        
        let profileTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBInboxCell.profileImgTapGesture(_:)))
        profileImgView.addGestureRecognizer(profileTapGesture)

        
        
        viewHei = 10
        viewWid = GRAPHICS.Screen_Width()/2.5
        posX = profileImgView.frame.maxX + 10
        posY = self.bounds.origin.y + 10
        var fontSize:CGFloat = 10;
        nameLabel.frame = CGRectMake(posX,posY,viewWid,viewHei)
        nameLabel.font = GRAPHICS.FONT_REGULAR(fontSize)
        nameLabel.text = "Manoj"
        bgView.addSubview(nameLabel)
        
        viewHei = 18
        viewWid = GRAPHICS.Screen_Width()/2
        posX = profileImgView.frame.maxX + 10
        posY = posY + viewHei
        fontSize = 14
        productNameLbl.frame = CGRectMake(posX,posY,viewWid,viewHei)
        productNameLbl.font = GRAPHICS.FONT_REGULAR(fontSize)
        bgView.addSubview(productNameLbl)
        
        viewHei = 15
        viewWid = GRAPHICS.Screen_Width()/1.8
        posX = profileImgView.frame.maxX + 10
        posY = posY + viewHei + 5//(self.frame.size.height - viewHei)/2
        fontSize = 12;
        msgLabel.frame = CGRectMake(posX,posY,viewWid,viewHei)
        msgLabel.font = GRAPHICS.FONT_REGULAR(fontSize)
        bgView.addSubview(msgLabel)
        
        
        viewHei = self.frame.size.height - 15
        viewWid = viewHei
        posX = self.frame.size.width - viewWid - 10
        posY = (self.frame.size.height - viewHei)/2
        
        productImgView.frame = CGRectMake(posX,posY,viewWid,viewHei)
        productImgView.userInteractionEnabled = true
        bgView.addSubview(productImgView)
        
        let productTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBInboxCell.productImgTapGesture(_:)))
        productImgView.addGestureRecognizer(productTapGesture)


        let dividerImg = GRAPHICS.SEARCH_DIVIDER_IMAGE()
        viewWid = self.frame.size.width - 20//dividerImg.size.width/2;
        viewHei = dividerImg.size.height/2
        posX = (self.frame.size.width - viewWid)/2
        posY = (self.frame.size.height - viewHei)
        let dividerImgView = UIImageView(frame: CGRectMake(posX,posY,viewWid,viewHei))
        dividerImgView.image = dividerImg
        bgView.addSubview(dividerImgView)
        
        
        
    }
    
    func profileImgTapGesture(tapGesture : UITapGestureRecognizer)
    {
        inboxProtocol!.profileImageGesture(tapGesture)
    }
    func productImgTapGesture(tapGesture : UITapGestureRecognizer)
    {
        inboxProtocol!.productImageGesture(tapGesture)
    }
    
    func setValuesToControls(profileImgStr:String , nameStr : String , messageStr : String , productNameStr :String , productImgString : String , profileImgTag : Int , productImgTag : Int)
    {
        profileImgView.tag = profileImgTag
        nameLabel.text = nameStr
        productNameLbl.text = productNameStr
        msgLabel.text = messageStr
        productImgView.tag = productImgTag
        
        let profileImageUrl = NSURL(string: profileImgStr)
        profileImgView.load(profileImageUrl!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
        
        if productImgString != ""{
        let productImageUrl = NSURL(string: productImgString)
        productImgView.load(productImageUrl!, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    
    
}
