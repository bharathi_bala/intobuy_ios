//
//  IBDiaryDataCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 18/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBDiaryDataCell: UITableViewCell {
    
    var leftImgView = UIImageView()
    var diaryLbl = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsFordiaryTable()
    }
    
    func createControlsFordiaryTable()
    {
        var xPos = CGFloat(10)
        let yPos = CGFloat(5)
        var width = CGFloat(40)
        var height = CGFloat(40)
        
        leftImgView.frame = CGRectMake(xPos, yPos, width, height)
        self.addSubview(leftImgView)
        
        xPos = leftImgView.frame.maxX + 10
        width = GRAPHICS.Screen_Width() - leftImgView.frame.size.width - 40
        height = 15
        diaryLbl.frame = CGRectMake(xPos, yPos, width, height)
        diaryLbl.font = GRAPHICS.FONT_REGULAR(14)
        diaryLbl.numberOfLines = 0;
        diaryLbl.lineBreakMode = .ByWordWrapping

        self.addSubview(diaryLbl)
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
