//
//  IBMyProductsCollectionViewCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 27/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

public protocol ProductProtocol : NSObjectProtocol
{
    func productMoreBtnAction(sender: UIButton)
    func productLikeBtnAction(sender : UIButton)
    func productCommentBtnAction(sender : UIButton)
    func productCartBtnAction(sender : UIButton)
}


class IBMyProductsCollectionViewCell: UICollectionViewCell {
    
    var bgView = UIView()
    var bgImgView = UIImageView()
    var productImgView = UIImageView()
    var productNameLbl = UILabel()
    var priceLbl = UILabel()
    var likeBtn = UIButton()
    var commentBtn = UIButton()
    var cartBtn = UIButton()
    var moreBtn = UIButton()
    var cartbigBtn:UIButton!
    var commentbigBtn:UIButton!
    var likebigBtn:UIButton!
    var moreBigBtn:UIButton!
    var productDelegate:ProductProtocol!
    var soldOutImgView = UIImageView()
    
    var commentLbl = UILabel()
    var likeLbl = UILabel()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    func createControlsForProductList(width : CGFloat , height : CGFloat, bgImage : UIImage ,gridIndex : Int , productImgUrl : String)
    {
        bgView.frame = CGRectMake(0,0,width,height)
//        bgView.backgroundColor = UIColor.orangeColor()
        self.addSubview(bgView)
        
        var contentSize = GRAPHICS.getImageWidthHeight(bgImage)
        var Width = contentSize.width
        var Height:CGFloat! //= contentSize.height
        
        if gridIndex == 1
        {
            if GRAPHICS.Screen_Type() == IPHONE_4 || GRAPHICS.Screen_Type() == IPHONE_5
            {
                Height = bgImage.size.height/1.4
            }
            else
            {
                Height = bgImage.size.height/1.2
            }
        }
        else
        {
            Height = contentSize.height
        }
        
        bgImgView.frame  = CGRectMake(-3,-5,Width,Height)
        bgImgView.image = bgImage
        bgImgView.userInteractionEnabled = true
        bgView.addSubview(bgImgView)
        
        var padding:CGFloat!
        var diff:CGFloat!
        var fontSize:CGFloat!
        if gridIndex == 1
        {
            if GRAPHICS.Screen_Type() == IPHONE_4 ||  GRAPHICS.Screen_Type() == IPHONE_5
            {
                padding = 11
                diff = 70
            }
            else{
            padding = 13
                diff = 75
            }
            fontSize = 12
        }
        else
        {
            padding = 15
            diff = 82
            fontSize = 14
        }

        
        productImgView = UIImageView(frame: CGRectMake(bgImgView.bounds.origin.x + padding, bgImgView.bounds.origin.y + padding + 3, bgImgView.frame.size.width - 2 * padding, bgImgView.frame.size.height - diff))
        productImgView.load(productImgUrl, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
        productImgView.userInteractionEnabled = true
        bgImgView.addSubview(productImgView)
        
        
        let soldOutImage = GRAPHICS.PRODUCT_SOLD_OUT_IMAGE()
        contentSize = GRAPHICS.getImageWidthHeight(soldOutImage!)
        Width = contentSize.width
        Height = contentSize.height
        var XPos = productImgView.frame.size.width - Width
        var YPos = productImgView.bounds.origin.y
        soldOutImgView.frame = CGRectMake(XPos,YPos,Width,Height)
        soldOutImgView.image = soldOutImage
        productImgView.addSubview(soldOutImgView)

        
        productNameLbl = UILabel(frame: CGRectMake(productImgView.frame.origin.x,productImgView.frame.maxY,productImgView.frame.size.width/2, 15))
        productNameLbl.text  = "Iphone 4"
        productNameLbl.font = GRAPHICS.FONT_REGULAR(fontSize)
        bgImgView.addSubview(productNameLbl)
        
        priceLbl = UILabel(frame: CGRectMake(productImgView.frame.origin.x,productNameLbl.frame.maxY,productImgView.frame.size.width/2, 15))
        priceLbl.text  = "$ 300"
        priceLbl.font = GRAPHICS.FONT_REGULAR(fontSize)
        bgImgView.addSubview(priceLbl)
        
        let shareImg = GRAPHICS.HOME_SHARE_BUTTON_IMAGE()
        contentSize = GRAPHICS.getImageWidthHeight(shareImg)
        Width = contentSize.width//shareImg.size.width/2
        Height = contentSize.height//shareImg.size.height/2
        XPos = productImgView.frame.maxX - Width
        YPos = productImgView.frame.maxY
        
        if gridIndex == 1
        {
            YPos = productImgView.frame.maxY - 5
        }
        moreBtn = UIButton(frame : CGRectMake(XPos,YPos,Width,Height))
        moreBtn.setBackgroundImage(shareImg, forState: .Normal)
        moreBtn.addTarget(self, action: #selector(IBMyProductsCollectionViewCell.moreButtonAction(_:)), forControlEvents: .TouchUpInside)
        bgImgView.addSubview(moreBtn)
        
        moreBigBtn = UIButton(frame : CGRectMake(moreBtn.frame.origin.x - 1,moreBtn.frame.origin.y - 2,moreBtn.frame.size.width + 4 , moreBtn.frame.size.height + 2))
        moreBigBtn.addTarget(self, action: #selector(IBMyProductsCollectionViewCell.moreButtonAction(_:)), forControlEvents: .TouchUpInside)
        bgImgView.addSubview(moreBigBtn)


        
        var cartImage = GRAPHICS.HOME_ADD_CART_BUTTON_IMAGE()
        var cartPinkImg = GRAPHICS.HOME_ADD_CART_PINK_BUTTON_IMAGE()
        if gridIndex == 1
        {
            cartImage = GRAPHICS.MY_PRODUCT_GRID_CART_BTN_IMAGE()
            cartPinkImg = GRAPHICS.MY_PRODUCT_GRID_CART_PINK_BTN_IMAGE()
            
        }
        contentSize = GRAPHICS.getImageWidthHeight(cartImage)
        Width = contentSize.width
        Height = contentSize.height
        XPos = moreBtn.frame.maxX - Width//productView.frame.size.width - Width
        
        if GRAPHICS.Screen_Type() == IPHONE_4 ||  GRAPHICS.Screen_Type() == IPHONE_5 {
            YPos = moreBtn.frame.maxY
        }
        else{
           YPos = moreBtn.frame.maxY - 5
        }
        if gridIndex == 0
        {
            productNameLbl.frame.origin.y = productImgView.frame.maxY + 10
            priceLbl.frame.origin.y = productNameLbl.frame.maxY + 5
            moreBtn.frame.origin.y = productImgView.frame.maxY + 10
            XPos = moreBigBtn.frame.origin.x - Width - 5
            YPos = moreBtn.frame.origin.y
        }
        cartBtn = UIButton(frame : CGRectMake(XPos,YPos,Width,Height))
        cartBtn.setBackgroundImage(cartImage, forState: .Normal)
        cartBtn.setBackgroundImage(cartPinkImg, forState: .Selected)
        cartBtn.addTarget(self, action: #selector(IBMyProductsCollectionViewCell.cartBtnAction(_:)), forControlEvents: .TouchUpInside)
        bgImgView.addSubview(cartBtn)
        
        cartbigBtn = UIButton(frame : CGRectMake(cartBtn.frame.origin.x - 2,cartBtn.frame.origin.y - 2,cartBtn.frame.size.width + 4 , cartBtn.frame.size.height + 4))
        cartbigBtn.addTarget(self, action: #selector(IBMyProductsCollectionViewCell.cartBtnAction(_:)), forControlEvents: .TouchUpInside)
        bgImgView.addSubview(cartbigBtn)
        
        Width = cartBtn.frame.size.width
        Height = contentSize.height
        XPos = cartbigBtn.frame.origin.x - Width
        YPos = cartBtn.frame.origin.y
        
        commentLbl.frame = CGRectMake(XPos,YPos,Width,Height)
        commentLbl.font = GRAPHICS.FONT_REGULAR(fontSize)
        commentLbl.textAlignment = .Center
        bgImgView.addSubview(commentLbl)
        

        
//        MY_PRODUCT_GRID_LIKE_BTN_NORMAL_IMAGE
//        MY_PRODUCT_GRID_LIKE_BTN_SELECTED_IMAGE
//        MY_PRODUCT_GRID_COMMENT_BTN_NORMAL_IMAGE
//        MY_PRODUCT_GRID_COMMENT_BTN_SELECTED_IMAGE
        
        var commentImg = GRAPHICS.HOME_COMMENT_BUTTON_IMAGE()
        var commentSelImg = GRAPHICS.HOME_COMMENT_BUTTON_SELECTED_IMAGE()
        if gridIndex == 1
        {
            commentImg = GRAPHICS.MY_PRODUCT_GRID_COMMENT_BTN_NORMAL_IMAGE()
            commentSelImg = GRAPHICS.MY_PRODUCT_GRID_COMMENT_BTN_SELECTED_IMAGE()
        }
        contentSize = GRAPHICS.getImageWidthHeight(commentImg)
        Width = contentSize.width
        Height = contentSize.height
        XPos = commentLbl.frame.origin.x - Width
        
        commentBtn.frame  = CGRectMake(XPos,YPos,Width,Height)
        commentBtn.setBackgroundImage(commentImg, forState: .Normal)
        commentBtn.setBackgroundImage(commentSelImg, forState: .Selected)
        commentBtn.addTarget(self, action: #selector(IBMyProductsCollectionViewCell.commentBtnAction(_:)), forControlEvents: .TouchUpInside)
        bgImgView.addSubview(commentBtn)
        
        commentbigBtn = UIButton(frame : CGRectMake(commentBtn.frame.origin.x - 2,commentBtn.frame.origin.y - 2,commentBtn.frame.size.width + 4 , commentBtn.frame.size.height + 4))
        commentbigBtn.addTarget(self, action: #selector(IBMyProductsCollectionViewCell.commentBtnAction(_:)), forControlEvents: .TouchUpInside)
        bgImgView.addSubview(commentbigBtn)

        
        Width = commentBtn.frame.size.width
        Height = contentSize.height
        XPos = commentbigBtn.frame.origin.x - Width
        YPos = commentBtn.frame.origin.y
        likeLbl.frame = CGRectMake(XPos,YPos,Width,Height)
        likeLbl.font = GRAPHICS.FONT_REGULAR(fontSize)
        likeLbl.textAlignment = .Center
        bgImgView.addSubview(likeLbl)
        
        
        var likeImg = GRAPHICS.HOME_FAVOURITE_BUTTON_IMAGE()
        var likeSel = GRAPHICS.HOME_FAVOURITE_BUTTON_SELECTED_IMAGE()
        if gridIndex == 1
        {
            likeImg = GRAPHICS.MY_PRODUCT_GRID_LIKE_BTN_NORMAL_IMAGE()
            likeSel = GRAPHICS.MY_PRODUCT_GRID_LIKE_BTN_SELECTED_IMAGE()
        }

        contentSize = GRAPHICS.getImageWidthHeight(likeImg)
        Width = contentSize.width//shareImg.size.width/2
        Height = contentSize.height
//        Width = likeImg.size.width/2
//        Height = likeImg.size.height/2
        XPos = likeLbl.frame.origin.x - Width
        likeBtn.frame = CGRectMake(XPos,YPos,Width,Height)
        likeBtn.setBackgroundImage(likeImg, forState: .Normal)
        likeBtn.setBackgroundImage(likeSel, forState: .Selected)
        likeBtn.addTarget(self, action: #selector(IBMyProductsCollectionViewCell.likeBtnAction(_:)), forControlEvents: .TouchUpInside)
        bgImgView.addSubview(likeBtn)
        
        likebigBtn = UIButton(frame : CGRectMake(likeBtn.frame.origin.x - 2,likeBtn.frame.origin.y - 2,likeBtn.frame.size.width + 4 , likeBtn.frame.size.height + 4))
        likebigBtn.addTarget(self, action: #selector(IBMyProductsCollectionViewCell.likeBtnAction(_:)), forControlEvents: .TouchUpInside)
        bgImgView.addSubview(likebigBtn)


        bgView.frame.size.height = bgImgView.frame.maxY + 10
    }
    
    func setvaluesForProducts(productName : String , ProductRate : String , commentArray :NSArray , likeBtnTag : Int , commentBtnTag : Int , cartBtnTag : Int , isLiked : String , moreBtnTag : Int, cartStatus : String, likeCount : String, quantityStr : String)
    {
        //print(moreBtnTag)
        moreBtn.tag = moreBtnTag
        moreBigBtn.tag = moreBtnTag
        likeBtn.tag = likeBtnTag
        likebigBtn.tag = likeBtnTag
        commentBtn.tag = commentBtnTag
        commentbigBtn.tag = likeBtnTag
        cartBtn.tag = cartBtnTag
        cartbigBtn.tag = cartBtnTag
        productNameLbl.text = productName
        priceLbl.text = ProductRate
        likeLbl.text = likeCount
        if commentArray.count > 0
        {
            commentBtn.selected = true
            commentLbl.text = String(commentArray.count)
           // commentBtn.frame.origin.x = commentLbl.frame.origin.x - commentBtn.frame.size.width - 5
        }
        else
        {
            commentBtn.selected = false
            commentLbl.text = "0"
            //commentBtn.frame.origin.x = cartBtn.frame.origin.x - commentBtn.frame.size.width - 5
        }
        if isLiked == "1"
        {
            likeBtn.selected = true
        }
        else
        {
            likeBtn.selected = false
        }
        
        if likeCount == ""
        {
            likeLbl.hidden = true
            //likeBtn.frame.origin.x = commentBtn.frame.origin.x - likeBtn.frame.size.width - 5
        }
        else
        {
            likeLbl.hidden = false
            //likeBtn.frame.origin.x = commentLbl.frame.origin.x - likeBtn.frame.size.width - 5
        }
        
        if cartStatus == "1"
        {
            cartBtn.selected = true
            cartbigBtn.selected = true
        }
        else
        {
            cartBtn.selected = false
            cartbigBtn.selected = false
        }
        
        if quantityStr == "" || quantityStr == "0"
        {
            soldOutImgView.hidden = false
        }
        else
        {
            soldOutImgView.hidden = true
        }
        
    }
    func cartBtnAction(sender : UIButton) -> () {
        productDelegate.productCartBtnAction(sender)
    }
    func commentBtnAction(sender : UIButton) -> () {
        productDelegate.productCommentBtnAction(sender)
    }
    func likeBtnAction(sender : UIButton) -> () {
        productDelegate.productLikeBtnAction(sender)
    }
    func moreButtonAction(sender : UIButton) -> () {
        productDelegate.productMoreBtnAction(sender)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
