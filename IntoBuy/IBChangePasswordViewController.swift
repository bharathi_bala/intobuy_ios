//
//  IBChangePasswordViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 05/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBChangePasswordViewController: IBBaseViewController,UITextFieldDelegate,ServerAPIDelegate {
    
    var oldPwdTF = UITextField()
    var newPwdTF = UITextField()
    var confirmNewPwdTF = UITextField()
    var keyBoardTool:UIToolbar!

    override func viewDidLoad() {
        bottomVal = 5
        toShowActivityBtn = true
        super.viewDidLoad()
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.m_settingsButton.hidden = true
        self.m_bgImageView.backgroundColor = UIColor.whiteColor()
        self.m_titleLabel.text = Settings_Title
        createControlsForChangePassword()
    }
    func createControlsForChangePassword()
    {
        let titleLbl = UILabel(frame : CGRectMake(GRAPHICS.Screen_X() + 10, self.m_bgImageView.bounds.origin.y,GRAPHICS.Screen_Width() - 20 , 50))
        if GRAPHICS.Screen_Type() == IPHONE_4 || GRAPHICS.Screen_Type() == IPHONE_5{
            titleLbl.frame.size.height = 40
        }
        titleLbl.text = "Change Password"
        titleLbl.font = GRAPHICS.FONT_REGULAR(12)
        self.m_bgImageView.addSubview(titleLbl)
        addBottomBorder(titleLbl)
        
        var xPos = GRAPHICS.Screen_X() + 10
        var yPos = titleLbl.frame.maxY + 30
        var width =  GRAPHICS.Screen_Width()/2.5
        var height:CGFloat = 15
        let oldPwdLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        oldPwdLbl.text = String(format: "%@:",ChangePwd_OldPassword)
        oldPwdLbl.font = GRAPHICS.FONT_REGULAR(12)
        self.m_bgImageView.addSubview(oldPwdLbl)
        
        height = 20
        width =  GRAPHICS.Screen_Width()/2
        let textBoxImg = GRAPHICS.SETTINGS_BIG_TEXTBOX_IMAGE()
        xPos = oldPwdLbl.frame.maxX + 10
        oldPwdTF.frame = CGRectMake(xPos,yPos,width,height)
        oldPwdTF.delegate = self
        oldPwdTF.inputAccessoryView = createKeyboardToolBar()
        oldPwdTF.background = textBoxImg
        oldPwdTF.secureTextEntry = true
        self.m_bgImageView.addSubview(oldPwdTF)
        
        height = 15
        xPos = GRAPHICS.Screen_X() + 10
        yPos = oldPwdLbl.frame.maxY + 30
        width =  GRAPHICS.Screen_Width()/2.5
        let newPwdLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        newPwdLbl.text = String(format: "%@:",ChangePwd_NewPassword)
        newPwdLbl.font = GRAPHICS.FONT_REGULAR(12)
        self.m_bgImageView.addSubview(newPwdLbl)
        
        height = 20
        xPos = newPwdLbl.frame.maxX + 10
        width =  GRAPHICS.Screen_Width()/2
        newPwdTF.frame = CGRectMake(xPos,yPos,width,height)
        newPwdTF.delegate = self
        newPwdTF.background = textBoxImg
        newPwdTF.inputAccessoryView = createKeyboardToolBar()
        newPwdTF.secureTextEntry = true
        self.m_bgImageView.addSubview(newPwdTF)
        
        height = 15
        xPos = GRAPHICS.Screen_X() + 10
        yPos = newPwdLbl.frame.maxY + 30
        width =  GRAPHICS.Screen_Width()/2.5
        let confirmNewPwdLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        confirmNewPwdLbl.text = String(format: "%@:",ChangePwd_ConfirmNewPassword)
        confirmNewPwdLbl.font = GRAPHICS.FONT_REGULAR(12)
        self.m_bgImageView.addSubview(confirmNewPwdLbl)
        
        height = 20
        xPos = confirmNewPwdLbl.frame.maxX + 10
        width =  GRAPHICS.Screen_Width()/2
        confirmNewPwdTF.frame = CGRectMake(xPos,yPos,width,height)
        confirmNewPwdTF.delegate = self
        confirmNewPwdTF.background = textBoxImg
        confirmNewPwdTF.secureTextEntry = true
        confirmNewPwdTF.inputAccessoryView = createKeyboardToolBar()
        self.m_bgImageView.addSubview(confirmNewPwdTF)
        
        let saveImg = GRAPHICS.SETTINGS_BILLING_BTN_IMAGE()
        width = (saveImg.size.width)/2
        height = (saveImg.size.height)/2
        if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            //height = 30
            width = (saveImg.size.width)/1.8
            height = (saveImg.size.height)/1.8
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = (saveImg.size.width)/1.5
            height = (saveImg.size.height)/1.5
        }
        
        xPos = (self.m_bgImageView.frame.size.width - width)/2
        yPos = confirmNewPwdTF.frame.maxY + 30
        let saveBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        saveBtn.setBackgroundImage(saveImg, forState: UIControlState.Normal)
        saveBtn.setTitle(saveDetails, forState: UIControlState.Normal)
        saveBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(14)
        saveBtn .addTarget(self, action: #selector(IBChangePasswordViewController.saveBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        self.m_bgImageView.addSubview(saveBtn)
    }

    //send btn action
    func saveBtnAction()
    {
        self.view.endEditing(true)
        if validation()
        {
            SwiftLoader.show(title: Loading, animated: true)
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_changepassword(getUserIdFromUserDefaults(),oldpassword:oldPwdTF.text!,password:newPwdTF.text!,cpassword:confirmNewPwdTF.text!)
        }
        else
        {
            
        }
    }
    func validation()->Bool{
        
        if oldPwdTF.text?.isEmpty == true{
            GRAPHICS.showAlert("Please Enter Old Password")
            //showAlert("Please enter First name")
            return false
        }
        else if (newPwdTF.text?.isEmpty ==  true){
            GRAPHICS.showAlert("Please Enter New Password")
            return false
        }
        else if (confirmNewPwdTF.text?.isEmpty ==  true){
            GRAPHICS.showAlert("Please Enter Confirm New Password")
            return false
        }

        else if newPwdTF.text != confirmNewPwdTF.text
        {
            GRAPHICS.showAlert("Your New and Confirm New Passwords are not same")
            return false
        }
        return true
    }
    // method for tool bar
    func createKeyboardToolBar() -> UIToolbar
    {
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        keyBoardTool = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        keyBoardTool!.barStyle = UIBarStyle.BlackOpaque
        keyBoardTool!.translucent = true
        keyBoardTool!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        keyBoardTool!.sizeToFit()
        
        
        let previousImage = GRAPHICS.PICKER_LEFT_ARROW_BTN()
        let nextImage = GRAPHICS.PICKER_RIGHT_ARROW_BTN()
        
        let prev_Btn = UIButton ()
        prev_Btn.frame = CGRectMake(10, 8, 40, 30)
        prev_Btn.backgroundColor =  UIColor.clearColor();
        prev_Btn.setBackgroundImage(previousImage, forState: .Normal)
        prev_Btn.addTarget(self, action: #selector(IBChangePasswordViewController.previousAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(prev_Btn)
        
        let next_Btn = UIButton ()
        next_Btn.frame = CGRectMake(50, 8, 40, 30)
        next_Btn.backgroundColor =  UIColor.clearColor();
        next_Btn.setBackgroundImage(nextImage, forState: .Normal)
        next_Btn.addTarget(self, action: #selector(IBChangePasswordViewController.nextAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(next_Btn)
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBChangePasswordViewController.doneBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(done_Btn)
        
        
        keyBoardTool!.userInteractionEnabled = true
        return keyBoardTool
        
    }
    
    func previousAction()
    {
        if confirmNewPwdTF.isFirstResponder()
        {
            confirmNewPwdTF.resignFirstResponder()
            newPwdTF.becomeFirstResponder()
        }
        else if newPwdTF.isFirstResponder()
        {
            newPwdTF.resignFirstResponder()
            oldPwdTF.becomeFirstResponder()
        }
        else
        {
            oldPwdTF.resignFirstResponder()
        }

    }
    func nextAction()
    {
        if oldPwdTF.isFirstResponder()
        {
            oldPwdTF.resignFirstResponder()
            newPwdTF.becomeFirstResponder()
        }
        else if newPwdTF.isFirstResponder()
        {
            newPwdTF.resignFirstResponder()
            confirmNewPwdTF.becomeFirstResponder()
        }
        else
        {
            confirmNewPwdTF.resignFirstResponder()
        }
    }
    func doneBtnAction()
    {
        self.view.endEditing(true)
    }
    //MARK:- textfield delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField == oldPwdTF
        {
            oldPwdTF.resignFirstResponder()
            newPwdTF.becomeFirstResponder()
        }
        else if textField == newPwdTF
        {
            newPwdTF.resignFirstResponder()
            confirmNewPwdTF.becomeFirstResponder()
        }
        else
        {
            confirmNewPwdTF.resignFirstResponder()
        }
        
        return true
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if(textField.frame.origin.y > self.m_bgImageView.frame.size.height - 280){
            UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.m_mainView.frame.origin.y =  -100
                }, completion: nil)
        }
        return true
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if(textField.frame.origin.y > self.m_bgImageView.frame.size.height - 280){
            UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.m_mainView.frame.origin.y =  0
                }, completion: nil)
        }

    }
    func addBottomBorder(view: UIView)
    {
        let lineImg = GRAPHICS.SEARCH_DIVIDER_IMAGE()
        let bottomBorder = UIImageView(frame: CGRectMake(view.bounds.origin.x + 5, view.frame.size.height-1.0, view.frame.size.width - 10, 1.0))
        bottomBorder.image = lineImg
        view.addSubview(bottomBorder)
    }
    //MARK:- APIU delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }

    func API_CALLBACK_changepassword(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"
        {
            showAlertViewWithMessage(resultDict.objectForKey("result") as! String)
            self.navigationController?.popViewControllerAnimated(true)
        }
        else
        {
            if let msg = resultDict.objectForKey("msg") as? String
            {
                showAlertViewWithMessage(msg)
            }
            else
            {
                showAlertViewWithMessage(ServerNotRespondingMessage)
            }
        }
    }
}
