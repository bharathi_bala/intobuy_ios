//
//  IBSingletonClass.swift
//  IntoBuy
//
//  Created by SmaatApps on 25/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBSingletonClass: NSObject
{
    //let uploadImg = GRAPHICS.DEFAULT_CAMERA_IMAGE()
    
    var m_UploadImageIndex = Int()
    var m_FirstImage : UIImage  = UIImage()
    var m_secondImage : UIImage = UIImage()//GRAPHICS.DEFAULT_CAMERA_IMAGE()
    var m_ThirdImage : UIImage  = UIImage()//GRAPHICS.DEFAULT_CAMERA_IMAGE()
    var m_FourthImage : UIImage  = UIImage()
    
    var m_productFirstImage : UIImage  = UIImage()
    var m_productsecondImage : UIImage = UIImage()//GRAPHICS.DEFAULT_CAMERA_IMAGE()
    var m_productThirdImage : UIImage  = UIImage()//
    var m_productFourthImage : UIImage  = UIImage()
    
    var m_isFromFirstImage = Bool()
    var m_isFromSecondImage = Bool()
    var m_isFromThirdImage  = Bool()
    var m_isFromFourthImage  = Bool()
    
    
    var m_isFromProductFirstImage = Bool()
    var m_isFromProductSecondImage = Bool()
    var m_isFromProductThirdImage  = Bool()
    var m_isFromProductFourthImage  = Bool()
    
    var isFromUpload = Bool()
    var  isFromSell = Bool()
    
    var screenIndex = NSInteger()
    
    var toEditProduct = Bool()
    var isFromImageTap = Bool()
    
    
    class var sharedInstance: IBSingletonClass
    {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: IBSingletonClass? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = IBSingletonClass()
        }
        return Static.instance!
    }

    
}

