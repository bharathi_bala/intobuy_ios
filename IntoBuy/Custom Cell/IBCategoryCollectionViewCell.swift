//
//  IBCategoryCollectionViewCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 23/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBCategoryCollectionViewCell: UICollectionViewCell {
    var m_categoryImgView:UIImageView!
    var m_categoryLbl:UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        m_categoryImgView = UIImageView(frame: CGRectMake(self.bounds.minX,self.bounds.minY,self.frame.size.width,self.frame.size.height))
        m_categoryImgView.backgroundColor = UIColor.clearColor()
        self.addSubview(m_categoryImgView)
        
        let transparentViw :UIView = UIView()
        transparentViw.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
        transparentViw.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.2)
         m_categoryImgView.addSubview(transparentViw)
        
        m_categoryLbl = UILabel(frame: m_categoryImgView.frame)
        m_categoryLbl.backgroundColor = UIColor.clearColor()
        m_categoryLbl.textAlignment = .Center
        //m_categoryLbl.alpha = 0.5
        m_categoryLbl.textColor = UIColor.whiteColor()
        m_categoryLbl.font = GRAPHICS.FONT_JENNASUE(40)
        m_categoryLbl.userInteractionEnabled = false
       transparentViw.addSubview(m_categoryLbl)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
}
