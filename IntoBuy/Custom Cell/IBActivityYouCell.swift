//
//  IBActivityYouCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 19/02/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

public protocol ActivityYouProtocol : NSObjectProtocol
{
    func ProfileImgGesture(tapGesture: UITapGestureRecognizer)
}


class IBActivityYouCell: UITableViewCell {
    
    var bgView = UIView()
    var m_profileImgView:UIImageView!
    var m_postLabel:UILabel!
   // var bottomBorder:UIImageView!
    var m_postedImgView:UIImageView!
    var m_postedView:UIView!
    var m_timeLbl = UILabel()
    var youdelegate:ActivityYouProtocol!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsForYou()
    }
    
    func createControlsForYou()
    {
        self.frame.size.width = GRAPHICS.Screen_Width()
        bgView.frame = CGRectMake(GRAPHICS.Screen_X(), 0, GRAPHICS.Screen_Width(), 100)
        self.contentView.addSubview(bgView)
        
        var xPos = GRAPHICS.Screen_X() + 10
        var yPos = bgView.bounds.origin.y + 10
        var width:CGFloat = 50
        var height:CGFloat = 50
        
        m_profileImgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        m_profileImgView.backgroundColor = UIColor.redColor()
        
        bgView.addSubview(m_profileImgView)
        
        m_profileImgView.userInteractionEnabled = true
        let profileTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBActivityYouCell.profilePicTapped(_:)))
        m_profileImgView.addGestureRecognizer(profileTapGesture)

        
        xPos = m_profileImgView.frame.maxX + 5
        yPos = (m_profileImgView.frame.size.height - 20) - 5// + 20
        width = 200
        height = 20
        
        
        m_postLabel = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_postLabel.backgroundColor = UIColor.clearColor()
        m_postLabel.font = GRAPHICS.FONT_REGULAR(14)
        bgView.addSubview(m_postLabel)
        
        m_timeLbl.frame = CGRectMake(m_postLabel.frame.maxX, m_postLabel.frame.origin.y, 30, 15)
        m_timeLbl.backgroundColor = UIColor.clearColor()
        m_timeLbl.font = GRAPHICS.FONT_REGULAR(14)
        m_timeLbl.textColor = UIColor.lightGrayColor()
        bgView.addSubview(m_timeLbl)
        
        width = 40
        height = 40
        xPos = GRAPHICS.Screen_Width() - width - 10
        yPos = self.bounds.origin.y + 15
        m_postedImgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        bgView.addSubview(m_postedImgView)
        
        xPos = m_postLabel.frame.origin.x
        yPos = m_postedImgView.frame.maxY + 10
        width =  GRAPHICS.Screen_Width() - m_profileImgView.frame.size.width - 20
        height = 50
        m_postedView = UIView(frame: CGRectMake(xPos,yPos,width,height))
        m_postedView.backgroundColor = UIColor.clearColor()
        bgView.addSubview(m_postedView)
        

    }
    
    func setValuesForControls(profilePicStr : String , newsStr : NSMutableAttributedString , imageArray : NSArray , dateStr : String)
    {
        if profilePicStr != ""
        {
            let profilePicUrl = NSURL(string: profilePicStr)
            m_profileImgView.load(profilePicUrl!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
        }
        else
        {
            m_profileImgView.image = GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE()
        }
        m_profileImgView.clipsToBounds = true
        m_profileImgView.layer.cornerRadius = m_profileImgView.frame.size.height/2
        m_postLabel.attributedText = newsStr
        m_postLabel.sizeToFit()
        m_timeLbl.frame.origin.x = (m_postLabel.frame.maxX)
        
        let addedDate = dateStr
        let addedDateStr = convertUtcToLocalFormat(addedDate)
        let dateFormat:NSDateFormatter = NSDateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormat.timeZone = NSTimeZone.localTimeZone()
        let startDate = dateFormat.dateFromString(addedDateStr)
        let intervalString = startDate!.relativeTimeToString()
        print("\(addedDateStr), \(intervalString)")
    
        let timeStr:String!
        let timeArray:NSArray = intervalString.componentsSeparatedByString(" ")
        let durationStr = timeArray.objectAtIndex(1) as! String
        let durationIntStr = timeArray.objectAtIndex(0) as! String
        
        if durationStr == "hours"
        {
            let str = "h"
            m_timeLbl.text = String(format : "%@ %@",durationIntStr,str)
        }
        else if durationStr == "days"
        {
            let str = "d"
            m_timeLbl.text = String(format : "%@ %@",durationIntStr,str)
        }
        else if durationStr == "minutes"
        {
            let str = "m"
            m_timeLbl.text = String(format : "%@ %@",durationIntStr,str)
        }
        else if durationStr == "seconds"
        {
            let str = "s"
            m_timeLbl.text = String(format : "%@ %@",durationIntStr,str)
        }
        
        
        if imageArray.count > 1
        {
            m_postedView.hidden = false
            m_postedImgView.hidden = true

            var xPos = m_postedView.bounds.origin.x
            let yPos = m_postedView.bounds.origin.y
            let width = m_postedView.frame.size.height
            let height = width
            
            for i in 0 ..< imageArray.count
            {
                let imageView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
                let imageDict = imageArray.objectAtIndex(i) as! NSDictionary
                let imageStr = imageDict.objectForKey("image") as! String
                let imageUrl = NSURL(string: imageStr)
                imageView.load(imageUrl!, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
                m_postedView.addSubview(imageView)
                xPos = xPos + width + 5
            }
            bgView.frame.size.height = m_postedView.frame.maxY + 10
        }
        else
        {
            m_postedView.hidden = true
            m_postedImgView.hidden = false
            if imageArray.count == 1
            {
                let imageDict = imageArray.objectAtIndex(0) as! NSDictionary
                let imageStr = imageDict.objectForKey("image") as! String
                let imageUrl = NSURL(string: imageStr)
                m_postedImgView.load(imageUrl!, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            }
            else
            {
                m_postedImgView.image = GRAPHICS.DEFAULT_CART_IMAGE()
            }
            bgView.frame.size.height = m_profileImgView.frame.maxY + 10
        }
        
//        let lineImg = GRAPHICS.SELL_LINE_IMAGE()
//        let bottomBorder = UIImageView(frame: CGRectMake(self.frame.origin.x + 5, self.frame.size.height-1.0, GRAPHICS.Screen_Width() - 10, 1.0))
//        bottomBorder.image = lineImg
//        self.addSubview(bottomBorder)
        
        
    }
    
    func hideOrShowTheView(toHideView : Bool)
    {
        if toHideView == true
        {
            m_postedView.hidden = true
            m_postedImgView.hidden = false
            self.frame.size.height = m_profileImgView.frame.maxY + 10
        }
        else{
            m_postedView.hidden = false
            m_postedImgView.hidden = true
            self.frame.size.height = m_postedView.frame.maxY + 10
        }
        

       // bottomBorder.frame.origin.y = self.frame.size.height - 1.0
    }
    
    func profilePicTapped(tapGesture : UITapGestureRecognizer)
    {
        youdelegate!.ProfileImgGesture(tapGesture)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
