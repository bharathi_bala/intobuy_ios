//
//  IBSettingsTableViewCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 07/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBSettingsTableViewCell: UITableViewCell {

    var m_titleLbl = UILabel()
    var m_switchBtn = UIButton()
    var m_arrowBtn = UIButton()
    var bottomBorder:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsForSettingsCell()
    }

    func createControlsForSettingsCell()
    {
        self.frame.size.height = 40.0
        let bgView = UIView(frame: CGRectMake(GRAPHICS.Screen_X(),self.bounds.origin.y,GRAPHICS.Screen_Width(),self.frame.size.height))
        self.addSubview(bgView)
        
        var posX = self.bounds.origin.x + 10
        var posY = self.bounds.origin.y
        var width = GRAPHICS.Screen_Width()/1.7
        var height = self.frame.size.height
        m_titleLbl.frame = CGRectMake(posX,posY,width,height)
        m_titleLbl.font = GRAPHICS.FONT_REGULAR(12)
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            m_titleLbl.font = GRAPHICS.FONT_REGULAR(14)
        }

        m_titleLbl.textColor = UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha: 1)
        bgView.addSubview(m_titleLbl)
        
        let btnImg = GRAPHICS.SETTINGS_SWITCH_BTN_NORMAL_IMAGE()
        let btnSelImg = GRAPHICS.SETTINGS_SWITCH_BTN_SELECTED_IMAGE()
        width = btnImg.size.width/2
        height = btnImg.size.height/2
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = btnImg.size.width/1.7
            height = btnImg.size.height/1.7
        }
        posX = GRAPHICS.Screen_Width() - width - 20
        posY = (self.frame.size.height - height)/2
        m_switchBtn.frame = CGRectMake(posX,posY,width,height)
        m_switchBtn.setBackgroundImage(btnImg, forState: UIControlState.Normal)
        m_switchBtn.setBackgroundImage(btnSelImg, forState: UIControlState.Selected)
        bgView.addSubview(m_switchBtn)
        
        
        let arrowBtnImg = GRAPHICS.SETTINGS_ARROW_BTN_IMAGE()
        width = arrowBtnImg.size.width/2
        height = arrowBtnImg.size.height/2
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = arrowBtnImg.size.width/1.7
            height = arrowBtnImg.size.height/1.7
        }
        posX = GRAPHICS.Screen_Width() - width - 20
        posY = (self.frame.size.height - height)/2

        m_arrowBtn.frame = CGRectMake(posX,posY,width,height)
        m_arrowBtn.setBackgroundImage(arrowBtnImg, forState: .Normal)
        bgView.addSubview(m_arrowBtn)
        
        let lineImg = GRAPHICS.SELL_LINE_IMAGE()
        bottomBorder = UIImageView(frame: CGRectMake(bgView.frame.origin.x + 10, bgView.frame.maxY-1.0, GRAPHICS.Screen_Width() - 20, 1.0))
        bottomBorder.image = lineImg
        bgView.addSubview(bottomBorder)

        
        
    }
    func showOrHideSwitchBtn(hideSwitchBtn:Bool)
    {
        if(hideSwitchBtn)
        {
            m_switchBtn.hidden = true
            m_arrowBtn.hidden = false
        }
        else{
            m_switchBtn.hidden = false
            m_arrowBtn.hidden = true
        }
    }
    func hideBottomLine(hideBottomLine: Bool)
    {
        if hideBottomLine == true
        {
            bottomBorder.hidden = true
        }
        else
        {
            bottomBorder.hidden = false
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
