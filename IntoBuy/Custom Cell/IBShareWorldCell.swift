//
//  IBShareWorldCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 31/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

public protocol ShareWorldProtocol : NSObjectProtocol
{
    func favoriteButtonAction(sender : UIButton)
}


class IBShareWorldCell: UITableViewCell {
    
    var bgView = UIView()
    var profileImgView = UIImageView()
    var nameLbl = UILabel()
    var locationLbl = UILabel()
    var favouritesBtn = UIButton()
    var productScrlView = UIScrollView()
    var shareWorldProtocol:ShareWorldProtocol!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createControls()
    }
    
    func createControls() -> () {
        
        var xPos = CGFloat(0.0)
        var yPos = CGFloat(0.0)
        var width = GRAPHICS.Screen_Width()
        var height = CGFloat(100.0)
        bgView.frame = CGRectMake(xPos, yPos, width, height)
        self.addSubview(bgView)
        
        xPos = 10
        yPos = 10
        width = 50
        height = 50
        profileImgView.frame = CGRectMake(xPos, yPos, width, height)
        profileImgView.backgroundColor = UIColor.redColor()
        profileImgView.layer.cornerRadius = height/2
        bgView.addSubview(profileImgView)
        profileImgView.clipsToBounds = true
        
        xPos = profileImgView.frame.maxX + 10
        width = GRAPHICS.Screen_Width()/2
        height = 15
        nameLbl.frame = CGRectMake(xPos, yPos, width, height)
        nameLbl.font = GRAPHICS.FONT_REGULAR(12)
        bgView.addSubview(nameLbl)
        
        yPos = nameLbl.frame.maxY
        locationLbl.frame = CGRectMake(xPos, yPos, width, height)
        locationLbl.font = GRAPHICS.FONT_REGULAR(10)
        locationLbl.textColor = lightGrayColor
        bgView.addSubview(locationLbl)

        let favouriteImg = GRAPHICS.SHARE_WORLD_STAR_ICON_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(favouriteImg)
        width = contentSize.width
        height = contentSize.height
        xPos = GRAPHICS.Screen_Width() - width - 10
        yPos = 15
        
        favouritesBtn.frame = CGRectMake(xPos, yPos, width, height)
        favouritesBtn.setBackgroundImage(favouriteImg, forState: .Normal)
        favouritesBtn.addTarget(self, action: #selector(IBShareWorldCell.favouritesBtnAction(_:)), forControlEvents: .TouchUpInside)
        bgView.addSubview(favouritesBtn)
        
        width = GRAPHICS.Screen_Width() - 10
        height = GRAPHICS.Screen_Width()/3 - 10//width
        xPos = GRAPHICS.Screen_X() + 5
        yPos = profileImgView.frame.maxY + 10
        productScrlView.frame = CGRectMake(xPos, yPos, width, height)
        bgView.addSubview(productScrlView)


        
            }
    func setValuesToControls(nameStr:String,locationStr:String ,profilePicUrl:NSURL,imagesArray:NSArray , favouritesBtnTag:Int) -> (){
        nameLbl.text = nameStr
        locationLbl.text = locationStr
        profileImgView.load(profilePicUrl, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
        
        let width = GRAPHICS.Screen_Width()/3 - 10
        let height = width
        var xPos = GRAPHICS.Screen_X() + 5
        let yPos = productScrlView.bounds.origin.y
        
        for i in 0 ..< imagesArray.count
        {
            let imgDict = imagesArray.objectAtIndex(i) as! NSDictionary
            let imgStr = imgDict.objectForKey("image") as! String
            let productImgUrl = NSURL(string: imgStr)
            let postedImgView = UIImageView(frame : CGRectMake(xPos, yPos, width, height))
            postedImgView.userInteractionEnabled = true
            postedImgView.tag = i
            productScrlView.addSubview(postedImgView)
            postedImgView.load(productImgUrl!, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            xPos = xPos + width + 10
        }
        favouritesBtn.tag = favouritesBtnTag
        productScrlView.contentSize = CGSizeMake(xPos + width + 10, productScrlView.frame.size.height)
        bgView.frame.size.height = yPos + height + 5
    }
    func  favouritesBtnAction(sender:UIButton) -> () {
        shareWorldProtocol.favoriteButtonAction(sender)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
