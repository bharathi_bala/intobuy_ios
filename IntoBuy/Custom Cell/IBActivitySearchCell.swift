//
//  IBActivitySearchCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 05/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBActivitySearchCell: UITableViewCell {

    var profileImgView = UIImageView()
    var nameLabel = UILabel()
    var friendBtn = UIButton()

    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsForActivitySearch()
    }

    func createControlsForActivitySearch()
    {
        self.frame.size.height = 60.0
        self.frame.size.width = GRAPHICS.Screen_Width()
        let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
        
        
        var viewWid = profileMaskImg.size.width/1.7
        var viewHei = profileMaskImg.size.height/1.7
        var posX = self.bounds.origin.x + 15
        var posY = (self.frame.size.height - viewHei)/2
        
        profileImgView.frame = CGRectMake(posX,posY,viewWid,viewHei)
        profileImgView.image = profileMaskImg
        profileImgView.userInteractionEnabled = true
        self.addSubview(profileImgView)
        
        let profileTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBActivitySearchCell.profilePicTapped(_:)))
        profileImgView.addGestureRecognizer(profileTapGesture)
        
        viewHei = 15
        viewWid = GRAPHICS.Screen_Width()/2.5
        posX = profileImgView.frame.maxX + 10
        posY = (self.frame.size.height - viewHei)/2
        let fontSize:CGFloat = 12;
        nameLabel.frame = CGRectMake(posX,posY,viewWid,viewHei)
        nameLabel.font = GRAPHICS.FONT_REGULAR(fontSize)
        nameLabel.text = "Manoj likes ur post"
        self.addSubview(nameLabel)
        
        let addFriendBtnImg = GRAPHICS.ACTIVITY_ADD_FRIEND_IMAGE()
        let tickBtnImg = GRAPHICS.ACTIVITY_BUTTON_TICK_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(addFriendBtnImg)
        viewHei = contentSize.width//addFriendBtnImg.size.width/2
        viewWid = contentSize.height//addFriendBtnImg.size.height/2
        posX = GRAPHICS.Screen_Width() - viewWid - 10
        posY = (self.frame.size.height - viewHei)/2
        friendBtn.frame = CGRectMake(posX,posY,viewWid,viewHei)
        friendBtn.setBackgroundImage(addFriendBtnImg, forState: .Normal)
        friendBtn.setBackgroundImage(tickBtnImg, forState: .Selected)
        self.addSubview(friendBtn)
    }
    
    func profilePicTapped(tapGesture : UITapGestureRecognizer)
    {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
