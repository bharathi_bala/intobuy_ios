//
//  IBMyWishlistCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 02/08/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBMyWishlistCell: MGSwipeTableCell {
    
    var bgView:UIView = UIView()
    var productImgView = UIImageView()
    var productNameLbl = UILabel()
    var reviewLbl = UILabel()
    var costLbl = UILabel()
    var cartBtn = UIButton()
    var bottomBorder = UIImageView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createControls()
    }
    
    
    func createControls()
    {
        self.frame.size.width = GRAPHICS.Screen_Width()
        
        bgView.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y,GRAPHICS.Screen_Width(),110)
        self.contentView.addSubview(bgView)
        
        var xPos:CGFloat = 10
        var yPos:CGFloat = 10
        var width = GRAPHICS.Screen_Width()/4
        var height = width
        
        productImgView.frame = CGRectMake(xPos, yPos, width, height)
        productImgView.layer.cornerRadius = 20
        productImgView.clipsToBounds = true
        bgView.addSubview(productImgView)
        
        xPos = productImgView.frame.maxX + 10
        width = GRAPHICS.Screen_Width()/2
        height = 15
        
        productNameLbl.frame = CGRectMake(xPos, yPos, width, height)
        productNameLbl.font = GRAPHICS.FONT_REGULAR(12)
        bgView.addSubview(productNameLbl)
        
        yPos = productNameLbl.frame.maxY + 5
        reviewLbl.frame = CGRectMake(xPos, yPos, width, height)
        reviewLbl.font = GRAPHICS.FONT_REGULAR(12)
        bgView.addSubview(reviewLbl)
        
        yPos = reviewLbl.frame.maxY + 5
        costLbl.frame = CGRectMake(xPos, yPos, width, height)
        costLbl.font = GRAPHICS.FONT_REGULAR(12)
        bgView.addSubview(costLbl)
        
        bgView.frame.size.height = productImgView.frame.maxY + 10
        print(bgView.frame.size.height)
        
        let cartImg = GRAPHICS.HOME_ADD_CART_BUTTON_IMAGE()
        let cartPinkImg = GRAPHICS.HOME_ADD_CART_PINK_BUTTON_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(cartImg)
        width = contentSize.width
        height = contentSize.height
        xPos = bgView.frame.size.width - width - 10
        yPos = bgView.frame.size.height - height - 10
        
        cartBtn.frame = CGRectMake(xPos,yPos,width,height)
        cartBtn.setBackgroundImage(cartImg, forState: .Normal)
        cartBtn.setBackgroundImage(cartPinkImg, forState: .Selected)
        cartBtn.addTarget(self, action: #selector(IBProductsTableViewCell.cartBtnAction(_:)), forControlEvents: .TouchUpInside)
        bgView.addSubview(cartBtn)
        
        let lineImg = GRAPHICS.PRODUCT_CATEGORY_BORDER_LINE_IMAGE()
        bottomBorder.frame = CGRectMake(bgView.frame.origin.x + 5, bgView.frame.maxY-1.0, GRAPHICS.Screen_Width() - 10, 1.0)
        bottomBorder.image = lineImg
        bgView.addSubview(bottomBorder)


    }
    
    func setValuesForControls(productImgStr : String , productNamestr : String , reviewStr : String , costStr : String , rating:String)
    {
        let productImgUrl = NSURL(string: productImgStr)
        productImgView.load(productImgUrl!, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
        
        productNameLbl.text = productNamestr
        let averageValue:Int = Int(rating)!
        var xPos = productNameLbl.frame.origin.x
        let yPos = productNameLbl.frame.maxY + 10
        let buyerImg = GRAPHICS.PROFILE_RATING_ICON_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(buyerImg)
        let height = contentSize.height
        let width = contentSize.width
        for i in 0 ..< 5
        {
            let ratingImgView = UIImageView(frame: CGRectMake(xPos, yPos, width, height))
            ratingImgView.image = buyerImg
            bgView.addSubview(ratingImgView)
            xPos = xPos + width + 5
        }
        reviewLbl.text = String(format: "%@ reviews",reviewStr)
        reviewLbl.frame.origin.y = yPos + height + 10
        
        costLbl.text = costStr
        costLbl.frame.origin.y = reviewLbl.frame.maxY + 10
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
