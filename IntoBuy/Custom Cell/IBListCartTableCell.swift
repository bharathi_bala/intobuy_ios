//
//  IBListCartTableCell.swift
//  IntoBuy
//
//  Created by SmaatApps on 18/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBListCartTableCell: MGSwipeTableCell
{
    var m_bgView:UIView = UIView()
    var m_cartIamegview:UIImageView = UIImageView()
    var m_productNameLbl:UILabel = UILabel()
    var m_stockLbl:UILabel = UILabel()
     var m_quantiytLbl:UILabel = UILabel()
     var m_priceLbl:UILabel = UILabel()
     var m_priceAmountLbl:UILabel = UILabel()
     var m_checkoutBtn:UIButton = UIButton()
    var m_favBtn:UIButton = UIButton()
    var m_mailBtn:UIButton = UIButton()
    var m_noOfQuantityTxtFiled:UITextField = UITextField()
    var addToCartBtn = UIButton()
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsForSearch()
    }
    
    func createControlsForSearch()
    {
        
         //self.frame.size.height = 130.0
        
        m_bgView.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y,GRAPHICS.Screen_Width(),110)
        self.contentView.addSubview(m_bgView)
        
        var xpos:CGFloat = 10
        var ypos:CGFloat = 10
        var width:CGFloat = 80
        var height:CGFloat = 80
        
        m_cartIamegview.frame = CGRectMake(xpos, ypos, width, height)
        m_cartIamegview.userInteractionEnabled = true
        m_bgView.addSubview(m_cartIamegview)
        
        
        xpos = m_cartIamegview.frame.maxX+5
        width = GRAPHICS.Screen_Width()-xpos
        height = 16
        
        m_productNameLbl = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr:"Transformer Deceprion cufflinks")
        m_bgView.addSubview(m_productNameLbl)
        
        ypos = m_productNameLbl.frame.maxY
        
        m_stockLbl = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr:"In Stock:29")
        m_bgView.addSubview(m_stockLbl)
        
        ypos = m_stockLbl.frame.maxY + 3
        height = 20
        
        //let quantityStr:String = String(format: "cart_quantity_text:")
        
        m_quantiytLbl = self.createLabel(xpos, ypos: ypos, width: 60, height: height, textStr:"Quantity:")
         m_bgView.addSubview(m_quantiytLbl)
        
        xpos = m_quantiytLbl.frame.maxX
        
        m_noOfQuantityTxtFiled.frame = CGRectMake(xpos, ypos, 50, height)
        m_noOfQuantityTxtFiled.text = "2"
        m_noOfQuantityTxtFiled.textColor = UIColor.blackColor()
        m_noOfQuantityTxtFiled.borderStyle = .RoundedRect
        m_noOfQuantityTxtFiled.font = GRAPHICS.FONT_REGULAR(12)
       // m_noOfQuantityTxtFiled.layer.cornerRadius = 1.8
        m_noOfQuantityTxtFiled.keyboardType = .NumberPad
        m_bgView.addSubview(m_noOfQuantityTxtFiled)
      
        
        ypos = m_quantiytLbl.frame.maxY
        xpos = m_quantiytLbl.frame.minX
        
        let priceStr:String = String(format: "price:")
        
        m_priceLbl = self.createLabel(xpos, ypos: ypos, width: 80, height: height, textStr:priceStr)
        m_bgView.addSubview(m_priceLbl)
        
        xpos = m_noOfQuantityTxtFiled.frame.minX
        
        m_priceAmountLbl = self.createLabel(xpos, ypos: ypos, width: 80, height: height, textStr:"$10.00")
        m_priceAmountLbl.textColor = appPinkColor
        m_bgView.addSubview(m_priceAmountLbl)
        
        
        let checkoutImageSize:CGSize = self.getImageWidthHeight(GRAPHICS.CART_CHECKOUT_ITEM_IMAGE())
        
        ypos = m_quantiytLbl.frame.minY
        xpos = GRAPHICS.Screen_Width() - checkoutImageSize.width - 10

        m_checkoutBtn.frame = CGRectMake(xpos, ypos, checkoutImageSize.width, checkoutImageSize.height)
        m_checkoutBtn .setTitle(cart_check_out_this_item, forState: .Normal)
        m_checkoutBtn .setBackgroundImage(GRAPHICS.PRODUCT_BUYNOW_IMAGE(), forState: .Normal)
        m_checkoutBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(10)
        m_bgView .addSubview(m_checkoutBtn)
        
        ypos = m_priceLbl.frame.minY
        xpos = xpos + 10
        
        let favImageSize:CGSize = self.getImageWidthHeight(GRAPHICS.CART_FAV_IMAGE())
        
        m_favBtn.frame = CGRectMake(xpos, ypos, favImageSize.width, favImageSize.height)
     
        m_favBtn .setBackgroundImage(GRAPHICS.CART_FAV_IMAGE(), forState: .Normal)
             m_bgView .addSubview(m_favBtn)
        
        xpos = m_favBtn.frame.maxX+10
        
        let mailImageSize:CGSize = self.getImageWidthHeight(GRAPHICS.CART_MAIL_IMAGE())
        
        m_mailBtn.frame = CGRectMake(xpos, ypos, mailImageSize.width, mailImageSize.height)
        
        m_mailBtn .setBackgroundImage(GRAPHICS.CART_MAIL_IMAGE(), forState: .Normal)
        m_bgView .addSubview(m_mailBtn)
        
        let addToCartImageSize:CGSize = self.getImageWidthHeight(GRAPHICS.APP_PINK_BUTTON_IAMGE())
        width = addToCartImageSize.width - 60
        ypos = m_checkoutBtn.frame.maxY + 10
        xpos = GRAPHICS.Screen_Width() - width - 30
        
        addToCartBtn.frame = CGRectMake(xpos, ypos, addToCartImageSize.width - 60, checkoutImageSize.height)
        addToCartBtn .setTitle(Favourites_AddToCartTitle, forState: .Normal)
        addToCartBtn .setBackgroundImage(GRAPHICS.APP_PINK_BUTTON_IAMGE(), forState: .Normal)
        addToCartBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(10)
        m_bgView .addSubview(addToCartBtn)

        let m_bottomlineView:UIView = UIView()
        m_bottomlineView.frame = CGRectMake(10, m_bgView.frame.size.height - 1, GRAPHICS.Screen_Width()-20, 1)
        m_bottomlineView.backgroundColor = UIColor.lightGrayColor()
        m_bgView.addSubview(m_bottomlineView)
    }
    
    func setValuesToControls(cartImgUrl : String , ProductName : String ,stockAvailable : String , quantityStr : String , priceStr : String)
    {
            m_cartIamegview.load(cartImgUrl, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            m_productNameLbl.text = ProductName
            m_noOfQuantityTxtFiled.text = quantityStr
            m_priceAmountLbl.text = priceStr
            m_stockLbl.text = String(format: "In Stock : %@",stockAvailable)
    }
    
    func getImageWidthHeight(imageName:UIImage)->CGSize
    {
        var fWidth:CGFloat = 0.0
        var fHeight:CGFloat = 0.0
        
        let searchiew:UIImage = imageName
        fWidth = searchiew.size.width/2
        fHeight = searchiew.size.height/2
        
        if GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus
        {
            fWidth = searchiew.size.width/1.5
            fHeight = searchiew.size.height/1.5
            
        }
        
        return CGSizeMake(fWidth, fHeight)
        
    }
    
    
    //Creation oF Product labels
    func  createLabel(xpos:CGFloat,ypos: CGFloat,width: CGFloat,height:CGFloat,textStr : String)-> UILabel
    {
        let lbl : UILabel = UILabel()
        lbl.frame = CGRectMake(xpos, ypos, width, height)
        lbl.backgroundColor = UIColor.clearColor()
        lbl.text = textStr
        lbl.textColor = UIColor.blackColor()
        lbl.textAlignment = .Left
        lbl.font = GRAPHICS.FONT_REGULAR(11)
        return lbl
        
    }
    
    func showORhideCartButton(toHideCartBtn : Bool)
    {
        if toHideCartBtn
        {
            addToCartBtn.hidden = true
            m_checkoutBtn .setTitle(cart_check_out_this_item, forState: .Normal)
            m_favBtn.hidden = false
            m_mailBtn.hidden = false
        }
        else
        {
            addToCartBtn.hidden = false
            m_checkoutBtn .setTitle(Favourites_RemoveTitle, forState: .Normal)
            m_favBtn.hidden = true
            m_mailBtn.hidden = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
