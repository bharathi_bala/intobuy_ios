//
//  IBSearchProductCategoryCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 01/02/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBSearchProductCategoryCell: UITableViewCell {
    
    var m_titleLbl = UILabel()
    var m_tickBtn = UIButton()
    var bottomBorder:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControls()
    }
    
    func createControls()
    {
        self.frame.size.height = 40.0
        let bgView = UIView(frame: CGRectMake(GRAPHICS.Screen_X(),self.bounds.origin.y,GRAPHICS.Screen_Width(),self.frame.size.height))
        self.addSubview(bgView)
        
        var posX = self.bounds.origin.x + 5
        var posY = self.bounds.origin.y
        var width = GRAPHICS.Screen_Width()/1.5
        var height = self.frame.size.height
        m_titleLbl.frame = CGRectMake(posX,posY,width,height)
        m_titleLbl.font = GRAPHICS.FONT_REGULAR(12)
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            m_titleLbl.font = GRAPHICS.FONT_REGULAR(14)
        }
        //m_titleLbl.textColor = UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha: 1)
        m_titleLbl.textColor = UIColor.blackColor()
        bgView.addSubview(m_titleLbl)
        
        let arrowBtnImg = GRAPHICS.PRODUCT_CATEGORY_TICK_IMAGE()
        width = arrowBtnImg.size.width/2
        height = arrowBtnImg.size.height/2
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            width = arrowBtnImg.size.width/1.7
            height = arrowBtnImg.size.height/1.7
        }
        posX = GRAPHICS.Screen_Width() - width - 10
        posY = (self.frame.size.height - height)/2
        
        m_tickBtn.frame = CGRectMake(posX,posY,width,height)
        m_tickBtn.setBackgroundImage(arrowBtnImg, forState: .Normal)
        bgView.addSubview(m_tickBtn)
        
        
        let lineImg = GRAPHICS.PRODUCT_CATEGORY_BORDER_LINE_IMAGE()
        bottomBorder = UIImageView(frame: CGRectMake(bgView.frame.origin.x + 5, bgView.frame.maxY-1.0, GRAPHICS.Screen_Width() - 10, 1.0))
        bottomBorder.image = lineImg
        bgView.addSubview(bottomBorder)

    }
    
    func ShowOrHideTickButton(hideTickBtn : Bool)
    {
        if(hideTickBtn)
        {
            m_tickBtn.hidden = true
        }
        else{
            m_tickBtn.hidden = false
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
