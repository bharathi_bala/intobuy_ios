//
//  IBProductsCollectionViewCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 02/08/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

public protocol ProductDelegate : NSObjectProtocol
{
    func moreButtonTapped(sender : UIButton)
}


class IBProductsCollectionViewCell: UICollectionViewCell {
    
    
    var bgView = UIView()
    var bgImgView = UIImageView()
    var productImgView = UIImageView()
    var productNameLbl = UILabel()
    var priceLbl = UILabel()
    var soldOutImgView = UIImageView()
    var moreBtn:UIButton!
    var moreBigBtn:UIButton!
    var reviewLabel = UILabel()
    
    var profileImgView = UIImageView()
    var dateLbl = UILabel()
    var userNameLbl = UILabel()
    var productDelegate:ProductDelegate!

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func createControlsForProductList(width : CGFloat , height : CGFloat, bgImage : UIImage )
    {
        bgView.frame = CGRectMake(0,0,width,height)
        //        bgView.backgroundColor = UIColor.orangeColor()
        self.addSubview(bgView)
        
        var contentSize = GRAPHICS.getImageWidthHeight(bgImage)
        var Width = contentSize.width
        var Height:CGFloat! //= contentSize.height
        
        if GRAPHICS.Screen_Type() == IPHONE_4 || GRAPHICS.Screen_Type() == IPHONE_5
        {
            Height = bgImage.size.height/1.2
        }
        else
        {
            Height = bgImage.size.height
        }
        
        
        bgImgView.frame  = CGRectMake(-3,-5,Width,Height)
        bgImgView.image = bgImage
        bgImgView.userInteractionEnabled = true
        bgView.addSubview(bgImgView)
        
        var padding:CGFloat!
        var diff:CGFloat!
        var fontSize:CGFloat!
        if GRAPHICS.Screen_Type() == IPHONE_4 ||  GRAPHICS.Screen_Type() == IPHONE_5
        {
            padding = 21
            diff = 70
        }
        else{
            padding = 26
            diff = 80
        }
        
        fontSize = 12
        
        productImgView = UIImageView(frame: CGRectMake(bgImgView.bounds.origin.x + 10, bgImgView.bounds.origin.y + padding, bgImgView.frame.size.width - 20, bgImgView.frame.size.height - (2 * diff)))
        //productImgView.load(productImgUrl, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
        productImgView.userInteractionEnabled = true
        bgImgView.addSubview(productImgView)
        
        let soldOutImage = GRAPHICS.PRODUCT_SOLD_OUT_IMAGE()
        contentSize = GRAPHICS.getImageWidthHeight(soldOutImage!)
        Width = contentSize.width
        Height = contentSize.height
        var XPos = productImgView.frame.size.width - Width
        var YPos = productImgView.bounds.origin.y
        soldOutImgView.frame = CGRectMake(XPos,YPos,Width,Height)
        soldOutImgView.image = soldOutImage
        productImgView.addSubview(soldOutImgView)
        
        productNameLbl.frame = CGRectMake(productImgView.frame.origin.x,productImgView.frame.maxY,productImgView.frame.size.width/2, 15)
//        productNameLbl.text  = "Iphone 4"
        productNameLbl.font = GRAPHICS.FONT_REGULAR(fontSize)
        bgImgView.addSubview(productNameLbl)
        
        priceLbl.frame = CGRectMake(productImgView.frame.origin.x,productNameLbl.frame.maxY,productImgView.frame.size.width/2, 15)
//        priceLbl.text  = "$ 300"
        priceLbl.font = GRAPHICS.FONT_REGULAR(fontSize)
        bgImgView.addSubview(priceLbl)
        
        XPos = productNameLbl.frame.origin.x
        YPos = priceLbl.frame.maxY + 5
        Width = 50
        Height = 50
        profileImgView.frame = CGRectMake(XPos,YPos,Width,Height)
        profileImgView.clipsToBounds = true
        profileImgView.layer.cornerRadius = Height/2
        profileImgView.backgroundColor = UIColor.greenColor()
        bgImgView.addSubview(profileImgView)
        
        XPos = profileImgView.frame.maxX + 5
        Width = bgImgView.frame.size.width/2
        Height = 15
        userNameLbl.frame = CGRectMake(XPos,YPos,Width,Height)
        userNameLbl.font = GRAPHICS.FONT_REGULAR(fontSize)
        bgImgView.addSubview(userNameLbl)
        
        YPos = userNameLbl.frame.maxY
        dateLbl.frame = CGRectMake(XPos,YPos,Width,Height)
        dateLbl.font = GRAPHICS.FONT_REGULAR(fontSize)
        bgImgView.addSubview(dateLbl)

        XPos = productNameLbl.frame.origin.x
        YPos = profileImgView.frame.maxY
        Width = GRAPHICS.Screen_Width()/2
        Height = 15
        reviewLabel.frame = CGRectMake(XPos,YPos,Width,Height)
        reviewLabel.font = GRAPHICS.FONT_REGULAR(fontSize)
        bgImgView.addSubview(reviewLabel)
        
        let shareImg = GRAPHICS.HOME_SHARE_BUTTON_IMAGE()
        contentSize = GRAPHICS.getImageWidthHeight(shareImg)
        Width = contentSize.width//shareImg.size.width/2
        Height = contentSize.height//shareImg.size.height/2
        XPos = productImgView.frame.maxX - Width
        //YPos = productImgView.frame.maxY
        
        YPos = bgImgView.frame.size.height - Height - 25
        moreBtn = UIButton(frame : CGRectMake(XPos,YPos,Width,Height))
        moreBtn.setBackgroundImage(shareImg, forState: .Normal)
        moreBtn.addTarget(self, action: #selector(IBProductsCollectionViewCell.moreButtonAction(_:)), forControlEvents: .TouchUpInside)
        bgImgView.addSubview(moreBtn)
        
        moreBigBtn = UIButton(frame : CGRectMake(moreBtn.frame.origin.x - 5,moreBtn.frame.origin.y - 5,moreBtn.frame.size.width + 10 , moreBtn.frame.size.height + 10))
        moreBigBtn.addTarget(self, action: #selector(IBProductsCollectionViewCell.moreButtonAction(_:)), forControlEvents: .TouchUpInside)
        bgImgView.addSubview(moreBigBtn)
        
        bgView.frame.size.height = bgImgView.frame.maxY
        self.frame.size.height = bgView.frame.maxY

    }
    
    func setValuesToControls(productImgStr : String ,productNameStr : String , productPriceStr : String , profileImgStr : String , userNameStr : String , dateStr :String , moreBtnTag : Int , quantityStr : String)
    {
        let productImgUrl = NSURL(string: productImgStr)
        productImgView.load(productImgUrl!, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
        
        productNameLbl.text = productNameStr
        priceLbl.text = productPriceStr
        
        let profilePicUrl = NSURL(string: profileImgStr)
        profileImgView.load(profilePicUrl!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
        
        userNameLbl.text = userNameStr
        dateLbl.text = dateStr
        moreBtn.tag = moreBtnTag
        moreBigBtn.tag = moreBtnTag
        
        let quantityInt = Int(quantityStr)
        if quantityInt == 0
        {
            soldOutImgView.hidden = false
        }
        else
        {
            soldOutImgView.hidden = true
        }
    }
    
    func moreButtonAction(sender : UIButton)
    {
        productDelegate!.moreButtonTapped(sender)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
