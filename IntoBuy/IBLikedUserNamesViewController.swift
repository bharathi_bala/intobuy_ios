//
//  IBLikedUserNamesViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 08/07/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBLikedUserNamesViewController: IBBaseViewController,UITableViewDelegate,UITableViewDataSource,searchProtocol {
    
    var m_likeTblView = UITableView()
    var likeArray = NSMutableArray()
    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        self.m_titleLabel.text = "Liked persons"
        self.hideSettingsBtn(true)
        self.hideBackBtn(false)
        self.hideBottomView(false)
        
        createTableView()
    }
    
    func createTableView()
    {
        m_likeTblView.frame = self.m_bgImageView.bounds
        m_likeTblView.delegate = self
        m_likeTblView.dataSource = self
        m_likeTblView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.m_bgImageView.addSubview(m_likeTblView)
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return likeArray.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70.0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Cell \(indexPath.row) \(indexPath.section)"
        var cell:IBSearchTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBSearchTableViewCell
        
        if (cell == nil) {
            cell = IBSearchTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        SwiftLoader.hide()
        cell?.imgDelegate = self
        let likeDict = likeArray.objectAtIndex(indexPath.row) as! NSDictionary
        cell?.valuesForControls(likeDict.objectForKey("avatar") as! String, name: likeDict.objectForKey("username") as! String, followState: "", trophyStr: "",isFromHome: false ,profileImgTag: indexPath.row)
        cell?.m_nameLabel.frame.origin.y = ((cell?.frame.size.height)! -  (cell?.m_nameLabel.frame.size.height)!)/2
        cell?.m_nameLabel.sizeToFit()
        addBottomBorder(cell!)
        cell?.selectionStyle = .None
        cell?.m_trophyLbl.hidden = true
        cell?.m_followBtn.hidden = true
        return cell!
    }
    func tapOnProfileImage(tapGesture : UITapGestureRecognizer)
    {
        let likeDict = likeArray.objectAtIndex((tapGesture.view?.tag)!) as! NSDictionary
        let myPageVC = IBMyPageViewController()
        myPageVC.m_isFromCameraBtn = false
        myPageVC.otherUserId = likeDict.objectForKey("userId") as! String
        self.navigationController?.pushViewController(myPageVC, animated: true)

    }
    func addBottomBorder(view: UIView)
    {
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRectMake(10, view.frame.size.height-1.0, GRAPHICS.Screen_Width() - 20, 1.0)
        bottomBorder.backgroundColor = UIColor.lightGrayColor().CGColor
        view.layer.addSublayer(bottomBorder)
    }
    
}
