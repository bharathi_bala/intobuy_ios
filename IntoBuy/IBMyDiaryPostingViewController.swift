//
//  IBMyDiaryPostingViewController.swift
//  IntoBuy
//
//  Created by SmaatApps on 12/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit


class IBMyDiaryPostingViewController: IBBaseViewController,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,AdobeUXImageEditorViewControllerDelegate, UIPopoverControllerDelegate,ServerAPIDelegate {
    
    var m_dateView:UIView = UIView()
    var m_datelabel:UILabel = UILabel()
    var m_notesTextView:UITextView = UITextView()
    var m_cameraButton:UIButton = UIButton()
    var m_saveButton:UIButton = UIButton()
    var m_imagePicker:UIImagePickerController = UIImagePickerController()
    var m_saveButtonView : UIView = UIView()
    var m_keyBoardToolbar:UIToolbar = UIToolbar()
    var m_scrollview:UIScrollView = UIScrollView()
    var m_diaryImageview:UIImageView = UIImageView()
    var m_isSelectedImage = false
   // var postDiaryDict = NSMutableDictionary()
    var dateStr = String()
    var diaryDict = NSMutableDictionary()
    var assetLibrary = ALAssetsLibrary()
    var shouldReleasePopover = Bool()
    var selectedImage = UIImage()
    var m_deleteImgView:UIImageView!
    var postImage1Data = NSData()
    var postImage2Data = NSData()
    var postImage3Data = NSData()
    var postImage4Data = NSData()
    //let diaryArray = NSMutableArray()
    let kAVYAviaryCilentIDKey = "7af03b5ab76e4ae7bc2bf7c1e07677f7";
    let kAVYAviarySecret = "db5690d4-ffaa-4a8f-a6a0-89ddad3264d2";

    
    
    var popover:UIPopoverController!
    
    
      override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.m_titleLabel.text = MyDiary
        m_bgImageView.image = GRAPHICS.PRODUCT_BG_IMAGE()
        self.hideBottomView(true)
        m_imagePicker.delegate = self
        self .initcontrols()
        self.changeFrameHeight()
      //  postDiaryDict.setValue(dateStr, forKey: "date")
        ////print(postDiaryDict)
        
//        ALAssetsLibrary * assetLibrary = [[ALAssetsLibrary alloc] init];
//        [self setAssetLibrary:assetLibrary];
//        [AdobeImageEditorOpenGLManager beginOpenGLLoad];
//        [AdobeImageEditorCustomization purgeGPUMemoryWhenPossible:YES];
        
        let assetsLib = ALAssetsLibrary()
        self.assetLibrary = assetsLib
        AdobeImageEditorOpenGLManager.beginOpenGLLoad();
        AdobeImageEditorCustomization.purgeGPUMemoryWhenPossible(true)
        AdobeUXAuthManager.sharedManager().setAuthenticationParametersWithClientID(kAVYAviaryCilentIDKey, withClientSecret: kAVYAviarySecret)
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
     }
    func createKeybaordToolbar()->UIToolbar
    {
       m_keyBoardToolbar.frame = CGRectMake(0, 0, 320, 40)
       m_keyBoardToolbar.barStyle = UIBarStyle.Default
     let spaceBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
     let doneButtonItem = UIBarButtonItem(title: Done, style:.Done, target: self, action: #selector(IBMyDiaryPostingViewController.doneButtonClicked))
        
        var items = [UIBarButtonItem]()
        
        items.append(spaceBarButtonItem)
        items.append(doneButtonItem)
        
        m_keyBoardToolbar.items = items
        
        return m_keyBoardToolbar
        
        }
 func doneButtonClicked()
 {
    
    m_notesTextView .resignFirstResponder()

}
    
    
    func initcontrols()
    {
        
    m_scrollview.frame = CGRectMake(GRAPHICS.Screen_X(), self.m_bgImageView.bounds.origin.y, GRAPHICS.Screen_Width(), GRAPHICS.Screen_Height()-self.m_headerView.frame.size.height);
    m_bgImageView .addSubview(m_scrollview)
        
        var xpos:CGFloat = 0
        var ypos:CGFloat = 0
        var width:CGFloat = GRAPHICS.Screen_Width()
        let height:CGFloat = 40
            
        m_dateView.frame = CGRectMake(xpos, ypos, width, height)
        m_dateView.backgroundColor = lightGrayColor
        m_scrollview .addSubview(m_dateView)
        
        xpos = 10;
        width = 120;
        
        let date = NSDate()
        let dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMM"
        
        let dateStr = dateFormatter.stringFromDate(date)

        
        //creationOflabel
        m_datelabel.frame = CGRectMake(xpos, ypos, width, height)
        m_datelabel.font = GRAPHICS.FONT_REGULAR(14)
        m_datelabel.text = dateStr
        m_datelabel.textColor = UIColor.whiteColor()
        m_dateView .addSubview(m_datelabel)
        
        
        /*
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "1.png")?.drawInRect(self.view.bounds)
        
        var image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        self.view.backgroundColor = UIColor(patternImage: image)
        */
        
        
        // Mark:-creationOf camerabutton
        
        xpos = 2
        ypos = m_dateView.frame.maxY+2
        width = GRAPHICS.Screen_Width()-4
        
        let imgSize:CGSize = self .getImageWidthHeight(GRAPHICS.MYDIARY_CAMERABUTTON_IMAGE())
        
       m_cameraButton.frame = CGRectMake(xpos, ypos, imgSize.width, imgSize.height)
        m_cameraButton .setBackgroundImage(GRAPHICS.MYDIARY_CAMERABUTTON_IMAGE(), forState: .Normal)
        m_cameraButton .addTarget(self, action:#selector(IBMyDiaryPostingViewController.cameraButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        m_scrollview .addSubview(m_cameraButton)
        
        //creating Diary imageview
        m_diaryImageview.frame = CGRectMake(0, ypos, GRAPHICS.Screen_Width(), 120)
        
        m_diaryImageview.contentMode = .ScaleAspectFit
//        m_diaryImageview.backgroundColor = UIColor.yellowColor()
        m_scrollview .addSubview(m_diaryImageview)
        
        
        ypos = m_cameraButton.frame.maxY+2
      //  width = GRAPHICS.Screen_Width()
        
        m_notesTextView.frame = CGRectMake(xpos, ypos, width,240)
        m_notesTextView.backgroundColor = lightYellowcolor
        m_notesTextView.text = AddText
        m_notesTextView.delegate = self;
        m_notesTextView.autocorrectionType = UITextAutocorrectionType.No
        m_notesTextView.font = GRAPHICS.FONT_REGULAR(14)
        m_notesTextView.layer.cornerRadius = 3.0
        m_scrollview .addSubview(m_notesTextView)
        
        ypos = m_notesTextView.frame.maxY
        xpos = 30;
        width = GRAPHICS.Screen_Width()-60
        
        m_saveButtonView.frame = CGRectMake(0, ypos, GRAPHICS.Screen_Width(), GRAPHICS.Screen_Height()-ypos)
        m_saveButtonView.backgroundColor = UIColor.whiteColor()
        m_scrollview .addSubview(m_saveButtonView)
        
         ypos = 30
        
        m_saveButton.frame = CGRectMake(xpos, ypos, width, 30)
        m_saveButton .setTitle(save, forState: .Normal)
        m_saveButton .setBackgroundImage(GRAPHICS.CART_QUANITY_CHCKOUTBTN_IMAGE(), forState: .Normal)
        m_saveButton .addTarget(self, action:#selector(IBMyDiaryPostingViewController.savebuttonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        m_saveButtonView .addSubview(m_saveButton)
        
        m_scrollview.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), m_saveButtonView.frame.maxY + 10)
        
        if IBSingletonClass.sharedInstance.m_isFromFirstImage
        {
            m_isSelectedImage = true
        }
        
    }
   func changeFrameHeight()
    {
        if !m_isSelectedImage
        {
            var ypos:CGFloat = m_dateView.frame.maxY+2
            
            m_diaryImageview.frame.size.height = 0;
            m_cameraButton.frame.origin.y = ypos
            
            ypos = m_cameraButton.frame.maxY+2
            m_notesTextView.frame.origin.y = ypos
            
            ypos = m_notesTextView.frame.maxY
            m_saveButtonView.frame.origin.y = ypos
            
            m_scrollview.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), m_saveButtonView.frame.maxY + 10)
            
        }
        else
        {
            
        m_diaryImageview.image = selectedImage
            var ypos:CGFloat = m_dateView.frame.maxY
            
            m_cameraButton.frame.size.height = 0;
            m_diaryImageview.frame.origin.y = ypos
            m_diaryImageview.frame.size.height = GRAPHICS.Screen_Height()/2.5;
            
            ypos = m_diaryImageview.frame.maxY+2
            m_notesTextView.frame.origin.y = ypos
            
            ypos = m_notesTextView.frame.maxY
            m_saveButtonView.frame.origin.y = ypos
        m_scrollview.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), m_saveButtonView.frame.maxY + 10)
        }
        
    }
    /*
    func validation()-> Bool
    {
      if(m_notesTextView.text.length <= 0)
    
    
    }
    
    */
    
    func savebuttonClicked(sender:UIButton)
    {
        self.view.endEditing(true)
        if m_isSelectedImage && m_notesTextView.text != "" && m_notesTextView.text != AddText
        {
            postImage1Data = UIImagePNGRepresentation(selectedImage)!
            
            let userIdStr = getUserIdFromUserDefaults()
            let todayDate = NSDate()
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFormatter.timeZone = NSTimeZone.localTimeZone()
            let todayDateStr = dateFormatter.stringFromDate(todayDate)

        
            SwiftLoader.show(title: "Loading...", animated: true)
            let serverApi = ServerAPI()
            serverApi.delegate = self
            let encodedStr = convertStringToEncodedString(m_notesTextView.text)
            serverApi.API_addDiaryFeed(userIdStr, date: todayDateStr, text: encodedStr, postrImage1: postImage1Data, postrImage2: postImage2Data, postrImage3: postImage3Data, postrImage4: postImage4Data)
            
//            let diaryArray =  postDiaryDict.objectForKey(dateStr)
//            diaryArray?.addObject(diaryDict)
//            let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//            appdelegate.diaryDictionary = postDiaryDict
//            self.navigationController?.popViewControllerAnimated(true)
        }
        else{
            showAlertViewWithMessage("You forgot to add something")
        }
    }
    func cameraButtonTapped(sender:UIButton)
    {
        self.view.endEditing(true)
        NSLog("camerabuttonclcied")
        self .createViewToDelete()
        
    }
    func createViewToDelete()
    {
        
        let bgImgView = GRAPHICS.MY_POST_TRANSPARENT_BG()
        var posX = GRAPHICS.Screen_X()
        var posY = GRAPHICS.Screen_Y()
        var width = GRAPHICS.Screen_Width()
        var height = GRAPHICS.Screen_Height()
        
        m_deleteImgView = UIImageView(frame: CGRectMake(posX, posY, width, height))
        m_deleteImgView.image = bgImgView
        m_deleteImgView.userInteractionEnabled = true
        self.m_mainView.addSubview(m_deleteImgView)
        
        let alertImg = GRAPHICS.MYDIARY_GALLERY_ALERT_IMAGE()
        width = alertImg.size.width/2
        height = alertImg.size.height/2
        posX = (GRAPHICS.Screen_Width() - width)/2
        posY = (GRAPHICS.Screen_Height() - height)/2
        let alertView = UIImageView(frame: CGRectMake(posX, posY, width, height))
        alertView.image = alertImg
        alertView.userInteractionEnabled = true
        m_deleteImgView.addSubview(alertView)
        
        posX = alertView.bounds.origin.x
        posY = alertView.bounds.origin.y
        height = 20
        width = alertView.frame.size.width
        let titleLbl = UILabel(frame: CGRectMake(posX, posY, width, height))
        titleLbl.text = "Image"
        titleLbl.font = GRAPHICS.FONT_REGULAR(14)
        titleLbl.textAlignment = .Center
        titleLbl.textColor = UIColor(red: 53.0/255.0, green: 36.0/255.0, blue: 198.0/255.0, alpha: 1.0)
        alertView.addSubview(titleLbl)
        
//        ChoosefromLibrary
//        takeAPhoto
        height = 30
        posY = alertView.frame.size.height - height
        let takeAPhotoBTn = UIButton(frame: CGRectMake(posX, posY, width, height))
        takeAPhotoBTn.setTitle(takeAPhoto, forState: .Normal)
        takeAPhotoBTn.setTitleColor(UIColor(red: 24.0/255.0, green: 24.0/255.0, blue: 24.0/255.0, alpha: 1.0), forState: .Normal)
        takeAPhotoBTn.titleLabel!.font = GRAPHICS.FONT_BOLD(12)
        takeAPhotoBTn.addTarget(self, action: #selector(IBMyDiaryPostingViewController.takePhotoBtnAction(_:)), forControlEvents: .TouchUpInside)
        addBottomBorder(takeAPhotoBTn)
        alertView.addSubview(takeAPhotoBTn)
        
        posY = takeAPhotoBTn.frame.origin.y - height
        let chooseFromLibBtn = UIButton(frame: CGRectMake(posX, posY, width, height))
        chooseFromLibBtn.setTitle(ChoosefromLibrary, forState: .Normal)
        chooseFromLibBtn.titleLabel!.font = GRAPHICS.FONT_BOLD(12)
        chooseFromLibBtn.setTitleColor(UIColor(red: 24.0/255.0, green: 24.0/255.0, blue: 24.0/255.0, alpha: 1.0), forState: .Normal)
        chooseFromLibBtn.addTarget(self, action: #selector(IBMyDiaryPostingViewController.chooseLibraryBtnAction(_:)), forControlEvents: .TouchUpInside)
        
        alertView.addSubview(chooseFromLibBtn)
    }
    func addBottomBorder(view: UIView)
    {
        //let lineImg = GRAPHICS.POST_ALERT_BORDER_LINE()
        let bottomBorder = UIImageView(frame: CGRectMake(view.frame.origin.x + 2, view.bounds.origin.y, view.frame.size.width - 4, 1.0))
        bottomBorder.backgroundColor = UIColor.lightGrayColor()
        view.addSubview(bottomBorder)
    }
    func chooseLibraryBtnAction(sender : UIButton)
    {
        self.galleryButtonTapped()
        m_deleteImgView.removeFromSuperview()
        m_deleteImgView = nil
    }
    func takePhotoBtnAction(sender : UIButton)
    {
        self.tappedOnCameraButton()
        m_deleteImgView.removeFromSuperview()
        m_deleteImgView = nil
    }
    
    
    //MARK:-ActionSheet Delegate methods
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex
        {
        case 0:
        
        actionSheet .dismissWithClickedButtonIndex(buttonIndex, animated: true)
        break
            
        case 1:
            self.tappedOnCameraButton()
            break
            
        case 2:
          self.galleryButtonTapped()
            break
            
        default:
            break
            
        }
        
    }
   
    func tappedOnCameraButton()
    {
       if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)
       {
        m_imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
//        m_imagePicker.allowsEditing = true
        self.presentViewController(m_imagePicker, animated: true, completion: nil)
        }
        else
       {
        
        showAlertViewWithMessage(cameraNotAvailable)
        
        }
    
    
    }
    
    func galleryButtonTapped()
    {
      m_imagePicker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
//        m_imagePicker.allowsEditing = true
      self.presentViewController(m_imagePicker, animated: true, completion: nil)
    
    }
    
//MARK: ImagePickerView Delagate Methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        
        let selectedImage : UIImage = info[UIImagePickerControllerEditedImage]as! UIImage
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Phone /*|| UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad*/
        {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else{
            self.dismissPopoverWithCompletion()
        }
        self.displayEditorForImage(selectedImage)
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        m_isSelectedImage = false
        self .changeFrameHeight()
        self .dismissViewControllerAnimated(true, completion: nil)
    }
    
    //adobe delegates
    func displayEditorForImage(imageToEdit : UIImage)
    {
        AdobeImageEditorOpenGLManager.requestOpenGLDataPurge()
        let editorViewController = AdobeUXImageEditorViewController(image: imageToEdit)
        editorViewController.delegate = self
        self.presentViewController(editorViewController, animated: false, completion: nil)
    }
    
    //MARK:-  Popover Methods
    func presentViewControllerInPopover(controller : UIViewController) -> () {
        
        
        let sourceRect = self.view.frame
        let popOverController = UIPopoverController(contentViewController: controller)
        popOverController.delegate = self
        self.popover = popOverController
        self.shouldReleasePopover = true
        popOverController .presentPopoverFromRect(sourceRect, inView: self.view, permittedArrowDirections: .Down, animated: true)
        
    }
    func dismissPopoverWithCompletion()
    {
        popover.dismissPopoverAnimated(true)
        self.popover = nil;
        
        //let delayinSeconds:NSTimeInterval = 0.5
        let popUpTime:dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW,  Int64(NSEC_PER_SEC))
        dispatch_after(popUpTime, dispatch_get_main_queue(),{
            });
    }
    func popoverControllerDidDismissPopover(popoverController : UIPopoverController)
    {
        if self.shouldReleasePopover {
            self.popover = nil
        }
        self.shouldReleasePopover = true
    }
    //MARK:- adobe delegate methods
    func photoEditor(editor: AdobeUXImageEditorViewController!, finishedWithImage image: UIImage!) {
        selectedImage = image
        m_isSelectedImage = true
        diaryDict.setValue(selectedImage, forKey: "image")
        changeFrameHeight()
        let defaults = NSUserDefaults()
        defaults.setBool(true, forKey: "isImagePickerPresent")
        self.dismissViewControllerAnimated(true, completion: nil)

    }
    
    func photoEditorCanceled(editor: AdobeUXImageEditorViewController!) {
       // [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isImagePickerPresent"];
        let defaults = NSUserDefaults()
        defaults.setBool(false, forKey: "isImagePickerPresent")
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK:-textView delegate Methods
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        m_scrollview.setContentOffset(CGPointMake(0, self.m_bgImageView.frame.origin.y + 216), animated: true)
        textView.inputAccessoryView = self.createKeybaordToolbar()
        return true
    }
    
    func textViewDidBeginEditing(textView: UITextView)
    {
        if textView.text == AddText
        {
            textView.text = ""
        }
    }
    func textViewDidEndEditing(textView: UITextView) {
        m_scrollview.setContentOffset(CGPointMake(0, 0), animated: true)
        if m_notesTextView.text != "" && m_notesTextView.text != AddText
        {
            diaryDict.setValue(m_notesTextView.text, forKey: "notes")
        }
    }
    func getImageWidthHeight(imageName:UIImage)->CGSize
    {
        var fWidth:CGFloat = 0.0
        var fHeight:CGFloat = 0.0
        
        let searchiew:UIImage = imageName
        fWidth = searchiew.size.width/2
        fHeight = searchiew.size.height/2
        
        if GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus
        {
            fWidth = searchiew.size.width/1.7
            fHeight = searchiew.size.height/1.7
        
        }
    
    return CGSizeMake(fWidth, fHeight)
      
    }
    override func shouldAutorotate() -> Bool {
        if UI_USER_INTERFACE_IDIOM() == .Pad
        {
            return UIInterfaceOrientationIsPortrait(.Portrait)
        }
        else
        {
            return true
        }

    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        self.shouldReleasePopover = false
        self.popover.dismissPopoverAnimated(true)
    }
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        if self.popover != nil{
            let popoverRef = self.view.frame
            self.popover.presentPopoverFromRect(popoverRef, inView: self.view, permittedArrowDirections: .Down, animated: true)
        }
    }
    // MARK: Api delegate
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    // api call back for get all users
    func API_CALLBACK_addDiaryFeed(resultDict: NSDictionary)
    {
        
        SwiftLoader.hide()
        if resultDict.objectForKey("error_code") as! String == "1"
        {
            showAlertViewWithMessage(resultDict.objectForKey("result") as! String)
            self.navigationController?.popViewControllerAnimated(true)
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
