//
//  IBBaseViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 25/11/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBBaseViewController: UIViewController {
    var m_mainView:UIView = UIView()
    var m_headerView:UIImageView = UIImageView()
    var m_bgImageView:UIImageView = UIImageView()
    var m_bottomView:UIView = UIView()
    var m_bigBackButton:UIButton = UIButton()
    var m_backButton:UIButton = UIButton()
    var m_bigSettingsButton:UIButton = UIButton()
    var m_settingsButton:UIButton = UIButton()
    var m_titleLabel:UILabel = UILabel()
    var m_bottomImageArr = NSMutableArray()
    var toShowActivityBtn = Bool()
    var m_isFromCameraBtn = Bool()
    var cartcountLbl : UILabel!
    var bottomVal = Int()
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        m_mainView.frame = CGRectMake(GRAPHICS.Screen_X(), GRAPHICS.Screen_Y(), GRAPHICS.Screen_Width(), GRAPHICS.Screen_Height());
        self.view .addSubview(m_mainView)
        self.createHeaderAndBgView()
        self.createBottomBar()
    }
    func createHeaderAndBgView()
    {
        var posX = m_mainView.bounds.origin.x
        var posY = m_mainView.bounds.origin.y
        var Width = GRAPHICS.Screen_Width()
        var height:CGFloat = 50
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            height = 60
        }
        
        m_headerView.frame = CGRectMake(posX, posY, Width, height)
        m_headerView.userInteractionEnabled = true
        m_headerView.backgroundColor = headerColor
        m_mainView.addSubview(m_headerView)
        
        
        posX = m_headerView.bounds.origin.x
        posY = m_headerView.bounds.origin.y
        Width = m_headerView.frame.size.width/5
        height = m_headerView.frame.size.height
        m_bigBackButton.frame = CGRectMake(posX, posY, Width, height)
        m_bigBackButton.addTarget(self, action: #selector(IBBaseViewController.backBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_headerView .addSubview(m_bigBackButton)
        
        
        
        
        let backImage = GRAPHICS .BACK_BUTTON_IMAGE()
        Width = (backImage?.size.width)!/2
        height = (backImage?.size.height)!/2;
        if (GRAPHICS.Screen_Type() == IPHONE_6) {
            Width = backImage!.size.width/1.8;
            height = backImage!.size.height/1.8;
        }
        else if (GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            Width = backImage!.size.width/1.5;
            height = backImage!.size.height/1.5;
        }
        posX = m_headerView.bounds.origin.x + 10
        posY = (m_headerView.frame.height - height) - 5
        m_backButton.frame = CGRectMake(posX, posY, Width, height)
        m_backButton.setBackgroundImage(backImage, forState: UIControlState.Normal)
        m_backButton.addTarget(self, action: #selector(IBBaseViewController.backBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_headerView .addSubview(m_backButton)
        
        Width = GRAPHICS.Screen_Width()/2
        height = m_headerView.frame.height/1.5
        posX = (GRAPHICS.Screen_Width() - Width)/2
        posY = (m_headerView.frame.size.height - height)/2
        m_titleLabel.frame = CGRectMake(posX, posY, Width, height)
        m_titleLabel.textColor = UIColor.whiteColor()
        m_titleLabel.font = GRAPHICS.FONT_JENNASUE(25)
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            m_titleLabel.font = GRAPHICS.FONT_JENNASUE(30)
        }
        else if GRAPHICS.Screen_Type() == IPHONE_4
        {
            m_titleLabel.font = GRAPHICS.FONT_JENNASUE(20)
        }
        m_titleLabel.textAlignment = NSTextAlignment.Center
        m_headerView.addSubview(m_titleLabel)
        
        
        
        posY = m_headerView.bounds.origin.y
        Width = m_headerView.frame.size.width/5
        posX = m_headerView.frame.size.width - Width
        height = m_headerView.frame.size.height
        m_bigSettingsButton.frame = CGRectMake(posX, posY, Width, height)
        m_bigSettingsButton.addTarget(self, action: #selector(IBBaseViewController.settingsBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_headerView .addSubview(m_bigSettingsButton)
        
        
        
        let settingsImg = GRAPHICS.SETTINGS_BUTTON_IMAGE()
        Width = (settingsImg?.size.width)!/2;
        height = (settingsImg?.size.height)!/2;
        if (GRAPHICS.Screen_Type() == IPHONE_6) {
            Width = settingsImg!.size.width/1.8;
            height = settingsImg!.size.height/1.8;
        }
        else if (GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            Width = settingsImg!.size.width/1.5;
            height = settingsImg!.size.height/1.5;
        }
        
        posX = m_headerView.frame.width - Width - 10
        posY = m_headerView.frame.height - height - 5
        m_settingsButton.frame = CGRectMake(posX, posY, Width, height)
        m_settingsButton.addTarget(self, action: #selector(IBBaseViewController.settingsBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        m_settingsButton.setBackgroundImage(settingsImg, forState: UIControlState.Normal)
        m_headerView .addSubview(m_settingsButton)
        
        
        
        posX = m_mainView.bounds.origin.x
        posY = m_headerView.frame.maxY;
        Width = GRAPHICS.Screen_Width()
        height = GRAPHICS.Screen_Height() - m_headerView.frame.height - 50
        m_bgImageView.frame = CGRectMake(posX, posY, Width, height)
        m_bgImageView.backgroundColor = UIColor.whiteColor()
        m_bgImageView.userInteractionEnabled = true
        m_mainView .addSubview(m_bgImageView)
        
    }
    
    func createBottomBar()
    {
        if(m_bottomImageArr.count > 0)
        {
            m_bottomImageArr.removeAllObjects()
        }
        m_bottomImageArr = [GRAPHICS.BOTTOM_BAR_HOME_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_PROFILE_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CAMERA_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_ACTIVITY_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CART_BTN_IMAGE()]//
        /* if(toShowActivityBtn)
         {
         m_bottomImageArr = [GRAPHICS.BOTTOM_BAR_HOME_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_PROFILE_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CAMERA_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_ACTIVITY_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CART_BTN_IMAGE()]//imageArrayForHomePage()
         }
         else{
         m_bottomImageArr = [GRAPHICS.BOTTOM_BAR_HOME_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_PROFILE_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CAMERA_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CHAT_ICON_BTN_IMAGE(),GRAPHICS.BOTTOM_BAR_CART_BTN_IMAGE()]//imageArrayForNotHomePage()
         }*/
        
        m_bottomView.frame = CGRectMake(GRAPHICS.Screen_X(), m_bgImageView.frame.maxY, GRAPHICS.Screen_Width(), 50);
        
        m_bottomView.backgroundColor = headerColor
        m_mainView.addSubview(m_bottomView)
        
        var paddingW:CGFloat = 20.0;
        if (GRAPHICS.Screen_Type() == IPHONE_6) {
            paddingW = 25;
        } else if (GRAPHICS.Screen_Type() == IPHONE_6_Plus) {
            paddingW = 30.0;
        }
        var xPos = m_bottomView.bounds.origin.x
        for i in 0 ..< m_bottomImageArr.count
        {
            
            if i == bottomVal
            {
                let bgImage = GRAPHICS.BOTTOM_BAR_SELECTION_BG_IMAGE()
                let contentSize = GRAPHICS.getImageWidthHeight(bgImage)
                let selectBgView = UIImageView(frame: CGRectMake(xPos /*(GRAPHICS.Screen_Width() - contentSize.width)/2*/, m_bottomView.bounds.origin.y, contentSize.width, m_bottomView.frame.size.height))
                selectBgView.image = bgImage
                selectBgView.userInteractionEnabled = true
                m_bottomView.addSubview(selectBgView)
            }
            
            let width = m_bottomView.frame.size.width/5
            let height = m_bottomView.frame.size.height
            let yPos = m_bottomView.bounds.origin.y
            let bottomBigBtns = UIButton(frame: CGRectMake(xPos,yPos, width, height))
            bottomBigBtns.tag = i
            bottomBigBtns .addTarget(self, action: #selector(IBBaseViewController.bottomBtnActions(_:)), forControlEvents: UIControlEvents.TouchUpInside);
            m_bottomView.addSubview(bottomBigBtns)
            xPos = xPos + width
            
            let btnImage =  m_bottomImageArr[i] as! UIImage
            let btnW = (btnImage.size.width)/2
            let btnH = (btnImage.size.height)/2
            let btnY = (m_bottomView.frame.size.height - btnH)/2
            let padding:CGFloat!
            if GRAPHICS.Screen_Type() == IPHONE_4 || GRAPHICS.Screen_Type() == IPHONE_5
            {
                padding = 3
            }
            else
            {
                padding = -0.5
            }
            let btnX = bottomBigBtns.frame.maxX - (bottomBigBtns.frame.size.width - btnW) - padding//(m_bottomView.frame.size.height - btnH)/2
            
            let bottomBtns = UIButton(frame: CGRectMake(btnX,btnY, btnW, btnH))
            bottomBtns .setBackgroundImage(btnImage, forState: UIControlState.Normal)
            bottomBtns.tag = i
            bottomBtns .addTarget(self, action: #selector(IBBaseViewController.bottomBtnActions(_:)), forControlEvents: UIControlEvents.TouchUpInside);
            m_bottomView.addSubview(bottomBtns)
            m_bottomView.bringSubviewToFront(bottomBtns)
            
            
            if i == 4
            {
                let width:CGFloat = 20
                let height:CGFloat = 20
                cartcountLbl = UILabel(frame:CGRectMake((m_bottomView.frame.size.width - width),bottomBtns.frame.origin.y - height/2,width,height))
                cartcountLbl.backgroundColor = UIColor.redColor()
                cartcountLbl.textAlignment = .Center
                cartcountLbl.textColor = UIColor.whiteColor()
                cartcountLbl.font = GRAPHICS.FONT_REGULAR(14)
                m_bottomView.addSubview(cartcountLbl)
                m_bottomView.bringSubviewToFront(cartcountLbl)
                cartcountLbl.layer.cornerRadius = height/2
                cartcountLbl.clipsToBounds = true
                let cartCountStr = getCartCountFromUserDefaults()
                if cartCountStr != ""
                {
                    cartcountLbl.text = cartCountStr
                }
                else
                {
                    cartcountLbl.hidden = true
                }
            }
            
            paddingW = bottomBtns.frame.maxX+40.0;
            if (GRAPHICS.Screen_Type() == IPHONE_6) {
                paddingW = bottomBtns.frame.maxX+50.0;
            }else if (GRAPHICS.Screen_Type() == IPHONE_6_Plus) {
                paddingW = bottomBtns.frame.maxX+60.0;
            }
            
            let lineViw = UIView(frame:CGRectMake(bottomBtns.frame.maxX + paddingW - 1,0,1,m_bottomView.frame.size.height))
            lineViw.backgroundColor = applightwhitecolor
           // m_bottomView.addSubview(lineViw)
            m_bottomView.bringSubviewToFront(lineViw)
            
        }
        
    }
    func bottomBtnActions(sender : UIButton)
    {
        if sender.tag == 0
        {
            if self.isKindOfClass(IBHomePageVcViewController)
            {
                
            }
            else{
                let homePageVc = IBHomePageVcViewController()
                toShowActivityBtn = true
                self.navigationController?.pushViewController(homePageVc, animated: false)
            }
            
        }
        else if sender.tag == 1
        {
            if self.isKindOfClass(IBMyPageViewController)
            {
                
            }
            else{
                let myPageVc = IBMyPageViewController()
                myPageVc.otherUserId = getUserIdFromUserDefaults()
                myPageVc.m_isFromCameraBtn = true
                toShowActivityBtn = false
                self.navigationController?.pushViewController(myPageVc, animated: false)
            }
            
        }
        else if sender.tag == 2
        {
            //let cameraVc = IBCameraViewController()
            //self.navigationController?.pushViewController(cameraVc, animated: true)
            m_isFromCameraBtn = true
            IBSingletonClass .sharedInstance.m_isFromFirstImage = false
            IBSingletonClass .sharedInstance.m_isFromSecondImage = false
            IBSingletonClass .sharedInstance.m_isFromThirdImage = false
            IBSingletonClass .sharedInstance.m_isFromFourthImage = false
            IBSingletonClass.sharedInstance.isFromUpload = true
            IBSingletonClass.sharedInstance.screenIndex = 1
            IBSingletonClass.sharedInstance.m_UploadImageIndex = 300
            IBSingletonClass.sharedInstance.toEditProduct = false
            IBSingletonClass.sharedInstance.isFromImageTap = false
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let ycameraViewVc = YCameraViewController()
            
            let camNav = UINavigationController(rootViewController: ycameraViewVc)
            
            camNav .setNavigationBarHidden(true, animated: false)
            appDelegate.navController!.presentViewController(camNav, animated: false, completion: nil);
        }
        else if sender.tag == 3
        {
            if self.isKindOfClass(IBActivityViewController)
            {
                
            }
            else{
                let myPageVc = IBActivityViewController()
                self.navigationController?.pushViewController(myPageVc, animated: false)
            }
            
            /* if toShowActivityBtn
             {
             if self.isKindOfClass(IBActivityViewController)
             {
             
             }
             else{
             let myPageVc = IBActivityViewController()
             self.navigationController?.pushViewController(myPageVc, animated: false)
             }
             
             }
             else{
             if self.isKindOfClass(IBContactsViewController)
             {
             
             }
             else{
             let myPageVc = IBContactsViewController()
             self.navigationController?.pushViewController(myPageVc, animated: false)
             }
             
             }*/
        }
        else
        {
            if self.isKindOfClass(IBListOfCartsViewController)
            {
                
            }
            else{
                let myPageVc = IBListOfCartsViewController()
                toShowActivityBtn = false
                self.navigationController?.pushViewController(myPageVc, animated: false)
            }
            
        }
    }
    func hideHeaderViewForView(hideHeaderView: Bool)
    {
        if(hideHeaderView)
        {
            m_headerView.hidden = true
            let height = GRAPHICS.Screen_Height()
            m_bgImageView.frame = CGRectMake(GRAPHICS .Screen_X(), m_mainView.bounds.origin.y, GRAPHICS.Screen_Width(), height)
            //m_bgImageView.frame.size.height = height
        }
        else
        {
            m_headerView.hidden = false
            let posY = m_headerView.frame.maxY
            let height = GRAPHICS.Screen_Height() - m_headerView.frame.size.height
            m_bgImageView.bounds.origin.y = posY
            m_bgImageView.frame.size.height = height
        }
    }
    func hideBottomView(hideBottomView : Bool)
    {
        if(hideBottomView)
        {
            m_bottomView.hidden = true
            let height = GRAPHICS.Screen_Height() - m_headerView.frame.size.height
            m_bgImageView.frame.size.height = height
        }
        else
        {
            m_bottomView.hidden = false
            let height = GRAPHICS.Screen_Height() - m_headerView.frame.size.height - m_bottomView.frame.size.height
            m_bgImageView.frame.size.height = height
        }
        
    }
    
    func hideSettingsBtn(hideSettingsBtn : Bool)
    {
        if(hideSettingsBtn)
        {
            m_settingsButton.hidden = true
            m_bigSettingsButton.hidden = true
        }
        else
        {
            m_settingsButton.hidden = false
            m_bigSettingsButton.hidden = false
        }
    }
    func hideBackBtn(hideBackBtn : Bool)
    {
        if(hideBackBtn)
        {
            m_bigBackButton.hidden = true
            m_backButton.hidden = true
        }
        else
        {
            m_bigBackButton.hidden = false
            m_backButton.hidden = false
        }
    }
    
    func backBtnAction()
    {
        self.navigationController! .popViewControllerAnimated(true)
    }
    func settingsBtnAction()
    {
        let settingsVC = IBSettingsViewController()
        self.navigationController!.pushViewController(settingsVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
