//
//  IBActivityYouEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 04/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBActivityYouEntity: NSObject {
    
    var newsId: String
    var wall_id: String
    var userId: String
    var toUserId: String
    var type: String
    var text: String
    var mainimage: String
    var addedDate: String
    var touseravatar: String
    var tousername: String
    var useravatar: String
    var username: String
    var images: NSArray
    
    
    init(dict: NSDictionary)
    {
        self.newsId = (GRAPHICS.checkKeyNotAvail(dict, key:"newsId")as? String)!
        self.wall_id = (GRAPHICS.checkKeyNotAvail(dict, key:"wall_id")as? String)!
        self.userId = (GRAPHICS.checkKeyNotAvail(dict, key:"userId")as? String)!
        self.toUserId = (GRAPHICS.checkKeyNotAvail(dict, key:"toUserId")as? String)!
        self.type = (GRAPHICS.checkKeyNotAvail(dict, key:"type")as? String)!
        self.text = (GRAPHICS.checkKeyNotAvail(dict, key:"text")as? String)!
        self.mainimage = (GRAPHICS.checkKeyNotAvail(dict, key:"mainimage")as? String)!
        self.addedDate = (GRAPHICS.checkKeyNotAvail(dict, key:"addedDate")as? String)!
        self.touseravatar = (GRAPHICS.checkKeyNotAvail(dict, key:"touseravatar")as? String)!
        self.tousername = (GRAPHICS.checkKeyNotAvail(dict, key:"tousername")as? String)!
        self.useravatar = (GRAPHICS.checkKeyNotAvail(dict, key:"useravatar")as? String)!
        self.username = (GRAPHICS.checkKeyNotAvail(dict, key:"username")as? String)!
        self.images = (GRAPHICS.checkKeyNotAvailForArray(dict, key:"images")as? NSArray)!
    }
    
}
