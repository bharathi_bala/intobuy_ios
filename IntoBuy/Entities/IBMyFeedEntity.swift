//
//  IBMyFeedEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 21/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBMyFeedEntity: NSObject {
    
    
    var wall_id: NSString
    var wall_type: NSString
    var userId: NSString
    var username : NSString
    var userProfile : NSString
    var post_heading : NSString
    var post_text : NSString
    var post_image : NSString
    var images : NSArray
    var added_date : NSString
    var status : NSString
    var isLiked : NSString
    var likedUserdetails : NSArray
    var isCommented : NSString
    var Comments : NSArray
    var product_id : NSString
    var seller_id : NSString
    var product_name : NSString
    var product_image : NSString
    var catogery_name:NSString
    var facebook:NSString
    var twitter:NSString
    var is_in_cart:NSString
    
    init(dict: NSDictionary) {
        self.wall_id = (GRAPHICS.checkKeyNotAvail(dict, key:"wall_id")as? NSString)!
        self.wall_type = (GRAPHICS.checkKeyNotAvail(dict, key:"wall_type")as? NSString)!
        self.userId = (GRAPHICS.checkKeyNotAvail(dict, key:"userId")as? NSString)!
        self.username = (GRAPHICS.checkKeyNotAvail(dict, key:"username")as? NSString)!
        self.userProfile = (GRAPHICS.checkKeyNotAvail(dict, key:"userProfile")as? NSString)!
        self.post_heading = (GRAPHICS.checkKeyNotAvail(dict, key:"post_heading")as? NSString)!
        self.post_text = (GRAPHICS.checkKeyNotAvail(dict, key:"post_text")as? NSString)!
        self.post_image = (GRAPHICS.checkKeyNotAvail(dict, key:"post_image")as? NSString)!
        self.images =  dict.objectForKey("images")as! NSArray//(dict, key:"images")as NSArray//(GRAPHICS.checkKeyNotAvail(dict, key:"images")as! NSArray)
        self.added_date = (GRAPHICS.checkKeyNotAvail(dict, key:"added_date")as? NSString)!
        self.status = (GRAPHICS.checkKeyNotAvail(dict, key:"status")as? NSString)!
        self.isLiked = (GRAPHICS.checkKeyNotAvail(dict, key:"isLiked")as? NSString)!
        self.isCommented = (GRAPHICS.checkKeyNotAvail(dict, key:"isCommented")as? NSString)!
        self.Comments = dict.objectForKey("Comments")as! NSArray//(GRAPHICS.checkKeyNotAvail(dict, key:"Comments")as! NSArray)
        self.likedUserdetails = dict.objectForKey("likedUserdetails")as! NSArray//(GRAPHICS.checkKeyNotAvail(dict, key:"likedUserNames")as? NSString)!
        self.product_id = (GRAPHICS.checkKeyNotAvail(dict, key:"product_id")as? NSString)!
        self.seller_id = (GRAPHICS.checkKeyNotAvail(dict, key:"seller_id")as? NSString)!
        self.product_name = (GRAPHICS.checkKeyNotAvail(dict, key:"product_name")as? NSString)!
        self.product_image = (GRAPHICS.checkKeyNotAvail(dict, key:"product_image")as? NSString)!
        self.catogery_name = (GRAPHICS.checkKeyNotAvail(dict, key:"catogery_name")as? NSString)!
        self.facebook = (GRAPHICS.checkKeyNotAvail(dict, key:"facebook")as? NSString)!
        self.twitter = (GRAPHICS.checkKeyNotAvail(dict, key:"twitter")as? NSString)!
        self.is_in_cart = (GRAPHICS.checkKeyNotAvail(dict, key:"is_in_cart")as? NSString)!
        
    }


}
