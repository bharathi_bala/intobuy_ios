//
//  IBBillingDetailsEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 01/07/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBBillingDetailsEntity: NSObject {
   
    var billingId:String
    var userId:String
    var card_number:String
    var address:String
    var address2:String
    var city:String
    var state:String
    var country:String
    var zip:String
    var expiry_month:String
    var expiry_year:String
    var cvc:String
    var status:String
    var country_name:String
    var card_owner_name:String
    
    init(dict: NSDictionary) {
        self.billingId = (GRAPHICS.checkKeyNotAvail(dict, key:"billingId")as? String)!
        self.userId = (GRAPHICS.checkKeyNotAvail(dict, key:"userId")as? String)!
        self.card_number = (GRAPHICS.checkKeyNotAvail(dict, key:"card_number")as? String)!
        self.address = (GRAPHICS.checkKeyNotAvail(dict, key:"address")as? String)!
        self.address2 = (GRAPHICS.checkKeyNotAvail(dict, key:"address2")as? String)!
        self.city = (GRAPHICS.checkKeyNotAvail(dict, key:"city")as? String)!
        self.state = (GRAPHICS.checkKeyNotAvail(dict, key:"state")as? String)!
        self.country = (GRAPHICS.checkKeyNotAvail(dict, key:"country")as? String)!
        self.zip = (GRAPHICS.checkKeyNotAvail(dict, key:"zip")as? String)!
        self.expiry_month = (GRAPHICS.checkKeyNotAvail(dict, key:"expiry_month")as? String)!
        self.expiry_year = (GRAPHICS.checkKeyNotAvail(dict, key:"expiry_year")as? String)!
        self.cvc = (GRAPHICS.checkKeyNotAvail(dict, key: "cvc")as! String)
        self.status = (GRAPHICS.checkKeyNotAvail(dict, key:"status")as? String)!
        self.country_name = (GRAPHICS.checkKeyNotAvail(dict, key:"country_name")as? String)!
        self.card_owner_name = (GRAPHICS.checkKeyNotAvail(dict, key:"card_owner_name")as? String)!
    }
}
