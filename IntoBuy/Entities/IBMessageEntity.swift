//
//  IBMessageEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 22/07/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBMessageEntity: NSObject {
    
    var msgId:String
    var chatId:String
    var msgFrom:String
    var FromUsername:String
    var FromAvatar:String
    var msgTo:String
    var ToUsername:String
    var ToAvatar:String
    var message:String
    var msgStatus:String
    var addedDate:String
    
    init(dict: NSDictionary) {
        self.msgId = (GRAPHICS.checkKeyNotAvail(dict, key:"msgId")as? String)!
        self.chatId = (GRAPHICS.checkKeyNotAvail(dict, key:"chatId")as? String)!
        self.msgFrom = (GRAPHICS.checkKeyNotAvail(dict, key:"msgFrom")as? String)!
        self.FromUsername = (GRAPHICS.checkKeyNotAvail(dict, key:"FromUsername")as? String)!
        self.FromAvatar = (GRAPHICS.checkKeyNotAvail(dict, key:"FromAvatar")as? String)!
        self.msgTo = (GRAPHICS.checkKeyNotAvail(dict, key:"msgTo")as? String)!
        self.ToUsername = (GRAPHICS.checkKeyNotAvail(dict, key:"ToUsername")as? String)!
        self.ToAvatar = (GRAPHICS.checkKeyNotAvail(dict, key:"ToAvatar")as? String)!
        self.message = (GRAPHICS.checkKeyNotAvail(dict, key:"message")as? String)!
        self.msgStatus = (GRAPHICS.checkKeyNotAvail(dict, key:"msgStatus")as? String)!
        self.addedDate = (GRAPHICS.checkKeyNotAvail(dict, key:"addedDate")as? String)!
    }
}
