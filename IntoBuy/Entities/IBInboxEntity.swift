//
//  IBInboxEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 27/07/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBInboxEntity: NSObject {

    var FromAvatar:String!
    var FromUsername:String!
    var ToAvatar:String!
    var ToUsername:String!
    var addedDate:String!
    var chatId:String!
    var message:String!
    var msgFrom:String!
    var msgId:String!
    var msgStatus:String!
    var msgTo:String!
    var did:String!
    var productId:String!
    var productname:String!
    var mainImage:String!

    
    init(dict: NSMutableDictionary) {
        self.FromAvatar = (GRAPHICS.checkKeyNotAvail(dict, key:"FromAvatar")as? String)!
        self.FromUsername = (GRAPHICS.checkKeyNotAvail(dict, key:"FromUsername")as? String)!
        self.ToAvatar = (GRAPHICS.checkKeyNotAvail(dict, key:"ToAvatar")as? String)!
        self.ToUsername = (GRAPHICS.checkKeyNotAvail(dict, key:"ToUsername")as? String)!
        self.addedDate = (GRAPHICS.checkKeyNotAvail(dict, key:"addedDate")as? String)!
        self.chatId = (GRAPHICS.checkKeyNotAvail(dict, key:"chatId")as? String)!
        self.message = (GRAPHICS.checkKeyNotAvail(dict, key:"message")as? String)!
        self.msgFrom = (GRAPHICS.checkKeyNotAvail(dict, key:"msgFrom")as? String)!
        self.msgId = (GRAPHICS.checkKeyNotAvail(dict, key:"msgId")as? String)!
        self.msgStatus = (GRAPHICS.checkKeyNotAvail(dict, key:"msgStatus")as? String)!
        self.msgTo = (GRAPHICS.checkKeyNotAvail(dict, key:"msgTo")as? String)!
        self.did = (GRAPHICS.checkKeyNotAvail(dict, key:"did")as? String)!
        self.productId = (GRAPHICS.checkKeyNotAvail(dict, key:"productId")as? String)!
        self.productname = (GRAPHICS.checkKeyNotAvail(dict, key:"productname")as? String)!
        self.mainImage = (GRAPHICS.checkKeyNotAvail(dict, key:"mainImage")as? String)!
    }
}
