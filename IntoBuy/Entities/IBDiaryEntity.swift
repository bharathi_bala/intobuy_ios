//
//  IBDiaryEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 27/06/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBDiaryEntity: NSObject {
    
    var id: String
    var userId: String
    var post_text: String
    var post_image : String
    var added_date : String
    var expiary_date : String
    var status : String
    
    init(dict: NSDictionary) {
        self.userId = (GRAPHICS.checkKeyNotAvail(dict, key:"userId")as? String)!
        self.id = (GRAPHICS.checkKeyNotAvail(dict, key:"id")as? String)!
        self.post_text = (GRAPHICS.checkKeyNotAvail(dict, key:"post_text")as? String)!
        self.post_image = (GRAPHICS.checkKeyNotAvail(dict, key:"post_image")as? String)!
        self.added_date = (GRAPHICS.checkKeyNotAvail(dict, key:"added_date")as? String)!
        self.expiary_date = (GRAPHICS.checkKeyNotAvail(dict, key:"expiary_date")as? String)!
        self.status = (GRAPHICS.checkKeyNotAvail(dict, key:"status")as? String)!
    }

}
