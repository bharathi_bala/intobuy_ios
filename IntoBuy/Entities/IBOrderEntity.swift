//
//  IBOrderEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 05/07/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBOrderEntity: NSObject {
    
    var user_status:String
    var transactionId:String
    var orderDate:String
    var status:String
    var userId:String
    var uniqueOID:String
    var productId:String
    var quantity:String
    var price:String
    var images:NSArray
    var billingaddress:NSArray
    var shippingaddress:NSArray
    var sellserdetails:NSArray
    var userdetails:NSArray
    var orderId:String
    var productimage:String
    var productname:String
    
    init(dict: NSDictionary) {
        self.user_status = (GRAPHICS.checkKeyNotAvail(dict, key:"user_status")as? String)!
        self.transactionId = (GRAPHICS.checkKeyNotAvail(dict, key:"transactionId")as? String)!
        self.orderDate = (GRAPHICS.checkKeyNotAvail(dict, key:"orderDate")as? String)!
        self.status = (GRAPHICS.checkKeyNotAvail(dict, key:"status")as? String)!
        self.userId = (GRAPHICS.checkKeyNotAvail(dict, key:"userId")as? String)!
        self.uniqueOID = (GRAPHICS.checkKeyNotAvail(dict, key:"uniqueOID")as? String)!
        self.productId = (GRAPHICS.checkKeyNotAvail(dict, key:"productId")as? String)!
        self.quantity = (GRAPHICS.checkKeyNotAvail(dict, key:"quantity")as? String)!
        self.price = (GRAPHICS.checkKeyNotAvail(dict, key:"price")as? String)!
        self.images = (GRAPHICS.checkKeyNotAvailForArray(dict, key: "images")as! NSArray)
        self.billingaddress = (GRAPHICS.checkKeyNotAvailForArray(dict, key: "billingaddress")as! NSArray)
        self.shippingaddress = (GRAPHICS.checkKeyNotAvailForArray(dict, key: "shippingaddress")as! NSArray)
        self.sellserdetails = (GRAPHICS.checkKeyNotAvailForArray(dict, key: "sellserdetails")as! NSArray)
        self.userdetails = (GRAPHICS.checkKeyNotAvailForArray(dict, key: "userdetails")as! NSArray)
        self.orderId = (GRAPHICS.checkKeyNotAvail(dict, key:"orderId")as? String)!
        self.productimage = (GRAPHICS.checkKeyNotAvail(dict, key:"productimage")as? String)!
        self.productname = (GRAPHICS.checkKeyNotAvail(dict, key:"productname")as? String)!
    }

}
