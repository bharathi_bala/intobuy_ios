//
//  IBProductDetailsEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 06/06/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBProductDetailsEntity: NSObject {
    
    var productId: NSString
    var sellerId: NSString
    var catId: NSString
    var subCatId : NSString
    var reviewcount : NSString
    var productname : NSString
    var descriptionStr : NSString
    var mainImage : NSString
    var images : NSArray
    var terms : NSString
    var quantity : NSString
    var price : NSString
    var sellername : NSString
    var avatar : NSString
    var addedDate : NSString
    var stockavailability : NSString
    var rating : NSString
    var address : NSString
    var comments : NSArray
    var latitude : NSString
    var logitude : NSString
    var facebook : NSString
    var shareStatus : NSString
    var shareworldStatus : NSString
    var shareworldStatusTwitter : NSString
    var status : NSString
    var sub_catogery_name : NSString
    var twitter : NSString
    var wall_id : NSString
    
    //Product detal page Comment Entity
    var added_date : NSString
    var comment : NSString
    var userId : NSString
    var userProfile : NSString
    var username : NSString
    
    init(dict: NSDictionary)
    {
        self.productId = (GRAPHICS.checkKeyNotAvail(dict, key:"productId")as? NSString)!
        self.sellerId = (GRAPHICS.checkKeyNotAvail(dict, key:"sellerId")as? NSString)!
        self.catId = (GRAPHICS.checkKeyNotAvail(dict, key:"catId")as? NSString)!
        self.subCatId = (GRAPHICS.checkKeyNotAvail(dict, key:"subCatId")as? NSString)!
        self.reviewcount = (GRAPHICS.checkKeyNotAvail(dict, key:"reviewcount")as? NSString)!
        self.productname = (GRAPHICS.checkKeyNotAvail(dict, key:"productname")as? NSString)!
        self.descriptionStr = (GRAPHICS.checkKeyNotAvail(dict, key:"description")as? NSString)!
        self.mainImage = (GRAPHICS.checkKeyNotAvail(dict, key:"mainImage")as? NSString)!
        self.images = (GRAPHICS.checkKeyNotAvailForArray(dict, key:"images")as? NSArray)!
         //(dict, key:"images")as NSArray//(GRAPHICS.checkKeyNotAvail(dict, key:"images")as! NSArray)
        self.terms = (GRAPHICS.checkKeyNotAvail(dict, key:"terms")as? NSString)!
        self.quantity = (GRAPHICS.checkKeyNotAvail(dict, key:"quantity")as? NSString)!
        self.price = (GRAPHICS.checkKeyNotAvail(dict, key:"price")as? NSString)!
        self.sellername = (GRAPHICS.checkKeyNotAvail(dict, key:"sellername")as? NSString)!
        self.comments = (GRAPHICS.checkKeyNotAvailForArray(dict, key: "comments")as! NSArray)
        self.avatar = (GRAPHICS.checkKeyNotAvail(dict, key:"avatar")as? NSString)!
        self.stockavailability = (GRAPHICS.checkKeyNotAvail(dict, key:"stockavailability")as? NSString)!
        self.rating = (GRAPHICS.checkKeyNotAvail(dict, key:"rating")as? NSString)!
        self.address = (GRAPHICS.checkKeyNotAvail(dict, key:"address")as? NSString)!
        self.addedDate = (GRAPHICS.checkKeyNotAvail(dict, key:"addedDate")as? NSString)!
        self.latitude = (GRAPHICS.checkKeyNotAvail(dict, key:"latitude")as? NSString)!
        self.logitude = (GRAPHICS.checkKeyNotAvail(dict, key:"logitude")as? NSString)!
        self.facebook = (GRAPHICS.checkKeyNotAvail(dict, key:"facebook")as? NSString)!
        self.shareStatus = (GRAPHICS.checkKeyNotAvail(dict, key:"shareStatus")as? NSString)!
        self.shareworldStatus = (GRAPHICS.checkKeyNotAvail(dict, key:"shareworldStatus")as? NSString)!
        self.shareworldStatusTwitter = (GRAPHICS.checkKeyNotAvail(dict, key:"shareworldStatusTwitter")as? NSString)!
        self.status = (GRAPHICS.checkKeyNotAvail(dict, key:"status")as? NSString)!
        self.sub_catogery_name = (GRAPHICS.checkKeyNotAvail(dict, key:"sub_catogery_name")as? NSString)!
        self.twitter = (GRAPHICS.checkKeyNotAvail(dict, key:"twitter")as? NSString)!
        
        self.added_date = (GRAPHICS.checkKeyNotAvail(dict, key:"added_date")as? NSString)!
        self.comment = (GRAPHICS.checkKeyNotAvail(dict, key:"comment")as? NSString)!
        self.userId = (GRAPHICS.checkKeyNotAvail(dict, key:"userId")as? NSString)!
        self.userProfile = (GRAPHICS.checkKeyNotAvail(dict, key:"userProfile")as? NSString)!
        self.username = (GRAPHICS.checkKeyNotAvail(dict, key:"username")as? NSString)!
        self.wall_id = (GRAPHICS.checkKeyNotAvail(dict, key:"wall_id")as? NSString)!
        
    }


    
    
}
