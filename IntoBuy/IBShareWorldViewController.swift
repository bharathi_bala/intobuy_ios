//
//  IBShareWorldViewController.swift
//  IntoBuy
//
//  Created by SmaatApps on 19/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class IBShareWorldViewController: IBBaseViewController,ServerAPIDelegate,MKMapViewDelegate,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,ShareWorldProtocol
{
    let m_mapview: MKMapView = MKMapView()
    var m_shareWorldListArray:NSMutableArray = NSMutableArray()
    var locationManger : CLLocationManager = CLLocationManager()
    var pointanotation : MKPointAnnotation = MKPointAnnotation()
    var shareWorldTblView = UITableView()
    var touchMapCoordinate : CLLocationCoordinate2D = CLLocationCoordinate2D()
    var imgIndex = 0
    var buttonView = UIView()
    var shareWorldRandomArray = NSMutableArray()
    var tapGestureTag = NSInteger()
    var randomCount = NSInteger()
    var  currentLocation = MKCoordinateRegion()
    //let calloutAnnotation : CalloutAnnotation?
    var shufflebtnIndex = 1
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.m_titleLabel.text = map_Share_world_title
        self.hideSettingsBtn(true)
        self.hideHeaderViewForView(false)
        self.hideBottomView(true)
        self.shareWorldApiCall()
        
    }
    func createMapview()
    {
        
        m_mapview.frame = CGRectMake(0, self.m_bgImageView.bounds.origin.y,GRAPHICS.Screen_Width(), self.m_bgImageView.frame.size.height)
        m_mapview.zoomEnabled = true
        m_mapview.scrollEnabled = true
        m_mapview.delegate = self
        m_bgImageView .addSubview(m_mapview)
        
        
        //locationManager
        locationManger.delegate = self
        locationManger.requestAlwaysAuthorization()
        locationManger.requestWhenInUseAuthorization()
        locationManger.startUpdatingLocation()
        
    }
    
    func createTableViewAndBottomView()
    {
        let shuffleImg = GRAPHICS.SHARE_WORLD_SHUFFLE_BTN_IMAGE()
        var contentSize = GRAPHICS.getImageWidthHeight(shuffleImg)

        var width = GRAPHICS.Screen_Width()
        var height = contentSize.height + 20//contentSize.height
        var xPos = GRAPHICS.Screen_X()
        var yPos = GRAPHICS.Screen_Height() - height

        buttonView.frame = CGRectMake(xPos,yPos,width,height)
        buttonView.backgroundColor = UIColor.clearColor()
        self.m_bgImageView.addSubview(buttonView)
        self.m_bgImageView.bringSubviewToFront(buttonView)
        
        width = contentSize.width
        height = contentSize.height
        xPos = GRAPHICS.Screen_X() + 10
        yPos = (buttonView.frame.size.height - height)/2
        
        let shuffleBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        shuffleBtn.setImage(shuffleImg, forState: .Normal)
        shuffleBtn.addTarget(self, action: #selector(IBShareWorldViewController.shuffleBtnAction), forControlEvents: .TouchUpInside)
        buttonView.addSubview(shuffleBtn)
        
        let homeImg = GRAPHICS.SHARE_WORLD_HOME_BTN_IMAGE()
        contentSize = GRAPHICS.getImageWidthHeight(homeImg)
        width = contentSize.width
        height = contentSize.height
        xPos = shuffleBtn.frame.maxX + 10
        let homeBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        homeBtn.setImage(homeImg, forState: .Normal)
        homeBtn.addTarget(self, action: #selector(IBShareWorldViewController.homeBtnAction), forControlEvents: .TouchUpInside)
        buttonView.addSubview(homeBtn)
//        buttonView.frame.size.height = homeBtn.frame.maxY + 10
//        buttonView.frame.origin.y = self.m_bgImageView.frame.size.height - buttonView.frame.size.height
        
        shareWorldTblView.frame = CGRectMake(GRAPHICS.Screen_X(), GRAPHICS.Screen_Height(), GRAPHICS.Screen_Width(), self.m_bgImageView.frame.size.height/3)
        shareWorldTblView.delegate = self
        shareWorldTblView.dataSource = self
        shareWorldTblView.separatorStyle = .None
        shareWorldTblView.pagingEnabled = true
        self.m_bgImageView.addSubview(shareWorldTblView)
        
        let upSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(IBShareWorldViewController.moveTableUp(_:)))
        upSwipeGesture.direction = .Up
        shareWorldTblView.addGestureRecognizer(upSwipeGesture)
        
        let downSwipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(IBShareWorldViewController.moveTableDown(_:)))
        downSwipeGesture.direction = .Down
        shareWorldTblView.addGestureRecognizer(downSwipeGesture)
        
        let upSwipeGestureForBottomView = UISwipeGestureRecognizer(target: self, action: #selector(IBShareWorldViewController.moveTableUp(_:)))
        upSwipeGestureForBottomView.direction = .Up
        buttonView.addGestureRecognizer(upSwipeGestureForBottomView)
        
        let downSwipeGestureForBottomView = UISwipeGestureRecognizer(target: self, action: #selector(IBShareWorldViewController.moveTableDown(_:)))
        downSwipeGestureForBottomView.direction = .Down
        buttonView.addGestureRecognizer(downSwipeGestureForBottomView)
 
    }
    
    func moveTableUp(swipeGesture : UISwipeGestureRecognizer) -> () {
        
        let posY =  self.shareWorldTblView.frame.origin.y
        if posY == GRAPHICS.Screen_Height()
        {
            if m_shareWorldListArray.count > 1{
            UIView.animateWithDuration(0.50, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.buttonView.frame.origin.y = self.m_mapview.frame.origin.y
                self.shareWorldTblView.frame.origin.y = self.buttonView.frame.maxY
                self.shareWorldTblView.frame.size.height = GRAPHICS.Screen_Height() - self.m_headerView.frame.size.height - self.buttonView.frame.size.height
                }, completion: nil)
            }
        }
        else
        {
            
        }
        
    }
    
    func moveTableDown(swipeGesture : UISwipeGestureRecognizer) -> () {
        let posY = m_mapview.frame.origin.y
        if posY == self.m_mapview.frame.origin.y
        {
            if self.shareWorldTblView.contentOffset.y == 0{
                UIView.animateWithDuration(0.50, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                    self.shareWorldTblView.frame.origin.y = GRAPHICS.Screen_Height()
                    self.shareWorldTblView.frame.size.height = self.m_bgImageView.frame.size.height/3
                    self.buttonView.frame.origin.y =  GRAPHICS.Screen_Height() - self.buttonView.frame.size.height
                    self.imgIndex = 0
                    }, completion: nil)
            }
        }
        else
        {
            
        }
    }

    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(true)
        m_mapview.pitchEnabled = true
        m_mapview.showsBuildings = true
        
    }
    func shuffleBtnAction() ->  (){
        
//        randomCount = random() % m_shareWorldListArray.count
//        getProductLocation()
//        shareWorldTblView.reloadData()
    }
    func homeBtnAction() ->() {
        m_mapview.setRegion(currentLocation, animated: true)
    }
    
    func shareWorldApiCall()
    {
        SwiftLoader.show(title: "Loading...", animated: true)
    
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_ShareWorldMap()
      }
    func getProductLocation()
    {
        let toRemoveCount : NSInteger = m_mapview.annotations.count
        var toRemove = NSMutableArray(capacity: toRemoveCount)
        var annotation1 : AnyObject
        
        for  annotation1  in m_mapview.annotations
        {
            if let annotation = annotation1 as? MKAnnotation {
                m_mapview.removeAnnotation(annotation)
            }
        }
        
        var coordinte1 = CLLocationCoordinate2D()
        
        for i in 0 ..< m_shareWorldListArray.count
        {
            let shareWorldEntity = m_shareWorldListArray.objectAtIndex(i) as! IBProductDetailsEntity
            var latitudeStr : String = shareWorldEntity.latitude as String //m_shareWorldListArray[i].objectForKey("latitude") as! String
            latitudeStr = latitudeStr.stringByReplacingOccurrencesOfString("\t", withString: "")
            
            var longitudeStr : String = shareWorldEntity.logitude as String//m_shareWorldListArray[i].objectForKey("logitude") as! String
             longitudeStr = longitudeStr.stringByReplacingOccurrencesOfString("\t", withString: "")
            
            let mylatitudeDouble = Double(latitudeStr)
            let myLongitudedouble = Double(longitudeStr)
            
            
            
            let pinAnnotation = PinAnnotation.init(latitude: mylatitudeDouble!, longitude: myLongitudedouble!)
            pinAnnotation.tag = i
            m_mapview.addAnnotation(pinAnnotation)
        }
    }
    
    
    
    //Mark :-MapView Delagate Methods
  
    func mapView(mapView: MKMapView, didAddAnnotationViews views: [MKAnnotationView])
    {
        let av : MKAnnotationView = MKAnnotationView()
        
        for annView:MKAnnotationView in views
        {
            
            let endFrame = annView.frame
            annView.frame = CGRectOffset(endFrame, 0, -500);
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                annView.frame = endFrame
            })
        }
        
        for av in views
        {
            // Don't pin drop if annotation is user location
            if  av.annotation is MKUserLocation
            {
                continue;
            }
            
            // Check if current annotation is inside visible map rect, else go to next one
            let point:MKMapPoint = MKMapPointForCoordinate(av.annotation!.coordinate)
            if MKMapRectContainsPoint(m_mapview.visibleMapRect, point)
            {
                continue;
                
            }
            
            let endFrame = av.frame
            
            // Move annotation out of view
            av.frame = CGRectMake(av.frame.origin.x, av.frame.origin.y - self.view.frame.size.height, av.frame.size.width, av.frame.size.height)
            
            //Animate drop
        }
        
    }
    
//    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
//    {
//        
//        if annotation.isKindOfClass(MKUserLocation) {
//            return nil
//        }
//
//        var annotationView  : MKAnnotationView!
//        var identifier = "Pin"
//        
//        if annotation .isKindOfClass(PinAnnotation)
//        {
//        annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier) as! MKPinAnnotationView
//            
//            
//            
//            if annotationView == nil
//            {
//                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
//                annotationView.image = UIImage(named: "")
//            }
//            
//            
//            
//        return  nil
//           
//        }
//    
//    }
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView?
    {
        
        if (annotation is MKUserLocation)
        {
            return nil
        }
        else
        {
            let reuseId = "test"
            
            
            var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
            let pinAnnotation = annotation as! PinAnnotation
            if anView == nil
            {
                anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                let annotationImg = GRAPHICS.SHARE_WORLD_ANNOTATION_IMAGE()
                anView!.image = annotationImg
                anView!.frame.size = GRAPHICS.getImageWidthHeight(annotationImg)
            }
//            else
//            {
//                //we are re-using a view, update its annotation reference...
//                anView!.annotation = pinAnnotation
//                anView!.image = GRAPHICS.SHARE_WORLD_ANNOTATION_IMAGE()
//            }
            anView!.tag = pinAnnotation.tag!
            let annoTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBShareWorldViewController.annotationTapGesture(_:)))
            anView!.addGestureRecognizer(annoTapGesture)
            
            anView!.canShowCallout = true
            anView!.userInteractionEnabled = true
            return anView
        }
    }
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView)
    {
        if view.annotation! .isKindOfClass(PinAnnotation)
        {
            /*
            let calloutAnnotation : CalloutAnnotation?
        let pinAnnotation :PinAnnotation = PinAnnotation(view.annotation)
            CalloutAnnotation.title = String(format:"%@ Products",PinAnnotation.title)
            CalloutAnnotation.coordinate = pinAnnotation.coordinate;
            pinAnnotation.calloutAnnotation = calloutAnnotation;
            m_mapview.addAnnotation(CalloutAnnotation)
            */
            
            //NSLog(@"pinAnnotation.title %@",pinAnnotation.title);
            //NSLog(@"pinAnnotation.strAgentName %@",pinAnnotation.strAgentName);
        }
       
    }
    
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView)
    {
        /*
        if view.annotation.isKindOfClass(PinAnnotation)
        {
            let pinAnnotation :PinAnnotation = PinAnnotation(view.annotation)
            
            //m_mapview.removeAnnotation(PinAnnotation.callouy)
            
           // pinAnnotation.calloutAnnotation = nil;
        
        
        }
*/
    }
    
    
    func annotationTapGesture(tapGesture : UITapGestureRecognizer) -> ()
    {
        //print(tapGesture.view!.tag)
        tapGestureTag = tapGesture.view!.tag
     /*   let productDetailVC = IBProductDetailsViewController()
        let myProductDict = m_shareWorldListArray.objectAtIndex(tapGesture.view!.tag) as! NSDictionary
        productDetailVC.productId = myProductDict.objectForKey("productId") as! String
        self.navigationController?.pushViewController(productDetailVC, animated: true)*/
        
        if imgIndex == 0{
            imgIndex = 1
            shareWorldTblView.reloadData()
            shareWorldTblView.setContentOffset(CGPointMake(0, (shareWorldTblView.frame.size.height) * CGFloat(tapGestureTag)), animated: true)
            UIView.animateWithDuration(0.50, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.shareWorldTblView.frame.origin.y = GRAPHICS.Screen_Height() - self.shareWorldTblView.frame.size.height
                self.buttonView.frame.origin.y =  self.shareWorldTblView.frame.origin.y - self.buttonView.frame.size.height
                }, completion: nil)
            
        }
        else{
            imgIndex = 0
            UIView.animateWithDuration(0.50, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.shareWorldTblView.frame.origin.y = GRAPHICS.Screen_Height()
            self.buttonView.frame.origin.y =  GRAPHICS.Screen_Height() - self.buttonView.frame.size.height
            }, completion: nil)
        }
        
    }
    func mapTapGesture(tapGesture : UITapGestureRecognizer) -> () {
        if imgIndex == 1{
            imgIndex = 0
            UIView.animateWithDuration(0.50, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                self.shareWorldTblView.frame.origin.y = GRAPHICS.Screen_Height()
                self.buttonView.frame.origin.y =  GRAPHICS.Screen_Height() - self.buttonView.frame.size.height
                }, completion: nil)
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(randomCount)
        return m_shareWorldListArray.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.m_bgImageView.frame.size.height/3
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cellIdentifier = "cell"
        var cell:IBShareWorldCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBShareWorldCell
        if (cell == nil) {
            cell = IBShareWorldCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        let shareWorldEntity = m_shareWorldListArray.objectAtIndex(indexPath.row) as! IBProductDetailsEntity
        let profileImageUrl = NSURL(string: shareWorldEntity.avatar as String)
        cell?.setValuesToControls(shareWorldEntity.sellername as String, locationStr: shareWorldEntity.address as String, profilePicUrl: profileImageUrl!, imagesArray: shareWorldEntity.images ,favouritesBtnTag:indexPath.row)
        cell?.shareWorldProtocol  = self
        cell?.selectionStyle = .None
        cell?.frame.size.height = self.m_bgImageView.frame.size.height/3
        return cell!
    }
    
    func favoriteButtonAction(sender : UIButton)
    {
        let shareWorldEntity = m_shareWorldListArray.objectAtIndex(sender.tag) as! IBProductDetailsEntity
        let productId = shareWorldEntity.productId as String
        let userId = getUserIdFromUserDefaults()
        
        
        SwiftLoader.show(title: Loading, animated: true)
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_addWish(userId, productId: productId)
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 50, longitudeDelta: 50))
        currentLocation = region
        
    }
    
    //MARK:- API delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }

    func API_CALLBACK_ShareWorldMap(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        if resultDict.objectForKey("error_code") as? String == "1"
        {
            let array1 : NSArray = (resultDict.objectForKey("result") as? NSArray)!
            for i in 0 ..< array1.count
            {
                let shareWorldDict = array1.objectAtIndex(i) as! NSDictionary
                let productEntity = IBProductDetailsEntity(dict: NSDictionary(dictionary: shareWorldDict))
                m_shareWorldListArray .addObject(productEntity)
            }
            //randomCount = random() % m_shareWorldListArray.count
            
            //print("share world list Array/%@",m_shareWorldListArray)
            self.createMapview()
            createTableViewAndBottomView()
            
            if m_shareWorldListArray.count > 0
            {
                self.getProductLocation()
            }
        }
        else
        {
            
        }
    }

    func API_CALLBACK_addWish(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        if resultDict.objectForKey("error_code") as? String == "1"
        {
            showAlertViewWithMessage((resultDict.objectForKey("result") as? String)!)
        }
        else
        {
            showAlertViewWithMessage((resultDict.objectForKey("result") as? String)!)
        }
        
    }

//    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
//        let location = locations.last
//        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
//        
//        m_mapview.setRegion(region, animated: true)
//    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
