//
//  IBCallOutAnnotationView.swift
//  IntoBuy
//
//  Created by SmaatApps on 21/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import CoreLocation

public protocol callOutAnnotationProtocol : NSObjectProtocol
{
    func callOutButtonClicked(title:String)
}

class IBCallOutAnnotationView: MKAnnotationView
{
    var strTitle : String = ""
    var strImagePropertyURl : String = ""
    var strAgentName  : String = ""
    var strPhoneNumber : String = ""
    var strPinIndex : String = ""
    var lbltitle : UILabel = UILabel()
    var lblagentName : UILabel = UILabel()
    var lblAgentContact:UILabel = UILabel()
    var buttn : UIButton = UIButton()
    var calloutOptionDelaget:callOutAnnotationProtocol?
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?)
    {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self .createControls()
       
    }
    func createControls()
    {
        self.frame = CGRectMake(0, 0, 140, 30)
        self.layer.borderColor = appPinkColor.CGColor
        self.layer.borderWidth = 1.0
        self.backgroundColor = grayColor
        
        //Create Title Label
        lbltitle.frame = CGRectMake(7, 0, 130, 25)
        lbltitle.textColor = UIColor.blackColor()
        lbltitle.textAlignment = .Center
        lbltitle.font = GRAPHICS.FONT_REGULAR(19)
        [self .addSubview(lbltitle)]
        
        
        buttn.frame = CGRectMake(260, 25, 30, 40)
        buttn.addTarget(self, action: #selector(IBCallOutAnnotationView.calloutButtonClicked) , forControlEvents: .TouchUpInside)
        self.addSubview(buttn)
        
        
    
    }
    
    func calloutButtonClicked()
    {
    
    
    
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
}


