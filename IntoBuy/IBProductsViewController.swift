//
//  IBProductsViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 02/08/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBProductsViewController: IBBaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UISearchBarDelegate,ServerAPIDelegate,ProductDelegate,UIActionSheetDelegate {
    
    var productCollView:UICollectionView!
    var searchView:UIView!
    var m_searchBar:UISearchBar!
    var categoryId = ""
    var productsArray = NSMutableArray()
    var filterArray = NSMutableArray()
    var type = ""
    var facebookStatus = ""
    var twitterStatus = ""
    var textToShare = String()//shareText1
    var imageToShare = UIImage()
    var strurl = NSURL()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initControls()
        createProductServiceApi()
        hideSettingsBtn(true)
    }
    
    func createProductServiceApi()
    {
        SwiftLoader.show(title: Loading, animated: true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getProductByCategory(categoryId)
    }
    
    func initControls()
    {
        var xPos = GRAPHICS.Screen_X()
        var yPos = self.m_bgImageView.bounds.origin.y
        var width = GRAPHICS.Screen_Width()
        var height:CGFloat = 60
        searchView = UIView(frame: CGRectMake(xPos,yPos,width,height))
        searchView.backgroundColor = UIColor.grayColor()
        self.m_bgImageView.addSubview(searchView)
        
        xPos = searchView.bounds.origin.x + 10
        yPos = searchView.bounds.origin.y + 10
        width = searchView.frame.size.width - 20
        height = searchView.frame.size.height - 20
        
        m_searchBar = UISearchBar(frame: CGRectMake(xPos,yPos,width,height))
        m_searchBar.delegate = self;
        m_searchBar.backgroundColor = UIColor.grayColor()
        m_searchBar.setBackgroundImage(GRAPHICS.SEARCH_TEXTBOX_IMAGE(), forBarPosition: .Top, barMetrics: .Default)
        m_searchBar.searchBarStyle = .Minimal
        m_searchBar.barStyle = .Default
        m_searchBar.placeholder = "Search Products"
        m_searchBar.translucent = false;
        m_searchBar.opaque = false
        searchView.addSubview(m_searchBar)
        createCollectionView()
    }
    func createCollectionView()
    {
        let xPos = GRAPHICS.Screen_X()
        let yPos = searchView.frame.maxY
        let width = GRAPHICS.Screen_Width()
        let height = self.m_bgImageView.frame.size.height - searchView.frame.size.height

        
        let categorylayOut = UICollectionViewFlowLayout()
        categorylayOut.itemSize =  CGSizeMake(GRAPHICS.Screen_Width()/2, 195.5)
        categorylayOut.scrollDirection = .Vertical
        categorylayOut.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        categorylayOut.minimumInteritemSpacing = 0
        categorylayOut.minimumLineSpacing = 0

        if productCollView != nil
        {
            productCollView.removeFromSuperview()
            productCollView = nil
        }
//        
//        m_myPostCollView = UICollectionView(frame: CGRectMake(m_pageScrlView.bounds.origin.x,profileTop.frame.maxY,m_pageScrlView.frame.size.width,m_pageScrlView.frame.size.height - profileTop.frame.size.height), collectionViewLayout: categorylayOut)
//        m_myPostCollView.backgroundColor = UIColor.clearColor()
//        m_myPostCollView.scrollEnabled = false
//        m_myPostCollView.bounces = true
//        m_myPostCollView.autoresizingMask = UIViewAutoresizing.FlexibleHeight
//        if postBtnIndex == 0
//        {
//            m_myPostCollView.registerClass(IBMyPageCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "cellIdentifier")
//        }
//        else{
//            m_myPostCollView.registerClass(IBMyProductsCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "cellIdentifier")
//        }
//        m_myPostCollView.delegate = self
//        m_myPostCollView.dataSource = self
//        m_pageScrlView.addSubview(m_myPostCollView)

        
        productCollView = UICollectionView(frame: CGRectMake(xPos,yPos,width,height), collectionViewLayout: categorylayOut)
        productCollView.backgroundColor = UIColor.clearColor()
        productCollView.scrollEnabled = true
        productCollView.bounces = true
        productCollView.registerClass(IBProductsCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "cellIdentifier")
        productCollView.delegate = self
        productCollView.dataSource = self
        productCollView.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        self.m_bgImageView.addSubview(productCollView)

    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if type == "searching"{
            return filterArray.count
        }
        else{
            return productsArray.count
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell:IBProductsCollectionViewCell? = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath) as? IBProductsCollectionViewCell
        
        //print(indexPath.item)
        let gridImg = UIImage(named: "intobuy-products-grid-view-tab2.png")//GRAPHICS .PRODUCT_GRID_IMAGE()
        cell?.createControlsForProductList((cell?.frame.size.width)!, height: (cell?.frame.size.height)!, bgImage: gridImg!)
        let productEntity:IBProductDetailsEntity!
        if type == "searching"
        {
            productEntity = filterArray.objectAtIndex(indexPath.item)as? IBProductDetailsEntity
        }
        else
        {
            productEntity = productsArray.objectAtIndex(indexPath.item)as? IBProductDetailsEntity
        }
       cell?.setValuesToControls((productEntity?.mainImage)! as String, productNameStr: productEntity.productname as String, productPriceStr: productEntity.price as String, profileImgStr: (productEntity?.avatar)! as String, userNameStr: (productEntity?.sellername)! as String, dateStr: (productEntity?.addedDate)! as String , moreBtnTag : indexPath.item , quantityStr : (productEntity?.quantity)! as String, btnLikeTag: indexPath.item)
        
        
//        cell?.productNameLbl.text = productEntity.productname as String
//        cell?.priceLbl.text = productEntity.price as String
        
        
        cell?.frame.size.height = (cell?.bgView.frame.maxY)!

        cell?.productDelegate = self
                return cell!
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let gridImg = UIImage(named: "intobuy-products-grid-view-tab2.png")//GRAPHICS .PRODUCT_GRID_IMAGE()
        var contentSize = GRAPHICS.getImageWidthHeight(gridImg!)
        let Width = GRAPHICS.Screen_Width()/2
        var Height:CGFloat! //= contentSize.height
        
        if GRAPHICS.Screen_Type() == IPHONE_4 || GRAPHICS.Screen_Type() == IPHONE_5
        {
            Height = gridImg!.size.height/1.2
        }
        else
        {
            Height = gridImg!.size.height
        }
        let newSize = CGSizeMake(Width, Height)
        return newSize
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let productEntity:IBProductDetailsEntity!
        if type == "searching"
        {
            productEntity = filterArray.objectAtIndex(indexPath.item)as? IBProductDetailsEntity
        }
        else
        {
            productEntity = productsArray.objectAtIndex(indexPath.item)as? IBProductDetailsEntity
        }
        let productDetailsVC = IBProductDetailsViewController()
        productDetailsVC.productId = productEntity.productId as String
        self.navigationController?.pushViewController(productDetailsVC, animated: true)
    }
    
    func moreButtonTapped(sender : UIButton)
    {
        var productEntity:IBProductDetailsEntity!
        if type == "searching"
        {
            productEntity = filterArray.objectAtIndex(sender.tag)as? IBProductDetailsEntity
        }
        else
        {
            productEntity = productsArray.objectAtIndex(sender.tag)as? IBProductDetailsEntity
        }
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
           // self.imageToShare = firstImageView.image!
            self.strurl = NSURL(string: productEntity.mainImage as String)!
            self.textToShare = productEntity.productname as String
            let indexPath = NSIndexPath(forItem: sender.tag, inSection: 0)
            let collectionViewCell = self.productCollView.cellForItemAtIndexPath(indexPath) as! IBProductsCollectionViewCell
            self.imageToShare = collectionViewCell.productImgView.image!
        })
        viewForReportAndShare()
        
    }
    
    func viewForReportAndShare() -> () {
        
        let actionSheet = UIActionSheet(title: Choosefrom, delegate: self, cancelButtonTitle: cancel, destructiveButtonTitle: nil, otherButtonTitles: Report_Inappropriate,Copy_share_URL)
        
        actionSheet.showInView(self.view)
        
        
        
        /*copyAlertView = UIView(frame: self.m_mainView.bounds)
         self.m_mainView.addSubview(copyAlertView)
         self.m_mainView.backgroundColor = UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
         
         let mainViewGesture = UITapGestureRecognizer(target: self, action: #selector(IBHomePageVcViewController.mainViewTapGesture))
         copyAlertView.addGestureRecognizer(mainViewGesture)
         
         let reportShareView = UIView(frame: CGRectMake(GRAPHICS.Screen_X() + 40,GRAPHICS.Screen_Height()/2 - 30,GRAPHICS.Screen_Width() - 80,60))
         reportShareView.backgroundColor = UIColor.whiteColor()
         reportShareView.layer.borderWidth = 1.0
         reportShareView.layer.borderColor = UIColor.lightGrayColor().CGColor
         copyAlertView.addSubview(reportShareView)
         
         let reportBtn = UIButton(frame : CGRectMake(reportShareView.bounds.origin.x,reportShareView.bounds.origin.y,reportShareView.frame.size.width,reportShareView.frame.size.height/2))
         reportBtn.setTitle("Report Inappropriate", forState: .Normal)
         reportBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
         reportBtn.titleLabel?.font = GRAPHICS.FONT_BOLD(12)
         reportBtn.addTarget(self, action: #selector(IBHomePageVcViewController.reportBtnAction), forControlEvents: .TouchUpInside)
         reportShareView.addSubview(reportBtn)
         
         let dividerView = UIView(frame: CGRectMake(reportBtn.bounds.origin.x,reportBtn.frame.size.height - 1,reportBtn.frame.size.width,1))
         dividerView.backgroundColor = UIColor.lightGrayColor()
         reportBtn.addSubview(dividerView)
         
         
         let copyUrlBtn = UIButton(frame : CGRectMake(reportShareView.bounds.origin.x,reportBtn.frame.maxY,reportShareView.frame.size.width,reportShareView.frame.size.height/2))
         copyUrlBtn.setTitle("Copy share URL", forState: .Normal)
         copyUrlBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
         copyUrlBtn.titleLabel?.font = GRAPHICS.FONT_BOLD(12)
         copyUrlBtn.addTarget(self, action: #selector(IBHomePageVcViewController.copyBtnAction), forControlEvents: .TouchUpInside)
         reportShareView.addSubview(copyUrlBtn)*/
    }
    //MARK:-ActionSheet Delegate methods
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex
        {
        case 0:
            
            actionSheet .dismissWithClickedButtonIndex(buttonIndex, animated: true)
            break
            
        case 1:
            self.reportBtnAction()
            break
            
        case 2:
            self.copyBtnAction()
            break
            
        default:
            break
            
        }
        
    }
    func reportBtnAction()
    {
        //        copyAlertView.removeFromSuperview()
        //        copyAlertView = nil
        //        self.m_mainView.backgroundColor = UIColor.clearColor()
        //        self.m_mainView.alpha = 1.0
        
        
    }
    func copyBtnAction()
    {
        //print(textToShare,imageToShare,strurl)
        shareText(textToShare, image1: imageToShare, url: strurl)
        //        copyAlertView.removeFromSuperview()
        //        copyAlertView = nil
        //        self.m_mainView.backgroundColor = UIColor.clearColor()
        //        self.m_mainView.alpha = 1.0
    }
    
    func shareText(text: String, image1: UIImage?, url: NSURL?)
    {
        let sharingItems = NSMutableArray()
        if (text != "")
        {
            sharingItems.addObject(text)
        }
        if image1  != nil
        {
            sharingItems.addObject(image1!)
        }
        if url != nil
        {
            sharingItems.addObject(url!)
        }
        
        let activityViewController = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
        var excludedItems = [String]()
        if facebookStatus == "0" && twitterStatus == "0" {
            if #available(iOS 9.0, *) {
                excludedItems = [UIActivityTypeMail,UIActivityTypeAirDrop,UIActivityTypeMessage,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypePostToWeibo,UIActivityTypeOpenInIBooks,UIActivityTypePrint]
            } else {
                // Fallback on earlier versions
            }
        }
        else if facebookStatus == "1" && twitterStatus == "0" {
            if #available(iOS 9.0, *) {
                excludedItems = [UIActivityTypeMail,UIActivityTypeAirDrop,UIActivityTypeMessage,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypePostToWeibo,UIActivityTypeOpenInIBooks,UIActivityTypePrint,UIActivityTypePostToFacebook]
            } else {
                // Fallback on earlier versions
            }
        }
        else if facebookStatus == "0" && twitterStatus == "1" {
            if #available(iOS 9.0, *) {
                excludedItems = [UIActivityTypeMail,UIActivityTypeAirDrop,UIActivityTypeMessage,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypePostToWeibo,UIActivityTypeOpenInIBooks,UIActivityTypePrint,UIActivityTypePostToTwitter]
            } else {
                // Fallback on earlier versions
            }
        }
        else{
            if #available(iOS 9.0, *) {
                excludedItems = [UIActivityTypeMail,UIActivityTypeAirDrop,UIActivityTypeMessage,UIActivityTypePostToFlickr,UIActivityTypePostToVimeo,UIActivityTypePostToWeibo,UIActivityTypeOpenInIBooks,UIActivityTypePrint,UIActivityTypePostToFacebook,UIActivityTypePostToTwitter]
            } else {
                // Fallback on earlier versions
            }
            
        }
        
        activityViewController.excludedActivityTypes = excludedItems
        self.navigationController!.presentViewController(activityViewController, animated: true, completion: nil)
    }

    
    // MARK: - search bar delegate
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        return true
    }
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar .setShowsCancelButton(true, animated: true)
    }
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar .setShowsCancelButton(false, animated: true)
    }
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(false, animated: true)
        return true
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        type = "alllist";
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar .resignFirstResponder()
        productCollView.reloadData()
    }
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        type = "searching"
        if searchBar.text == ""
        {
            
        }
        else{
            let searchPredicate = NSPredicate(format: "productname BEGINSWITH[cd] %@", searchText as String!)
            let array = productsArray.filteredArrayUsingPredicate(searchPredicate)
            //print(array)
            filterArray .removeAllObjects()
            filterArray .addObjectsFromArray(array)
            productCollView.reloadData()
        }
        
        
    }
    
    //MARK:- server API delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    func API_CALLBACK_getProductByCategory(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            productsArray.removeAllObjects()
            let resultArray = resultDict.objectForKey("result") as! NSArray
            if resultArray.count > 0
            {
                for i in 0 ..< resultArray.count
                {
                    let productDict = resultArray.objectAtIndex(i) as! NSDictionary
                    let productEntity = IBProductDetailsEntity(dict: NSDictionary(dictionary: productDict))
                    productsArray.addObject(productEntity)
                }
            }
            else
            {
                showAlertViewWithMessage("No Products found")
            }
            productCollView.reloadData()
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }

    
    // deepika for like button api call need to pass the wall_id but no its empty
    func productLikeBtnAction(sender : UIButton) {
        
        if Reachability.isConnectedToNetwork() == true
        {
            
            let productEntity = productsArray.objectAtIndex(sender.tag) as? IBProductDetailsEntity
            let userIdStr = getUserIdFromUserDefaults()
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_addWallLike(productEntity?.wall_id as! String, userId: userIdStr)
            }
        }
        else
        {
            showAlertViewWithMessage(NoInternetConnection)
        }
    }
    
    // deepika for like button api call need to pass the wall_id but no its empty
    func API_CALLBACK_addWallLike(resultDict: NSDictionary)
    {
        //SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            
            
        }
            
        else{
            showAlertViewWithMessage((resultDict.objectForKey("msg")as? String!)!)
        }
    }
    
    
}
