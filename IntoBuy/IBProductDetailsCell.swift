//
//  IBProductDetailsCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 17/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBProductDetailsCell: UITableViewCell {
    
    var profileImgView = UIImageView()
    var nameLabel = UILabel()
    var commentLabel = UILabel()
    var timeLabel = UILabel()
    let imgHeight = CGFloat(50)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsForProductComments()
    }
    
    func createControlsForProductComments()
    {
        let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
        var posX = GRAPHICS.Screen_X()+10
        var posY = CGFloat(5)
        var width = CGFloat(50)
        var height = CGFloat(50)
        
        profileImgView.frame = CGRectMake(posX,posY,imgHeight,imgHeight)
        profileImgView.clipsToBounds = true
        profileImgView.layer.cornerRadius = imgHeight/2
        profileImgView.backgroundColor = UIColor.clearColor()
        
        self.addSubview(profileImgView)
        
        posX = profileImgView.frame.maxX + 10
        width = GRAPHICS.Screen_Width()/2
        height = 20
        
        nameLabel.frame = CGRectMake(posX,posY,GRAPHICS.Screen_Width() - imgHeight - 30,height)
        nameLabel.font = GRAPHICS.FONT_REGULAR(12)
        nameLabel.textColor = headerColor
        nameLabel.text = "Manoj"
        self.addSubview(nameLabel)
        
        posY = nameLabel.frame.maxY
        width = GRAPHICS.Screen_Width() - profileImgView.frame.size.width - 30
        commentLabel.frame = CGRectMake(posX,posY,width,height)
        commentLabel.font = GRAPHICS.FONT_REGULAR(12)
        commentLabel.text = "divabs badis;b bi;auds "
        commentLabel.numberOfLines = 0;
        commentLabel.lineBreakMode = .ByWordWrapping
        
        self.addSubview(commentLabel)
        
        width = 100
        posY = commentLabel.frame.maxY
        timeLabel.frame = CGRectMake(posX,posY,width,height)
        timeLabel.font = GRAPHICS.FONT_REGULAR(12)
        timeLabel.text = "1 min"
        timeLabel.textAlignment = .Left;
        timeLabel.textColor = UIColor.lightGrayColor()
        self.addSubview(timeLabel)

    }
    
    func setValuesToTheControls(commentDetailsDict : NSDictionary) -> ()
    {
        let profilePicUrl = commentDetailsDict.objectForKey("userProfile") as! String
        let userNameStr = commentDetailsDict.objectForKey("username") as! String
        let commentstr = commentDetailsDict.objectForKey("comment") as! String
        let commentText = decodeEncodedStringToNormalString(commentstr)
        
        let addedDate = commentDetailsDict.objectForKey("added_date") as! String
        let addedDateStr = convertUtcToLocalFormat(addedDate)
        let dateFormat:NSDateFormatter = NSDateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormat.timeZone = NSTimeZone.localTimeZone()
        let startDate = dateFormat.dateFromString(addedDateStr)
        let intervalString = startDate!.relativeTimeToString()
        //print("\(addedDateStr), \(intervalString)")
        
        profileImgView.load(profilePicUrl, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil);
        nameLabel.text = userNameStr
        commentLabel.text = commentText
        timeLabel.text = intervalString

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
