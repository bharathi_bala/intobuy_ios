
//
//  IBProductsTableViewCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 15/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

public protocol HomeProtocol : NSObjectProtocol
{
    func likeForPost(sender: UIButton)
    func commentForPost(sender: UIButton)
    func shareForPost(sender: UIButton)
    func addProductToCart(sender: UIButton)
    func scrollViewGesture(tapGesture: UITapGestureRecognizer)
    func likeLabelGesture(tapGesture: UITapGestureRecognizer)
}

class IBProductsTableViewCell: UITableViewCell {
    
    var m_bgView:UIView!
    var m_profileImgView:UIImageView!
    var m_profileNameLbl:UILabel!
    var m_addCartBtn:UIButton!
    var m_productScrlView:UIScrollView!
    var m_productImgView:UIImageView!
    var m_categoryLbl:UILabel!
    var m_productNameLbl:UILabel!
    var m_likeBtn:UIButton!
    var m_commentBtn:UIButton!
    var m_commentLbl:UILabel!
    var m_shareBtn:UIButton!
    var shareSubBtn:UIButton!
    var m_likePersonsLbl:UILabel!
    var btnDelegate:HomeProtocol!
    var m_dateLbl:UILabel!
    var likeImgView:UIImageView!
    var m_firstImage:UIImageView!
    var m_secondImage:UIImageView!
    var m_thirdImage:UIImageView!
    var m_fourthImage:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsToViewProducts()
    }
    
    func createControlsToViewProducts()
    {
        var xPos = GRAPHICS.Screen_X()
        var yPos:CGFloat = 0
        var width = GRAPHICS.Screen_Width()
        var height = GRAPHICS.Screen_Height()/2
        
        m_bgView = UIView(frame: CGRectMake(xPos,yPos,width,height))
        self.addSubview(m_bgView)
        
        
        xPos = GRAPHICS.Screen_X()
        yPos = m_bgView.bounds.origin.y
        width = GRAPHICS.Screen_Width()
        height = width
        m_productScrlView = UIScrollView(frame: CGRectMake(xPos,yPos,width,height))
        //m_productImgView.image = UIImage(named: "IMG_1300.jpg")
        m_productScrlView.pagingEnabled = true
        m_productScrlView.userInteractionEnabled = true
        m_bgView.addSubview(m_productScrlView)
        
        
        
        if m_firstImage != nil
        {
            m_firstImage.removeFromSuperview()
            m_firstImage = nil
        }
        
        xPos = m_productScrlView.bounds.origin.x
        yPos = m_productScrlView.bounds.origin.y
        width = m_productScrlView.frame.size.width
        height = m_productScrlView.frame.size.height
        m_firstImage = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        m_firstImage.userInteractionEnabled = true
//        m_productScrlView.addSubview(m_firstImage)
//        m_firstImage.contentMode = .ScaleToFill
        
        if m_secondImage != nil
        {
            m_secondImage.removeFromSuperview()
            m_secondImage = nil
        }

        xPos = m_firstImage.frame.maxX
        m_secondImage = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        m_secondImage.userInteractionEnabled = true
//        m_productScrlView.addSubview(m_secondImage)
//        m_secondImage.contentMode = .ScaleToFill
        
        if m_thirdImage != nil
        {
            m_thirdImage.removeFromSuperview()
            m_thirdImage = nil
        }

        xPos = m_secondImage.frame.maxX
        m_thirdImage = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        m_thirdImage.userInteractionEnabled = true
//        m_productScrlView.addSubview(m_thirdImage)
//        m_thirdImage.contentMode = .ScaleToFill
        
        
        if m_fourthImage != nil
        {
            m_fourthImage.removeFromSuperview()
            m_fourthImage = nil
        }

        xPos = m_thirdImage.frame.maxX
        m_fourthImage = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        m_fourthImage.userInteractionEnabled = true
//        m_productScrlView.addSubview(m_fourthImage)
//        m_fourthImage.contentMode = .ScaleToFill

        
//        let tapGesture = UITapGestureRecognizer(target: self, action: "productImageAction:")
//        tapGesture.numberOfTapsRequired = 1
//        m_firstImage.addGestureRecognizer(tapGesture);
//        m_secondImage.addGestureRecognizer(tapGesture);
//        m_thirdImage.addGestureRecognizer(tapGesture);

        
        xPos = m_bgView.bounds.origin.x + 5
        yPos = m_productScrlView.frame.maxY + 10
        width = GRAPHICS.Screen_Width()/2
        height = 15
        m_categoryLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_categoryLbl.text = "Sporting Wear"
        m_categoryLbl.textColor = headerColor
        m_categoryLbl.font = GRAPHICS.FONT_REGULAR(12)
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            m_categoryLbl.font = GRAPHICS.FONT_REGULAR(13)
        }

        m_bgView.addSubview(m_categoryLbl)
        
        yPos = m_categoryLbl.frame.maxY + 5
        m_productNameLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_productNameLbl.text = "Product Name"
        m_productNameLbl.font = GRAPHICS.FONT_REGULAR(12)
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            m_productNameLbl.font = GRAPHICS.FONT_REGULAR(13)
        }

        m_bgView.addSubview(m_productNameLbl)

        let likeImg = GRAPHICS.HOME_FAVOURITE_BUTTON_IMAGE()
        let likeSel = GRAPHICS.HOME_FAVOURITE_BUTTON_SELECTED_IMAGE()
        yPos = m_productNameLbl.frame.maxY + 5
        width = likeImg.size.width/2
        height = likeImg.size.height/2
        m_likeBtn = UIButton(frame : CGRectMake(xPos,yPos,width,height))
        m_likeBtn.setBackgroundImage(likeImg, forState: .Normal)
        m_likeBtn.setBackgroundImage(likeSel, forState: .Selected)
        m_likeBtn.addTarget(self, action: #selector(IBProductsTableViewCell.likeBtnAction(_:)), forControlEvents: .TouchUpInside)
        m_bgView.addSubview(m_likeBtn)
        
        let commentImg = GRAPHICS.HOME_COMMENT_BUTTON_IMAGE()
        xPos = m_likeBtn.frame.maxX + 15
        width = commentImg.size.width/1.7
        height = commentImg.size.height/1.7
        m_commentBtn = UIButton(frame : CGRectMake(xPos,yPos-1.5,width,height))
        m_commentBtn.setBackgroundImage(commentImg, forState: .Normal)
        m_commentBtn.addTarget(self, action: #selector(IBProductsTableViewCell.commentBtnAction(_:)), forControlEvents: .TouchUpInside)
        m_bgView.addSubview(m_commentBtn)
        
        xPos = m_commentBtn.frame.maxX+5
        yPos = yPos + 5
        m_commentLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_commentLbl.font = GRAPHICS.FONT_REGULAR(12)
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            m_commentLbl.font = GRAPHICS.FONT_REGULAR(13)
        }
        m_commentLbl.hidden = true
        m_bgView.addSubview(m_commentLbl)
        
        let cartImg = GRAPHICS.HOME_ADD_CART_BUTTON_IMAGE()
        let cartPinkImg = GRAPHICS.HOME_ADD_CART_PINK_BUTTON_IMAGE()
        
        width = cartImg.size.width/2
        height = cartImg.size.height/2
        xPos = m_commentLbl.frame.maxX+5
        //yPos = yPos - 5
        m_addCartBtn = UIButton(frame : CGRectMake(xPos,yPos,width,height))
        m_addCartBtn.setBackgroundImage(cartImg, forState: .Normal)
        m_addCartBtn.setBackgroundImage(cartPinkImg, forState: .Selected)
        m_addCartBtn.addTarget(self, action: #selector(IBProductsTableViewCell.cartBtnAction(_:)), forControlEvents: .TouchUpInside)
        m_bgView.addSubview(m_addCartBtn)
       // m_bgView.bringSubviewToFront(m_addCartBtn)

        

        let shareImg = GRAPHICS.HOME_SHARE_BUTTON_IMAGE()
        width = shareImg.size.width/2
        height = shareImg.size.height/2
        xPos = m_bgView.frame.size.width - width - 30
        yPos = m_commentBtn.frame.origin.y
        m_shareBtn = UIButton(frame : CGRectMake(xPos,yPos,width,height))
        m_shareBtn.setBackgroundImage(shareImg, forState: .Normal)
        m_shareBtn.addTarget(self, action: #selector(IBProductsTableViewCell.shareBtnAction(_:)), forControlEvents: .TouchUpInside)
        m_bgView.addSubview(m_shareBtn)
        
        shareSubBtn = UIButton(frame: CGRectMake(xPos,m_shareBtn.frame.origin . y - 5,width * 2,height))
        shareSubBtn.bounds.origin.y = m_shareBtn.frame.maxY - 5
        shareSubBtn.addTarget(self, action: #selector(IBProductsTableViewCell.shareBtnAction(_:)), forControlEvents: .TouchUpInside)
        m_bgView.addSubview(shareSubBtn)
        
        
        width = 20
        height = 20
        xPos = m_bgView.bounds.origin.x + 5
        yPos = m_commentBtn.frame.maxY + 10
        likeImgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        likeImgView.image = GRAPHICS.HOME_FAVOURITE_BUTTON_SELECTED_IMAGE()
        m_bgView.addSubview(likeImgView)
        
        
        width = GRAPHICS.Screen_Width() - 50
        xPos = likeImgView.frame.maxX + 10
        m_likePersonsLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_likePersonsLbl.font = GRAPHICS.FONT_REGULAR(12)
        if(GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            m_likePersonsLbl.font = GRAPHICS.FONT_REGULAR(13)
        }
        m_likePersonsLbl.text = FirstToLike
        m_likePersonsLbl.textColor = headerColor
        m_bgView.addSubview(m_likePersonsLbl)
        m_likePersonsLbl.userInteractionEnabled = true
        
        let likeLblGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductsTableViewCell.likeLabelTapGesture(_:)))
        m_likePersonsLbl.addGestureRecognizer(likeLblGesture)
        
        m_bgView.frame.size.height = m_likePersonsLbl.frame.maxY + 10
        
        
//        width = 80
//        xPos = GRAPHICS.Screen_Width() - width - 20
//        yPos = self.bounds.origin.y + 10
//        m_dateLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
//        m_dateLbl.font = GRAPHICS.FONT_REGULAR(12)
//        m_dateLbl.text = "2 days ago"
//        self.addSubview(m_dateLbl)
        
     }
    func setTextToTheControls(categoryStr :String, productStr: String, likeStr: String, commentArray:NSArray, commentStr:NSString , likedNamesStr:String, likeTag:Int ,commentTag:Int, imageArray:NSArray, isLiked: String, toHideCartBtn:Bool ,scrollViewTag: Int , imageStr : String ,shareBtnTag : Int , cartStatus: String)
    {
        
        if(toHideCartBtn == true)
        {
            m_categoryLbl.hidden = true
            m_productNameLbl.frame.origin.y = m_productScrlView.frame.maxY + 10
            m_productNameLbl.frame.origin.x =  m_bgView.bounds.origin.x + 10
            m_likeBtn.frame.origin.x = m_productNameLbl.frame.origin.x
            m_likeBtn.frame.origin.y = m_productNameLbl.frame.maxY + 5
            m_commentBtn.frame.origin .y = m_likeBtn.frame.origin.y - 3
            m_commentBtn.frame.origin.x = m_likeBtn.frame.maxX + 15
            m_commentLbl.frame.origin .y = m_commentBtn.frame.origin.y
            m_commentLbl.frame.origin.x = m_commentBtn.frame.maxX + 5
            m_shareBtn.frame.origin.y = m_commentBtn.frame.origin.y
            likeImgView.frame.origin .y = m_commentBtn.frame.maxY + 10
            m_likePersonsLbl.frame.origin.y = likeImgView.frame.origin.y
            m_shareBtn.frame.origin.x = m_bgView.frame.size.width - m_shareBtn.frame.size.width - 15;
            
        }
        else
        {
            m_categoryLbl.hidden = false
            m_categoryLbl.text = categoryStr
            m_productNameLbl.frame.origin.y = m_categoryLbl.frame.maxY + 10
            m_productNameLbl.frame.origin.x =  m_bgView.bounds.origin.x + 10
            m_likeBtn.frame.origin.x = m_productNameLbl.frame.origin.x
            m_likeBtn.frame.origin.y = m_productNameLbl.frame.maxY + 5
            m_commentBtn.frame.origin .y = m_likeBtn.frame.origin.y
            m_commentBtn.frame.origin.x = m_likeBtn.frame.maxX + 15
            m_commentLbl.frame.origin .y = m_commentBtn.frame.origin.y
            m_commentLbl.frame.origin.x = m_commentBtn.frame.maxX + 5
            m_shareBtn.frame.origin.y = m_commentBtn.frame.origin.y
            likeImgView.frame.origin .y = m_commentBtn.frame.maxY + 10
            m_likePersonsLbl.frame.origin.y = likeImgView.frame.origin.y
            m_shareBtn.frame.origin.x = m_bgView.frame.size.width - m_shareBtn.frame.size.width - 30
        }
        
        if cartStatus == "1"
        {
            m_addCartBtn.selected = true
        }
        else
        {
            m_addCartBtn.selected = false
        }
        
        m_productNameLbl.text = productStr
        m_likeBtn.tag = likeTag
        m_commentBtn.tag = commentTag
        m_shareBtn.tag = shareBtnTag
        shareSubBtn.tag = shareBtnTag
        m_addCartBtn.hidden = toHideCartBtn
        m_productScrlView.tag = scrollViewTag
        
        m_firstImage.hidden = true
        m_secondImage.hidden = true
        m_thirdImage.hidden = true
        m_fourthImage.hidden = true
        m_firstImage.tag = scrollViewTag +  1
        m_secondImage.tag = scrollViewTag +  2
        m_thirdImage.tag = scrollViewTag +  3
        
        var xPos = m_productScrlView.bounds.origin.x
        let yPos = m_productScrlView.bounds.origin.y
        let width = m_productScrlView.frame.size.width
        let height = m_productScrlView.frame.size.height

        if imageArray.count == 0
        {
//            m_firstImage.hidden = false
//           m_firstImage.load(imageStr, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            
            let imageView = UIImageView(frame: CGRectMake(xPos, yPos, width, height))
           // imageView.load(imageStr, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            /*let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) -> Void in
                
            }*/
            
            //imageView.sd_setImageWithURL(NSURL(string: imageStr)!,placeholderImage: GRAPHICS.DEFAULT_CART_IMAGE(), completed: block) //Commented by arun to avoid scroll stuck issue
            //imageView.sd_setImageWithURL(NSURL(string: imageStr), placeholderImage: GRAPHICS.DEFAULT_CART_IMAGE())
            imageView.sd_setImageWithURL(NSURL(string: imageStr)!, placeholderImage: GRAPHICS.DEFAULT_CART_IMAGE(), options: .ScaleDownLargeImages)

            imageView.userInteractionEnabled = true
            imageView.contentMode = .ScaleToFill
            imageView.tag = scrollViewTag
            m_productScrlView.addSubview(imageView)
            
            if imageArray.count > 0
            {
            let imgTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductsTableViewCell.productImageAction(_:)))
            imgTapGesture.numberOfTapsRequired = 1
            imageView.addGestureRecognizer(imgTapGesture);
            }

        }
        
        
        for i in 0  ..< imageArray.count
        {
            
            let imageDict = imageArray.objectAtIndex(i)as! NSDictionary
            let imageUrl = imageDict.objectForKey("image")as! String

            let imageView = UIImageView(frame: CGRectMake(xPos, yPos, width, height))
            //imageView.load(imageUrl, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
            
            /*let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                
            }
            
            imageView.sd_setImageWithURL(NSURL(string: imageUrl)!, placeholderImage:GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completed: block)*/
            imageView.sd_setImageWithURL(NSURL(string: imageUrl)!, placeholderImage: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), options: .ScaleDownLargeImages)
            //imageView.sd_setImageWithURL(NSURL(string: imageUrl), placeholderImage: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE())
            
            imageView.userInteractionEnabled = true
            imageView.contentMode = .ScaleToFill
            imageView.tag = scrollViewTag + i + 1
            m_productScrlView.addSubview(imageView)
            
            xPos = xPos + width
            
            let imgTapGesture = UITapGestureRecognizer(target: self, action: #selector(IBProductsTableViewCell.productImageAction(_:)))
            imgTapGesture.numberOfTapsRequired = 1
            imageView.addGestureRecognizer(imgTapGesture);


            
//            if( i == 0)
//            {
//                m_firstImage.hidden = false
//                let imageDict = imageArray.objectAtIndex(i)as! NSDictionary
//                let imageUrl = imageDict.objectForKey("image")as! String
//
//                m_firstImage.load(imageUrl, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
//            }
//            else if( i == 1){
//                m_secondImage.hidden = false
//                let imageDict = imageArray.objectAtIndex(i)as! NSDictionary
//                let imageUrl = imageDict.objectForKey("image")as! String
//
//                m_secondImage.load(imageUrl, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
//            }
//            else if(i == 2){
//                m_thirdImage.hidden = false
//                let imageDict = imageArray.objectAtIndex(i)as! NSDictionary
//                let imageUrl = imageDict.objectForKey("image")as! String
//
//                m_thirdImage.load(imageUrl, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
//            }
//            else if (i == 3)
//            {
//                m_fourthImage.hidden = false
//                let imageDict = imageArray.objectAtIndex(i)as! NSDictionary
//                let imageUrl = imageDict.objectForKey("image")as! String
//
//                m_fourthImage.load(imageUrl, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
//            }

        }
        
        m_productScrlView.contentSize = CGSizeMake(GRAPHICS.Screen_Width() * (CGFloat(imageArray.count)) , m_productScrlView.frame.size.height)
        
        if isLiked == "1"
        {
            m_likeBtn.selected = true
        }
        else{
            m_likeBtn.selected = false
        }
        
        
        if commentArray.count > 0
        {
            m_commentBtn.setBackgroundImage(GRAPHICS.HOME_COMMENT_BUTTON_SELECTED_IMAGE(), forState: .Normal)
            m_commentLbl.hidden = false
            m_commentLbl.text = String(format: "%d",commentArray.count)
            m_commentLbl.sizeToFit()
            m_addCartBtn.frame.origin.x = m_commentLbl.frame.maxX + 10//5
        }
        else{
            m_commentBtn.setBackgroundImage(GRAPHICS.HOME_COMMENT_BUTTON_IMAGE(), forState: .Normal)
            m_commentLbl.hidden = true
            m_addCartBtn.frame.origin.x = m_commentBtn.frame.maxX + 15
        }
        if likedNamesStr == ""
        {
            m_likePersonsLbl.text = FirstToLike
        }
        else
        {
            m_likePersonsLbl.text = likedNamesStr
        }
        
    }
    func likeBtnAction(sender: UIButton)
    {
        btnDelegate!.likeForPost(sender)
    }
    func commentBtnAction(sender: UIButton)
    {
        btnDelegate!.commentForPost(sender)
    }
    func cartBtnAction(sender: UIButton)
    {
        btnDelegate!.addProductToCart(sender)
    }
    func shareBtnAction(sender: UIButton)
    {
        btnDelegate!.shareForPost(sender)
    }
    func productImageAction(tapGesture: UITapGestureRecognizer)
    {
        btnDelegate!.scrollViewGesture(tapGesture)
    }
    func likeLabelTapGesture(tapGesture: UITapGestureRecognizer)
    {
        btnDelegate!.likeLabelGesture(tapGesture)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
