/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

#import "AdobeAssetLibraryItem.h"
#import "AdobeColorTheme.h"

#define IsAdobeAssetLibraryItemColorTheme(item) ([item isKindOfClass:[AdobeAssetLibraryItemColorTheme class]])

/** Color Theme Rule */
typedef NS_ENUM (NSUInteger, AdobeAssetLibraryColorThemeRule)
{
    /** Unknown */
    AdobeAssetLibraryColorThemeRuleUnknown,
    /** Not set */
    AdobeAssetLibraryColorThemeRuleNotSet,
    /** Analogous */
    AdobeAssetLibraryColorThemeRuleAnalogous,
    /** Complimentary */
    AdobeAssetLibraryColorThemeRuleComplimentary,
    /** Monochromatic */
    AdobeAssetLibraryColorThemeRuleMonochromatic,
    /** Triad */
    AdobeAssetLibraryColorThemeRuleTriad,
    /** Custom */
    AdobeAssetLibraryColorThemeRuleCustom,
} __deprecated_msg ("Use AdobeColorThemeRule instead");

/** Describes the "mood" of a particular color theme. */
typedef NS_ENUM (NSUInteger, AdobeAssetLibraryColorThemeMood)
{
    /** Unknown */
    AdobeAssetLibraryColorThemeMoodUnknown,
    /** Not set */
    AdobeAssetLibraryColorThemeMoodNotSet,
    /** Colorful */
    AdobeAssetLibraryColorThemeMoodColorful,
    /** Bright */
    AdobeAssetLibraryColorThemeMoodBright,
    /** Muted */
    AdobeAssetLibraryColorThemeMoodMuted,
    /** Dark */
    AdobeAssetLibraryColorThemeMoodDark,
    /** Custom */
    AdobeAssetLibraryColorThemeMoodCustom,
} __deprecated_msg ("Use AdobeColorThemeMood instead");

/**
 * AdobeAssetLibraryItemColorTheme represents a color theme item within a library.
 */
@interface AdobeAssetLibraryItemColorTheme : AdobeAssetLibraryItem

@property (readonly, nonatomic, strong) NSArray * tags __deprecated_msg("Use colorTheme.tags instead");

@property (readonly, nonatomic, assign) AdobeColorThemeRule rule __deprecated_msg("Use colorTheme.rule instead");

@property (readonly, nonatomic, assign) AdobeColorThemeMood mood __deprecated_msg("Use colorTheme.mood instead");

@property (readonly, nonatomic, strong) NSArray * swatches __deprecated_msg("Use [AdobeColorTheme initWithSwatches:] instead");

- (NSArray *)getRGBColors __deprecated_msg("Use [colorTheme colors] instead");

/** Color Theme associated with this AdobeAssetLibraryItemColorTheme object */
@property (readonly, nonatomic, strong) AdobeColorTheme *colorTheme;

@end
