/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2015 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 *
 * THIS FILE IS PART OF THE CREATIVE SDK PUBLIC API
 *
 ******************************************************************************/

#import <Foundation/Foundation.h>

/** The mime type for compressed zip files */
extern NSString *const kAdobeFileExtensionTypeZip;

/** The mime type for PSD Photoshop documents */
extern NSString *const kAdobeFileExtensionTypePSD;

/** The mime type for PSB Photoshop documents */
extern NSString *const kAdobeFileExtensionTypePSB;

/** The mime type for Quicktime video files */
extern NSString *const kAdobeFileExtensionTypeMOV;

/** The mime type for MP4 video files */
extern NSString *const kAdobeFileExtensionTypeMP4;

/** The mime type for jpeg files */
extern NSString *const kAdobeFileExtensionTypeJPEG;

/** The mime type for AI Illustrator documents */
extern NSString *const kAdobeFileExtensionTypeAI;

/** The mime type for AIT Illustrator documents */
extern NSString *const kAdobeFileExtensionTypeAIT;

/** The mime type for Postscript files */
extern NSString *const kAdobeFileExtensionTypeEPS;

/** The mime type for INDD InDesign documents */
extern NSString *const kAdobeFileExtensionTypeINDD;

/** The mime type for IDML InDesign documents */
extern NSString *const kAdobeFileExtensionTypeIDML;

/** The mime type for IDMS InDesign documents */
extern NSString *const kAdobeFileExtensionTypeIDMS;

/** The mime type for INDT InDesign documents */
extern NSString *const kAdobeFileExtensionTypeINDT;

/** The mime type for gif files */
extern NSString *const kAdobeFileExtensionTypeGIF;

/** The mime type for png files */
extern NSString *const kAdobeFileExtensionTypePNG;

/** The mime type for tiff files */
extern NSString *const kAdobeFileExtensionTypeTIFF;

/** The mime type for bmp files */
extern NSString *const kAdobeFileExtensionTypeBMP;

/** The mime type for pdf files */
extern NSString *const kAdobeFileExtensionTypePDF;

/** The mime type for dmg files */
extern NSString *const kAdobeFileExtensionTypeDMG;

/** The mime type for dng files */
extern NSString *const kAdobeFileExtensionTypeDNG;

/** The mime type for shape files */
extern NSString *const kAdobeFileExtensionTypeSHAPE;

/** The mime type for svg files */
extern NSString *const kAdobeFileExtensionTypeSVG;

@interface AdobeAssetFileExtensions : NSObject

/**
 * A utility method that returns the file extension (e.g, ".pdf") for a
 * given MIME type (e.g., "application/pdf"), if available. This method
 * only provides mappings to extension constants defined in this file.
 *
 * @param type The MIME type.
 *
 * @return The extension, if a mapping is available for it; otherwise nil.
 */
+ (NSString *)extensionForMIMEType:(NSString *)type;

@end