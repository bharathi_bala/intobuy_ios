/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2013 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 *
 * THIS FILE IS PART OF THE CREATIVE SDK PUBLIC API
 *
 ******************************************************************************/

/**
 *
 * This is the list of all AdobeAppLibraryCategories constants that can be used to setup the inclusionList and filter arrays for the
 * following AdobeUXAppLibrary methods:
 *
 * <ul>
 * <li><code>-popupAppLibraryWithParent:withInclusionList:withOrder:onSuccess:onError:</code></li>
 * <li><code>–popupAppLibraryWithParent:withFilter:filterType:withOrder:onSuccess:onError:</code>
 * </ul>
 *
 */

/**
 * Represents the Image Capture Category in the Adobe App Library.
 */
extern NSString *const kALCategoryCapture;

/**
 * Represents the Community Category in the Adobe App Library.
 */
extern NSString *const kALCategoryCommunity;

/**
 * Represents the Design Category in the Adobe App Library.
 */
extern NSString *const kALCategoryDesign;

/**
 * Represents the Drawing Category in the Adobe App Library.
 */
extern NSString *const kALCategoryDrawing;

/**
 * Represents the Image Editing Category in the Adobe App Library.
 */
extern NSString *const kALCategoryImageEditing;

/**
 * Represents the Photography Category in the Adobe App Library.
 */
extern NSString *const kALCategoryPhotography;

/**
 * Represents the Photo & Video Category in the Adobe App Library.
 */
extern NSString *const kALCategoryPhotoVideo;

/**
 * Represents the Productivity Category in the Adobe App Library.
 */
extern NSString *const kALCategoryProductivity;

/**
 * Represents the Storytelling Category in the Adobe App Library.
 */
extern NSString *const kALCategoryStorytelling;

/**
 * Represents the UI/UX Design Category in the Adobe App Library.
 */
extern NSString *const kALCategoryUIUXDesign;

/**
 * Represents the Utility Category in the Adobe App Library.
 */
extern NSString *const kALCategoryUtility;

/**
 * Represents the Video Category in the Adobe App Library.
 */
extern NSString *const kALCategoryVideo;
