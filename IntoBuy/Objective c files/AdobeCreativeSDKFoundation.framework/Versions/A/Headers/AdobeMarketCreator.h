/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class AdobeAssetAsyncRequest;

/**
 * AdobeMarketCreatorAvatarSize represents the size of the avatar for an AdobeMarket asset creator.
 */
typedef NS_ENUM (NSInteger, AdobeMarketCreatorAvatarSize)
{
    /** Small. */
    AdobeMarketCreatorAvatarSizeSmall  = 0,
    /** Medium. */
    AdobeMarketCreatorAvatarSizeMedium = 1,
    /** Large. */
    AdobeMarketCreatorAvatarSizeLarge  = 2
};

/**
 * AdobeMarketCreator represents the creator of an asset in the AdobeMarket.  It provides accessors for determining
 * the properties of the creator as well as the ability to enumerate the creators assets in the AdobeMarket
 * and for retrieving the creator's avatar as a UIImage.
 */
@interface AdobeMarketCreator : NSObject

/**
 * Dimensions of the avatar image.
 */
@property (readwrite, nonatomic, assign) CGSize avatarSize;

/**
 * City location of the Creator
 */
@property (readwrite, nonatomic, strong) NSString *city;

/**
 * Country location of the Creator
 */
@property (readwrite, nonatomic, strong) NSString *country;

/**
 * Unique Id for the Creator
 */
@property (readwrite, nonatomic, strong) NSString *creatorID;

/**
 * Display name for the Creator
 */
@property (readwrite, nonatomic, strong) NSString *displayName;

/**
 * First name of the Creator
 */
@property (readwrite, nonatomic, strong) NSString *firstName;

/**
 * Array of images for the avatar the Creator
 */
@property (readwrite, nonatomic, strong) NSArray  *images;

/**
 * Last name of the Creator
 */
@property (readwrite, nonatomic, strong) NSString *lastName;

/**
 * State location of the Creator
 */
@property (readwrite, nonatomic, strong) NSString *state;

/**
 * User name of the Creator
 */
@property (readwrite, nonatomic, strong) NSString *userName;


/**
 * Get the creator's avatar.
 *
 * @param size The size of the avatar to return.
 * @param progressBlock Code block passed the current fraction completed for creating the avatar.
 * @param completionBlock Get the rendition data, and notified if returned from local cache.
 * @param cancellationBlock Code block run when the process is cancelled.
 * @param errorBlock Optionally be notified of an error.
 */
- (AdobeAssetAsyncRequest *)avatarImageForSize:(AdobeMarketCreatorAvatarSize)size
                                    onProgress:(void (^)(double fractionCompleted))progressBlock
                                  onCompletion:(void (^)(UIImage *image, BOOL fromCache))completionBlock
                                onCancellation:(void (^)(void))cancellationBlock
                                       onError:(void (^)(NSError *error))errorBlock;

/**
 * Get an array of all AdobeMarketAsset objects created by the creator asynchronously.
 *
 * @param page The current page of assets to retrieve
 * @param size The number of assets per page
 * @param priority The priority of the request.
 * @param progressBlock Optionally track the upload progress.
 * @param completionBlock Get the rendition data, and notified if returned from local cache.
 * @param cancellationBlock Optionally be notified of a cancellation on upload.
 * @param errorBlock Optionally be notified of an error.
 */
- (void)assetsForPage:(NSNumber *)page
              forSize:(NSNumber *)size
         withPriority:(NSOperationQueuePriority)priority
           onProgress:(void (^)(double fractionCompleted))progressBlock
         onCompletion:(void (^)(NSArray *marketAssets, NSNumber *totalPages, NSNumber *totalAssetCount))completionBlock
       onCancellation:(void (^)(void))cancellationBlock
              onError:(void (^)(NSError *error))errorBlock;

/**
 * launch the Behance Profile page of the creator in Safari.
 */
- (void)launchBehanceProfilePage;

@end
