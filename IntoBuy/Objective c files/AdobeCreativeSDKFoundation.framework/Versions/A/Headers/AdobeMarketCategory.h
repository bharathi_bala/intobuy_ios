/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

/**
 * Brushes category.
 */
extern NSString *const kMarketAssetsCategoryBrushes;

/**
 * The "For Placement" category.
 */
extern NSString *const kMarketAssetsCategoryForPlacement;

/**
 * Icons category.
 */
extern NSString *const kMarketAssetsCategoryIcons;

/**
 * Patterns category.
 */
extern NSString *const kMarketAssetsCategoryPatterns;

/**
 * User Interfaces category.
 */
extern NSString *const kMarketAssetsCategoryUserInterfaces;

/**
 * Vectors category.
 */
extern NSString *const kMarketAssetsCategoryVectors;

/**
 * AdobeMarketCategory represents a category in the Adobe Market. It provides methods that allow 
 * the client to obtain information about the category and to enumerate the categories available in 
 * the Creative Cloud Market.
 *
 * Currently the following constants can be used to for the corresponding category.
 *
 * - `kMarketAssetsCategoryBrushes`
 * - `kMarketAssetsCategoryForPlacement`
 * - `kMarketAssetsCategoryIcons`
 * - `kMarketAssetsCategoryPatterns`
 * - `kMarketAssetsCategoryUserInterfaces`
 * - `kMarketAssetsCategoryVectors`
 */
@interface AdobeMarketCategory : NSObject

/**
* Unique ID of the category.
*/
@property (readwrite, nonatomic, copy) NSString *categoryID;

/**
 * The category name.
 */
@property (readwrite, nonatomic, copy) NSString *categoryName;

/**
 * A friendly name for the category. This value can be displayed to the end user.
 */
@property (readwrite, nonatomic, copy) NSString *englishCategoryName;

/**
 * Whether the reciever has any subcategories. If so, @c subCategories can be used to get those 
 * subcategories.
 */
@property (readonly, nonatomic) BOOL hasSubCategories;

/**
 * List of subcategories. Each subcategory is an instance of AdobeMarketCategory.
 */
@property (readonly, nonatomic) NSArray *subCategories;

/**
 * Fetch all the available categories from the server asynchronously.
 *
 * @note If the logged-in user is not entitled to the Market Browser, the @c onError block will be
 * called with an appropriate error message.
 *
 * @param priority          The priority of the network request.
 * @param progressBlock     Optionally track the request progress. Specify @c NULL to omit.
 * @param completionBlock   Block to execute when the categories have been retrieved. An NSArray of 
 *                          AdobeMarketCategory object will be passed to that block.
 * @param cancellationBlock Optionally track whether the network operation was canclled. Specify 
 *                          @c NULL to omit.
 * @param errorBlock        Block to execute if an error occurs. Specify @c NULL to omit.
 */
+ (void)categories:(NSOperationQueuePriority)priority
        onProgress:(void (^)(double fractionCompleted))progressBlock
      onCompletion:(void (^)(NSArray *categories))completionBlock
    onCancellation:(void (^)(void))cancellationBlock
           onError:(void (^)(NSError *error))errorBlock;

@end
