/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

#import <Foundation/Foundation.h>

/**
 * Represents an element in a library composite.
 */
@interface AdobeLibraryElement : NSObject <NSMutableCopying>

#pragma mark Properties

/** The category id of the element. */
@property (readonly, nonatomic) NSString *categoryId;

/** The creation time of the element (time interval in seconds since 1970). */
@property (readonly, nonatomic) NSTimeInterval created;

/** The unique identifier of the element. */
@property (readonly, nonatomic) NSString *elementId;

/** Last modification time of the element (time interval in seconds since 1970). */
@property (readonly, nonatomic) NSTimeInterval modified;

/** The name of the element. */
@property (readonly, nonatomic) NSString *name;

/** The type of the asset element, e.g. 'application/vnd.adobe.element.color', 'application/vnd.adobe.element.image' */
@property (readonly, nonatomic) NSString *type;

@end
