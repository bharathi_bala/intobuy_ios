/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

#import <Foundation/Foundation.h>

/**
 * Indicates that the representation is a rendition. That is, a representation that is derivable from, and is visually similar to the primary representation. For example, a flattened PNG version of a PSD would be a rendition representation.
 */
extern NSString *const AdobeLibraryRepresenationRelationshipTypeRendition;

/**
 * The main representation - this should be the highest-fidelity form of the element, and is considered the most 'truthful' version of the element. For example, an image added from Photoshop would use a PSD as its primary representation, whereas one added from Illustrator would use an AI file.
 */
extern NSString *const AdobeLibraryRepresenationRelationshipTypePrimary;

/**
 * Indicates that this is an alternate representation. A representation that is not derivable from the primary representation, but stores alternative, or auxiliary data. This should be used sparingly.
 */
extern NSString *const AdobeLibraryRepresenationRelationshipTypeAlternate;

/**
 * Represents a representation of a library element.
 */
@interface AdobeLibraryRepresentation : NSObject <NSMutableCopying>

/** The application that created this representation. */
@property (readonly, nonatomic) NSString *application;

/** Whether or not this representation is a full size rendition of the element */
@property (readonly, nonatomic, getter = isFullSize) BOOL fullSize;

/** The height of the representation. */
@property (readonly, nonatomic) NSNumber *height;

/** The md5 hash of the asset on the server if this is a file based representation, otherwise nil. */
@property (readonly, nonatomic) NSString *md5;

/** The unique identifier of the representation. */
@property (readonly, nonatomic) NSString *representationId;

/** The content-type of the representation. */
@property (readonly, nonatomic) NSString *type;

/** The relationship of the representation. See above for built-in values. */
@property (readonly, nonatomic) NSString *relationship;

/** The width of the representation. */
@property (readonly, nonatomic) NSNumber *width;

/**
 * \brief Returns the value for the property identified by a given key.
 *
 * \param key             The key of the value to search for.
 * \param keyNamespace    The namespace for the key.
 *
 * \return returns the value of the property identified by the key and namespace.
 */
- (id)valueForKey:(NSString *)key forNamespace:(NSString *)keyNamespace;

/**
 * \brief Returns whether or not this representation is of the requested type or
 *              of a type that is compatible with the requested type.
 *
 * \param requestedType	The type to check this representation for.
 *
 * \return whether or not this representation is compatible with the requested type.
 */
- (BOOL)isCompatibleType:(NSString *)requestedType;

@end
