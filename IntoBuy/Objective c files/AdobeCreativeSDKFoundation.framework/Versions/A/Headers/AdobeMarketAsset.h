/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "AdobeMarketCategory.h"
#import "AdobeMarketCreator.h"

@class AdobeAssetAsyncRequest;

/**
 * The content-types for which to request a market asset rendition.
 */
typedef NS_ENUM (NSInteger, AdobeMarketAssetFileRenditionType)
{
    /** The JPEG file rendition type on the Creative Cloud */
    AdobeMarketAssetFileRenditionTypeJPEG = 0,

    /** The PNG file rendition type on the Creative Cloud */
    AdobeMarketAssetFileRenditionTypePNG
};

/**
 * AdobeMarketImageDimension represents the image dimensions in width and height
 * of an AdobeMarketAsset
 */
typedef NS_ENUM (NSInteger, AdobeMarketImageDimension)
{
    /** Width of the image. */
    AdobeMarketImageDimensionWidth  = 0,
    
    /** Height of the image. */
    AdobeMarketImageDimensionHeight = 1
};

/**
 * AdobeMarketAssetSortType is an enumerated type that allows the client to specify
 * sorting options when accessing market assets
 */
typedef NS_ENUM (NSInteger, AdobeMarketAssetSortType)
{
    /** Sort by featured. */
    AdobeMarketAssetSortTypeFeatured   = 0,
    
    /** Sort by date. */
    AdobeMarketAssetSortTypeDate       = 1,
    
    /** Sort by relevance. */
    AdobeMarketAssetSortTypeRelevance  = 2,
    
    /** Sort by downloaded. */
    AdobeMarketAssetSortTypeDownloaded = 3
};

/**
 * AdobeMarketAsset represents an asset in the Creative Cloud Market. It provides accessors that 
 * allow the client to obtain information about the asset, download the asset, download thumbnails 
 * and previews of the asset, and browse for 'similar' assets given an asset.
 */
@interface AdobeMarketAsset : NSObject

/**
 * Unique identifier for the asset.
 */
@property (nonatomic, copy) NSString *assetID;

/**
 * The category where the asset may be found.
 */
@property (nonatomic, strong) AdobeMarketCategory *category;

/**
 * Creator of the asset.
 */
@property (nonatomic, strong) AdobeMarketCreator *creator;

/**
 * Date the asset was created.
 */
@property (nonatomic, strong) NSDate *dateCreated;

/**
 * Date the asset was published.
 */
@property (nonatomic, strong) NSDate *datePublished;

/**
 * Dimensions of the asset.
 */
@property (nonatomic, assign) CGSize dimensions;

/**
 * Whether or not the asset has been downloaded to the user's Creative Cloud files.
 */
@property (nonatomic, getter = isDownloaded) BOOL downloaded;

/**
 * The date the asset was featured.
 */
@property (nonatomic, strong) NSDate *featured;

/**
 * File size of the asset.
 */
@property (nonatomic, assign) long fileSize;

/**
 * Label of the asset.
 */
@property (nonatomic, copy) NSString *label;

/**
 * MIME type of the asset.
 */
@property (nonatomic, copy) NSString *nativeMimeType;

/**
 * Dimensions for the preview image representing the asset.
 */
@property (nonatomic, assign) CGSize previewDimensions;

/**
 * Additional MIME types that may be used with the asset.
 */
@property (nonatomic, copy) NSArray *supportedMimeTypes;

/**
 * Array of tags related to the asset.
 */
@property (nonatomic, copy) NSArray *tags;

/**
 * Dimensions for the thumbnail representing the asset.
 */
@property (nonatomic, assign) CGSize thumbnailDimensions;

/**
 * Title of the asset.
 */
@property (nonatomic, copy) NSString *title;

/**
 * Fetch Market Assets based on the specified parameters asynchronously.
 *
 * This method will use the passed in arguments to construct a network request to fetch Market 
 * Assets from the Creative Cloud Market Browser.
 *
 * @note If the logged-in user is not entitled to the Market Browser, the @c onError block will be 
 * called with an appropriate error message.
 *
 * @param nextPage          The HREF for the next page of assets or @c nil to fetch the first page. 
 *                          The completion block will indicate the next page's HREF value that can 
 *                          be specified for this argument to fetch that page.
 * @param priority          Priority of the network request.
 * @param queryString       A string query to further filter asset based on their names and tags.
 * @param sortType          Defines how the results should be sorted.
 * @param creator           Filter the returned assets to the specified creator. Specify @c nil to 
 *                          omit.
 * @param categories        Filter the assets to the specified categories. Specify @c nil to omit.
 * @param subCategories     Filter the returned assets to the specified subcategories. The 
 *                          specified subcategory must match it's super-category that is specified 
 *                          for the @c categores argument. Specify @c to omit.
 * @param progressBlock     Optionally track the progress of the network request. Specify @c NULL 
 *                          to omit.
 * @param completionBlock   Block to execute when all the request assets have been fetched. An 
 *                          array of @c AdobeMarketAsset objects and an NSString HREF value for the 
 *                          next page of assets will be passed to that block. If there are no more 
 *                          page available, the HREF value will be @c nil.
 * @param cancellationBlock Optionally track the cancellation of the network request. Specify 
 *                          @c NULL to omit.
 * @param errorBlock        Bock to execute if an error occurs. Specify @c NULL to omit.
 */
+ (void)assetsForNextPage:(NSString *)nextPage
             withPriority:(NSOperationQueuePriority)priority
                 forQuery:(NSString *)queryString
              forSortType:(AdobeMarketAssetSortType)sortType
               forCreator:(NSString *)creator
            forCategories:(NSArray *)categories
         forSubCategories:(NSArray *)subCategories
               onProgress:(void (^)(double fractionCompleted))progressBlock
             onCompletion:(void (^)(NSArray *assets, NSString *nextPageHref))completionBlock
           onCancellation:(void (^)(void))cancellationBlock
                  onError:(void (^)(NSError *error))errorBlock;

/**
 * Copy the asset to the logged-in user's account.
 *
 * @param priority        Priority of the request.
 * @param completionBlock Block that is called when the asset has been copied.
 * @param errorBlock      Called when an error occurs.
 *
 * @return An asynchronous request object that can be used to monitor the progress of the request
 * and optionally cancel/interrupt it.
 */
- (AdobeAssetAsyncRequest *)copyAssetWithRequestPriority:(NSOperationQueuePriority)priority
                                            onCompletion:(void (^)(NSDictionary *))completionBlock
                                                 onError:(void (^)(NSError *))errorBlock;


/**
 * Get the data of the file asynchronously.
 *
 * The downloaded bits for the asset will be stored in memory as they arrive from the network. If
 * the asset being downloaded is large, this method could use a significant amount of memory, which
 * could potentially cause your application to be terminated. It is recommended to only use this
 * method for downloading small assets. For downloading assets of any arbitrary size, consider using
 * @c downloadToPath:withPriority:mimeType:onProgress:completion:cancellation:error: which stores
 * the downloaded bits in a temporary file on disk during download.
 *
 * @param priority The priority of the request.
 * @param mimeType Mime type of the file.
 * @param progressBlock Optionally track the upload progress.
 * @param completionBlock Get the rendition data, and notified if returned from local cache.
 * @param cancellationBlock Optionally be notified of a cancellation on upload.
 * @param errorBlock Optionally be notified of an error.
 *
 * @see downloadToPath:withPriority:mimeType:onProgress:completion:cancellation:error:
 */
- (AdobeAssetAsyncRequest *)download:(NSOperationQueuePriority)priority
                        withMimeType:(NSString *)mimeType
                          onProgress:(void (^)(double fractionCompleted))progressBlock
                        onCompletion:(void (^)(NSData *data))completionBlock
                      onCancellation:(void (^)(void))cancellationBlock
                             onError:(void (^)(NSError *error))errorBlock;

/**
 * Cancel the download request.
 *
 * Calling this method will cancel the underlying HTTP request that was created for the download
 * request by calling @c download:withMimeType:onProgress:onCompletion:onCancellation:onError:
 *
 * @see download:withMimeType:onProgress:onCompletion:onCancellation:onError:
 */
- (void)cancelDownloadRequest;

/**
 * Download the Market Asset to the specified @c filePath asynchronously.
 *
 * This method is the preferred way for downloading Market Assets since the downloaded bits are
 * stored in a temporary file on disk instead of memory. Once the download operation has finished,
 * the downloaded bits will be available at the specified file system path.
 *
 * @note The downloaded bits are only accessible @em after the file has been fully downloaded. To
 * observer the progress of the download process, use the @c onProgress block.
 *
 * @param filePath            File system path where the downloaded bits will be placed once the
 *                            download process has completed.
 * @param priority            The initial priority of the request. This can be changed using the
 *                            returned object from the this method.
 * @param mimeType            MIME type of the downloaded file.
 * @param progressHandler     Optionally used to track the progress of the download task. Specify
 *                            @c NULL if you don't want to track the download progress.
 * @param completionHandler   The block to execute after the file has been successfully downloaded.
 *                            The file system path to the downloaded file will be returned.
 * @param cancellationHandler Optionally used to be notified of cancellation of the request.
 *                            Specify @c NULL to omit the block.
 * @param errorHandler        Optionally be notified of any errors. Specify @c NULL to omit the
 *                            error block.
 *
 * @return An instance of the @c AdobeAssetAsyncRequst used to control the request. The request
 *         priority can be changed or the request itself can be cancelled.
 */
- (AdobeAssetAsyncRequest *)downloadToPath:(NSString *)filePath
                              withPriority:(NSOperationQueuePriority)priority
                                  mimeType:(NSString *)mimeType
                                onProgress:(void (^)(double fractionCompleted))progressHandler
                                completion:(void (^)(AdobeMarketAsset *marketAsset))completionHandler
                              cancellation:(void (^)(void))cancellationHandler
                                     error:(void (^)(NSError *error))errorHandler;

/**
 * Get the rendition of the asset as a UIImage asynchronously and scale along an edge.
 *
 * The maximum size value is 1024. Any values larger are clipped to 1024.
 *
 * @param dimension Width or height to scale to.
 * @param size Max length of the edge we scale to.
 * @param type Rendition file type.
 * @param priority The priority of the request.
 * @param progressBlock Optionally track the upload progress.
 * @param completionBlock Get the rendition data as a UIImage, and notified if returned from local cache.
 * @param cancellationBlock Optionally be notified of a cancellation on upload.
 * @param errorBlock Optionally be notified of an error.
 */
- (AdobeAssetAsyncRequest *)downloadRenditionWithDimension:(AdobeMarketImageDimension)dimension
                                                  withSize:(NSUInteger)size
                                                  withType:(AdobeMarketAssetFileRenditionType)type
                                              withPriority:(NSOperationQueuePriority)priority
                                                onProgress:(void (^)(double fractionCompleted))progressBlock
                                              onCompletion:(void (^)(UIImage *image, BOOL fromCache))completionBlock
                                            onCancellation:(void (^)(void))cancellationBlock
                                                   onError:(void (^)(NSError *error))errorBlock __deprecated_msg("Use downloadRenditionWithDimension:size:type:priority:progressBlock:completionBlock:cancellationBlock:errorBlock");

/**
 * Get the rendition of the asset as NSData asynchronously and scale along an edge.
 *
 * The maximum size value is 1024. Any values larger are clipped to 1024.
 *
 * @param dimension Width or height to scale to.
 * @param size Max length of the edge we scale to.
 * @param type Rendition file type.
 * @param priority The priority of the request.
 * @param progressBlock Optionally track the upload progress.
 * @param completionBlock Get the rendition data, and notified if returned from local cache.
 * @param cancellationBlock Optionally be notified of a cancellation on upload.
 * @param errorBlock Optionally be notified of an error.
 */
- (AdobeAssetAsyncRequest *)downloadRenditionWithDimension:(AdobeMarketImageDimension)dimension
                                                      size:(NSUInteger)size
                                                      type:(AdobeMarketAssetFileRenditionType)type
                                                  priority:(NSOperationQueuePriority)priority
                                             progressBlock:(void (^)(double fractionCompleted))progressBlock
                                           completionBlock:(void (^)(NSData *imageData, BOOL fromCache))completionBlock
                                         cancellationBlock:(void (^)(void))cancellationBlock
                                                errorBlock:(void (^)(NSError *error))errorBlock;

/**
 * Cancel the rendition request.
 */
- (void)cancelRenditionRequest;

/**
 * Change the priority of the rendition request.
 *
 * @param priority The priority of the request.
 */
- (void)changeRenditionRequestPriority:(NSOperationQueuePriority)priority;

/**
 * Get an array of AdobeMarketAsset objects that are similar to the current asset asynchronously.
 *
 * @param page The current page of assets to retrieve
 * @param size The number of assets per page
 * @param priority The priority of the request.
 * @param progressBlock Optionally track the upload progress.
 * @param completionBlock Get the rendition data, and notified if returned from local cache.
 * @param cancellationBlock Optionally be notified of a cancellation on upload.
 * @param errorBlock Optionally be notified of an error.
 */
- (void)similarAssetsForPage:(NSNumber *)page
                     forSize:(NSNumber *)size
                withPriority:(NSOperationQueuePriority)priority
                  onProgress:(void (^)(double fractionCompleted))progressBlock
                onCompletion:(void (^)(NSArray *marketAssets, NSNumber *totalPages, NSNumber *totalAssetCount))completionBlock
              onCancellation:(void (^)(void))cancellationBlock
                     onError:(void (^)(NSError *error))errorBlock;

@end
