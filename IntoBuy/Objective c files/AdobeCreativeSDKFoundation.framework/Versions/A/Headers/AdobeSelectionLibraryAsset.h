/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

#import "AdobeSelection.h"

/**
 * A utility to help determine if an item is an AdobeSelectionLibraryAsset.
 */
#define IsAdobeSelectionLibraryAsset(item) ([item isKindOfClass:[AdobeSelectionLibraryAsset class]])

/**
 * Represents an Adobe Library selected asset.
 */
@interface AdobeSelectionLibraryAsset : AdobeSelection

/** Selected color values for the library. */
@property (strong, nonatomic, readonly) NSArray *selectedColorIDs;
/** Selected color themes for the library. */
@property (strong, nonatomic, readonly) NSArray *selectedColorThemeIDs;
/** Selected image values for the library. */
@property (strong, nonatomic, readonly) NSArray *selectedImageIDs;

@end
