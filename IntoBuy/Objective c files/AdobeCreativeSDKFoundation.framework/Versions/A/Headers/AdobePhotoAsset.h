/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

#import "AdobePhoto.h"

#import "AdobePhotoAssetRendition.h"
#import "AdobePhotoTypes.h"

@class AdobePhotoCollection;
@class AdobePhotoCatalog;
@class AdobePhotoAssetRevision;

/** Asset Type */
typedef NS_ENUM (NSInteger, AdobePhotoAssetType)
{
    /** Image type */
    AdobePhotoAssetTypeImage = 1,
};

/**
 * Keys to use to reference AdobePhotoAssetRenditions in the rendition dictionary.
 */
extern NSString *const AdobePhotoAssetRenditionImageFullSize;
extern NSString *const AdobePhotoAssetRenditionImagePreview;
extern NSString *const AdobePhotoAssetRenditionImageFavorite;
extern NSString *const AdobePhotoAssetRenditionImage2048;
extern NSString *const AdobePhotoAssetRenditionImage1024;
extern NSString *const AdobePhotoAssetRenditionImageThumbnail2x;
extern NSString *const AdobePhotoAssetRenditionImageThumbnail;

/**
 * Metadata extraction rules used when generating renditions.
 */
extern NSString *const AdobePhotoAssetRenditionMetadataExtractAll;
extern NSString *const AdobePhotoAssetRenditionMetadataExtractNone;
extern NSString *const AdobePhotoAssetRenditionMetadataExtractCaption;
extern NSString *const AdobePhotoAssetRenditionMetadataExtractGeometry;
extern NSString *const AdobePhotoAssetRenditionMetadataExtractLocation;
extern NSString *const AdobePhotoAssetRenditionMetadataExtractXMP;
extern NSString *const AdobePhotoAssetRenditionMetadataSuppressCaption;
extern NSString *const AdobePhotoAssetRenditionMetadataSuppressGeometry;
extern NSString *const AdobePhotoAssetRenditionMetadataSuppressLocation;
extern NSString *const AdobePhotoAssetRenditionMetadataSuppressXMP;

typedef NSDictionary AdobePhotoAssetRenditionDictionary;
typedef NSArray      AdobePhotoAssetRenditionTypes;
typedef NSArray      AdobePhotoAssetRenditionMetadataRules;

/**
 * A utility to help determine if an AdobePhoto is an AdobePhotoAsset.
 */
#define IsAdobePhotoAsset(item) ([item isKindOfClass:[AdobePhotoAsset class]])

/**
 * An asset is a single photo or video (when supported). Each asset has one or more revisions; a revision can contain multiple files or renditions of the asset.
 */
@interface AdobePhotoAsset : AdobePhoto <NSCopying, NSCoding>

/**
 * The catalog the asset is in.
 */
@property (readonly, nonatomic, strong) AdobePhotoCatalog *catalog;

/**
 * The asset's content-type.
 */
@property (nonatomic, readonly, strong) NSString *contentType;

/**
 * This asset's master.
 */
@property (nonatomic, readonly, strong) NSURL *master;

/**
 * Whether the master data exists in the cloud. If the asset was uploaded by Lightroom Desktop, this will be set to NO.
 */
@property (nonatomic, readonly, assign) BOOL masterDataExists;

/**
 * The asset metadata.
 */
@property (nonatomic, readonly, strong) NSDictionary *metadata;

/**
 * The name of the item.
 */
@property (nonatomic, readonly, strong) NSString *name;

/**
 * The asset's order (only valid in a collection).
 */
@property (nonatomic, strong) NSString *order;

/**
 * This asset's proxy.
 */
@property (nonatomic, readonly, strong) NSURL *proxy;

/**
 * Whether the proxy data exists in the cloud. If the asset was uploaded by Lightroom Desktop, this will be set to YES.
 */
@property (nonatomic, readonly, assign) BOOL proxyDataExists;

/**
 * The asset's renditions.
 * The renditions are keyed using the AdobePhotoAssetRenditionImage and AdobePhotoAssetRenditionVideo constants defined above.
 */
@property (nonatomic, readonly, strong) AdobePhotoAssetRenditionDictionary *renditions;

/**
 * The asset's size.
 */
@property (nonatomic, readonly, assign) long long size;

/**
 * The asset's type.
 */
@property (nonatomic, readonly, assign) AdobePhotoAssetType type;

/**
 * This asset's current (latest) revision.
 */
@property (nonatomic, readonly, strong) AdobePhotoAssetRevision * latestRevision __deprecated_msg("This property is being removed and no alternative is being added.");

/**
 * This asset's revision.
 */
@property (nonatomic, readonly, strong) AdobePhotoAssetRevision * revision __deprecated_msg("This property is being removed and no alternative is being added.");

/**
 * Create (upload) a new asset and add into a collection on the Adobe Photo service asynchronously.
 * For an image file: Preview, standard, and 2x thumbnail renditions are generated automatically.
 *
 * @param name The name of the asset.
 * @param collection The collection to add the asset into.
 * @param path The local path to the data. The path must be locally valid and cannot be nil.
 * @param type The content-type of the asset. The content-type is required and cannot be nil.
 * @param progressBlock Optionally track the upload progress.
 * @param completionBlock Optionally get an updated reference to the created AdobePhotoAsset when complete.
 * @param cancellationBlock Optionally be notified of a cancellation on upload.
 * @param errorBlock Optionally be notified of an error.
 */
+ (AdobePhotoAsset *)create:(NSString *)name
               inCollection:(AdobePhotoCollection *)collection
               withDataPath:(NSURL *)path
            withContentType:(NSString *)type
                 onProgress:(void (^)(double fractionCompleted))progressBlock
               onCompletion:(void (^)(AdobePhotoAsset *asset))completionBlock
             onCancellation:(void (^)(void))cancellationBlock
                    onError:(void (^)(NSError *error))errorBlock;

/**
 * Cancel the creation request.
 */
- (void)cancelCreationRequest;

/**
 * Change the priority of the upload. By default: NSOperationQueuePriorityNormal.
 *
 * @param priority The priority of the request.
 */
- (void)changeCreationRequestPriority:(NSOperationQueuePriority)priority;

/**
 * Refresh this asset from the Adobe Photo service asynchronously.
 *
 * @param completionBlock Optionally be notified when complete.
 * @param errorBlock Optionally be notified of an error.
 */
- (void)refresh:(void (^)(AdobePhotoAsset *asset))completionBlock
        onError:(void (^)(NSError *error))errorBlock;

/**
 * Delete this asset from the Adobe Photo service asynchronously.
 *
 * @param completionBlock Optionally be notified when complete.
 * @param errorBlock Optionally be notified of an error.
 */
- (void)delete:(void (^)(void))completionBlock
       onError:(void (^)(NSError *error))errorBlock;

/**
 * Upload the master data (limited to 200MB) for this asset on the Adobe Photo service asynchronously.
 * Since the connection may be unstable, we recommend that the data is no more that 20MB.
 *
 * @param path The path to the master data. The path must be locally valid and cannot be nil.
 * @param priority The priority of the request.
 * @param generate If renditions should be automatically generated.
 * @param progressBlock Optionally track the upload progress.
 * @param completionBlock Optionally get an updated reference to the asset when complete.
 * @param cancellationBlock Optionally be notified of a cancellation on upload.
 * @param errorBlock Optionally be notified of an error.
 */
- (void)uploadMasterDataFromPath:(NSURL *)path
                    withPriority:(NSOperationQueuePriority)priority
          autoGenerateRenditions:(BOOL)generate
                      onProgress:(void (^)(double fractionCompleted))progressBlock
                    onCompletion:(void (^)(AdobePhotoAsset *asset))completionBlock
                  onCancellation:(void (^)(void))cancellationBlock
                         onError:(void (^)(NSError *error))errorBlock;

/**
 * Upload the master data for this asset on the Adobe Photo service asynchronously.
 *
 * @param rendition The rendition.
 * @param priority The priority of the request.
 * @param progressBlock Optionally track the upload progress.
 * @param completionBlock Optionally get an updated reference to the asset when complete.
 * @param cancellationBlock Optionally be notified of a cancellation on upload.
 * @param errorBlock Optionally be notified of an error.
 */
- (void)uploadRendition:(AdobePhotoAssetRendition *)rendition
           withPriority:(NSOperationQueuePriority)priority
             onProgress:(void (^)(double fractionCompleted))progressBlock
           onCompletion:(void (^)(AdobePhotoAsset *asset))completionBlock
         onCancellation:(void (^)(void))cancellationBlock
                onError:(void (^)(NSError *error))errorBlock;

/**
 * Cancel the upload request.
 */
- (void)cancelUploadRequest;

/**
 * Change the priority of the upload. By default: NSOperationQueuePriorityNormal.
 *
 * @param priority The priority of the request.
 */
- (void)changeUploadRequestPriority:(NSOperationQueuePriority)priority;

/**
 * Get the master data for this asset on the Adobe Photo service asynchronously.
 *
 * Be careful with this API as the data size may be quite large (i.e. check size before pulling). To
 * downloads files of any size consider using
 * @c downloadMasterDataToFile:withPriority:onProgress:onCompletion:onCancellation:onError: which stores
 * the downloaded bits in a temporary file on disk.
 *
 * @param priority The priority of the request.
 * @param progressBlock Optionally track the download progress.
 * @param completionBlock Returns the master data when complete.
 * @param cancellationBlock Optionally be notified of a cancellation on download.
 * @param errorBlock Optionally be notified of an error.
 *
 * Once the download task has been created, the priority of the underlying HTTP request can be
 * changed using the @c changeDownloadRequestPriority: method. The download action can
 * also be cancelled using the @c cancelDownloadRequest method.
 *
 * @see changeDownloadRequestPriority:
 * @see cancelDownloadRequest
 * @see downloadMasterDataToFile:withPriority:onProgress:onCompletion:onCancellation:onError:
 */
- (void)downloadMasterData:(NSOperationQueuePriority)priority
                onProgress:(void (^)(double fractionCompleted))progressBlock
              onCompletion:(void (^)(NSData *data, BOOL wasCached))completionBlock
            onCancellation:(void (^)(void))cancellationBlock
                   onError:(void (^)(NSError *error))errorBlock;

/**
 * Cancel the download request.
 */
- (void)cancelDownloadRequest;

/**
 * Change the priority of the download. By default: NSOperationQueuePriorityNormal.
 *
 * @param priority The priority of the request.
 */
- (void)changeDownloadRequestPriority:(NSOperationQueuePriority)priority;

/**
 * Download the master data for the receiver from the Adobe Photo service asynchronously.
 *
 * The downloaded bits are stored in a temporary file on disk as they arrive. This prevents
 * excessive memory usage when downloading large files. Generally this method should be used and
 * preferred to @c downloadMasterData:onProgress:onCompletion:onCancellation:onError:.
 *
 * Once the download task has been created, the priority of the underlying HTTP request can be
 * changed using the @c changeDownloadRequestPriority: method. The download action can
 * also be cancelled using the @c cancelDownloadRequest method.
 *
 * @param filePath            File system path where the final downloaded file should be stored.
 * @param priority            Priority of the request task.
 * @param progressHandler     Optionally track the download progress. Specify @c NULL to omit.
 * @param completionHandler   Block to execute when the download task has completed successfully.
 * @param cancellationHandler Optionally be notified of cancellation of the task. Specify @c NULL
 *                            to omit.
 * @param errorHandler        Optionally be notified of any errors. Specify @c NULL to omit.
 *
 * @see changeDownloadRequestPriority:
 * @see cancelDownloadRequest
 */
- (void)downloadMasterDataToFile:(NSString *)filePath
                    withPriority:(NSOperationQueuePriority)priority
                      onProgress:(void (^)(double fractionCompleted))progressHandler
                    onCompletion:(void (^)(AdobePhotoAsset *photoAsset))completionHandler
                  onCancellation:(void (^)(void))cancellationHandler
                         onError:(void (^)(NSError *error))errorHandler;

/**
 * Get the proxy data (DNG) for this asset on the Adobe Photo service asynchronously.
 *
 * @param priority The priority of the request.
 * @param progressBlock Optionally track the download progress.
 * @param completionBlock Returns the master data when complete.
 * @param cancellationBlock Optionally be notified of a cancellation on download.
 * @param errorBlock Optionally be notified of an error.
 *
 * Once the download task has been created, the priority of the underlying HTTP request can be
 * changed using the @c changeDownloadRequestPriority: method. The download action can
 * also be cancelled using the @c cancelDownloadRequest method.
 *
 * @see changeDownloadRequestPriority:
 * @see cancelDownloadRequest
 */
- (void)downloadProxyData:(NSOperationQueuePriority)priority
               onProgress:(void (^)(double fractionCompleted))progressBlock
             onCompletion:(void (^)(NSData *data, BOOL wasCached))completionBlock
           onCancellation:(void (^)(void))cancellationBlock
                  onError:(void (^)(NSError *error))errorBlock;

/**
 * Download the proxy data (DNG) for this asset from the Adobe Photo service asynchronously.
 *
 * The downloaded bits are stored in a temporary file on disk as they arrive. This prevents
 * excessive memory usage when downloading large files. Generally this method should be used and
 * preferred to @c downloadProxyData:onProgress:onCompletion:onCancellation:onError:.
 *
 * Once the download task has been created, the priority of the underlying HTTP request can be
 * changed using the @c changeDownloadRequestPriority: method. The download action can
 * also be cancelled using the @c cancelDownloadRequest method.
 *
 * @param filePath            File system path where the final downloaded file should be stored.
 * @param priority            Priority of the request task.
 * @param progressHandler     Optionally track the download progress. Specify @c NULL to omit.
 * @param completionHandler   Block to execute when the download task has completed successfully.
 * @param cancellationHandler Optionally be notified of cancellation of the task. Specify @c NULL
 *                            to omit.
 * @param errorHandler        Optionally be notified of any errors. Specify @c NULL to omit.
 *
 * @see changeDownloadRequestPriority:
 * @see cancelDownloadRequest
 */
- (void)downloadProxyDataToFile:(NSString *)filePath
                   withPriority:(NSOperationQueuePriority)priority
                     onProgress:(void (^)(double fractionCompleted))progressHandler
                   onCompletion:(void (^)(AdobePhotoAsset *photoAsset))completionHandler
                 onCancellation:(void (^)(void))cancellationHandler
                        onError:(void (^)(NSError *error))errorHandler;

/**
 * Change the priority of the underlying HTTP request that was created by for
 * @c downloadMasterDataToFile:withPriority:onProgress:onCompletion:onCancellation:onError:
 *
 * @param priority The new priority of the HTTP request.
 *
 * @see downloadMasterDataToFile:withPriority:onProgress:onCompletion:onCancellation:onError:
 */
- (void)changeDownloadMasterDataToFilePriority:(NSOperationQueuePriority)priority __deprecated_msg("Use changeDownloadRequestPriority: instead.");

/**
 * Cancel the underlying HTTP request that was created for
 * @c downloadMasterDataToFile:withPriority:onProgress:onCompletion:onCancellation:onError:
 *
 * @see downloadMasterDataToFile:withPriority:onProgress:onCompletion:onCancellation:onError:
 */
- (void)cancelDownloadMasterDataToFileRequest __deprecated_msg("Use canceDownloadRequest instead.");

/**
 * Generate a series of renditions for this asset on the Adobe Photo service asynchronously.
 *
 * @param renditions The renditions to generate.
 * @param priority The priority of the request.
 * @param completionBlock Returns the master data when complete.
 * @param errorBlock Optionally be notified of an error.
 */
- (void)generateRenditions:(AdobePhotoAssetRenditions *)renditions
              withPriority:(NSOperationQueuePriority)priority
              onCompletion:(void (^)(AdobePhotoAsset *photoAsset))completionBlock
                   onError:(void (^)(NSError *error))errorBlock;

/**
 * Get the rendition data for this asset on the Adobe Photo service asynchronously.
 *
 * @param rendition The rendition to download.
 * @param priority The priority of the request.
 * @param progressBlock Optionally track the download progress.
 * @param completionBlock Returns the master data when complete.
 * @param cancellationBlock Optionally be notified of a cancellation on download.
 * @param errorBlock Optionally be notified of an error.
 */
- (void)downloadRendition:(AdobePhotoAssetRendition *)rendition
             withPriority:(NSOperationQueuePriority)priority
               onProgress:(void (^)(double fractionCompleted))progressBlock
             onCompletion:(void (^)(NSData *data, BOOL wasCached))completionBlock
           onCancellation:(void (^)(void))cancellationBlock
                  onError:(void (^)(NSError *error))errorBlock;

/**
 * A utility to test the equlity of two AdobePhotoAssets.
 *
 * @param asset The AdobePhotoAsset to test against.
 *
 * @returns True if the assets are the same.
 */
- (BOOL)isEqualToAsset:(AdobePhotoAsset *)asset;

@end
