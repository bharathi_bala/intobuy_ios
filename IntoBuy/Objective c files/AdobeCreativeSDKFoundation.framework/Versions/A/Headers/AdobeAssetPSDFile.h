/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

#import "AdobeAssetFile.h"
#import "AdobePSDComposite.h"

@class AdobePSDPreview;

/**
 * A utility to help determine if an AdobeAsset is an AdobeAssetPSDFile.
 */
#define IsAdobeAssetPSDFile(item) ([item isKindOfClass:[AdobeAssetPSDFile class]])

/**
 * AdobeAssetPSDFile represents a Photoshop Document (PSD file).  It provides methods for reading
 * PSD previews and composites from PSD files, creating PSD files from composites, and getting
 * layer renditions from PSD files.
 */
@interface AdobeAssetPSDFile : AdobeAssetFile

#pragma mark Create

/**
 * Create a PSD file by asynchronously pushing a PSD composite structure to Adobe Creative
 * Cloud. This operation performs a pushPSDComposite operation on
 * the returned AdobeAssetPSDFile instance, which can then be used to either change the request
 * priority or to cancel it.
 *
 * @param psdComposite The PSD composite used to create the PSD file on Adobe Creative Cloud.
 * @param overwrite In the case of a new PSD composite whether it should overwrite an already existing
 * composite. Set this to YES if a previous push has failed with an AdobeDCXErrorCompositeAlreadyExists
 * @param progressBlock Optionally, track the upload progress.
 * @param completionBlock Optionally, get an updated reference to the created file when complete.
 * @param cancellationBlock Optionally, be notified of a cancellation on upload.
 * @param errorBlock Optionally, be notified of an error.
 *
 * @returns A place holder pointer to the AdobeAssetPSDFile.
 */
+ (AdobeAssetPSDFile *)createWithPSDComposite:(AdobePSDComposite *)psdComposite
                            overwriteExisting:(BOOL)overwrite
                                   onProgress:(void (^)(double fractionCompleted))progressBlock
                                 onCompletion:(void (^)(AdobeAssetPSDFile *file))completionBlock
                               onCancellation:(void (^)(void))cancellationBlock
                                      onError:(void (^)(NSError *error))errorBlock;

#pragma mark PSD Preview

/**
 * Gets a read-only description of the PSD file from Adobe Creative Cloud.
 *
 * @param layerCompId Optionally, the id of a layer comp to apply before generating the preview. If nil
 * no layer comp will be explicitly applied. TODO -- NOT YET IMPLEMENTED
 * @param progressBlock Optionally, track the download progress of the composite representation.
 * @param completionBlock Optionally, get a reference to a PSD composite object for the composite representation when download is complete.
 * @param cancellationBlock Optionally, be notified of a cancellation on download.
 * @param errorBlock Optionally, be notified of an error.
 */

- (void)getPreviewWithAppliedLayerCompId:(NSNumber *)layerCompId
                              onProgress:(void (^)(double))progressBlock
                            onCompletion:(void (^)(AdobePSDPreview *psdPreview))completionBlock
                          onCancellation:(void (^)(void))cancellationBlock
                                 onError:(void (^)(NSError *error))errorBlock;

#pragma mark PSD Composite Pull

/**
 * Create a PSD composite object for this Creative Cloud file and pull down its composite
 * representation asynchronously from the cloud. The returned PSD composite object
 * can then be used to inspect or modify the PSD file via its composite representation.
 *
 * @param path The path where the local composite is stored.
 * @param progressBlock Optionally, track the download progress of the composite representation.
 * @param completionBlock Optionally, get a reference to a PSD composite object for the composite representation when download is complete.
 * @param cancellationBlock Optionally, be notified of a cancellation on download.
 * @param errorBlock Optionally, be notified of an error.
 */
- (void)pullPSDCompositeAt:(NSString *)path
                onProgress:(void (^)(double))progressBlock
              onCompletion:(void (^)(AdobePSDComposite *psdComposite))completionBlock
            onCancellation:(void (^)(void))cancellationBlock
                   onError:(void (^)(NSError *error))errorBlock;

- (void)pullMinimalPSDCompositeAt:(NSString *)path
                       onProgress:(void (^)(double))progressBlock
                     onCompletion:(void (^)(AdobePSDComposite *psdComposite))completionBlock
                   onCancellation:(void (^)(void))cancellationBlock
                          onError:(void (^)(NSError *error))errorBlock;


/**
 * Cancel the rendition request.
 */
- (void)cancelPullPSDCompositeRequest;

/**
 * Change the priority of the rendition request.
 *
 * @param priority The priority of the request.
 */
- (void)changePSDCompositePullRequestPriority:(NSOperationQueuePriority)priority;

#pragma mark PSD Composite Push

/**
 * Cancel the push request.
 */
- (void)cancelPushPSDCompositeRequest;

/**
 * Change the priority of the push request.
 *
 * @param priority The priority of the push request.
 */
- (void)changePushPSDCompositeRequestPriority:(NSOperationQueuePriority)priority;

#pragma mark Renditions

/**
 * Get a rendition of a layer in the PSD asynchronously.
 *
 * @param layerID The ID of the layer for which a rendition should be generated. Pass nil to render the entire PSD.
 * @param layerCompID The ID of the layerComp which should be applied. Pass nil to apply the default layerComp.
 * @param type The type of rendition to request.
 * @param dimensions The dimensions of the rendition in 'points'.  The value gets adjusted to
 *              pixels depending on the screen resolution.  The largest of the dimensions' width and
 *              height sets the bounds for the rendition size (width and height).
 * @param priority The priority of the request.
 * @param progressBlock Optionally, track the upload progress.
 * @param completionBlock Get the rendition data, and notified if returned from local cache.
 * @param cancellationBlock Optionally, be notified of a cancellation on upload.
 * @param errorBlock Optionally, be notified of an error for a request.
 *
 * @return The token for the request that can be used to cancel the request or change its priority.
 */
- (id)getRenditionForLayer:(NSNumber *)layerID
             withLayerComp:(NSNumber *)layerCompID
                  withType:(AdobeAssetFileRenditionType)type
                  withSize:(CGSize)dimensions
              withPriority:(NSOperationQueuePriority)priority
                onProgress:(void (^)(double))progressBlock
              onCompletion:(void (^)(NSData *, BOOL))completionBlock
            onCancellation:(void (^)(void))cancellationBlock
                   onError:(void (^)(NSError *))errorBlock;

/**
 * Cancel the layer rendition request.
 *
 * @param requestToken The token of the request to cancel.
 */
- (void)cancelLayerRenditionRequest:(id)requestToken;

/**
 * Change the priority of the layer rendition request.
 *
 * @param requestToken Token ID of the request to cancel.
 * @param priority The priority of the request.
 */
- (void)changeLayerRenditionRequestPriority:(id)requestToken
                            withNewPriority:(NSOperationQueuePriority)priority;

@end
