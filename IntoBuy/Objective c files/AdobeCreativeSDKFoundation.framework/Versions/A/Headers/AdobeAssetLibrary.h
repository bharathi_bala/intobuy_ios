/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

#import "AdobeAssetPackage.h"

#import "AdobeAssetLibraryItemColor.h"
#import "AdobeAssetLibraryItemColorTheme.h"
#import "AdobeAssetLibraryItemImage.h"

/**
 * AdobeAssetPackageLibrary represents a Creative Cloud Library and provides
 * access to data about the package (manifest, renditions, etc).
 */

/**
 * A utility to help determine if an AdobeAsset is an AdobeAssetLibrary.
 */
#define IsAdobeAssetLibrary(item) ([item isKindOfClass:[AdobeAssetLibrary class]])

/**
 * AdobeAssetLibrary represents a Creative Cloud Library package and provides access to its contents.
 */
@interface AdobeAssetLibrary : AdobeAssetPackage

/**
 * The Colors in the package. All dictionary objects are referenced by object ID.
 */
@property (nonatomic, readonly, strong) NSDictionary *colors;

/**
 * The Color Themes in the package. All dictionary objects are referenced by object ID.
 */
@property (nonatomic, readonly, strong) NSDictionary *colorThemes;

/**
 * The images in the package. All dictionary objects are referenced by object ID.
 */
@property (nonatomic, readonly, strong) NSDictionary *images;

/**
 * A utility to test the equlity of two AdobeAssetLibrary instances.
 *
 * @param library the AdobeAssetLibrary to test against.
 */
- (BOOL)isEqualToLibrary:(AdobeAssetLibrary *)library;

@end