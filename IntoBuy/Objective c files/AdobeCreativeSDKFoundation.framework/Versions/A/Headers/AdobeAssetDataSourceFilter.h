/******************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * ___________________
 *
 * Copyright 2014 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains the property of
 * Adobe Systems Incorporated and its suppliers, if any. The intellectual and
 * technical concepts contained herein are proprietary to Adobe Systems
 * Incorporated and its suppliers and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this
 * material is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

#import <Foundation/Foundation.h>

/**
 * Represents the file assets in the Creative Cloud. The default view.
 */
extern NSString *const AdobeAssetDataSourceFiles;

/**
 * Represents all user brushes in the Creative Cloud.
 *
 * @note Not yet implemented.
 */
extern NSString *const AdobeAssetDataSourceBrushes;

/**
 * Represents Compositions in the Creative Cloud.
 *
 * @note Not yet implemented.
 */
extern NSString *const AdobeAssetDataSourceCompositions;

/**
 * Represents Lightroom Photos in the Creative Cloud.
 *
 * @note Not yet implemented.
 */
extern NSString *const AdobeAssetDataSourcePhotos;

/**
 * Represents Shape objects in the Creative Cloud.
 *
 * @note Not yet implemented.
 */
extern NSString *const AdobeAssetDataSourceShapes;

/**
 * Represents Photoshop Sketches assets in the Creative Cloud.
 */
extern NSString *const AdobeAssetDataSourceSketches;

/**
 * Represents Illustrator Line assets in the Creative Cloud.
 */
extern NSString *const AdobeAssetDataSourceLine;

/**
 * Represents Illustrator Draw assets in the Creative Cloud.
 */
extern NSString *const AdobeAssetDataSourceDraw;

/**
 * Represents iOS device Camera Roll objects.
 *
 * @note Not yet implemented.
 */
extern NSString *const AdobeAssetDataSourceCameraRoll;

/**
 * Represents Design Library assets in the Creative Cloud.
 */
extern NSString *const AdobeAssetDataSourceLibrary;

/**
 * Whether the specified data sources are the only ones supported (inclusion) or should be excluded
 * (exclusion).
 */
typedef NS_ENUM (NSInteger, AdobeAssetDataSourceFilterType)
{
    /**
     * The specified data sources should be the only included types.
     */
    AdobeAssetDataSourceFilterInclusive = 1,

    /**
     * The specified data sources should be excluded.
     */
    AdobeAssetDataSourceFilterExclusive
};

/**
 * Class that represents filters that will be applied to the datasources available in the Asset Browser. (See AdobeUXAssetBrowser.)
 */
@interface AdobeAssetDataSourceFilter : NSObject

/**
 * Datasources to consider. An NSArray of AdobeAssetDataSource constants.
 */
@property (nonatomic, readonly, strong) NSArray *dataSources;

/**
 * The type of the filter; inclustionary or exclusionary.
 */
@property (nonatomic, readonly, assign) AdobeAssetDataSourceFilterType filterType;

/**
 * Whether the datasources are inclusionary or exclusionary as specified by the @c filterType
 * attribute when creating an instance of the receiver.
 */
@property (nonatomic, readonly, getter = isInclusive) BOOL inclusive;

/**
 * Returns a collection of all supported data sources in the order that they should be displayed.
 *
 * @return An NSArray containing one or all of the AdobeAssetDataSource types in the order they
 * should be displayed. This is usually an en-US alphabetically sorted list of the datasource names.
 */
+ (NSArray *)allSupportedDataSources;

- (instancetype)initWithDataSources:(NSArray *)dataSource filterType:(AdobeAssetDataSourceFilterType)filterType;

@end
