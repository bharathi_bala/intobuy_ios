//
//  AVYSDKDemoViewController.h
//  AviaryDemo-iOS
//
//  Created by Michael Vitrano on 1/23/13.
//  Copyright (c) 2013 Aviary. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ShareToViewController.h"


#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "YCameraViewController.h"
#import <AdobeCreativeSDKFoundation/AdobeCreativeSDKFoundation.h>
#import <AdobeCreativeSDKImage/AdobeCreativeSDKImage.h>
#import "IntoBuy-Swift.h"
//@class IBUploadImageViewController;


@interface AVYSDKDemoViewController : UIViewController<AdobeUXImageEditorViewControllerDelegate>
{
    NSString* screenType;
    BOOL isFromEditorCancel;
    

}
@property (strong, nonatomic) IBOutlet UIButton *editSampleButton;
@property (strong, nonatomic) IBOutlet UIButton *choosePhotoButton;
@property (strong, nonatomic) IBOutlet UIImageView *logoImageView;
@property (strong, nonatomic) UIImage *EditImage;
- (IBAction)editSample:(id)sender;
- (IBAction)choosePhoto:(id)sender;
    - (BOOL)isVisible;

@end
