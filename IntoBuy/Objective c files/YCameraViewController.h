//
//  PhotoShapViewController.h
//  NoshedItStaging
//
//  Created by yuvraj on 08/01/14.
//  Copyright (c) 2014 limbasiya.nirav@gmail.com. All rights reserved.
//

//
//  ARC Helper
#ifndef ah_retain
#if __has_feature(objc_arc)
#define ah_retain self
#define ah_dealloc self
#define release self
#define autorelease self
#else
#define ah_retain retain
#define ah_dealloc dealloc
#define __bridge
#endif
#endif

//  ARC Helper ends

#import "IntoBuy-Bridging-Header.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMotion/CoreMotion.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <QuartzCore/QuartzCore.h>
#import <AdobeCreativeSDKFoundation/AdobeCreativeSDKFoundation.h>
#import <AdobeCreativeSDKImage/AdobeCreativeSDKImage.h>
#import "AVYSDKDemoViewController.h"
//#import "IntoBuyApp-Swift.h"
//#import <AviarySDK/AviarySDK.h>
@protocol YCameraViewControllerDelegate;

@interface YCameraViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate,   UIPopoverControllerDelegate,AdobeUXImageEditorViewControllerDelegate>{
    
    UIImagePickerController *imgPicker;
    BOOL pickerDidShow;
    
    BOOL isPresenting;
    
    //Today Implementation
    BOOL FrontCamera;
    BOOL haveImage;
    BOOL initializeCamera, photoFromCam;
    AVCaptureSession *session;
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
    AVCaptureStillImageOutput *stillImageOutput;
    UIImage *croppedImageWithoutOrientation;
    UIView *cameraView;
    UIImageView *bottomView;
    UIImageView *lineImgView;
}
@property (nonatomic, readwrite) BOOL dontAllowResetRestaurant;
@property (nonatomic, assign) id delegate;

#pragma mark -
@property (nonatomic, strong) IBOutlet UIButton *photoCaptureButton;
@property (nonatomic, strong) IBOutlet UIButton *flashBtn;
@property (nonatomic, strong) IBOutlet UIButton *cameraToggleButton;
@property (nonatomic, strong) IBOutlet UIButton *libraryToggleButton;
@property (nonatomic, strong) IBOutlet UIButton *flashToggleButton;
@property (retain, nonatomic) IBOutlet UIImageView *ImgViewGrid;
@property (nonatomic, strong) IBOutlet UIView *photoBar;
@property (nonatomic, strong) IBOutlet UIView *topBar;
@property (retain, nonatomic) IBOutlet UIView *imagePreview;
@property (retain, nonatomic) IBOutlet UIImageView *captureImage;
@property (nonatomic, strong) ALAssetsLibrary *assetLibrary;
@end

@protocol YCameraViewControllerDelegate
- (void)didFinishPickingImage:(UIImage *)image;
- (void)yCameraControllerDidCancel;
- (void)yCameraControllerdidSkipped;


@property (strong, nonatomic) IBOutlet UIImageView *imagePreviewView;
@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, assign) BOOL shouldReleasePopover;


@end
