//
//  AVYSDKDemoViewController.m
//  AviaryDemo-iOS
//
//  Created by Michael Vitrano on 1/23/13.
//  Copyright (c) 2013 Aviary. All rights reserved.
//

#import "AVYSDKDemoViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <QuartzCore/QuartzCore.h>
//#import <AviarySDK/AviarySDK.h>
 //#import <IntoBuy/IntoBuy-Swift.h>
#import "IntoBuy-Bridging-Header.h"
//#import "IBUploadImageViewController.h/IBUploadImageViewController.h-Swift.h"
#import "IntoBuyAvY-Swift.h"
//#import <IntoBuy/IntoBuy-Swift.h>


static NSString * const kAVYAviaryCilentIDKey = @"7af03b5ab76e4ae7bc2bf7c1e07677f7";
static NSString * const kAVYAviarySecret = @"db5690d4-ffaa-4a8f-a6a0-89ddad3264d2";

@protocol UploadImageProtoCol;

@interface AVYSDKDemoViewController () < UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>
{
    IBUploadImageViewController *uploadImagevc;
    IBProductsSellViewController *productSellVc;
    
    
}
@property (strong, nonatomic) IBOutlet UIImageView *imagePreviewView;
@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, assign) BOOL shouldReleasePopover;
@property (nonatomic, strong) ALAssetsLibrary *assetLibrary;
- (IBAction)libraryClick:(id)sender;

@end

@implementation AVYSDKDemoViewController
@synthesize EditImage;

- (BOOL)isVisible
{
    return [self isViewLoaded] && self.view.window;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.tabBarItem.image = [UIImage imageNamed:@"camera.png"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"camera2.png"];
    }
    return self;
}


#pragma mark - View Controller Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"avy viewdid load called");
    
    // Allocate Asset Library
    ALAssetsLibrary * assetLibrary = [[ALAssetsLibrary alloc] init];
    [self setAssetLibrary:assetLibrary];
    
    // Start the Aviary Editor OpenGL Load
    // [AVYOpenGLManager beginOpenGLLoad];
    [AdobeImageEditorOpenGLManager beginOpenGLLoad];
    [AdobeImageEditorCustomization purgeGPUMemoryWhenPossible:YES];
    
  }
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"avy view will appear called");
    
    
    if(isFromEditorCancel)
    {
        [self.navigationController popViewControllerAnimated:YES];
        isFromEditorCancel = NO;
        
    }
    else{
        [[AdobeUXAuthManager sharedManager] setAuthenticationParametersWithClientID:kAVYAviaryCilentIDKey clientSecret:kAVYAviarySecret enableSignUp:NO];
        
        UIViewController *vc = [self presentedViewController];
        
        
        
        AppDelegate * appDelegate =  [[UIApplication sharedApplication]delegate];
        NSLog(@"this is VC2");
        NSLog(@"not visible Visible");
        AdobeUXImageEditorViewController *editorController = [[AdobeUXImageEditorViewController alloc] initWithImage:EditImage];
        [editorController setDelegate:self];
        [self.navigationController pushViewController:editorController animated:NO];

    }
    
//        [self.navigationController presentViewController:editorController animated:NO completion:nil];
//    [appDelegate.navController presentViewController:editorController animated:NO completion:nil];
    //}
//    else
//    {
//        NSLog(@" Visible");
//        [self dismissViewControllerAnimated:YES completion:nil];
//    
//    
//    }

   
/*
  
    YCameraViewController *camController;
  
    
    CGRect screenBounds1 = [[UIScreen mainScreen] bounds];
    if (screenBounds1.size.height >= 667)
    {
        camController = [[YCameraViewController alloc] initWithNibName:@"YCameraViewController6" bundle:nil];
    }
    if (screenBounds1.size.height < 667)
    {
         camController = [[YCameraViewController alloc] initWithNibName:@"YCameraViewController" bundle:nil];
    }

    
    camController.delegate=self;
//    [self.presentedViewController presentViewController:camController animated:YES completion:^{
//        
//    }];
    [self presentViewController:camController animated:NO completion:^{
        // completion code
        
    }];

    */
    
//    
//   AVCaptureSession *session = [[AVCaptureSession alloc] init];
//    AVCaptureDevice *videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
//    if (videoDevice)
//    {
//        NSError *error;
//        AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
//        if (!error)
//        {
//            if ([session canAddInput:videoInput])
//            {
//                [session addInput:videoInput];
//                AVCaptureVideoPreviewLayer *previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
//                previewLayer.frame = CGRectMake(14, 40,400, 450);
//                [self.view.layer addSublayer:previewLayer];
//                [session startRunning];
//            }
//        }
//    }

    //[self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - Photo Editor Launch Methods
/*
- (void)launchEditorWithAsset:(ALAsset *)asset
{
    UIImage *editingResImage = [self editingResImageForAsset:asset];
    UIImage *highResImage = [self highResImageForAsset:asset];
    
    [self launchPhotoEditorWithImage:editingResImage
                 highResolutionImage:highResImage];
}

- (void)launchEditorWithSampleImage
{
    UIImage *sampleImage = [UIImage imageNamed:@"Demo.png"];
    
    [self launchPhotoEditorWithImage:sampleImage
                 highResolutionImage:nil];
}
*/
#pragma mark - Photo Editor Creation and Presentation
/*
- (void)launchPhotoEditorWithImage:(UIImage *)editingResImage
               highResolutionImage:(UIImage *)highResImage
{    
    // Customize the editor's apperance. The customization options really
    // only need to be set once in this case since they are never changing,
    // so we used dispatch once here.
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self setPhotoEditorCustomizationOptions];
    });
    
    // Initialize the photo editor and set its delegate

    
    AVYPhotoEditorController * photoEditor = [[AVYPhotoEditorController alloc] initWithImage:editingResImage];
   
    [photoEditor setDelegate:self];
    
    // If a high res image is passed, create the high res context with
    // the image and the photo editor.
    if (highResImage) {
        [self setupHighResRenderForPhotoEditor:photoEditor
                                      withImage:highResImage];
    }
    
    // Present the photo editor.
    [self presentViewController:photoEditor
                       animated:YES
                     completion:nil];
}

- (void)setupHighResRenderForPhotoEditor:(AVYPhotoEditorController *)photoEditor
                                withImage:(UIImage *)highResImage
{
    // Enqueue the render with the high resolution image. The render will asynchonously apply all changes made in the editor on
    // the provided image. It will not complete until some point after the editor closes. When rendering completes, the completion block
    // will be called on the main thread with the resulting image. If the user cancels or no edits were made, then the result will be `nil`
    // and the error parameter will provide a description of the error that occured.
    
    id<AVYPhotoEditorRender> render = [photoEditor enqueueHighResolutionRenderWithImage:highResImage
                                                                            completion:^(UIImage *result, NSError *error) {
                                                                                if (result) {
                                                                                    UIImageWriteToSavedPhotosAlbum(result, nil, nil, NULL);
                                                                                } else {
                                                                                    NSLog(@"High-res render failed with error : %@", error);
                                                                                }
                                                                            }];
    
    
    // Provide a block to receive updates about the status of the render. This block will be called potentially multiple times, always
    // from the main thread.
    
    [render setProgressHandler:^(CGFloat progress) {
        NSLog(@"Render now %lf percent complete", round(progress * 100.0f));
    }];
}

#pragma Photo Editor Delegate Methods

// This is called when the user taps "Done" in the photo editor.
//- (void)photoEditor:(AVYPhotoEditorController *)editor
//  finishedWithImage:(UIImage *)image
//{
//    [[self imagePreviewView] setImage:image];
//    [[self imagePreviewView] setContentMode:UIViewContentModeScaleAspectFit];
//    
//    ShareToViewController* shareView=[[ShareToViewController alloc]initWithNibName:@"ShareToViewController_iPhone5" bundle:nil Selectedimage:image];
//    
//    
//    [self.navigationController pushViewController:shareView animated:YES];
//    
//    [self dismissViewControllerAnimated:YES
//                             completion:nil];
//    
//}

#pragma mark  This is called when the user taps "Cancel" in the photo editor.
//- (void) photoEditorCanceled:(AVYPhotoEditorController *)editor
//{
//    [self dismissViewControllerAnimated:YES
//                             completion:nil];
//}
*/
#pragma mark - Photo Editor Customization
/*
- (void) setPhotoEditorCustomizationOptions
{
    // Set API Key and Secret
    [AVYPhotoEditorController setAPIKey:kAVYAviaryAPIKey secret:kAVYAviarySecret];
    
    // Set Tool Order
    NSArray *toolOrder = @[kAVYEffects,
                            kAVYFocus,
                            kAVYFrames,
                            kAVYStickers,
                            kAVYEnhance,
                            kAVYOrientation,
                            kAVYCrop, 
                            kAVYColorAdjust, 
                            kAVYLightingAdjust, 
                            kAVYSplash, 
                            kAVYDraw, 
                            kAVYText, 
                            kAVYRedeye, 
                            kAVYWhiten, 
                            kAVYBlemish, 
                            kAVYMeme];
    // Set Tool Order
    NSArray *toolOrder = @[kAVYEffects,
                           kAVYFocus,
                           kAVYEnhance,
                           kAVYOrientation,
                           kAVYCrop,
                           kAVYColorAdjust,
                           kAVYLightingAdjust,
                           kAVYSplash,
                           kAVYDraw,
                           kAVYText,
                           kAVYRedeye,
                           kAVYWhiten,
                           kAVYBlemish,
                           kAVYMeme];

    [AVYPhotoEditorCustomization setToolOrder:toolOrder];
    
    // Set Custom Crop Sizes
    [AVYPhotoEditorCustomization setCropToolOriginalEnabled:NO];
    [AVYPhotoEditorCustomization setCropToolCustomEnabled:YES];
    NSDictionary *fourBySix = @{kAVYCropPresetHeight : @4.0f,
                                kAVYCropPresetWidth : @6.0f};
    NSDictionary *fiveBySeven = @{kAVYCropPresetHeight : @5.0f,
                                  kAVYCropPresetWidth : @7.0f};
    NSDictionary *square = @{kAVYCropPresetName: @"Square",
                             kAVYCropPresetHeight : @1.0f,
                             kAVYCropPresetWidth : @1.0f};
    
    [AVYPhotoEditorCustomization setCropToolPresets:@[fourBySix,
                                                     fiveBySeven,
                                                     square]];
    
    // Set Supported Orientations
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        NSArray *supportedOrientations = @[@(UIInterfaceOrientationPortrait),
                                           @(UIInterfaceOrientationPortraitUpsideDown),
                                           @(UIInterfaceOrientationLandscapeLeft),
                                           @(UIInterfaceOrientationLandscapeRight)];
        [AVYPhotoEditorCustomization setSupportedIpadOrientations:supportedOrientations];
    }
}
*/
#pragma mark - Creative Adobe SDK Delegate Methods

- (void)displayEditorForImage:(UIImage *)imageToEdit
{
    
    
    //  static dispatch_once_t onceToken;
    //  dispatch_once(&onceToken, ^{
    
    //[AdobeUXImageEditorViewController setAPIKey:@"288cd6ad65e5475798b6b610503a49bb" secret:@"4509756b-edf1-4312-afa5-0569f017216e"];
    
    
    
    // });
    
    [AdobeImageEditorOpenGLManager requestOpenGLDataPurge];
    
    
    
//    AdobeUXImageEditorViewController *editorController = [[AdobeUXImageEditorViewController alloc] initWithImage:imageToEdit];
//    [editorController setDelegate:self];
//    [self presentViewController:editorController animated:YES completion:^{}];
}

- (void)photoEditor:(AdobeUXImageEditorViewController *)editor finishedWithImage:(UIImage *)image
{
    // Handle the result image here and dismiss the editor.
    // [self doSomethingWithImage:image]; // Developer-defined method that presents the final editing-resolution image to the user, perhaps.
      NSLog(@"Uploadimage called");
   
    AppDelegate * appDelegate =  [[UIApplication sharedApplication]delegate];

  //  [self  dismissViewControllerAnimated:NO completion:^{
    if ([IBSingletonClass sharedInstance].isFromUpload) {
        IBUploadImageViewController *uploadImagevc1= [[IBUploadImageViewController alloc]init];
        
        uploadImagevc1.m_uploadImage = image;
        
        int index =[IBSingletonClass sharedInstance].m_UploadImageIndex;
        
        
        
        NSLog(@"index value is %d",index);
        
        if(index == 300 || index == 0)
        {
            [IBSingletonClass sharedInstance].m_FirstImage = image;
            IBSingletonClass.sharedInstance.m_isFromFirstImage = YES;
            
        }
        if(index == 301)
        {
            [IBSingletonClass sharedInstance].m_secondImage = image;
            IBSingletonClass.sharedInstance.m_isFromSecondImage = YES;
            
        }
        if(index == 302)
        {
            [IBSingletonClass sharedInstance].m_ThirdImage = image;
            IBSingletonClass.sharedInstance.m_isFromThirdImage = YES;
            
        }
        if(index == 303)
        {
            [IBSingletonClass sharedInstance].m_FourthImage = image;
            IBSingletonClass.sharedInstance.m_isFromFourthImage = YES;
            
        }
        [self.navigationController pushViewController:uploadImagevc1 animated:NO];

    }
    else if ([IBSingletonClass sharedInstance].isFromSell) {
        
        int index =[IBSingletonClass sharedInstance].m_UploadImageIndex;
        
        
        
        NSLog(@"index value is %d",index);
        
        if(index == 100 || index == 0)
        {
            if ([IBSingletonClass sharedInstance].toEditProduct)
            {
                [IBSingletonClass sharedInstance].m_productFirstImage = image;
                IBSingletonClass.sharedInstance.m_isFromProductFirstImage = YES;
                
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if ([view isKindOfClass:[IBProductsSellViewController class]])
                    {
                        IBProductsSellViewController *sellVc = (IBProductsSellViewController *)view;
                        sellVc.firstImageView.image = image;
                        sellVc.productEntity.productimage = UIImagePNGRepresentation(image);
                        [sellVc createCloseButtonForImage:(sellVc.firstImageView)];
                    }
                }
                [self dismissViewControllerAnimated:true completion:nil];
            }
            else
            {
                [IBSingletonClass sharedInstance].m_productFirstImage = image;
                IBSingletonClass.sharedInstance.m_isFromProductFirstImage = YES;
                AppDelegate * appDelegate =  [[UIApplication sharedApplication]delegate];
                
                if ([IBSingletonClass sharedInstance].isFromImageTap)
                {
                    for (UIViewController *view in self.navigationController.viewControllers) {
                        if ([view isKindOfClass:[IBProductsSellViewController class]])
                        {
                            IBProductsSellViewController *sellVc = (IBProductsSellViewController *)view;
                            sellVc.firstImageView.image = image;
                            sellVc.productEntity.productimage = UIImagePNGRepresentation(image);
                            [sellVc createCloseButtonForImage:(sellVc.secondImageView)];
                            [self.navigationController popToViewController:sellVc animated:false];
                            return;
                        }
                    }
                }
                else
                {
                    IBProductsSellViewController *uploadImagevc1= [[IBProductsSellViewController alloc]init];
                    uploadImagevc1.isFromProfile = NO;
                    uploadImagevc1.m_uploadImage = image;
                    [self.navigationController pushViewController:uploadImagevc1 animated:NO];
                }
            }
        }
        if(index == 101)
        {
            [IBSingletonClass sharedInstance].m_productsecondImage = image;
            IBSingletonClass.sharedInstance.m_isFromProductSecondImage = YES;
            //            for var vc:UIViewController in (self.navigationController?.viewControllers)!
            //            {
            //                if vc .isKindOfClass(IBHomeViewController)
            //                {
            //                    let homePage : IBHomeViewController = vc as! IBHomeViewController
            //                    homePage.m_categoryStr = categoryStr//(m_categoryArray.objectAtIndex(m_selectionIndex) as? String)!
            //                    homePage.isFromCategory = true
            //                    homePage.isFromFilter = false
            //                    homePage.isFromLocation = false
            //                }
            //            }
            if ([IBSingletonClass sharedInstance].toEditProduct){
                for (UIViewController *view in appDelegate.navController.viewControllers) {
                    if ([view isKindOfClass:[IBProductsSellViewController class]])
                    {
                        IBProductsSellViewController *sellVc = (IBProductsSellViewController *)view;
                        sellVc.secondImageView.image = image;
                        sellVc.productEntity.productimage2 = UIImagePNGRepresentation(image);
                        [sellVc createCloseButtonForImage:(sellVc.secondImageView)];
                    }
                }
                [self dismissViewControllerAnimated:true completion:nil];
            }
            else{
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if ([view isKindOfClass:[IBProductsSellViewController class]])
                    {
                        IBProductsSellViewController *sellVc = (IBProductsSellViewController *)view;
                        sellVc.secondImageView.image = image;
                        sellVc.productEntity.productimage2 = UIImagePNGRepresentation(image);
                        [sellVc createCloseButtonForImage:(sellVc.secondImageView)];
                        [self.navigationController popToViewController:sellVc animated:false];
                        return;
                    }
                }
                
            }
            
        }
        if(index == 102)
        {
            [IBSingletonClass sharedInstance].m_productThirdImage = image;
            IBSingletonClass.sharedInstance.m_isFromProductThirdImage = YES;
            if ([IBSingletonClass sharedInstance].toEditProduct){
                
                for (UIViewController *view in appDelegate.navController.viewControllers) {
                    if ([view isKindOfClass:[IBProductsSellViewController class]])
                    {
                        IBProductsSellViewController *sellVc = (IBProductsSellViewController *)view;
                        sellVc.thirdImageView.image = image;
                        sellVc.productEntity.productimage3 = UIImagePNGRepresentation(image);
                        [sellVc createCloseButtonForImage:(sellVc.thirdImageView)];
                    }
                }
                [self dismissViewControllerAnimated:true completion:nil];
                
            }
            else{
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if ([view isKindOfClass:[IBProductsSellViewController class]])
                    {
                        IBProductsSellViewController *sellVc = (IBProductsSellViewController *)view;
                        sellVc.thirdImageView.image = image;
                        sellVc.productEntity.productimage3 = UIImagePNGRepresentation(image);
                        [sellVc createCloseButtonForImage:(sellVc.thirdImageView)];
                        [self.navigationController popToViewController:sellVc animated:false];
                        return;
                    }
                }
                
            }
            
        }
        if (index == 103)
        {
            //            m_productFourthImage
            [IBSingletonClass sharedInstance].m_productFourthImage = image;
            IBSingletonClass.sharedInstance.m_isFromProductFourthImage = YES;
            
            if ([IBSingletonClass sharedInstance].toEditProduct){
                for (UIViewController *view in appDelegate.navController.viewControllers) {
                    if ([view isKindOfClass:[IBProductsSellViewController class]])
                    {
                        IBProductsSellViewController *sellVc = (IBProductsSellViewController *)view;
                        sellVc.fourthImageView.image = image;
                        sellVc.productEntity.productimage4 = UIImagePNGRepresentation(image);
                        [sellVc createCloseButtonForImage:(sellVc.fourthImageView)];
                    }
                }
                [self dismissViewControllerAnimated:true completion:nil];
                
            }
            else{
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if ([view isKindOfClass:[IBProductsSellViewController class]])
                    {
                        IBProductsSellViewController *sellVc = (IBProductsSellViewController *)view;
                        sellVc.fourthImageView.image = image;
                        sellVc.productEntity.productimage4 = UIImagePNGRepresentation(image);
                        [sellVc createCloseButtonForImage:(sellVc.fourthImageView)];
                        [self.navigationController popToViewController:sellVc animated:false];
                        return;
                    }
                }
                
            }
            
        }
        
    }

    
    
    
}

- (void)photoEditorCanceled:(AdobeUXImageEditorViewController *)editor
{
    isFromEditorCancel  = YES;
    
    
     NSLog(@"Photo Editor  cancelled");
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isImagePickerPresent"];
    [self dismissPopoverWithCompletion:nil];

    
    [self.navigationController popViewControllerAnimated:NO];
    
//    [self dismissViewControllerAnimated:NO completion:^{
//        YCameraViewController *camController;
//        
//        
//        CGRect screenBounds1 = [[UIScreen mainScreen] bounds];
//        if (screenBounds1.size.height >= 667)
//        {
//            camController = [[YCameraViewController alloc] initWithNibName:@"YCameraViewController6" bundle:nil];
//        }
//        if (screenBounds1.size.height < 667)
//        {
//            camController = [[YCameraViewController alloc] initWithNibName:@"YCameraViewController" bundle:nil];
//        }
//        
//        
//        camController.delegate=self;
//        //    [self.presentedViewController presentViewController:camController animated:YES completion:^{
//        //
//        //    }];
////        [self presentViewController:camController animated:NO completion:^{
////            // completion code
////            
////        }];
//        AppDelegate * appDelegate =  [[UIApplication sharedApplication]delegate];
//        [self presentViewController:camController animated:NO completion:^{}];
//        
//    }];
    
   
     
    
    
    
    // Dismiss the editor.
    //[self.presentingViewController.presentingViewController dismissModalViewControllerAnimated:YES];
    
    
    
}


#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
   NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
 
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    [self displayEditorForImage:image];
   // NSArray *arrayPaths = NSSearchPathForDirectoriesInDomains(
                                                              ///NSDocumentDirectory,
                                                              //NSUserDomainMask,
                                                          //    YES);
//    NSString *docDir = [arrayPaths objectAtIndex:0];
//    NSString *filename = @"mypic.png";
//    NSString *fullpath = [docDir stringByAppendingPathComponent:filename];
    //[UIImagePNGRepresentation(image) writeToFile:fullpath atomically:YES];
    
    //NSString *filePath = [docDir stringByAppendingPathComponent:@"mypic.png"];
    
    //NSURL *assetURL=[NSURL URLWithString:filePath];
    //ALAsset *asset;
    
    
    
  //Start
    
  /*  ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
   // asset=[ALAsset ]
 void(^completion)(void)  = ^(void){
        
     
     
     if( [picker sourceType] == UIImagePickerControllerSourceTypeCamera )
     {
     [library writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error )
      {
          NSLog(@"IMAGE SAVED TO PHOTO ALBUM");
          [library assetForURL:assetURL resultBlock:^(ALAsset *asset )
           {
               NSLog(@"we have our ALAsset!");
               [self launchEditorWithAsset:asset];
           }
                  failureBlock:^(NSError *error )
           {
               NSLog(@"Error loading asset");
           }];
      }];
     
     }
     if( [picker sourceType] == UIImagePickerControllerSourceTypeSavedPhotosAlbum )
     {
          [[self assetLibrary] assetForURL:assetURL resultBlock:^(ALAsset *asset) {
          if (asset)
          {
         [self launchEditorWithAsset:asset];
          }
         } failureBlock:^(NSError *error) {
          [[[UIAlertView alloc] initWithTitle:@"Error"
                                                 message:@"Please enable access to your device's photos."
                                                delegate:nil
                                       cancelButtonTitle:@"OK"
                                       otherButtonTitles:nil] show];
                 }];

     }
   };
   */
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [self dismissViewControllerAnimated:NO
                                 completion:nil];
    }else{
        [self dismissPopoverWithCompletion:nil];
    }
   
   
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:NO
                             completion:nil];
}

#pragma mark - Popover Methods

- (void)presentViewControllerInPopover:(UIViewController *)controller
{
     NSLog(@"PopOver called");
    
    CGRect sourceRect = [[self choosePhotoButton] frame];
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:controller];
    [popover setDelegate:self];
    [self setPopover:popover];
    [self setShouldReleasePopover:YES];
    
    [popover presentPopoverFromRect:sourceRect
                             inView:[self view]
           permittedArrowDirections:UIPopoverArrowDirectionDown
                           animated:YES];
}

- (void)dismissPopoverWithCompletion:(void(^)(void))completion
{
     NSLog(@"PopOver dismiss called");
    
    [[self popover] dismissPopoverAnimated:YES];
    [self setPopover:nil];

    NSTimeInterval delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
       // completion();
    });
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if ([self shouldReleasePopover]){
        [self setPopover:nil];
    }
    [self setShouldReleasePopover:YES];
}

#pragma mark - ALAssets Helper Methods

- (UIImage *)editingResImageForAsset:(ALAsset*)asset
{
    CGImageRef image = [[asset defaultRepresentation] fullScreenImage];
    
    return [UIImage imageWithCGImage:image
                               scale:1.0
                         orientation:UIImageOrientationUp];
}

- (UIImage *)highResImageForAsset:(ALAsset*)asset
{
    ALAssetRepresentation *representation = [asset defaultRepresentation];
    
    CGImageRef image = [representation fullResolutionImage];
    UIImageOrientation orientation = [representation orientation];
    CGFloat scale = [representation scale];
    
    return [UIImage imageWithCGImage:image
                               scale:scale
                         orientation:orientation];
}

#pragma mark - Interface Actions

- (IBAction)editSample:(id)sender
{
    
    NSLog(@"sample edit called");
    
//    if ([self hasValidAPIKey]) {
//       // [self launchEditorWithSampleImage];
//    }
}

- (IBAction)choosePhoto:(id)sender
{
   // if ([self hasValidAPIKey])
    //{
    AppDelegate * appDelegate =  [[UIApplication sharedApplication]delegate];
      NSLog(@"Choose photo called");
    
        UIImagePickerController *imagePicker = [UIImagePickerController new];
        //[imagePicker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setDelegate:self];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
           // [self presentViewController:imagePicker animated:YES completion:nil];
            [appDelegate.navController presentViewController:imagePicker animated:YES completion:nil];
         }else
        {
            [self presentViewControllerInPopover:imagePicker];
        }
   // }
}

#pragma mark - Rotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationIsPortrait(interfaceOrientation);
    }else{
        return YES;
    }
}

- (BOOL)shouldAutorotate
{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                duration:(NSTimeInterval)duration
{
    [self setShouldReleasePopover:NO];
    [[self popover] dismissPopoverAnimated:YES];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if ([self popover]) {
        CGRect popoverRef = [[self choosePhotoButton] frame];
        [[self popover] presentPopoverFromRect:popoverRef
                                        inView:[self view]
                      permittedArrowDirections:UIPopoverArrowDirectionDown
                                      animated:YES];
    }
}

#pragma mark - Status Bar Style

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Private Helper Methods

/*- (BOOL)hasValidAPIKey
{
    if ([kAVYAviaryAPIKey isEqualToString:@"288cd6ad65e5475798b6b610503a49bb"] ||
        [kAVYAviarySecret isEqualToString:@"4509756b-edf1-4312-afa5-0569f017216e"]) {
        
        [[[UIAlertView alloc] initWithTitle:@"Oops!"
                                    message:@"You forgot to add your API key and secret!"
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        return NO;
    }
    return YES;
}*/

- (IBAction)libraryClick:(id)sender {
    AppDelegate * appDelegate =  [[UIApplication sharedApplication]delegate];
    UIImagePickerController *imagePicker = [UIImagePickerController new];
       [imagePicker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
       [imagePicker setDelegate:self];
    
     NSLog(@"Library click called");
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
       // [self presentViewController:imagePicker animated:YES completion:nil];
        [appDelegate.navController presentViewController:imagePicker animated:YES completion:nil];
    }else
    {
        [self presentViewControllerInPopover:imagePicker];
    }

}
@end
