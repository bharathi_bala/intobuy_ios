//
//  PhotoShapViewController.m
//  NoshedItStaging
//
//  Created by yuvraj on 08/01/14.
//  Copyright (c) 2014 limbasiya.nirav@gmail.com. All rights reserved.
//


//#import "IntoBuy-swift.h"
#import "YCameraViewController.h"
#import <ImageIO/ImageIO.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <QuartzCore/QuartzCore.h>
#import "IntoBuy-Swift.h"
#import "IntoBuyAvY-Swift.h"

//#import <AviarySDK/AviarySDK.h>
#define DegreesToRadians(x) ((x) * M_PI / 180.0)

static NSString * const kAVYAviaryCilentIDKey = @"7af03b5ab76e4ae7bc2bf7c1e07677f7";
static NSString * const kAVYAviarySecret = @"db5690d4-ffaa-4a8f-a6a0-89ddad3264d2";


@interface YCameraViewController (){
    UIInterfaceOrientation orientationLast, orientationAfterProcess;
    CMMotionManager *motionManager;
}
@property (strong, nonatomic) IBOutlet UIImageView *imagePreviewView;
@property (nonatomic, strong) UIPopoverController *popover;
@property (nonatomic, assign) BOOL shouldReleasePopover;

@end

@implementation YCameraViewController
@synthesize delegate;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]){
    //        self.edgesForExtendedLayout = UIRectEdgeNone;
    //    }
    
    self.navigationController.navigationBarHidden = YES;
    [self.navigationController setNavigationBarHidden:YES];
    
	// Do any additional setup after loading the view.
    pickerDidShow = NO;
    
    FrontCamera = NO;
    self.captureImage.hidden = YES;
    
    // Setup UIImagePicker Controller
    imgPicker = [[UIImagePickerController alloc] init];
    imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imgPicker.delegate = self;
    imgPicker.allowsEditing = NO;
    
    croppedImageWithoutOrientation = [[UIImage alloc] init];
    
    initializeCamera = YES;
    photoFromCam = YES;
    
    // Initialize Motion Manager
    [self initializeMotionManager];
    
    // Allocate Asset Library
    ALAssetsLibrary * assetLibrary = [[ALAssetsLibrary alloc] init];
    [self setAssetLibrary:assetLibrary];
    
    // Start the Aviary Editor OpenGL Load
  //  [AVYOpenGLManager beginOpenGLLoad];
    
    [AdobeImageEditorOpenGLManager beginOpenGLLoad];
    [AdobeImageEditorCustomization purgeGPUMemoryWhenPossible:YES];
    
      [[AdobeUXAuthManager sharedManager] setAuthenticationParametersWithClientID:kAVYAviaryCilentIDKey clientSecret:kAVYAviarySecret enableSignUp:NO];
    [self createSeperateViewForCamera];
    
}

-(void)createSeperateViewForCamera
{
    
    UIImage *bottomBgImg = [UIImage imageNamed:@"Intobuy-camera-bottom-bar.png"];
    CGFloat bottomH = bottomBgImg.size.height/2;
    if (self.view.frame.size.height == 667) {
        bottomH = bottomBgImg.size.height/1.8;
    }
    else if (self.view.frame.size.height > 667)
    {
        bottomH = bottomBgImg.size.height/1.5;
    }

    
    CGFloat xPos = self.view.frame.origin.x;
    CGFloat yPos = self.view.frame.origin.y;
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height - bottomH;
    
    
    
    
    cameraView = [[UIView alloc]init];
    cameraView.frame = CGRectMake(xPos,yPos,width,height);
    cameraView.userInteractionEnabled = YES;
    cameraView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:cameraView];
    
    //imgPicker.cameraOverlayView = cameraView;
    

    self.captureImage = [[UIImageView alloc]init];
    self.captureImage.frame = CGRectMake(xPos,yPos,width,height);
    self.captureImage.userInteractionEnabled = YES;
    [self.view addSubview:self.captureImage];
    
    self.imagePreview = [[UIImageView alloc]init];
    self.imagePreview.frame = CGRectMake(xPos,yPos,width,height);
    self.imagePreview.userInteractionEnabled = YES;
    [self.view addSubview:self.imagePreview];

    
    
    self.ImgViewGrid = [[UIImageView alloc]init];
    self.ImgViewGrid.frame = CGRectMake(xPos,yPos,width,height);
    self.ImgViewGrid.image = [UIImage imageNamed:@"camera-grid iphone5.png"];
    self.ImgViewGrid.userInteractionEnabled = YES;
    [self.view addSubview:self.ImgViewGrid];
    
    
    
    
    height = 60;
    self.topBar = [[UIView alloc]init];
    self.topBar.frame = CGRectMake(xPos,yPos,width,height);
    self.topBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.topBar];
    
    UIImage *backImage = [UIImage imageNamed:@"intobuy-mydiary-back-button.png"];
    width = backImage.size.width/2;
    height = backImage.size.height/2;
    if (self.view.frame.size.height == 667) {
        width = backImage.size.width/1.8;
        height = backImage.size.height/1.8;
    }
    else if (self.view.frame.size.height > 667)
    {
        width = backImage.size.width/1.5;
        height = backImage.size.height/1.5;
    }
    xPos = self.topBar.frame.origin.x + 15;
    yPos = self.topBar.frame.size.height - height;
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(xPos,yPos,width,height);
    [backBtn setBackgroundImage:backImage forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    [self.topBar addSubview:backBtn];
    
    UIButton *cancelBigBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBigBtn.frame = CGRectMake(xPos - 5,self.topBar.bounds.origin.y,width + 10,self.topBar.frame.size.height);
    [cancelBigBtn setBackgroundColor:[UIColor clearColor]];
    [cancelBigBtn addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    [self.topBar addSubview:cancelBigBtn];
    
    
    UIImage *selfieImg = [UIImage imageNamed:@"front-camera.png"];
    width = selfieImg.size.width;
    height = selfieImg.size.height;
//    if (self.view.frame.size.height == 667) {
//        width = selfieImg.size.width/1.8;
//        height = selfieImg.size.height/1.8;
//    }
//    else if (self.view.frame.size.height > 667)
//    {
//        width = selfieImg.size.width/1.5;
//        height = selfieImg.size.height/1.5;
//    }
    xPos = (self.topBar.frame.size.width - width)/2;
    yPos = self.topBar.frame.size.height - height;
    UIButton *selfieBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    selfieBtn.frame = CGRectMake(xPos,yPos,width,height);
    [selfieBtn setBackgroundImage:selfieImg forState:UIControlStateNormal];
    [selfieBtn addTarget:self action:@selector(switchCamera:) forControlEvents:UIControlEventTouchUpInside];
    [self.topBar addSubview:selfieBtn];

    
    
    UIImage *cancelImage = [UIImage imageNamed:@"intobuy-camera-screen-flash-icon-normal.png"];
    UIImage *flashSelImg = [UIImage imageNamed:@"intobuy-camera-screen-flash-icon-over.png"];
    width = cancelImage.size.width/3;
    height = cancelImage.size.height/3;
    if (self.view.frame.size.height == 667) {
        width = cancelImage.size.width/2;
        height = cancelImage.size.height/2;
    }
    else if (self.view.frame.size.height > 667)
    {
            width = cancelImage.size.width/1.8;
            height = cancelImage.size.height/1.8;
    }
    xPos = self.topBar.frame.size.width - width - 15;
    yPos = self.topBar.frame.size.height - height;
    self.flashBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.flashBtn.frame = CGRectMake(xPos,yPos,width,height);
    [self.flashBtn setBackgroundImage:cancelImage forState:UIControlStateNormal];
    [self.flashBtn setBackgroundImage:flashSelImg forState:UIControlStateSelected];
    [self.flashBtn addTarget:self action:@selector(toogleFlash:) forControlEvents:UIControlEventTouchUpInside];
    [self.topBar addSubview:self.flashBtn];
    
    
    
    
    
    width = self.view.frame.size.width;
    height = 100;
    xPos = self.view.frame.origin.x;
    yPos = cameraView.frame.size.height - 100;
    self.photoBar= [[UIView alloc]init];
    self.photoBar.frame = CGRectMake(xPos,yPos,width,height);
    self.photoBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.photoBar];
    
    //
    UIImage *libraryImg = [UIImage imageNamed:@"Intobuy-camera-photo-icon.png"];
    width = libraryImg.size.width/2;
    height = libraryImg.size.height/2;
    if (self.view.frame.size.height == 667) {
        width = libraryImg.size.width/1.8;
        height = libraryImg.size.height/1.8;
    }
    else if (self.view.frame.size.height > 667)
    {
        width = libraryImg.size.width/1.5;
        height = libraryImg.size.height/1.5;
    }
    xPos = self.photoBar.bounds.origin.x + 15;
    yPos = (self.photoBar.frame.size.height - height)/2;
    self.libraryToggleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.libraryToggleButton.frame = CGRectMake(xPos,yPos,width,height);
    [self.libraryToggleButton setBackgroundImage:libraryImg forState:UIControlStateNormal];
    [self.libraryToggleButton addTarget:self action:@selector(switchToLibrary:) forControlEvents:UIControlEventTouchUpInside];
    [self.photoBar addSubview:self.libraryToggleButton];
    
    UIImage *cameraImg = [UIImage imageNamed:@"intobuy-camera-button-new.png"];
    width = cameraImg.size.width/2;
    height = cameraImg.size.height/2;
    if (self.view.frame.size.height == 667) {
        width = cameraImg.size.width/1.8;
        height = cameraImg.size.height/1.8;
    }
    else if (self.view.frame.size.height > 667)
    {
        width = cameraImg.size.width/1.5;
        height = cameraImg.size.height/1.5;
    }
    xPos = (self.photoBar.frame.size.width - width)/2;
    yPos = (self.photoBar.frame.size.height - height)/2;
    self.cameraToggleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cameraToggleButton.frame = CGRectMake(xPos,yPos,width,height);
    [self.cameraToggleButton setBackgroundImage:cameraImg forState:UIControlStateNormal];
    [self.cameraToggleButton addTarget:self action:@selector(snapImage:) forControlEvents:UIControlEventTouchUpInside];
    [self.photoBar addSubview:self.cameraToggleButton];
    
    //
    UIImage *flashImg = [UIImage imageNamed:@"intobuy-camera-rotate-button.png"];
    width = flashImg.size.width/2;
    height = flashImg.size.height/2;
    if (self.view.frame.size.height == 667) {
        width = flashImg.size.width/1.8;
        height = flashImg.size.height/1.8;
    }
    else if (self.view.frame.size.height > 667)
    {
        width = flashImg.size.width/1.5;
        height = flashImg.size.height/1.5;
    }
    xPos = (self.photoBar.frame.size.width - width)-15;
    yPos = (self.photoBar.frame.size.height - height)/2;
    self.flashToggleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.flashToggleButton.frame = CGRectMake(xPos,yPos,width,height);
    [self.flashToggleButton setBackgroundImage:flashImg forState:UIControlStateNormal];
    [self.flashToggleButton addTarget:self action:@selector(toogleFlash:) forControlEvents:UIControlEventTouchUpInside];
    //[self.photoBar addSubview:self.flashToggleButton];

    
    yPos = self.photoBar.frame.origin.y + self.photoBar.frame.size.height;
    height = bottomH;
    xPos = 0;
    width = self.view.frame.size.width;
    bottomView = [[UIImageView alloc]init];
    bottomView.frame = CGRectMake(xPos,yPos,width,height);
    bottomView.image = bottomBgImg;
    [self.view addSubview:bottomView];
    if ([IBSingletonClass sharedInstance].screenIndex == 1) {
        bottomView.userInteractionEnabled = YES;
    }
    else{
        bottomView.userInteractionEnabled = NO;
    }
    
    
    
    NSArray *btnArray = [NSArray arrayWithObjects:@"Photo",@"Product", nil];
    xPos = 0;
    yPos = 0;
    width = width/2;
    for (int i = 0; i < btnArray.count; i++) {
        UIButton *bottomButton = [UIButton buttonWithType:UIButtonTypeCustom];
        bottomButton.frame = CGRectMake(xPos,yPos , width, height);
        [bottomButton setTitle:[btnArray objectAtIndex:i] forState:UIControlStateNormal];
        [bottomButton addTarget:self action:@selector(bottombuttonActions:) forControlEvents:UIControlEventTouchUpInside];
        
        bottomButton.tag = i;
        [bottomButton setTitleColor:[UIColor colorWithRed:221.0/255.0 green:0/255.0 blue:28/255.0 alpha:1.0] forState:UIControlStateNormal];
        [bottomView addSubview:bottomButton];
        xPos = xPos + width;
        if ([IBSingletonClass sharedInstance].isFromUpload) {
            if (i == 0)
            {
               [self addBottomBorder:bottomButton];
            }
        }
        else{
            if (i == 1)
            {
                [self addBottomBorder:bottomButton];
            }

        }

    }
    
}
-(void)bottombuttonActions:(UIButton *)sender
{
    for(UIView *view in [bottomView subviews]){
        if([view isKindOfClass:[UIButton class]]){
            UIButton *btn = (UIButton *)view;
                btn.selected = NO;
            [lineImgView removeFromSuperview];
            lineImgView = nil;
        }
    }
    sender.selected = YES;
    if(sender.tag == 0)
    {
        [IBSingletonClass sharedInstance].isFromUpload = YES;
        [IBSingletonClass sharedInstance].isFromSell = NO;
            IBSingletonClass.sharedInstance.m_UploadImageIndex = 300;

        NSLog(@"photo");
    }
    else{
        NSLog(@"Sell");
        [IBSingletonClass sharedInstance].isFromSell = YES;
        [IBSingletonClass sharedInstance].isFromUpload = NO;
       
            IBSingletonClass.sharedInstance.m_UploadImageIndex = 100;
    }
    
    [self addBottomBorder:sender];

}
- (void)addBottomBorder:(UIButton *)view {
    UIImage *lineImg = [UIImage imageNamed:@"Intobuy-camera-bottom-bar-selection-line.png"];
    CGFloat lineH;
    if (self.view.frame.size.height == 667) {
        lineH = lineImg.size.height/1.8;
    }
    else if (self.view.frame.size.height > 667)
    {
        lineH = lineImg.size.height/1.5;
    }
    else
    {
        lineH = lineImg.size.height/2;
    }

    lineImgView = [[UIImageView alloc]init];
    lineImgView.frame = CGRectMake(0, view.frame.size.height - lineH, self.view.frame.size.width/2, lineH);
    lineImgView.image = lineImg;
    [view addSubview:lineImgView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([IBSingletonClass sharedInstance].screenIndex == 1) {
        bottomView.userInteractionEnabled = YES;
    }
    else{
        bottomView.userInteractionEnabled = NO;
    }

  

    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (initializeCamera){
        initializeCamera = NO;
        
        // Initialize camera
        [self initializeCamera];
        self.captureImage.hidden = NO;
        self.imagePreview.hidden = NO;
        haveImage = NO;
    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [session stopRunning];
    //    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    //    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dealloc
{
    [_imagePreview release];
    [_captureImage release];
    [imgPicker release];
    imgPicker = nil;
    
    if (session)
        [session release], session=nil;
    
    if (captureVideoPreviewLayer)
        [captureVideoPreviewLayer release], captureVideoPreviewLayer=nil;
    
    if (stillImageOutput)
        [stillImageOutput release], stillImageOutput=nil;
}

#pragma mark - CoreMotion Task
- (void)initializeMotionManager{
    motionManager = [[CMMotionManager alloc] init];
    motionManager.accelerometerUpdateInterval = .2;
    motionManager.gyroUpdateInterval = .2;
    
    [motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                        withHandler:^(CMAccelerometerData  *accelerometerData, NSError *error) {
                                            if (!error) {
                                                [self outputAccelertionData:accelerometerData.acceleration];
                                            }
                                            else{
                                                NSLog(@"%@", error);
                                            }
                                        }];
}

#pragma mark - UIAccelerometer callback

- (void)outputAccelertionData:(CMAcceleration)acceleration{
    UIInterfaceOrientation orientationNew;
    
    if (acceleration.x >= 0.75) {
        orientationNew = UIInterfaceOrientationLandscapeLeft;
    }
    else if (acceleration.x <= -0.75) {
        orientationNew = UIInterfaceOrientationLandscapeRight;
    }
    else if (acceleration.y <= -0.75) {
        orientationNew = UIInterfaceOrientationPortrait;
    }
    else if (acceleration.y >= 0.75) {
        orientationNew = UIInterfaceOrientationPortraitUpsideDown;
    }
    else {
        // Consider same as last time
        return;
    }
    
    if (orientationNew == orientationLast)
        return;
    
    //    NSLog(@"Going from %@ to %@!", [[self class] orientationToText:orientationLast], [[self class] orientationToText:orientationNew]);
    
    orientationLast = orientationNew;
}

#ifdef DEBUG
+(NSString*)orientationToText:(const UIInterfaceOrientation)ORIENTATION {
    switch (ORIENTATION) {
        case UIInterfaceOrientationPortrait:
            return @"UIInterfaceOrientationPortrait";
        case UIInterfaceOrientationPortraitUpsideDown:
            return @"UIInterfaceOrientationPortraitUpsideDown";
        case UIInterfaceOrientationLandscapeLeft:
            return @"UIInterfaceOrientationLandscapeLeft";
        case UIInterfaceOrientationLandscapeRight:
            return @"UIInterfaceOrientationLandscapeRight";
    }
    return @"Unknown orientation!";
}
#endif

#pragma mark - Camera Initialization

//AVCaptureSession to show live video feed in view
- (void) initializeCamera {
    [self goToCamera];
}

#pragma mark - Camera permission handling

- (IBAction)goToCamera
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self popCamera];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access to %@", AVMediaTypeVideo);
                 [self popCamera];
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [self camDenied];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        // My own Helper class is used here to pop a dialog in one simple line.
       // [Helper popAlertMessageWithTitle:@"Error" alertText:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access."];
    }
    else
    {
        [self camDenied];
    }
}

- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    
    NSString *alertText;
    NSString *alertButton;
    
    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings)
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera. Do you wish to do change in settings.";
        
        alertButton = @"Ok";
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:alertText
                              delegate:self
                              cancelButtonTitle:alertButton
                              otherButtonTitles:@"Cancel", nil];
        alert.tag = 3491832;
        [alert show];
    }
    else
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
        
        alertButton = @"OK";
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:alertText
                              delegate:self
                              cancelButtonTitle:alertButton
                              otherButtonTitles:nil];
        alert.tag = 3491832;
        [alert show];
    }
    
    
}

- (void) popCamera {
    
    if (session)
        [session release], session=nil;
    
    session = [[AVCaptureSession alloc] init];
    session.sessionPreset = AVCaptureSessionPresetPhoto;
    
    if (captureVideoPreviewLayer)
        [captureVideoPreviewLayer release], captureVideoPreviewLayer=nil;
    
    captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    [captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    captureVideoPreviewLayer.frame = self.imagePreview.bounds;
    [self.imagePreview.layer addSublayer:captureVideoPreviewLayer];
    
    UIView *view = [self imagePreview];
    CALayer *viewLayer = [view layer];
    [viewLayer setMasksToBounds:YES];
    
    CGRect bounds = [view bounds];
    [captureVideoPreviewLayer setFrame:bounds];
    
    NSArray *devices = [AVCaptureDevice devices];
    AVCaptureDevice *frontCamera=nil;
    AVCaptureDevice *backCamera=nil;
    
    // check if device available
    if (devices.count==0) {
        NSLog(@"No Camera Available");
        [self disableCameraDeviceControls];
        return;
    }
    
    for (AVCaptureDevice *device in devices) {
        
        NSLog(@"Device name: %@", [device localizedName]);
        
        if ([device hasMediaType:AVMediaTypeVideo]) {
            
            if ([device position] == AVCaptureDevicePositionBack) {
                NSLog(@"Device position : back");
                backCamera = device;
            }
            else {
                NSLog(@"Device position : front");
                frontCamera = device;
            }
        }
    }
    
    if (!FrontCamera) {
        
        if ([backCamera hasFlash]){
            [backCamera lockForConfiguration:nil];
            if (self.flashToggleButton.selected)
                [backCamera setFlashMode:AVCaptureFlashModeOn];
            else
                [backCamera setFlashMode:AVCaptureFlashModeOff];
            [backCamera unlockForConfiguration];
            
            [self.flashToggleButton setEnabled:YES];
        }
        else{
            if ([backCamera isFlashModeSupported:AVCaptureFlashModeOff]) {
                [backCamera lockForConfiguration:nil];
                [backCamera setFlashMode:AVCaptureFlashModeOff];
                [backCamera unlockForConfiguration];
            }
            [self.flashToggleButton setEnabled:NO];
            
        }
        
        NSError *error = nil;
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:backCamera error:&error];
        if (!input) {
            NSLog(@"ERROR: trying to open camera: %@", error);
        }
        [session addInput:input];
    }
    
    if (FrontCamera)
    {
        [self.flashToggleButton setEnabled:NO];
        NSError *error = nil;
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:frontCamera error:&error];
        if (!input)
        {
            NSLog(@"ERROR: trying to open camera: %@", error);
        }
        [session addInput:input];
    }
    
    if (stillImageOutput)
        [stillImageOutput release], stillImageOutput=nil;
    
    stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil] autorelease];
    [stillImageOutput setOutputSettings:outputSettings];
    
    [session addOutput:stillImageOutput];
    
    [session startRunning];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 3491832)
    {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
            if (canOpenSettings)
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
        else {
           
        }
    }
}

- (IBAction)snapImage:(id)sender {
    [self.photoCaptureButton setEnabled:NO];
    
    if (!haveImage)
    {
        self.captureImage.image = nil; //remove old image from view
        self.captureImage.hidden = NO; //show the captured image view
        self.imagePreview.hidden = YES; //hide the live video feed
        [self capImage];
    }
    else
    {
        self.captureImage.hidden = YES;
        self.imagePreview.hidden = NO;
        haveImage = NO;
    }
}

- (void) capImage { //method to capture image from AVCaptureSession video feed
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in stillImageOutput.connections) {
        
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            
            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
                break;
            }
        }
        
        if (videoConnection) {
            break;
        }
    }
    
    NSLog(@"about to request a capture from: %@", stillImageOutput);
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        
        if (imageSampleBuffer != NULL) {
            
            NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
            [self processImage:[UIImage imageWithData:imageData]];
        }
    }];
    [self.photoCaptureButton setEnabled:YES];
}

- (UIImage*)imageWithImage:(UIImage *)sourceImage scaledToWidth:(float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void) processImage:(UIImage *)image { //process captured image, crop, resize and rotate
    haveImage = YES;
    photoFromCam = YES;
    
    // Resize image to 640x640
    // Resize image
    //    NSLog(@"Image size %@",NSStringFromCGSize(image.size));
    
    UIImage *smallImage = [self imageWithImage:image scaledToWidth:640.0f]; //
    
//    NSData *imageData = [[NSData alloc] init];
//    for (float compression = 1.0; compression >= 0.0; compression -= .1) {
//        imageData = UIImageJPEGRepresentation(smallImage, compression);
//        NSInteger imageLength = imageData.length;
//        if (imageLength < 1000000) {
//            break;
//        }
//    }
//    UIImage *finalImage = [UIImage imageWithData:imageData];
    
    
    
    CGRect cropRect = CGRectMake(0, 0, smallImage.size.width, smallImage.size.height);
    CGImageRef imageRef = CGImageCreateWithImageInRect([smallImage CGImage], cropRect);
    
    croppedImageWithoutOrientation = [[UIImage imageWithCGImage:imageRef] copy];
    
    UIImage *croppedImage = nil;
    //    assetOrientation = ALAssetOrientationUp;
    
    // adjust image orientation
    NSLog(@"orientation: %d",orientationLast);
    orientationAfterProcess = orientationLast;
    switch (orientationLast) {
        case UIInterfaceOrientationPortrait:
            NSLog(@"UIInterfaceOrientationPortrait");
            croppedImage = [UIImage imageWithCGImage:imageRef];
            break;
            
        case UIInterfaceOrientationPortraitUpsideDown:
            NSLog(@"UIInterfaceOrientationPortraitUpsideDown");
            croppedImage = [[[UIImage alloc] initWithCGImage: imageRef
                                                       scale: 0.5
                                                 orientation: UIImageOrientationDown] autorelease];
            break;
            
        case UIInterfaceOrientationLandscapeLeft:
            NSLog(@"UIInterfaceOrientationLandscapeLeft");
            croppedImage = [[[UIImage alloc] initWithCGImage: imageRef
                                                       scale: 0.5
                                                 orientation: UIImageOrientationRight] autorelease];
            break;
            
        case UIInterfaceOrientationLandscapeRight:
            NSLog(@"UIInterfaceOrientationLandscapeRight");
            croppedImage = [[[UIImage alloc] initWithCGImage: imageRef
                                                       scale: 0.5
                                                 orientation: UIImageOrientationLeft] autorelease];
            break;
            
        default:
            croppedImage = [UIImage imageWithCGImage:imageRef];
            break;
    }
    
    CGImageRelease(imageRef);
    
    //[self.captureImage setImage:croppedImage];
    
    
    
    
    [self displayEditorForImage:croppedImage];
    /* ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeImageToSavedPhotosAlbum:croppedImage.CGImage orientation:(ALAssetOrientation)croppedImage.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error )
     {
         NSLog(@"IMAGE SAVED TO PHOTO ALBUM");
         [library assetForURL:assetURL resultBlock:^(ALAsset *asset )
          {
              NSLog(@"we have our ALAsset!");
              [self launchEditorWithAsset:asset];
          }
                 failureBlock:^(NSError *error )
          {
              NSLog(@"Error loading asset");
          }];
     }];*/
    //[self setCapturedImage];
    
    
    
//    // Dismiss self view controller
//    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)setCapturedImage{
    // Stop capturing image
    [session stopRunning];
    
    // Hide Top/Bottom controller after taking photo for editing
    [self hideControllers];
}

#pragma mark - Device Availability Controls
- (void)disableCameraDeviceControls{
    self.cameraToggleButton.enabled = NO;
    self.flashToggleButton.enabled = NO;
    self.photoCaptureButton.enabled = NO;
}

#pragma mark - UIImagePicker Delegate
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
//    if (info)
//    {
//        photoFromCam = NO;
//        
//        UIImage* outputImage = [info objectForKey:UIImagePickerControllerEditedImage];
//        if (outputImage == nil) {
//            outputImage = [info objectForKey:UIImagePickerControllerOriginalImage];
//        }
//        
//        if (outputImage) {
//            self.captureImage.hidden = NO;
//            self.captureImage.image=outputImage;
//            
//            [self dismissViewControllerAnimated:YES completion:nil];
//            
//            // Hide Top/Bottom controller after taking photo for editing
//            [self hideControllers];
//        }
//    }
//}


#pragma mark - Button clicks
- (IBAction)gridToogle:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (sender.selected) {
        sender.selected = NO;
        [UIView animateWithDuration:0.2 delay:0.0 options:0 animations:^{
            self.ImgViewGrid.alpha = 1.0f;
        } completion:nil];
    }
    else{
        sender.selected = YES;
        [UIView animateWithDuration:0.2 delay:0.0 options:0 animations:^{
            self.ImgViewGrid.alpha = 0.0f;
        } completion:nil];
    }
}

-(IBAction)switchToLibrary:(id)sender {
    
    if (session) {
        [session stopRunning];
    }
    isPresenting = YES;
    
    
    //    self.captureImage = nil;
    
    //    UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
    //    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //    imagePickerController.delegate = self;
    //    imagePickerController.allowsEditing = YES;
    [self presentViewController:imgPicker animated:YES completion:NULL];
}

- (IBAction)skipped:(id)sender{
    
    if ([delegate respondsToSelector:@selector(yCameraControllerdidSkipped)]) {
        [delegate yCameraControllerdidSkipped];
    }
    
    // Dismiss self view controller
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction) cancel:(id)sender {
    if ([delegate respondsToSelector:@selector(yCameraControllerDidCancel)]) {
        [delegate yCameraControllerDidCancel];
    }
    
    // Dismiss self view controller
    [self dismissViewControllerAnimated:NO completion:nil];
    
    //commented lines
//  AppDelegate*  appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
//    [appDelegate MainHomeView];
    
}

- (IBAction)donePhotoCapture:(id)sender{
    
    if ([delegate respondsToSelector:@selector(didFinishPickingImage:)]) {
        [delegate didFinishPickingImage:self.captureImage.image];
    }
    
    // Dismiss self view controller
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)retakePhoto:(id)sender{
    [self.photoCaptureButton setEnabled:YES];
    self.captureImage.image = nil;
    self.imagePreview.hidden = NO;
    // Show Camera device controls
    [self showControllers];
    
    haveImage=NO;
    FrontCamera = NO;
//    [self performSelector:@selector(initializeCamera) withObject:nil afterDelay:0.001];
    [session startRunning];
}

- (IBAction)switchCamera:(UIButton *)sender { //switch cameras front and rear cameras
    // Stop current recording process
    [session stopRunning];
    
    if (sender.selected) {  // Switch to Back camera
        sender.selected = NO;
        FrontCamera = NO;
        [self performSelector:@selector(initializeCamera) withObject:nil afterDelay:0.001];
    }
    else {                  // Switch to Front camera
        sender.selected = YES;
        FrontCamera = YES;
        [self performSelector:@selector(initializeCamera) withObject:nil afterDelay:0.001];
    }
}

- (IBAction)toogleFlash:(UIButton *)sender{
    if (!FrontCamera) {
        if (sender.selected) { // Set flash off
            [sender setSelected:NO];
            
            NSArray *devices = [AVCaptureDevice devices];
            for (AVCaptureDevice *device in devices) {
                
                NSLog(@"Device name: %@", [device localizedName]);
                
                if ([device hasMediaType:AVMediaTypeVideo]) {
                    
                    if ([device position] == AVCaptureDevicePositionBack) {
                        NSLog(@"Device position : back");
                        if ([device hasFlash]){
                            
                            [device lockForConfiguration:nil];
                            [device setFlashMode:AVCaptureFlashModeOff];
                            [device unlockForConfiguration];
                            
                            break;
                        }
                    }
                }
            }
            
        }
        else{                  // Set flash on
            [sender setSelected:YES];
            
            NSArray *devices = [AVCaptureDevice devices];
            for (AVCaptureDevice *device in devices) {
                
                NSLog(@"Device name: %@", [device localizedName]);
                
                if ([device hasMediaType:AVMediaTypeVideo]) {
                    
                    if ([device position] == AVCaptureDevicePositionBack) {
                        NSLog(@"Device position : back");
                        if ([device hasFlash]){
                            
                            [device lockForConfiguration:nil];
                            [device setFlashMode:AVCaptureFlashModeOn];
                            [device unlockForConfiguration];
                            
                            break;
                        }
                    }
                }
            }
            
        }
    }
}

#pragma mark - UI Control Helpers
- (void)hideControllers{
    [UIView animateWithDuration:0.2 animations:^{
        //1)animate them out of screen
        self.photoBar.center = CGPointMake(self.photoBar.center.x, self.photoBar.center.y+116.0);
        self.topBar.center = CGPointMake(self.topBar.center.x, self.topBar.center.y-44.0);
        
        //2)actually hide them
        self.photoBar.alpha = 0.0;
        self.topBar.alpha = 0.0;
        
    } completion:nil];
}

- (void)showControllers{
    [UIView animateWithDuration:0.2 animations:^{
        //1)animate them into screen
        self.photoBar.center = CGPointMake(self.photoBar.center.x, self.photoBar.center.y-116.0);
        self.topBar.center = CGPointMake(self.topBar.center.x, self.topBar.center.y+44.0);
        
        //2)actually show them
        self.photoBar.alpha = 1.0;
        self.topBar.alpha = 1.0;
        
    } completion:nil];
}
#pragma mark Aviary SDk
#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    
    if( isPresenting)
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        
        isPresenting = false;
    }
//    AppDelegate *appdlegate = [[UIApplication sharedApplication]delegate];
//
//    
//    
//    [appdlegate.navController dismissViewControllerAnimated:NO completion:nil];

    [self displayEditorForImage:image];
    
    
    if (info)
    {
        photoFromCam = NO;
        
        UIImage* outputImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        if (outputImage == nil)
        {
            outputImage = [info objectForKey:UIImagePickerControllerOriginalImage];
        }
        
        if (outputImage) {
         ///   self.captureImage.hidden = NO;
          //  self.captureImage.image=outputImage;
            
            //[self dismissViewControllerAnimated:YES completion:nil];
            
            // Hide Top/Bottom controller after taking photo for editing
            // [self hideControllers];
        }
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    initializeCamera = YES;
    if( isPresenting)
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        
        isPresenting = false;
    }
//    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Popover Methods

- (void)presentViewControllerInPopover:(UIViewController *)controller
{
    CGRect sourceRect = [[self photoCaptureButton] frame];
    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:controller];
    [popover setDelegate:self];
    [self setPopover:popover];
    [self setShouldReleasePopover:YES];
    
    [popover presentPopoverFromRect:sourceRect
                             inView:[self view]
           permittedArrowDirections:UIPopoverArrowDirectionDown
                           animated:YES];
}

- (void)dismissPopoverWithCompletion:(void(^)(void))completion
{
    [[self popover] dismissPopoverAnimated:YES];
    [self setPopover:nil];
    
    NSTimeInterval delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        completion();
    });
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if ([self shouldReleasePopover]){
        [self setPopover:nil];
    }
    [self setShouldReleasePopover:YES];
}

#pragma mark - ALAssets Helper Methods

- (UIImage *)editingResImageForAsset:(ALAsset*)asset
{
    CGImageRef image = [[asset defaultRepresentation] fullScreenImage];
    
    return [UIImage imageWithCGImage:image
                               scale:1.0
                         orientation:UIImageOrientationUp];
}

- (UIImage *)highResImageForAsset:(ALAsset*)asset
{
    ALAssetRepresentation *representation = [asset defaultRepresentation];
    
    CGImageRef image = [representation fullResolutionImage];
    UIImageOrientation orientation = [representation orientation];
    CGFloat scale = [representation scale];
    
    return [UIImage imageWithCGImage:image
                               scale:scale
                         orientation:orientation];
}

#pragma mark - Creative Adobe SDK Delegate Methods

- (void)displayEditorForImage:(UIImage *)imageToEdit
{
    
    
    //  static dispatch_once_t onceToken;
    //  dispatch_once(&onceToken, ^{
    
    //[AdobeUXImageEditorViewController setAPIKey:@"288cd6ad65e5475798b6b610503a49bb" secret:@"4509756b-edf1-4312-afa5-0569f017216e"];
    
    
    
    // });
    
    initializeCamera = YES;
    [AdobeImageEditorOpenGLManager requestOpenGLDataPurge];
    
   // UINavigationController *navController = [[UINavigationController alloc]init];
    
    AppDelegate *appdlegate = [[UIApplication sharedApplication]delegate];
    
    AVYSDKDemoViewController *demovc = [[AVYSDKDemoViewController alloc]initWithNibName:@"AVYSDKDemoViewController_iPhone" bundle:nil];
    
    
    
    demovc.EditImage = imageToEdit;
//    [self presentViewController:camnavVC animated:NO completion:nil];
    
    [self.navigationController pushViewController:demovc animated:false];
    
    
    
    /*
    
    AdobeUXImageEditorViewController *editorController = [[AdobeUXImageEditorViewController alloc] initWithImage:imageToEdit];
    [editorController setDelegate:self];
    [self presentViewController:editorController animated:YES completion:^{
        
        AVYSDKDemoViewController *demovc = [[AVYSDKDemoViewController alloc]initWithNibName:@"AVYSDKDemoViewController_iPhone" bundle:nil];
        [self.navigationController pushViewController:demovc animated:true];
        
    }];
     */
   // [self.navigationController presentViewController:editorController animated:YES completion:^{}];
    
    
    
 
    
}

- (void)photoEditor:(AdobeUXImageEditorViewController *)editor finishedWithImage:(UIImage *)image
{
    // Handle the result image here and dismiss the editor.
    // [self doSomethingWithImage:image]; // Developer-defined method that presents the final editing-resolution image to the user, perhaps.
    
    //commented lines
//    gloabalImage = image;
//    
//    [self dismissViewControllerAnimated:YES completion:^{
//       
//    }];
//  
//    
//    ShareToViewController* shareView=[[ShareToViewController alloc]initWithNibName:@"ShareToViewController_iPhone5" bundle:nil Selectedimage:gloabalImage];
//   // [self.navigationController pushViewController:shareView animated:YES];
//    [self presentViewController:shareView animated:YES completion:nil];
    
  
}

- (void)photoEditorCanceled:(AdobeUXImageEditorViewController *)editor
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isImagePickerPresent"];
    
    // Dismiss the editor.
    [self dismissViewControllerAnimated:YES completion:nil];
    
    

}


@end
