//
//  SubCategoriesEntity.swift
//  IntoBuy
//
//  Created by SmaatApps on 23/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class SubCategoriesEntity: NSObject
{
var strSubCatID : String = ""
var strSubCatname : String = ""
var strSubCatImage  : String = ""


func initWithCategoriesDictionary(subCategoryDictionary:NSDictionary)
        
{
    strSubCatID = (subCategoryDictionary.objectForKey("subCatId") as? String!)!
    
    strSubCatname = subCategoryDictionary.objectForKey("catname") as! String
    
    strSubCatImage = subCategoryDictionary.objectForKey("catimage") as! String
    
    
}

}

