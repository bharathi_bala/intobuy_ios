//
//  IBListOfCartsViewController.swift
//  IntoBuy
//
//  Created by SmaatApps on 18/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBListOfCartsViewController: IBBaseViewController,UITableViewDataSource,UITableViewDelegate,MGSwipeTableCellDelegate,ServerAPIDelegate,UITextFieldDelegate {
    
    var m_tableView:UITableView!
    var m_lblOrderSUmmary: UILabel = UILabel()
    var m_viwbottomline : UIView = UIView()
    var m_lblSubTotal:UILabel!
    var m_lbldeliveryFee:UILabel!
    var m_lbltotal:UILabel!
    var m_lblSubTotalPrice:UILabel!
    var m_lbldeliveryFeePrice:UILabel!
    var m_lbltotalAmount:UILabel!
    var m_checkoutBtn:UIButton = UIButton()
    var m_scrollview:UIScrollView!
    var m_downview : UIView!
    var cartArray = NSMutableArray()
    var m_orderArray = NSMutableArray()
    var cartButton = UIButton()
    var orderDetailsButton = UIButton()
    var btnIndex = 0
    var subtotalAmt = 0
    var keyBoardTool:UIToolbar!
    
    //for edit product
    var quantityStr = String()
    var totalQuantityStr = String()
    var productId = String()
    var cartId = String()
    
    override func viewDidLoad() {
        bottomVal = 4
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideSettingsBtn(true)
        //m_orderArray = ["Transformer deception cufflinks","Lamphorgini children toy","BMW with air bag","Bharath Benz"]
        self.m_titleLabel.text = Carts_cart_text
        createSegmentButtons()
        createServiceForCart()
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self.createServiceToGetOrderDetails()
        })
        
        
        //cartArray = ["10.00","20.00","30.00","40.00"]
        
        
    }
    
    func  createServiceForCart() -> () {
        SwiftLoader.show(title: "Loading...", animated: true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getProductsOnCart(getUserIdFromUserDefaults())
    }
    func  createServiceToGetOrderDetails() -> () {
        //SwiftLoader.show(title: "Loading...", animated: true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getOrders(getUserIdFromUserDefaults())
    }

    
    func createSegmentButtons()
    {
        let headerBtnNormal = GRAPHICS.ACTIVITY_BUTTON_NORMAL_IMAGE()
        let headerBtnSelected = GRAPHICS.ACTIVITY_BUTTON_SELECTED_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(headerBtnNormal)
        var xPos = GRAPHICS.Screen_X()
        let yPos = self.m_bgImageView.bounds.origin.y
        let width = contentSize.width//headerBtnNormal.size.width/2
        let height = contentSize.height//headerBtnNormal.size.height/2
        
        cartButton.frame = CGRectMake(xPos,yPos,width,height)
        cartButton.setBackgroundImage(headerBtnNormal, forState: .Normal)
        cartButton.setBackgroundImage(headerBtnSelected, forState: .Selected)
        cartButton.setTitle("Cart", forState: .Normal)
        cartButton.titleLabel?.font = GRAPHICS.FONT_BOLD(13)
        cartButton.addTarget(self, action: #selector(IBListOfCartsViewController.cartBtnAction(_:)), forControlEvents: .TouchUpInside)
        cartButton.selected = true
        self.m_bgImageView.addSubview(cartButton)
        
        xPos = cartButton.frame.maxX
        orderDetailsButton.frame = CGRectMake(xPos,yPos,width,height)
        orderDetailsButton.setBackgroundImage(headerBtnNormal, forState: .Normal)
        orderDetailsButton.setBackgroundImage(headerBtnSelected, forState: .Selected)
        orderDetailsButton.setTitle("Order Details", forState: .Normal)
        orderDetailsButton.titleLabel?.font = GRAPHICS.FONT_BOLD(13)
        orderDetailsButton.addTarget(self, action: #selector(IBListOfCartsViewController.orderDetailsBtnAction(_:)), forControlEvents: .TouchUpInside)
        self.m_bgImageView.addSubview(orderDetailsButton)
        addDividerImage(orderDetailsButton)

    }
    
    func initControls()
    {
        if m_scrollview != nil{
            m_scrollview.removeFromSuperview()
            m_scrollview = nil
        }
        
        m_scrollview = UIScrollView(frame: CGRectMake(GRAPHICS.Screen_X(), orderDetailsButton.frame.maxY, GRAPHICS.Screen_Width(), GRAPHICS.Screen_Height()-self.m_headerView.frame.size.height-orderDetailsButton.frame.size.height));
        m_bgImageView .addSubview(m_scrollview)
        
        
        var tblHeight:CGFloat
        
        if btnIndex == 0 {
           tblHeight = CGFloat(cartArray.count * 110)
        }
        else{
            tblHeight = CGFloat(Double(m_orderArray.count) * 103.667)
        }
        
        m_tableView = UITableView()
        m_tableView.frame = CGRectMake(0, 0, GRAPHICS.Screen_Width(), tblHeight)
        m_tableView.dataSource = self
        m_tableView.delegate = self
        m_tableView.separatorStyle = .None
        m_tableView.scrollEnabled = false
        m_scrollview.addSubview(m_tableView)
        
        self .createBottomViewForCart()
       
    }
    
    func createBottomViewForCart()
    {
        var ypos : CGFloat = m_tableView.frame.maxY + 5
        var xpos : CGFloat = 0
        var width: CGFloat = GRAPHICS.Screen_Width()
        var height: CGFloat = 180
        let rightLabelWidth: CGFloat = 130
        
        //Creation of BottomView
        m_downview = UIView(frame:CGRectMake(xpos,ypos,width,height))
        m_downview.backgroundColor = UIColor.clearColor()
        m_scrollview.addSubview(m_downview)
        
        //Creation of Orderlabel
        
        xpos = 10
        ypos = 10
        width = GRAPHICS.Screen_Width()-20
        height = 30
        
        
        m_lblOrderSUmmary = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr: cart_order_summary_text)
        m_lblOrderSUmmary.textAlignment = .Left
        m_downview.addSubview(m_lblOrderSUmmary)
        
        m_viwbottomline = self.creationOfBottomLineView(xpos, ypos:m_lblOrderSUmmary.frame.maxY-1, width: width, height: 1)
        m_downview.addSubview(m_viwbottomline)
        
        //Creation of Payment labels
        
        ypos = m_viwbottomline.frame.maxY
        width = 140
        
        m_lblSubTotal = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr: cart_subtotal_text)
        m_lblSubTotal.textAlignment = .Left
        m_downview.addSubview(m_lblSubTotal)
        
        xpos = (GRAPHICS.Screen_Width()-rightLabelWidth-20)
       
        m_lblSubTotalPrice = self.createLabel(xpos, ypos: ypos, width: rightLabelWidth, height: height, textStr: String(format:"$%d",subtotalAmt))
        m_lblSubTotalPrice.textAlignment = NSTextAlignment.Right
        m_downview.addSubview(m_lblSubTotalPrice)
        
        
        //creation of delivery Fee label
   /*     ypos = m_lblSubTotal.frame.maxY
        width = 170
        xpos = 10
        
        m_lbldeliveryFee = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr: cart_delivery_fee)
        m_lbldeliveryFee.textColor = appPinkColor
        m_lbldeliveryFee.textAlignment = .Left
        m_downview.addSubview(m_lbldeliveryFee)
        
        xpos = (GRAPHICS.Screen_Width()-rightLabelWidth-20)
        
        m_lbldeliveryFeePrice = self.createLabel(xpos, ypos: ypos, width: rightLabelWidth, height: height, textStr: "$10.00")
        m_lbldeliveryFeePrice.textAlignment = NSTextAlignment.Right
        m_downview.addSubview(m_lbldeliveryFeePrice)
        
        //creation of total Fee label
        ypos = m_lbldeliveryFee.frame.maxY
        width = 170
        xpos = 10
        
        m_lbltotal = self.createLabel(xpos, ypos: ypos, width: width, height: height, textStr: cart_total_fee)
        m_lbltotal.textAlignment = .Left
        m_downview.addSubview(m_lbltotal)
        
        xpos = (GRAPHICS.Screen_Width()-rightLabelWidth-20)
        
        let totalAmt = subtotalAmt + Int(10.00)
        let totalAmtStr = String(format:"$ %d",totalAmt)

        m_lbltotalAmount = self.createLabel(xpos, ypos: ypos, width: rightLabelWidth, height: height, textStr: totalAmtStr)
        m_lbltotalAmount.textAlignment = NSTextAlignment.Right
        m_downview.addSubview(m_lbltotalAmount)*/
        
        ypos = m_lblSubTotal.frame.maxY + 5

        
        
     
        let imgSize:CGSize = self .getImageWidthHeight(GRAPHICS.PRODUCT_BUYNOW_IMAGE())
        
        xpos = (GRAPHICS.Screen_Width() - imgSize.width - 10)
        
        m_checkoutBtn.frame = CGRectMake(xpos, ypos, imgSize.width, imgSize.height)
        m_checkoutBtn .setTitle(cart_checkOut_all_text, forState: .Normal)
        m_checkoutBtn.addTarget(self, action: #selector(IBListOfCartsViewController.checkOutAllButtonClicked(_:)), forControlEvents: .TouchUpInside)
        m_checkoutBtn .setBackgroundImage(GRAPHICS.PRODUCT_BUYNOW_IMAGE(), forState: .Normal)
        m_checkoutBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(12)
        m_downview .addSubview(m_checkoutBtn)
              
        
     
    m_scrollview.contentSize = CGSizeMake(GRAPHICS.Screen_Width(),max(GRAPHICS.Screen_Width(),m_downview.frame.maxY+50))
    }
    
    //Tableview datasource and delagte methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if btnIndex == 0{
        return cartArray.count
        }
        else{
            return m_orderArray.count
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cellIdentifier = "Cell"
        if btnIndex == 0{
        var cell:IBListCartTableCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBListCartTableCell
        
        if (cell == nil) {
            cell = IBListCartTableCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
       
        
        //tableView.frame.size.height = max(10,tableView.contentSize.height)
        cell?.showORhideCartButton(true)
        let cartEntity = cartArray.objectAtIndex(indexPath.row) as! IBCartEntity
        //cell?.m_priceAmountLbl.text = cartArray.objectAtIndex(indexPath.row)as? String
        cell?.setValuesToControls(cartEntity.mainImage, ProductName: cartEntity.productname, stockAvailable: cartEntity.remainingquantity, quantityStr: cartEntity.quantity, priceStr:("$" + cartEntity.totalprice))
            cell?.m_cartIamegview.tag = indexPath.row * 10
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IBListOfCartsViewController.productImageTapGesture(_:)))
            cell?.m_cartIamegview.addGestureRecognizer(tapGesture)
            
        cell?.delegate = self
            
        cell?.m_noOfQuantityTxtFiled.delegate = self
        cell?.m_noOfQuantityTxtFiled.tag = indexPath.row * 100
            
        cell?.m_checkoutBtn.tag = indexPath.row
        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        tableView.frame.size.height = CGFloat(cartArray.count * 110)
        m_downview.frame.origin.y = tableView.frame.maxY
            cell?.m_checkoutBtn.tag = indexPath.row
            cell?.m_checkoutBtn.addTarget(self, action: #selector(IBListOfCartsViewController.checkoutBtnAction(_:)), forControlEvents: .TouchUpInside)
            
            
        return cell!
        }
        else{
            var cell:IBMyOrdersCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBMyOrdersCell
            if (cell == nil) {
                cell = IBMyOrdersCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            }
            let orderEntity = m_orderArray.objectAtIndex(indexPath.row) as! IBOrderEntity
            
            let dateFormatter:NSDateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.dateFromString(orderEntity.orderDate)
            
            let newdateFormatter:NSDateFormatter = NSDateFormatter()
            newdateFormatter.dateFormat = "dd/MM/yyyy"
            let dateStr = newdateFormatter.stringFromDate(date!)

            
            //let dateStr = dateFormatter.stringFromDate(date)
            cell?.selectionStyle = .None
            cell?.setTitlesToAllLabels(orderEntity.productname, dateString: dateStr, orderNoStr: orderEntity.orderId, transIdStr: orderEntity.transactionId, quantityStr: orderEntity.quantity, priceStr: orderEntity.price, buttonStatus: orderEntity.user_status ,productImage:orderEntity.productimage )
            tableView.frame.size.height = CGFloat(Double(m_orderArray.count)) * (cell?.frame.size.height)! + 50/*103.667*/
            m_scrollview.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), tableView.frame.maxY)
            return cell!

        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if btnIndex == 0{
            return 110
        }
        else{
            return 103.667
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        cartArray.removeObjectAtIndex(indexPath.row)
//        m_tableView.reloadData()
    }
    
    func productImageTapGesture(tapGesture : UITapGestureRecognizer)
    {
        let cartEntity = cartArray.objectAtIndex((tapGesture.view?.tag)!/10) as! IBCartEntity
        let productDetailVC = IBProductDetailsViewController()
        productDetailVC.productId = cartEntity.productId
        self.navigationController?.pushViewController(productDetailVC, animated: true)
    }
    
    //Creation oF Product labels
    func  createLabel(xpos:CGFloat,ypos: CGFloat,width: CGFloat,height:CGFloat,textStr : String)-> UILabel
    {
        let lbl : UILabel = UILabel()
        lbl.frame = CGRectMake(xpos, ypos, width, height)
        lbl.backgroundColor = UIColor.clearColor()
        lbl.text = textStr
        lbl.textColor = UIColor.blackColor()
        lbl.font = GRAPHICS.FONT_REGULAR(13)
        return lbl
        
    }
    //MARK:- mgswipe tablew cell
    func swipeTableCell(cell: MGSwipeTableCell!, canSwipe direction: MGSwipeDirection) -> Bool {
        return true
    }
    func swipeTableCell(cell: MGSwipeTableCell!, swipeButtonsForDirection direction: MGSwipeDirection, swipeSettings: MGSwipeSettings!, expansionSettings: MGSwipeExpansionSettings!) -> [AnyObject]! {
        
        swipeSettings.transition = .Border;
        expansionSettings.buttonIndex = 0;
      //  __weak MSSendNotificationViewController * me = self;
        
        //let   me =  IBListOfCartsViewController()
        //    MSNotificationEntity *entity = [m_notificationsArray objectAtIndex:[m_notificationTableView indexPathForCell:cell].row];
        
        if (direction == .LeftToRight) {
            
        }
        else {
            
            expansionSettings.fillOnTrigger = true;
            expansionSettings.threshold = 1.1;
            // Add a remove button to the cell
            let removeButton = MGSwipeButton(title: "Remove", backgroundColor: UIColor.redColor(), callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                //print(self.cartArray)
                let indexPath = self.m_tableView.indexPathForCell(sender);
                self.deleteAction(indexPath!)
                return true
            })
            cell?.rightButtons = [removeButton]
            return [removeButton];
        }

        return nil
    }
    func swipeTableCell(cell: MGSwipeTableCell!, didChangeSwipeState state: MGSwipeState, gestureIsActive: Bool) {
        
    }
    func deleteAction(indexPath : NSIndexPath)
    {
        //print(indexPath.row)
        //print(self.cartArray)
        let cartEntity = self.cartArray.objectAtIndex(indexPath.row) as! IBCartEntity
        let userIdStr = getUserIdFromUserDefaults()
        let cartIdStr = cartEntity.cartId
        
        SwiftLoader.show(title: "Loading...", animated: true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_deleteProductFromCart(userIdStr, cartId: cartIdStr)
    }
    //MARK:- textfield delegates
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        textField.inputAccessoryView = createKeyboardToolBar()
        if textField.tag > 200
        {
            m_scrollview.setContentOffset(CGPointMake(0, m_scrollview.contentOffset.y + 250), animated: true)
        }
        
        return true
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.tag > 200
        {
            m_scrollview.setContentOffset(CGPointMake(0, 0), animated: true)
        }
        
        let cartEntity = self.cartArray.objectAtIndex(textField.tag/100) as! IBCartEntity
        quantityStr = textField.text!
        productId = cartEntity.productId
        totalQuantityStr = cartEntity.totalquantity
        cartId = cartEntity.cartId
        
        let quantityValueInt = Int(quantityStr)
        let inStockInt = Int(totalQuantityStr)
        if inStockInt >= quantityValueInt
        {
            updateQuantity()
        }
        else
        {
            showAlertViewWithMessage("Please enter quantity as per available stock quantity")
            textField.text = "1"
        }
        
        //indexPath.row * 100
        
//        productID=[[MyCartArray objectAtIndex:indexPath.section]objectForKey:@"pid"];
//        
//        stockValue=[[MyCartArray objectAtIndex:indexPath.section]objectForKey:@"remainingquantity"];
//        StockValue=[stockValue intValue];
//        textQTY=textField.text
        
        
        
//        let indexPath = NSIndexPath(forRow: 0, inSection: textField.tag/100);
//        let tableViewCell = self.m_tableView.cellForRowAtIndexPath(indexPath) as! IBProductsTableViewCell
        
    }
    
    
    func updateQuantity()
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            
//                let quantityValueInt = Int(self.quantityStr)
//                let inStockInt = Int(self.inStockStr)
//            if inStockInt >= quantityValueInt
//            {
                let userId = getUserIdFromUserDefaults()
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_editCart(self.cartId, userId: userId, quantity: self.quantityStr)
                
//            }
//            else
//            {
                //showAlertViewWithMessage("Please enter quantity as per available stock quantity")
                
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter quantity as per available stock quantity." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alert show];
          //  }
        })
    }
    
    
    //creation Of BottomLines
    func creationOfBottomLineView(xpos: CGFloat,ypos: CGFloat,width : CGFloat,height:CGFloat)->UIView
    {
        let viw : UIView = UIView()
        viw.frame = CGRectMake(xpos, ypos, width, height)
        viw.backgroundColor = UIColor.lightGrayColor()
        return viw
    }
    //MARK:- checkout btn action
    func checkoutBtnAction(sender : UIButton)
    {
        let cartEntity = cartArray.objectAtIndex(sender.tag) as! IBCartEntity
        do {
            let js = JSONSerializer.toJson(cartEntity)
            
            let objectData = js.dataUsingEncoding(NSUTF8StringEncoding)
            let json = try NSJSONSerialization .JSONObjectWithData(objectData!, options: .MutableContainers)
            let productArray = NSMutableArray(object: json as! NSDictionary)
            saveProductDetailsToUserDefaults( productArray)
        } catch let error as NSError {
            //print("Failed to load: \(error.localizedDescription)")
        }
        
        quantityStr = cartEntity.quantity
        let quantityValueInt = Int(quantityStr)//Int(cartEntity.quantity)
        let inStockInt = Int(cartEntity.totalquantity)
        if inStockInt >= quantityValueInt
        {
            if quantityValueInt != 0{
                let shippingVc = IBShippingDetailsViewController()
                shippingVc.isFromProducts = true
                shippingVc.productIdStr = cartEntity.productId
                shippingVc.sellerIdStr = cartEntity.sellerId
                self.navigationController?.pushViewController(shippingVc, animated: true)
            }
            else{
                showAlertViewWithMessage("Please enter possible quantity")
            }
        }
        else
        {
            showAlertViewWithMessage("Please enter quantity as per available stock quantity")
        }
    }
    //MARK: - checkout all button action
    func checkOutAllButtonClicked(sender: UIButton)
    {
        let productIdArray = NSMutableArray()
        let quantityArray = NSMutableArray()
//        let productsDict = NSMutableDictionary(object: cartArray, forKey: "allProducts")
//        saveProductDetailsToUserDefaults(NSDictionary(dictionary: productsDict))
        if cartArray.count >  0
        {
            for i in 0  ..< cartArray.count
            {
                let cartEntity = cartArray.objectAtIndex(i) as! IBCartEntity
                productIdArray.addObject(cartEntity.productId)
                quantityArray.addObject(cartEntity.quantity)
            }
            let quantityStr = quantityArray.componentsJoinedByString(",")
            let productIdstr = productIdArray.componentsJoinedByString(",")
            if quantityStr.containsString("0")
            {
                showAlertViewWithMessage("Any one of product quantity is zero")
            }
            else
            {
                
                let shippingVc = IBShippingDetailsViewController()
                shippingVc.isFromProducts = true
                shippingVc.productIdStr = productIdstr
                shippingVc.sellerIdStr = ""
                self.navigationController?.pushViewController(shippingVc, animated: true)
            }
        }
        else{
            showAlertViewWithMessage(CheckOut_All_Alert)
        }
    }
    //Get CorrectImage width and Image Height
    func getImageWidthHeight(imageName:UIImage)->CGSize
    {
        var fWidth:CGFloat = 0.0
        var fHeight:CGFloat = 0.0
        
        let searchiew:UIImage = imageName
        fWidth = searchiew.size.width/2.5
        fHeight = searchiew.size.height/2.5
        
        if GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus
        {
            fWidth = searchiew.size.width/2.0
            fHeight = searchiew.size.height/2.0
        }
        
        return CGSizeMake(fWidth, fHeight)
        
    }
    // method for tool bar
    func createKeyboardToolBar() -> UIToolbar
    {
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        keyBoardTool = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        keyBoardTool!.barStyle = UIBarStyle.BlackOpaque
        keyBoardTool!.translucent = true
        keyBoardTool!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        keyBoardTool!.sizeToFit()
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBListOfCartsViewController.doneBtnAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(done_Btn)
        
        
        keyBoardTool!.userInteractionEnabled = true
        return keyBoardTool
        
    }
    func doneBtnAction() -> () {
        self.view.endEditing(true)
    }

    
    func cartBtnAction(sender : UIButton)
    {
        btnIndex = 0
        cartButton.selected = true
        orderDetailsButton.selected = false
        m_downview.hidden = false
        m_tableView.frame.size.height = CGFloat(cartArray.count * 110)//m_scrollview.frame.size.height - m_downview.frame.size.height
        m_scrollview.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), m_downview.frame.maxY+50)
        m_tableView.reloadData()
    }
    func orderDetailsBtnAction(sender : UIButton)
    {
        btnIndex = 1
        cartButton.selected = false
        orderDetailsButton.selected = true
        m_downview.hidden = true
        m_tableView.frame.size.height =  m_scrollview.frame.size.height - orderDetailsButton.frame.size.height
        m_scrollview.contentSize = CGSizeMake(GRAPHICS.Screen_Width(), m_tableView.frame.maxY)
        m_tableView.reloadData()
    }

    func addDividerImage(view : UIView)
    {
        let dividerImg = GRAPHICS.ACTIVITY_DIVIDER_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(dividerImg)
        let width = contentSize.width
        let height = contentSize.height
        let dividerImgView = UIImageView(frame : CGRectMake(view.bounds.origin.x, (view.frame.size.height - height)/2, width, height))
        dividerImgView.image = dividerImg
        view.addSubview(dividerImgView)
    }
    func addBottomBorder(view: UIView)
    {
        //let lineImg = GRAPHICS.POST_ALERT_BORDER_LINE()
        let bottomBorder = UIImageView(frame: CGRectMake(view.frame.origin.x + 2, view.bounds.origin.y, view.frame.size.width - 4, 1.0))
        bottomBorder.backgroundColor = UIColor.lightGrayColor()
        view.addSubview(bottomBorder)
    }

    //MARK:- api delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    func API_CALLBACK_getProductsOnCart(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        cartArray.removeAllObjects()
        if errorCode == "1"
        {
            let resultArray = resultDict.objectForKey("result") as! NSArray
            saveProductDetailsToUserDefaults(resultArray)
            var updatedAmount = 0
            for i in 0  ..< resultArray.count 
            {
                let cartDict = resultArray.objectAtIndex(i) as! NSDictionary
                let cartEntity = IBCartEntity(dict: cartDict)
                cartArray.addObject(cartEntity)
                subtotalAmt = Int(Float(cartEntity.totalprice)!) + updatedAmount
                updatedAmount = subtotalAmt
            }
            if cartArray.count == 0
            {
                subtotalAmt = 0
            }

            initControls()
        }
        else
        {
           showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    func API_CALLBACK_deleteProductFromCart(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code") as! String
        if errorCode == "1"
        {
            showAlertViewWithMessage(resultDict.objectForKey("result") as! String)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_getProductsOnCart(getUserIdFromUserDefaults())
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
                
                let serverApi = ServerAPI()
                serverApi.delegate = self
                serverApi.API_cartcount(getUserIdFromUserDefaults())
            }
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    func API_CALLBACK_cartcount(resultDict: NSDictionary)
    {
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            let cartCountStr = resultDict.objectForKey("result")as! String
            saveCartCountInUserDefaults(cartCountStr)
            cartcountLbl.text = cartCountStr
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }
    func API_CALLBACK_editCart(resultDict: NSDictionary)
    {
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            cartArray.removeAllObjects()
            
            let resultArray = resultDict.objectForKey("result") as! NSArray
            saveProductDetailsToUserDefaults(resultArray)
            var updatedAmount = 0
            for i in 0  ..< resultArray.count
            {
                let cartDict = resultArray.objectAtIndex(i) as! NSDictionary
                let cartEntity = IBCartEntity(dict: cartDict)
                cartArray.addObject(cartEntity)
                subtotalAmt = Int(Float(cartEntity.totalprice)!) + updatedAmount
                updatedAmount = subtotalAmt
            }
            initControls()
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }

    }
    func API_CALLBACK_getOrders(resultDict: NSDictionary)
    {
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            m_orderArray.removeAllObjects()
            let resultArray = resultDict.objectForKey("result") as! NSArray
            for i in 0 ..< resultArray.count
            {
                let resultDict = resultArray.objectAtIndex(i) as! NSDictionary
                let orderEntity = IBOrderEntity(dict: NSDictionary(dictionary: resultDict))
                m_orderArray.addObject(orderEntity)
            }
           // m_tableView.reloadData()
        }
        else
        {
            showAlertViewWithMessage(ServerNotRespondingMessage)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
