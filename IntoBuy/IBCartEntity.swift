//
//  IBCartEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 17/06/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBCartEntity: NSObject {
    
    var userId: String
    var cartId: String
    var sellerId: String
    var productId : String
    var quantity : String
    var descriptionStr : String
    var mainImage : String
    var price : String
    var totalprice : String
    var productname : String
    var selleravatar : String
    var status : String
    var remainingquantity : String
    var totalquantity : String
    
    init(dict: NSDictionary) {
        self.userId = (GRAPHICS.checkKeyNotAvail(dict, key:"userId")as? String)!
        self.cartId = (GRAPHICS.checkKeyNotAvail(dict, key:"cartId")as? String)!
        self.sellerId = (GRAPHICS.checkKeyNotAvail(dict, key:"sellerId")as? String)!
        self.productId = (GRAPHICS.checkKeyNotAvail(dict, key:"productId")as? String)!
        self.quantity = (GRAPHICS.checkKeyNotAvail(dict, key:"quantity")as? String)!
        self.descriptionStr = (GRAPHICS.checkKeyNotAvail(dict, key:"description")as? String)!
        self.mainImage = (GRAPHICS.checkKeyNotAvail(dict, key:"mainImage")as? String)!
        self.price = (GRAPHICS.checkKeyNotAvail(dict, key:"price")as? String)!
        self.totalprice = (GRAPHICS.checkKeyNotAvail(dict, key:"totalprice")as? String)!
        self.productname = (GRAPHICS.checkKeyNotAvail(dict, key:"productname")as? String)!
        self.selleravatar = (GRAPHICS.checkKeyNotAvail(dict, key:"selleravatar")as? String)!
        self.status = (GRAPHICS.checkKeyNotAvail(dict, key: "status")as! String)
        self.remainingquantity = (GRAPHICS.checkKeyNotAvail(dict, key:"remainingquantity")as? String)!
        self.totalquantity = (GRAPHICS.checkKeyNotAvail(dict, key:"totalquantity")as? String)!
    }


}
