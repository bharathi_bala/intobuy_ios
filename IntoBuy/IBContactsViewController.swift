//
//  IBContactsViewController.swift
//  IntoBuy
//
//  Created by Manojkumar on 19/04/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit
import AddressBook

class IBContactsViewController: IBBaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate,MGSwipeTableCellDelegate,ServerAPIDelegate,ContactDelegate,InboxDelegate, searchProtocol {
    
    var m_searchArray = NSMutableArray()
    var searchTextField = UITextField()
    var searchView:UIView!
    var buttonArray:NSArray = [Contacts,"Inbox"]
    var contactsTblView:UITableView!
    var btnIndex = 0
    var contactArray = NSMutableArray()
    var msgArray = NSMutableArray()
    var keyBoardTool:UIToolbar!
    var msgTextView:UITextView!
    var sendButton:UIButton!
    var msgBtnIndex = 0
    var msgstring = ""
    var inboxArray = NSMutableArray()
    var productArray:NSMutableArray = ["Gift set","Pen stand","Accessories","Stationaries","Gadgets","Textiles","Vehicles"]
    var addressBook: ABAddressBookRef?
    var productStr = String()
    var amountStr = String()
    var numArray = NSMutableArray()
    var inboxDict = NSDictionary()
    let searchButton = UIButton() // deepika to fix the issue in Search Friends fucntion not working is modified as button 

    
    override func viewDidLoad() {
        bottomVal = 5
        super.viewDidLoad()
        callServiceForFollowing()
        self.hideSettingsBtn(true)
        self.hideHeaderViewForView(false)
        self.hideBottomView(false)
        self.m_titleLabel.text = Contacts
        createControls()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self.callServiceForInbox()
        })
        
    }
    
    
    func createControls()
    {
        searchView = UIView(frame: CGRectMake(self.m_bgImageView.bounds.origin.x,self.m_bgImageView.bounds.origin.y,self.m_bgImageView.frame.size.width,50))
        searchView.backgroundColor = lightGrayColor
        self.m_bgImageView .addSubview(searchView)
        
        //Jose Modified
        
        let searchImage = GRAPHICS.PRODUCT_SEARCH_IMAGE()
        
        let posX = CGFloat(20)
        let textImg = GRAPHICS.PRODUCT_SEARCH_TEXT_IMAGE()
        var imgWidth = searchView.frame.width - (posX*2) - (((searchImage.size.width)/1.5) + 5)//textImg.size.width/2
        var imgHei = CGFloat(30)//textImg.size.height/1.5
        
        searchTextField.frame = CGRectMake(posX, (searchView.frame.size.height - imgHei)/2, imgWidth, imgHei)
        searchTextField.background = textImg
        searchTextField.delegate = self
        searchTextField.inputAccessoryView = createKeyboardToolBar()
        searchTextField.placeholder = SearchFriends
        searchTextField.font = GRAPHICS.FONT_REGULAR(14)
        searchTextField.textAlignment = NSTextAlignment.Center
        searchTextField.returnKeyType = .Done
        searchTextField.addTarget(self, action: #selector(IBContactsViewController.textFieldDidChange(_:)), forControlEvents: .EditingChanged)
        searchView.addSubview(searchTextField)
        
//        let searchImage = GRAPHICS.PRODUCT_SEARCH_IMAGE()
        imgWidth = CGFloat(30)//(searchImage.size.width)/1.5
        imgHei = CGFloat(30)//searchImage.size.height/1.5
        
//        let searchImgView = UIImageView(frame: CGRectMake(searchTextField.frame.maxX + 5, (searchView.frame.size.height - imgHei)/2, imgWidth, imgHei))
//        searchImgView.image = searchImage
//        searchImgView.contentMode = .ScaleAspectFit
//        searchView.addSubview(searchImgView)
        
        // deepika edited
        searchButton.frame = CGRect(x: searchTextField.frame.maxX + 5, y: (searchView.frame.size.height - imgHei)/2, width: imgWidth, height: imgHei)
        searchButton.setImage(searchImage, forState: .Normal)
        searchButton.addTarget(self, action: #selector(IBContactsViewController.searchButtonAction(_:)), forControlEvents: .TouchUpInside)
        searchView.addSubview(searchButton)
        
        createSegmentButtons()
    }
    
    // deepika edited
    func searchButtonAction(sender: UIButton) {
        searchTextField.resignFirstResponder()
    }
    
    func createSegmentButtons()
    {
        //Jose Edited
        var xPos = GRAPHICS.Screen_X()
        let segmentImgNormal = GRAPHICS.ACTIVITY_BUTTON_NORMAL_IMAGE()
        let segmentImgSel = GRAPHICS.ACTIVITY_BUTTON_SELECTED_IMAGE()
//        let contentSize = GRAPHICS.getImageWidthHeight(segmentImgNormal)
        let width = (m_bgImageView.frame.width/2)//contentSize.width
        let height = CGFloat(30)//contentSize.height
        for i in 0  ..< buttonArray.count
        {
            
            let yPos = searchView.frame.maxY
            let segmentBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
            segmentBtn.setBackgroundImage(segmentImgNormal, forState: .Normal)
            segmentBtn.setBackgroundImage(segmentImgSel, forState: .Selected)
            segmentBtn.setTitle(buttonArray.objectAtIndex(i) as? String, forState: .Normal)
            segmentBtn.titleLabel!.font = GRAPHICS.FONT_REGULAR(14)
            segmentBtn.tag = i
            segmentBtn.addTarget(self, action: #selector(IBContactsViewController.segmentBtnAction(_:)), forControlEvents: .TouchUpInside)
            self.m_bgImageView .addSubview(segmentBtn)
            self.addDividerImage(segmentBtn)
            if i == 0
            {
                segmentBtn.selected = true
            }
            xPos = xPos + width
        }
        createTableView()
    }
    
    func createTableView()
    {
        let xPos = GRAPHICS.Screen_X()
        let yPos = searchView.frame.maxY + 30
        let height = self.m_bgImageView.frame.size.height - searchView.frame.size.height - 30
        let width = GRAPHICS.Screen_Width()
        //self.m_bgImageView.frame.size.height - searchView.frame.size.height  - sender.frame.size.height
        contactsTblView = UITableView(frame: CGRectMake(xPos,yPos,width,height))
        contactsTblView.delegate = self
        contactsTblView.dataSource = self
        contactsTblView.separatorStyle = .None
        self.m_bgImageView .addSubview(contactsTblView)
    }
    //MARK: - service for search
    func callServiceForSearch()
    {
        let userDetailUD = NSUserDefaults()
        let userIdStr = userDetailUD.objectForKey("user_id") as! String;
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getAllUsers(userIdStr)
    }
    //MARK: - service for following
    func callServiceForFollowing()
    {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getFollowingForUser(getUserIdFromUserDefaults(), myUserId: getUserIdFromUserDefaults())
    }
    //MARK: - service for following
    func callServiceForInbox()
    {
        let serverApi = ServerAPI()
        serverApi.delegate = self
        serverApi.API_getUserInbox(getUserIdFromUserDefaults())
    }
    //MARK: - table view delegates
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if btnIndex == 0
        {
            return 60
            
        }
        else if btnIndex == 1
        {
            return 80
        }
        else
        {
            return 70.0
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if btnIndex == 0
        {
            return contactArray.count
        }
        else if btnIndex == 1
        {
            return inboxArray.count
        }
        else
        {
            return m_searchArray.count
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        if btnIndex == 0
        {
            var cell:IBContactsCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBContactsCell
            if (cell == nil) {
                cell = IBContactsCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            let contactDict = contactArray.objectAtIndex(indexPath.row) as? NSDictionary
            cell?.nameLabel.text = contactDict?.objectForKey("username") as? String
            cell?.profileImgView.load(NSURL(string: contactDict?.objectForKey("avatar") as! String)!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
            cell?.nameLabel.sizeToFit()
            cell?.profileImgView.tag = indexPath.row * 100
            cell?.diaryBtn.tag = indexPath.row * 10
            cell?.msgBtn.tag = indexPath.row
            cell?.contactProtocol = self
            cell?.diaryBtn.addTarget(self, action: #selector(IBContactsViewController.diaryBtnAction(_:)), forControlEvents: .TouchUpInside)
            cell?.msgBtn.addTarget(self, action: #selector(IBContactsViewController.msgBtnAction(_:)), forControlEvents: .TouchUpInside)
           
            cell!.diaryBtn.hidden = true //Added based on feedback
            
            return cell!;
        }
        else if btnIndex == 1
        {
            var cell:IBInboxCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBInboxCell
            if (cell == nil) {
                cell = IBInboxCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            }
            cell?.inboxProtocol = self
            
            let inboxEntity = inboxArray.objectAtIndex(indexPath.row) as! IBInboxEntity
            
            if inboxEntity.productId == ""
            {
                let toId = inboxEntity.msgTo
                let fromId = inboxEntity.msgFrom
                
                if fromId == getUserIdFromUserDefaults()
                {
                    //                    cell?.nameLabel.text = inboxEntity.ToUsername
                    //                    let profileImageUrl = NSURL(string: inboxEntity.ToAvatar)
                    //                    cell?.profileImgView.load(profileImageUrl!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
                    let messageStr = decodeEncodedStringToNormalString(inboxEntity.message)
                    cell?.setValuesToControls(inboxEntity.ToAvatar, nameStr: inboxEntity.ToUsername, messageStr: messageStr, productNameStr: "", productImgString: "", profileImgTag: indexPath.row, productImgTag: indexPath.row + 10)
                }
                else{
                    //                    cell?.nameLabel.text = inboxEntity.FromUsername
                    //                    let profileImageUrl = NSURL(string: inboxEntity.FromAvatar)
                    //                    cell?.profileImgView.load(profileImageUrl!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
                    
                    let messageStr = decodeEncodedStringToNormalString(inboxEntity.message)
                    cell?.setValuesToControls(inboxEntity.ToAvatar, nameStr: inboxEntity.FromUsername, messageStr: messageStr, productNameStr: "", productImgString: "", profileImgTag: indexPath.row, productImgTag: indexPath.row + 10)
                    
                }
                //                let messageStr = decodeEncodedStringToNormalString(inboxEntity.message)
                //                cell?.msgLabel.text = messageStr
                cell?.productNameLbl.hidden = true
                cell?.productImgView.hidden = true
                
            }
            else
            {
                let toId = inboxEntity.msgTo
                let fromId = inboxEntity.msgFrom
                if fromId == getUserIdFromUserDefaults()
                {
                    // cell?.nameLabel.text = inboxEntity.ToUsername
                    //                    let profileImageUrl = NSURL(string: inboxEntity.ToAvatar)
                    //                    cell?.profileImgView.load(profileImageUrl!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
                    
                    let messageStr = decodeEncodedStringToNormalString(inboxEntity.message)
                    cell?.setValuesToControls(inboxEntity.ToAvatar, nameStr: inboxEntity.ToUsername, messageStr: messageStr, productNameStr: inboxEntity.productname, productImgString: inboxEntity.mainImage, profileImgTag: indexPath.row, productImgTag: indexPath.row + 10)
                }
                else{
                    //cell?.nameLabel.text = inboxEntity.FromUsername
                    
                    let messageStr = decodeEncodedStringToNormalString(inboxEntity.message)
                    cell?.setValuesToControls(inboxEntity.ToAvatar, nameStr: inboxEntity.FromUsername, messageStr: messageStr, productNameStr: inboxEntity.productname, productImgString: inboxEntity.mainImage, profileImgTag: indexPath.row, productImgTag: indexPath.row + 10)
                    
                    
                    //                    let profileImageUrl = NSURL(string: inboxEntity.FromAvatar)
                    //                    cell?.profileImgView.load(profileImageUrl!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
                }
                
                //                let productImageUrl = NSURL(string: inboxEntity.mainImage)
                //                cell?.productImgView.load(productImageUrl!, placeholder: GRAPHICS.DEFAULT_CART_IMAGE(), completionHandler: nil)
                
                
                //                let messageStr = decodeEncodedStringToNormalString(inboxEntity.message)
                //                cell?.msgLabel.text = messageStr
                //                cell?.productNameLbl.text = inboxEntity.productname
                cell?.productNameLbl.hidden = false
                cell?.productImgView.hidden = false
                
            }
            
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            return cell!;
            
        }
        else
        {
            var cell:IBSearchTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBSearchTableViewCell
            
            if (cell == nil) {
                cell = IBSearchTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            }
            //SwiftLoader.hide()
            var folloStatus = "0"
            if m_searchArray.count > 0 {
                let resultDict = m_searchArray.objectAtIndex(indexPath.row)as! NSDictionary
                let followStatus = resultDict.objectForKey("amIfollowing")as? String!;
                if(followStatus! == "1")
                {
                    folloStatus = "1"
                }
                else{
                    folloStatus = "0"
                }
                cell?.selectionStyle = UITableViewCellSelectionStyle.None
                cell?.valuesForControls((resultDict.objectForKey("avatar")as? String!)!, name: (resultDict.objectForKey("username")as? String!)!, followState: folloStatus, trophyStr: (resultDict.objectForKey("buytrophy")as? String!)!,isFromHome: true, profileImgTag : indexPath.row)
                cell?.ShowOrHideTrophyLbl(true)
                cell?.isFromHome = false
                cell?.imgDelegate = self
                cell?.m_followBtn.tag = indexPath.row
                cell?.m_followBtn.setBackgroundImage(GRAPHICS.APP_PINK_BUTTON_IAMGE(), forState: .Normal)
                cell?.m_followBtn.titleLabel?.font = GRAPHICS.FONT_REGULAR(12)
                cell?.m_followBtn.addTarget(self, action: #selector(IBContactsViewController.followBtnTarget(_:)), forControlEvents: .TouchUpInside)
                
                cell?.m_profileImgView.tag = indexPath.row
                
                //                let profileTapGesture = UITapGestureRecognizer(target: self, action:#selector(IBContactsViewController.profileImgTapGesture(_:)))
                //                profileTapGesture.numberOfTapsRequired = 1
                //                cell?.m_profileImgView.addGestureRecognizer(profileTapGesture)
                
                
                
                
            }
            return cell!;
            
        }
    }
    func followBtnTarget(sender:UIButton)
    {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let userDetailUD = NSUserDefaults()
        let userIdStr = userDetailUD.objectForKey("user_id") as! String;
        let resultDict = m_searchArray.objectAtIndex(sender.tag)as! NSDictionary
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_follow(userIdStr, followerId: resultDict.objectForKey("userId") as! String)
        }
    }
    // api call back for follow
    func API_CALLBACK_follow(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            self.callServiceForSearch()
            //m_searchTblView.reloadData()
        }
        else{
            showAlertViewWithMessage((resultDict.objectForKey("msg")as? String!)!)
        }
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if btnIndex == 0
        {
            
            
            
        }
        else if btnIndex == 1
        {
            let inboxEntity = inboxArray.objectAtIndex(indexPath.row) as! IBInboxEntity
            //            let chatDetailsArray = chatDict.objectForKey("chatdetails") as! NSArray
            //            let chatDetailsDict = chatDetailsArray.objectAtIndex(chatDetailsArray.count - 1) as! NSDictionary
            let toId = inboxEntity.msgTo//chatDetailsDict.objectForKey("msgTo") as! String
            let fromId = inboxEntity.msgFrom//chatDetailsDict.objectForKey("msgFrom") as! String
            let messageVC = IBMessageViewController()
            if inboxEntity.productId == ""
            {
                messageVC.isForProductDiscussion = false
            }
            else
            {
                messageVC.isForProductDiscussion = true
            }
            if fromId == getUserIdFromUserDefaults()
            {
                messageVC.toUserId =  toId
                messageVC.nameStr = inboxEntity.ToUsername//chatDetailsDict.objectForKey("ToUsername") as! String
            }
            else
            {
                messageVC.toUserId =  fromId
                messageVC.nameStr = inboxEntity.FromUsername//chatDetailsDict.objectForKey("FromUsername") as! String
            }
            messageVC.productId = inboxEntity.productId
            self.navigationController?.pushViewController(messageVC, animated: true)
        }
        else
        {
           
            productStr = (productArray.objectAtIndex(indexPath.row) as? String)!
            amountStr = "$ 20"
            

        }
    }
    //MARK:- contacts cell delegate
    func diaryBtnAction(sender : UIButton)
    {
        let contactDict = contactArray.objectAtIndex(sender.tag/10) as? NSDictionary
        let calenderScreen = IBMydiaryCalendarViewController()
        calenderScreen.userIdStr = (contactDict?.objectForKey("userId") as? String)!
        self.navigationController?.pushViewController(calenderScreen, animated: true)
    }
    
    func msgBtnAction(sender : UIButton)
    {
        let contactDict = contactArray.objectAtIndex(sender.tag) as? NSDictionary
        let messageVC = IBMessageViewController()
        messageVC.toUserId = (contactDict?.objectForKey("userId") as? String)!
        messageVC.chatId = ""
        messageVC.nameStr = (contactDict?.objectForKey("username") as? String)!
        self.navigationController?.pushViewController(messageVC, animated: true)
    }
    //MARK: - inbox delegate
    func profileImageGesture(tapGesture : UITapGestureRecognizer)
    {
        
        
        let contactEntity = inboxArray.objectAtIndex((tapGesture.view?.tag)!) as? IBInboxEntity
        let myPageVc = IBMyPageViewController()
        if contactEntity?.msgFrom == getUserIdFromUserDefaults()
        {
            myPageVc.otherUserId = (contactEntity?.msgTo)!
        }
        else{
            myPageVc.otherUserId = (contactEntity?.msgFrom)!
        }
        self.navigationController?.pushViewController(myPageVc, animated: true)
        
    }
    func productImageGesture(tapGesture : UITapGestureRecognizer)
    {
        let inboxEntity = inboxArray.objectAtIndex((tapGesture.view?.tag)! - 10) as! IBInboxEntity
        let productDetailsVC = IBProductDetailsViewController()
        productDetailsVC.productId = inboxEntity.productId
        self.navigationController?.pushViewController(productDetailsVC, animated: true)
        
    }
    
    
    func tapOnProfileImage(tapGesture : UITapGestureRecognizer)
    {
        

        let resultDict = m_searchArray.objectAtIndex((tapGesture.view?.tag)!) as! NSDictionary // deepika to fix the issue in searching contact.
        let userId = resultDict.objectForKey("userId") as! String
        
        let myPageVc = IBMyPageViewController()
        myPageVc.otherUserId = userId
        self.navigationController?.pushViewController(myPageVc, animated: true)
    }
    
    //MARK:- mgswipe tablew cell
    func swipeTableCell(cell: MGSwipeTableCell!, canSwipe direction: MGSwipeDirection) -> Bool {
        return true
    }
    func swipeTableCell(cell: MGSwipeTableCell!, swipeButtonsForDirection direction: MGSwipeDirection, swipeSettings: MGSwipeSettings!, expansionSettings: MGSwipeExpansionSettings!) -> [AnyObject]! {
        
        swipeSettings.transition = .Border;
        expansionSettings.buttonIndex = 0;
        //  __weak MSSendNotificationViewController * me = self;
        
        //let   me =  IBListOfCartsViewController()
        //    MSNotificationEntity *entity = [m_notificationsArray objectAtIndex:[m_notificationTableView indexPathForCell:cell].row];
        
        if (direction == .LeftToRight) {
            
        }
        else {
            
            expansionSettings.fillOnTrigger = true;
            expansionSettings.threshold = 1.1;
            // Add a remove button to the cell
            let removeButton = MGSwipeButton(title: "Remove", backgroundColor: UIColor.redColor(), callback: {
                (sender: MGSwipeTableCell!) -> Bool in
                //print(self.inboxArray)
                let indexPath = self.contactsTblView.indexPathForCell(sender);
                self.deleteAction(indexPath!)
                return true
            })
            cell?.rightButtons = [removeButton]
            return [removeButton];
        }
        
        return nil
    }
    func swipeTableCell(cell: MGSwipeTableCell!, didChangeSwipeState state: MGSwipeState, gestureIsActive: Bool) {
        
    }
    func deleteAction(indexPath : NSIndexPath)
    {
        //print(indexPath.row)
        //print(self.inboxArray)
        //m_tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
        inboxArray.removeObjectAtIndex(indexPath.row)
        contactsTblView.reloadData()
        
    }
    
    //MARK: - textfield delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidChange(textField: UITextField) -> () {
        btnIndex = 2
        callServiceForSearch()
    }
    
    //MARK: - textview delegate
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        msgTextView.text = ""
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.m_mainView.frame.origin.y =  -265
            }, completion: nil)
        textView.inputAccessoryView = self.createKeyboardToolBar()
        return true
    }
    func textViewDidEndEditing(textView: UITextView) {
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.m_mainView.frame.origin.y =  0
            }, completion: nil)
        
        msgstring = msgTextView.text!;
    }
    func textViewDidChange(textView: UITextView) {
        msgstring = msgTextView.text!;
    }
    func createKeyboardToolBar() -> UIToolbar
    {
        if(keyBoardTool != nil)
        {
            keyBoardTool .removeFromSuperview()
            keyBoardTool = nil
        }
        keyBoardTool = UIToolbar(frame:CGRectMake(0, GRAPHICS.Screen_Height() - 266, GRAPHICS.Screen_Width(), 50.0))
        keyBoardTool!.barStyle = UIBarStyle.BlackOpaque
        keyBoardTool!.translucent = true
        keyBoardTool!.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        keyBoardTool!.sizeToFit()
        
        
        // let doneButton = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("nextAction"))
        
        let cancel_Btn = UIButton ()
        cancel_Btn.frame = CGRectMake(10, 0, 70, 50)
        cancel_Btn.backgroundColor =  UIColor.clearColor();
        cancel_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        cancel_Btn.setTitle("Cancel", forState: UIControlState.Normal)
        cancel_Btn.addTarget(self, action: #selector(IBContactsViewController.cancelAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(cancel_Btn)
        
        
        let done_Btn = UIButton ()
        done_Btn.frame = CGRectMake(CGRectGetWidth(self.view.frame)-80, 0, 70, 50)
        done_Btn.backgroundColor =  UIColor.clearColor();
        done_Btn.layer.cornerRadius = 7
        done_Btn .setTitleColor(UIColor.init(colorLiteralRed: 230.0/255.0, green: 0, blue: 73.0/255.0, alpha: 1.0), forState: UIControlState.Normal)
        done_Btn.setTitle("Done", forState: UIControlState.Normal)
        done_Btn.addTarget(self, action: #selector(IBContactsViewController.doneAction), forControlEvents: UIControlEvents.TouchUpInside)
        keyBoardTool.addSubview(done_Btn)
        
        keyBoardTool!.userInteractionEnabled = true
        return keyBoardTool
        
    }
    func cancelAction()
    {
        if searchTextField.isFirstResponder()
        {
            searchTextField.text = ""
            searchTextField.resignFirstResponder()
        }
        else if msgTextView.isFirstResponder()
        {
            msgTextView.text = Contacts_EnterMsgText
            msgTextView.resignFirstResponder()
        }
    }
    func doneAction()
    {
        if searchTextField.isFirstResponder()
        {
            searchTextField.resignFirstResponder()
        }
        else if msgTextView.isFirstResponder()
        {
            msgTextView.resignFirstResponder()
        }
    }
    func segmentBtnAction(sender : UIButton)
    {
        btnIndex = sender.tag
        for button in self.m_bgImageView.subviews {
            if button .isKindOfClass(UIButton.self) {
                let buttn = button as! UIButton
                buttn.selected = false
            }
        }
        sender.selected = true
        if  sender.tag == 1
        {
            btnIndex = 1
        }
        else{
            btnIndex = 0
            
        }
        //        sender.selected = true
        //        if  btnIndex == 1
        //        {
        //            //msgBtnIndex = 0
        //            contactsTblView.frame.size.height = self.m_bgImageView.frame.size.height - searchView.frame.size.height  - sender.frame.size.height
        //            msgTextView.hidden = true
        //            sendButton.hidden = true
        ////            if msgBtnIndex == 0
        ////            {
        ////                msgBtnIndex = 1
        ////                contactsTblView.frame.size.height = contactsTblView.frame.size.height - msgTextView.frame.size.height
        ////                //print(contactsTblView.frame.size.height)
        ////                msgTextView.frame.origin.y = contactsTblView.frame.maxY
        ////                sendButton.frame.origin.y = msgTextView.frame.origin.y
        ////                msgTextView.hidden = false
        ////                sendButton.hidden = false
        ////            }
        ////            else
        ////            {
        ////
        ////            }
        //        }
        //        else
        //        {
        //
        //        }
        contactsTblView.reloadData()
    }
    func createViewForMessageSection()
    {
        
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    func sendButtonAction(sender : UIButton)
    {
        msgTextView.resignFirstResponder()
        if msgTextView.text != "" && msgTextView.text != Contacts_EnterMsgText
        {
            msgArray.addObject(msgTextView.text)
            
        }
        else
        {
            showAlertViewWithMessage("Please Enter Message")
        }
        contactsTblView.reloadData()
        contactsTblView.setContentOffset(CGPointMake(0, (contactsTblView.contentSize.height + 20) - contactsTblView.frame.size.height), animated: true)
        msgTextView.text = Contacts_EnterMsgText
    }
    func addDividerImage(view : UIView)
    {
        let dividerImg = GRAPHICS.ACTIVITY_DIVIDER_IMAGE()
        let contentSize = GRAPHICS.getImageWidthHeight(dividerImg)
        let width = contentSize.width
        let height = view.frame.height//contentSize.height
        let dividerImgView = UIImageView(frame : CGRectMake(view.bounds.origin.x, (view.frame.size.height - height)/2, width, height))
        dividerImgView.image = dividerImg
        view.addSubview(dividerImgView)
    }
    
    // MARK: Api delegate
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
    // api call back for get all users
    func API_CALLBACK_getAllUsers(resultDict: NSDictionary)
    {
        //SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            let searchLocalArr = (resultDict.objectForKey("result")!.mutableCopy() as! NSArray)
            let localArray = NSMutableArray()
            for i in 0  ..< searchLocalArr.count
            {
                let userDict = searchLocalArr.objectAtIndex(i) as! NSDictionary
                let userId = userDict.objectForKey("userId") as! String
                if (userId != getUserIdFromUserDefaults())
                {
                    
                    localArray.addObject(userDict)
                    let searchPredicate = NSPredicate(format: "username BEGINSWITH[cd] %@", searchTextField.text as String!)
                    let array = localArray.filteredArrayUsingPredicate(searchPredicate)
                    //print(array)
                    m_searchArray .removeAllObjects()
                    m_searchArray .addObjectsFromArray(array)
                    contactsTblView.reloadData()
                    
                }
                
            }
            //m_searchArray = (resultDict.objectForKey("result")!.mutableCopy() as! NSMutableArray)
            //            //print(m_searchArray)
            //            //print(m_searchArray.count)
            //m_searchTblView.reloadData()
        }
        else{
            GRAPHICS.showAlert(resultDict.objectForKey("msg")as? String!)
        }
    }
    // api call back for get all users
    func API_CALLBACK_getFollowingForUser(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            if let contctary = resultDict.objectForKey("result") as? NSArray
            {
                if contctary.count > 0
                {
                    for i in 0 ..< contctary.count
                    {
                      let dict = contctary.objectAtIndex(i) as! NSDictionary
                      contactArray.addObject(dict)
                        
                    }
                }
            }
            contactsTblView.reloadData()
        }
        else
        {
            GRAPHICS.showAlert(resultDict.objectForKey("msg")as? String!)
        }
    }
    func API_CALLBACK_getUserInbox(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        let errorCode = resultDict.objectForKey("error_code")as? String!
        if errorCode == "1"
        {
            inboxArray.removeAllObjects()
            inboxDict = resultDict.objectForKey("result") as! NSDictionary
            let chatArray = inboxDict.objectForKey("chat") as! NSArray
            
            for i in 0 ..< chatArray.count
            {
                let dict = chatArray.objectAtIndex(i) as! NSDictionary
                let chatUserArray = dict.objectForKey("chatdetails") as! NSArray
                let chatDict = chatUserArray.objectAtIndex(0) as! NSDictionary
                let inboxEntity = IBInboxEntity(dict: NSDictionary(dictionary: chatDict))
                inboxArray.addObject(inboxEntity)
            }
            
            let discussionArray = inboxDict.objectForKey("discussion_chat") as! NSArray
            for i in 0 ..< discussionArray.count
            {
                let dict = discussionArray.objectAtIndex(i) as! NSDictionary
                let discussionDetArray = dict.objectForKey("discussion_details") as! NSArray
                let discussionDict = discussionDetArray.objectAtIndex(0) as! NSDictionary
                let inboxEntity = IBInboxEntity(dict: NSDictionary(dictionary: discussionDict))
                inboxArray.addObject(inboxEntity)
            }
            //print(inboxArray)
            
            let sortedArray = inboxArray.sortedArrayUsingComparator {
                (obj1, obj2) -> NSComparisonResult in
                
                let p1 = obj1 as! IBInboxEntity
                let p2 = obj2 as! IBInboxEntity
                let result = p2.addedDate!.compare(p1.addedDate)
                return result
            }
            
            inboxArray.removeAllObjects()
            inboxArray.addObjectsFromArray(sortedArray)
            
            contactsTblView.reloadData()
        }
        else
        {
            GRAPHICS.showAlert(resultDict.objectForKey("msg")as? String!)
        }
        
    }
    
}
