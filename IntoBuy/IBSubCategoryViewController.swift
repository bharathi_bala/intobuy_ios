//
//  IBSubCategoryViewController.swift
//  IntoBuy
//
//  Created by SmaatApps on 23/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBSubCategoryViewController: IBBaseViewController,UICollectionViewDataSource,UICollectionViewDelegate,ServerAPIDelegate
{
    var m_catID : String = String()
    
   var m_categoryCollView:UICollectionView!
    
    var m_subCategoriesArray : NSMutableArray = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //print("catID is /%@",m_catID)
        self.hideSettingsBtn(true)
        self.m_titleLabel.text = "Sub Categories"
        self.initControls()
        self.callSubCategorysAPI()
        
    }
    
    func callSubCategorysAPI()
    {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        
        let serverApi = ServerAPI()
        serverApi.delegate = self
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
           serverApi.API_getActiveSubCategories(self.m_catID)
        })
    }
    func API_CALLBACK_getActiveSubCategories(resultDict: NSDictionary)
    {
        NSLog("active subcategories");
        SwiftLoader.hide()
        
        if resultDict.objectForKey("error_code") as? String == "1"
        {
            let array1 : NSArray = (resultDict.objectForKey("result") as? NSArray)!
            m_subCategoriesArray = NSMutableArray()
            for i in 0 ..< array1.count
            {
                
                m_subCategoriesArray.addObject(array1.objectAtIndex(i))
               
                
            }
            //print("subcategories list Array/%@",m_subCategoriesArray)
            if m_subCategoriesArray.count == 0
            {
               showAlertViewWithMessage(Noresults_found_text)
                
            }
            
        }
        m_categoryCollView.reloadData()
    
    
    }
    
    // MARK: Api delegate
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        //        m_forgotBgView.removeFromSuperview()
        showAlertViewWithMessage(errorMessage);
    }
    
    func initControls()
    {
        let categorylayOut = UICollectionViewFlowLayout()
        categorylayOut.itemSize =  CGSizeMake((GRAPHICS.Screen_Width()-20)/2, (GRAPHICS.Screen_Width()-20)/2)
        categorylayOut.scrollDirection = .Vertical
        categorylayOut.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
        
        m_categoryCollView = UICollectionView(frame: CGRectMake(0,0,GRAPHICS.Screen_Width(),GRAPHICS.Screen_Height()-60-40), collectionViewLayout: categorylayOut)
        m_categoryCollView.backgroundColor = UIColor.clearColor()
        m_categoryCollView.scrollEnabled = true
        m_categoryCollView.bounces = true
        m_categoryCollView.registerClass(IBCategoryCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "cellIdentifier")
        m_categoryCollView.delegate = self
        m_categoryCollView.dataSource = self
        self.m_bgImageView.addSubview(m_categoryCollView)
        // m_catScrlView.contentSize = CGSizeMake(m_catScrlView.frame.size.width,m_categoryCollView.frame.maxY + 10)
        

    
    }
    
    //MARK: - collection view delegates
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
      return m_subCategoriesArray.count
    
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
            let cell:IBCategoryCollectionViewCell? = collectionView.dequeueReusableCellWithReuseIdentifier("cellIdentifier", forIndexPath: indexPath) as? IBCategoryCollectionViewCell
        
        
            let dict = m_subCategoriesArray.objectAtIndex(indexPath.item)as? NSDictionary
            let imageUrlStr = dict?.objectForKey("catimage") as? String!
            let catName = dict?.objectForKey("catname") as? String!
            cell?.m_categoryImgView.load(imageUrlStr!)
            cell?.m_categoryLbl.text = catName
            
            //collectionView.frame.size.height = collectionView.frame.size.height * CGFloat(m_categoryArray.count)
                      return cell!
        }
   
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        let dict = m_subCategoriesArray.objectAtIndex(indexPath.item)as? NSDictionary
        let catId = dict!.objectForKey("subCatId") as! String
        let productVC = IBProductsViewController()
        productVC.categoryId = catId
        self.navigationController?.pushViewController(productVC, animated: true)
    }

  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
