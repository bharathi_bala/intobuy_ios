//
//  IBSearchFreindsViewController.swift
//  IntoBuy
//
//  Created by SmaatApps on 18/08/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBSearchFreindsViewController: IBBaseViewController,UITableViewDataSource,UITableViewDelegate,ServerAPIDelegate
{

    var m_searchTableViw:UITableView!
    var m_serachArry = NSMutableArray()
    let cellheight = CGFloat(60)
    var strFollowerID = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.m_titleLabel.text = SearchFriends
        self.createTableView()
        

        // Do any additional setup after loading the view.
    }
    func createTableView()
    {
        m_searchTableViw = UITableView(frame: CGRectMake(GRAPHICS.Screen_X(),0,GRAPHICS.Screen_Width(),self.m_bgImageView.frame.size.height))
        m_searchTableViw.backgroundColor = UIColor.clearColor()
        m_searchTableViw.delegate = self
        m_searchTableViw.dataSource = self
        m_searchTableViw.separatorStyle = .None
        self.m_bgImageView.addSubview(m_searchTableViw)
    
    }
    
    //MARK:- tableView delegates
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
       return cellheight
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return m_serachArry.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let entity = m_serachArry.objectAtIndex(indexPath.row) as! IBActivityFollowingEntity
        
        let cellIdentifier = "cell Identifier \(indexPath.row)"
        
            var cell:IBActivitySearchCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? IBActivitySearchCell
            if (cell == nil) {
                cell = IBActivitySearchCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
            }
        cell?.setControls(entity, indexpath: indexPath, delegate: self)
        
        cell?.friendBtn.tag = indexPath.row
        cell?.friendBtn.addTarget(self, action: #selector(IBSearchFreindsViewController.friendButtonAction(_:)), forControlEvents: .TouchUpInside)
            return cell!
        
    }
    func friendButtonAction(sender:UIButton)
    {
        let entity = m_serachArry.objectAtIndex(sender.tag) as! IBActivityFollowingEntity
        
        if entity.stramIFollowing as String != "1"
        {
        SwiftLoader.show(animated: true)
        SwiftLoader.show(title:"Loading...", animated:true)
        strFollowerID = entity.strUserID as String
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) { () -> Void in
            let serverApi = ServerAPI()
            serverApi.delegate = self
            serverApi.API_follow(getUserIdFromUserDefaults(), followerId: entity.strUserID as String)
        }
        }
    
    }
    func API_CALLBACK_follow(resultDict: NSDictionary)
    {
        SwiftLoader.hide()
        
        for i in 0 ..< m_serachArry.count
        {
            let entity = m_serachArry.objectAtIndex(i) as! IBActivityFollowingEntity
            
            if strFollowerID != "" && (strFollowerID == entity.strUserID as String)
            {
               entity.stramIFollowing = "1"
               m_serachArry.replaceObjectAtIndex(i, withObject: entity)
               m_searchTableViw.reloadData()
            }
            
        }
    }
    
    func profilePicTapped(recogniser:UIGestureRecognizer)
    {
       let entity = m_serachArry.objectAtIndex((recogniser.view?.tag)!) as! IBActivityFollowingEntity
        
        let myPageVC = IBMyPageViewController()
        myPageVC.m_isFromCameraBtn = false
        myPageVC.otherUserId = (entity.strUserID as String)
        self.navigationController?.pushViewController(myPageVC, animated: true)
    
    }
    //MARK:- server API delegates
    func API_CALLBACK_Error(errorNumber:Int,errorMessage:String)
    {
        SwiftLoader.hide()
        showAlertViewWithMessage(errorMessage)
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
