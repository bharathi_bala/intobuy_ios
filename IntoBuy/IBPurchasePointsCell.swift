//
//  IBPurchasePointsCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 28/01/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBPurchasePointsCell: UITableViewCell {
    
    var dividerImgView:UIImageView!
    var m_titleLbl:UILabel!
    var m_valueLbl:UILabel!
    var m_arrowBtn:UIButton!
    var m_amountBtn:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControls()
    }
    
    func createControls()
    {
        self.frame.size.width = GRAPHICS.Screen_Width()
        
        var xPos = GRAPHICS.Screen_X() + 10
        var height = self.frame.size.height - 20
        var width = GRAPHICS.Screen_Width()/2
        var yPos = (self.frame.size.height - height)/2
        
        m_titleLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_titleLbl.font = GRAPHICS.FONT_REGULAR(12)
       // m_titleLbl.text = "Points Balance"
        self.addSubview(m_titleLbl)
        
        width = GRAPHICS.Screen_Width()/3
        xPos = self.frame.size.width - width - 10
        
        m_valueLbl = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_valueLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_valueLbl.textAlignment = .Right
       // m_valueLbl.text = "23 coins"
        self.addSubview(m_valueLbl)
        
        
        let arrowImg = GRAPHICS.SETTINGS_ARROW_BTN_IMAGE()
        let arrowSel = GRAPHICS.SETTINGS_DOWN_ARROW_IMAGE()
        width = arrowImg.size.width/2
        height = arrowImg.size.height/2
        yPos = (self.frame.size.height - height)/2
        xPos = self.frame.size.width - width
        
        m_arrowBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        m_arrowBtn.setBackgroundImage(arrowImg, forState: .Normal)
        m_arrowBtn.setBackgroundImage(arrowSel, forState: .Selected)
        self.addSubview(m_arrowBtn)
        
        let amountImage = GRAPHICS.SETTINGS_AMOUNT_BUTTON_IMAGE()
        width = amountImage.size.width/2
        height = amountImage.size.height/2
        yPos = (self.frame.size.height - height)/2
        xPos = self.frame.size.width - width - 10
        m_amountBtn = UIButton(frame: CGRectMake(xPos,yPos,width,height))
        m_amountBtn.setBackgroundImage(amountImage, forState: .Normal)
        m_amountBtn.titleLabel!.font = GRAPHICS.FONT_REGULAR(12)
        m_amountBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
        self.addSubview(m_amountBtn)
        
        let dividerImg = GRAPHICS.SEARCH_DIVIDER_IMAGE()
        width = GRAPHICS.Screen_Width() - 20
        height = dividerImg.size.height/2
        xPos = GRAPHICS.Screen_X() + 10
        yPos = (self.frame.size.height - height)
        dividerImgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        dividerImgView.image = dividerImg
        self.addSubview(dividerImgView)
    }
    func showOrHideDivider(toHideLine : Bool)
    {
        if toHideLine == true
        {
            dividerImgView.hidden = true
        }
        else{
            dividerImgView.hidden = false
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
