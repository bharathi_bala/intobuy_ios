//
//  ChatCustomCell.swift
//  MeetIntro
//
//  Created by DHEENA on 25/11/15.
//  Copyright © 2015 Smaat Apps and Technologies. All rights reserved.
//

import Foundation
import UIKit
import UIKit


var cellBool : String = String()

class IBChatCustomCell: UITableViewCell {

    var profilePicImgView = UIImageView()
    var m_Message_Label = UILabel()
    var m_Date_Label = UILabel()
    var m_Divider_view = UIView()
    var m_arrowImageView = UIImageView()
    var m_MessagelabelBg_view = UIView()
    
     var m_ChatMessageReceivedTime_Label = UILabel()
   

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.clearColor()
        
        
        profilePicImgView = UIImageView(frame: CGRectZero)
        profilePicImgView.backgroundColor = UIColor.clearColor()
        
        //profilePicImgView.image = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
        self.addSubview(profilePicImgView)
        
        m_MessagelabelBg_view = UIView(frame: CGRectZero)
        m_MessagelabelBg_view.backgroundColor = UIColor.clearColor()
        self.addSubview(m_MessagelabelBg_view)
        
//        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: Selector("buttonTapped:"))
//        m_MessagelabelBg_view.addGestureRecognizer(longPressRecognizer)
        
        
        
        
        m_Message_Label = UILabel(frame: CGRectZero)
        m_Message_Label.font = GRAPHICS.FONT_REGULAR(12)
        m_Message_Label.numberOfLines = 0
        m_Message_Label.layer.cornerRadius = 12
        m_Message_Label.clipsToBounds = true
        m_Message_Label.sizeToFit()
        m_Message_Label.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
        m_Message_Label.textAlignment = .Left
        m_Message_Label.lineBreakMode = NSLineBreakMode.ByClipping
//        m_Message_Label.backgroundColor = UIColor(red: 69.0/255, green: 129.0/255, blue: 223.0/255, alpha: 1)
        m_Message_Label.textColor = UIColor.whiteColor()
        m_MessagelabelBg_view.addSubview(m_Message_Label)
        
        m_Date_Label = UILabel(frame: CGRectZero)
        m_Date_Label.font = GRAPHICS.FONT_REGULAR(10)
        m_Date_Label.numberOfLines = 1
        m_Date_Label.sizeToFit()
        m_Date_Label.textAlignment = .Left
        m_Date_Label.textColor = UIColor.blackColor()
        self.addSubview(m_Date_Label)

        m_arrowImageView = UIImageView(frame: CGRectZero)
        m_arrowImageView.backgroundColor = UIColor.clearColor()
        self.addSubview(m_arrowImageView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        ////print("called after bool")
     
    }
    
    func setControls(ReceiverString : String ,height : CGFloat,width : CGFloat ,msgText:String , profilePic : String, date : String)
    {
        m_Message_Label.text = msgText
        m_Date_Label.text =  convertUtcToLocalFormatForChat(date) //convert to local t ime
        
        if ReceiverString == "SomeOne"
        {
            
            var XPos : CGFloat = 10.0
            var  YPos : CGFloat = 5//height-50
            var Width : CGFloat = 70.0
            var Height : CGFloat = 30
            
            
            var contentSize:CGSize = GRAPHICS.getImageWidthHeight(GRAPHICS.SEARCH_PROFILE_MASK_IMAGE())
            Width = contentSize.width
            Height = contentSize.height
            profilePicImgView.frame = CGRectMake(XPos, YPos, Width, Height)
           // m_UserName_Label.text = "Adams"
             //m_UserImg_View.layer.cornerRadius = Width/2
            let profilePicUrl = NSURL(string: profilePic)
            profilePicImgView.load(profilePicUrl!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
            profilePicImgView.layer.cornerRadius = Height/2
            //m_UserName_Label.text = "James"
            profilePicImgView.clipsToBounds = true
            
            let bubbleImg = GRAPHICS.CONTACTS_GRAY_BUBBLE_IMAGE()
            contentSize = GRAPHICS.getImageWidthHeight(bubbleImg)
            Width = contentSize.width
            Height = contentSize.height
            XPos = profilePicImgView.frame.maxX + 5
            let bubbleImgView = UIImageView(frame : CGRectMake(XPos, YPos, Width, Height))
            bubbleImgView.image = bubbleImg
            self.addSubview(bubbleImgView)
            
            
            XPos = CGRectGetMaxX(bubbleImgView.frame) - 5
            YPos = 5
            Width = width + 8
            Height = height-30
            
            m_MessagelabelBg_view.frame = CGRectMake(XPos, YPos, Width, Height)
            m_MessagelabelBg_view.backgroundColor = UIColor.init(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)//UIColor.blueColor()//FontChatGreenColor
            m_Message_Label.frame = CGRectMake(4, 0, CGRectGetWidth(m_MessagelabelBg_view.frame)-8, CGRectGetHeight(m_MessagelabelBg_view.frame))
            
            //m_Message_Label.backgroundColor = UIColor.blueColor()
            
           // m_Message_Label.textColor = UIColorFromRGB(FontWhiteColor, alpha: 1)
            
            
            m_Date_Label.frame = CGRect(x: profilePicImgView.frame.maxX, y: m_MessagelabelBg_view.frame.maxY, width: 200, height: 25)
            m_Date_Label.textAlignment = .Left
            
            YPos = CGRectGetMaxY(m_MessagelabelBg_view.frame)
            Width = 100
            Height = 20.0
            
            if CGRectGetWidth(m_MessagelabelBg_view.frame) < 70
            {
                
                XPos = CGRectGetMinX(m_MessagelabelBg_view.frame)
                //m_ChatMessageReceivedTime_Label.textAlignment = NSTextAlignment.Left
                m_MessagelabelBg_view.layer.cornerRadius = 3

                
            }
            else
            {
                XPos = CGRectGetMinX(m_MessagelabelBg_view.frame)
               // m_ChatMessageReceivedTime_Label.textAlignment = NSTextAlignment.Left
                m_MessagelabelBg_view.layer.cornerRadius = 3
            }
            m_ChatMessageReceivedTime_Label.frame = CGRectMake(XPos, YPos, Width, Height)
            
            XPos = CGRectGetMinX(m_MessagelabelBg_view.frame)-15
            YPos = CGRectGetMaxY(m_MessagelabelBg_view.frame)-20
            Width = 15
            Height = 15
            
            m_arrowImageView.frame = CGRectMake(XPos, YPos, Width, Height)
           // m_arrowImageView.image = GRAPHICS.CHAT_GREEN_ICON_IMAGE()
//            if isNegotiateChat
//            {
//               
//                m_Message_Label.textAlignment = .Center
//                 m_Message_Label.font = GRAPHICS.FONT_REGULAR(16)
//            }
            }
            
        else
            
        {
            
            var XPos : CGFloat = GRAPHICS.Screen_Width()-50
            var  YPos : CGFloat = 5//height-50
            var Width : CGFloat = 70.0
            var Height : CGFloat = 25
            
            var contentSize:CGSize = GRAPHICS.getImageWidthHeight(GRAPHICS.SEARCH_PROFILE_MASK_IMAGE())
            Width = contentSize.width
            Height = contentSize.height
            XPos = GRAPHICS.Screen_Width() - Width - 10
            profilePicImgView.frame = CGRectMake(XPos, YPos, Width, Height)
            let profilePicUrl = NSURL(string: profilePic)
            profilePicImgView.load(profilePicUrl!, placeholder: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completionHandler: nil)
            profilePicImgView.layer.cornerRadius = Height/2
            //m_UserName_Label.text = "James"
            profilePicImgView.clipsToBounds = true
            
            let bubbleImg = GRAPHICS.CONTACTS_BLUE_BUBBLE_IMAGE()
            contentSize = GRAPHICS.getImageWidthHeight(bubbleImg)
            Width = contentSize.width
            Height = contentSize.height
            XPos = profilePicImgView.frame.origin.x - Width/2
            YPos = profilePicImgView.frame.origin.y
            let bubbleImgView = UIImageView(frame : CGRectMake(XPos, YPos, Width, Height))
            bubbleImgView.image = bubbleImg
            self.addSubview(bubbleImgView)

            
            
            Width = width + 8
            XPos = bubbleImgView.frame.origin.x - width
           // YPos = 5
            Height = height-30
            
            m_MessagelabelBg_view.frame = CGRectMake(XPos, YPos, Width, Height)
            m_MessagelabelBg_view.backgroundColor = UIColor.init(red: 109.0/255.0, green: 207.0/255.0, blue: 246.0/255.0, alpha: 1.0)
            m_Message_Label.frame = CGRectMake(4, 0, CGRectGetWidth(m_MessagelabelBg_view.frame)-8, CGRectGetHeight(m_MessagelabelBg_view.frame))
         //   m_Message_Label.backgroundColor = UIColor.redColor()
        //    m_Message_Label.textColor = UIColorFromRGB(FontWhiteColor, alpha: 1)

            
            m_Date_Label.frame = CGRect(x:(profilePicImgView.frame.origin.x - 200) , y: m_MessagelabelBg_view.frame.maxY, width: 200, height: 25)
            m_Date_Label.textAlignment = .Right
            
            
            YPos = CGRectGetMaxY(m_MessagelabelBg_view.frame)
            Width = 100
            Height = 20.0
            
            if CGRectGetWidth(m_MessagelabelBg_view.frame) < 70
            {
                XPos = CGRectGetMaxX(m_MessagelabelBg_view.frame)-Width
                
                m_MessagelabelBg_view.layer.cornerRadius = 3
            }
            else
            {
                XPos = CGRectGetMaxX(m_MessagelabelBg_view.frame)-Width
               
                m_MessagelabelBg_view.layer.cornerRadius = 3
                
            }
            
//               m_ChatMessageReceivedTime_Label.frame = CGRectMake(XPos, YPos, Width, Height)
//               m_ChatMessageReceivedTime_Label.textAlignment = NSTextAlignment.Right
            
            XPos = CGRectGetMaxX(m_MessagelabelBg_view.frame)
            YPos = CGRectGetMaxY(m_MessagelabelBg_view.frame)-20
            Width = 15
            Height = 15
            
            m_arrowImageView.frame = CGRectMake(XPos, YPos, Width, Height)
            //m_arrowImageView.image = GRAPHICS.CHAT_PURPLE_ICON_IMAGE()
//            if isNegotiateChat
//            {
//                
//                 m_Message_Label.textAlignment = .Center
//                m_Message_Label.font = GRAPHICS.FONT_REGULAR(16)
//            }
            
        }
        
        
    }
    
    
    
    
    
    
}
