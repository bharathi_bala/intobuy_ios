//
//  ServerAPI.swift
//  IntoBuy
//
//  Created by 53 on 27/10/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import Foundation

var STATUS_CODE_100 = "100"
var STATUS_CODE_101 = "101"
var STATUS_CODE_99 = "99"
var STATUS_CODE_1 = "1"

class ServerAPI : NSObject
{
    var delegate: ServerAPIDelegate?
    var baseURL:String = "http://50.87.171.216/intobuy/webservice/index.php/"
    var CountryList:String = "?action=getActiveCountry"
    var data: NSMutableData = NSMutableData()
    var isFromProfilePage:Bool!
    var isForProduct:Bool!
    var isForDiary:Bool!
    //Service Names
    
    var LogIn:String = "login"
    var Registration:String = "Register"
    var ForgotPassword:String = "forgotpassword"
    var getActiveCountry:String = "getActiveCountry"
    var forgotpassword:String = "forgotpassword"
    var getAllPlans:String = "getAllPlans"
    var purchaseMembership:String = "purchaseMembership"
    var getAllUsers = "getAllUsers"
    var login = "login"
    var logout = "logout"
    var follow = "follow"
    var getMyFeeds = "getMyFeeds"
    var addWallLike = "addWallLike"
    var getWallComments = "getWallComments"
    var addWallComment = "addWallComment"
    var shareWorldMap = "shareworldmap"
    var getActiveBanners = "getActiveBanners"
    var getActiveCategories = "getActiveCategories"
    var myaccount = "myaccount"
    var addFeed = "addfeed"
    var getActiveSubCatogory = "getActiveSubCategories"
    var deleteImageFromPost = "deleteImageFromPost"
    var Uploadprofilepic = "Uploadprofilepic"
    var ProductDetail = "Productdetails"
    var AddProduct = "addProduct"
    var GetUserprofile = "getUserprofile"
    var getFollowingForUser = "getFollowingForUser"
    var getFollowersForUser = "getFollowersForUser"
    var addProductToCart = "addProductToCart"
    var cartcount = "cartcount"
    var getProductsOnCart = "getProductsOnCart"
    var deleteProductFromCart = "deleteProductFromCart"
    var addDiaryFeed = "addDiaryFeed"
    var getDiary = "getDiary"
    var checkDiaryAccess = "checkDiaryAccess"
    var getMyPlan = "getMyPlan"
    var editShippingAddress = "editShippingAddress"
    var getShippingAddress = "getShippingAddress"
    var deleteproduct = "deleteproduct"
    var editProduct = "editProduct"
    var getrewardpointsplans = "getrewardpointsplans"
    var PlaceOrder = "PlaceOrder"
    var successOrder = "successOrder"
    var purchaseRewardPointPlan = "purchaseRewardPointPlan"
    var AddBillingDetails = "AddBillingDetails"
    var getBillingdetails = "getBillingdetails"
    var editCart = "editCart"
    var getOrders = "getOrders"
    var diarydelete = "diarydelete"
    var shareworldmap = "shareworldmap"
    var Shareproduct = "Shareproduct"
    var GetMessageConversion = "GetMessageConversion"
    var AddChatMessage = "AddChatMessage"
    var getUserInbox = "getUserInbox"
    var getProductDiscussion = "getProductDiscussion"
    var addMsgTodiscussion = "addMsgTodiscussion"
    var addWish = "addWish"
    var changepassword = "changepassword"
    var getMyWhishlist = "getMyWhishlist"
    var removeWish = "removeWish"
    var getProductByCategory = "getProductByCategory"
    var getMyNews = "getMyNews"
    var getNews = "getNews"
    var searchUSer = "searchuser"
    
    
    //MARK: -  REgistration
    func API_callRegisterWebService(Firstname : NSString ,LastName:NSString,Email:NSString,PhoneNo:NSString,password:NSString,DeviceId:NSString,Device:NSString,Gender:NSString,DOB:NSString,UserName:NSString,Country:NSString,AboutMe:NSString,UserType:NSString,FaceBookId:NSString)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["firstName":Firstname,"lastName":LastName,"emailId":Email,"phoneNumber":PhoneNo,"password":password,"device_id":DeviceId,"device":Device,"gender":Gender,"DOB":DOB,"username":UserName,"country":Country,"aboutme":AboutMe,"userType":UserType,"facebookId":FaceBookId]
            
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,Registration])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_SignUp!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    func API_getActiveCountry(UserId : NSString)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":UserId]
            
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getActiveCountry])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getActiveCountry!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    func API_forgotpassword(Email : NSString)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["email":Email]
            
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,forgotpassword])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_forgotpassword!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    func API_getAllPlans()
    {
        if Reachability.isConnectedToNetwork() == true
        {
            //  let parameters = [""]
            
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getAllPlans])
            
            request(.POST, urlString,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getAllPlans!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    
    func API_purchaseMembership(userId : NSString ,planId:NSString ,transactionId:NSString)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"planId":planId,"transactionId":transactionId]
            
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,purchaseMembership])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_purchaseMembership!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    func API_login(emailId : NSString ,password : NSString , device_id : NSString ,device : NSString)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["emailId":emailId,"password":password,"device_id":device_id,"device":device]
            
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,login])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_login!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    func API_logout(UserId : NSString)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":UserId]
            
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,logout])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_logout!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    func API_getAllUsers(UserId : NSString)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":UserId]
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getAllUsers])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getAllUsers!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    func API_follow(UserId : NSString, followerId : NSString)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":UserId, "followerId":followerId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,follow])
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_follow!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    func API_getMyFeeds(UserId : NSString)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":UserId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getMyFeeds])
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getMyFeeds!(JSON as! NSDictionary)
                    }
                    else
                    {
                        
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    // api for wall like
    func API_addWallLike(wall_id: String,  userId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId ,"wall_id":wall_id]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,addWallLike])
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_addWallLike!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    // API for getting comments
    func API_getWallComments(wall_id: String,  userId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId ,"wall_id":wall_id]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getWallComments])
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getWallComments!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    
    
    // API for all wall comments
    func API_addWallComment(wall_id: String,  userId:String ,comment:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId ,"wall_id":wall_id,"comment":comment]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,addWallComment])
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_addWallComment!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    
    func API_ShareWorldMap()
    {
        if Reachability.isConnectedToNetwork() == true
        {
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,shareWorldMap])
            
            request(.POST, urlString, parameters: nil,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_ShareWorldMap!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    func API_getActiveBanners(userId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getActiveBanners])
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getActiveBanners!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    func API_getActiveCategories()
    {
        if Reachability.isConnectedToNetwork() == true
        {
            //let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getActiveCategories])
            request(.POST, urlString,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getActiveCategories!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    func API_myaccount(userId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,myaccount])
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_myaccount!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    
    func API_getActiveSubCategories(catID:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["catId":catID]
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getActiveSubCatogory])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getActiveSubCategories!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:"Invalid Response from Server")
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    
    func API_addFeed(wall_id: String,  userId:String ,post_heading:String,post_text:String,comment:String,var postrImage1:NSData!,var postrImage2:NSData!, var postrImage3:NSData!,var postrImage4:NSData! ,wall_status:String)
    {
        isFromProfilePage = false
        isForProduct = false
        isForDiary = false
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId ,"wall_type":wall_id,"post_heading":post_heading,"post_text":post_text,"comment":comment,"wall_status":wall_status]
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,addFeed])
           
        
            if(postrImage1!.length == 0)
            {
                postrImage1 = NSData()
            }
            if(postrImage2!.length == 0)
            {
                postrImage2 = NSData()
            }
            if(postrImage3!.length == 0)
            {
                postrImage3 = NSData()
            }
            if(postrImage4!.length == 0)
            {
                postrImage4 = NSData()
            }

            
            
            let urlRequest = urlRequestWithComponents(urlString, parameters:parameters, postImage1Data: postrImage1!,postImage2Data:postrImage2!,postImage3Data:postrImage3!,postImage4Data: postrImage4!)
            
            
            upload(urlRequest.0, data: urlRequest.1) .progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                ////print(totalBytesWritten)
                }.responseJSON(completionHandler: { (response) -> Void in
                    //print("RESPONSE \(response)")
                    (response).result
                    if let JSON = (response).result.value
                    {
                        ////print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_AddFeed!(JSON as! NSDictionary)
                    }
                    else
                    {
                        ////print("A JSON parsing error occurred, here are the details: %@" ,response.result.error)
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
                })
        }
        else
        {
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    
    // this function creates the required URLRequestConvertible and NSData we need to use Alamofire.upload
    func urlRequestWithComponents(urlString:String, parameters:Dictionary<String, String>, postImage1Data:NSData, postImage2Data:NSData,postImage3Data:NSData ,postImage4Data:NSData) -> (URLRequestConvertible, NSData)
    {
        
        // create url request to send
        let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        mutableURLRequest.HTTPMethod = Method.POST.rawValue
        let boundaryConstant = "myRandomBoundary12345";
        let contentType = "multipart/form-data;boundary="+boundaryConstant
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        // create upload data to send
        let uploadData = NSMutableData()

        if (!isFromProfilePage)
        {
            
            if(postImage1Data.length > 0)
            {
                
                // Post Image Data
                uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                if !isForProduct
                {
                    if !isForDiary
                    {
                        uploadData.appendData("Content-Disposition: form-data; name=\"postimage\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                    }
                    else{
                        uploadData.appendData("Content-Disposition: form-data; name=\"Pic\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                    }
                }
                else{
                    uploadData.appendData("Content-Disposition: form-data; name=\"productimage\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                }
                
                uploadData.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                uploadData.appendData(postImage1Data)
            }
            
            if(postImage2Data.length > 0)
            {
                // Post Image2 Data
                uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                if !isForProduct
                {
                    uploadData.appendData("Content-Disposition: form-data; name=\"postimage2\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                }
                else{
                    uploadData.appendData("Content-Disposition: form-data; name=\"productimage2\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                }
                
                uploadData.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                uploadData.appendData(postImage2Data)
            }
            if(postImage3Data.length  > 0)
            {
                
                // Post Image3 Data
                uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                if !isForProduct
                {
                    uploadData.appendData("Content-Disposition: form-data; name=\"postimage3\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                }
                else{
                    uploadData.appendData("Content-Disposition: form-data; name=\"productimage3\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                }
                
                uploadData.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                uploadData.appendData(postImage3Data)
            }
            if(postImage4Data.length  > 0)
            {
                
                // Post Image3 Data
                uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                if !isForProduct
                {
                    uploadData.appendData("Content-Disposition: form-data; name=\"postimage4\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                }
                else{
                    uploadData.appendData("Content-Disposition: form-data; name=\"productimage4\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                }
                
                uploadData.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                uploadData.appendData(postImage4Data)
            }

        }
        else{
            
            // Post Image Data
            uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            uploadData.appendData("Content-Disposition: form-data; name=\"ProfilePic\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            uploadData.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            uploadData.appendData(postImage1Data)
        }
        
        // add parameters
        for (key, value) in parameters {
            uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            uploadData.appendData("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
        }
        uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        
        
        // return URLRequestConvertible and NSData
        return (ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0,uploadData)
        
    }
    //MARK: - deleteimageFrompost API
    func API_deleteImageFromPost(userId:String, wall_id:String, imagenameurl:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["wall_id": wall_id,"imagenameurl":imagenameurl,"userId":userId]
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,deleteImageFromPost])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_deleteImageFromPost!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK: - api for upload profile pic
    func API_Uploadprofilepic(userId: String , ProfilePic:NSData)
    {
        isFromProfilePage = true
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,Uploadprofilepic])
            
            let image1 = GRAPHICS.DEFAULT_CART_IMAGE()
            let imageData1 = UIImageJPEGRepresentation(image1,1.0);
            let image2 = GRAPHICS.DEFAULT_CART_IMAGE()
            let imageData2 = UIImageJPEGRepresentation(image2,1.0)
            
            let urlRequest = urlRequestWithComponents(urlString, parameters: parameters, postImage1Data: ProfilePic, postImage2Data:imageData1!,postImage3Data:imageData2!, postImage4Data:imageData2!)
            
            
            upload(urlRequest.0, data: urlRequest.1) .progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                ////print(totalBytesWritten)
                }.responseJSON(completionHandler: { (response) -> Void in
                    //print("RESPONSE \(response)")
                    (response).result
                    if let JSON = (response).result.value
                    {
                        ////print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_Uploadprofilepic!(JSON as! NSDictionary)
                    }
                    else
                    {
                        ////print("A JSON parsing error occurred, here are the details: %@" ,response.result.error)
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
                })

            
            
            
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    
    func API_ProductDetail(productId : String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["productId":productId]
            
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,ProductDetail])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_ProductDetail!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }

    }
    //@"http://maps.googleapis.com/maps/api/"
   // @"geocode/json?latlng=%@,%@&sensor=true"
    //MARK:- get address by lat long
    
    func API_getAddressFromGoogle(latitude : String, longitude : String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["latitude":latitude , "longitude" : longitude]
            
            
            let urlString:String = String(format: "http://maps.googleapis.com/maps/api/geocode/json?latlng=%@,%@&sensor=true",latitude,longitude)
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getAddress!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    func API_PlaceSuggestion(placeToSearch : NSMutableString)
    {
        if Reachability.isConnectedToNetwork() == true
        {
           // let parameters = ["placeToSearch":placeToSearch]
            
            
            let urlString:String = String(format: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&sensor=false&key=AIzaSyAi7WUkrDTcfHkf_art08i27-moEVGy1ug",placeToSearch)
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                let url = NSURL(string: urlString)
                let data:NSData = NSData(contentsOfURL: url!)!
                self.performSelectorOnMainThread(#selector(ServerAPI.receivedSuggestedPlace(_:)), withObject: data, waitUntilDone: true)
                
            })
        }

        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    
    func receivedSuggestedPlace(responseData : NSData)
    {
//        NSError* error;
//        @try {
//            NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: responseData  options: NSJSONReadingAllowFragments  error: &error];
//            NSMutableArray *suggestedPlaceArray = [[NSMutableArray alloc] init];
//            NSArray *mainArray = [JSON objectForKey:@"predictions"];
//            for (int index = 0; index < mainArray.count; index ++) {
//                NSDictionary *suggestedPalce = [mainArray objectAtIndex:index];
//                [suggestedPlaceArray addObject:[suggestedPalce objectForKey:@"description"]];
//            }
//            if ([objServerAPI.serverAPIDelegate respondsToSelector:@selector(API_CALLBACK_SuggestedPlace:)]) {
//                [objServerAPI->serverAPIDelegate API_CALLBACK_SuggestedPlace:suggestedPlaceArray];
//            }
//        }
//        @catch (NSException *exception){
//            NSLog(@"Error-%@",exception.description);
//            [objServerAPI->serverAPIDelegate API_CALLBACK_ServerError:kServerNotRespondingMessage];
//        }
        do
        {
            let JSON = try NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            let suggestedPlaceArray = NSMutableArray()
            let mainArray = JSON.objectForKey("predictions") as! NSArray
            for i in 0  ..< mainArray.count 
            {
                let suggestedPlaceDict = mainArray.objectAtIndex(i) as! NSDictionary
                suggestedPlaceArray.addObject(suggestedPlaceDict)
            }
            //print(suggestedPlaceArray)
            
            self.delegate?.API_CALLBACK_PlaceSuggestion!(suggestedPlaceArray)
        }
        catch
        {
            self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
        }
    }
    
    //MARK:- api for add product
    func API_addProduct(productEntity : IBAddProductEntity)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            isForProduct = true
            isFromProfilePage = false
            isForDiary = false
            let parameters = ["userId":productEntity.userId ,"categoryId":productEntity.categoryId,"subcategoryId":productEntity.subcategoryId,"productname":productEntity.productname,"description":productEntity.descriptionStr,"price":productEntity.price,"quantity":productEntity.quantity,"terms":productEntity.terms,"facebook":productEntity.facebook,"twitter":productEntity.twitter,"address":productEntity.address,"latitude":productEntity.latitude,"logitude":productEntity.longitude]
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,AddProduct])
            
            if(productEntity.productimage.length == 0)
            {
                productEntity.productimage = NSData()
            }
            if(productEntity.productimage2.length == 0)
            {
                productEntity.productimage2 = NSData()
            }
            if(productEntity.productimage3.length == 0)
            {
                productEntity.productimage3 = NSData()
            }
            if(productEntity.productimage4.length == 0)
            {
                productEntity.productimage4 = NSData()
            }
            
            
            let urlRequest = urlRequestWithComponents(urlString, parameters:parameters, postImage1Data: productEntity.productimage,postImage2Data:productEntity.productimage2,postImage3Data:productEntity.productimage3,postImage4Data: productEntity.productimage4)
            
            
            upload(urlRequest.0, data: urlRequest.1) .progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                ////print(totalBytesWritten)
                }.responseJSON(completionHandler: { (response) -> Void in
                    ////print("RESPONSE \(response)")
                    (response).result
                    if let JSON = (response).result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_AddProduct!(JSON as! NSDictionary)
                    }
                    else
                    {
                        ////print("A JSON parsing error occurred, here are the details: %@" ,response.result.error)
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
                })
        }
        else
        {
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- api for get user profile
    func API_getUserprofile(userId:String , myId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId , "myId":myId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,GetUserprofile])
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_GetUserprofile!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- api Following list
    func API_getFollowingForUser(UserId:String , myUserId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["UserId":UserId , "myUserId":myUserId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getFollowingForUser])
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getFollowingForUser!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- api Followers list
    func API_getFollowersForUser(UserId:String , myUserId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["UserId":UserId , "myUserId":myUserId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getFollowersForUser])
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getFollowersForUser!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- addProductToCart API
    func API_addProductToCart(userId:String , productId:String , sellerId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId , "productId":productId , "sellerId":sellerId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,addProductToCart])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_addProductToCart!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    
    //MARK:- cartcountt API
    func API_cartcount(userId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,cartcount])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_cartcount!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- getProductsOnCart
    func API_getProductsOnCart(userId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getProductsOnCart])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getProductsOnCart!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    
    //MARK:- deleteProductFromCart
    func API_deleteProductFromCart(userId:String , cartId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId ,"cartId":cartId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,deleteProductFromCart])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_deleteProductFromCart!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    
    //MARK:- Add diary feed
    func API_addDiaryFeed(userId:String , date:String , text:String, var postrImage1:NSData , var postrImage2:NSData , var postrImage3:NSData , var postrImage4:NSData )
    {
        isFromProfilePage = false
        isForProduct = false
        isForDiary = true
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId ,"date":date,"text":text]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,addDiaryFeed])
            
            if(postrImage1.length == 0)
            {
                postrImage1 = NSData()
            }
            if(postrImage2.length == 0)
            {
                postrImage2 = NSData()
            }
            if(postrImage3.length == 0)
            {
                postrImage3 = NSData()
            }
            if(postrImage4.length == 0)
            {
                postrImage4 = NSData()
            }
            
            let urlRequest = urlRequestWithComponents(urlString, parameters:parameters, postImage1Data: postrImage1,postImage2Data:postrImage2,postImage3Data:postrImage3,postImage4Data: postrImage4)
            
            upload(urlRequest.0, data: urlRequest.1) .progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                ////print(totalBytesWritten)
                }.responseJSON(completionHandler: { (response) -> Void in
                    //print("RESPONSE \(response)")
                    (response).result
                    if let JSON = (response).result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_addDiaryFeed!(JSON as! NSDictionary)
                    }
                    else
                    {
                        ////print("A JSON parsing error occurred, here are the details: %@" ,response.result.error)
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
                })
        }
        else
        {
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    
    //MARK:- get Diary
    func API_getDiary(userId:String , today:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId ,"today":today]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getDiary])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
            {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getDiary!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- check Diary access
    func API_checkDiaryAccess(fromUserId:String ,toUserId:String , today:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["fromUserId":fromUserId ,"toUserId":toUserId , "today":today]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,checkDiaryAccess])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_checkDiaryAccess!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }

    //MARK:- getMyPlan API
    func API_getMyPlan(userId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getMyPlan])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
            {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getMyPlan!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }

    }
    //MARK:- edit shipping address
    func API_editShippingAddress(userId:String,country:String,name:String,address1:String,address2:String,state:String,contact:String,zip:String,remark:String,city:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"country":country,"name":name,"address1":address1,"address2":address2,"state":state,"contact":contact,"zip":zip,"remark":remark,"city":city]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,editShippingAddress])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_editShippingAddress!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }

    }
    //MARK:- get Shipping Address
    func API_getShippingAddress(userId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getShippingAddress])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getShippingAddress!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    func API_deleteproduct(userId:String,productId:String)
    {
        //deleteproduct
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"productId":productId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,deleteproduct])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_deleteproduct!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }

    }
    //Edit Product
    func API_editProduct(productEntity : IBAddProductEntity , productId : String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            isForProduct = true
            isFromProfilePage = false
            isForDiary = false
            let parameters = ["userId":productEntity.userId ,"categoryId":productEntity.categoryId,"subcategoryId":productEntity.subcategoryId,"productname":productEntity.productname,"description":productEntity.descriptionStr,"price":productEntity.price,"quantity":productEntity.quantity,"terms":productEntity.terms,"facebook":productEntity.facebook,"twitter":productEntity.twitter,"address":productEntity.address , "productId":productId , "latitude":productEntity.latitude,"logitude":productEntity.longitude]
            
            let urlString:String = String(format: "%@%@", arguments: [baseURL,editProduct])
            
            if(productEntity.productimage.length == 0)
            {
                productEntity.productimage = NSData()
            }
            if(productEntity.productimage2.length == 0)
            {
                productEntity.productimage2 = NSData()
            }
            if(productEntity.productimage3.length == 0)
            {
                productEntity.productimage3 = NSData()
            }
            if(productEntity.productimage4.length == 0)
            {
                productEntity.productimage4 = NSData()
            }
            
            
            let urlRequest = urlRequestWithComponents(urlString, parameters:parameters, postImage1Data: productEntity.productimage,postImage2Data:productEntity.productimage2,postImage3Data:productEntity.productimage3,postImage4Data: productEntity.productimage4)
            
            
            upload(urlRequest.0, data: urlRequest.1) .progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                ////print(totalBytesWritten)
                }.responseJSON(completionHandler: { (response) -> Void in
                    ////print("RESPONSE \(response)")
                    (response).result
                    if let JSON = (response).result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_editProduct!(JSON as! NSDictionary)
                    }
                    else
                    {
                        ////print("A JSON parsing error occurred, here are the details: %@" ,response.result.error)
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
                })
        }
        else
        {
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARk:- get reward points API
    func API_getrewardpointsplans(userId : String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameter = ["userId" : userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getrewardpointsplans])
            
            request(.POST, urlString,parameters: parameter, encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getrewardpointsplans!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
 
    }
    //MARK:- placeOrder API
    func API_PlaceOrder(productId : String,userId : String,sellerId : String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameter = ["userId" : userId ,"productId" : productId, "sellerId" : sellerId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,PlaceOrder])
            
            request(.POST, urlString,parameters: parameter, encoding:.URL).responseJSON
            {
                    response in
                    
                    if let JSON = response.result.value
                    {
                       // //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_PlaceOrder!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    //MARK:- successOrder API
    func API_successOrder(userId : String,orderId : String,transactionId : String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameter = ["userId" : userId ,"orderId" : orderId, "transactionId" : transactionId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,successOrder])
            
            request(.POST, urlString,parameters: parameter, encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_successOrder!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- purchaseRewardPointPlan API
    func API_purchaseRewardPointPlan(userId : String,reward_plan_Id : String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameter = ["userId" : userId ,"reward_plan_Id" : reward_plan_Id]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,purchaseRewardPointPlan])
            
            request(.POST, urlString,parameters: parameter, encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_purchaseRewardPointPlan!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- add Billing address
    func API_AddBillingDetails(userId:String,address1:String,address2:String,state:String,country:String,zip:String,city:String,contact:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"address1":address1,"address2":address2,"state":state,"country":country,"zip":zip,"city":city,"contact":contact]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,AddBillingDetails])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_AddBillingDetails!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    //MARK:- get billing details
    func API_getBillingdetails(userId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getBillingdetails])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getBillingdetails!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    //MARK:- editCart API
    func API_editCart(cartId:String,userId:String,quantity:String){
        //editCart
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"cartId":cartId,"quantity":quantity]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,editCart])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_editCart!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    func API_getOrders(userId : NSString)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getOrders])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
            {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getOrders!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
 //MARK:- diarydelete API
    func API_diarydelete(userId : NSString ,id:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"id":id]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,diarydelete])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_diarydelete!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    //MARK:- Shareproduct
    func API_Shareproduct(userId : String , productId:String , shareStatus:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"productId":productId,"shareStatus":shareStatus]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,Shareproduct])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
            {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_Shareproduct!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    //MARK:- AddChatMessage API
    func API_GetMessageConversion(userId:String,chatId:String,toId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"chatId":chatId,"toId":toId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,GetMessageConversion])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_GetMessageConversion!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    
    func API_AddChatMessage(userId:String,toId:String,message:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"toId":toId,"message":message]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,AddChatMessage])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_AddChatMessage!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    //MARK:- getUserInbox API
    func API_getUserInbox(userId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getUserInbox])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getUserInbox!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- getProductDiscussion API
    func API_getProductDiscussion(senderId:String,product_id:String,receiverId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["senderId":senderId,"product_id":product_id,"receiverId":receiverId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getProductDiscussion])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getProductDiscussion!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- addMsgTodiscussion API
    func API_addMsgTodiscussion(senderId:String,productId:String,receiverId:String,message:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["senderId":senderId,"productId":productId,"receiverId":receiverId,"message":message]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,addMsgTodiscussion])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_addMsgTodiscussion!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }

    }
    //MARK:- Add wish api
    func API_addWish(userId:String,productId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"productId":productId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,addWish])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_addWish!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
        
    }
    //MARK:- change password API
    func API_changepassword(userId:String,oldpassword:String,password:String,cpassword:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"oldpassword":oldpassword,"password":password,"cpassword":cpassword]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,changepassword])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_changepassword!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }

    }
    //MARK:- getMyWhishlist API
    func API_getMyWhishlist(userId:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getMyWhishlist])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getMyWhishlist!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
                }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- removeWish API
    func API_removeWish(wishId:String ,userId:String )
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"wishId":wishId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,removeWish])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_removeWish!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    //MARK:- getProductByCategory API
    func API_getProductByCategory(subCatId : String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["subCatId":subCatId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getProductByCategory])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getProductByCategory!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    func API_getMyNews(userId : String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getMyNews])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getMyNews!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    
    func API_getNews(userId : String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,getNews])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                        self.delegate?.API_CALLBACK_getNews!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                        self.delegate?.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
            self.delegate?.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    
    func API_getSearchFriends(userId : String,delegate:ServerAPIDelegate,usertype:String,country:String)
    {
        if Reachability.isConnectedToNetwork() == true
        {
            let parameters = ["userId":userId,"userType":usertype,"search":country]
            let urlString:String = String(format: "%@%@", arguments: [baseURL,searchUSer])
            
            request(.POST, urlString, parameters: parameters,encoding:.URL).responseJSON
                {
                    response in
                    if let JSON = response.result.value
                    {
                        //print("A JSON Result :\(JSON)")
                       delegate.API_CALLBACK_SearchFriends!(JSON as! NSDictionary)
                    }
                    else
                    {
                        //print("A JSON parsing error occurred, here are the details:")
                       delegate.API_CALLBACK_Error(0,errorMessage:ServerNotRespondingMessage)
                    }
            }
        }
        else
        {
            //print("No Internet connection Available")
           delegate.API_CALLBACK_Error(0,errorMessage:NoInternetConnection)
        }
    }
    

}
