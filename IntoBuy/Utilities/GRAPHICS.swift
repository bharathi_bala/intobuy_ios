//
//  GRAPHICS.swift
//  MeetIntro
//
//  Created by Jose on 12/11/15.
//  Copyright © 2015 Smaat Apps and Technologies. All rights reserved.
//

import UIKit
//#define kLightBlueColour "UIColor(red: 216/255, green: 219/255, blue: 248/255, alpha: 1.0)"

var SCREEN_TYPE = NSInteger()
var SCREEN_HEIGHT = CGFloat()
var SCREEN_WIDTH = CGFloat()
var SCREEN_X = CGFloat()
var SCREEN_Y = CGFloat()

let IPHONE_4 = 1
let IPHONE_5 = 2
let IPHONE_6 = 3
let IPHONE_6_Plus = 4
let IPAD = 5

let headerColor:UIColor = UIColor(red: 237.0/255.0, green: 20.0/255.0, blue: 91.0/255.0, alpha: 1.0)
let shadeColor:UIColor = UIColor(red: 230.0/255.0, green: 240.0/255.0, blue: 254.0/255.0, alpha: 1.0)
let grayColor:UIColor = UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
let lightYellowcolor:UIColor = UIColor(red: 252/255.0, green: 248/255.0, blue: 183/255.0, alpha: 1.0)
let lightGrayColor:UIColor = UIColor(red: 131/255.0, green: 131/255.0, blue: 131/255.0, alpha: 1.0)
let textFieldBordercolor:UIColor = UIColor(red: 190/255.0, green: 204/255.0, blue: 210/255.0, alpha: 1.0)

let bottomlineColor:UIColor = UIColor(red: 41/255.0, green: 41/255.0, blue: 41/255.0, alpha: 1.0)

let appPinkColor:UIColor = UIColor(red: 230/255.0, green: 10/255.0, blue: 73/255.0, alpha: 1.0)

public var applightGraycolor : UIColor = UIColor(red: 213/255, green: 213/255, blue: 213/255, alpha: 1.0)

public var applightwhitecolor : UIColor = UIColor(red: 221/255, green: 226/255, blue: 229/255, alpha: 1.0)


public class GRAPHICS
{
    public class func initializeTheScreen ()
    {
        var screenRect : CGRect
        
        screenRect = UIScreen.mainScreen().bounds
        
        SCREEN_HEIGHT = screenRect.size.height
        SCREEN_WIDTH = screenRect.size.width
        
        if screenRect.size.height <= 480
        {
            SCREEN_TYPE = IPHONE_4
        }
        else if (screenRect.size.height > 480.0 && screenRect.size.height <= 580)
        {
            SCREEN_TYPE = IPHONE_5
        }
        else if screenRect.size.width == 375
        {
            SCREEN_TYPE = IPHONE_6
        }
        else if screenRect.size.width == 414
        {
            SCREEN_TYPE = IPHONE_6_Plus
        }
        else
        {
            SCREEN_TYPE = IPAD
        }
        
        SCREEN_X = 0
        SCREEN_Y = 0
    }
    
    public class func Screen_Type() -> NSInteger
    {
        return SCREEN_TYPE
    }
    
    public class func Screen_Width() -> CGFloat
    {
        return SCREEN_WIDTH
    }
    
    public class func Screen_Height() -> CGFloat
    {
        return SCREEN_HEIGHT
    }
    
    public class func Screen_X() -> CGFloat
    {
        return SCREEN_X
    }
    
    public class func Screen_Y() -> CGFloat
    {
        return SCREEN_Y
    }
    
    //FONT Methods
    
    public class func FONT_REGULAR(s:CGFloat) -> UIFont?
    {
        return UIFont (name: "HelveticaNeue", size:s)
    }
    
    public class func FONT_BOLD(s:CGFloat) -> UIFont?
    {
        return UIFont (name: "HelveticaNeue-Bold", size:s)
    }
    public class func FONT_HAPPY_PHANTOM(s:CGFloat) -> UIFont?
    {
        return UIFont (name: "Happy_Phantom", size: s)
    }
    public class func FONT_BOLD_HAPPY_PHANTOM(s:CGFloat) -> UIFont?
    {
        return UIFont (name: "Happy Phantom-Bold", size: s)
    }
    public class func FONT_JENNASUE(s:CGFloat) -> UIFont?
    {
        return UIFont (name: "JennaSue", size: s)
    }
    
    
    // login images
    public class func LOGIN_BACKGROUND_IMAGE() -> UIImage?
    {
        return UIImage(named: "intobuy-login-bg.png")
    }
    public class func LOGIN_BG_IMAGE() -> UIImage?
    {
        return UIImage(named: "intobuy-login-bg_new.png")
    }
    
    public class func LOGIN_LOGO_IMAGE() -> UIImage?
    {
        return UIImage(named: "intobuy-login-logo_new.png")
    }
    public class func LOGIN_TEXTBOX_IMAGE() -> UIImage?
    {
        return UIImage(named: "intobuy-login-text-box_new.png")
    }
    public class func LOGIN_SIGNIN_BTN_IMAGE() -> UIImage?
    {
        return UIImage(named: "intobuy-login-signin-button_new.png")
    }
    //    public class func LOGIN_REGISTER_BTN_IMAGE() -> UIImage?
    //    {
    //        return UIImage(named: "intobuy-login-register-button.png")
    //    }
    public class func LOGIN_POPUP_CLOSE_BTN_IMAGE() -> UIImage?
    {
        return UIImage(named: "intobuy-cose-button.png")
    }
    public class func LOGIN_FB_SIGNIN_BTN_IMAGE() -> UIImage?
    {
        return UIImage(named: "intobuy-login-facebook-button_new.png")
    }
    public class func LOGIN_SIGNUP_BG_IMAGE() -> UIImage?
    {
        return UIImage(named: "intobuy-login-register-bg.png")
    }
    
    // header view images
    public class func SETTINGS_BUTTON_IMAGE() -> UIImage?
    {
        return UIImage(named: "intobuy-mydiary-setings-button.png");
    }
    public class func MENU_BUTTON_IMAGE() -> UIImage?
    {
        return UIImage (named: "intobuy-product-menu-icon.png");
    }
    public class func BACK_BUTTON_IMAGE() -> UIImage?
    {
        return UIImage (named: "intobuy-mydiary-back-button.png")
    }
    public class func PRODUCT_BG_IMAGE() -> UIImage?
    {
        return UIImage (named: "intobuy-products-grid-view-bg.png")
    }
    public class func PRODUCT_GRID_IMAGE() -> UIImage?
    {
        return UIImage (named: "intobuy-products-grid-view-tab.png")
    }
    public class func PRODUCT_SOLD_OUT_IMAGE() -> UIImage?
    {
        return UIImage (named: "My-products-grid-view-sold-out-icon.png")
    }
    public class func PRODUCT_LIST_IMAGE() -> UIImage?
    {
        return UIImage (named: "intobuy-products-list-view-tab.png")
    }
    public class func PRODUCT_SEARCH_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-product-search-icon.png")!
    }
    public class func PRODUCT_SEARCH_TEXT_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-product-search-text-box.png")!
    }
    public class func PRODUCT_SEARCH_DIVIDER_IMAGE() -> UIImage
    {
        return UIImage (named: "into-buy-for-her-header-divider.png")!
    }
    
    // profile screen images
    public class func PROFILE_CAMERA_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-my-diary-camera-icon.png")!
    }
    public class func PROFILE_RATING_BACKGROUND_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-my-diary-grid-view-rating-bg.png")!
    }
    public class func PROFILE_RATING_BUYER_ICON_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-my-diary-grid-view-buyer-points-icon.png")!
    }
    public class func PROFILE_RATING_SELLER_ICON_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-my-diary-grid-view-seller-points-icon.png")!
    }
    public class func PROFILE_RATING_ICON_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-my-diary-grid-view-seller-rating-icon.png")!
    }
    public class func PROFILE_FOLLOW_BUTTON_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-my-diary-grid-view-folloe-button.png")!
    }
    public class func PROFILE_DIVIDER_LINE_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-my-diary-grid-view-divider.png")!
    }
    
    // search screen images
    public class func SEARCH_PROFILE_MASK_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-search-profile-image-mask.png")!
    }
    public class func SEARCH_FOLLOW_BTN_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-search-follow.png")!
    }
    
    public class func SEARCH_DIVIDER_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-search-divider.png")!
    }
    public class func SEARCH_TEXTBOX_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-search-text-box.png")!
    }
    // sell screen images
    public class func SELL_ADD_PRODUCT_BTN_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-sell-add-button.png")!
    }
    public class func SELL_DROP_BTN_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-sell-drop-down-icon.png")!
    }
    public class func SELL_SWITCH_BTN_NORMAL_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-sell-toggle-off.png")!
    }
    public class func SELL_SWITCH_BTN_SELECTED_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-sell-toggle-on.png")!
    }
    public class func SELL_PRODUCT_BTN_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-sell-post-producr-now-button.png")!
    }
    public class func SELL_LINE_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-sell-text-line.png")!
    }
    //
    public class func SELL_QUANTITY_BOX_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-cart-quantity-text-box.png")!
    }
    
    public class func SETTINGS_SWITCH_BTN_NORMAL_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-sell-toglle-off.png")!
    }
    public class func SETTINGS_SWITCH_BTN_SELECTED_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-sell-toglle-on.png")!
    }
    
    public class func SETTINGS_BIG_TEXTBOX_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-Register-text-box.png")!
    }
    public class func SETTINGS_SMALL_TEXTBOX_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-Register-date-and-month-text-box.png")!
    }
    public class func SETTINGS_CARD_TEXTBOX_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-Register-year-text-box.png")!
    }
    public class func SETTINGS_BILLING_BTN_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuySettings-billing-details-button.png")!
    }
    public class func SETTINGS_ARROW_BTN_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuy-settings-aeeow.png")!
    }
    public class func SETTINGS_DROP_DOWN_IMAGE() -> UIImage
    {
        return UIImage (named: "intobuySettings-billing-details-exdp-date-text-box-dropdown-arrow.png")!
    }
    public class func SETTINGS_CHECKBOX_IMAGE_NORMAL() ->  UIImage
    {
        return UIImage (named: "intobuySettings-billing-details-tick-box-bg.png")!
    }
    public class func SETTINGS_CHECKBOX_IMAGE_SELECTED() ->  UIImage
    {
        return UIImage (named: "intobuySettings-billing-details-tickbox.png")!
    }
    // MARK: - settings purchase points
    public class func SETTINGS_DOWN_ARROW_IMAGE() -> UIImage
    {
        return UIImage (named: "Settings-my-points-top-up-drop-down-arrow.png")!
    }
    public class func SETTINGS_AMOUNT_BUTTON_IMAGE() -> UIImage
    {
        return UIImage (named: "Settings-my-points-top-up-drop-down-value-bg.png")!
    }
    
    // MARK: - HomePage Icons
    
    
    public class func HOMEPAGE_POINT_NORMAL()->UIImage
    {
        return UIImage(named: "intobuy-homepage-friends-point-normal.png")!
        
    }
    public class func HOMEPAGE_POINT_SELECTED()->UIImage
    {
        return UIImage(named: "intobuy-homepage-friends-point-select.png")!
        
    }
    
    //    public class func HOMEPAGE_FRIENDS_SHOP_ICON()->UIImage
    //    {
    //        return UIImage(named: "intobuy-homepage-friends-shop-icon.png")!
    //
    //    }
    public class func HOMEPAGE_WEB_ICON()->UIImage
    {
        return UIImage(named: "intobuy-homepage-friends-web-icon.png")!
        
    }
    
    //    public class func HOMEPAGE_FRIENDS_PROFILE()->UIImage
    //    {
    //        return UIImage(named:"intobuy-homepage-friends-profile-icon.png")!
    //
    //    }
    public class func CART_QUANITY_CHCKOUTBTN_IMAGE()->UIImage
    {
        return UIImage(named:"intobuy-cart-quantity-check-out-all-button.png")!
        
    }
    
    // MARK: - membership plan
    public class func MEMBERSHIP_PLAN_BG_IMAGE() -> UIImage
    {
        return UIImage (named: "303.png")!
    }
    public class func MEMBERSHIP_PLAN_BUYNOW_BTN_IMAGE() -> UIImage
    {
        return UIImage (named: "buy_now_btn.png")!
    }
    
    public class func SPLASH_IMAGE() -> UIImage
    {
        // var splashImage = UIImage();
        if(GRAPHICS.Screen_Type() == IPHONE_4)
        {
            return UIImage (named: "intobuy-splash-iphone4.png")!
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_5)
        {
            return  UIImage (named: "intobuy-splash-iphone5.png")!
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6)
        {
            return UIImage (named: "intobuy-splash-iphone6.png")!
        }
        else if(GRAPHICS.Screen_Type() == IPHONE_6_Plus)
        {
            return UIImage (named: "intobuy-splash-iphone6-plus.png")!
        }
        else{
            return UIImage (named: "intobuy-splash-iphone4.png")!
        }
    }
    public class func pink_button() -> UIImage
    {
        return UIImage (named: "pink_btn.png")!
    }
    
    //MARK: - MyDiary Plan
    
    public class func MYDIARY_CAMERABUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-diary-posting-camera-button.png")!
        
    }
    //Intobuy_LeftArrowwithBox.PNG
    
    public class func MYDIARY_DELETEBUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-diary-screen-delete-button.png")!
    }
    public class func MYDIARY_LEFTARROW_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy_LeftArrowwithBox.PNG")!
    }
    public class func MYDIARY_RIGHTARROW_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy_RightArrowwithBox.PNG")!
        
    }
    public class func MYDIARY_GALLERY_ALERT_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-diary-screen-popup-bg.png")!
        
    }
    
    
    
    
    public class func PICKER_LEFT_ARROW_BTN() -> UIImage
    {
        return UIImage (named: "left-arrow.png")!
    }
    public class func PICKER_RIGHT_ARROW_BTN() -> UIImage
    {
        return UIImage (named: "right-arrow.png")!
    }
    //MARK: - home feed images
    public class func HOME_FAVOURITE_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "homepage-newsfeed-with-product-fav-icon.png")!
    }
    public class func HOME_FAVOURITE_BUTTON_SELECTED_IMAGE() -> UIImage
    {
        return UIImage(named: "homepage-newsfeed-with-product-fav-icon-over.png")!
    }
    public class func HOME_COMMENT_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "homepage-newsfeed-with-product-commect-icon.png")!
    }
    public class func HOME_COMMENT_BUTTON_SELECTED_IMAGE() -> UIImage
    {
        return UIImage(named: "homepage-newsfeed-with-product-commect-icon-over.png")!
    }
    public class func HOME_SHARE_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "homepage-newsfeed-with-product-more-icon.png")!
    }
    public class func HOME_ADD_CART_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "homepage-newsfeed-with-product-shop-icon.png")!
    }
    public class func HOME_ADD_CART_PINK_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "homepage-newsfeed-with-product-shop-icon-pink.png")!
    }
    
    
    //MARK: -create post Ui
    public class func CREATE_POST_CROSS_BTN_IMG() -> UIImage{
        return UIImage(named: "cross-button.png")!
    }
    public class func CREATE_POST_TICK_BTN_IMG() -> UIImage{
        return UIImage(named: "tab-tick-button.png")!
    }
    public class func CREATE_POST_HEADER_TICK_BTN_IMG() -> UIImage{
        return UIImage(named: "tick-button.png")!
    }
    public class func COMMENT_BTN_IMG() -> UIImage{
        return UIImage(named: "comment-butto.png")!
    }
    public class func HOME_CART_PLACEHOLDER_BTN_IMG() -> UIImage{
        return UIImage(named: "comment-butto.png")!
    }
    
    public class func DEFAULT_PROFILE_PIC_IMAGE() -> UIImage{
        
        return UIImage(named: "intobuy_default_profile_image.png")!
    }
    public class func DEFAULT_CART_IMAGE() -> UIImage{
        return UIImage(named: "intobuy-place-holder-image-light.png")!
    }
    
    
    //MARK: - method for alert
    public class func showAlert(msg:String!){
        SwiftLoader.hide()
        let alert = UIAlertView(title: "IntoBuy", message:msg, delegate: self, cancelButtonTitle:"Ok")
        alert .show()
    }
    
    //MARK : -AddProduct TextField Images
    public class func DROP_DOWN_IMAGE()->UIImage
    {
        return UIImage (named:"dropdown_arrow.png")!
        
    }
    
    public class func APP_PINK_BUTTON_IAMGE()->UIImage
    {
        return UIImage (named:"intobuy-homepage-friends-unfollow-button.png")!
        
    }
    
    //MARK :-Product detail screen Icons
    public class func PRODUCT_BUYNOW_IMAGE()->UIImage
    {
        return UIImage (named:"My-product-details-2-buy-niw-button.png")!
        
    }
    public class func PRODUCT_RATING_STAR_IMAGE()->UIImage
    {
        return UIImage (named:"My-product-details-2-ratting-star.png")!
        
    }
    public class func PRODUCT_SHARE_ICON_IAMGE()->UIImage
    {
        return UIImage (named:"My-product-details-2-share-icon.png")!
        
    }
    
    //MARK :-Cart Images
    public class func CART_CHECKOUT_ITEM_IMAGE()->UIImage
    {
        return UIImage (named:"intobuy-cart-quantity-check-out-this-item-button.png")!
        
    }
    public class func CART_FAV_IMAGE()->UIImage
    {
        return UIImage (named:"intobuy-cart-fav-icon.png")!
    }
    public class func CART_MAIL_IMAGE()->UIImage
    {
        return UIImage (named:"intobuy-cart-mail-icon.png")!
    }
    
    public class func DEFAULT_CAMERA_IMAGE()->UIImage
    {
        return UIImage (named:"intobuy-sell-add-button.png")!
    }
    // my profile images
    public class func POST_DELETE_BUTTON()-> UIImage
    {
        return UIImage (named: "into-buy-delete-button.png")!
    }
    public class func POST_ALERT_BG_IMAGE()-> UIImage
    {
        return UIImage (named: "into-buy-delete-popup-bg.png")!
    }
    public class func POST_ALERT_BORDER_LINE()-> UIImage
    {
        return UIImage (named: "into-buy-delete-popup-line.png")!
    }
    public class func IMAGE_FULL_SCREEN_DONE_BUTTON()-> UIImage
    {
        return UIImage (named: "into-buy-done-button.png")!
    }
    public class func MY_POST_TRANSPARENT_BG()-> UIImage
    {
        return UIImage (named: "into-buy-tranparent-bg.png")!
    }
    public class func SEARCH_BAR_ICON_IMAGE()-> UIImage
    {
        return UIImage(named: "into-buy-for-her-search-bar.png")!
    }
    //MARK:- order details
    public class func ORDER_DETAILS_SOLD_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-order-details-sold-button.png")!
    }
    public class func ORDER_DETAILS_BOUGHT_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-order-details-bought-button.png")!
    }
    //MARK:- product category
    public class func PRODUCT_CATEGORY_TICK_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-search-wirthin-tick-mark.png")!
    }
    public class func PRODUCT_CATEGORY_UP_ARROW_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-search-wirthin-up-arrow.png")!
    }
    public class func PRODUCT_CATEGORY_DOWN_ARROW_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-search-wirthin-down-arrow.png")!
    }
    public class func PRODUCT_CATEGORY_BORDER_LINE_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-search-wirthin-divider.png")!
    }
    //MARK:- product filter
    public class func PRODUCT_FILTER_TEXTBOX_IMAGE() -> UIImage
    {
        return UIImage(named: "into-buy-filter-maximum-text-box.png")!
    }
    public class func PRODUCT_FILTER_APPLY_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "into-buy-filter-apply-button.png")!
    }
    //MARK:- product location
    public class func PRODUCT_LOCATION_BOTTOMVIEW_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-loaction-bottom-bar.png")!
    }
    public class func PRODUCT_LOCATION_SLIDER_BG_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-loaction-scale-bg.png")!
    }
    public class func PRODUCT_LOCATION_SLIDER_THUMB_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-loaction-scale-pointer.png")!
    }
    //MARK:- Product grid more images
    public class func MY_PRODUCT_GRID_MORE_BG_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-products-grid-view-more-bg.png")!
    }
    public class func MY_PRODUCT_GRID_COMMENT_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-products-grid-view-more-comment.png")!
    }
    public class func MY_PRODUCT_GRID_DELETE_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-products-grid-view-more-delete.png")!
    }
    public class func MY_PRODUCT_GRID_EDIT_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-products-grid-view-more-edit.png")!
    }
    public class func MY_PRODUCT_GRID_SHARE_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-products-grid-view-more-share.png")!
    }
    public class func MY_PRODUCT_GRID_DIVIDER_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-products-grid-view-more-diivder.png")!
    }
    
    
    //MARK:- Product list more images
    public class func MY_PRODUCT_LIST_MORE_BG_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-My-products-list-view-NEW-new-more-popup.png")!
    }
    public class func MY_PRODUCT_LIST_COMMENT_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-My-products-list-view-NEW-new-more-popup-comment.png")!
    }
    public class func MY_PRODUCT_LIST_DELETE_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-My-products-list-view-NEW-new-more-popup-delete.png")!
    }
    public class func MY_PRODUCT_LIST_EDIT_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-My-products-list-view-NEW-new-more-popup-edit.png")!
    }
    public class func MY_PRODUCT_LIST_SHARE_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-My-products-list-view-NEW-new-more-popup-share.png")!
    }
    public class func MY_PRODUCT_LIST_DIVIDER_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-My-products-list-view-NEW-new-more-popup-divider.png")!
    }
    
    //MARK:- activity ui images
    //intobuy-activity-add-feind-ping.png
    public class func ACTIVITY_ADD_FRIEND_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-activity-add-feind-ping.png")!
    }
    public class func ACTIVITY_DIVIDER_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-activity-tab-divider.png")!
    }
    public class func ACTIVITY_BUTTON_NORMAL_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-activity-tab-off.png")!
    }
    public class func ACTIVITY_BUTTON_SELECTED_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-activity-tab-on.png")!
    }
    public class func ACTIVITY_BUTTON_TICK_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-activity-tick-mark.png")!
    }
    
    //MARK:- contact Images
    public class func CONTACTS_HEADER_BUTTON_NORMAL_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-contatcs-tab-normal.png")!
    }
    public class func CONTACTS_HEADER_BUTTON_SELECTED_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-contatcs-tab-selection.png")!
    }
    public class func CONTACTS_ADD_CONTACTS_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-contatcs-add-contacts-normal.png")!
    }
    public class func CONTACTS_CONTACTS_BOOK_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-contatcs-book-icon.png")!
    }
    public class func CONTACTS_MESSAGE_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-contatcs-messages.png")!
    }
    public class func CONTACTS_BLUE_BUBBLE_IMAGE() -> UIImage
    {
        return UIImage(named: "IntoBuy_BlueChatBubblePointer.png")!
    }
    public class func CONTACTS_GRAY_BUBBLE_IMAGE() -> UIImage
    {
        return UIImage(named: "IntoBuy_GreyChatBubblePointer.png")!
    }
    
    
    
    //MARK:- contact us images
    public class func CONTACTS_US_GMAIL_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-Settings-contact-us-gmail.png")!
    }
    public class func CONTACTS_US_MAIL_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-Settings-contact-us-mail.png")!
    }
    public class func CONTACTS_US_OUTLOOK_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-Settings-contact-us-outlook.png")!
    }
    public class func CONTACTS_US_YAHOO_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-Settings-contact-us-yahoo.png")!
    }
    public class func CONTACTS_US_ZICON_BUTTON_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-Settings-contact-us-z-icon.png")!
    }
    
    
    //MARK:- settings languages
    public class func SETTINGS_LANGUAGE_BG() -> UIImage
    {
        return UIImage(named: "Intobuy-Settings-language-bg.png")!
    }
    public class func SETTINGS_LANGUAGE_BTN_NORMAL() -> UIImage
    {
        return UIImage(named: "Intobuy-Settings-language-normal.png")!
    }
    public class func SETTINGS_LANGUAGE_BTN_SELECTED() -> UIImage
    {
        return UIImage(named: "Intobuy-Settings-language-selected.png")!
    }
    
    //MARK: - bottom bar images
    public class func BOTTOM_BAR_HOME_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy-bb-home.png")!
    }
    public class func BOTTOM_BAR_ACTIVITY_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy-bb-activity.png")!
    }
    public class func BOTTOM_BAR_PROFILE_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy-bb-mypage.png")!
    }
    public class func BOTTOM_BAR_CAMERA_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy-bb-eliipse.png")!
    }
    public class func BOTTOM_BAR_CHAT_ICON_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "my page_icon03.png")!
    }
    public class func BOTTOM_BAR_CART_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy-bb-cart.png")!
    }
    public class func BOTTOM_BAR_SELECTION_BG_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy-bb-slection.png")!
    }
    //
    public class func MY_PAGE_Comment_ICON_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "my page_icon01.png")!
    }
    public class func MY_PAGE_LIKE_ICON_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "my page_icon02.png")!
    }
    
    //MARK:- share world images
    public class func SHARE_WORLD_ANNOTATION_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-shareworld-map-pointer.png")!
    }
    public class func SHARE_WORLD_HOME_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-shareworld-home-button.png")!
    }
    public class func SHARE_WORLD_SHUFFLE_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-shareworld-shuffle-button.png")!
    }
    public class func SHARE_WORLD_STAR_ICON_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-shareworld-star-icon.png")!
    }
    
    //MARK:- followers list
    public class func APP_GRAY_IMG() -> UIImage
    {
        return UIImage(named: "intobuy-homepage-friends-unfollow-button.png")!
    }
    public class func FOLLOWING_FOLLOW_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-page-followers-grey-button.png")!
    }
    
    public class func MY_PAGE_BG_IMAGE() -> UIImage
    {
        return UIImage(named: "My-products-list-view-photo-bg.jpg")!
    }
    public class func SELL_DELETE_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-sell-new-delete-button.png")!
    }
    //intobuy-products-grid-view-cart-icon.png
    public class func MY_PRODUCT_GRID_CART_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-products-grid-view-cart-icon.png")!
    }
    //
    public class func MY_PRODUCT_GRID_CART_PINK_BTN_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-products-grid-view-cart-icon-pink.png")!
    }
    //My page
    public class func MY_PAGE_LIKE_BTN_NORMAL_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy-my-page-full-screen-favourite-normal.png")!
    }
    public class func MY_PAGE_LIKE_BTN_SELECTED_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy-my-page-full-screen-favourite-over.png")!
    }
    public class func MY_PAGE_COMMENT_BTN_NORMAL_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy-my-page-full-screen-comment-normal.png")!
    }
    public class func MY_PAGE_COMMENT_BTN_SELECTED_IMAGE() -> UIImage
    {
        return UIImage(named: "Intobuy-my-page-full-screen-comment-over.png")!
    }
    //My product
    public class func MY_PRODUCT_GRID_LIKE_BTN_NORMAL_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-products-grid-view-favourite-normal.png")!
    }
    public class func MY_PRODUCT_GRID_LIKE_BTN_SELECTED_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-products-grid-view-favourite-over.png")!
    }
    public class func MY_PRODUCT_GRID_COMMENT_BTN_NORMAL_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-products-grid-view-comment-normal.png")!
    }
    public class func MY_PRODUCT_GRID_COMMENT_BTN_SELECTED_IMAGE() -> UIImage
    {
        return UIImage(named: "intobuy-my-products-grid-view-comment-over.png")!
    }
      
    public class func checkKeyNotAvail( dict: NSDictionary, key:String) -> AnyObject
    {
        if let val = dict[key]
        {
            if val is String
            {
                return val
            }
            return ""
        }
        else {
            
            return ""
        }
    }
    public class func checkKeyNotAvailInUD( userDefaults: NSUserDefaults, key:String) -> AnyObject
    {
        if let val = userDefaults.objectForKey(key)
        {
            if val is String
            {
                return val
            }
            return ""
        }
        else {
            
            return ""
        }
    }
    
    public class func checkKeyNotAvailForArray( dict: NSDictionary, key:String) -> AnyObject
    {
        if let val = dict[key]
        {
            if val is NSArray
            {
                return val
            }
            return NSArray()
        }
        else {
            
            return NSArray()
        }
    }
    //Get CorrectImage width and Image Height
    public class func getImageWidthHeight(imageName:UIImage)->CGSize
    {
        var fWidth:CGFloat = 0.0
        var fHeight:CGFloat = 0.0
        
        let searchiew:UIImage = imageName
        fWidth = searchiew.size.width/2
        fHeight = searchiew.size.height/2
        
        if GRAPHICS.Screen_Type() == IPHONE_6 || GRAPHICS.Screen_Type() == IPHONE_6_Plus
        {
            fWidth = searchiew.size.width/1.67
            fHeight = searchiew.size.height/1.67
            
        }
        
        return CGSizeMake(fWidth, fHeight)
    }
    
}
