//
//  IBCommentTableViewCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 16/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit

class IBCommentTableViewCell: UITableViewCell {

    var m_profileImgView:UIImageView!
    var m_userNameLbl:UILabel!
    var m_commentLbl:UILabel!
    var m_dateLbl:UILabel!
    var dividerImgView:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControls()
    }
    
    func createControls()
    {
        let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
        var posX = self.bounds.origin.x + 10
        var posY = self.bounds.origin.y + 10
        var width = profileMaskImg.size.width/2
        var height = profileMaskImg.size.height/2
        
        m_profileImgView = UIImageView(frame: CGRectMake(posX,posY,width,height))
       // m_profileImgView.image = profileMaskImg
        m_profileImgView.clipsToBounds = true
        m_profileImgView.layer.cornerRadius = height/2
        m_profileImgView.backgroundColor = UIColor.clearColor()

        self.addSubview(m_profileImgView)
        
        posX = m_profileImgView.frame.maxX + 5
        //posY = posY
        width = GRAPHICS.Screen_Width()/2
        height = 24
        
        m_userNameLbl = UILabel(frame: CGRectMake(posX,posY,width,height))
        m_userNameLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_userNameLbl.text = "dasdbvasd"
        self.addSubview(m_userNameLbl)
        
        
        posX = m_profileImgView.frame.minX
        posY = m_profileImgView.frame.maxY + 5
        width = GRAPHICS.Screen_Width() - m_profileImgView.frame.size.width - 20
        m_commentLbl = UILabel(frame: CGRectMake(posX,posY,width,height))
        m_commentLbl.font = GRAPHICS.FONT_REGULAR(12)
        m_commentLbl.text = "divabs badis;b bi;auds "
        m_commentLbl.numberOfLines = 0;
        m_commentLbl.lineBreakMode = .ByWordWrapping

        self.addSubview(m_commentLbl)
        
        
        width = 100
        posX = GRAPHICS.Screen_Width() - width - 20
        posY = m_userNameLbl.frame.origin.y//m_profileImgView.bounds.origin.y + 10
        m_dateLbl = UILabel(frame: CGRectMake(posX,posY,width,height))
        m_dateLbl.font = GRAPHICS.FONT_REGULAR(12)
        //m_dateLbl.text = "2 days ago"
        m_dateLbl.textAlignment = .Right;
        self.addSubview(m_dateLbl)
        
        //self.frame.size.height = m_commentLbl.frame.maxY + 20
//        // divider image
        let dividerImg = GRAPHICS.SEARCH_DIVIDER_IMAGE()
         width = GRAPHICS.Screen_Width() - 20
         height = dividerImg.size.height/2
         posX = GRAPHICS.Screen_X() + 10//(self.frame.size.width - width)/2
        posY = (self.frame.size.height - height)
        dividerImgView = UIImageView(frame: CGRectMake(0,posY,GRAPHICS.Screen_Width(),height))
        dividerImgView.image = dividerImg
        self.addSubview(dividerImgView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
