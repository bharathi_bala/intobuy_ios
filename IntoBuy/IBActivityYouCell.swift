//
//  IBActivityYouCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 19/02/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBActivityYouCell: UITableViewCell
{
    
    var m_profileImgView:UIImageView!
    var m_postLabel:UILabel!
    // var bottomBorder:UIImageView!
    var m_postedImgView:UIImageView!
    var vwline = UIView()
    let normalHeight = CGFloat(60)
    let dynamicCellheight = CGFloat(110)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    func setValuesForControls(imageArray : NSArray, dateStr : String,delegate:AnyObject,indexpath:NSIndexPath,entity:IBActivityFollowingEntity,btnindex1:NSInteger)
    {
        for view in self.subviews
        {
            view.removeFromSuperview()
        }
        
        var xPos = GRAPHICS.Screen_X() + 10
        var yPos = CGFloat(10)
        var width:CGFloat = 40
        var height:CGFloat = 40
        let rightImgviwWidth = CGFloat(40)
        
        m_profileImgView = UIImageView(frame: CGRectMake(xPos,yPos,width,height))
        m_profileImgView.backgroundColor = UIColor.clearColor()
        m_profileImgView.image = GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE()
        m_profileImgView.layer.cornerRadius = height/2
        m_profileImgView.clipsToBounds = true
        self.addSubview(m_profileImgView)
        
        if entity.useravatar as String != ""
        {
            /*let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) -> Void in
                
            }
            
            m_profileImgView.sd_setImageWithURL(NSURL(string: (entity.useravatar as String))!, placeholderImage:GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completed: block)*/
            m_profileImgView.sd_setImageWithURL(NSURL(string: (entity.useravatar as String))!, placeholderImage: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), options: .ScaleDownLargeImages)
        }
        
        m_profileImgView.userInteractionEnabled = true
        let profileTapGesture = UITapGestureRecognizer(target: delegate, action: #selector(IBActivityViewController.ProfileImgGesture(_:)))
        self.addGestureRecognizer(profileTapGesture)
        
        xPos = m_profileImgView.frame.maxX + 5
        yPos = m_profileImgView.frame.minY
        width = GRAPHICS.Screen_Width() - m_profileImgView.frame.maxX - rightImgviwWidth - 20
        height = m_profileImgView.frame.size.height
        
        var timeDatestr = gettheDateString(entity, indexpath: indexpath, delegate: delegate)
       // //print("time date string %@",timeDatestr)
        
        timeDatestr = ". " + timeDatestr
        
        m_postLabel = UILabel(frame: CGRectMake(xPos,yPos,width,height))
        m_postLabel.backgroundColor = UIColor.clearColor()
        m_postLabel.font = GRAPHICS.FONT_REGULAR(12)
        m_postLabel.numberOfLines = 0
        m_postLabel.attributedText = getActivityAttributeString((entity.text as String) + timeDatestr , valueString: entity.username as String, entity: entity,indexpath: indexpath,delegate1: delegate)
        self.addSubview(m_postLabel)
        
        xPos = GRAPHICS.Screen_Width() - rightImgviwWidth - 10
        
        vwline = UIView(frame:CGRectMake(10,normalHeight - 1,GRAPHICS.Screen_Width() - 20,1))
        vwline.backgroundColor = lightGrayColor
        self.addSubview(vwline)
        
        if m_postedImgView != nil
        {
            m_postedImgView.removeFromSuperview()
            m_postedImgView = nil
        }
        
        yPos = (normalHeight - rightImgviwWidth)/2
        
        if imageArray.count == 1
        {
            m_postedImgView = UIImageView(frame: CGRectMake(xPos,yPos,rightImgviwWidth,rightImgviwWidth))
            let imageDict =  imageArray.objectAtIndex(0) as! NSDictionary
            
            /*let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) -> Void in
                
            }
            
             m_postedImgView.sd_setImageWithURL(NSURL(string: imageDict.objectForKey("image") as! String)!, placeholderImage:GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completed: block)*/
            m_postedImgView.sd_setImageWithURL(NSURL(string: imageDict.objectForKey("image") as! String)!, placeholderImage: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), options: .ScaleDownLargeImages)
            
            self.addSubview(m_postedImgView)
            vwline.frame.origin.y = normalHeight - 1
        }
        else
        {
            xPos = m_postLabel.frame.minX;
            yPos = m_profileImgView.frame.maxY + 5
            
            if imageArray.count > 0
            {
                let scrollviw = UIScrollView(frame: CGRectMake(xPos, yPos,  CGFloat(imageArray.count) * (rightImgviwWidth + 10), rightImgviwWidth))
                scrollviw.backgroundColor = UIColor.clearColor()
                self.addSubview(scrollviw)
                
                xPos  = 0; yPos = 0;
                
                for i in 0 ..< imageArray.count
                {
                    m_postedImgView = UIImageView(frame: CGRectMake(xPos,yPos,rightImgviwWidth,rightImgviwWidth))
                    let imageDict = imageArray.objectAtIndex(i) as! NSDictionary
                    let imageStr = imageDict.objectForKey("image") as! String
                    let imageUrl = NSURL(string: imageStr)
                   
                    /*let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) -> Void in
                        
                    }
                    
                     m_postedImgView.sd_setImageWithURL(imageUrl!, placeholderImage:GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), completed: block)*/
                    m_postedImgView.sd_setImageWithURL(imageUrl!, placeholderImage: GRAPHICS.DEFAULT_PROFILE_PIC_IMAGE(), options: .ScaleDownLargeImages)
                    
                    scrollviw.addSubview(m_postedImgView)
                    xPos = xPos + rightImgviwWidth + 5
                    
                }
                
                scrollviw.contentSize =  CGSizeMake(xPos, rightImgviwWidth)
                
                vwline.frame.origin.y = dynamicCellheight - 1
            }
        }
        
       
        
    }
    
    func gettheDateString(entity:IBActivityFollowingEntity,indexpath:NSIndexPath,delegate:AnyObject) -> String
    {
        var datestr = ""
        
        let addedDate = entity.addedDate as String
        let addedDateStr = convertUtcToLocalFormat(addedDate)
        let dateFormat:NSDateFormatter = NSDateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormat.timeZone = NSTimeZone.localTimeZone()
        let startDate = dateFormat.dateFromString(addedDateStr)
        let intervalString = startDate!.relativeTimeToString()
       // //print("\(addedDateStr), \(intervalString)")
        
        let timeStr:String!
        let timeArray:NSArray = intervalString.componentsSeparatedByString(" ")
        let durationStr = timeArray.objectAtIndex(1) as! String
        let durationIntStr = timeArray.objectAtIndex(0) as! String
        
        if durationStr == "hours" || durationStr == "hour"
        {
            let str = "h"
            datestr = String(format : "%@%@",durationIntStr,str)
        }
        else if durationStr == "days" || durationStr == "day"
        {
            let str = "d"
            datestr = String(format : "%@%@",durationIntStr,str)
        }
        else if durationStr == "minutes" || durationStr == "minute"
        {
            let str = "m"
            datestr = String(format : "%@%@",durationIntStr,str)
        }
        else if durationStr == "seconds" || durationStr == "second"
        {
            let str = "s"
            datestr = String(format : "%@%@",durationIntStr,str)
        }
        else if durationStr == "weeks" || durationStr == "week"
        {
            let str = "w"
            datestr = String(format : "%@%@",durationIntStr,str)
        }
        
        return datestr
        
    }
    //Creation Of Division DrawStatus
    func getActivityAttributeString(totalString:String,valueString:String,entity:IBActivityFollowingEntity,indexpath:NSIndexPath,delegate1:AnyObject) -> NSMutableAttributedString
    {
        var timeDatestr = gettheDateString(entity, indexpath: indexpath, delegate: delegate1)
       // //print("time date string %@",timeDatestr)
        
        timeDatestr = ". " + timeDatestr
        
        let feedtitlestr = totalString
        
        // 1
        let attributedString = NSMutableAttributedString(string: feedtitlestr)
        
        // 2
        let firstAttributes = [NSFontAttributeName: GRAPHICS.FONT_REGULAR(12)!,NSForegroundColorAttributeName:headerColor]
        let secondAttributes = [NSFontAttributeName:GRAPHICS.FONT_REGULAR(12)!,NSForegroundColorAttributeName:headerColor]
        
         let thirdAttribute = [NSFontAttributeName:GRAPHICS.FONT_REGULAR(12)!,NSForegroundColorAttributeName:UIColor.grayColor()]
        
        
        let str = feedtitlestr as NSString
        
        // 3
        attributedString.addAttributes(firstAttributes, range: str.rangeOfString(entity.username as String))
        attributedString.addAttributes(secondAttributes, range: str.rangeOfString(entity.tousername as String))
        attributedString.addAttributes(thirdAttribute, range: str.rangeOfString(timeDatestr))
        
        
        // 4
        return attributedString
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat
    {
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
