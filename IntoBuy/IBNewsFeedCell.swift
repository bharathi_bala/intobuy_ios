//
//  IBNewsFeedCell.swift
//  IntoBuy
//
//  Created by Manojkumar on 20/04/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBNewsFeedCell: UITableViewCell {

    
    var profileImgView = UIImageView()
    var postedImgView = UIImageView()
    var nameLabel = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createControlsForContacts()
    }
    func createControlsForContacts()
    {
        self.frame.size.height = 60.0
        self.frame.size.width = GRAPHICS.Screen_Width()
        let profileMaskImg = GRAPHICS.SEARCH_PROFILE_MASK_IMAGE()
        
        
        var viewWid = profileMaskImg.size.width/1.7
        var viewHei = profileMaskImg.size.height/1.7
        var posX = self.bounds.origin.x + 15
        var posY = (self.frame.size.height - viewHei)/2
        
        profileImgView.frame = CGRectMake(posX,posY,viewWid,viewHei)
        profileImgView.image = profileMaskImg
        self.addSubview(profileImgView)
        
        viewHei = 15
        viewWid = GRAPHICS.Screen_Width()/2.5
        posX = profileImgView.frame.maxX + 10
        posY = (self.frame.size.height - viewHei)/2
        let fontSize:CGFloat = 12;
        nameLabel.frame = CGRectMake(posX,posY,viewWid,viewHei)
        nameLabel.font = GRAPHICS.FONT_REGULAR(fontSize)
        nameLabel.text = "Manoj likes ur post"
        self.addSubview(nameLabel)
        
        
        viewWid = profileMaskImg.size.width/1.7
        viewHei = profileMaskImg.size.height/1.7
        posX = self.frame.size.width - viewWid - 15
        posY = (self.frame.size.height - viewHei)/2
        
        postedImgView.frame = CGRectMake(posX,posY,viewWid,viewHei)
        postedImgView.backgroundColor = UIColor.redColor()
        self.addSubview(postedImgView)

        let dividerImg = GRAPHICS.SEARCH_DIVIDER_IMAGE()
        viewWid = self.frame.size.width - 20//dividerImg.size.width/2;
        viewHei = dividerImg.size.height/2
        posX = (self.frame.size.width - viewWid)/2
        posY = (self.frame.size.height - viewHei)
        let dividerImgView = UIImageView(frame: CGRectMake(posX,posY,viewWid,viewHei))
        dividerImgView.image = dividerImg
        self.addSubview(dividerImgView)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    
    
}
