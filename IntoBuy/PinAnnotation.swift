//
//  PinAnnotation.swift
//  IntoBuy
//
//  Created by SmaatApps on 21/12/15.
//  Copyright © 2015 Premkumar. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import CoreLocation

class PinAnnotation: NSObject,MKAnnotation
{
    
    var title: String?
    var subtitle: String?
    var strImagePropertyURl : String?
    var strAgentName  : String?
    var strPhoneNumber : String?
    var strPinIndex : String?
    var latitude: Double
    var longitude:Double
    var tag : Int?
   // var calloutAnnotatin : CalloutAnnotation?
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    /*
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }

*/

}
