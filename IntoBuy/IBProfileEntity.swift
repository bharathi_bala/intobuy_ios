//
//  IBProfileEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 14/06/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBProfileEntity: NSObject {
    
    var userId: String
    var userType: String
    var username: String
    var fname : String
    var lname : String
    var contact : String
    var email : String
    var country : String
    var CountryName : String
    var aboutme : String
    var avatar : String
    var following : NSArray
    var followers : NSArray
    var rewardpoints : String
    var buytrophy : String
    var selltrophy : String
    var planname : String
    var expiryDate : String
    var myPost : NSArray
    var myProduct : NSArray
    var am_following : String
    
    init(dict: NSDictionary) {
        self.userId = (GRAPHICS.checkKeyNotAvail(dict, key:"userId")as? String)!
        self.userType = (GRAPHICS.checkKeyNotAvail(dict, key:"userType")as? String)!
        self.username = (GRAPHICS.checkKeyNotAvail(dict, key:"username")as? String)!
        self.fname = (GRAPHICS.checkKeyNotAvail(dict, key:"fname")as? String)!
        self.lname = (GRAPHICS.checkKeyNotAvail(dict, key:"lname")as? String)!
        self.contact = (GRAPHICS.checkKeyNotAvail(dict, key:"contact")as? String)!
        self.email = (GRAPHICS.checkKeyNotAvail(dict, key:"email")as? String)!
        self.country = (GRAPHICS.checkKeyNotAvail(dict, key:"country")as? String)!
        self.CountryName = (GRAPHICS.checkKeyNotAvail(dict, key:"CountryName")as? String)!
        self.aboutme = (GRAPHICS.checkKeyNotAvail(dict, key:"aboutme")as? String)!
        self.avatar = (GRAPHICS.checkKeyNotAvail(dict, key:"avatar")as? String)!
        self.following = (GRAPHICS.checkKeyNotAvailForArray(dict, key: "following")as! NSArray)
        self.followers = (GRAPHICS.checkKeyNotAvailForArray(dict, key: "followers")as! NSArray)
        self.rewardpoints = (GRAPHICS.checkKeyNotAvail(dict, key:"rewardpoints")as? String)!
        self.buytrophy = (GRAPHICS.checkKeyNotAvail(dict, key:"buytrophy")as? String)!
        self.selltrophy = (GRAPHICS.checkKeyNotAvail(dict, key:"selltrophy")as? String)!
        self.planname = (GRAPHICS.checkKeyNotAvail(dict, key:"planname")as? String)!
        self.expiryDate = (GRAPHICS.checkKeyNotAvail(dict, key:"expiryDate")as? String)!
        self.myPost = (GRAPHICS.checkKeyNotAvailForArray(dict, key: "myPost")as! NSArray)
        self.myProduct = (GRAPHICS.checkKeyNotAvailForArray(dict, key: "myProduct")as! NSArray)
        self.am_following = (GRAPHICS.checkKeyNotAvail(dict, key:"am_following")as? String)!
    }



}
