//
//  INActivityFollowingEntity.swift
//  IntoBuy
//
//  Created by Manojkumar on 05/05/16.
//  Copyright © 2016 Premkumar. All rights reserved.
//

import UIKit

class IBActivityFollowingEntity: NSObject {
    
    var newsId: String
    var wall_id: String
    var userId: String
    var type: String
    var text: String
    var addedDate: String
    var touseravatar: String
    var tousername: String
    var useravatar: String
    var username: String
    var mainimage: String
    var images: NSArray
    
    
    //SearchFRiends Crendials
    var strUserID :String
    var strUserType:String
    var strUserName:String
    var strfname:String
    var strlName:String
    var strContact:String
    var stremail:String
    var strCountry:String
    var strCountryName:String
    var straboutme:String
    var stravathar:String
    var strrewardPoints:String
    var strbuyTrophy:String
    var strSellTrophy:String
    var strPlanname:String
    var strExpiryDate:String
    var stramIFollowing:String
    var strisBlocked:String
    
    init(dict: NSDictionary)
    {
        self.newsId = (GRAPHICS.checkKeyNotAvail(dict, key:"newsId")as? String)!
        self.wall_id = (GRAPHICS.checkKeyNotAvail(dict, key:"wall_id")as? String)!
        self.userId = (GRAPHICS.checkKeyNotAvail(dict, key:"userId")as? String)!
        self.type = (GRAPHICS.checkKeyNotAvail(dict, key:"type")as? String)!
        self.text = (GRAPHICS.checkKeyNotAvail(dict, key:"text")as? String)!
        self.mainimage = (GRAPHICS.checkKeyNotAvail(dict, key:"mainimage")as? String)!
        self.addedDate = (GRAPHICS.checkKeyNotAvail(dict, key:"addedDate")as? String)!
        self.touseravatar = (GRAPHICS.checkKeyNotAvail(dict, key:"touseravatar")as? String)!
        self.tousername = (GRAPHICS.checkKeyNotAvail(dict, key:"tousername")as? String)!
        self.useravatar = (GRAPHICS.checkKeyNotAvail(dict, key:"useravatar")as? String)!
        self.username = (GRAPHICS.checkKeyNotAvail(dict, key:"username")as? String)!
        self.images = (GRAPHICS.checkKeyNotAvailForArray(dict, key:"images")as? NSArray)!
        
        //Search Credentials
        self.strUserID = (GRAPHICS.checkKeyNotAvail(dict, key:"userId")as? String)!
        self.strUserType = (GRAPHICS.checkKeyNotAvail(dict, key:"userType")as? String)!
        self.strUserName = (GRAPHICS.checkKeyNotAvail(dict, key:"username")as? String)!
        self.strfname = (GRAPHICS.checkKeyNotAvail(dict, key:"fname")as? String)!
        self.strlName = (GRAPHICS.checkKeyNotAvail(dict, key:"lname")as? String)!
        self.strContact = (GRAPHICS.checkKeyNotAvail(dict, key:"contact")as? String)!
        self.stremail = (GRAPHICS.checkKeyNotAvail(dict, key:"email")as? String)!
        self.strCountry = (GRAPHICS.checkKeyNotAvail(dict, key:"country")as? String)!
        self.strCountryName = (GRAPHICS.checkKeyNotAvail(dict, key:"CountryName")as? String)!
        self.straboutme = (GRAPHICS.checkKeyNotAvail(dict, key:"aboutme")as? String)!
        self.stravathar = (GRAPHICS.checkKeyNotAvail(dict, key:"avatar")as? String)!
        self.strrewardPoints = (GRAPHICS.checkKeyNotAvail(dict, key:"rewardpoints")as? String)!
        self.strbuyTrophy = (GRAPHICS.checkKeyNotAvail(dict, key:"buytrophy")as? String)!
        self.strSellTrophy = (GRAPHICS.checkKeyNotAvail(dict, key:"selltrophy")as? String)!
        self.strPlanname = (GRAPHICS.checkKeyNotAvail(dict, key:"planname")as? String)!
        self.strExpiryDate = (GRAPHICS.checkKeyNotAvail(dict, key:"expiryDate")as? String)!
        self.stramIFollowing = (GRAPHICS.checkKeyNotAvail(dict, key:"amIfollowing")as? String)!
        self.strisBlocked = (GRAPHICS.checkKeyNotAvail(dict, key:"isBlocked")as? String)!
        
    }
    
    
}
